��    Z      �     �      �     �     �     �     �     �               ,     =     N     T     Z     `     f  6   m     �     �     �  %   �     �     �     	  	   	     	     	     &	     ,	     >	     G	     N	  
   S	     ^	     f	     k	     p	     u	     }	     �	     �	     �	  	   �	     �	     �	     �	  y   �	     3
  
   P
     [
     d
  	   q
     {
     �
     �
  (   �
  6   �
         2   !     T     t  	   }     �     �     �     �  	   �     �  /   �     �               '     8     I     [     _     d     j     p          �     �     �     �     �     �     �     �     �     �  =  �     =     B     S     b     s     �     �     �     �     �     �     �     �     �  ?   �     3     ;     T  *   Z     �     �     �  
   �     �  	   �     �     �  	   �     �     �     �          
            	        '     .     3     A     I     Y     b     j  f   s  #   �     �               "     +     2     :     H  ;   h  *   �  )   �  +   �     %     2     ;     G     T     Y     a  	   n  1   x     �     �     �     �               /     1     6     <     B     U     k     �     �     �     �     �     �     �     �  
   �     P           	   -   "       4      %   ;   N         H       +      A   E   
      T   O       $       F   2   ,   K   I   C   Z       W              ?          V              8       '      3         R   Y       =   U           #   J               0       /   )          B              S             <                 !              6              L            (   X   .   D       :      9                         1      G             >   *   7       &   M   Q   5   @         of  %d Byte %d Bytes %d day %d days %d hour %d hours %d minute %d minutes %d month %d months %d second %d seconds %d week %d weeks %d year %d years %s GB %s KB %s MB %s TB %s ago A view block with the name '%s' is already/still open. Actions An Internal Error Has Occurred. April Are you sure you want to delete # %s? August December Delete Delete %s Edit Edit %s Error Error in field %s February Friday Home Invalid %s January July June List List %s March May Monday New %s Not Found November October On %s %s Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end} Please correct errors below. Related %s Saturday Scaffold ::  September Submit Sunday The %1$s has been %2$s The %1$s with id: %2$s has been deleted. The requested address %s was not found on this server. The requested file was not found There was an error deleting the %1$s with id: %2$s This field cannot be left blank Thursday Today, %s Tomorrow, %s Tuesday View View %s Wednesday Yesterday, %s You are not authorized to access that location. about a day ago about a minute ago about a second ago about a week ago about a year ago about an hour ago and days empty in %s in about a day in about a minute in about a second in about a week in about a year in about an hour just now next on %s previous saved updated Project-Id-Version: PROJECT VERSION
PO-Revision-Date: 2016-07-27 10:23-0300
Language-Team: EMAIL@ADDRESS
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
POT-Creation-Date: 
X-Generator: Poedit 1.8.8
Last-Translator: 
Language: pt_BR
  de  %d Byte %d Bytes %d dia %d dias %d hora $d horas %d minuto %d minutos %d mês %d meses %d segundo %d segundos %d semana %d semanas %d ano %d anos %s GB %s KB %s MB %s TB há %s Um “view block” com o nome ‘%s’ já/ainda está aberto. Ações Um erro interno ocorreu. Abril Você tem certeza que deseja excluir # %s? Agosto Dezembro Excluir Excluir %s Editar Editar %s Erro Erro no campo %s Fevereiro Sexta-feira Página Inicial Inválido %s Janeiro Julho Junho Listar Listar %s Março Maio Segunda-feira Novo %s Não Encontrado Novembro Outubro No %s %s Página {:page} de {:pages}, exibindo {:current} registros de {:count}, do registro {:start} ao {:end} Por favor, corrija os erros abaixo. %s relacionado Sábado Scaffold ::  Setembro Enviar Domingo %1$s foi %2$s %1$s com id: %2$s foi excluido. O endereço requisitado %s não foi encontrado no servidor. O arquivo requisitado não foi encontrado. Houve um erro excluindo %1$s com id: %2$s Este campo não pode ser deixado em branco. Quinta-feira Hoje, %s Amanhã, %s Terça-feira View View %s Quarta-feira Ontem, %s Você não está autorizado a acessar este local. há cerca de um dia há cerca de um minuto há cerca de um segundo há cerca de uma semana há cerca de um ano há cerca de uma hora e dias vazio em %s em cerca de um dia em cerca de um minuto em cerca de um segundo em cerca de uma semana em cerca de um ano em cerca de uma hora agora próximo no %s anterior salvo atualizado 