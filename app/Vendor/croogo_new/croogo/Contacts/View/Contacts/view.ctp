<section class="m-content">
	<div class="m-article-header">
		<div class="l-wrapper">
	        <header class="b-article-header">
				<div class="b-article-header_large">
	            	<h2 class="b-article-header_title b-article-header_title--yellow">
	            		<?php echo $contact['Contact']['title']; ?>
	            	</h2>
	          	</div>
			</header>
		</div>
    </div>
	
	<?php 
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('pagina_contato')){
		echo $this->Regions->blocks('pagina_contato'); 
	}
	?>
	<div class="m-facaparte u-spacer_down-400" id="contact-<?php echo $contact['Contact']['id']; ?>">
		<div class="l-wrapper">
	    	<div class="row">
				<div class="col-sm-6">
                	<article class="b-article b-article--no-margin">
                		<?php 
                		//Se tem algum bloco ativo nesta região
						if ($this->Regions->blocks('esquerda_contato')){
							echo $this->Regions->blocks('esquerda_contato'); 
						}
                		echo $contact['Contact']['body']; 
                		?>
                	</article>
              	</div>
              	<div class="col-sm-6">
                	<header class="l-header l-header--small l-header--yellow-bar">
                 		<h4 class="l-header_title l-header_title--small">
                 			<?php echo $contact['Contact']['title']; ?>
                 		</h3>
                	</header>
	                <?php if ($contact['Contact']['message_status']):  ?>
					<fieldset>
						<?php
							echo $this->Form->create('Message', array(
								'url' => array(
									'plugin' => 'contacts',
									'controller' => 'contacts',
									'action' => 'view',
									$contact['Contact']['alias'],
								),
							));
				
							$this->Form->inputDefaults(array(
								'label' => false,
								'class' => 'form-control',
								'div' => array('class' => 'form-group'),
							));
				
							echo $this->Form->input('Message.name', array('placeholder' => __('Seu nome'))); 
							echo $this->Form->input('Message.email', array('placeholder' => __('Seu email'))); 
							echo $this->Form->input('Message.title', array('placeholder' => __('Assunto'))); 
							echo $this->Form->input('Message.body', array('placeholder' => __('Mensagem'))); 
							if ($contact['Contact']['message_captcha']):
								echo $this->Recaptcha->display_form();
							endif; 
				
							echo $this->Form->button(__('Enviar'), array('class' => 'btn btn-info'));
							echo $this->Form->end();
						?>
							
					</fieldset>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section> 