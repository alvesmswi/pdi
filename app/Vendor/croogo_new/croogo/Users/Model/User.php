<?php

App::uses('UsersAppModel', 'Users.Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User
 *
 * @category Model
 * @package  Croogo.Users.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class User extends UsersAppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'User';

/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
	public $actsAs = array(
		'Acl' => array(
			'className' => 'Croogo.CroogoAcl',
			'type' => 'requester',
		),
		'Croogo.Trackable',
		'Search.Searchable',
	);


/**
 * Model associations: belongsTo
 *
 * @var array
 * @access public
 */
	public $belongsTo = array('Users.Role');

/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
		'username' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Nome de usuário já existente.',
				'last' => true,
			),
			'notEmpty' => array(
				'rule' => 'notBlank',
				'message' => 'Este campo é obrigatório',
				'last' => true,
			),
			/*'validAlias' => array(
				'rule' => 'validAlias',
				'message' => 'Este campo precisa ser Alfanumerico',
				'last' => true,
			),*/
		),
		'email' => array(
			'email' => array(
				'rule' => 'email',
				'message' => 'Por favor, forneça um endereço de e-mail válido.',
				'last' => true,
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Email já existente.',
				'last' => true,
			),
		),
		'password' => array(
			'rule' => array('minLength', 6),
			'message' => 'As senhas devem ter pelo menos 6 caracteres.',
		),
		'verify_password' => array(
			'rule' => 'validIdentical',
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => 'notBlank',
				'message' => 'Este campo não pode ser deixado em branco.',
				'last' => true,
			),
			/*'validName' => array(
				'rule' => 'validName',
				'message' => 'Este campo deve ser alfanumérico',
				'last' => true,
			),*/
		),
		'website' => array(
			'url' => array(
				'rule' => 'url',
				'message' => 'Este campo deve ser uma URL válida',
				'allowEmpty' => true,
			),
		),
	);

/**
 * Filter search fields
 *
 * @var array
 * @access public
 */
	public $filterArgs = array(
		'name' => array('type' => 'like', 'field' => array('User.name', 'User.username')),
		'role_id' => array('type' => 'value'),
		'email' => array('type' => 'like', 'field' => array('User.email')),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'name',
		'email',
		'Role.title' => 'Role',
		'data_expiracao' => array('type' => 'date'),
		'status' => array('type' => 'boolean'),
	);

/**
 * Edit fields for this model
 *
 * @var array
 */
	protected $_editFields = array(
		'role_id',
		'username',
		'name',
		'email',
		'website',
		'status',
	);

/**
 * Constructor. Configures the order property.
 *
 * @param bool|int|string|array $id Set this ID for this model on startup,
 * can also be an array of options, see above.
 * @param string $table Name of database table to use.
 * @param string $ds DataSource connection name.
 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->order = $this->alias . '.name ASC';
	}

/**
 * beforeDelete
 *
 * @param boolean $cascade
 * @return boolean
 */
	public function beforeDelete($cascade = true) {
		$this->Role->Behaviors->attach('Croogo.Aliasable');
		$adminRoleId = $this->Role->byAlias('admin');

		$current = AuthComponent::user();
		if (!empty($current['id']) && $current['id'] == $this->id) {
			return false;
		}
		if ($this->field('role_id') == $adminRoleId) {
			$count = $this->find('count', array(
				'conditions' => array(
					$this->escapeField() . ' <>' => $this->id,
					$this->escapeField('role_id') => $adminRoleId,
					$this->escapeField('status') => true,
				)
			));
			return ($count > 0);
		}
		return true;
	}

/**
 * beforeSave
 *
 * @param array $options
 * @return boolean
 */
	public function beforeSave($options = array()) {
		if (!empty($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}

		if (isset($this->data[$this->alias]['data_expiracao']) && !empty($this->data[$this->alias]['data_expiracao'])) {
			$data = str_replace('/', '-', $this->data[$this->alias]['data_expiracao']);
			$this->data[$this->alias]['data_expiracao'] = date('Y-m-d H:i:s', strtotime($data));
		}

		if(!empty($this->data['User']['foto']['name'])) {
			$this->data['User']['foto'] = $this->upload($this->data['User']['foto']);
		} else {
			unset($this->data['User']['foto']);
		}

		// pr($this->data);exit;
		return true;
	}

/**
 * _identical
 *
 * @param string $check
 * @return boolean
 * @deprecated Protected validation methods are no longer supported
 */
	protected function _identical($check) {
		return $this->validIdentical($check);
	}

/**
 * validIdentical
 *
 * @param string $check
 * @return boolean
 */
	public function validIdentical($check) {
		if (isset($this->data[$this->alias]['password'])) {
			if ($this->data[$this->alias]['password'] != $check['verify_password']) {
				return __d('croogo', 'As senhas não coincidem. Por favor, tente novamente.');
			}
		}
		return true;
	}

	public function upload($imagem = array(), $dir = 'img/foto_user')
	{
		$dir = WWW_ROOT.$dir.DS;

		if(($imagem['error']!=0) and ($imagem['size']==0)) {
			throw new NotImplementedException('Alguma coisa deu errado, o upload retornou erro '.$imagem['error'].' e tamanho '.$imagem['size']);
		}

		$this->checa_dir($dir);

		$imagem = $this->checa_nome($imagem, $dir);

		$this->move_arquivos($imagem, $dir);

		return $imagem['name'];
	}

	public function checa_dir($dir)
	{
		App::uses('Folder', 'Utility');
		$folder = new Folder();
		if (!is_dir($dir)){
			$folder->create($dir);
		}
	}

	/**
	 * Verifica se o nome do arquivo já existe, se existir adiciona um numero ao nome e verifica novamente
	 * @access public
	 * @param Array $imagem
	 * @param String $data
	 * @return nome da imagem
	*/
	public function checa_nome($imagem, $dir)
	{
		$imagem_info = pathinfo($dir.$imagem['name']);
		$imagem_nome = $this->trata_nome($imagem_info['filename']).'.'.$imagem_info['extension'];
		// debug($imagem_nome);
		$conta = 2;
		while (file_exists($dir.$imagem_nome)) {
			$imagem_nome  = $this->trata_nome($imagem_info['filename']).'-'.$conta;
			$imagem_nome .= '.'.$imagem_info['extension'];
			$conta++;
			// debug($imagem_nome);
		}
		$imagem['name'] = $imagem_nome;
		return $imagem;
	}

	/**
	 * Trata o nome removendo espaços, acentos e caracteres em maiúsculo.
	 * @access public
	 * @param Array $imagem
	 * @param String $data
	*/
	public function trata_nome($imagem_nome)
	{
		$imagem_nome = strtolower(Inflector::slug($imagem_nome,'-'));
		return $imagem_nome;
	}

	/**
	 * Move o arquivo para a pasta de destino.
	 * @access public
	 * @param Array $imagem
	 * @param String $data
	*/
	public function move_arquivos($imagem, $dir)
	{
		App::uses('File', 'Utility');
		$arquivo = new File($imagem['tmp_name']);
		$arquivo->copy($dir.$imagem['name']);
		$arquivo->close();
	}

}
