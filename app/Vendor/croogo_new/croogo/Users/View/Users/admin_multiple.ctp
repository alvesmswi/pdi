<?php

$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb($this->Html->icon('home'), '/admin')
	->addCrumb(__d('croogo', 'Users'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'index'))
	->addCrumb(__d('croogo', 'Usuários em Massa'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'multiple'));

$this->set('title_for_layout', __d('croogo', 'Usuários em Massa'));

$this->start('actions');
$this->end();

$this->append('form-start', $this->Form->create('User', array(
	'fieldAccess' => array(
		//'User.role_id' => 1,
	),
	'class' => 'protected-form',
)));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Usuários em Massa'), '#user-multiple');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('user-multiple') .
		$this->Form->input('role_id', array('label' => __d('croogo', 'Role'))) .
		'<div id="dias-de-uso">' .
		$this->Form->input('dias_uso', array(
			'label' => __d('croogo', 'Dias de uso'),
			'type' => 'number',
			'min' => 1
		)) .
		'</div>' .
		$this->Form->input('email', array(
			'label' => __d('croogo', 'E-mails (separados por "<b>;</b>")'),
			'placeholder' => 'email1@exeplo.com.br;email2@exemplo.com.br',
			'type' => 'textarea',
			'autocomplete' => 'off'
		));
	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(
			__d('croogo', 'Cancel'), array('action' => 'index'),
			array('button' => 'danger')
		);

	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());

$script = <<<EOF
	$('#UserRoleId').on('change', function(e) {
		var role_id = $('#UserRoleId').val();

		if (role_id == 5 || role_id == 6 || role_id == 7) {
			$('#dias-de-uso').hide();
			$('#dias-de-uso #UserDiasUso').attr('required', false);
		} else {
			$('#dias-de-uso').show();
			$('#dias-de-uso #UserDiasUso').attr('required', true);
		}
	});

	$('#UserRoleId').trigger('change');
EOF;

$this->Js->buffer($script);
