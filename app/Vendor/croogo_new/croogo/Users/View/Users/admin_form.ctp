<?php

$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb($this->Html->icon('home'), '/admin')
	->addCrumb(__d('croogo', 'Users'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['User']['name'], array(
		'plugin' => 'users', 'controller' => 'users', 'action' => 'edit',
		$this->request->data['User']['id']
	));
	$this->set('title_for_layout', __d('croogo', 'Edit user %s', $this->request->data['User']['username']));

	$data_expiracao = '';
	if (!empty($this->request->data['User']['data_expiracao'])) {
		$data_expiracao = date('d/m/Y', strtotime($this->request->data['User']['data_expiracao']));
	}
} else {
	$this->set('title_for_layout', __d('croogo', 'New user'));
}

if ($this->request->params['action'] == 'admin_add') {
	$data_expiracao = '';
	$this->Html->addCrumb(__d('croogo', 'Add'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'add'));
}

$this->start('actions');
if ($this->request->params['action'] == 'admin_edit'):
	echo $this->Croogo->adminAction(__d('croogo', 'Reset password'), array('action' => 'reset_password', $this->request->params['pass']['0']));
endif;
$this->end();

$this->append('form-start', $this->Form->create('User', array(
	'fieldAccess' => array(
		//'User.role_id' => 1,
	),
	'class' => 'protected-form',
	'enctype' => 'multipart/form-data'
)));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'User'), '#user-main');
	echo $this->Croogo->adminTab(__d('croogo', 'Dados pessoais'), '#user-dados');
	echo $this->Croogo->adminTab(__d('croogo', 'Foto'), '#foto');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('user-main') .
		$this->Form->input('id') .
		$this->Form->input('role_id', array('label' => __d('croogo', 'Role'))) .
		$this->Form->input('username', array(
			'label' => __d('croogo', 'Username'),
		)) .
		$this->Form->input('name', array(
			'label' => __d('croogo', 'Name'),
		)) .
		$this->Form->input('email', array(
			'label' => __d('croogo', 'Email'),
		));
	echo $this->Html->tabEnd();

	echo $this->Html->tabStart('user-dados') .
		$this->Form->input('bio', array(
			'label' => 'Biografia'
		)) .
		$this->Form->input('website', array(
			'label' => __d('croogo', 'Website'),
		)) .
		$this->Form->input('facebook', array(
			'label' => __d('croogo', 'Facebook'),
		)) .
		$this->Form->input('instagram', array(
			'label' => __d('croogo', 'Instagram'),
		)) .
		$this->Form->input('twitter', array(
			'label' => __d('croogo', 'Twitter'),
		)) .
		$this->Form->input('telefone', array(
			'label' => __d('croogo', 'Telefone'),
		)) .
		$this->Form->input('cpf', array(
			'label' => __d('croogo', 'CPF'),
		)) .
		$this->Form->input('endereco', array(
			'label' => __d('croogo', 'Endereço'),
		)) .
		$this->Form->input('numero', array(
			'label' => __d('croogo', 'Número'),
		)) .
		$this->Form->input('cep', array(
			'label' => __d('croogo', 'CEP'),
		));
	echo $this->Html->tabEnd();

	echo $this->Html->tabStart('foto') .
		$this->Form->input('foto', array(
			'label' => __d('croogo', 'Foto'),
			'type' => 'file'
		));

	if(isset($this->data['User']['foto']) && !empty($this->data['User']['foto'])){
		echo $this->Form->input('check', array(
			'label' => 'Apagar Foto?',
			'type'  => 'checkbox'
		));
		echo '<img src="'.Router::url('/', true).'img/foto_user/'.$this->data['User']['foto'].'" class="thumbnail" style="width:200px" />';
	}

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(
			__d('croogo', 'Cancel'), array('action' => 'index'),
			array('button' => 'danger')
		);

	if ($this->request->params['action'] == 'admin_add'):
		echo $this->Form->input('notification', array(
			'label' => __d('croogo', 'Send Activation Email'),
			'type' => 'checkbox',
		));
	endif;

	echo $this->Form->input('status', array(
		'label' => __d('croogo', 'Status'),
	));

	$showPassword = !empty($this->request->data['User']['status']);
	if ($this->request->params['action'] == 'admin_add'):
		$out = $this->Form->input('password', array(
			'label' => __d('croogo', 'Password'),
			'disabled' => !$showPassword,
		));
		$out .= $this->Form->input('verify_password', array(
			'label' => __d('croogo', 'Verify Password'),
			'disabled' => !$showPassword,
			'type' => 'password'
		));

		$this->Form->unlockField('User.password');
		$this->Form->unlockField('User.verify_password');

		echo $this->Html->div(null, $out, array(
			'id' => 'passwords',
			'style' => $showPassword ? '' : 'display: none',
		));
	endif;

	echo '<div id="data-expiracao">';
	echo $this->Form->input('data_expiracao', array(
		'type' => 'text',
		'label' => 'Data de expiração',
		'value' => $data_expiracao,
		'class' => 'input-datetime2',
		'style' => 'text-align:left;'
	));
	echo '</div>';

	?>

	<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.input-datetime2').datetimepicker({
				dateFormat: 'dd/mm/yy',
				showTimepicker: false
			})

			$('#UserRoleId').change(function() {
				var grupo_nome = $('#UserRoleId option:selected').text();
				if (grupo_nome != 'Admin' && grupo_nome != 'Colunista' && grupo_nome != 'Gerente' && grupo_nome != 'Editor') {
					$('#data-expiracao').show();
				} else {
					$('#data-expiracao').hide();
					$('#data-expiracao').val('');
				}
			});

			$('#UserRoleId').trigger('change');
		;});
	</script>

	<?php

	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());

$script = <<<EOF
	$('#UserStatus').on('change', function(e) {
		var passwords = $('#passwords');
		var elements = $('input', passwords);
		elements.prop('disabled', !this.checked);
		if (this.checked) {
			passwords.show('fast');
		} else {
			passwords.hide('fast');
		}
	});
	$('#UserNotification').on('change', function(e) {
		var status = $('#UserStatus');
		status.attr('checked', false);
		status.trigger('change').parent().toggle('fast');
	});
EOF;

if ($this->request->params['action'] == 'admin_add'):
	$this->Js->buffer($script);
endif;
