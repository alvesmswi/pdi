<?php
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Users'), '/admin/users')
	->addCrumb(__d('croogo', 'Assinantes expirando'), '/' . $this->request->url);

// $this->append('search', $this->element('admin/search_expiracao'));

$this->append('form-start', $this->Form->create(
	'User',
	array(
		'url' => array('controller' => 'users', 'action' => 'enviarEmailExpiracaoEmMassa'),
		'class' => 'form-inline'
	)
));

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('name', __d('croogo', 'Name')),
		$this->Paginator->sort('email', __d('croogo', 'Email')),
		$this->Paginator->sort('data_expiracao', __d('croogo', 'Expiração')),
		''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php foreach ($assinantes as $assinante): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('User.' . $assinante['User']['id'] . '.enviarEmail', array('class' => 'row-select')); ?></td>
		<td><?php echo $assinante['User']['id']; ?></td>
		<td><?php echo $assinante['User']['name']; ?></td>
		<td><?php echo $assinante['User']['email']; ?></td>
		<td><?php echo date('d/m/Y', strtotime($assinante['User']['data_expiracao'])); ?></td>
		<td>
			<div class="item-actions">
			<?php
				echo $this->Croogo->adminRowActions($assinante['User']['id']);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'enviarEmailExpiracao', $assinante['User']['id']),
					array('icon' => $this->Theme->getIcon('envelope'), 'tooltip' => __d('croogo', 'Enviar e-mail de alerta'))
				);
			?>
			</div>
		</td>
	</tr>
<?php endforeach ?>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('User.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'enviar_email' => __d('croogo', 'Enviar e-mail')
		),
		'empty' => true,
		'required' => true
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'submit',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('User.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));

$this->end();

$this->append('form-end', $this->Form->end());
