<?php

$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb($this->Html->icon('home'), '/admin')
	->addCrumb(__d('croogo', 'Users'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'index'))
	->addCrumb(__d('croogo', 'Usuários em Massa'), array('plugin' => 'users', 'controller' => 'users', 'action' => 'multiple'));

$this->set('title_for_layout', __d('croogo', 'Usuários em Massa'));
?>

<script>
	function ajaxProcessamento() {
		$.ajax({
			async:true,
			type:'post',
			complete:function(request, json) {
				$('#porcentagem').html(request.responseText);
			},
			url:'/admin/users/users/multiple_ajax'
		});
	}
</script>

<?php
echo '<div id="porcentagem">';
	if (!isset($concluido)) {
		// echo $this->Form->button('Chamar novamente', array('type' => 'button', 'onclick' => 'ajaxProcessamento()'));
		echo '<script>setTimeout("ajaxProcessamento();", 0);</script>';

		echo '<div class="alert alert-info fade in" style="margin: 10px;">';
			echo 'Aguarde, criando Usuário <strong>'.$processadas.'</strong> de <strong>'.$total.'</strong>';
		echo '</div>';
	} else {
		echo '<div class="alert alert-success fade in" style="margin:10px;">';
			echo 'Execução finalizada. <span class="label label-success">'.$processadas.' processadas</span>'. ((!empty($erros))?('<span class="label label-important">'.count($erros).' erros</span>'):(null));
		echo '</div>';
	}

	echo '<div class="progress progress-striped active" style="margin: 10px;">';
		echo '<div class="bar" style="width: '.$porcentagem.'%;"><span>'.$porcentagem.'%</span></div>';
	echo '</div>';

	if (isset($concluido)) {
		echo '<div class="form-actions" style="text-align: right;">';
			echo '<a href="/admin/users" class="btn btn-success data-loading" data-loading-text="Concluindo...">Concluir</a>';
		echo '</div>';
	}
echo '</div>';
