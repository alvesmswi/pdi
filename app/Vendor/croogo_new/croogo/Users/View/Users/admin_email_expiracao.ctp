<?php

$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Users'), '/admin/users')
	->addCrumb(__d('croogo', 'Assinantes expirando'), '/admin/users/users/assinantes_expirar')
	->addCrumb(__d('croogo', 'Modelo de E-mail'), '/' . $this->request->url);


$this->set('title_for_layout', __d('croogo', 'Modelo de E-mail'));

$this->append('form-start', $this->Form->create('Setting', array('class' => 'protected-form',)));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Model de E-mail'), '#email-template');
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('email-template') .
		$this->Form->input('id') .
		$this->Form->input('value', array(
			'label' => __d('croogo', 'Body'),
			'type' => 'textarea'
		));
	echo $this->Html->tabEnd();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(
			__d('croogo', 'Cancel'), array('action' => 'index'),
			array('button' => 'danger')
		);

	echo $this->Html->endBox();
$this->end();

$this->append('form-end', $this->Form->end());
