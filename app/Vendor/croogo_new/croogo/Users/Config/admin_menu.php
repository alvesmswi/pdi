<?php

CroogoNav::add('sidebar', 'users', array(
	'icon' => 'user',
	'title' => __d('croogo', 'Users'),
	'url' => array(
		'admin' => true,
		'plugin' => 'users',
		'controller' => 'users',
		'action' => 'index',
	),
	'weight' => 50,
	'children' => array(
		'users' => array(
			'title' => __d('croogo', 'Users'),
			'url' => array(
				'admin' => true,
				'plugin' => 'users',
				'controller' => 'users',
				'action' => 'index',
			),
			'weight' => 10,
		),
		'roles' => array(
			'title' => __d('croogo', 'Roles'),
			'url' => array(
				'admin' => true,
				'plugin' => 'users',
				'controller' => 'roles',
				'action' => 'index',
			),
			'weight' => 20,
		),
		'users_multiple' => array(
			'title' => __d('croogo', 'Usuários em Massa'),
			'url' => array(
				'admin' => true,
				'plugin' => 'users',
				'controller' => 'users',
				'action' => 'multiple',
			),
			'weight' => 20
		),
		'users_expire' => array(
			'title' => __d('croogo', 'Assinantes expirando'),
			'url' => array(
				'admin' => true,
				'plugin' => 'users',
				'controller' => 'users',
				'action' => 'assinantes_expirar',
			),
			'weight' => 20,
			'children' => array(
				'users_expire_email_template' => array(
					'title' => __d('croogo', 'Modelo de E-mail'),
					'url' => array(
						'admin' => true,
						'plugin' => 'users',
						'controller' => 'users',
						'action' => 'email_expiracao',
					),
					'weight' => 10
				)
			)
		),
	),
));
