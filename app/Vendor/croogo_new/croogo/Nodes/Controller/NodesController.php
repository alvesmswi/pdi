<?php

App::uses('NodesAppController', 'Nodes.Controller');
App::uses('Croogo', 'Lib');

/**
 * Nodes Controller
 *
 * @category Nodes.Controller
 * @package  Croogo.Nodes
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class NodesController extends NodesAppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Nodes';

/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Cookie',
		'Croogo.BulkProcess',
		'Croogo.Recaptcha',
		'Search.Prg' => array(
			'presetForm' => array(
				'paramType' => 'querystring',
			),
			'commonProcess' => array(
				'paramType' => 'querystring',
				'filterEmpty' => true,
			),
		),
	);

/**
 * Preset Variable Search
 *
 * @var array
 * @access public
 */
	public $presetVars = true;
	public $usePaginationCache = true;

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array(
		'Nodes.Node',
	);

/**
 * afterConstruct
 */
	public function afterConstruct() {
		parent::afterConstruct();
		$this->_setupAclComponent();
	}

/**
 * beforeFilter
 *
 * @return void
 * @access public
 */
	public function beforeFilter() {
		parent::beforeFilter();

		if (isset($this->request->params['slug'])) {
			$this->request->params['named']['slug'] = $this->request->params['slug'];
		}
		if (isset($this->request->params['type'])) {
			$this->request->params['named']['type'] = $this->request->params['type'];
		}
		$this->Security->unlockedActions[] = 'admin_toggle';
		$this->Security->unlockedActions[] = 'admin_edit';
		$this->Security->unlockedActions[] = 'admin_add';
		$this->Security->unlockedActions[] = 'editorias_ajax';

		//configura o Cookie
		//$this->Cookie->path = TMP;
		$this->Cookie->httpOnly = true;
		$this->Cookie->type('aes');

		$this->Auth->allow(
			'flush',
			'midiaindoor', 
			'megamenu',
			'editorias',
			'editorias_ajax',
			'tipo',
			'tipo_ajax'
		);
	}

/**
 * Toggle Node status
 *
 * @param string $id Node id
 * @param integer $status Current Node status
 * @return void
 */
	public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}

/**
 * Admin index
 *
 * @return void
 * @access public
 */
	public function admin_index() {
		$this->set('title_for_layout', __d('croogo', 'Content'));
		$this->Prg->commonProcess();

		$Node = $this->{$this->modelClass};
		$Node->recursive = -1;

		$alias = $this->modelClass;
		$this->paginate[$alias]['order'] = 'Node.id DESC';
		$this->paginate[$alias]['limit'] = 50;
		$this->paginate[$alias]['conditions'] = array();
		//$this->paginate[$alias]['contain'] = array('User');
		$this->paginate[$alias]['joins'] = array(
			array('table' => 'model_taxonomies',
				'alias' => 'Modtax',
				'type' => 'LEFT',
				'conditions' => array(
					'Modtax.foreign_key = Node.id',
					'Modtax.model = "Node"',
				)
			),
			array('table' => 'taxonomies',
				'alias' => 'Tax',
				'type' => 'LEFT',
				'conditions' => array(
					'Tax.id = Modtax.taxonomy_id'
				)
			),
			array('table' => 'terms',
				'alias' => 'Term',
				'type' => 'LEFT',
				'conditions' => array(
					'Term.id = Tax.term_id'
				)
			),
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'LEFT',
				'conditions' => array(
					'User.id = Node.user_id'
				)
			)
		);
		$this->paginate[$alias]['fields'] = array(
			'DISTINCT Node.id',
			'Node.title',
			'Node.type',
			'Node.lida',
			'Node.status',
			'Node.promote',
			'Node.ordem',
			'Node.slug',
			'Node.publish_start',
			'Node.onesignal_push',
			'Node.exclusivo',
			'User.username',
			'Term.id',
			'Term.title'
		);
		$this->Prg->parsedParams();

		$criteria = $Node->parseCriteria($this->Prg->parsedParams());

		if (isset($this->request->query['term_id']) && !empty($this->request->query['term_id'])) {
			//adiciona ao filtro
			$criteria['Term.id'] = $this->request->query['term_id'];
			//
			$this->request->data['Node']['term_id'] = $this->request->query['term_id'];
		}

		//Se não tem filtros ativos
		if(empty($criteria)){
			//se tem conteúdo padrao
			if(Configure::read('Site.conteudo_padrao')){
				$this->paginate[$alias]['conditions']['Node.type'] = Configure::read('Site.conteudo_padrao');
			}else{
				$this->paginate[$alias]['conditions']['Node.type'] = 'noticia';
			}
		}

		//se foi solicitado apenas as materias da capa
		if(isset($this->params->named['capa'])){
			$this->paginate[$alias]['conditions']['Node.promote'] = 1;
			$this->paginate[$alias]['order'] = 'Node.ordem ASC, Node.publish_start ASC';
		}

		//Seta se vai vincular o Multiattach
		$this->Node->vincularMultiattach = 0; //zero para não trazer nada
		
		$nodes = $this->paginate($criteria);

		$nodeTypes = $Node->Taxonomy->Vocabulary->Type->find('list', array(
			'fields' => array('Type.alias', 'Type.title')
			));
		$this->set(compact('nodes', 'types', 'typeAliases', 'nodeTypes'));

		if (isset($this->request->params['named']['links']) || isset($this->request->query['chooser'])) {
			$this->layout = 'admin_popup';
			$this->render('admin_chooser');
		}
	}

	function admin_index_ajax(){
		
	}

/**
 * Display node hierarchy scoped on Content type
 *
 * @return void
 */
	public function admin_hierarchy() {
		$this->Prg->commonProcess();

		if (empty($this->request->query['type'])) {
			$this->Session->setFlash(__d('croogo', 'Type must be specified'), 'flash', array(
				'class' => 'error',
			));
			return $this->redirect(array('action' => 'index'));
		}

		$Node = $this->{$this->modelClass};
		$Node->recursive = 0;

		$alias = $this->modelClass;
		$conditions = array();

		$type = $Node->Taxonomy->Vocabulary->Type->find('first', array(
			'recursive' => -1,
			'conditions' => array(
				'alias' => $this->request->query('type'),
			),
		));
		$types = $Node->Taxonomy->Vocabulary->Type->find('all');
		$typeAliases = Hash::extract($types, '{n}.Type.alias');

		$criteria = $Node->parseCriteria($this->Prg->parsedParams());
		$nodeTypes = $Node->Taxonomy->Vocabulary->Type->find('list', array(
			'fields' => array('Type.alias', 'Type.title')
		));

		$nodesTree = $this->Node->generateTreeList($criteria);
		$nodes = array();
		foreach ($nodesTree as $nodeId => $title) {
			$node = $Node->find('first', array(
				'conditions' => array(
					$Node->escapeField('id') => $nodeId,
				),
			));
			if ($node) {
				$depth = substr_count($title, '_', 0);
				$node['Node']['depth'] = $depth;
				$nodes[] = $node;
			}
		}
		$this->set(compact('nodes', 'type', 'types', 'typeAliases', 'nodeTypes'));
	}

/**
 * Move a node up when scoped to Content type
 *
 * @param integer $id Node id
 * @param integer $step Step
 * @return void
 */
	public function admin_moveup($id, $step = 1) {
		if ($this->Node->moveUp($id, $step)) {
			$this->Session->setFlash(__d('croogo', 'Moved up successfully'), 'flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__d('croogo', 'Could not move up'), 'flash', array('class' => 'error'));
		}
		return $this->redirect($this->referer());
	}

/**
 * Move a node down when scoped to Content type
 *
 * @param integer $id Node id
 * @param integer $step Step
 */
	public function admin_movedown($id, $step = 1) {
		if ($this->Node->moveDown($id, $step)) {
			$this->Session->setFlash(__d('croogo', 'Moved down successfully'), 'flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__d('croogo', 'Could not move down'), 'flash', array('class' => 'error'));
		}
		return $this->redirect($this->referer());
	}

/**
 * Admin create
 *
 * @return void
 * @access public
 */
	public function admin_create() {
		$this->set('title_for_layout', __d('croogo', 'Create content'));

		$types = $this->{$this->modelClass}->Taxonomy->Vocabulary->Type->find('all', array(
			'order' => array(
				'Type.alias' => 'ASC',
			),
		));
		$this->set(compact('types'));
	}

/**
 * Admin add
 *
 * @param string $typeAlias
 * @return void
 * @access public
 */
	public function admin_add($typeAlias = 'node') {
		$Node = $this->{$this->modelClass};
		$type = $Node->Taxonomy->Vocabulary->Type->findByAlias($typeAlias);
		if (!isset($type['Type']['alias'])) {
			$this->Session->setFlash(__d('croogo', 'Content type does not exist.'));
			return $this->redirect(array('action' => 'create'));
		}

		//Se postou alguma coisa
		if (!empty($this->request->data)) {

			if (isset($this->request->data[$Node->alias]['type'])) {
				$typeAlias = $this->request->data[$Node->alias]['type'];
				$Node->type = $typeAlias;
			}

			###TRATA AS DATAS
			//se foi informada a publish_start
			if(!empty($this->request->data['Node']['publish_start'])){
				$publish_start = str_replace('/','-', $this->request->data['Node']['publish_start']);
				$this->request->data['Node']['publish_start'] = date('Y-m-d H:i:s', strtotime($publish_start));
			}else{
				$this->request->data['Node']['creapublish_startted'] = date('Y-m-d H:i:s');
			}
			//se foi informada a publish_end
			if(!empty($this->request->data['Node']['publish_end'])){
				$publish_end = str_replace('/','-', $this->request->data['Node']['publish_end']);
				$this->request->data['Node']['publish_end'] = date('Y-m-d H:i:s', strtotime($publish_end));
			}
			//se foi informada a created
			if(!empty($this->request->data['Node']['created'])){
				$created = str_replace('/','-', $this->request->data['Node']['created']);
				$this->request->data['Node']['created'] = date('Y-m-d H:i:s', strtotime($created));
			}else{
				$this->request->data['Node']['created'] = date('Y-m-d H:i:s');
			}

			//se salvar corretamente
			if ($Node->saveNode($this->request->data, $typeAlias)) {

				//verifica tem algum anexo com ID temporário
				if(isset($this->request->data['Multiattach']['node_id_temp']) && !empty($this->request->data['Multiattach']['node_id_temp'])){
					$this->loadModel('Multiattach.Multiattach');
					$anexosTemporarios = $this->Multiattach->find('first',array(
						'fields' => array(
							'Multiattach.node_id'
						),
						'conditions' => array(
							'Multiattach.node_id' => $this->request->data['Multiattach']['node_id_temp']
						),
						'recursive' => -1
					));
					//se encontrou algum arquivo temporario
					if($anexosTemporarios){
						$this->Multiattach->updateAll(
							array(
								'Multiattach.node_id' => $this->Node->getLastInsertId()
							),
							array(
								'Multiattach.node_id' => $this->request->data['Multiattach']['node_id_temp']
							)
						);
					}
				}

				Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $this->request->data));
				$this->Session->setFlash(__d('croogo', '%s has been saved', $type['Type']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $Node->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', $type['Type']['title']), 'flash', array('class' => 'error'));
			}
		} else {
			$this->Croogo->setReferer();
			$this->request->data[$Node->alias]['user_id'] = $this->Session->read('Auth.User.id');
		}

		//se o plugin de Blog está ativo
		if(CakePlugin::loaded('Blog') && isset($type['Params']['blog']) && $type['Params']['blog']){
			$this->loadModel('Blog.Blog');
			$this->set('blogs_lista', $this->Blog->lista());
		}

		$this->set('title_for_layout', __d('croogo', 'Create content: %s', $type['Type']['title']));
		$Node->type = $type['Type']['alias'];
		$Node->Behaviors->attach('Tree', array(
			'scope' => array(
				$Node->escapeField('type') => $Node->type,
			),
		));

		$this->_setCommonVariables($type);
	}

/**
 * Admin edit
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		//verifica se está arquivado
		$this->Node->arquivadoCheck($id);

		$Node = $this->{$this->modelClass};
		$Node->id = $id;
		$typeAlias = $Node->field('type');

		$type = $Node->Taxonomy->Vocabulary->Type->findByAlias($typeAlias);

		if (!empty($this->request->data)) {

			###TRATA AS DATAS
			//se foi informada a publish_start
			if(!empty($this->request->data['Node']['publish_start'])){
				$publish_start = str_replace('/','-', $this->request->data['Node']['publish_start']);
				$this->request->data['Node']['publish_start'] = date('Y-m-d H:i:s', strtotime($publish_start));
			}else{
				$this->request->data['Node']['creapublish_startted'] = date('Y-m-d H:i:s');
			}
			//se foi informada a publish_end
			if(!empty($this->request->data['Node']['publish_end'])){
				$publish_end = str_replace('/','-', $this->request->data['Node']['publish_end']);
				$this->request->data['Node']['publish_end'] = date('Y-m-d H:i:s', strtotime($publish_end));
			}
			//se foi informada a created
			if(!empty($this->request->data['Node']['created'])){
				$created = str_replace('/','-', $this->request->data['Node']['created']);
				$this->request->data['Node']['created'] = date('Y-m-d H:i:s', strtotime($created));
			}else{
				$this->request->data['Node']['created'] = date('Y-m-d H:i:s');
			}

			//Se salvar corretamente
			if ($Node->saveNode($this->request->data, $typeAlias)) {
				$this->request->data['Node']['type'] = $typeAlias;
				$data = $this->request->data;
				Croogo::dispatchEvent('Controller.Nodes.afterEdit', $this, compact('data'));
				$this->Session->setFlash(__d('croogo', '%s has been saved', $type['Type']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $Node->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', $type['Type']['title']), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$data = $Node->read(null, $id);
			
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}

			//se o plugin de editorias está ativo
			if(CakePlugin::loaded('Editorias') && !empty($data['Editorias'])){
				$editorias = $data['Editorias'];
				unset($data['Editorias']);
				foreach($editorias as $editoria){
					$data['Editoria']['Editorias'][] = $editoria['id'];
				}
			}
			$data['Role']['Role'] = $Node->decodeData($data[$Node->alias]['visibility_roles']);
			$this->request->data = $data;
		}

		//se o plugin de Blog está ativo
		if(CakePlugin::loaded('Blog') && isset($type['Params']['blog']) && $type['Params']['blog']){
			$this->loadModel('Blog.Blog');
			$this->set('blogs_lista', $this->Blog->lista());
		}

		$this->set('title_for_layout', __d('croogo', 'Edit %s: %s', $type['Type']['title'], $this->request->data[$Node->alias]['title']));
		$this->_setCommonVariables($type);
	}

/**
 * Admin update paths
 *
 * @return void
 * @access public
 */
	public function admin_update_paths() {
		$Node = $this->{$this->modelClass};
		if ($Node->updateAllNodesPaths()) {
			$messageFlash = __d('croogo', 'Paths updated.');
			$class = 'success';
		} else {
			$messageFlash = __d('croogo', 'Something went wrong while updating paths.' . "\n" . 'Please try again');
			$class = 'error';
		}

		$this->Session->setFlash($messageFlash, 'flash', compact('class'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * Admin delete
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('croogo', 'Invalid id for Node'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		$Node = $this->{$this->modelClass};
		if ($Node->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Node deleted'), 'flash', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * Admin delete meta
 *
 * @param integer $id
 * @return void
 * @access public
 * @deprecated Use MetaController::admin_delete_meta()
 */
	public function admin_delete_meta($id = null) {
		$success = false;
		$Node = $this->{$this->modelClass};
		if ($id != null && $Node->Meta->delete($id)) {
			$success = true;
		} else {
			if (!$Node->Meta->exists($id)) {
				$success = true;
			}
		}

		$success = array('success' => $success);
		$this->set(compact('success'));
		$this->set('_serialize', 'success');
	}

/**
 * Admin add meta
 *
 * @return void
 * @access public
 * @deprecated Use MetaController::admin_add_meta()
 */
	public function admin_add_meta() {
		$this->layout = 'ajax';
	}

/**
 * Admin process
 *
 * @return void
 * @access public
 */
	public function admin_process() {
		$Node = $this->{$this->modelClass};
		list($action, $ids) = $this->BulkProcess->getRequestVars($Node->alias);

		$displayName = Inflector::pluralize(Inflector::humanize($Node->alias));
		$options = array(
			'redirect' => $this->referer(),
			'multiple' => array('copy' => false),
			'messageMap' => array(
				'delete' => __d('croogo', '%s deleted', $displayName),
				'publish' => __d('croogo', '%s published', $displayName),
				'unpublish' => __d('croogo', '%s unpublished', $displayName),
				'promote' => __d('croogo', '%s promoted', $displayName),
				'unpromote' => __d('croogo', '%s unpromoted', $displayName),
				'copy' => __d('croogo', '%s copied', $displayName),
			),
		);
		return $this->BulkProcess->process($Node, $action, $ids, $options);
	}

/**
 * Index
 *
 * @return void
 * @access public
 */
	public function index() {
		$Node = $this->{$this->modelClass};

		if (!isset($this->request->params['named']['type'])) {
			$this->request->params['named']['type'] = 'node';
		}

		//se é empresa
		if($this->request->params['named']['type']=='empresa'){
			//procura no cache
			$categoriaEmpresas = Cache::read('categoriaEmpresas', 'long_');
			//se nao encontrou
			if(!$categoriaEmpresas){
				//procura os termos de empresa
				$arrayCategorias = $Node->Taxonomy->find(
					'all',
					array(
						'fields' => array(
							'Term.id',
							'Term.title',
							'Term.slug'
						),
						'order' => array('Term.title'),
						'conditions' => array(
							'Vocabulary.id' => 13 //Empresa
						)
					)
				);
				$categoriaEmpresas = array();
				foreach($arrayCategorias as $categoria){
					$categoriaEmpresas[$categoria['Term']['id'].'|'.$categoria['Term']['slug']] = $categoria['Term']['title'];
				}
				Cache::write('categoriaEmpresas', $categoriaEmpresas, 'long_');
			}
			$this->set('categoriaEmpresas',$categoriaEmpresas);

			//se foi informado o categoria ou um temro
			if(isset($this->request->query['categoria']) || isset($this->request->query['termo'])){
				$this->usePaginationCache = false;//desliga o cache momentaneamente
				if(isset($this->request->query['categoria']) && !empty($this->request->query['categoria'])){
					$arrayTermo = explode('|',$this->request->query['categoria']);
					$termo = '{"'.$arrayTermo[0].'":"'.$arrayTermo[1].'"}';
					$this->paginate[$Node->alias]['conditions']['Node.terms'] = $termo;
				}
				if(isset($this->request->query['termo'])){
					$this->paginate[$Node->alias]['conditions']['EmpresaDetail.keywords LIKE'] = "%".$this->request->query['termo']."%";
				}
			}
		}

		$this->paginate[$Node->alias]['order'] = $Node->escapeField('publish_start') . ' DESC';
		$this->paginate[$Node->alias]['conditions']['Node.status'] = 1;

		//se foi informado o ano e o mes
		if(isset($this->request->query['ano']) || isset($this->request->query['mes'])){
			$this->usePaginationCache = false;//desliga o cache momentaneamente
			if(isset($this->request->query['mes'])){
				$this->paginate[$Node->alias]['conditions']['MONTH(Node.publish_start)'] = $this->request->query['mes'];
			}
			if(isset($this->request->query['ano'])){
				$this->paginate[$Node->alias]['conditions']['YEAR(Node.publish_start)'] = $this->request->query['ano'];
			}
		}else{
			$this->paginate[$Node->alias]['conditions']['AND'][$Node->escapeField('publish_start').' <='] = date('Y-m-d H:i:s');
		}
		//se foi informado o UF
		if(isset($this->request->query['uf']) && !empty($this->request->query['uf'])){
			$this->usePaginationCache = false;//desliga o cache momentaneamente
			$this->paginate[$Node->alias]['conditions']['EditalDetail.uf'] = $this->request->query['uf'];
		}
		//se foi informado a cidade
		if(isset($this->request->query['cidade']) && !empty($this->request->query['cidade'])){
			$this->usePaginationCache = false;//desliga o cache momentaneamente
			$this->paginate[$Node->alias]['conditions']['EditalDetail.cidade LIKE'] = "%".$this->request->query['cidade']."%";
		}
		//Condições padrão para não trazer nodes encerrados
		$this->paginate[$Node->alias]['conditions']['OR'][$Node->escapeField('publish_end').' >='] 	= date('Y-m-d H:i:s');
		$this->paginate[$Node->alias]['conditions']['OR'][$Node->escapeField('publish_end')] 		= null;

		//se foi informado o limit
		if (isset($this->request->params['named']['limit'])) {
			$limit = $this->request->params['named']['limit'];
		} else {
			$limit = Configure::read('Reading.nodes_per_page');
		}

		//Inclui somente os campos necessários
		$this->paginate[$Node->alias]['fields'] = array(
			'DISTINCT Node.id',
			'Node.title',
			'Node.excerpt',
			'Node.type',
			'Node.slug',
			'Node.terms',
			'Node.path',
			'Node.publish_start',
			'Node.exclusivo',
			'Node.promote'
		);

		//prepara o contain
		$this->paginate[$Node->alias]['contain'] = array(
			//'Meta',
			'Taxonomy' => array(
				'fields' => array(
					'id'
				),
				'Term' => array(
					'fields' => array(
						'id',
						'title',
						'slug'
					)
				),
				'Vocabulary' => array(
					'fields' => array(
						'id',
						'title',
						'alias'
					)
				)
			),
			'NoticiumDetail' => array(
				'fields' => array(
					'id',
					'jornalista',
					'chapeu',
					'titulo_capa',
					'keywords'
				)
			),
			'User' => array(
				'fields' => array(
					'id',
					'username'
				)
			)
		);

		//Seta se vai vincular o Multiattach
		$this->Node->vincularMultiattach = 1; //zero para não trazer nada

		if (isset($this->request->params['named']['type'])) {
			$type = $Node->Taxonomy->Vocabulary->Type->find('first', array(
				'conditions' => array(
					'Type.alias' => $this->request->params['named']['type'],
				),
				'cache' => array(
					'name' => 'type_' . $this->request->params['named']['type'],
					'config' => 'nodes_index',
				),
			));
			if (!isset($type['Type']['id'])) {
				$this->Session->setFlash(__d('croogo', 'Invalid content type.'), 'flash', array('class' => 'error'));
				return $this->redirect('/');
			}
			if (isset($type['Params']['nodes_per_page']) && empty($this->request->params['named']['limit'])) {
				$limit = $type['Params']['nodes_per_page'];
			}
			$this->paginate[$Node->alias]['conditions']['Node.type'] = $type['Type']['alias'];
			$this->set('title_for_layout', $type['Type']['title']);
		}

		$this->paginate[$Node->alias]['limit'] = $limit;

		$this->paginate[$Node->alias]['order'] = 'Node.publish_start DESC';

		$cacheDisable = Configure::read('Cache.disable');
		if (!$cacheDisable) {
			$cacheNamePrefix = 'nodes_index_' . $this->Croogo->roleId() . '_' . Configure::read('Config.language');
			if (isset($type)) {
				$cacheNamePrefix .= '_' . $type['Type']['alias'];
			}
			$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
			$cacheName = $cacheNamePrefix . '_' . $this->request->params['named']['type'] . '_' . $this->paginate['page'] . '_' . $limit;
			$cacheNamePaging = $cacheNamePrefix . '_' . $this->request->params['named']['type'] . '_' . $this->paginate['page'] . '_' . $limit . '_paging';
			$cacheConfig = 'nodes_index';
			$nodes = Cache::read($cacheName, $cacheConfig);
			if (!$nodes) {
				$nodes = $this->paginate($Node->alias);
				Cache::write($cacheName, $nodes, $cacheConfig);
				Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
			} else {
				$paging = Cache::read($cacheNamePaging, $cacheConfig);
				$this->request->params['paging'] = $paging;
			}
		} else {
			$nodes = $this->paginate($Node->alias);
		}

		//se pode fazer download
		if(isset($type['Params']['download']) && $type['Params']['download']){
			//informa a view
			$this->set('download', true);
		}

		$this->set(compact('type', 'nodes'));
		$this->Croogo->viewFallback(array(
			'index_' . $type['Type']['alias'],
		));

		//$this->set('nodeArquivos', $this->_arquivo($type['Type']['alias']));
	}

/**
 * Term
 *
 * @return void
 * @access public
 */
	public function term() {
		$Node = $this->{$this->modelClass};

		$slug = $this->request->params['named']['slug'];
		$query = "
		SELECT
		Term.id, Term.title, Term.slug, Filho.id, Filho.title, Filho.slug
		FROM terms AS Term
		LEFT JOIN taxonomies AS Tax ON(Tax.term_id = Term.id)
		LEFT JOIN taxonomies AS Tax2 ON(Tax2.parent_id = Tax.id)
		LEFT JOIN terms AS Filho ON(Filho.id = Tax2.term_id)
		WHERE Term.slug = '$slug';
		";

		$cacheConfig = 'nodes_term';
		$cacheName = 'nodes_term_'.$slug;
		$terms = Cache::read($cacheName, $cacheConfig);
		if (!$terms) {
			$terms = $Node->query($query);
			Cache::write($cacheName, $terms, $cacheConfig);
		}

		if (empty($terms)) {
			$this->Session->setFlash(__d('croogo', 'Invalid Term.'), 'flash', array('class' => 'error'));
			return $this->redirect('/');
		}

		$arrayTermo = array();
		//percorre cada termo
		foreach($terms as $term){
			$arrayTermo[$term['Term']['id']] = '{"'.$term['Term']['id'].'":"'.$term['Term']['slug'].'"}';
			//se tem filhos
			if(!empty($term['Filho']['id'])){
				$arrayTermo[$term['Filho']['id']] = '{"'.$term['Filho']['id'].'":"'.$term['Filho']['slug'].'"}';
			}
		}

		if (!isset($this->request->params['named']['type'])) {
			$this->request->params['named']['type'] = 'node';
		}

		if (isset($this->request->params['named']['limit'])) {
			$limit = $this->request->params['named']['limit'];
		} else {
			$limit = Configure::read('Reading.nodes_per_page');
		}

		$this->paginate[$Node->alias]['order'] = 'Node.publish_start DESC';

		$this->paginate[$Node->alias]['conditions'] = array(
			'Node.status' => '1'
		);

		$this->paginate[$Node->alias]['conditions']['Node.publish_start <='] 	= date('Y-m-d H:i:s');
		$this->paginate[$Node->alias]['conditions']['OR']['Node.publish_end >='] 		= date('Y-m-d H:i:s');
		$this->paginate[$Node->alias]['conditions']['OR']['Node.publish_end'] 			= null;

		//Inclui somente os campos necessários
		$this->paginate[$Node->alias]['fields'] = array(
			'DISTINCT Node.id',
			'Node.title',
			'Node.excerpt',
			'Node.type',
			'Node.slug',
			'Node.terms',
			'Node.path',
			'Node.publish_start',
			'Node.exclusivo',
			'Node.promote'
		);

		//prepara o contain
		$this->paginate[$Node->alias]['contain'] = array(
			//'Meta',
			'Taxonomy' => array(
				'fields' => array(
					'id'
				),
				'Term' => array(
					'fields' => array(
						'id',
						'title',
						'slug'
					)
				),
				'Vocabulary' => array(
					'fields' => array(
						'id',
						'title',
						'alias'
					)
				)
			),
			'NoticiumDetail' => array(
				'fields' => array(
					'id',
					'jornalista',
					'chapeu',
					'titulo_capa',
					'keywords'
				)
			),
			'User' => array(
				'fields' => array(
					'id',
					'username'
				)
			)
		);

		//complemento do cache
		$cacheNamePagingExt = '';

		if (isset($this->request->params['named']['type'])) {

			//se é guia
			if($this->request->params['named']['type'] == 'guia_cidade'){
				$this->paginate[$Node->alias]['conditions']['Node.terms LIKE'] = '%"' . $this->request->params['named']['slug'] . '"%';
			}else{
				$this->paginate[$Node->alias]['conditions']['Node.terms'] = $arrayTermo;
			}
			//Se é do tipo guia
			if(($this->request->params['named']['type'] == 'guia') && CakePlugin::loaded('Guia')){

				//se foi informado cidade
				if(isset($this->params->query['cidade']) && !empty($this->params->query['cidade'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.preg_replace('/[^a-z0-9]/i', '', strtolower($this->params->query['cidade']));
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.cidade'] = $this->params->query['cidade'];
				}
				//se foi informado bairro
				if(isset($this->params->query['bairro']) && !empty($this->params->query['bairro'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.preg_replace('/[^a-z0-9]/i', '', strtolower($this->params->query['bairro']));
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.bairro'] = $this->params->query['bairro'];
				}
				//se foi informado preco
				if(isset($this->params->query['preco_medio']) && !empty($this->params->query['preco_medio'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['preco_medio'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.preco_medio'] = $this->params->query['preco_medio'];
				}
				//se foi informado balada genero
				if(isset($this->params->query['balada_genero']) && !empty($this->params->query['balada_genero'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['balada_genero'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.balada_genero'] = $this->params->query['balada_genero'];
				}
				//se foi informado cozinha
				if(isset($this->params->query['cozinhaId']) && !empty($this->params->query['cozinhaId'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['cozinhaId'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.'.$this->params->query['cozinhaId']] = 1;
				}
				//se foi informado opcional
				if(isset($this->params->query['opcionalId']) && !empty($this->params->query['opcionalId'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['opcionalId'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.'.$this->params->query['opcionalId']] = 1;
				}
				//se foi informado pagamento
				if(isset($this->params->query['pagamentoId']) && !empty($this->params->query['pagamentoId'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['pagamentoId'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.'.$this->params->query['pagamentoId']] = 1;
				}
				//se foi informado dias
				if(isset($this->params->query['diaId']) && !empty($this->params->query['diaId'])){
					//nome do cache
					$cacheNamePagingExt .= '_'.$this->params->query['diaId'];
					//adiciona na condição
					$this->paginate[$Node->alias]['conditions']['AND']['Guia.'.$this->params->query['diaId']] = 1;
				}
				//contain
				$this->paginate[$Node->alias]['contain'][] = 'Guia';

			}

			$type = $Node->Taxonomy->Vocabulary->Type->find('first', array(
				'conditions' => array(
					'Type.alias' => $this->request->params['named']['type'],
				),
				'cache' => array(
					'name' => 'type_' . $this->request->params['named']['type'],
					'config' => 'nodes_term',
				),
			));
			if (!isset($type['Type']['id'])) {
				$this->Session->setFlash(__d('croogo', 'Invalid content type.'), 'flash', array('class' => 'error'));
				return $this->redirect('/');
			}
			if (isset($type['Params']['nodes_per_page']) && empty($this->request->params['named']['limit'])) {
				$limit = $type['Params']['nodes_per_page'];
			}
			$this->paginate[$Node->alias]['conditions'][$Node->escapeField('type')] = $type['Type']['alias'];
			$this->set('title_for_layout', $term['Term']['title']);

			// Para o novo Tema de Impressos
			if ($this->request->params['named']['type'] == 'impresso' && CakePlugin::loaded('PageFlip')) {
				$this->paginate[$Node->alias]['contain'] = array(
					'Taxonomy' => array(
						'Term',
						'Vocabulary',
					),
					'ImpressoDetail',
					'PageFlip'
				);
			}
		}

		$this->paginate[$Node->alias]['limit'] = $limit;

		//Seta se vai vincular o Multiattach
		$this->Node->vincularMultiattach = 1; //zero para não trazer nada

		$cacheDisable = Configure::read('Cache.disable');
		if (!$cacheDisable) {
			$cacheNamePrefix = 'nodes_term_' . $this->Croogo->roleId() . '_' . $this->request->params['named']['slug'] . '_' . Configure::read('Config.language');
			if (isset($type)) {
				$cacheNamePrefix .= '_' . $type['Type']['alias'];
			}
			$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
			$cacheName = $cacheNamePrefix . '_' . $this->paginate['page'] . '_' . $limit;
			$cacheNamePaging = $cacheNamePrefix . '_' . $this->paginate['page'] . '_' . $limit . '_paging'.$cacheNamePagingExt;
			$cacheConfig = 'nodes_term';
			$nodes = Cache::read($cacheName, $cacheConfig);
			if (!$nodes) {
				$nodes = $this->paginate($Node->alias);
				Cache::write($cacheName, $nodes, $cacheConfig);
				Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
			} else {
				$paging = Cache::read($cacheNamePaging, $cacheConfig);
				$this->request->params['paging'] = $paging;
			}
		} else {
			$nodes = $this->paginate($Node->alias);
		}
		$this->set(compact('term', 'type', 'nodes'));
		$this->Croogo->viewFallback(array(
			'term_' . $term['Term']['id'],
			'term_' . $term['Term']['slug'],
			'term_' . $type['Type']['alias'] . '_' . $term['Term']['slug'],
			'term_' . $type['Type']['alias']
		));
	}

	public function midiaindoor() {
		$Node = $this->{$this->modelClass};
		$this->set('title_for_layout', __d('croogo', 'Home'));

		$roleId = $this->Croogo->roleId();
		$this->paginate[$Node->alias]['conditions'] = array(
			'Node.promote' => 1
		);

		if (isset($this->request->params['named']['limit'])) {
			$limit = $this->request->params['named']['limit'];
		} else {
			$limit = Configure::read('Reading.nodes_per_page');
		}

		if (isset($this->request->params['named']['type'])) {
			$type = $Node->Taxonomy->Vocabulary->Type->findByAlias($this->request->params['named']['type']);
			if (!isset($type['Type']['id'])) {
				$this->Session->setFlash(__d('croogo', 'Invalid content type.'), 'flash', array('class' => 'error'));
				return $this->redirect('/');
			}
			if (isset($type['Params']['nodes_per_page']) && empty($this->request->params['named']['limit'])) {
				$limit = $type['Params']['nodes_per_page'];
			}
			$this->paginate[$Node->alias]['conditions'][$Node->escapeField('type')] = $type['Type']['alias'];
			$this->set('title_for_layout', $type['Type']['title']);
			$this->set(compact('type'));
		}
		$this->paginate[$Node->alias]['limit'] = $limit;
		$this->paginate[$Node->alias]['order'] = 'publish_start DESC';
		$this->paginate[$Node->alias]['contain'] = array(
			'NoticiumDetail'
		);

		//Seta se vai vincular o Multiattach
		$this->Node->vincularMultiattach = 1; //zero para não trazer nada

		$cacheDisable = Configure::read('Cache.disable');
		if (!$cacheDisable) {
			$cacheNamePrefix = 'midiaindoor' . $this->Croogo->roleId() . '_' . Configure::read('Config.language');
			if (isset($type)) {
				$cacheNamePrefix .= '_' . $type['Type']['alias'];
			}
			$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
			$cacheName = $cacheNamePrefix . '_' . $this->paginate['page'] . '_' . $limit;
			$cacheNamePaging = $cacheNamePrefix . '_' . $this->paginate['page'] . '_' . $limit . '_paging';
			$cacheConfig = 'nodes_promoted';
			$nodes = Cache::read($cacheName, $cacheConfig);
			if (!$nodes) {
				$nodes = $this->paginate($Node->alias);
				Cache::write($cacheName, $nodes, $cacheConfig);
				Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
			} else {
				$paging = Cache::read($cacheNamePaging, $cacheConfig);
				$this->request->params['paging'] = $paging;
			}
		} else {
			$nodes = $this->paginate($Node->alias);
		}
		$this->set(compact('nodes'));
	}

/**
 * Search
 *
 * @param string $typeAlias
 * @return void
 * @access public
 */

	public function search() {
		if(isset($this->params->query['q'])){
			//trata a palavra
			$keyword = $this->params->query['q'];
		}

		$nodes = array();
		//se foi informado alguma coisa
		if(isset($this->params->query['q']) && !empty($this->params->query['q']) && !Configure::read('Site.busca_google')){

			$Node = $this->{$this->modelClass};
			$this->paginate[$Node->alias]['conditions']['Node.status'] 		= 1;
			$this->paginate[$Node->alias]['conditions']['Node.title LIKE '] = "%$keyword%";
			//Condições padrão para não trazer nodes encerrados
			$this->paginate[$Node->alias]['conditions']['AND']['publish_start <='] 	= date('Y-m-d H:i:s');
			$this->paginate[$Node->alias]['conditions']['OR']['publish_end >='] 	= date('Y-m-d H:i:s');
			$this->paginate[$Node->alias]['conditions']['OR']['publish_end'] 		= null;

			//se foi informado o limit
			if (isset($this->request->params['named']['limit'])) {
				$limit = $this->request->params['named']['limit'];
			} else {
				$limit = Configure::read('Reading.nodes_per_page');
			}
			$this->paginate[$Node->alias]['limit'] = $limit;

			//Inclui somente os campos necessários
			$this->paginate[$Node->alias]['fields'] = array(
				'DISTINCT Node.id',
				'Node.title',
				'Node.excerpt',
				'Node.type',
				'Node.slug',
				'Node.terms',
				'Node.path',
				'Node.publish_start',
				'Node.exclusivo',
				'Node.promote',
			);

			//prepara o contain
			$this->paginate[$Node->alias]['contain'] = array(
				//'Meta',
				'Taxonomy' => array(
					'fields' => array(
						'id'
					),
					'Term' => array(
						'fields' => array(
							'id',
							'title',
							'slug'
						)
					),
					'Vocabulary' => array(
						'fields' => array(
							'id',
							'title',
							'alias'
						)
					)
				),
				'NoticiumDetail' => array(
					'fields' => array(
						'id',
						'jornalista',
						'chapeu',
						'titulo_capa',
						'keywords'
					)
				),
				'User' => array(
					'fields' => array(
						'id',
						'username'
					)
				)
			);
			//Seta se vai vincular o Multiattach
			$this->Node->vincularMultiattach = 1; //zero para não trazer nada

			$this->paginate[$Node->alias]['order'] = 'Node.publish_start DESC';
			$this->paginate[$Node->alias]['group'] = 'Node.id';

			$cacheDisable = Configure::read('Cache.disable');
			if (!$cacheDisable) {
				$cacheNamePrefix = 'nodes_search_' . $keyword;
				$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
				$cacheName = $cacheNamePrefix . '_' . $keyword . '_' . $this->paginate['page'] . '_' . $limit;
				$cacheNamePaging = $cacheNamePrefix . '_' . $keyword . '_' . $this->paginate['page'] . '_' . $limit . '_paging';
				$cacheConfig = 'nodes_index';
				$nodes = Cache::read($cacheName, $cacheConfig);
				if (!$nodes) {
					$nodes = $this->paginate($Node->alias);
					Cache::write($cacheName, $nodes, $cacheConfig);
					Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
				} else {
					$paging = Cache::read($cacheNamePaging, $cacheConfig);
					$this->request->params['paging'] = $paging;
				}
			} else {
				$nodes = $this->paginate($Node->alias);
			}
		}
		$this->set('title_for_layout', 'Pesquisa por: '.$keyword);
		$this->set(compact('nodes'));
	}

/**
 * View
 *
 * @param integer $id
 * @return void
 * @access public
 */
function _paywall($nodeId){
	//verifica se o plugin está ativo
	if(CakePlugin::loaded('Paywall')){
		//Pega o IP de quem está acessando
		$IP = $_SERVER['REMOTE_ADDR'];
		//pega os dados do usuario
		$user = $this->Auth->user();
		//se é anonimo
		if(empty($user)){
			$userId 		= 0;
			$userRole 		= 0;
			$maxNoticias 	= Configure::read('Paywall.PaywallAnonimo');
		}else if($user['Role']['alias'] == 'registered'){//Registrado
			$userId 		= $user['id'];
			$userRole 		= $user['role_id'];
			$maxNoticias 	= Configure::read('Paywall.PaywallCadastrado');
		}else if($user['Role']['alias'] == 'assinante'){//Assinante
			$userId 		= $user['id'];
			$userRole 		= $user['role_id'];
			$maxNoticias 	= Configure::read('Paywall.PaywallAnonimo');
		}else{
			//Se é Admin, Colunista, Editor ou Gerente, permite a visualização
			return true;
		}

		//se o máximo de notícia é ilimitado
		if($maxNoticias == '0'){
			return true;
		}

		//monta o padrão de chave
		$chave = $IP.'.'.$userRole.'.'.$userId;

		//verifica se o usuario está tentando ver a notícia que ele já viu
		if($this->Cookie->check('Paywall.'.$chave.'.'.$nodeId)){
			return true;
		}

		//conta as noticias já visitadas
		$cookieNoticias = $this->Cookie->read('Paywall.'.$chave);
		$totalLidas = count($cookieNoticias);

		//pega o cookie
		$cookie = $this->Cookie->read('Paywall');
		//pr($cookie);pr('$totalLidas '.$totalLidas);pr('$maxNoticias '.$maxNoticias);exit();
		//se já atingiu o limite
		if($totalLidas >= $maxNoticias){
			return false;
		}else{
			$paywallTempo = Configure::read('Paywall.PaywallTempo');
			$this->Cookie->write('Paywall.'.$chave.'.'.$nodeId, 1, false, $paywallTempo.' hours');
			return true;
		}
	//Plugin NÃO instalado
	}else{
		return true;
	}

}

	public function view($id = null, $ajax = false) {

		if($ajax){
			$this->layout = 'ajax';
		}

		$Node = $this->{$this->modelClass};

		if (isset($this->request->params['named']['slug']) && isset($this->request->params['named']['type'])) {

			//verifica se está arquivado
			$this->Node->arquivadoCheck(null, $this->request->params['named']['slug'], $this->request->params['named']['type']);

			$Node->type = $this->request->params['named']['type'];
			$type = $Node->Taxonomy->Vocabulary->Type->find('first', array(
				'conditions' => array(
					'Type.alias' => $Node->type,
				),
				'cache' => array(
					'name' => 'type_' . $Node->type,
					'config' => 'nodes_view',
				),
			));

			//Busca o node
			$node = $Node->find('viewBySlug', array(
				'slug' => $this->request->params['named']['slug'],
				'type' => $this->request->params['named']['type']
			));

		} elseif ($id == null) {

			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect('/');

		} else {

			//verifica se está arquivado
			$this->Node->arquivadoCheck($id);

			$Node->type = '';
			//Busca o node
			$node = $Node->find('viewById', array(
				'id' => $id
			));
			$Node->type = $node['Node']['type'];
			$type = $Node->Taxonomy->Vocabulary->Type->find('first', array(
				'conditions' => array(
					'Type.alias' => $Node->type,
				),
				'cache' => array(
					'name' => 'type_' . $Node->type,
					'config' => 'nodes_view',
				),
			));
		}
		if (!isset($node[$Node->alias][$Node->primaryKey])) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect('/');
		}
		//se este tipo de conteudo é para assinante
		if(isset($type['Params']['assinante']) && $type['Params']['assinante']){
			//verifica se este tipo de conteudo pode ser visto apenas por assinantes
			$this->verificaAssinantes($this->here, $node);
		}
		//se este node é exclusivo para assinante
		if(isset($node['Node']['exclusivo']) && ($node['Node']['exclusivo'])){
			//verifica se este tipo de conteudo pode ser visto apenas por assinantes
			$this->verificaAssinantes($this->here, $node);
		}
		//se é para exibir com page flip
		if(isset($type['Params']['pageflip']) && $type['Params']['pageflip']){
			//informa a view
			$this->set('pageflip', true);
		}
		//se pode fazer download
		if(isset($type['Params']['pageflip_download']) && $type['Params']['pageflip_download']){
			//informa a view
			$this->set('pageflip_download', true);
		}
		//se pode fazer download
		if(isset($type['Params']['download']) && $type['Params']['download']){
			//informa a view
			$this->set('download', true);
		}
		//se o contador está ativo
		if(isset($type['Params']['contador']) && $type['Params']['contador']){
			$contador = true;
		}
		//se o é para exibir os relacionados
		if(isset($type['Params']['relacionados']) && $type['Params']['relacionados']){
			$nodesRelacionados = $this->_nodesRelacionados($node['Node']['id'], $node['Node']['terms']);
			$this->set('nodesRelacionados',$nodesRelacionados);
		}
		//Se está ativo o contador
		if(isset($contador) && $contador){
			$this->_contador($node['Node']['id'], $node['Node']['lida']);
		}
		//se o plugin de correlatas está ativo
		if(CakePlugin::loaded('Correlatas')){
			$relacionadas = $this->Node->relacionadas($node['Node']['id']);
			$this->set('relacionadas',$relacionadas);
		}
		//se tem parametros
		if(isset($type['Params'])){
			//passa para a view
			$this->set('nodeParams', $type['Params']);
		}
		//se o é para exibir os veja tambem
		if(isset($type['Params']['vejatambem']) && $type['Params']['vejatambem'] && isset($node['Editorias']) && !empty($node['Editorias'])){
			$vejatambem = $this->_vejaTambem($node['Node']['id'], $node['Editorias']);
			$this->set('vejatambem',$vejatambem);
		}

		//verificar se não é para exibir o sidebar
		if(Configure::read('Site.node_id_sem_sidebar')){
			$node_id_sem_sidebar = explode(',',Configure::read('Site.node_id_sem_sidebar'));
			if(in_array($node['Node']['id'], $node_id_sem_sidebar)){
				$this->set('sidebar',0);
			}
		}

		//se é notícia
		if($node['Node']['type'] == 'noticia'){
			//verifica as permissões do Paywall
			if(!$this->_paywall($node['Node']['id'])){
				$paywallMensagem = Configure::read('Paywall.PaywallMensagem');
				if(!empty($paywallMensagem)){
					$this->Session->setFlash($paywallMensagem, 'flash', array('class' => 'alert alert-warning'));
				}
				$redirect = '/'.$node['Node']['type'].'/'.$node['Node']['slug'];
				return $this->redirect('/paywall/paywall/cadastro?redirect='.$redirect);
			}
		}

		$data = $node;
		$event = new CakeEvent('Controller.Nodes.view', $this, compact('data'));
		$this->getEventManager()->dispatch($event);

		$this->set('title_for_layout', $node[$Node->alias]['title']);
		$this->set(compact('node', 'type', 'comments'));

		//se a chamada foi por ajax
		if($ajax){
			$this->Croogo->viewFallback(array(
				'ajax_' . $type['Type']['alias']
			));
		}else{
			$this->Croogo->viewFallback(array(
				'view_' . $type['Type']['alias'] . '_' . $node[$Node->alias]['slug'],
				'view_' . $node[$Node->alias][$Node->primaryKey],
				'view_' . $type['Type']['alias'],
			));
		}
	}

	private function _contador($id, $lida = 0){
		//pega o ip e nome
		$ip = $this->request->clientIp();
		$name = $ip.$id;
		//verifica se NÃO existe este cookie
		if(!$this->Cookie->check($name)){			
			//atualiza o banco
			$lida++;
			$this->Node->query("UPDATE nodes SET lida = $lida WHERE id = $id;");
			//grava o cookie por 1 hora
			$this->Cookie->write($name, $name, false, 3600);	
		}
		return true;		
	}

	private function verificaAssinantes($urlRedirect='', $node = null){
		//pega o grupo do usuario logado
		$usuarioLogado = $this->Auth->user();
		$autorizado = false;
		//se está logado
		if(!empty($usuarioLogado)){

			//Se está logado admin, colunista ou editor
			if(
				$usuarioLogado['Role']['alias'] == 'admin' ||
				$usuarioLogado['Role']['alias'] == 'editor' ||
				$usuarioLogado['Role']['alias'] == 'colunista' ||
				$usuarioLogado['Role']['alias'] == 'gerente' ||
				$usuarioLogado['Role']['alias'] == 'cortesia' ||
				$usuarioLogado['Role']['alias'] == 'funcionario' ||
				$usuarioLogado['Role']['alias'] == 'assinante-direto'
			){
				$autorizado = true;
			//Se é usuário comum
			}else{
				//Se tem que pagar para ser assinante
				if(Configure::read('Paywall.PaywallPagamento')){
					//consulta no banco para ver se está pago
					$Node = $this->{$this->modelClass};
					$pagamento = $Node->query('SELECT status FROM pagseguros WHERE user_id = '.$this->Session->read('Auth.User.id'));
					//se encontrou e está pago
					if(
						isset($pagamento[0]['pagseguros']['status']) &&
						($pagamento[0]['pagseguros']['status'] == 3 || $pagamento[0]['pagseguros']['status'] == 4)
					){
						$autorizado = true;
					}else{
						$autorizado = false;
						return $this->redirect('/pagseguro/pagseguro/assinatura_pagamento');
					}
				//Se não precisa pagar
				}else{
					$autorizado = true;
				}
			}
		}

		$this->set('autorizado', $autorizado);

		//Se NÃO estiver autorizado
		if(!$autorizado){
			// For bloqueio parcial e não é um pageflip
			if (Configure::read('Paywall.PaywallBloqueioParcial') && !isset($node['PageFlip'])) {
				$user_agent = false;

				//se o userAgent estiver autorizado
				if(
					isset($_SERVER['HTTP_USER_AGENT']) &&
					strstr($_SERVER['HTTP_USER_AGENT'], 'facebook') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'Facebot') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'google') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'WhatsApp')
				){
					$user_agent = true;
				}

				$this->set('user_agent', $user_agent);
				return true;

			} else {
				//se o userAgent estiver autorizado
				if(
					isset($_SERVER['HTTP_USER_AGENT']) &&
					strstr($_SERVER['HTTP_USER_AGENT'], 'facebook') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'Facebot') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'google') ||
					strstr($_SERVER['HTTP_USER_AGENT'], 'WhatsApp')
				){
					return true;
				}else{
					//redireciona para a tela de login
					$this->Flash->warning('Você não está autorizado a acessar este conteúdo.');
					$urlBloqueio = '/users/users/login';
					if(Configure::read('Paywall.paginaBloqueio')){
						$urlBloqueio = Configure::read('Paywall.paginaBloqueio');
					}
					return $this->redirect($urlBloqueio.'?redirect='.$urlRedirect);
				}
			}
		}
	}

	function _vejaTambem($nodeId, $editorias = null){
		$nomeCache = 'veja_tamebem_'.$nodeId;
		$limit = (Configure::read('Node.relacionados')) ? Configure::read('Node.relacionados') : 6;
		$diasAtras = '2';
		//Se foi informado quantos dias atras que é para procurar nodes
		if(Configure::read('Site.blocos_home_dias_atras')){
			$diasAtras = Configure::read('Site.blocos_home_dias_atras');
		}
		$Node = $this->{$this->modelClass};

		if(!empty($editorias)){
			$where = '(';
			foreach($editorias as $key =>  $editoria){
				$slug = $editoria['slug'];
				//se tem mais de um
				if($key > 0){
					$where .= " OR ";
				}
				$where .= "Editoria.slug = '$slug'";
			}
			$where .= ')';
			$sql = "
			SELECT 
				Node.publish_start, Node.id, Node.title, Node.path, Node.ordem, Node.excerpt,
				Editoria.title, Editoria.slug, 
				Multiattach.filename, Multiattach.meta, Multiattach.mime,
				NoticiumDetail.chapeu, NoticiumDetail.titulo_capa 
			FROM nodes AS Node 
			LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
			LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
			LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
			LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
			WHERE 
				Node.publish_start <= '".date('Y-m-d H:i:s')."' AND 
				(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
				Node.type = 'noticia' AND 
				Node.status = '1' AND 
				$where
			GROUP BY Node.id 
			ORDER BY Node.publish_start DESC 
			LIMIT $limit;
			";

			$cacheDisable = Configure::read('Cache.disable');

			//Se o cache está ativo
			if (!$cacheDisable) {
				$cacheName = 'vejatambem_'.$nodeId;
				$cacheConfig = 'nodes_relacionados_';
				$nodes = Cache::read($cacheName, $cacheConfig);
				if (!$nodes) {
					$nodes = $Node->query($sql);
					Cache::write($cacheName, $nodes, $cacheConfig);
				}
			}else{
				$nodes = $Node->query($sql);
			}
			return $nodes;
		}		
	}

	function _nodesRelacionados($nodeId, $nodeTermo){
		//pr($this->theme);
		$diasAtras = '2';
		//Se foi informado quantos dias atras que é para procurar nodes
		if(Configure::read('Site.blocos_home_dias_atras')){
			$diasAtras = Configure::read('Site.blocos_home_dias_atras');
		}
		$Node = $this->{$this->modelClass};

		//Pega o nome do termo
		$termos = json_decode($nodeTermo);
		$termos = (array)$termos;
		$termo = 'sem_termo';
		foreach($termos as $t){
			$termo = $t;
		}

		

		$fields = 'Node.id, Node.title, Node.excerpt, Node.path, NoticiumDetail.chapeu, Multiattach.filename';

		if ($Node->NoticiumDetail->schema('link_externo')) {
			$fields .= ', NoticiumDetail.link_externo';
		}

		$limit_relacionados = (Configure::read('Node.relacionados')) ? Configure::read('Node.relacionados') : 6;

		$sql = "
			SELECT {$fields}
			FROM nodes AS Node
			LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
			LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
			WHERE
				Node.type = 'noticia' AND
				Node.terms = '$nodeTermo' AND
				Node.id <> '$nodeId' AND
				Node.publish_start <= '".date('Y-m-d H:i:s')."' AND
				Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND
				((Node.publish_end IS NULL) OR (Node.publish_end >= '".date('Y-m-d H:i:s')."')) AND
				Node.status = '1'
			GROUP BY Node.id
			ORDER BY Node.publish_start DESC
			LIMIT {$limit_relacionados};";

		$cacheDisable = Configure::read('Cache.disable');

		//Se o cache está ativo
		if (!$cacheDisable) {
			$cacheName = $termo.'_'.$nodeId;
			$cacheConfig = 'nodes_relacionados_';
			$nodes = Cache::read($cacheName, $cacheConfig);
			if (!$nodes) {
				$nodes = $Node->query($sql);
				Cache::write($cacheName, $nodes, $cacheConfig);
			}
		}else{
			$nodes = $Node->query($sql);
		}
		
		return $nodes;
	}

/**
 * View Fallback
 *
 * @param mixed $views
 * @return string
 * @access protected
 * @deprecated Use CroogoComponent::viewFallback()
 */
	protected function _viewFallback($views) {
		return $this->Croogo->viewFallback($views);
	}

/**
 * Set common form variables to views
 * @param array $type Type data
 * @return void
 */
	protected function _setCommonVariables($type) {
		if (isset($this->Taxonomies)) {
			$this->Taxonomies->prepareCommonData($type);
		}
		$Node = $this->{$this->modelClass};
		if (!empty($this->request->data[$Node->alias]['parent_id'])) {
			$Node->id = $this->request->data[$Node->alias]['parent_id'];
			$parentTitle = $Node->field('title');
		}
		$roles = $Node->User->Role->find('list');
		$this->set(compact('parentTitle', 'roles'));
	}

	public function flush($configCache = 'default'){
		Configure::write('debug',1);
		$this->autoRender = false;
		if(Cache::clear(false, $configCache)){
			pr('apagado o cache de '.$configCache);
		}

		//$this->loadModel('Link');
		//$this->Link->recover();
	}

	public function keywords($keyword='') {
		$nodes = array();
		$Node = $this->{$this->modelClass};
		//se foi informado alguma coisa
		if(isset($keyword) && !empty($keyword)){

			$diasAtras = '5';

			//trata a palavra
			$keyword = urldecode($keyword);
			
			//condições
			$arrayConditions = array();
			$arrayConditions['AND']['Node.status'] 						= 1;
			$arrayConditions['AND']['NoticiumDetail.keywords LIKE '] 	= "%$keyword%";
			$arrayConditions['AND']['Node.publish_start <='] 			= date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'));
			$arrayConditions['OR']['Node.publish_end >='] 				= date('Y-m-d H:i:s');
			$arrayConditions['OR']['Node.publish_end'] 					= null;

			$this->set('title_for_layout', $keyword);
			
			$cacheDisable = Configure::read('Cache.disable');
			if (!$cacheDisable) {
				$keyword = strtolower(Inflector::slug($keyword));
				$cacheName = 'nodes_keywords_' . $keyword . '_' . $limit;
				$cacheConfig = 'nodes_index';
				$nodes = Cache::read($cacheName, $cacheConfig);
				if (!$nodes) {
					$nodes = $Node->find(
						'all',
						array(
							'conditions' => $arrayConditions,
							'contain' => array(
								'Taxonomy' => array(
									'Term',
									'Vocabulary',
								),
								'NoticiumDetail'
							),
							'order' => 'Node.publish_start DESC',
							'limit' => 20
						)
					);
					Cache::write($cacheName, $nodes, $cacheConfig);
				}
			} else {
				$nodes = $Node->find(
					'all',
					array(
						'conditions' => $arrayConditions,
						'contain' => array(
							'Taxonomy' => array(
								'Term',
								'Vocabulary',
							),
							'NoticiumDetail'
						),
						'order' => 'Node.publish_start DESC',
						'limit' => 20
					)
				);
			}
		}
		
		$this->set(compact('nodes'));
		$this->Croogo->viewFallback(array(
			'index_keywords'
		));
	}

	public function megamenu($term = null)
	{
		if (empty($this->request->params['requested'])) {
            throw new ForbiddenException();
		}

		$this->autoRender = false;

		$Node = $this->{$this->modelClass};

		$diasAtras = '2';
		if (Configure::read('Site.blocos_home_dias_atras')) {
			$diasAtras = Configure::read('Site.blocos_home_dias_atras');
		}

		$fields = 'Node.id, Node.title, Node.path, NoticiumDetail.chapeu, Multiattach.filename';
		if ($Node->NoticiumDetail->schema('link_externo')) {
			$fields .= ', NoticiumDetail.link_externo';
		}

		if (!empty($term)) {
			$node_term = "Node.terms LIKE '%$term%' AND";
		} else {
			$term = 'sem_termo';
			$node_term = null;
		}

		$sql = "
			SELECT {$fields}
			FROM nodes AS Node
			LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
			LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
			WHERE
				Node.type = 'noticia' AND
				{$node_term}
				Node.publish_start <= '".date('Y-m-d H:i:s')."' AND
				Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND
				((Node.publish_end IS NULL) OR (Node.publish_end >= '".date('Y-m-d H:i:s')."')) AND
				Node.status = '1'
			GROUP BY Node.id
			ORDER BY Node.publish_start DESC
			LIMIT 6;";

		$cacheDisable = Configure::read('Cache.disable');

		if (!$cacheDisable) {
			$cacheName = $term.'_megamenu_'.$this->Croogo->roleId() . '_' . Configure::read('Config.language');
			$cacheConfig = 'nodes_megamenu_';
			$nodes = Cache::read($cacheName, $cacheConfig);
			if (!$nodes) {
				$nodes = $Node->query($sql);
				Cache::write($cacheName, $nodes, $cacheConfig);
			}
		}else{
			$nodes = $Node->query($sql);
		}

		return $nodes;
	}

	public function editorias(){
		//pega o slug da editoria
		$slug = $this->params['editoria'];

		//pega o nome da editoria
		$editoria = $this->Node->query("SELECT id, title, slug FROM editorias WHERE slug = '$slug';");

		//manda o titulo para a view
		$this->set('title', $editoria[0]['editorias']['title']);
		$this->set('slug', $slug);

		//prepara o nome da view
		$this->Croogo->viewFallback(array(
			'editoria_' . $slug
		));
	}

	public function editorias_ajax(){
		$slug = $this->params->query['slug'];
		$page = $this->params->query['page'];
		$limit = $this->params->query['limit'];

		$nomeCache = 'editorias_lista';

		$offset = (($page - 1) * $limit);
		$sql = "
		SELECT 
			Node.publish_start, Node.id, Node.title, Node.path, 
			Editoria.title, Editoria.slug, 
			Multiattach.filename, 
			NoticiumDetail.chapeu
		FROM nodes AS Node 
		LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
		LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
		LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
		LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
		WHERE 
			Node.publish_start <= '".date('Y-m-d H:i:s')."' AND 
			(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
			Editoria.slug = '$slug' AND 
			Node.status = '1' 
		ORDER BY Node.publish_start DESC 
		LIMIT $offset, $limit;
		";

		$cacheDisable = Configure::read('Cache.disable');
		//Se o cache está ativo
		if (!$cacheDisable) {
			$cacheName = 'editorias_'.$slug . '_' . $limit . '_' . $page;
			$cacheConfig = 'editorias';
			$nodes = Cache::read($cacheName, $cacheConfig);
			//se NÃO encontrar dados no cache, procura no banco novamente
			if (!$nodes) {
				$nodes = $this->Node->query($sql);
				//grava o resultado no cache
				Cache::write($cacheName, $nodes, $cacheConfig);
			}
		}else{
			$nodes = $this->Node->query($sql);
		}
		//envia os dados para view
		$this->set('nodes', $nodes);
		$this->layout = 'ajax';
		//prepara o nome da view
		$this->Croogo->viewFallback(array(
			'editorias_ajax_' . $slug
		));
	}

	public function tipo(){
		//pega o slug da conteudo
		$slug = $this->params['type'];

		//pega o nome da conteudo
		$tipo = $this->Node->query("SELECT id, title, alias FROM types WHERE alias = '$slug';");

		//manda o titulo para a view
		$this->set('title', $tipo[0]['types']['title']);
		$this->set('slug', $slug);

		//prepara o nome da view
		$this->Croogo->viewFallback(array(
			'tipo_' . $slug
		));
	}

	public function tipo_ajax(){

		$slug = $this->params->query['slug'];
		$page = $this->params->query['page'];
		$limit = $this->params->query['limit'];

		$nomeCache = 'editorias_lista';

		$offset = (($page - 1) * $limit);
		$sql = "
		SELECT 
			Node.publish_start, Node.id, Node.title, Node.path, Node.type,  
			Editoria.title, Editoria.slug, 
			Multiattach.filename, 
			NoticiumDetail.chapeu
		FROM nodes AS Node 
		LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
		LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
		LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
		LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
		WHERE 
			Node.publish_start <= '".date('Y-m-d H:i:s')."' AND 
			(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
			Node.type = '$slug' AND 
			Node.status = '1' 
		GROUP BY Node.id 	
		ORDER BY Node.publish_start DESC
		LIMIT $offset, $limit;
		";

		$cacheDisable = Configure::read('Cache.disable');
		//Se o cache está ativo
		if (!$cacheDisable) {
			$cacheName = 'tipos_'.$slug . '_' . $limit . '_' . $page;
			$cacheConfig = 'tipos';
			$nodes = Cache::read($cacheName, $cacheConfig);
			//se NÃO encontrar dados no cache, procura no banco novamente
			if (!$nodes) {
				$nodes = $this->Node->query($sql);
				//grava o resultado no cache
				Cache::write($cacheName, $nodes, $cacheConfig);
			}
		}else{
			$nodes = $this->Node->query($sql);
		}
		//envia os dados para view
		$this->set('nodes', $nodes);
		$this->layout = 'ajax';
		//prepara o nome da view
		$this->Croogo->viewFallback(array(
			'tipo_ajax_' . $slug
		));
	}
}
