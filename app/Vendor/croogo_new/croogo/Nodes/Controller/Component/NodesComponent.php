<?php

App::uses('Component', 'Controller');

/**
 * Nodes Component
 *
 * @category Component
 * @package  Croogo.Nodes.Controller.Component
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class NodesComponent extends Component {

/**
 * Nodes for layout
 *
 * @var string
 * @access public
 */
	public $nodesForLayout = array();

/**
 * initialize
 *
 * @param Controller $controller instance of controller
 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
		if (isset($controller->Node)) {
			$this->Node = $controller->Node;
		} else {
			$this->Node = ClassRegistry::init('Nodes.Node');
		}

		if (Configure::read('Access Control.multiRole')) {
			Configure::write('Acl.classname', 'Acl.HabtmDbAcl');
			App::uses('HabtmDbAcl', 'Acl.Controller/Component/Acl');
			$controller->Acl->adapter('HabtmDbAcl');
			$this->Node->User->bindModel(array(
				'hasAndBelongsToMany' => array(
					'Role' => array(
						'className' => 'Users.Role',
						'with' => 'Users.RolesUser',
					),
				),
			), false);
		}
	}

/**
 * Startup
 *
 * @param Controller $controller instance of controller
 * @return void
 */
	public function startup(Controller $controller) {
		if (!isset($controller->request->params['admin']) && !isset($controller->request->params['requested'])) {
			$this->nodes();
		}
	}

/**
 * Nodes
 *
 * Nodes will be available in this variable in views: $nodes_for_layout
 *
 * @return void
 */
	public function nodes() {
		$roleId = $this->controller->Croogo->roleId();

		$nodes = $this->controller->Blocks->blocksData['nodes'];

		foreach ($nodes as $alias => $options) {
			//Se foi informado SQL
			if(isset($options['sql'])){

				$fields = 'Node.id, Node.title, Node.excerpt, Node.lida, Node.slug, Node.path, Node.terms, Node.ordem, Node.publish_start, Node.type, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.mime ';

				if (isset($options['linkExterno'])) {
					$fields .= ', NoticiumDetail.link_externo ';
				}

				$limit = '6';
				if(Configure::read('Site.blocos_home_total_noticias')){
					$limit = Configure::read('Site.blocos_home_total_noticias');
				}

				$diasAtras = '15';
				if(Configure::read('Site.blocos_home_dias_atras')){
					$diasAtras = Configure::read('Site.blocos_home_dias_atras');
				}

				$maisLidasDiasAtras = '2';
				if(Configure::read('Site.mais_lidas_dias_atras')){
					$maisLidasDiasAtras = Configure::read('Site.mais_lidas_dias_atras');
				}

				$cacheConfig 	= 'home_';
				if(isset($options['cacheConfig'])){
					$cacheConfig = $options['cacheConfig'];
				}
				$nomeCache 	= 'sql_';
				if(isset($options['nomeCache'])){
					$nomeCache = $options['nomeCache'];
				}

				if(isset($options['type'])){
					$type = $options['type'];
				}else{
					$type = 'noticia';
				}

				if(isset($options['join'])){
					$join = $options['join'];
				}else{
					$join = '';
				}

				//Where padrão
				$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND Node.type = '".$type."'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";

				$order = 'Node.publish_start DESC ';

				//se tem fields
				if(isset($options['editoriaID']) && isset($options['editoriaNome'])){
					$nomeCache = $options['editoriaNome'];
					//trata os termos
					$termo = "{\"".$options['editoriaID']."\":\"".$options['editoriaNome']."\"}";
					$termo = str_replace('""', '"', $termo);
					//verifica se é para trazer os promovidos
					if(isset($options['promote'])){
						$promovido = $options['promote'];
					}else{
						$promovido = '0';
					}

					//Se foi informado quantos dias atras que é para procurar nodes
					if(isset($options['diasAtras'])){
						$diasAtras = $options['diasAtras'];
					}

					//monta o WHERE
					$where = "Node.terms = '".$termo."' AND Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND Node.type = '".$type."'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' AND Node.promote='$promovido'";
				}

				if(isset($options['promote'])){
					$where = "Node.promote  = '1' AND Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND Node.type = 'noticia'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
				}

				if(isset($options['maisLidas'])){
					$cacheConfig = 'maislidas_';
					$nomeCache = 'maislidas_';
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$maisLidasDiasAtras.' days'))."' AND Node.type = 'noticia'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$order = "Node.lida DESC";
				}

				// se tem limit
				if(isset($options['limit'])){
					$limit = $options['limit'];
					$limit = str_replace(array("{","}"), array("'","'"), $limit);
					$limit = str_replace('"', '', $limit);
				}

				// se tem fields
				if(isset($options['fields'])) {
					$fields = $options['fields'];
					$fields = str_replace(array("{","}"), array("'","'"), $fields);
					$fields = str_replace('"', '', $fields);
				}

				//Se for HOME BP utilza o plugin de home
				if(isset($options['destaquesTopBp'])){

					$nomeCache = 'HomeTop_'.$options['destaquesTopBp'];
					$where = 'Node.ordem >= 0 AND Node.ordem <= 12 AND status = 1 AND promote = 1';

					if(!isset($options['fields'])) {
						$fields = 'Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, Node.type, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.meta';
					}

					$limit = 18;
					if(isset($options['limit'])){
						$limit = $options['limit'];
					}

					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.ordem ASC
					LIMIT $limit;
					";
				//Se for HOME BP utilza o plugin de home
				}else if(isset($options['destaquesHomeOrdenados'])){

					$nomeCache = 'Home_'.$options['destaquesHomeOrdenados'];

					if(!$options['ordemInicio']){
						$ordemInicio = 1;
					}
					$ordemInicio = $options['ordemInicio'];
					$where = 'Node.ordem >= "'.$ordemInicio.'" AND Node.ordem <= "'.$options['ordemFim'].'" AND Node.status = 1 AND Node.promote = 1';

					if(!isset($options['fields'])) {
						$fields = 'Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, Node.type, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.meta';
					}

					$limit = 16;
					if(isset($options['limit'])){
						$limit = $options['limit'];
					}

					$sql = "
					SELECT
							DISTINCT Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, Node.type,
							NoticiumDetail.chapeu, NoticiumDetail.titulo_capa,
							Multiattach.filename, Multiattach.meta
						FROM nodes AS Node
						LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
						LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
						WHERE $where 
					UNION(
						SELECT
							DISTINCT Node.id, Node.title, Node.slug, CONCAT('/blog/', Blog.slug, '/post/', Node.slug), Node.ordem, Node.ordem, Node.excerpt,
							'post', Blog.title, '',
							Image.filename, Image.credito
						FROM blog_nodes AS Node
						LEFT JOIN blogs AS Blog ON(Blog.id = Node.blog_id)
						LEFT JOIN media_images AS Image ON(Image.node_id = Node.id AND Image.controller = 'blogs')
						WHERE $where 
					)
					ORDER BY ordem ASC
					LIMIT $limit;
					";
				//Se for HOME BP utilza o plugin de home
				/***
				 * o método abaixo serve quando é selcionado não apenas qual notícia deve aparecer na capa,
				 * mas também qual é a ordem de cada uma delas, pois a funç]ap abaixo
				 * pesquisa e mantem a ordem selecionada no CMS
				 *
				*/
				}else if(isset($options['destaquesHomeSelecionados'])){

					$nomeCache = 'Home_'.$options['destaquesHomeSelecionados'];

					$where = 'Node.id IN('.Configure::read('Home.'.$options['destaquesHomeSelecionados']).')';

					if(!isset($options['fields'])) {
						$fields = 'Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, Node.type, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.meta';

						if (isset($options['linkExterno'])) {
							$fields .= ', NoticiumDetail.link_externo';
						}
					}

					$limit = 16;
					if(isset($options['limit'])){
						$limit = $options['limit'];
					}

					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY FIELD(Node.id, ".Configure::read('Home.'.$options['destaquesHomeSelecionados']).")
					LIMIT $limit;
					";
				//Se for destaquesHome
				}else if(isset($options['destaquesHome'])){

					//se foi informado ajax
					$nomeCache = 'destaquesHome';
					$where = "Node.ordem > 0 AND Node.promote = '1' AND Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = 'noticia'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";

					if(!isset($options['fields'])) {
						$fields = 'Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, Node.type, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.meta, Multiattach.mime';

						if (isset($options['linkExterno'])) {
							$fields .= ', NoticiumDetail.link_externo';
						}
					}

					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.ordem ASC, Node.publish_start DESC
					LIMIT 18;
					";

					//se foi informado por Ajax
					if(isset($options['ajax'])){
						$nomeCache = 'destaquesHomeAjax';
						$sql = "
						SELECT 
							Node.publish_start, Node.id, Node.title, Node.path, Node.ordem, Node.excerpt,
							Editoria.title, Editoria.slug, 
							Multiattach.filename, Multiattach.meta, Multiattach.mime,
							NoticiumDetail.chapeu, NoticiumDetail.titulo_capa 
						FROM nodes AS Node 
						LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
						LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
						LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
						LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
						WHERE 
							Node.ordem > 0 AND 
							Node.promote = '1' AND 
							Node.publish_start <= '".date('Y-m-d H:i:s')."' AND 
							(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
							Node.type = 'noticia' AND 
							Node.status = '1' 
						GROUP BY Node.id 
						ORDER BY Node.ordem ASC, Node.publish_start DESC 
						LIMIT $limit;
						";
					}
				
				//Se for editoriaSlug
				}else if(isset($options['editoriaSlug'])){

					$nomeCache = 'editoriaSlug'.$options['editoriaSlug'];
					$termo = $options['editoriaSlug'];

					$sql = "
					SELECT 
						Node.publish_start, Node.id, Node.title, Node.path, Node.ordem, Node.excerpt,
						Editoria.title, Editoria.slug, 
						Multiattach.filename, Multiattach.meta, Multiattach.mime,
						NoticiumDetail.chapeu, NoticiumDetail.titulo_capa 
					FROM nodes AS Node 
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
					LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
					LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
					WHERE 
						Node.publish_start <= '".date('Y-m-d H:i:s')."' AND 
						(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
						Node.type = 'noticia' AND 
						Editoria.slug = '$termo' AND 
						Node.status = '1' 
					GROUP BY Node.id 
					ORDER BY Node.publish_start DESC 
					LIMIT $limit;
					";

				//Se for ultimas noticias
				}else if(isset($options['ultimasNoticias'])){

					$nomeCache = 'ultimasNoticias';

					$sql = "
					SELECT 
						Node.publish_start, Node.id, Node.title, Node.path, Node.ordem, Node.excerpt, Node.terms, 
						Editoria.title, Editoria.slug, 
						Multiattach.filename, Multiattach.meta, Multiattach.mime,
						NoticiumDetail.chapeu, NoticiumDetail.titulo_capa 
					FROM nodes AS Node 
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
					LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
					LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
					WHERE 
						Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND
						(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
						Node.type = 'noticia' AND 
						Node.status = '1' 
					GROUP BY Node.id 
					ORDER BY Node.publish_start DESC 
					LIMIT $limit;
					";

				//Se for Mais Lidas
				}else if(isset($options['maisLidasAjax'])){

					$nomeCache = 'maisLidasAjax';

					$sql = "
					SELECT 
						Node.publish_start, Node.id, Node.title, Node.path, Node.ordem, Node.excerpt,
						Editoria.title, Editoria.slug, 
						Multiattach.filename, Multiattach.meta, Multiattach.mime,
						NoticiumDetail.chapeu, NoticiumDetail.titulo_capa 
					FROM nodes AS Node 
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
					LEFT JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id) 
					LEFT JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id) 
					WHERE 
						Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND
						(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND 
						Node.type = 'noticia' AND 
						Node.status = '1' 
					GROUP BY Node.id 
					ORDER BY Node.publish_start DESC 
					LIMIT $limit;
					";
				//Destaques do Guia
				}else if(isset($options['destaquesGuia'])){
					$nomeCache = 'destaquesGuia';
					$where = "Node.promote = '1' AND Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = 'guia'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";

					if(!isset($options['fields'])) {
						$fields = 'Node.id, Node.title, Node.slug, Node.path, Node.terms, Node.ordem, Node.excerpt, NoticiumDetail.chapeu, NoticiumDetail.titulo_capa, Multiattach.filename, Multiattach.meta';
					}

					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.ordem ASC, Node.publish_start DESC
					LIMIT 18;
					";
				//Se for ultimas noticias da home
				}else if(isset($options['ultimasNoticiasHome'])){
					$nomeCache = 'ultimasNoticiasHome';
					$limit = 6;
					if(Configure::read('Site.total_ultimas_noticias')){
						$limit = Configure::read('Site.total_ultimas_noticias');
					}
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND Node.type = 'noticia'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Node.publish_start, Node.terms
					FROM nodes AS Node
					WHERE $where
					ORDER BY Node.publish_start DESC
					LIMIT $limit;
					";
				//Se for palavra chave
				}else if(isset($options['palavrachave'])){
					$cacheConfig = 'maislidas_';
					$nomeCache = 'palavrachave';

					if(isset($options['diasAtras']) && !empty($options['diasAtras'])){
						$diasAtras = trim($options['diasAtras']);
					}
					$palavrachave = trim($options['palavrachave']);
					$where = "
						Node.publish_start <= '".date('Y-m-d H:i:s')."' AND
						Node.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND
						Node.type = 'noticia'  AND
						(Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND
						Node.status = 1 AND
						NoticiumDetail.keywords LIKE '%$palavrachave%'
						";
					$sql = "
					SELECT Node.publish_start, Node.id, Node.title, Node.path, Multiattach.filename
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.ordem ASC, Node.publish_start DESC
					LIMIT $limit;
					";
				//Se for imagem do dia
				}else if(isset($options['imagemDia'])){
					$nomeCache = 'imagemDia';
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = 'imagem'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Multiattach.filename, Multiattach.meta
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					WHERE $where
					ORDER BY Node.publish_start DESC
					LIMIT 1;
					";
				//Se for Galeria da Home
				}else if(isset($options['galeriaHome'])){
					$nomeCache = 'galeriaHome';
					$where = "Node.type = 'galeria' AND Node.publish_start <= '".date('Y-m-d H:i:s')."' AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Multiattach.filename, Multiattach.meta
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.publish_start DESC
					LIMIT 3;
					";
				//Se for ultimo impresso
				}else if(isset($options['ultimoImpresso'])){
					$nomeCache = 'ultimoImpresso';
					$type = 'impresso';
					if($options['ultimoImpresso'] != 'true'){
						$nomeCache = $options['ultimoImpresso'];
						$type = $options['ultimoImpresso'];
					}
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = '".$type."'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Node.type, Node.publish_start, Multiattach.filename, Multiattach.meta, ImpressoDetail.link
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN impresso_details AS ImpressoDetail ON(ImpressoDetail.node_id = Node.id)
					WHERE $where
					ORDER BY Node.publish_start DESC
					LIMIT 1;
					";

					// Para o novo Tema de Impressos
					if (isset($options['pageFlip']) && $options['pageFlip']) {
						$sql = "SELECT Node.id, Node.title, Node.path, Node.publish_start, Multiattach.filename, Multiattach.meta, ImpressoDetail.link, PageFlip.id, PageFlip.images FROM nodes AS Node LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id AND Multiattach.mime = 'image/jpeg') LEFT JOIN impresso_details AS ImpressoDetail ON(ImpressoDetail.node_id = Node.id) LEFT JOIN pageflip_pdfs AS PageFlip ON(PageFlip.node_id = Node.id) WHERE $where ORDER BY Node.publish_start DESC LIMIT 1;";
					}
				//Se for de video destacado
				}else if(isset($options['impressosDestaque'])){
					$nomeCache = 'impressosDestaque';
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = 'impresso'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Node.publish_start, Multiattach.filename, Multiattach.meta, ImpressoDetail.link, PageFlip.id, PageFlip.images
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id AND Multiattach.mime = 'image/jpeg')
					LEFT JOIN impresso_details AS ImpressoDetail ON(ImpressoDetail.node_id = Node.id)
					LEFT JOIN pageflip_pdfs AS PageFlip ON(PageFlip.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY Node.publish_start DESC
					LIMIT 1, 11;
					";
				//Se for de video destacado
				}else if(isset($options['video_destacado'])){
					$nomeCache = 'video_destacado';
					$where = "Node.publish_start <= '".date('Y-m-d H:i:s')."' AND Node.type = 'video'  AND (Node.publish_end IS NULL OR Node.publish_end >= '".date('Y-m-d H:i:s')."') AND Node.promote = 1 AND Node.status = '1' ";
					$sql = "
					SELECT Node.id, Node.title, Node.path, Multiattach.filename, Multiattach.meta, VideoDetail.*
					FROM nodes AS Node
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id AND Multiattach.mime = 'image/jpeg')
					LEFT JOIN video_details AS VideoDetail ON(VideoDetail.node_id = Node.id)
					WHERE $where
					ORDER BY Node.publish_start DESC
					LIMIT 1;
					";
				//se for normal
				} else if (isset($options['blockVideos'])) {
					$nomeCache = 'home_videos_block';

					$fields = "*";
					$where = "Node.type = 'video' AND Node.status = '1' ";
					$order = "Node.publish_start DESC";
					$limit = 4;

					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN video_details AS VideoDetail ON(VideoDetail.node_id = Node.id)
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					WHERE $where
					GROUP BY Node.id
					ORDER BY $order
					LIMIT $limit
					";
				} else if (isset($options['blogID']) && isset($options['blogName'])) {
					$nomeCache = 'Home_blog_'.$options['blogName'];

					$blog_id = preg_replace('/\D/', '', $options['blogID']);
					$fields = "Node.*, Blog.id, Blog.title, Blog.slug, Image.*";
					$where = "Node.blog_id = '{$blog_id}' AND Node.status = '1' ";

					$sql = "
					SELECT $fields
					FROM blog_nodes AS Node
					LEFT JOIN blogs AS Blog ON(Blog.id = Node.blog_id)
					LEFT JOIN media_images AS Image ON(Image.node_id = Node.id AND Image.controller = 'blogs')
					WHERE $where
					GROUP BY Node.id
					ORDER BY $order
					LIMIT $limit
					";
				} else if (isset($options['classificados'])) {
					$nomeCache = 'home_classificados_block';

					$fields = "*";
					$where = "Node.status = '1' ";

					$sql = "
					SELECT $fields
					FROM classificado_nodes AS Node
					LEFT JOIN classificado_categorias AS Categoria ON(Categoria.id = Node.categoria_id)
					LEFT JOIN classificados AS Classificado ON(Classificado.id = Categoria.classificado_id)
					LEFT JOIN media_images AS Image ON(Image.node_id = Node.id AND Image.controller = 'classificados')
					WHERE $where
					GROUP BY Node.id
					ORDER BY $order
					LIMIT $limit
					";
				} else {
					// $nomeCache = 'comuns_';
					$sql = "
					SELECT $fields
					FROM nodes AS Node
					LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
					LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
					LEFT JOIN model_taxonomies AS ModTax ON(ModTax.foreign_key = Node.id)
					LEFT JOIN taxonomies AS Tax ON(Tax.id = ModTax.taxonomy_id) $join
					WHERE $where
					GROUP BY Node.id
					ORDER BY $order
					LIMIT $limit
					";
				}

				$cacheDisable = Configure::read('Cache.disable');
				//Se o cache está ativo
				if (!$cacheDisable && isset($nomeCache)) {
					$roleId = $this->controller->Croogo->roleId();
					$cacheName = $nomeCache.'_'.$roleId . '_' . Configure::read('Config.language');

					//Se não foi informado a condfiguracao do cache
					if(!isset($cacheConfig)){
						$cacheConfig = 'default';
					}

					$nodes = Cache::read($cacheName, $cacheConfig);

					if (!$nodes) {
						$nodes = $this->Node->query($sql);
						Cache::write($cacheName, $nodes, $cacheConfig);
					}
				}else{
					$nodes = $this->Node->query($sql);
				}

				$this->nodesForLayout[$alias] = $nodes;

			//Normal
			}else{

				$_nodeOptions = array(
					'find' => 'all',
					'conditions' => array(
						'Node.status' => 1,
						'Node.publish_start <=' => date('Y-m-d H:i:s'),
						'OR' => array(
							'Node.publish_end >=' => date('Y-m-d H:i:s'),
							'Node.publish_end'	=> null
						),
					),
					'order' => 'Node.created DESC',
					'limit' => 5
				);

				$options = Hash::merge($_nodeOptions, $options);
				$options['limit'] = str_replace('"', '', $options['limit']);

				//Opções padrão
				$opcoes = array(
					'conditions' => $options['conditions'],
					'order' => $options['order'],
					'limit' => $options['limit'],
					'cache' => array(
						'prefix' => 'nodes_' . $alias,
						'config' => 'croogo_nodes',
					),
				);

				//Se foi informado contain
				if(isset($options['contain'])){
					$options['contain'] = $this->controller->Blocks->stringToArray($options['contain']);
					$opcoes['contain'] = $options['contain'];
				}

				//Se foi informado recursive
				if(isset($options['recursive'])){
					$opcoes['recursive'] = str_replace('"', '', $options['recursive']);
					$opcoes['recursive'] = $options['recursive'];
				}

				$node = $this->Node->find($options['find'], $opcoes);
				$this->nodesForLayout[$alias] = $node;
			}
		}
	}

/**
 * beforeRender
 *
 * @param object $controller instance of controller
 * @return void
 */
	public function beforeRender(Controller $controller) {
		$controller->set('nodes_for_layout', $this->nodesForLayout);
	}

}
