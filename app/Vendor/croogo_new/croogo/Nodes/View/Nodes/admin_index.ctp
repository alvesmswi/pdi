<?php

$this->extend('/Common/admin_index');
$this->Croogo->adminScript(array('Nodes.admin'));

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Content'), '/' . $this->request->url);

$this->append('actions');
	//lista as materias da capa
	echo '<a href="/admin/nodes/nodes/add/noticia" class="btn btn-success"><i class="icon-plus"> </i> Nova Notícia</a>';
	echo '<a href="/admin/nodes/nodes/index/capa:true" class="btn btn-info pull-right"><i class="icon-list"> </i> Listar Materias da Capa </a>';
$this->end();

$this->append('search', $this->element('admin/nodes_search'));

$this->append('form-start', $this->Form->create(
	'Node',
	array(
		'url' => array('controller' => 'nodes', 'action' => 'process'),
		'class' => 'form-inline'
	)
));


$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('title', __d('croogo', 'Title')),
		$this->Paginator->sort('lida', __d('croogo', 'Views')),
		$this->Paginator->sort('publish_start', 'Data'),
		$this->Paginator->sort('type', __d('croogo', 'Type')),
		$this->Paginator->sort('user_id', __d('croogo', 'User')),
		$this->Paginator->sort('status', __d('croogo', 'Status')),
		''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php
	$today = date("Y-m-d H:i:s");
?>
<?php foreach ($nodes as $node): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('Node.' . $node['Node']['id'] . '.id', array('class' => 'row-select')); ?></td>
		<td><?php echo $node['Node']['id']; ?></td>
		<td>
			<span>
			<?php
				echo $this->Html->link($node['Node']['title'], array(
					'admin' => false,
					'controller' => 'nodes',
					'action' => 'view',
					'type' => $node['Node']['type'],
					'slug' => $node['Node']['slug']
				));
			?>
			</span>

			<?php if(isset($node['Term']['title']) && !empty($node['Term']['title'])) { ?>
				<span class="label label-warning" style="background-color: #666;"><?php echo $node['Term']['title']; ?></span>
			<?php } ?>

			<?php if(isset($node['Node']['exclusivo']) && !empty($node['Node']['exclusivo']) && $node['Node']['exclusivo'] == 1) { ?>
				<span class="label label-warning" style="background-color: #468847;"><?php echo '$'; ?></span>
			<?php } ?>

			<?php if ($node['Node']['publish_start'] > $today): ?>
				<span>
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC2ElEQVQ4T11TbUhTYRQ+Z3Pf03SzUHHbVRckWSkGYkImwSQwtSX97OufIFJhqZRlGCn5UYjYv6x/BblUhNAfiYVOiVLUWNHQfVgK6jbXvufuG/fezanvj3t5z3nPOc/znHMQDpzXvwM6QkM5AjkDALlRt4kATiMPxq4fFY/vDcHYZcDkp5CHddIEqNDIeSKVnJ+qEKGM8TuCxGv3RDatHjro24FRQpO+G7kSC+NjE7w0+SkeHWlTyflFhYcFmhQxX4iE8RDuBWE/4AjSoW8bYavdE5mlefyW2lyJhU3wYt7VpUlMqDynkmqFCIhcFEjvvAdvTw1XhbUghAkhn+w+s+3fzsit/OQGbJ/Z0kkE2HsxJzFLKeEL2cLR5ykNg+DsquGQMGkIgwjBGdgJjZg9K/4wqcfmz2vd+Udk+lK1jDooaFrjIKx3XI6bdxUDmLR6LPMbPgPWjdmNV44r89RJQjmTnQPKFaPuG8DyVL8LIIaCoWhzBz3vfmwt4dWhZdftkswkiYDH5Y+iZf7HHhrgZ5t+D4WY7AT8YUKeT6+6sfrtL1djKZUkFvCi9eOICx5/OMiKvc89ugT+ME2eTVrceH5g0XizSJWXrZTK2a4hAgECxU+GwPigOqZntHSsrQDLDp/n1ax9CU/3f+8uyVHqq0+kU6zIQKCsYxgmmqpYxVlK7MSQ6DwwJgLDi+uWqeUtA2p7ZnQKiaC39qw2Ky1RLLzQOQwf71XFA2Mk4sMA6+5AqP+LecXpD9ez5oz2qa5TGYcqrxVnaxktmMHjJjEmGkeLsQXCEfLGuGJe+Ls98qe5pIFNkNw6QQFg28nMlCJ9gVqTniwR7u0I11yANZcvZJizWxdWnbMApMXVWsaNMnPETRNUJCFSpxSLKgophShfrUxVK+QyJtLm9HjnbY7Nr9bN4LY3NIo0vy/QURZfpn29ujuuA4RyQODWmaNiAoRpoGEMOnX71vk/xCYuHzc8B5YAAAAASUVORK5CYII=" title="Agendado para <?= $node['Node']['publish_start']; ?>">
				</span>
			<?php endif; ?>

			<?php if ($node['Node']['promote'] == 1): ?>
				<span class="label label-info"><?php echo __d('croogo', 'promoted'); ?></span>
				<span class="label label-default"><?php echo $node['Node']["ordem"] ?></span>
			<?php endif ?>

			<?php if ($node['Node']['status'] == CroogoStatus::PREVIEW): ?>
				<span class="label label-warning"><?php echo __d('croogo', 'preview'); ?></span>
			<?php endif ?>

		</td>
		<td>
			<span class="label label-success"><?php echo $node['Node']["lida"] ?></span>
		</td>
		<td><?php echo date('d/m/Y H:i',strtotime($node['Node']['publish_start'])); ?></td>
		<td>
		<?php
			echo $this->Html->link($node['Node']['type'], array(
				'action' => 'hierarchy',
				'?' => array(
					'type' => $node['Node']['type'],
				),
			));
		?>
		</td>
		<td>
			<?php echo $node['User']['username']; ?>
		</td>
		<td>
			<?php
				echo $this->element('admin/toggle', array(
					'id' => $node['Node']['id'],
					'status' => (int)$node['Node']['status'],
				));
			?>
		</td>
		<td>
			<div class="item-actions">
			<?php
				echo $this->Croogo->adminRowActions($node['Node']['id']);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'edit', $node['Node']['id']),
					array('icon' => $this->Theme->getIcon('update'), 'tooltip' => __d('croogo', 'Edit this item'))
				);
				/*echo ' ' . $this->Croogo->adminRowAction('',
					'#Node' . $node['Node']['id'] . 'Id',
					array(
						'icon' => $this->Theme->getIcon('copy'),
						'tooltip' => __d('croogo', 'Create a copy'),
						'rowAction' => 'copy',
					)
				);*/

				echo ' ' . $this->Croogo->adminRowAction('',
					'#Node' . $node['Node']['id'] . 'Id',
					array(
						'icon' => $this->Theme->getIcon('delete'),
						'class' => 'delete',
						'tooltip' => __d('croogo', 'Remove this item'),
						'rowAction' => 'delete',
					),
					__d('croogo', 'Are you sure?')
				);

				//Se o plugin está ativo
				if(CakePlugin::loaded('Onesignal')){
					//se for noticias
					//if($node['Node']['type'] == 'noticia'){
						//se já foi configurado o OneSignal
						if(Configure::read('Onesignal.oneSignalId') != '' && Configure::read('Onesignal.oneSignalKey') != ''){
							//se já foi notificado
							if($node['Node']['onesignal_push']){ ?>
								<a style="color:#ccc;" onclick="alert('Notícia já notificada.')"><i class="icon-comment-alt icon-large"></i></a>
							<?php
							}else{
								echo $this->Html->link(
									'<i class="icon-comment-alt icon-large"></i>',
									array('plugin' => 'onesignal', 'controller' => 'onesignal', 'action' => 'create', $node['Node']['id']),
									array('confirm' => 'Tem certeza que deseja notificar os inscritos desta notícia?')
								);
							}
						}else{ ?>
							<a style="color:#ccc;" onclick="alert('O plugin de notificações ainda não foi configurado! Entre em contato com a MSWI.')"><i class="icon-comment-alt icon-large"></i></a>
						<?php }
					//}
				}

				$urlSite = Router::url('/', true);
				$urlSite = str_replace('http:', 'https:', $urlSite);
				$urlSite = str_replace('mswi.', '', $urlSite);
				// pr($urlSite);exit;

				$urlTwitter = "https://twitter.com/intent/tweet?text=".$node['Node']['title'].'&url='.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'];
				echo ' ' . $this->Croogo->adminRowAction('',
					'twitter',
					array(
						'icon' => $this->Theme->getIcon('twitter'),
						'tooltip' => ('Publicar esta Notícia no Twitter'),
						'target' => 'blank',
						'onclick' => 'window.open("'.$urlTwitter.'","popup","width=600,height=600"); return false;'
					)
				);

				$urlFace = 'https://www.facebook.com/dialog/share?app_id=140586622674265&href='.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'].'&picture=&title='.$node['Node']['title'].'&description=&redirect_uri=';
				echo ' ' . $this->Croogo->adminRowAction('',
					'facebook',
					array(
						'icon' => $this->Theme->getIcon('facebook'),
						'tooltip' => ('Publicar esta Notícia no Facebook'),
						'target' => 'blank',
						'onclick' => 'window.open("'.$urlFace.'","popup","width=600,height=600"); return false;'
					)
				);

				echo ' ' . $this->Croogo->adminRowAction('',
					'copyurl',
					array(
						'icon' => $this->Theme->getIcon('link'),
						'tooltip' => ('Copiar a URL'),
						'onclick' => 'copyUrl("inputUrl'.$node['Node']['id'].'"); return false;',
						'url' => '#'
					)
				);
				echo '<input 
					type="text" 
					id="inputUrl'.$node['Node']['id'].'" 
					value="'.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'].'" 
					readonly="true" 
					style="width:1px;height:0;padding:0;border:0;"
					/>';
			?>
			</div>
		</td>
	</tr>
<?php endforeach ?>
<script>
	function copyUrl(inputUrl) {
		var urlToCopy = document.getElementById(inputUrl);
		urlToCopy.select();
		document.execCommand("copy");
		alert("URL Copiada! " + urlToCopy.value);
	}
</script>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('Node.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'publish' => __d('croogo', 'Publish'),
			'unpublish' => __d('croogo', 'Unpublish'),
			'promote' => __d('croogo', 'Promote'),
			'unpromote' => __d('croogo', 'Unpromote'),
			'delete' => __d('croogo', 'Delete'),
			/*'copy' => array(
				'value' => 'copy',
				'name' => __d('croogo', 'Copy'),
				'hidden' => true,
			),*/
		),
		'empty' => true,
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'button',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('Node.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));
	$this->Js->buffer("$('.bulk-process').on('click', Nodes.confirmProcess);");

$this->end();

$this->append('form-end', $this->Form->end());
