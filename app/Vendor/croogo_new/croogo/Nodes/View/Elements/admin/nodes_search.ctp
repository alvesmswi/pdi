<?php

$url = isset($url) ? $url : array('action' => 'index');

?>
<div class="clearfix filter">
<?php
	echo $this->Form->create('Node', array(
		'class' => 'form-inline',
		'url' => $url,
		'inputDefaults' => array(
			'label' => false,
		),
	));

	echo $this->Form->input('chooser', array(
		'type' => 'hidden',
		'value' => isset($this->request->query['chooser']),
	));

	echo $this->Form->input('filter', array(
		'title' => __d('croogo', 'Search'),
		'placeholder' => __d('croogo', 'Search...'),
		'tooltip' => false,
	));

	if (!isset($this->request->query['chooser'])):

		echo $this->Form->input('type', array(
			'options' => $nodeTypes,
			'empty' => __d('croogo', 'Type'),
		));

		echo $this->Form->input('status', array(
			'options' => array(
				'1' => __d('croogo', 'Published'),
				'0' => __d('croogo', 'Unpublished'),
			),
			'empty' => __d('croogo', 'Status'),
		));

		echo $this->Form->input('promote', array(
			'options' => array(
				'1' => __d('croogo', 'Yes'),
				'0' => __d('croogo', 'No'),
			),
			'empty' => __d('croogo', 'Promoted'),
		));

	endif;

	//pr($arrayTermos);
	/*echo $this->Form->input('slug', array(
		'title' => __d('croogo', 'Slug'),
		'placeholder' => __d('croogo', 'Slug'),
		'tooltip' => false,
		'required' => false
	));*/

	echo $this->Form->input('publish_start', array(
		'title' => __d('croogo', 'Data de publicação'),
		'placeholder' => __d('croogo', 'Data de publicação'),
		'tooltip' => false,
		'required' => false,
		'type' => 'text'
	));

	$sqlTermos = 'SELECT id, title FROM terms ORDER BY title ASC;';
	$resultTermos = $this->Mswi->query($sqlTermos, 'listTermos');
	$arrayTermos = array();
	foreach($resultTermos as $termo){
		$arrayTermos[$termo['terms']['id']] = $termo['terms']['title'];
	}
	echo $this->Form->input('term_id', array(
		'options' => $arrayTermos,
		'empty' => 'Editorias'
	));

	echo $this->Form->input(__d('croogo', 'Filter'), array(
		'type' => 'submit',
	));
	echo $this->Form->end();
?>
</div>
