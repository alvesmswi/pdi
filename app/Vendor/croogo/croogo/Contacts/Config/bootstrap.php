<?php

CroogoCache::config('contacts_view', array_merge(
	Configure::read('Cache.defaultConfig'),
	array('groups' => array('contacts'))
));

// Configure Wysiwyg
Croogo::mergeConfig('Wysiwyg.actions', array(
	'Contacts/admin_add' => array(
		array(
			'elements' => 'ContactBody',
		),
	),
	'Contacts/admin_edit' => array(
		array(
			'elements' => 'ContactBody',
		),
	),
	'Translate/admin_edit' => array(
		array(
			'elements' => 'ContactBody',
		),
	),
));

Croogo::mergeConfig('Translate.models.Contact', array(
	'fields' => array(
		'title' => 'titleTranslation',
		'body' => 'bodyTranslation',
	),
	'translateModel' => 'Contacts.Contact',
));
