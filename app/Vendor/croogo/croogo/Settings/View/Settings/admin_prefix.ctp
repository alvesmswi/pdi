<?php

$this->extend('/Common/admin_edit');

$this->Html->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Settings'), array('plugin' => 'settings', 'controller' => 'settings', 'action' => 'index'))
	->addCrumb($prefix, '/' . $this->request->url);

$this->append('form-start', $this->Form->create('Setting', array(
	'url' => array(
		'controller' => 'settings',
		'action' => 'prefix',
		$prefix,
	),
	'class' => 'protected-form',
)));

$this->append('tab-heading');
	echo $this->Croogo->adminTab($prefix, '#settings-main');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('settings-main');
		$i = 0;

		foreach ($settings as $setting) :

			if (!empty($setting['Params']['tab'])) {
				continue;
			}

			//se for o layout dos destaques
			if($setting['Setting']['key'] == 'Site.layout_destaque'){ 

				$keyE = explode('.', $setting['Setting']['key']);
				$keyTitle = Inflector::humanize($keyE['1']);
				$label = ($setting['Setting']['title'] != null) ? $setting['Setting']['title'] : $keyTitle;
				$i = $setting['Setting']['id'];
				$value = $setting['Setting']['value'];
				$name = 'data[Setting]['.$i.'][value]';

				echo
					$this->Form->input("Setting.$i.id", array(
						'value' => $setting['Setting']['id'],
					)) .
					$this->Form->input("Setting.$i.key", array(
						'type' => 'hidden', 
						'value' => $setting['Setting']['key']
					)) .
					$this->SettingsForm->input($setting, __d('croogo', $label), $i);
			?>
				<style>
					.flex-row {
						display: flex;
						flex-flow: row wrap;
						margin-top: 10px;
					}
					.radio-group {
						overflow: hidden;
						position: relative;
						display: block;
						
					}
					.radio-group input {
						position: absolute;
						width: 1px;
						height: 1px;
						padding: 0;
						margin: -1px;
						overflow: hidden;
						clip: rect(0,0,0,0);
						border: 0;
					}
					.radio-group input:checked ~ label {
						background: #50c878;
					}
					.radio-group label {
						cursor: pointer;
						display: block;
						padding: 10px;
					}
					.capa img{
						width: 215px;
			    		margin-right: 10px;
					}
				</style>

				<div class="flex-row">
					<div class="radio-group">
						<input type="radio" name="<?=$name?>" id="capa-1" <?=($value=='1')?('checked'):('')?> value ="1">
						<label for="capa-1" class="capa">
							<img src="/theme/Mswi/img/Capa1.png" alt="Capa pequena" />
						</label>
					</div>
					<div class="radio-group">
						<input type="radio" name="<?=$name?>" id="capa-2" <?=($value=='2')?('checked'):('')?> value ="2">
						<label for="capa-2" alt="Capa média" class="capa">
							<img src="/theme/Mswi/img/Capa2.png" alt="Capa pequena" />
						</label>
					</div>
					<div class="radio-group">
						<input type="radio" name="<?=$name?>" id="capa-3" <?=($value=='3')?('checked'):('')?> value ="3">
						<label for="capa-3" alt="Capa grande" class="capa">
							<img src="/theme/Mswi/img/Capa3.png" alt="Capa pequena" />
						</label>
					</div>
				</div>
			<?php

			}else{

				$keyE = explode('.', $setting['Setting']['key']);
				$keyTitle = Inflector::humanize($keyE['1']);

				$label = ($setting['Setting']['title'] != null) ? $setting['Setting']['title'] : $keyTitle;

				$i = $setting['Setting']['id'];
				echo
					$this->Form->input("Setting.$i.id", array(
						'value' => $setting['Setting']['id'],
					)) .
					$this->Form->input("Setting.$i.key", array(
						'type' => 'hidden', 'value' => $setting['Setting']['key']
					)) .
					$this->SettingsForm->input($setting, __d('croogo', $label), $i);
			}
			$i++;

		endforeach;

		echo $this->Croogo->adminTabs();
	echo $this->Html->tabEnd();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Saving')) .
		$this->Form->button(__d('croogo', 'Save')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'btn btn-danger')) .
	$this->Html->endBox();
	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
