<div class="attachments index">

	<h2><?php echo $title_for_layout; ?></h2>

	<div class="<?php echo $this->Theme->getCssClass('row'); ?>">
		<div class="actions <?php echo $this->Theme->getCssClass('columnFull'); ?>">
			<ul class="nav-buttons">
			<?php
				echo $this->Croogo->adminAction(
					__d('croogo', 'New Attachment'),
					array('action' => 'add', 'editor' => 1)
				);
			?>
			</ul>
		</div>
	</div>

	<?php echo $this->element('admin/attachments_search');?>

	<table class="table table-striped">
	<?php
		$tableHeaders = $this->Html->tableHeaders(array(
			$this->Paginator->sort('id', __d('croogo', 'Id')),
			'&nbsp;',
			$this->Paginator->sort('title', __d('croogo', 'Title')),
			'&nbsp;',
			__d('croogo', 'URL'),
		));
		echo $tableHeaders;

		$rows = array();
		foreach ($attachments as $attachment):
			list($mimeType, $mimeSubtype) = explode('/', $attachment['Attachment']['mime_type']);
			$imagecreatefrom = array('gif', 'jpeg', 'png', 'string', 'wbmp', 'webp', 'xbm', 'xpm');
			if ($mimeType == 'image' && in_array($mimeSubtype, $imagecreatefrom)) {
				$thumbnail = $this->Image->resize($attachment['Attachment']['path'], 100, 200);
			} else {
				$thumbnail = $this->Html->thumbnail('/croogo/img/icons/page_white.png', array('alt' => $attachment['Attachment']['mime_type'])) . ' ' . $attachment['Attachment']['mime_type'] . ' (' . $this->Filemanager->filename2ext($attachment['Attachment']['slug']) . ')';
			}

			$insertCode = $this->Html->link('', '#', array(
				'onclick' => "Croogo.Wysiwyg.choose('" . $attachment['Attachment']['slug'] . "');",
				'escapeTitle' => false,
				'icon' => $this->Theme->getIcon('attach'),
				'tooltip' => __d('croogo', 'Insert')
			));

			$rows[] = array(
				$attachment['Attachment']['id'],
				$this->Html->link($thumbnail,
					'#',
					array('onclick' => "Croogo.Wysiwyg.choose('" . $attachment['Attachment']['slug'] . "');")
				),
				$attachment['Attachment']['title'],
				$insertCode,
				$this->Html->link(Router::url($attachment['Attachment']['path']),
					'#',
					array('onclick' => "Croogo.Wysiwyg.choose('" . $attachment['Attachment']['slug'] . "');")
				)
			);
		endforeach;

		echo $this->Html->tableCells($rows);
		echo $tableHeaders;
	?>
	</table>
</div>

<div class="<?php echo $this->Theme->getCssClass('row'); ?>">
	<div class="<?php echo $this->Theme->getCssClass('columnFull'); ?>">
		<?php echo $this->element('admin/pagination'); ?>
	</div>
</div>
