<style>
	table {
		width: 100%;
		margin-bottom: 15px;
		border-collapse: collapse;
	}
	td, th {
		border: 1px solid #cdcdcd;
		padding: 5px;
	}
	tr:nth-child(even) {
		background-color: #f2f2f2;
	}
	.bold {
		font-weight: bold;
	}
	.center {
		text-align: center;
	}
</style>

<table>
	<tr>
		<td class="center bold" colspan="7"><?php echo Configure::read('Site.title'); ?></td>
	</tr>
</table>

<table>
	<tr>
		<th>Nome</th>
		<th>Username</th>
		<th>E-mail</th>
		<th>Grupo</th>
		<th>Status</th>
		<th>Expiração</th>
		<th>Criado</th>
	</tr>
	<?php $total = 0; ?>
	<?php foreach ($registros as $key => $registro): ?>
		<tr>
			<td><?php echo $registro['User']['name'] ?></td>
			<td><?php echo $registro['User']['username'] ?></td>
			<td><?php echo $registro['User']['email'] ?></td>
			<td><?php echo $registro['Role']['title'] ?></td>
			<td><?php echo ($registro['User']['status']) ? 'Ativo' : 'Inativo'; ?></td>
			<td><?php echo (!empty($registro['User']['data_expiracao'])) ? date('d/m/Y H:i', strtotime($registro['User']['data_expiracao'])) : null; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($registro['User']['created'])) ?></td>
		</tr>
	<?php endforeach; ?>
</table>
