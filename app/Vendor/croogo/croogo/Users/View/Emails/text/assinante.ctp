<?php echo __d('croogo', 'Hello %s', $user['User']['name']); ?>,

<?php echo Configure::read('Assinantes.emailTemplate'); ?>

<?php
if (empty($url)):
	$url = Router::url(array(
		'controller' => 'users',
		'action' => 'activate',
		$user['User']['username'],
	), true);
endif;

// echo __d('croogo', 'Por favor, visite este link para adicionar mais tempo a sua Assinatura: %s', $url);
