<?php

$this->response->type('application/pdf');

App::import('Vendor', 'Dompdf', array('file' => 'dompdf/autoload.inc.php'));

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();

$dompdf->loadHtml($this->fetch('content'));
#echo $this->fetch('content');

// (Optional) Setup the paper size and orientation
$orientation = ((isset($orientation)) ? $orientation : 'landscape');
$dompdf->setPaper('A4', $orientation);

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
echo $dompdf->output();
