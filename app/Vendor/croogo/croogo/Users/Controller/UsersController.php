<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('UsersAppController', 'Users.Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Users Controller
 *
 * @category Controller
 * @package  Croogo.Users.Controller
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class UsersController extends UsersAppController {

/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Search.Prg' => array(
			'presetForm' => array(
				'paramType' => 'querystring',
			),
			'commonProcess' => array(
				'paramType' => 'querystring',
				'filterEmpty' => true,
			),
		),
	);

/**
 * Preset Variables Search
 *
 * @var array
 * @access public
 */
	public $presetVars = true;

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Users.User');

/**
 * implementedEvents
 *
 * @return array
 */
	public function implementedEvents() {
		return parent::implementedEvents() + array(
			'Controller.Users.beforeAdminLogin' => 'onBeforeAdminLogin',
			'Controller.Users.adminLoginFailure' => 'onAdminLoginFailure',
		);
	}
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Security->validatePost = false;
		$this->Security->csrfCheck = false;
		$this->Security->enabled = false;
		$this->Security->unlockedActions[] = 'reset_password';

		$this->Auth->allow('alertaAssinantes'); // Função para ser acessada via CRON
	}
/**
 * Notify user when failed_login_limit hash been hit
 *
 * @return bool
 */
	public function onBeforeAdminLogin() {
		$field = $this->Auth->authenticate['all']['fields']['username'];
		if (empty($this->request->data)) {
			return true;
		}
		$cacheName = 'auth_failed_' . $this->request->data['User'][$field];
		$cacheValue = Cache::read($cacheName, 'users_login');
		if ($cacheValue >= Configure::read('User.failed_login_limit')) {
			$this->Session->setFlash(__d('croogo', 'You have reached maximum limit for failed login attempts. Please try again after a few minutes.'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => $this->request->params['action']));
		}
		return true;
	}

/**
 * Record the number of times a user has failed authentication in cache
 *
 * @return bool
 * @access public
 */
	public function onAdminLoginFailure() {
		$field = $this->Auth->authenticate['all']['fields']['username'];
		if (empty($this->request->data)) {
			return true;
		}
		$cacheName = 'auth_failed_' . $this->request->data['User'][$field];
		$cacheValue = Cache::read($cacheName, 'users_login');
		Cache::write($cacheName, (int)$cacheValue + 1, 'users_login');
		return true;
	}

/**
 * Admin index
 *
 * @return void
 * @access public
 * $searchField : Identify fields for search
 */
	public function admin_index() {
		$this->set('title_for_layout', __d('croogo', 'Users'));
		$this->Prg->commonProcess();
		$searchFields = array('role_id', 'name', 'email');

		$this->User->recursive = 0;
		$criteria = $this->User->parseCriteria($this->Prg->parsedParams());
		$arrayConditionsGrupos = array();
		//se não for admin
		if($this->Auth->user('role_id')<>1){
			//não exibe os admins
			$criteria['User.role_id <>'] = 1;
			$arrayConditionsGrupos['id <>'] = 1;
		}
		$this->paginate['conditions'] = $criteria;

		$this->set('users', $this->paginate());

		$this->set('roles', $this->User->Role->find(
			'list',
			array(
				'conditions' => $arrayConditionsGrupos
			)
		));
		$this->set('displayFields', $this->User->displayFields());
		$this->set('searchFields', $searchFields);

		if (isset($this->request->query['chooser'])) {
			$this->layout = 'admin_popup';
		}
	}

/**
 * Send activation email
 */
	private function __sendActivationEmail() {
		if (empty($this->request->data['User']['notification'])) {
			return;
		}

		$user = $this->request->data['User'];
		$activationUrl = Router::url(array(
			'admin' => false,
			'plugin' => 'users',
			'controller' => 'users',
			'action' => 'activate',
			$user['username'],
			$user['activation_key'],
		), true);
		$this->_sendEmail(
			array(Configure::read('Site.title'), $this->_getSenderEmail()),
			$user['email'],
			__d('croogo', '[%s] Please activate your account', Configure::read('Site.title')),
			'Users.register',
			'user activation',
			$this->theme,
			array(
				'user' => $this->request->data,
				'url' => $activationUrl,
			)
		);
	}

/**
 * Admin add
 *
 * @return void
 * @access public
 */
	public function admin_add() {
		if (!empty($this->request->data)) {

			if(!isset($this->request->data['User']['role_id']) || empty($this->request->data['User']['role_id'])) {
				$this->request->data['User']['role_id'] = 4; // Assinante
			}

			$this->User->create();
			$this->request->data['User']['activation_key'] = md5(uniqid());
			if ($this->User->saveAssociated($this->request->data)) {
				$this->request->data['User']['id'] = $this->User->id;
				$this->__sendActivationEmail();

				$this->Session->setFlash(__d('croogo', 'The User has been saved'), 'flash', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'alert alert-danger'));
				unset($this->request->data['User']['password']);
			}
		} else {
			$this->request->data['User']['role_id'] = 2; // default Role: Registered
		}
		$arrayConditionsGrupos = array();
		//se não for admin
		if($this->Auth->user('role_id')<>1){
			//não exibe os admins
			$arrayConditionsGrupos['id <>'] = 1;
		}
		$roles = $this->User->Role->find(
			'list',
			array(
				'conditions' => $arrayConditionsGrupos
			)
		);
		$this->set(compact('roles'));
	}

/**
 * Admin edit
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_edit($id = null) {
		if (!empty($this->request->data)) {
			if(isset($this->request->data['User']['check']) && $this->request->data['User']['check'] == 1){
				$id = $this->request->data['User']['id'];
				$this->User->query("UPDATE users SET foto = '' WHERE id = $id;");
			}

			if ($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The User has been saved'), 'flash', array('class' => 'success'));
				return $this->Croogo->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'alert alert-danger'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$arrayConditionsGrupos = array();
		//se não for admin
		if($this->Auth->user('role_id')<>1){
			//não exibe os admins
			$arrayConditionsGrupos['id <>'] = 1;
		}
		$roles = $this->User->Role->find(
			'list',
			array(
				'conditions' => $arrayConditionsGrupos
			)
		);
		$this->set(compact('roles'));
		$this->set('editFields', $this->User->editFields());
	}

/**
 * Admin reset password
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_reset_password($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid User'), 'flash', array('class' => 'alert alert-danger'));
			return $this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			$senha = AuthComponent::password($this->data['User']['password']);
			$this->User->query("UPDATE users SET password = '$senha' WHERE id = $id;");
			$this->Session->setFlash(__d('croogo', 'Password has been reset.'), 'flash', array('class' => 'alert alert-success'));
			return $this->redirect(array('action' => 'index'));

		}
		$this->request->data = $this->User->findById($id);
	}

/**
 * Admin delete
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('croogo', 'Invalid id for User'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'User deleted'), 'flash', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__d('croogo', 'User cannot be deleted'), 'flash', array('class' => 'alert alert-danger'));
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * Admin login
 *
 * @return void
 * @access public
 */
	public function admin_login() {
		$this->set('title_for_layout', __d('croogo', 'Admin Login'));
		$this->layout = "admin_login";
		if ($this->Auth->user('id')) {
			if (!$this->Session->check('Message.auth')) {
				$this->Session->setFlash(
					__d('croogo', 'You are already logged in'), 'flash',
					array('class' => 'alert alert-alert'), 'auth'
				);
			}
			return $this->redirect($this->Auth->redirect());
		}
		if ($this->request->is('post')) {
			Croogo::dispatchEvent('Controller.Users.beforeAdminLogin', $this);
			if ($this->Auth->login()) {
				Croogo::dispatchEvent('Controller.Users.adminLoginSuccessful', $this);
				return $this->redirect($this->Auth->redirect());
			} else {
				Croogo::dispatchEvent('Controller.Users.adminLoginFailure', $this);
				$this->Auth->authError = __d('croogo', 'Login ou Senha inválidos');
				$this->Session->setFlash($this->Auth->authError, 'flash', array('class' => 'error'), 'auth');
				return $this->redirect($this->Auth->loginAction);
			}
		}
	}

/**
 * Admin logout
 *
 * @return void
 * @access public
 */
	public function admin_logout() {
		Croogo::dispatchEvent('Controller.Users.adminLogoutSuccessful', $this);
		$this->Session->setFlash(__d('croogo', 'Logout efetuado com sucesso'), 'flash', array('class' => 'alert alert-success'));
		return $this->redirect($this->Auth->logout());
	}

/**
 * Index
 *
 * @return void
 * @access public
 */
	public function index() {
		$this->set('title_for_layout', __d('croogo', 'Users'));
	}

/**
 * Convenience method to send email
 *
 * @param string $from Sender email
 * @param string $to Receiver email
 * @param string $subject Subject
 * @param string $template Template to use
 * @param string $theme Theme to use
 * @param array  $viewVars Vars to use inside template
 * @param string $emailType user activation, reset password, used in log message when failing.
 * @return boolean True if email was sent, False otherwise.
 */
	protected function _sendEmail($from, $to, $subject, $template, $emailType, $theme = null, $viewVars = null) {
		if (is_null($theme)) {
			$theme = $this->theme;
		}
		$success = false;

		try {
			$email = new CakeEmail();
			$email->from($from[1], $from[0]);
			$email->to($to);
			$email->subject($subject);
			$email->template($template);
			$email->viewVars($viewVars);
			$email->theme($theme);
			$success = $email->send();
		} catch (SocketException $e) {
			$this->log(sprintf('Error sending %s notification : %s', $emailType, $e->getMessage()));
		}

		return $success;
	}

/**
 * Add
 *
 * @return void
 * @access public
 */
	public function add() {
		$this->set('title_for_layout', __d('croogo', 'Register'));
		if (!empty($this->request->data)) {
			$this->User->create();
			if(isset($this->request->data['SitesUser'])){
				$this->request->data['SitesUser']['site_id'];
				$this->request->data['SitesUser']['user_id'];
			}
			//se não foi informado o Grupo
			if(!isset($this->request->data['User']['role_id'])){
				$this->request->data['User']['role_id'] = 2; // Registered
			}
			//se não foi informado o Username
			if(!isset($this->request->data['User']['username'])){
				$this->request->data['User']['username'] = htmlspecialchars($this->request->data['User']['email']);
			}else{
				$this->request->data['User']['username'] = htmlspecialchars($this->request->data['User']['username']);
			}

			//se é preciso pagar para ser assinante
			if(Configure::read('Paywall.PaywallPagamento')){
				$this->request->data['User']['status'] = 0;
			}else{
				$this->request->data['User']['status'] = 1;
			}

			$this->request->data['User']['activation_key'] = md5(uniqid());

			//$this->request->data['User']['website'] = htmlspecialchars($this->request->data['User']['website']);
			$this->request->data['User']['name'] = htmlspecialchars($this->request->data['User']['name']);
			if ($this->User->save($this->request->data)) {
				Croogo::dispatchEvent('Controller.Users.registrationSuccessful', $this);
				$this->request->data['User']['password'] = null;

				$this->_sendEmail(
					array(Configure::read('Site.title'), $this->_getSenderEmail()),
					$this->request->data['User']['email'],
					__d('croogo', '[%s] Please activate your account', Configure::read('Site.title')),
					'Users.register',
					'user activation',
					$this->theme,
					array('user' => $this->request->data)
				);

				$this->Session->setFlash('Enviamos um email para você com instruções de como ativar a sua conta!', 'flash', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'login'));
			} else {
				Croogo::dispatchEvent('Controller.Users.registrationFailure', $this);
				$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * Activate
 *
 * @param string $username
 * @param string $key
 * @return void
 * @access public
 */
	public function activate($username = null, $key = null) {
		if ($username == null || $key == null) {
			return $this->redirect(array('action' => 'login'));
		}

		if ($this->Auth->user('id')) {
			$this->Session->setFlash('Você está logado como '.$this->Auth->user('name'), 'flash', array('class' => 'alert alert-success'));
			return $this->redirect($this->referer());
		}

		$redirect = array('action' => 'login');
		if (
			$this->User->hasAny(array(
				'User.username' => $username,
				'User.activation_key' => $key,
				'User.status' => 0,
			))
		) {
			$user = $this->User->findByUsername($username);
			$this->User->id = $user['User']['id'];

			$db = $this->User->getDataSource();
			$key = md5(uniqid());
			$this->User->updateAll(array(
				$this->User->escapeField('status') => $db->value(1),
				$this->User->escapeField('activation_key') => $db->value($key),
			), array(
				$this->User->escapeField('id') => $this->User->id
			));

			if (isset($user) && empty($user['User']['password'])) {
				$redirect = array('action' => 'reset', $username, $key);
			}

			Croogo::dispatchEvent('Controller.Users.activationSuccessful', $this);
			$this->Session->setFlash(__d('croogo', 'Account activated successfully.'), 'flash', array('class' => 'alert alert-success'));
		} else {
			Croogo::dispatchEvent('Controller.Users.activationFailure', $this);
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'alert alert-danger'));
		}

		if ($redirect) {
			return $this->redirect($redirect);
		}
	}

/**
 * Edit
 *
 * @return void
 * @access public
 */
	public function edit() {
	}

/**
 * Forgot
 *
 * @return void
 * @access public
 */
	public function forgot() {
		$this->set('title_for_layout', __d('croogo', 'Forgot Password'));

		if (!empty($this->request->data) && isset($this->request->data['User']['email'])) {
			$user = $this->User->findByEmail($this->request->data['User']['email']);
			if (!isset($user['User']['id'])) {
				$this->Session->setFlash('Email não encontrado', 'flash', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => $this->action));
			}

			$this->User->id = $user['User']['id'];
			$activationKey = md5(uniqid());
			$this->User->saveField('activation_key', $activationKey);
			$this->set(compact('user', 'activationKey'));

			$emailSent = $this->_sendEmail(
				array(Configure::read('Site.title'), $this->_getSenderEmail()),
				$user['User']['email'],
				__d('croogo', '[%s] Reset Password', Configure::read('Site.title')),
				'Users.forgot_password',
				'reset password',
				$this->theme,
				compact('user', 'activationKey')
			);

			if ($emailSent) {
				$this->Session->setFlash(__d('croogo', 'An email has been sent with instructions for resetting your password.'), 'flash', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'login'));
			} else {
				$this->Session->setFlash(__d('croogo', 'An error occurred. Please try again.'), 'flash', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * Reset
 *
 * @param string $username
 * @param string $key
 * @return void
 * @access public
 */
	public function reset($username = null, $key = null) {
		$this->set('title_for_layout', __d('croogo', 'Reset Password'));

		if ($username == null || $key == null) {
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'alert alert-danger'));
			return $this->redirect(array('action' => 'login'));
		}

		$user = $this->User->find('first', array(
			'conditions' => array(
				'User.username' => $username,
				'User.activation_key' => $key,
			),
		));
		if (!isset($user['User']['id'])) {
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'alert alert-danger'));
			return $this->redirect(array('action' => 'login'));
		}

		if (!empty($this->request->data) && isset($this->request->data['User']['password'])) {
			$this->User->id = $user['User']['id'];
			$user['User']['activation_key'] = md5(uniqid());
			$user['User']['password'] = $this->request->data['User']['password'];
			$user['User']['verify_password'] = $this->request->data['User']['verify_password'];
			$options = array('fieldList' => array('password', 'verify_password', 'activation_key'));
			if ($this->User->save($user['User'], $options)) {
				$this->Session->setFlash(__d('croogo', 'Your password has been reset successfully.'), 'flash', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'login'));
			} else {
				$this->Session->setFlash(__d('croogo', 'An error occurred. Please try again.'), 'flash', array('class' => 'alert alert-danger'));
			}
		}

		$this->set(compact('user', 'username', 'key'));
	}

/**
 * Login
 *
 * @return boolean
 * @access public
 */
	public function login()
	{
		$this->set('title_for_layout', __d('croogo', 'Log in'));

		if (isset($this->request->query['redirect']) && !empty($this->request->query['redirect'])) {
			$redirect = $this->request->query['redirect'];

			$this->Session->write('assinanteRedirect', $redirect);
		} elseif (isset($_SESSION['assinanteRedirect'])) {
			$redirect = $this->Session->read('assinanteRedirect');
		}

		if ($this->request->is('post')) {
			Croogo::dispatchEvent('Controller.Users.beforeLogin', $this);

			if ($this->Auth->login()) {
				Croogo::dispatchEvent('Controller.Users.loginSuccessful', $this);

				//se foi infomado um redirect
				if (isset($redirect) && !empty($redirect)) {
					$this->Session->delete('assinanteRedirect');

					return $this->redirect($this->data['User']['redirect']);
				} else {
					return $this->redirect($this->Auth->redirect());
				}
			} else {
				Croogo::dispatchEvent('Controller.Users.loginFailure', $this);
				//$this->Session->setFlash($this->Auth->authError, 'flash', array('class' => 'alert alert-danger'), 'auth');
				$this->Session->setFlash('Login ou Senha incorretos', 'flash', array('class' => 'alert alert-danger'), 'auth');
				return $this->redirect($this->Auth->loginAction);
			}
		}
	}

/**
 * Logout
 *
 * @return void
 * @access public
 */
	public function logout() {
		Croogo::dispatchEvent('Controller.Users.beforeLogout', $this);
		$this->Session->setFlash(__d('croogo', 'Log out successful.'), 'flash', array('class' => 'alert alert-success'));
		$redirect = $this->Auth->logout();
		Croogo::dispatchEvent('Controller.Users.afterLogout', $this);
		return $this->redirect($redirect);
	}

/**
 * View
 *
 * @param string $username
 * @return void
 * @access public
 */
	public function view($username = null) {
		if ($username == null) {
			$username = $this->Auth->user('username');
		}
		$user = $this->User->findByUsername($username);
		if (!isset($user['User']['id'])) {
			$this->Session->setFlash(__d('croogo', 'Invalid User.'), 'flash', array('class' => 'alert alert-danger'));
			return $this->redirect('/');
		}

		$this->set('title_for_layout', $user['User']['name']);
		$this->set(compact('user'));
	}

	protected function _getSenderEmail() {
		return 'croogo@' . preg_replace('#^www\.#', '', strtolower($_SERVER['SERVER_NAME']));
	}

	/**
	 * Add
	 *
	 * @return void
	 * @access public
	 */
	public function admin_multiple()
	{
		if (!empty($this->request->data)) {
			$arrayUsersSave = array();

			$this->request->data['User']['notification'] = 1;

			if(!isset($this->request->data['User']['role_id']) || empty($this->request->data['User']['role_id'])) {
				$this->request->data['User']['role_id'] = 4; // Assinante
			}

			if (isset($this->request->data['User']['email']) && !empty($this->request->data['User']['email'])) {
				$emails = explode(';', trim($this->request->data['User']['email']));

				if (!empty($emails)) {
					foreach ($emails as $key => $email) {
						if (!empty($email)) {
							$arrayUsersSave['User'][$key]['notification'] = 1;
							$arrayUsersSave['User'][$key]['role_id'] = $this->request->data['User']['role_id'];

							if (isset($this->request->data['User']['dias_uso']) && !empty($this->request->data['User']['dias_uso'])) {
								$arrayUsersSave['User'][$key]['data_expiracao'] = date('Y-m-d 00:00', strtotime('+ '.$this->request->data['User']['dias_uso'].' days'));
							}

							$arrayUsersSave['User'][$key]['email'] = trim($email);
							$arrayUsersSave['User'][$key]['name'] = Inflector::humanize(Inflector::slug(trim($email)));
							$arrayUsersSave['User'][$key]['username'] = Inflector::slug(trim($email));
						}
					}
				}
			}

			$this->admin_multiple_ajax(count($arrayUsersSave['User']), $arrayUsersSave, $arrayUsersSave);
		} else {
			$this->request->data['User']['role_id'] = 4; // default Role: Assinante
		}

		$arrayConditionsGrupos = array();
		if ($this->Auth->user('role_id') <> 1) {
			$arrayConditionsGrupos['id <>'] = 1; // não exibe os admins
		}
		$roles = $this->User->Role->find(
			'list',
			array(
				'conditions' => $arrayConditionsGrupos
			)
		);

		$this->set(compact('roles'));
	}

	public function admin_multiple_ajax($total = 0, $arrayDados = array(), $dados = array(), $processadas = 0, $erros = array())
	{
		if(isset($_SESSION['MultipleUsers'])){
			$processadas 	= $this->Session->read('MultipleUsers.processadas');
			$erros 			= $this->Session->read('MultipleUsers.erros');
			$total 			= $this->Session->read('MultipleUsers.total');
			$arrayDados 	= $this->Session->read('MultipleUsers.arrayDados');
			$dados 			= $this->Session->read('MultipleUsers.dados');
		}

		if (!empty($arrayDados)) {
			$this->request->data['User'] = $arrayDados['User'][$processadas];

			$this->User->create();
			$this->request->data['User']['activation_key'] = md5(uniqid());
			if ($this->User->saveAssociated($this->request->data)) {
				$this->request->data['User']['id'] = $this->User->id;
				$this->__sendActivationEmail();
			} else {
				$erros[] = 'Não foi possivel criar o Usuário: '.$this->request->data['User']['email'];
			}

			unset($arrayDados['User'][$processadas]);

			$processadas++;
		}

		$this->Session->write('MultipleUsers.processadas', $processadas);
		$this->Session->write('MultipleUsers.erros', $erros);
		$this->Session->write('MultipleUsers.total', $total);
		$this->Session->write('MultipleUsers.arrayDados', $arrayDados);
		$this->Session->write('MultipleUsers.dados', $dados);

		$this->set('processadas', $processadas);
		$this->set('erros', $erros);
		$this->set('total', $total);
		$this->set('porcentagem', round($processadas / $total * 100));

		if ($processadas >= $total) {
			$this->set('concluido', true);

			$this->Session->delete('MultipleUsers');
		}

		$this->render('admin_multiple_ajax');
	}

	public function admin_exportar($tipo = 'pdf')
	{
		$this->layout = $tipo;

		$conditions = array();

		if (isset($this->params->query['role_id']) && !empty($this->params->query['role_id'])) {
			$conditions['AND']['Role.id'] = $this->params->query['role_id'];
		}
		if (isset($this->params->query['name']) && !empty($this->params->query['name'])) {
			$conditions['OR']['User.name'] = $this->params->query['name'];
		}
		if (isset($this->params->query['email']) && !empty($this->params->query['email'])) {
			$conditions['OR']['User.email'] = $this->params->query['email'];
		}

		$users = $this->User->find('all', array(
			'conditions' => $conditions,
			'contain' => array(
				'Role'
			),
			'order' => array(
				'User.name' => 'ASC'
			),
			'fields' => array(
				'User.id',
				'User.username',
				'User.name',
				'User.email',
				'User.status',
				'User.data_expiracao',
				'User.created',
				'Role.id',
				'Role.title'
			)
		));

		$this->set('registros', $users);
	}

	public function alertaAssinantes()
	{
		$this->autoRender = false;

		// Grupo 4 = Assinantes
		$conditions['AND']['role_id'] = 4;

		// Data de Expiração for diferente de null
		$conditions['AND']['data_expiracao !='] = null;

		// Data de Expiração MENOR ou IGUAL a daqui 1 dia, 7 dias ou 15 dias
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+1 day'));
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+7 days'));
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+15 days'));

		// Busca os assinantes que estão para expirar 15, 7 ou 1
		$assinantes = $this->User->find('all', array(
			'conditions' => $conditions,
			'recursive' => -1,
			'fields' => array(
				'id',
				'name',
				'email',
				'data_expiracao'
			)
		));

		// Se tem Assinantes
		if (!empty($assinantes)) {
			// Percorre os Assianntes para Enviar o E-mail
			foreach ($assinantes as $key => $assinante) {
				// Envia E-mail para o Assiante
				$this->_sendEmail(
					array(Configure::read('Site.title'), $this->_getSenderEmail()), // FROM
					$assinante['User']['email'], // TO
					__d('croogo', '[%s] Sua assinatura está perto de expirar', Configure::read('Site.title')), // SUBJECT
					'Users.assinante', // TEMPLATE
					'lembrete assinante', // EMAIL TYPE
					$this->theme, // THEME
					array( // viewVars
						'user' => $assinante,
						'url' => Router::url('/pagseguro/pagseguro/assinatura_pagamento?status=0', true),
					)
				);
			}
		}
	}

	public function admin_assinantes_expirar()
	{
		// Grupo 4 = Assinantes
		$conditions['AND']['role_id'] = 4;

		// Data de Expiração for diferente de null
		$conditions['AND']['data_expiracao !='] = null;

		// Data de Expiração MENOR ou IGUAL a daqui 1 dia, 7 dias ou 15 dias
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+1 day'));
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+7 days'));
		$conditions['OR'][]['data_expiracao'] = date('Y-m-d 00:00:00', strtotime('+15 days'));

		// Busca os assinantes que estão para expirar 15, 7 ou 1
		$assinantes = $this->User->find('all', array(
			'conditions' => $conditions,
			'recursive' => -1,
			'fields' => array(
				'id',
				'name',
				'email',
				'data_expiracao'
			),
			'order' => array(
				'data_expiracao' => 'ASC'
			)
		));

		$this->set('assinantes', $assinantes);
		$this->set('showActions', false);
	}

	public function admin_enviarEmailExpiracao($id)
	{
		$this->autoRender = false;

		$assinante = $this->User->find('first', array(
			'conditions' => array(
				'id' => $id
			),
			'recursive' => -1,
			'fields' => array(
				'id',
				'name',
				'email',
				'data_expiracao'
			)
		));

		// Envia E-mail para o Assiante
		$this->_sendEmail(
			array(Configure::read('Site.title'), $this->_getSenderEmail()), // FROM
			$assinante['User']['email'], // TO
			__d('croogo', '[%s] Sua assinatura está perto de expirar', Configure::read('Site.title')), // SUBJECT
			'Users.assinante', // TEMPLATE
			'lembrete assinante', // EMAIL TYPE
			$this->theme, // THEME
			array( // viewVars
				'user' => $assinante,
				'url' => Router::url('/pagseguro/pagseguro/assinatura_pagamento?status=0', true),
			)
		);

		$this->Session->setFlash('E-mail de alerta de expiração enviado!', 'flash', array('class' => 'alert alert-success'));
		return $this->redirect(array('action' => 'admin_index'));
	}

	public function admin_email_expiracao()
	{
		$this->loadModel('Settings.Setting');

		// Pegar o Template de E-mail
		if (!empty($this->request->data)) {
			$this->request->data['Setting']['key'] = 'Assinantes.emailTemplate';

			$this->Setting->create();
			// Salvar o Arquivo com os novos dados
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash('Modelo de E-mail salvo!', 'flash', array('class' => 'alert alert-success'));
			}
		}

		// Trazer o Dados do Template para Editar
		$this->request->data = $this->Setting->find('first', array(
			'conditions' => array(
				'key' => 'Assinantes.emailTemplate'
			),
			'recursive' => -1
		));
	}

	public function admin_enviarEmailExpiracaoEmMassa()
	{
		$this->autoRender = false;

		$assinantesEnviar = array();
		foreach ($this->data['User'] as $key => $value) {
			if ($key != 'checkAll') {
				if (isset($value['enviarEmail']) && $value['enviarEmail']) {
					$assinantesEnviar[$key] = $key;
				}
			}
		}

		if (!$this->data['User']['checkAll'] && empty($assinantesEnviar)) {
			$this->Session->setFlash(__d('croogo', 'É necessário selecionar ao menos um Assinante!'), 'flash', array('class' => 'alert alert-danger'));

			return $this->redirect($this->referer());
		}

		foreach ($assinantesEnviar as $key => $id) {
			$assinante = $this->User->find('first', array(
				'conditions' => array(
					'id' => $id
				),
				'recursive' => -1,
				'fields' => array(
					'id',
					'name',
					'email',
					'data_expiracao'
				)
			));

			// Envia E-mail para o Assiante
			$this->_sendEmail(
				array(Configure::read('Site.title'), $this->_getSenderEmail()), // FROM
				$assinante['User']['email'], // TO
				__d('croogo', '[%s] Sua assinatura está perto de expirar', Configure::read('Site.title')), // SUBJECT
				'Users.assinante', // TEMPLATE
				'lembrete assinante', // EMAIL TYPE
				$this->theme, // THEME
				array( // viewVars
					'user' => $assinante,
					'url' => Router::url('/pagseguro/pagseguro/assinatura_pagamento?status=0', true),
				)
			);
		}

		$this->Session->setFlash('E-mail de alerta de expiração enviado!', 'flash', array('class' => 'alert alert-success'));
		return $this->redirect($this->referer());
	}
}
