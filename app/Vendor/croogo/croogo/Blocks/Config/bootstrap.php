<?php

CroogoCache::config('croogo_blocks', array_merge(
	Configure::read('Cache.defaultConfig'),
	array('groups' => array('blocks'))
));

Croogo::hookComponent('*', array(
	'Blocks.Blocks' => array(
		'priority' => 5,
	)
));

Croogo::hookHelper('*', 'Blocks.Regions');

// Configure Wysiwyg
/*Croogo::mergeConfig('Wysiwyg.actions', array(
	'Blocks/admin_add' => array(
		array(
			'elements' => 'BlockBody',
		),
	),
	'Blocks/admin_edit' => array(
		array(
			'elements' => 'BlockBody',
		),
	),
	'Translate/admin_edit' => array(
		array(
			'elements' => 'BlockBody',
		),
	),
));*/

Croogo::mergeConfig('Translate.models.Block', array(
	'fields' => array(
		'title' => 'titleTranslation',
		'body' => 'bodyTranslation',
	),
	'translateModel' => 'Blocks.Block',
));
