<?php
$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb($title_for_layout, '/' . $this->request->url);


//lista as materias da capa
echo '<a href="/admin/nodes/nodes/add/noticia" class="btn btn-success"><i class="icon-plus"> </i> Nova Notícia</a>';
echo '<a href="/admin/nodes/nodes/index/capa:true" class="btn btn-info pull-right"><i class="icon-list"> </i> Listar Materias da Capa </a>';

echo '<div class="clearfix filter" style="margin-top: 10px;">';

	echo $this->Form->create('Node');

	echo $this->Form->input('titulo', array(
		'label' => 'Por título'
	));

	echo $this->Form->input('status', array(
		'label' => 'Publicado',
		'options' => array(
			'1' => 'Sim',
			'0' => 'Não',
		),
		'empty' => true,
	));

	echo $this->Form->input('promote', array(
		'label' => 'Destacado',
		'options' => array(
			'1' => 'Sim',
			'0' => 'Não',
		),
		'empty' => true
	));

	echo $this->Form->input('publish_start', array(
		'label' => 'Publicação',
		'type' => 'text',
		'class' => 'input-datetime hasDatepicker'
	));
	?>
	<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.input-datetime').datetimepicker({
				dateFormat: 'dd/mm/yy',
				timeFormat: 'hh:mm'
			})
		})
	</script>
	<?php
	//Busca os Termos
	$sql = 'SELECT id, title FROM terms ORDER BY title ASC;';
	$sqlResult = $this->Mswi->query($sql, 'listTermos');
	$arrayOptions = array();
	foreach($sqlResult as $termo){
		$arrayOptions[$termo['terms']['id']] = $termo['terms']['title'];
	}
	echo $this->Form->input('term_id', array(
		'label' => 'Termos',
		'options' => $arrayOptions,
		'empty' => true
	));

	//se o plugin de Editorias está ativo
	if(CakePlugin::loaded('Editorias')){
		//Busca as Editorias
		$sql = 'SELECT slug, title FROM editorias ORDER BY title ASC;';
		$sqlResult = $this->Mswi->query($sql, 'listEditorias');
		$arrayOptions = array();
		foreach($sqlResult as $termo){
			$arrayOptions[$termo['editorias']['slug']] = $termo['editorias']['title'];
		}
		echo $this->Form->input('editoria', array(
			'label' => 'Editorias',
			'options' => $arrayOptions,
			'empty' => true
		));
	}

	//Busca os Tipos
	$sql = 'SELECT alias, title FROM types ORDER BY title ASC;';
	$sqlResult = $this->Mswi->query($sql, 'listTypes');
	$arrayOptions = array();
	foreach($sqlResult as $termo){
		$arrayOptions[$termo['types']['alias']] = $termo['types']['title'];
	}
	echo $this->Form->input('type', array(
		'label' => 'Tipo',
		'options' => $arrayOptions,
		'empty' => false,
		'default' => Configure::read('Site.conteudo_padrao') ? Configure::read('Site.conteudo_padrao') : 'noticia'
	));
    echo $this->Form->input('dataInicial',
        array(
            'label' => 'Data Inicial',
            'class' => 'input text datetime2',
            'placeholder' => 'Data Inicial',
            'style' => 'width: 100px'
        )
    );
    echo $this->Form->input('dataFinal',
        array(
            'label' => 'Data Final',
            'class' => 'input text datetime2',
            'placeholder' => 'Data Final',
            'style' => 'width: 100px'
        )
    );

	echo $this->Form->input('Filtrar', array(
		'label' => false,
		'type' => 'submit',
		'class' => 'btn btn-warning input-block-level'
	));
	echo $this->Form->end();
echo '</div>';

//se foi solicitado apenas matérias da capa e não foi filtrado
if(isset($this->params['named']['capa']) && $this->params['named']['capa'] && !isset($this->request->data['Node']['promote'])){
	$this->request->data['Node']['promote'] = 1;
}

echo $this->Mswi->verMaisAjax('admin_index_ajax', $this->request->data, 1, 30);
?>

<script type="text/javascript" src="/details/js/jquery.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime2').datepicker({
            dateFormat: 'dd/mm/yy',
            timeFormat: 'hh:mm'
        })
        ;});
</script>