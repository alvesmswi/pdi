<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<rss version="2.0">
	<channel>
		<title>Mais notícias de Foz: www.h2foz.com.br</title>
		<description>Midiaindoor</description>
		<link/>
		<language>pt-br</language>
    <?php 
    if(!empty($nodes)){ 
        foreach($nodes as $node){ 
			$imgSrc = '';
			//se tem imagem
			if(isset($node['Multiattach'][0]['Multiattach']['filename'])){
				$imgSrc = Router::url('/', true).'fl/normal/'.$node['Multiattach'][0]['Multiattach']['filename'];
			}

			//se tem chapeu
			if(!empty($node['NoticiumDetail']['chapeu'])){
				$chapeu = $node['NoticiumDetail']['chapeu'];
			}else{
				$chapeu = $this->Mswi->categoryName($node['Node']['terms']);
			}			
			?>
            <item>
				<id><?php echo $node['Node']['id']; ?></id>
				<title><![CDATA[<?php echo $chapeu; ?>]]></title>
				<description><![CDATA[<?php echo $node['Node']['title']; ?>]]></description>
				<linkfoto><?php echo $imgSrc; ?></linkfoto>
				<creditfoto><![CDATA[<?php echo Configure::read('Site.title'); ?>]]></creditfoto>
				<pubdate><?php echo date('d/m/Y H:i:s', strtotime($node['Node']['publish_start'])).'.000'; ?></pubdate>
				<atualizacao><?php echo date('d/m/Y H:i:s', strtotime($node['Node']['updated'])).'.000'; ?></atualizacao>
			</item>
    <?php    
        }
    }
    ?>
	</channel>
</rss>