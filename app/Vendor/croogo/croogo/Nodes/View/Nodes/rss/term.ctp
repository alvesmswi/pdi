<?php
function rss_transform($item) {
	return array(
		'title' => $item['Node']['title'],
		'link' => Router::url($item['Node']['url'], true),
		'guid' => Router::url($item['Node']['url'], true),
		'description' => $item['Node']['excerpt'],
		'pubDate' => $item['Node']['publish_start']
	);
}

$this->set('items', $this->Rss->items($nodes, 'rss_transform'));
?>