<?php
#SEM ABAS: Se é uma notícia e está configurado para form simples sem aba
if($type['Type']['alias'] == 'noticia' && Configure::read('Site.noticia_form_simples')){
	echo $this->element('noticia_form_simples');

#FORM COMUM: Com abas
}else{
	$this->extend('/Common/admin_edit');

	$this->Croogo->adminScript('Nodes.admin');

	$this->Html
		->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
		->addCrumb(__d('croogo', 'Content'), array('controller' => 'nodes', 'action' => 'index'));

	//Se está adicionando
	if ($this->request->params['action'] == 'admin_add') {
		$formUrl = array('action' => 'add', $typeAlias);
		$this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'nodes', 'action' => 'create'));
		$created 		= date('d/m/Y H:i');
		$publish_start 	= date('d/m/Y H:i');
		$publish_end 	= '';
	}

	/*if ($this->request->params['action'] == 'admin_add') {
        $formUrl = array('action' => 'add', $typeAlias);
        $this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'nodes', 'action' => 'create'));
        $hora = date('H:i:s');
        $timestamp = strtotime($hora) - 60*60;
        $created        = date('d/m/Y H:i', $timestamp);
        $publish_start  = date('d/m/Y H:i', $timestamp);
        $publish_end    = '';
	}*/

	//Se está editando
	if ($this->request->params['action'] == 'admin_edit') {
		$formUrl = array('action' => 'edit');
		$this->Html->addCrumb($this->request->data['Node']['title'], '/' . $this->request->url);
		$created 		= date('d/m/Y H:i', strtotime($this->request->data['Node']['created']));
		$publish_start 	= date('d/m/Y H:i', strtotime($this->request->data['Node']['publish_start']));
		$publish_end 	= '';
		//se tem data de fim
		if(!empty($this->request->data['Node']['publish_end'])){
			$publish_end = date('d/m/Y H:i', strtotime($this->request->data['Node']['publish_end']));
		}
	}

	$this->Html->addCrumb($type['Type']['title'], array(
		'plugin' => 'nodes',
		'controller' => 'nodes',
		'action' => 'hierarchy',
		'?' => array(
			'type' => $type['Type']['alias'],
		),
	));

	$lookupUrl = $this->Html->apiUrl(array(
		'plugin' => 'users',
		'controller' => 'users',
		'action' => 'lookup',
	));

	$this->append('form-start', $this->Form->create('Node', array(
		'url' => $formUrl,
		'class' => 'protected-form',
	)));
	$inputDefaults = $this->Form->inputDefaults();
	$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

	$this->append('tab-heading');
		echo $this->Croogo->adminTab(__d('croogo', $type['Type']['title']), '#node-main');
		echo $this->Croogo->adminTabs();
	$this->end();

	$this->append('tab-content');

		echo $this->Html->tabStart('node-main');
			echo $this->Form->input('id');


			echo $this->Form->input('type', array(
				'type' => 'hidden',
				'value' => $type['Type']['alias']
			));

			//Se precisa selecionar um blog
			if(isset($blogs_lista)){
				echo $this->Form->input('blog_id', array(
					'label' => 'Blog / Coluna',
					'options' => $blogs_lista,
					'required' => 1
				));
			}

			echo $this->Form->input('title', array(
				'label' => __d('croogo', 'Title'),
			)) .
			$this->Form->input('slug', array(
				'class' => trim($inputClass . ' slug'),
				'label' => __d('croogo', 'Slug (URL do conteúdo)')
			)) .
			$this->Form->input('excerpt', array(
				'label' => 'Resumo (Gravata)',
			)) .
			$this->Form->input('body', array(
				'label' => __d('croogo', 'Body'),
				'class' => $inputClass . (!$type['Type']['format_use_wysiwyg'] ? ' no-wysiwyg' : ''),
			)) .
		$this->Html->tabEnd();

		echo $this->Croogo->adminTabs();

	$this->end();

	$username = isset($this->request->data['User']['name']) ? 	$this->request->data['User']['name'] : $this->Session->read('Auth.User.name');

	$this->start('panels');
		echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
			$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
			$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
			$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
			$this->Form->input('status', array(
				'legend' => false,
				'type' => 'radio',
				'default' => CroogoStatus::UNPUBLISHED,
				'options' => $this->Croogo->statuses(),
			));

			//se é uma noticia
			if($type['Type']['alias'] == 'noticia' || (isset($type['Params']['promote']) && $type['Params']['promote'])){

				if(Configure::read('Site.posicoes_capa')){
					$options['Destaques'] = array(
						1 => '1 - Super Destaque',
						2 => '2 - Destaque médio',
						3 => '3 - Destaque pequeno'
					);
					$a = 4;
					$posicoes = Configure::read('Site.posicoes_capa');
					$grupo = 'Outros Destaques';
					while ($a <= $posicoes) {
						$options[$grupo][$a] = $a;
						$a++;
						if($a >= 13){
							$grupo = 'Bloco central de Destaques';
						}
						if($a >= 19){
							$grupo = 'Lista de notícias inferiores';
						}
					}
					//pr($options);exit();
					echo $this->Form->input('ordem', array(
						'label' => 'Posição na Home (Ordenação)',
						'empty' => true,
						'options' => $options
					));
				}else{
					echo $this->Form->input('promote', array(
						'label' => 'Destacado na página inicial',
					));
					echo $this->Form->input('ordem', array(
					'type' => 'number',
					'label' => 'Posição da Home (Ordenação)',
					'value' => isset($this->data['Node']['ordem']) ? $this->data['Node']['ordem'] : 0
					));
				}


			}

			echo $this->Form->autocomplete('user_id', array(
				'type' => 'text',
				'label' => 'Autor',
				'autocomplete' => array(
					'default' => $username,
					'data-displayField' => 'name',
					'data-primaryKey' => 'id',
					'data-queryField' => 'name',
					'data-relatedElement' => '#NodeUserId',
					'data-url' => $lookupUrl
				),
			)) .

			$this->Html->div('input-daterange',
				$this->Form->input('publish_start', array(
					'label' => 'Início da publicação',
					'type' => 'text',
					'value' => $publish_start,
					'class' => 'input-datetime2',
					'style' => 'text-align:left;'
				)) .
				$this->Form->input('publish_end', array(
					'label' => 'Fim da publicação',
					'type' => 'text',
					'value' => $publish_end,
					'class' => 'input-datetime2',
					'style' => 'text-align:left;',
					'before' => '<i class="fa fa-calendar"></i>'
				))
			) .

			$this->Form->input('created', array(
				'type' => 'text',
				'label' => 'Criado em',
				'value' => $created,
				'readonly' => 'readonly'
			));

			$exclusivo = 0;
			if(isset($type['Params']['padrao_exclusivo']) && $type['Params']['padrao_exclusivo']){
				$exclusivo = 1;
			}
			echo $this->Form->input('exclusivo', array(
				'label' => 'Exclusivo para assinantes',
				'default' => $exclusivo
			));

			//Se está editando e é galeria
			if ($this->request->params['action'] == 'admin_edit' && $this->data['Node']['type'] == 'galeria') {
				$codigo = '[node:'.$this->data['Node']['type'].' id='.$this->data['Node']['id'].']';
				echo $this->Form->input('codigo', array(
					'type' => 'text',
					'label' => 'Código de incorporação',
					'value' => $codigo,
					'readonly' => 'readonly'
				));
			}

	?>
		<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.input-datetime2').datetimepicker({
					dateFormat: 'dd/mm/yy',
					timeFormat: 'hh:mm'
				})
			;});
		</script>

	<?php
		echo $this->Html->endBox();

		echo $this->Croogo->adminBoxes();

	$this->end();

	$this->append('form-end', $this->Form->end());
}
?>
