<?php 
//pr($nodes);
$page = $this->params->query['page'];
$proximaPagina = $page + 1;

if(!empty($nodes)){
    $today = date("Y-m-d H:i:s");
    
    echo '<table class="table table-stripped" style="margin:0;">';
    //se é a primeira página
    if($page == 1){
        echo '
        <thead>
            <th>'.$this->Paginator->sort('id','ID').'</th>
            <th>'.$this->Paginator->sort('title', 'Título').'</th>
            <th>'.$this->Paginator->sort('publish_start', 'Data').'</th>
            <th>'.$this->Paginator->sort('type', 'Tipo').'</th>
            <th>'.$this->Paginator->sort('user_id', 'Autor').'</th>
            <th>'.$this->Paginator->sort('status', 'Ativo').'</th>
            <th></th>
        </thead>';
    }
        
    echo '<tbody>';
        //percorre os nodes
        foreach($nodes as $key => $node){
           // pr($node);
            echo '<tr>';
                echo '<td>'.$node['Node']['id'].'</td>';
                echo '<td><a href="'.$node['Node']['path'].'" target="_blank">'.$node['Node']['title'].'</a>';
                    //Se tem Editoria
                    if(!empty($node['Editoria']['title'])) { 
                        echo ' <span class="label" style="background-color: #08c;" title="Editoria">'.$node['Editoria']['title'].'</span>';
                    }
                    //Se tem termos
                    if(!empty($node['Node']['terms'])) { 
                        echo ' <span class="label" style="background-color: #666;" title="Termo">'.$this->Mswi->categoryName($node['Node']['terms']).'</span>';
                    }
                    //Se é exclusivo
                    if($node['Node']['exclusivo']) { 
                        echo ' <span class="label" style="background-color: #468847;" title="Exclusivo para assinantes">$</span>';
                    }
                    //Se é destacado
                    if($node['Node']['promote']) { 
                        echo ' <span class="label" style="background-color: #F00;" title="Destacado">'.$node['Node']['ordem'].'</span>';
                    }
                    //Se está agendado
                    if($node['Node']['publish_start'] > $today) { 
                        echo ' <span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC2ElEQVQ4T11TbUhTYRQ+Z3Pf03SzUHHbVRckWSkGYkImwSQwtSX97OufIFJhqZRlGCn5UYjYv6x/BblUhNAfiYVOiVLUWNHQfVgK6jbXvufuG/fezanvj3t5z3nPOc/znHMQDpzXvwM6QkM5AjkDALlRt4kATiMPxq4fFY/vDcHYZcDkp5CHddIEqNDIeSKVnJ+qEKGM8TuCxGv3RDatHjro24FRQpO+G7kSC+NjE7w0+SkeHWlTyflFhYcFmhQxX4iE8RDuBWE/4AjSoW8bYavdE5mlefyW2lyJhU3wYt7VpUlMqDynkmqFCIhcFEjvvAdvTw1XhbUghAkhn+w+s+3fzsit/OQGbJ/Z0kkE2HsxJzFLKeEL2cLR5ykNg+DsquGQMGkIgwjBGdgJjZg9K/4wqcfmz2vd+Udk+lK1jDooaFrjIKx3XI6bdxUDmLR6LPMbPgPWjdmNV44r89RJQjmTnQPKFaPuG8DyVL8LIIaCoWhzBz3vfmwt4dWhZdftkswkiYDH5Y+iZf7HHhrgZ5t+D4WY7AT8YUKeT6+6sfrtL1djKZUkFvCi9eOICx5/OMiKvc89ugT+ME2eTVrceH5g0XizSJWXrZTK2a4hAgECxU+GwPigOqZntHSsrQDLDp/n1ax9CU/3f+8uyVHqq0+kU6zIQKCsYxgmmqpYxVlK7MSQ6DwwJgLDi+uWqeUtA2p7ZnQKiaC39qw2Ky1RLLzQOQwf71XFA2Mk4sMA6+5AqP+LecXpD9ez5oz2qa5TGYcqrxVnaxktmMHjJjEmGkeLsQXCEfLGuGJe+Ls98qe5pIFNkNw6QQFg28nMlCJ9gVqTniwR7u0I11yANZcvZJizWxdWnbMApMXVWsaNMnPETRNUJCFSpxSLKgophShfrUxVK+QyJtLm9HjnbY7Nr9bN4LY3NIo0vy/QURZfpn29ujuuA4RyQODWmaNiAoRpoGEMOnX71vk/xCYuHzc8B5YAAAAASUVORK5CYII=" title="Agendado para '.date('d/m/Y H:i', strtotime($node['Node']['publish_start'])).'"></span>';
                    }
                echo'</td>';
                echo '<td>'.date('d/m/Y H:i', strtotime($node['Node']['publish_start'])).'</td>';                
                echo '<td>'.$this->Mswi->typeName($node['Node']['type']).'</td>';
                echo '<td>'.$node['User']['name'].'</td>';
                echo '<td>';
                    echo $this->element('admin/toggle', array(
                        'id' => $node['Node']['id'],
                        'status' => (int)$node['Node']['status'],
                    ));
                echo '</td>';
                echo '<td>';
                    echo '<div class="item-actions">';
                        echo $this->Croogo->adminRowActions($node['Node']['id']);
                        echo ' ' . $this->Croogo->adminRowAction('',
                            array('action' => 'edit', $node['Node']['id']),
                            array(
                                'icon' => $this->Theme->getIcon('update'), 
                                'title' => 'Editar'
                            )
                        );

            /*  Se for usuário admin tem permissão visível  para deletar*/

            if($this->Session->read('Auth.User.Role.alias') === 'admin') {
                echo ' ' . $this->Croogo->adminRowAction('',
                        array('action' => 'delete', $node['Node']['id']),
                        array(
                            'icon' => $this->Theme->getIcon('delete'),
                            'class' => 'delete',
                            'tooltip' => __d('croogo', 'Remove this item'),
                            'title' => 'Deletar',
                        ),
                        __d('croogo', 'Are you sure?')
                    );
            }

                        //Se o plugin está ativo
                        if(CakePlugin::loaded('Onesignal')){
                            //se já foi configurado o OneSignal
                            if(Configure::read('Onesignal.oneSignalId') != '' && Configure::read('Onesignal.oneSignalKey') != ''){
                                //se já foi notificado
                                if($node['Node']['onesignal_push']){ ?>
                                    <a style="color:#ccc;" onclick="alert('Notícia já notificada.')"><i class="icon-comment-alt icon-large"></i></a>
                                <?php
                                }else{
                                    echo $this->Html->link(
                                        '<i class="icon-comment-alt icon-large" title="Notificar leitores (Push)"></i>',
                                        array('plugin' => 'onesignal', 'controller' => 'onesignal', 'action' => 'create', $node['Node']['id']),
                                        array('confirm' => 'Tem certeza que deseja notificar os inscritos desta notícia?')
                                    );
                                }
                            }else{ ?>
                                <a style="color:#ccc;" onclick="alert('O plugin de notificações ainda não foi configurado! Entre em contato com a MSWI.')"><i class="icon-comment-alt icon-large"></i></a>
                            <?php }
                        }

                        $urlSite = Router::url('/', true);
                        $urlSite = str_replace('http:', 'https:', $urlSite);
                        $urlSite = str_replace('mswi.', '', $urlSite);

                        $urlTwitter = "https://twitter.com/intent/tweet?text=".$node['Node']['title'].'&url='.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'];
                        echo ' ' . $this->Croogo->adminRowAction('',
                            'twitter',
                            array(
                                'icon' => $this->Theme->getIcon('twitter'),
                                'tooltip' => ('Publicar esta Notícia no Twitter'),
                                'target' => 'blank',
                                'onclick' => 'window.open("'.$urlTwitter.'","popup","width=600,height=600"); return false;',
                                'title' => 'Compartilhar no Twitter'
                            )
                        );

                        $urlFace = 'https://www.facebook.com/dialog/share?app_id=140586622674265&href='.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'].'&picture=&title='.$node['Node']['title'].'&description=&redirect_uri=';
                        echo ' ' . $this->Croogo->adminRowAction('',
                            'facebook',
                            array(
                                'icon' => $this->Theme->getIcon('facebook'),
                                'tooltip' => ('Publicar esta Notícia no Facebook'),
                                'target' => 'blank',
                                'onclick' => 'window.open("'.$urlFace.'","popup","width=600,height=600"); return false;',
                                'title' => 'Compartilhar no Facebook'
                            )
                        );

                        echo ' ' . $this->Croogo->adminRowAction('',
                            'copyurl',
                            array(
                                'icon' => $this->Theme->getIcon('link'),
                                'tooltip' => ('Copiar a URL'),
                                'onclick' => 'copyUrl("inputUrl'.$node['Node']['id'].'"); return false;',
                                'url' => '#',
                                'title' => 'Copiar URL'
                            )
                        );
                        echo '<input 
                            type="text" 
                            id="inputUrl'.$node['Node']['id'].'" 
                            value="'.$urlSite.$node['Node']['type'].'/'.$node['Node']['slug'].'" 
                            readonly="true" 
                            style="width:1px;height:0;padding:0;border:0;"
                            />';
                    
                    echo '</div>';
                echo '</td>';
            echo '</tr>';
        }
        echo '
        </tbody>
    </table>';
?>

<script>
    function copyUrl(inputUrl) {
        var urlToCopy = document.getElementById(inputUrl);
        urlToCopy.select();
        document.execCommand("copy");
        alert("URL Copiada! " + urlToCopy.value);
    }
</script>

<?php    
    //se tem algum registro
    if(!empty($nodes)){
        $data = json_decode($this->params->query['slug'], true);
        echo $this->Mswi->verMaisAjax('admin_index_ajax', $data, $proximaPagina, 30);
    }
}else{
    //se está na prmieira página
    if($page == 1){
        echo '<br /><div class="alert">Renhum registro encontrado.</div>';
    }    
}
?>