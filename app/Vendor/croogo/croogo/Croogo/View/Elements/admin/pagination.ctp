<div class="pagination">
	<p>
	<?php
		echo $this->Paginator->counter(array(
			'format' => __d('croogo', 'Página {:page} de {:pages}, exibindo {:current} registros de {:count} total, registro iniciado em {:start}, terminando em {:end}'),
		));
	?>
	</p>
	<ul>
		<?php echo $this->Paginator->first('< ' . __d('croogo', 'primeiro')); ?>
		<?php echo $this->Paginator->prev('< ' . __d('croogo', 'anterior')); ?>
		<?php echo $this->Paginator->numbers(); ?>
		<?php echo $this->Paginator->next(__d('croogo', 'próximo') . ' >'); ?>
		<?php echo $this->Paginator->last(__d('croogo', 'último') . ' >'); ?>
	</ul>
</div>
