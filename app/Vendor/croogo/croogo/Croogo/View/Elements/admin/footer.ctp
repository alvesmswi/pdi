<footer class="navbar-inverse">
	<div class="navbar-inner">

	<div class="footer-content">
	<?php
		$link = $this->Html->link(
			__d('croogo', 'Mswi Soluções Web %s', strval(Configure::read('Croogo.version'))),
			'http://www.mswi.com.br'
		);
	?>
	<?php echo __d('croogo', '%s', $link); ?>
	<?php echo $this->Html->image('//assets.croogo.org/powered_by.png'); ?>
	</div>

	</div>
</footer>