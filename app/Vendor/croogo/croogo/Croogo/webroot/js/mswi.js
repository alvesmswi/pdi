$(document).ready(function() {
	/*$('#NodeAddForm, #NodeEditForm').on('submit', function(e) {
		if ($('#NodeType').val() === 'noticia') {
			var order = +$('#NodeOrdem').val();
			var promote = $('#NodePromote').is(":checked");

			if (promote && order <= 0) {
				e.preventDefault();
				//alert('Notícias promovidas para a página principal precisam ter ordem maior que 0.');
			} else if (!promote && order > 0) {
				e.preventDefault();
				//alert('Notícias com ordem maior que 0 devem ser promovidas para a página principal.');
			}
		}
	});*/
	/*
	Quando submeter um form com esta classe, vai exibir o overlay
	*/
	$('.form-loading').on('submit', function(e) {
		$('#divLoading').show();		
	});

	//Mascaras
    $('.cnpj').mask('99.999.999/9999-99', {reverse: true});
	  $('.date').mask('99/99/9999',{placeholder:"mm/dd/yyyy",reverse: true});
  	$('.time').mask('99:99:99', {reverse: true});
  	$('.date_time').mask('99/99/9999 99:99:99', {reverse: true});
  	$('.cep').mask('99999-999', {reverse: true});
  	$('.cpf').mask('999.999.999-99', {reverse: true});
    $('.money').maskMoney({
      'decimal': ',',
      'thousands': '.'
    });
    $('.fone')
    .mask("(99) 9999-9999?9")
    .focusout(function (event) {  
        var target, phone, element;  
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
        phone = target.value.replace(/\D/g, '');
        element = $(target);  
        element.unmask();  
        if(phone.length > 10) {  
            element.mask("(99) 99999-999?9");  
        } else {  
            element.mask("(99) 9999-9999?9");  
        }  
    });
});
