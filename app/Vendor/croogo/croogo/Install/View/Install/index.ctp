<div class="install">
	<h2><?php echo $title_for_layout; ?></h2>
	<?php
		$check = true;

		// tmp is writable
		if (is_writable(TMP)) {
			echo '<p class="success">' . __d('croogo', 'Você tem permissão para alterar a pasta TMP') . '</p>';
		} else {
			$check = false;
			echo '<p class="error">' . __d('croogo', 'Você NÃO tem permissão para alterar a pasta TMP') . '</p>';
		}

		// config is writable
		if (is_writable(APP . 'Config')) {
			echo '<p class="success">' . __d('croogo', 'Você tem permissão para alterar a pasta Config.') . '</p>';
		} else {
			$check = false;
			echo '<p class="error">' . __d('croogo', 'Você NÃO tem permissão para alterar a pasta Config.') . '</p>';
		}

		// php version
		$minPhpVersion = '5.3.10';
		$operator = '>=';
		if (version_compare(phpversion(), $minPhpVersion, $operator)) {
			echo '<p class="success">' . sprintf(__d('croogo', 'PHP versão %s %s %s'), phpversion(), $operator, $minPhpVersion) . '</p>';
		} else {
			$check = false;
			echo '<p class="error">' . sprintf(__d('croogo', 'PHP versão %s < %s'), phpversion(), $minPhpVersion) . '</p>';
		}

		// cakephp version
		$minCakeVersion = '2.5.4';
		$cakeVersion = Configure::version();
		$operator = '>=';
		if (version_compare($cakeVersion, $minCakeVersion, $operator)) {
			echo '<p class="success">' . __d('croogo', 'CakePhp versão %s %s %s', $cakeVersion, $operator, $minCakeVersion) . '</p>';
		} else {
			$check = false;
			echo '<p class="error">' . __d('croogo', 'CakePHP versão %s < %s', $cakeVersion, $minCakeVersion) . '</p>';
		}

?>
</div>
<?php
if ($check) {
	$out = $this->Html->link(__d('croogo', 'Instalar'), array(
		'action' => 'database',
	), array(
		'button' => 'success',
		'tooltip' => array(
			'data-title' => __d('croogo', 'Clique para iniciar a instalação'),
			'data-placement' => 'left',
		),
	));
} else {
	$out = '<p>' . __d('croogo', 'A instalação não pode continuar por não anteder as requisições mínimas') . '</p>';
}
echo $this->Html->div('form-actions', $out);
?>
