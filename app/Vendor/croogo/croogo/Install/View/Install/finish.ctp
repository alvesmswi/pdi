<div class="install">
	<h2><?php echo $title_for_layout; ?></h2>

<?php if ($installed): ?>
	<p class="success">
	<?php echo __d('croogo', '
		Usuário %s criado com permissões administrativas.',
		sprintf('<strong>%s</strong>', $user['User']['username']));
	?>
	</p>

	<p>
		<?php echo __d('croogo', 'Painel Administrativo: %s', $this->Html->link(Router::url('/admin', true), Router::url('/admin', true))); ?>
	</p>

	<p>
	<?php
	echo __d('croogo', 'Você pode começar %s ou %s.',
		$this->Html->link(__d('croogo', 'Configurando o seu site'), $urlSettings),
		$this->Html->link(__d('croogo', 'Criando uma nova página'), $urlBlogAdd)
		);
	?>
	</p>
<?php endif; ?>

	<blockquote>
		<h3><?php echo __d('croogo', 'Recursos'); ?></h3>
		<ul class="bullet">
			<li><?php echo $this->Html->link('Mswi Soluções Web', 'http://www.mswi.com.br'); ?></li>
		</ul>
	</blockquote>
	&nbsp;
</div>