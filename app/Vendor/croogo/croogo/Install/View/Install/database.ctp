<?php
echo $this->Form->create(false, array(
	'url' => array(
		'plugin' => 'install',
		'controller' => 'install',
		'action' => 'database'
	),
	'inputDefaults' => array(
		'class' => 'span11',
	),
), array(
	'class' => 'inline',
));
?>
<div class="install">
	<h2><?php echo $title_for_layout; ?></h2>

	<?php if ($currentConfiguration['exists']):  ?>
		<div class="alert alert-warning">
			<strong><?php echo __d('croogo', 'Atenção'); ?>:</strong>
			<?php echo __d('croogo', 'O arquivo `database.php` já existe.'); ?>
			<?php
			if ($currentConfiguration['valid']):
				$valid = __d('croogo', 'Valido');
				$class = 'text-success';
			else:
				$valid = __d('croogo', 'Invalido');
				$class = 'text-error';
			endif;
			echo __d('croogo', 'Este arquivo é %s.', $this->Html->tag('span', $valid, compact('class')));
			?>
			<?php if ($currentConfiguration['valid']): ?>
			<?php
				echo $this->Html->link(
					__d('croogo', 'Reutilizar este arquivo e continuar.'),
					array('action' => 'data')
				);
			?>
			<?php else: ?>
				<?php echo __d('croogo', 'Este arquivo será substituído'); ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php
		echo $this->Form->input('datasource', array(
			'placeholder' => __d('croogo', 'Database'),
			'default' => 'Database/Mysql',
			'empty' => false,
			'options' => array(
				'Database/Mysql' => 'mysql',
				//'Database/Sqlite' => 'sqlite',
				//'Database/Postgres' => 'postgres',
				//'Database/Sqlserver' => 'mssql',
			),
		));
		echo $this->Form->input('host', array(
			'placeholder' => __d('croogo', 'Host'),
			'default' => 'localhost',
			'tooltip' => __d('croogo', 'Hostname ou IP'),
			'before' => '<span class="add-on"><i class="icon-home"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
			'required' => true,
		));
		echo $this->Form->input('login', array(
			'placeholder' => __d('croogo', 'Login'),
			'default' => 'root',
			'tooltip' => __d('croogo', 'Login/username do Banco de Dados'),
			'before' => '<span class="add-on"><i class="icon-user"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
			'required' => true,
		));
		echo $this->Form->input('password', array(
			'placeholder' => __d('croogo', 'Senha'),
			'tooltip' => __d('croogo', 'Senha do Banco de Dados'),
			'before' => '<span class="add-on"><i class="icon-key"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
		));
		echo $this->Form->input('database', array(
			'placeholder' => __d('croogo', 'Banco'),
			'tooltip' => __d('croogo', 'Nome do Banco de Dados'),
			'before' => '<span class="add-on"><i class="icon-briefcase"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
			'required' => true,
		));
		echo $this->Form->input('prefix', array(
			'placeholder' => __d('croogo', 'Prefixo'),
			'tooltip' => __d('croogo', 'Prefixo das tabelas (opcional)'),
			'before' => '<span class="add-on"><i class="icon-minus"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
		));
		echo $this->Form->input('port', array(
			'placeholder' => __d('croogo', 'Porta'),
			'tooltip' => __d('croogo', 'Porta (opcional)'),
			'before' => '<span class="add-on"><i class="icon-asterisk"></i></span>',
			'div' => 'input input-prepend',
			'label' => false,
		));
	?>
</div>
<div class="form-actions">
	<?php echo $this->Form->submit('Próximo', array('button' => 'success', 'div' => 'input submit')); ?>
</div>
<?php echo $this->Form->end(); ?>