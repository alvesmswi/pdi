<div class="install">
	<h2><?php echo $title_for_layout; ?></h2>

	<p>
	<?php
	echo __d('croogo', 'Criar tabelas e carregar os dados iniciais');
	?>
	</p>
</div>
<div class="form-actions">
<?php
echo $this->Html->link(__d('croogo', 'Criar tabelas'), array(
	'plugin' => 'install',
	'controller' => 'install',
	'action' => 'data',
	'?' => array('run' => 1),
), array(
	'tooltip' => array(
		'data-title' => __d('croogo', 'Clique aqui para construir seu banco de dados'),
		'data-placement' => 'left',
	),
	'button' => 'success',
	'icon' => 'none',
	'onclick' => '$(this).attr(\'disabled\', \'disabled\').find(\'i\').addClass(\'icon-spin icon-spinner\');',
));
?>
</div>