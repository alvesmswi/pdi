<?php
echo $this->Form->create(null, array(
	'url' => array('controller' => 'install', 'action' => 'adminuser'),
	'inputDefaults' => array(
		'class' => 'span11',
	),
));
?>
<div class="install">
	<h2><?php echo __d('croogo', 'Passo 3: Criar um Admin'); ?></h2>
	<?php
		echo $this->Form->input('User.username', array(
			'placeholder' => __d('croogo', 'Username'),
			'before' => '<span class="add-on"><i class="icon-user"></i></span>',
			'div' => 'input text input-prepend',
			'label' => false,
		));
		echo $this->Form->input('User.password', array(
			'placeholder' => __d('croogo', 'Nova Senha'),
			'value' => '',
			'before' => '<span class="add-on"><i class="icon-key"></i></span>',
			'div' => 'input password input-prepend',
			'label' => false,
		));
		echo $this->Form->input('User.verify_password', array(
			'placeholder' => __d('croogo', 'Repita a senha'),
			'type' => 'password',
			'value' => '',
			'before' => '<span class="add-on"><i class="icon-key"></i></span>',
			'div' => 'input password input-prepend',
			'label' => false,
		));
	?>
</div>
<div class="form-actions">
	<?php echo $this->Form->submit(__d('croogo', 'Salvar'), array('class' => 'btn btn-success', 'div' => false)); ?>
	<?php echo $this->Html->link(__d('croogo', 'Cancelar'), array('action' => 'index'), array( 'class' => 'btn cancel')); ?>
</div>
<?php
	echo $this->Form->end();
?>