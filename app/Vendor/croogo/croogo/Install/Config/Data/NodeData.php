<?php
class NodeData {

	public $table = 'nodes';

	public $records = array(
		array(
			'id' => '1',
			'parent_id' => '',
			'user_id' => '1',
			'title' => 'Bem vindo!',
			'slug' => 'bem-vindo',
			'body' => '<p>Bem vindo ao seu novo website criado pelo CMS da <a href="http://www.mswi.com.br" target="_blank">Mswi Soluções Web Inteligentes</a></p>',
			'excerpt' => '',
			'status' => '1',
			'mime_type' => '',
			'comment_status' => '2',
			'comment_count' => '1',
			'promote' => '1',
			'path' => '/blog/bem-vindo',
			'terms' => '{"1":"uncategorized"}',
			'sticky' => '0',
			'lft' => '1',
			'rght' => '2',
			'visibility_roles' => '',
			'type' => 'blog',
			'updated' => '2009-12-25 11:00:00',
			'created' => '2009-12-25 11:00:00'
		),
		array(
			'id' => '2',
			'parent_id' => '',
			'user_id' => '1',
			'title' => 'Sobre',
			'slug' => 'sobre',
			'body' => '<p>Este é um exemplo de página, coloque aqui as informações sobre o seu website.</p>',
			'excerpt' => '',
			'status' => '1',
			'mime_type' => '',
			'comment_status' => '0',
			'comment_count' => '0',
			'promote' => '0',
			'path' => '/sobre',
			'terms' => '',
			'sticky' => '0',
			'lft' => '1',
			'rght' => '2',
			'visibility_roles' => '',
			'type' => 'page',
			'updated' => '2009-12-25 22:00:00',
			'created' => '2009-12-25 22:00:00'
		),
	);

}
