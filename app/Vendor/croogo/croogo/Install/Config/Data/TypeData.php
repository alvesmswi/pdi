<?php
class TypeData {

	public $table = 'types';

	public $records = array(
		array(
			'id' => '1',
			'title' => 'Página',
			'alias' => 'page',
			'description' => 'Uma página é um método simples para criar e exibir informação que raramente muda, como uma seção "Sobre nós" de um site. Por padrão, uma entrada de página não permite comentários de visitantes.',
			'format_show_author' => '0',
			'format_show_date' => '0',
			'comment_status' => '0',
			'comment_approve' => '1',
			'comment_spam_protection' => '0',
			'comment_captcha' => '0',
			'params' => '',
			'plugin' => null,
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
		array(
			'id' => '2',
			'title' => 'Blog',
			'alias' => 'blog',
			'description' => 'Um blog é um único post para um jornal online, ou blog.',
			'format_show_author' => '1',
			'format_show_date' => '1',
			'comment_status' => '2',
			'comment_approve' => '1',
			'comment_spam_protection' => '0',
			'comment_captcha' => '0',
			'params' => '',
			'plugin' => null,
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
		array(
			'id' => '4',
			'title' => 'Node',
			'alias' => 'node',
			'description' => 'Tipo de conteúdo padrão',
			'format_show_author' => '1',
			'format_show_date' => '1',
			'comment_status' => '2',
			'comment_approve' => '1',
			'comment_spam_protection' => '0',
			'comment_captcha' => '0',
			'params' => '',
			'plugin' => null,
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
	);

}
