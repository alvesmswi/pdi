<?php
class CommentData {

	public $table = 'comments';

	public $records = array(
		array(
			'id' => '1',
			'parent_id' => '',
			'model' => 'Node',
			'foreign_key' => '1',
			'user_id' => '0',
			'name' => 'Sr Mswi',
			'email' => 'email@examplo.com',
			'website' => 'http://www.mswi.com.br',
			'ip' => '127.0.0.1',
			'title' => '',
			'body' => 'Oi, este é o primeiro comentário.',
			'rating' => '',
			'status' => '1',
			'notify' => '0',
			'type' => 'blog',
			'comment_type' => 'comment',
			'lft' => '1',
			'rght' => '2',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
	);

}
