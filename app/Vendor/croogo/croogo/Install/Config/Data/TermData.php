<?php
class TermData {

	public $table = 'terms';

	public $records = array(
		array(
			'id' => '1',
			'title' => 'Sem categoria',
			'slug' => 'sem-categoria',
			'description' => '',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
		array(
			'id' => '2',
			'title' => 'Avisos',
			'slug' => 'avisos',
			'description' => '',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
		array(
			'id' => '3',
			'title' => 'minha-tag',
			'slug' => 'minha-tag',
			'description' => '',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
	);

}
