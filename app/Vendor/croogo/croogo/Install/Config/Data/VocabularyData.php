<?php
class VocabularyData {

	public $table = 'vocabularies';

	public $records = array(
		array(
			'id' => '1',
			'title' => 'Categorias',
			'alias' => 'categorias',
			'description' => '',
			'required' => '0',
			'multiple' => '1',
			'tags' => '0',
			'plugin' => null,
			'weight' => '1',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
		array(
			'id' => '2',
			'title' => 'Tags',
			'alias' => 'tags',
			'description' => '',
			'required' => '0',
			'multiple' => '1',
			'tags' => '0',
			'plugin' => null,
			'weight' => '2',
			'updated' => '2016-08-01 12:00:00',
			'created' => '2016-08-01 12:00:00'
		),
	);

}
