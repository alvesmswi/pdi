<?php
App::uses('AppHelper', 'View/Helper');
App::uses('StringConverter', 'Croogo.Lib/Utility');

class MswiHelper extends AppHelper {
	
	public $helpers = array(
		'Html',
		'Form',
		'Menus',
		'Js'
	);
	
	public function linkToUrl($link) {
		// if link is in the format: controller:contacts/action:view
		if (strstr($link, 'controller:')) {
			return $this->Menus->linkStringToArray($link);
		}else{
			return $link;
		}
	}
	
	public function dateFormat($format = '%A, %d de %B de %Y', $date = 'Y-m-d H:i:s'){
		setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
		date_default_timezone_set( 'America/Sao_Paulo' );
		return utf8_encode(strftime($format, strtotime(date($date))));
	}
	
	public function sitesList($options = array()){
		$_options = array(
			'plugin' => 'sites',
			'controller' => 'sites',
			'action' => 'admin_index',
		);
		$options = array_merge($_options, $options);
		$output = '';
		//if (isset($this->_View->viewVars['nodes_for_layout'][$alias])) {
			pr($this->_View->viewVars);exit();
			//$nodes = $this->_View->viewVars['nodes_for_layout']['node'];
			$output = $this->_View->element($options['element'], array(
				'alias' => 'node',
				'nodesList' => $this->_View->viewVars['nodes_for_layout']['node'],
				'options' => $options,
			));
		//}
		return $output;
	}

	/**
	 * Get terms string and return last name term
	 */
	public function categoryName($term) 
	{
		App::import('Model', 'Taxonomy.Term');
		$Term = new Term();
		$Term->recursive = -1;

		$objTerm = json_decode($term);

		$nameTerm = null;
		if (!empty($objTerm)) {
			foreach ($objTerm as $id => $name) {
				$term = $Term->findById($id, ['title']);
				if(isset($term['Term'])){
					$nameTerm = $term['Term']['title'];
				}else{
					$nameTerm = 'Não informado';
				}
			}
		}

		return $nameTerm;
	}

	public function categorySlug($term) 
	{
		$objTerm = json_decode($term);

		$nameTerm = null;
		if (!empty($objTerm)) {
			foreach ($objTerm as $id => $name) {
				$nameTerm = $name;
			}
		}

		return $nameTerm;
	}

	public function categoryColor($term) 
	{
		App::import('Model', 'Taxonomy.Term');
		$Term = new Term();
		$Term->recursive = -1;

		$objTerm = json_decode($term);

		$colorTerm = 'background-color: #333 !important;';
		if (!empty($objTerm)) {
			foreach ($objTerm as $id => $name) {
				$term = $Term->findById($id, ['title', 'description']);
				if(isset($term['Term']['description']) && !empty(trim($term['Term']['description']))){
					$colorTerm = 'background-color: '.trim($term['Term']['description']).' !important;';
				}
			}
		}

		return $colorTerm;
	}

	/**
	 * Get type string and return name type
	 */
	public function typeName($type) 
	{
		App::import('Model', 'Type');
		$Type = new Type();
		$Type->recursive = -1;

		$nameType = null;
		$typeFind = $Type->findByAlias($type, ['title']);
		if (!empty($typeFind)) {
			$nameType = $typeFind['Type']['title'];
		}

		return $nameType;
	}

	/**
	 * Get slug string and return name slug
	 */
	public function slugName($slug) 
	{
		App::import('Model', 'Taxonomy.Term');
		$Term = new Term();
		$Term->recursive = -1;

		$nameSlug = null;
		$slugFind = $Term->findBySlug($slug, ['title']);
		if (!empty($slugFind)) {
			$nameSlug = $slugFind['Term']['title'];
		}

		return $nameSlug;
	}

	/**
	 * Get category id and return info
	 */
	public function categoryInfo($termId) 
	{
		App::import('Model', 'Taxonomy.Term');
		$Term = new Term();
		$Term->recursive = -1;

		$termFind = $Term->findById($termId);
		if (!empty($termFind)) {
			$arrayCapas = explode(';', $termFind['Term']['description']);
			if (!empty($arrayCapas[0])) {
				foreach ($arrayCapas as $key => $capa) {
					preg_match("/(\S+)=(\S+)$/", $capa, $nova_capa);
					$termFind['Term']['capas'][$nova_capa[1]] = $nova_capa[2];
				}
			}

			return $termFind;
		}

		return false;
	}

	/**
	 * Get vocabulary alias and return
	 */
	public function vocabulary($alias) 
	{
		App::import('Model', 'Vocabulary');
		$Vocabulary = new Vocabulary();
		$Vocabulary->contain('Taxonomy');

		$vocabularyFind = $Vocabulary->findByAlias($alias);
		if (!empty($vocabularyFind)) {
			App::import('Model', 'Node');
			$Node = new Node();

			foreach ($vocabularyFind['Taxonomy'] as $key => $taxonomy) {
				$info = $this->categoryInfo($taxonomy['term_id']);
				$conditions = array($Node->escapeField('terms') . ' LIKE' => '%"' . $info['Term']['slug'] . '"%');
				$node = $Node->find(
					'first', 
					array(
						'conditions' => $conditions, 
						'order' => array('Node.publish_start DESC'),
						'fields' => array('Node.title', 'Node.slug', 'Node.type')
					)
				);

				$vocabularyFind['Taxonomy'][$key]['Term'] = $info['Term'];
				$vocabularyFind['Taxonomy'][$key]['Node'] = $node['Node'];
			}
			
			return $vocabularyFind;
		}

		return false;
	}

	public function weatherText($code = null)
	{
		switch ($code) {
			case '19':
			case '20':
			case '21':
			case '22':
			case '25':
			case '26':
			case '44':
				$textTemp = 'Nublado';
				break;
			
			case '3':
			case '4':
			case '37':
			case '38':
			case '39':
			case '45':
			case '47':
				$textTemp = 'Chuva forte';
				break;
			
			case '29':
			case '30':
			case '3200':
				$textTemp = 'Poucas nuvens';
				break;
			
			case '9':
			case '11':
			case '12':
			case '40':
				$textTemp = 'Chovendo';
				break;
			
			case '5':
			case '6':
			case '7':
			case '8':
			case '10':
			case '35':
				$textTemp = 'Neve com chuva';
				break;

			case '13':
			case '14':
			case '15':
			case '16':
			case '17':
			case '18':
			case '41':
			case '42':
			case '43':
			case '46':
				$textTemp = 'Nevando';
				break;

			case '27':
			case '28':
				$textTemp = 'Encoberto';
				break;

			case '31':
			case '32':
			case '33':
			case '34':
			case '36':
				$textTemp = 'Ensolarado';
				break;

			case '0':
			case '1':
			case '2':
			case '23':
			case '24':
				$textTemp = 'Vendaval';
				break;
			
			default:
				$textTemp = 'Não disponível';
				break;
		}

		return $textTemp;
	}

	public function weatherDay($day = null)
	{
		switch ($day) {
			case 'Mon':
				$textDay = 'Seg';
				break;
			
			case 'Tue':
				$textDay = 'Ter';
				break;

			case 'Wed':
				$textDay = 'Qua';
				break;

			case 'Thu':
				$textDay = 'Qui';
				break;

			case 'Fri':
				$textDay = 'Sex';
				break;

			case 'Sat':
				$textDay = 'Sab';
				break;

			case 'Sun':
				$textDay = 'Dom';
				break;
			
			default:
				$textDay = 'Segunda';
				break;
		}

		return $textDay;
	}

	public function weatherClass($code = null)
	{
		switch ($code) {
			case '19':
			case '20':
			case '21':
			case '22':
			case '25':
			case '26':
			case '44':
				$class = 'cloudy';
				break;
			
			case '3':
			case '4':
			case '37':
			case '38':
			case '39':
			case '45':
			case '47':
				$class = 'lightning';
				break;
			
			case '29':
			case '30':
			case '3200':
				$class = 'normal';
				break;
			
			case '9':
			case '11':
			case '12':
			case '40':
				$class = 'raining';
				break;
			
			case '5':
			case '6':
			case '7':
			case '8':
			case '10':
			case '35':
				$class = 'snow-rain';
				break;

			case '13':
			case '14':
			case '15':
			case '16':
			case '17':
			case '18':
			case '41':
			case '42':
			case '43':
			case '46':
				$class = 'snowing';
				break;

			case '27':
			case '28':
				$class = 'sun-clouds';
				break;

			case '31':
			case '32':
			case '33':
			case '34':
			case '36':
				$class = 'sunny';
				break;

			case '0':
			case '1':
			case '2':
			case '23':
			case '24':
				$class = 'windy';
				break;
			
			default:
				$class = 'sunny';
				break;
		}

		return $class;
	}

	function tratarData($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('/', '-', $valor);
			$resultValor = date('Y-m-d', strtotime($valorReplace));
		}else{
			$resultValor = date('d/m/Y', strtotime($valor));
		}
		return $resultValor;
	}

	function tratarValor($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('.', '', $valor);
			$valorReplace = str_replace(',', '.', $valorReplace);
			$resultValor = $valorReplace;
		}else{
			$resultValor = number_format($valor,2,',','.');
		}
		return $resultValor;
	}

	function diasAtras($timestamp) {
        $passou = '';
        $segundo = time() - $timestamp;
        $minuto = (int)($segundo / 60);
        $hora = (int)($minuto / 60);
        $dia = (int)($hora / 24);
        $meses = (int)($dia / 30);
        $ano = (int)($dia / 365);
        
        if ($ano >= 1) {
            $passou = 'há ' .$ano . ($ano != 1 ? ' anos' : ' ano');
        } else if ($meses >= 1) {
            $passou = 'há ' .$meses . ($ano != 1 ? ' meses' : ' mes');
        } else if ($dia >= 1) {
            $passou = 'há ' .$dia . ($dia != 1 ? ' dia' : ' dia');
        } else if ($hora >= 1) {
            $how_log_ago = $hora . ($hora != 1 ? ' horas' : ' hora');
        } else if ($minuto >= 1) {
            $passou = 'há ' .$minuto . ($minuto != 1 ? ' minutos' : ' minutos');
        } else {
            $passou = 'há ' .$segundo . ($segundo != 1 ? ' segundos' : ' segundo');
        }
        return $passou;
    }
	
	function removeTagsWP($string){
		$var = preg_replace ("/\[.+?\]/", "", $string);	
		return $var;
	}
	function limpaTags($string){
		$var = strip_tags($string, '<p><br><b><i><s><img><a><iframe>');
		return $var;
	}
	function removeStyle($string){
		$string = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $string);
		return $string;
	}

	function removeCacheImgs($string){
		$var = str_replace(array('.png','.jpg','.gif'), array('.png?123','.jpg?123','.gif?123'), $string);
		return $var;
	}

	public function correlatas($nodeId) 
	{
		App::import('Model', 'Nodes.Node');
		$Node = new Node();
		$arrayCorrelatas = $Node->relacionadas($nodeId);
		return $arrayCorrelatas;
	}
	
	function getCodigo($corpo){
		preg_match_all(
			"/\[.+?\]/",
			$corpo,
			$codigo
		);	
		if(!empty($codigo)){
			return $codigo[0][0];
		}
		return false;
	}

	public function lerCodigos($corpo){
		//trata uma possível string codificada
		$parseString = new StringConverter();
		$arrayNode = $parseString->parseString('node|n', $corpo);
		if(!empty($arrayNode)){
			foreach($arrayNode as $type => $node){
				$codigo = $this->getCodigo($corpo);
				$getHtml = $this->requestAction('nodes/nodes/view/'.$node['id'].'/true',array('return'));
				return str_replace($codigo, $getHtml, $corpo);
			}
		}else{
			return $corpo;
		}
	}

	public function query($sql, $cacheName, $tempo = '+1 minute') 
	{
		App::import('Model', 'Nodes.Node');
		$Node = new Node();
		
		$cacheName = Inflector::slug($cacheName);
		//Procura no cache
		$arrayResult = Cache::read($cacheName, 'home_');
		//Se não encontrou nada:
		if(!$arrayResult){
			//Configura o cache;
			$arrayResult = $Node->query($sql);
			Cache::write($cacheName, $arrayResult, 'home_');
		}
		return $arrayResult;
	}

	public function gerarKeywords($string){
		$result = '';
		$contador = 0;
		$string = substr($string,0,256);
		$string = strip_tags($string);
		$arrayString = explode(' ',$string);
		foreach($arrayString as $str){
			$contador++;
			$str = trim($str);
			if(strlen($str) > 2){
				$result .= $str;
				if($contador < count($arrayString)){
					$result .= ',';
				}
			}
			
		}
		return $result;
	}	

	function verMaisAjax($action, $slug, $page=1, $limit=10, $Jquery = '$'){
		//imagem de loading
		$imgLoading = '<br /><img src=/nodes/img/loading.gif class=center-block />';

		//se foi informado um array (data)
		if(is_array($slug)){
			$slug = json_encode($slug);
		}

		//se a página é 1, então é o primeiro load
		if($page == 1){
			//escreve a primeira div
			echo '<div id="conteudo"></div>';

			//Cria o primeiro load da página 
			$primeiroLoad = $this->Js->request(					   
				array(
					'plugin' => 'nodes',
					'controller' => 'nodes',
					'action' => $action
				),
				array(
					'data' => array(
						'slug' => $slug,
						'page' => $page,
						'limit' => $limit
					),
					'before' => $Jquery.'("#conteudo").append("'.$imgLoading.'");',
					'async' => true, 
					'update' => '#conteudo'
				)
			);

			//Gera o bloco de scripts
			return $this->Html->scriptBlock(
				//Quanto a página estiver pronta
				$this->Js->domReady(
					$primeiroLoad
				),
				//imprime no rodapé
				array('inline'=>false)
			);

		//Demais páginas:	
		}else{
			//escreve a div de veja mais
			echo '
			<div id="verMaisConteudo'.$page.'">
				<br />
				<button id="verMaisBtn'.$page.'" class="btn center-block" style="width:100%;">Carregar Mais</button>
			</div>';

			//cria o Ajax de veja mais
			$this->Js->get('#verMaisBtn'.$page);
			$this->Js->event(
				'click',
				$this->Js->request(
					array(
						'plugin' => 'nodes',
						'controller' => 'nodes',
						'action' => $action
					),
					array(
						'data' => array(
							'slug' => $slug,
							'page' => $page,
							'limit' => $limit
						),
						'before' => $Jquery.'("#verMaisBtn'.$page.'").hide();'.$Jquery.'("#verMaisConteudo'.$page.'").append("'.$imgLoading.'");',
						'async' => true, 
						'update' => '#verMaisConteudo'.$page
					)
				)
			);
		}
		
	}

	public function ehArquivo($mime){

		if($mime == 'application/pdf' || $mime == 'text/plain' || $mime == 'application/msword' 
		|| $mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
		|| $mime == 'application/vnd.ms-excel'){
			return true;
		}else{
			return false;
		}

	}
}