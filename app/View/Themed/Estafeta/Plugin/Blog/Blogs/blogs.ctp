<div class="row mb-40">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="section-title">
      <h2 class="h6 header-color inline-block uppercase"><?php echo $title_for_layout; ?></h2>
    </div>
  </div>
</div>

<?php  if (!empty($blogs)): ?>
  <div class="row">
    <div class="zm-posts clearfix">
      <?php foreach ($blogs as $blog): ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
          <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
            <div class="zm-post-thumb">
              <a href="/blog/<?php echo $blog['Blog']['slug']; ?>" data-dark-overlay="2.5"  data-scrim-bottom="9">
                <?php if (isset($blog['Blog']['image'])): ?>
                <div class="lazyload">
                    <!--
                        <img src="<?php echo $blog['Blog']['image'] ?>" alt="<?php echo $blog['Blog']['title']; ?>" style="width:300px;height:270px;">
                    -->
                </div>
                <?php else: ?>
                  <div class="lazyload">
                      <!--
                        <img src="/theme/<?php echo Configure::read('Site.theme')?>/images/post/trend/b/7.jpg" alt="<?php echo $blog['Blog']['title']; ?>">
                      -->
                  </div>
                <?php endif; ?>
              </a>
            </div>
            <div class="zm-post-dis text-white">
              <?php $url = ((isset($blog['Post']['url'])) ? $this->Html->url($blog['Post']['url']) : "/blog/{$blog['Blog']['slug']}/post/{$blog['Post']['slug']}"); ?>
              <h2 class="zm-post-title h3"><a href="<?php echo $url ?>"><?php echo $blog['Post']['title']; ?></a></h2>
              <div class="zm-post-meta">
                <ul>
                  <li class="s-meta">
                    <a href="/blog/<?php echo $blog['Blog']['slug']; ?>" class="zm-author"><?php echo $blog['Blog']['title']; ?></a>
                  </li>
                </ul>
              </div>
            </div>
          </article>
        </div>
      <?php endforeach; ?>
    </div>

    <?php echo $this->element('paginator'); ?>
  </div>
<?php endif; ?>
<div class="lazyload">
    <!--
        <?php
          //Se tem algum bloco ativo nesta região
          if($this->Regions->blocks('publicidade_lista')){
            echo $this->Regions->blocks('publicidade_lista');
          }
        ?>
    -->
</div>