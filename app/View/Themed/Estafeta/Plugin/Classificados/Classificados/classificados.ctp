<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/classificados.css">

<?php 
$this->start('classificados_header');
  echo '<div class="container pt-70 xs-pt-30">';
    echo $this->element('Classificados.big_banner'); 
  echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<?php foreach ($classificados as $key => $c) { ?>
  <?php if (!empty($c['Anuncio'])) { ?>
    <section class="s-list u-spacer_bottom">
      <header class="o-header">
        <span class="o-header_title"><?php echo $c['Classificado']['title'] ?></span> 
        <a href="<?php echo $this->Html->url($c['Classificado']['url']) ?>" class="o-header_read-more">
          Veja todos
        </a>
      </header>
      <div class="row">
        <?php foreach ($c['Anuncio'] as $key => $a) { ?>
          <div class="col-sm-3 col-xs-6">
            <div class="o-classificados">
              <a href="<?php echo $this->Html->url($a['Categoria']['url']) ?>" class="o-classificados_category">
                <?php echo $a['Categoria']['title'] ?>
              </a>
              <a href="<?php echo $this->Html->url($a['Anuncio']['url']) ?>" class="o-classificados_content">
                <?php if(isset($a['Images'][0])) { ?>
                  <img src="<?php echo $this->MediasImage->imagePreset($a['Images'][0]['filename'], '186'); ?>" class="o-classificados_image"> 
                <?php } ?>
                <span class="o-classificados_title"><?php echo $a['Anuncio']['title'] ?></span>
                <span class="o-classificados_price"><?php echo $a['Anuncio']['preco'] ?></span>
              </a>
            </div>
          </div>
        <?php } ?>
      </div>
    </section>
  <?php } ?>
<?php } ?>
