<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/classificados.css">

<?php 
$this->start('classificados_header');
  echo '<div class="container pt-70 xs-pt-30">'; ?>
    <div class="o-classificados_header">
      <h2 class="o-classificados_header-title"><i class="i-icon i-icon_search"></i>Buscar <?php echo $search ?></h2>
      <form method="GET" action="/classificados/search" class="o-classificados_header-form">
        <label class="sr-only" for="classificadosHeaderSearch">Pesquisar</label>
        <input type="text" class="o-classificados_header-input" name="q" id="classificadosHeaderSearch" placeholder="Pesquisar nos classificados">
        <button type="submit" class="o-classificados_header-btn"><i class="i-icon i-icon_search"></i>Pesquisar</button>
        <div class="o-classificados_header-backdrop" aria-hidden="true"></div>
      </form>
    </div>
  <?php
  echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<div class="o-classificados_list">
  <header class="o-classificados_list-header">
    <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Ordenar por <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <?php 
        $classificado = $this->params['classificado'];
        $secao = $this->params['slug'];
        ?>
        <li><?php echo $this->Paginator->sort('publish_start', 'Mais recentes', array('direction' => 'desc')); ?></li>
        <li><?php echo $this->Paginator->sort('preco', 'Menor valor', array('direction' => 'asc')); ?></li>
        <li><?php echo $this->Paginator->sort('preco', 'Maior valor', array('direction' => 'desc')); ?></li>
      </ul>
    </div><span class="o-classificados_n-anuncios"><?php echo $total_anuncios ?> Anúncios</span>
  </header>
  <div class="row">
    <?php foreach ($anuncios as $key => $anuncio) { ?>
      <div class="col-sm-4 col-xs-6">
        <div class="o-classificados">
          <a href="<?php echo $this->Html->url($anuncio['Anuncio']['url']) ?>" class="o-classificados_content">
            <?php if(isset($anuncio['Images'][0])) { ?>
              <img src="<?php echo $this->MediasImage->imagePreset($anuncio['Images'][0]['filename'], '258'); ?>" class="o-classificados_image"> 
            <?php } ?>
            <span class="o-classificados_title"><?php echo $anuncio['Anuncio']['title'] ?></span>
            <span class="o-classificados_price"><?php echo $anuncio['Anuncio']['preco'] ?></span>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php echo $this->element('paginator'); ?>
</div>
