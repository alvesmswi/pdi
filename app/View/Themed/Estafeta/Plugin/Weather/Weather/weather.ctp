<?php if (!empty($condition) && !empty($forecast)): ?>
  <li>
    <div class="c-weather">
      <div class="c-weather_temp">
        <span class="c-weather_icon c-weather_icon--small c-weather_icon--<?php echo $this->Mswi->weatherClass($condition->code); ?>"></span> <?php echo $condition->temp ?>º C
      </div>
      <div class="c-weather_panel">
        <header class="c-weather_content c-weather_header">
          <h4><?php echo $cidade ?>, <?php echo strftime('%d de %B de %Y'); ?></h4>
        </header>
        <div class="c-weather_content" style="padding-top: 8px;">
          <h4 class="c-weather_header">Hoje</h4>
          <span class="c-weather_icon c-weather_icon--large c-weather_icon--<?php echo $this->Mswi->weatherClass($condition->code); ?>"></span>
          <div class="c-weather_description">
            <h3 class="c-weather_title">
              <span style="color:#2f2f2f;"><?php echo $condition->temp ?>º C</span>
              <span style="color:#2f2f2f;"><?php echo $this->Mswi->weatherText($condition->code) ?></span>
            </h3>
            <span class="c-weather_temperature" style="color:#2f2f2f;">
              <span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> <?php echo $forecast[0]->high ?>º C
            </span>
            <span class="c-weather_temperature" style="color:#2f2f2f;">
              <span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> <?php echo $forecast[0]->low ?>º C
            </span>
          </div>
        </div>

        <div class="c-weather_content" style="padding-top: 8px;">
          <h4 class="c-weather_header">Esta semana</h4>
          <div class="c-weather_forecasts">
            <?php for ($i=1; $i < 5; $i++): ?>
              <div class="c-weather_forecast">
                  <time datetime="<?php echo date('Y-m-d H:i', strtotime($forecast[$i]->date)) ?>" class="c-weather_date">
                      <span style="color:#2f2f2f;"><?php echo $this->Mswi->weatherDay($forecast[$i]->day) ?></span>
                      <span style="color:#2f2f2f;"><?php echo date('d/m', strtotime($forecast[$i]->date)) ?></span>
                  </time>
                  <span class="c-weather_icon c-weather_icon--large c-weather_icon--<?php echo $this->Mswi->weatherClass($forecast[$i]->code); ?>"></span>
                  <span class="c-weather_temperature c-weather_temperature--small" style="color:#2f2f2f;">
                    <span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> <?php echo $forecast[$i]->high ?>º C
                  </span>
                  <span class="c-weather_temperature c-weather_temperature--small" style="color:#2f2f2f;">
                    <span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> <?php echo $forecast[$i]->low ?>º C
                  </span>
              </div>
            <?php endfor; ?>
          </div>
        </div>
          <div class="lazyload">
              <!--
                <a href="https://www.yahoo.com/?ilc=401" target="_blank" class="pull-right"> <img src="https://poweredby.yahoo.com/purple.png" width="134" height="29"> </a>
              -->
          </div>
      </div>
    </div>
  </li>
<?php endif; ?>
