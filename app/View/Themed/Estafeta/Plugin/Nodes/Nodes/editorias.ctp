<?php echo $this->element('node_metas'); ?>
<section class="u-spacer_bottom">
    <div class="row mb-40">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">
            <?php
              echo $title;
            ?>
          </h2>
        </div>
      </div>
    </div>
    <div class="u-spacer_bottom">
        <?php 
        echo $this->Mswi->verMaisAjax('editorias_ajax', $slug, 1, 9);
        ?>
    </div>
</section>
