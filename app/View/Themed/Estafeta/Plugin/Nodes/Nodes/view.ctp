<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  $this->Nodes->set($node); 

  echo $this->element('node_metas');

  // Seta variaveis de exibição padrão
  $body = $node['Node']['body'];
  $exibirImagens = true;
  $exibirAssineJa = false;

  // Se for para Bloquear a matéria Parcialmente
  if ((isset($autorizado) && !$autorizado) && 
    (isset($user_agent) && !$user_agent) && $node['Node']['exclusivo'] && Configure::read('Paywall.PaywallBloqueioParcial')) {
    // Script de overflow que mostra o bloco e faz o efeito de shadow
    echo '<style>.zm-post-content {opacity: .3;}</style>';

    // PHP para bloquear o Corpo da Notícia e as Imagens
    $text_plain = strip_tags($body, '<p><br><b>');
    $len = (strlen($text_plain) > Configure::read('Paywall.PaywallQtdeCaracteres')) ? Configure::read('Paywall.PaywallQtdeCaracteres') : (strlen($text_plain) / 2);
    $body = substr($text_plain, 0, $len);
    $exibirImagens = false;
    $exibirAssineJa = true;
  }
?>

<div class="row">
  <div class="col-md-3 sidebar-warp columns hidden-xs">
    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('interna_esquerda')){
        echo $this->Regions->blocks('interna_esquerda');
      }
    ?>

    <?php if (isset($relacionadas) && !empty($relacionadas)): ?>
      <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12 mb-40">
        <div class="row mb-40">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="section-title">
              <h2 class="h6 header-color inline-block uppercase">Relacionadas</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="zm-posts">
              <?php foreach ($relacionadas as $relacionada): ?>
                <?php 
                  $title = $relacionada['Node']['title'];
                  $path = $relacionada['Node']['path'];
                ?>
                <article class="zm-post-lay-e zm-single-post clearfix">
                  <div class="zm-post-header">
                    <h2 class="zm-post-title">
                      <a href="<?php echo $path ?>"><?php echo $title ?></a>
                    </h2>
                  </div>
                </article>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </aside>
    <?php endif; ?>

    <?php
      if(isset($node['Multiattach'])){
        $aux = false;
        foreach ($node['Multiattach'] as $multi) {
          if($this->Mswi->ehArquivo($multi['Multiattach']['mime'])){
            $aux = true;
          }
        } 
        if($aux){ ?>
          <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12 mb-40">
            <div class="row mb-40">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-title">
                  <h2 class="h6 header-color inline-block uppercase">Arquivos</h2>
                </div>  
              </div>  
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="zm-posts"> 
                  <?php
                  foreach ($node['Multiattach'] as $attach) {
                    if($this->Mswi->ehArquivo($attach['Multiattach']['mime'])){ ?>
                      <article class="zm-post-lay-e zm-single-post clearfix" style="margin-bottom: 15px">
                        <div class="zm-post-header">
                          <h2 class="zm-post-title">
                            <a href="<?php echo Router::url('/', true).'fl/normal/'.$attach['Multiattach']['filename'] ?>" download="<?php echo $attach['Multiattach']['filename'] ?>" class="o-news_content">							
                              <span class="o-news_title"><?php echo preg_replace("/(.*-)/","",$attach['Multiattach']['filename'],1) ?></span>
                            </a>
                          </h2>  
                        </div>  
                      </article>	
                    <?php }
                  }?>
                </div>  
              </div>  
            </div>   
          </aside>  
      <?php }
      } 
    ?>

  </div>

  <div class="col-md-9 columns">
    <?php
    $autor = strstr($type['Type']['params'], 'autor');
    if(!empty($autor)){
    ?>
      <div class="section-title mb-20" style="text-align:center;">
          <h1 class="zm-post-title h2" style="color:#074f2c;font-size:36px;"><?php echo $this->request->query['editoria'] ?></h1>
      </div>
        <?php 
            $foto = '/img/foto_user/default.png';
            if(isset($node['User']['foto']) && !empty($node['User']['foto'])){
                $foto = '/img/foto_user/' . $node['User']['foto'];
            }
        ?>
        <div class="container" id="autor" style="margin-bottom:20px;">
          <div class="row">
            <div class="col-sm-2 col-md-2">
                    <img src="<?php echo $foto?>"
                        alt="" class="img-circle img-responsive" style="margin-bottom:15px;" />

            </div>
            <div class="col-sm-4 col-md-4">
              <blockquote>
                <p><?php echo $node['User']['name']?></p> <small><cite ><?php echo $node['User']['bio']?></cite></small>
              </blockquote>
                <p>
                  <a href="mailto:<?php echo $node['User']['email']?>"><i class="glyphicon glyphicon-envelope" style="margin-right:10px;"></i></a>
                  <?php if($node['User']['facebook']){?>
                    <a href="<?php echo $node['User']['facebook']?>"><i class="fa fa-facebook-official" style="margin-right:10px;"></i></a>
                  <?php }
                  if($node['User']['twitter']){?>  
                    <a href="<?php echo $node['User']['twitter']?>"><i class="fa fa-twitter" style="margin-right:10px;"></i></a>
                  <?php }
                  if($node['User']['instagram']){?>  
                    <a href="<?php echo $node['User']['instagram']?>"><i class="fa fa-instagram"></i></a>
                  <?php }?>
                </p>
            </div>
          </div>
        </div>
    <?php }?>    

    <article class="zm-post-lay-single">
    <?php if(empty($autor)){?>      
      <div class="section-title mb-20">
        <h2 class="h6 header-color inline-block uppercase"><?php echo $this->request->query['editoria']; ?></h2>
      </div>
    <?php }?>  
      <div class="zm-post-header" style="padding: 0px 0 15px;">
          <h1 class="zm-post-title h2" style="font-size:36px;line-height:50px;"><?php echo $node['Node']['title']; ?></h1>
          <blockquote><?php echo $node['Node']['excerpt']; ?></blockquote>
          
          <div class="zm-post-meta ">
            <div class="addthis_inline_share_toolbox hidden-sm" style="float:left;"></div>
            <ul class="text-right">
              <li class="s-meta">
                <?php 
                  if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
                    echo $node['NoticiumDetail']['jornalista'];
                  }else{
                    echo $node['User']['name'];
                  }
                ?>
              </li>
              <li class="s-meta">
                  <div class="lazyload">
                      <!--
                      <?php echo htmlentities(strftime('%d de %B de %Y', strtotime($node['Node']['publish_start']))); ?>
                      --->
                  </div>
              </li>
            </ul>
          </div>
          
        </div>
      <?php if (isset($node['Multiattach']) && !empty($node['Multiattach']) && !$this->Mswi->ehArquivo($node['Multiattach'][0]['Multiattach']['mime'])): ?>
        <div class="zm-post-thumb" style="padding-bottom: 25px;border-bottom: 1px solid #e6e6e6;">
          <?php
            $imagemUrlArray = array(
              'plugin' => 'Multiattach',
              'controller' => 'Multiattach',
              'action' => 'displayFile', 
              'admin' => false,
              'filename' => $node['Multiattach'][0]['Multiattach']['filename'],
              'dimension' => '645largura'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
            $imageTitle = htmlspecialchars((!empty($node['Multiattach'][0]['Multiattach']['metaDisplay']) ? $node['Multiattach'][0]['Multiattach']['metaDisplay'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
          
            $imagemUrlArray['dimension'] = 'normal';

            //prepara a imagem para o OG
            $this->Html->meta(
              array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
              null,
              array('inline' => false)
            );
            
            //pega os detalhes da imagem
            $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

            //largura da imagem
            $this->Html->meta(
              array('property' => 'og:image:width', 'content' => $imageArray[0]),
              null,
              array('inline' => false)
            );
            //Altura da imagem
            $this->Html->meta(
              array('property' => 'og:image:height', 'content' => $imageArray[1]),
              null,
              array('inline' => false)
            );
            //Mime da imagem
            $this->Html->meta(
              array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
              null,
              array('inline' => false)
            );
            //Alt da imagem
            $this->Html->meta(
              array('property' => 'og:image:alt', 'content' => $imageTitle),
              null,
              array('inline' => false)
            );

          ?>
            <div class="lazyload">
                <!--
                  <figure>
                    <img src="<?php echo $imagemUrl; ?>" title="<?php echo $imageTitle; ?>" alt="<?php echo $imageTitle; ?>">
                    <?php
                    $fotografo = $node['Multiattach'][0]['Multiattach']['comment'];
                    $descricao = isset($node['Multiattach'][0]['Multiattach']['metaDisplay']) ? $node['Multiattach'][0]['Multiattach']['metaDisplay'] : "";
                    //se tem legenda ou fotografo
                    if(!empty($fotografo) || !empty($descricao)){ ?>
                      <figcaption>
                        <?php
                        if(!empty($descricao)){
                          echo $descricao;
                        }
                        if(!empty($fotografo)){
                          echo '(Foto: '.$fotografo.')';
                        }
                        ?>
                      </figcaption>
                    <?php } ?>
                  </figure>
                -->
            </div>
        </div>
      <?php endif; ?>
      
      <div id="corpo" class="zm-post-content" style="padding-top: 15px;">
        <?php echo $this->Mswi->lerCodigos($body); ?>
      </div>

      <?php
        if ($exibirAssineJa) {
          // Chamar element de bloco de Assine agora!
          echo $this->element('paywall_bloqueio_parcial');
        }
        $countImages = 0;
        foreach($node['Multiattach'] as $count){
          if(!$this->Mswi->ehArquivo($count['Multiattach']['mime'])){
            $countImages += 1;
          }
        }
      ?>      

      <?php if ($exibirImagens): ?>
        <?php if (isset($node['Multiattach']) && $countImages > 1): ?>
          <div class="zm-single-product">
            <div class="tab-content slick-slider">
              <?php 
              $chave_galeria = 0;
              foreach ($node['Multiattach'] as $key_gallery => $foto):                 
                if(!$this->Mswi->ehArquivo($foto['Multiattach']['mime'])){
                  ?>
                  <?php
                    $chave_galeria = $chave_galeria + 1;
                    $class_gallery = ($chave_galeria == 1) ? ' in active' : null;
                  ?>
                  <div id="zm-product-<?php echo $chave_galeria; ?>" class="tab-pane fade<?php echo $class_gallery; ?>" style="width: 645px; height: 394px">
                    <?php
                      $imagemUrlArray = array(
                        'plugin' => 'Multiattach',
                        'controller' => 'Multiattach',
                        'action' => 'displayFile', 
                        'admin' => false,
                        'filename' => $foto['Multiattach']['filename'],
                        'dimension'	=> '870x580'
                      );
                      $imagemUrl = $this->Html->url($imagemUrlArray);
                      $imageTitle = htmlspecialchars((!empty($foto['Multiattach']['metaDisplay']) ? $foto['Multiattach']['metaDisplay'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                      $imagemUrlArray['dimension'] = 'normal';
                      $imagemLightUrl = $this->Html->url($imagemUrlArray);
                    ?>
                            <a href="<?php echo $imagemLightUrl; ?>" data-lightbox="roadtrip" data-title="<?php echo $imageTitle; ?>">
                              <figure>
                                <img data-lazy="<?php echo $imagemUrl; ?>" />
                                <figcaption><?php echo $imageTitle; ?></figcaption>
                              </figure>
                            </a>
                  </div>
                <?php 
                }
              endforeach; ?>
            </div>
            <div class="slick-nav" style="width: 500px; margin: 0px auto 0px auto">
                <?php
                $chave_galeria = 0;
                foreach ($node['Multiattach'] as $key_gallery => $foto):
                  if(!$this->Mswi->ehArquivo($foto['Multiattach']['mime'])){
                      $chave_galeria = $chave_galeria + 1;
                      $class_gallery = ($chave_galeria == 1) ? ' is-checked' : null;
                    ?>
                              <?php
                                $imagemUrlArray = array(
                                  'plugin' => 'Multiattach',
                                  'controller' => 'Multiattach',
                                  'action' => 'displayFile',
                                  'admin' => false,
                                  'filename' => $foto['Multiattach']['filename'],
                                  'dimension'	=> '166x111'
                                );
                                $imagemUrl = $this->Html->url($imagemUrlArray);
                                $imageTitle = htmlspecialchars((!empty($foto['Multiattach']['meta']) ? $foto['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                              ?>
                      <div>

                                <a data-toggle="tab" href="#zm-product-<?php echo $chave_galeria; ?>">
                                    <img data-lazy="<?php echo $imagemUrl; ?>" />
                                </a>

                          </div>

                  <?php
                }
              endforeach; ?>
          </div>
              <script
                      src="https://code.jquery.com/jquery-2.2.4.min.js"
                      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                      crossorigin="anonymous"></script>
              <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/slick.min.js"></script>

              <script type="text/javascript">

                  /* galeria de imagem*/

                  jQuery('.slick-slider').slick({
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      fade: true,
                      arrows: false,
                      lazy: true,
                      asNavFor: '.slick-nav'
                  });
                  jQuery('.slick-nav').slick({
                      slidesToShow: 4,
                      slidesToScroll: 3,
                      asNavFor: '.slick-slider',
                      dots: false,
                      centerMode: true,
                      focusOnSelect: true,
                      lazy:true,
                      <?php if(isset($chave_galeria) && $chave_galeria > 5){
                          echo 'arrows: true,';
                      }
                      ?>
                  });
              </script>
        <?php endif; ?>

        <div class="entry-meta-small clearfix ptb-40 mtb-40 border-top border-bottom">
          <?php echo $this->element('view_tags'); ?>
          <?php //echo $this->element('view_share'); ?>
          <!--COMENTÁRIOS DO FACEBOOK-->
          <div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].'/'.$node['Node']['type'].'/'.$node['Node']['slug']?>" data-numposts="5" data-width="100%"></div>
        </div>
      <?php endif; ?>
    </article>
  </div>
</div>

<?php if (isset($relacionadas) && !empty($relacionadas)): ?>
  <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12 mb-40 visible-xs">
    <div class="row mb-40">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">Relacionadas</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="zm-posts">
          <?php foreach ($relacionadas as $relacionada): ?>
            <?php 
              $title = $relacionada['Node']['title'];
              $path = $relacionada['Node']['path'];
            ?>
            <article class="zm-post-lay-e zm-single-post clearfix">
              <div class="zm-post-header">
                <h2 class="zm-post-title">
                  <a href="<?php echo $path ?>"><?php echo $title ?></a>
                </h2>
              </div>
            </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </aside>
<?php endif; ?>

<?php if ($exibirImagens): ?>
  <?php if (isset($nodesRelacionados) && !empty($nodesRelacionados)): ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <aside class="zm-post-lay-a2-area">
        <div class="post-title mb-40">
          <h2 class="h6 inline-block">Veja Também</h2>
        </div>
        <div class="row">
          <div class="zm-posts clearfix">
            <?php foreach ($nodesRelacionados as $keyRelacionado => $nodeRelacionado): ?>
              <?php
                $link_noticia = $nodeRelacionado['Node']['path'];
                if (isset($nodeRelacionado['NoticiumDetail']['link_externo']) && !empty($nodeRelacionado['NoticiumDetail']['link_externo'])) {
                  $link_noticia = $nodeRelacionado['NoticiumDetail']['link_externo'];
                }

                $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/a/a2/11.jpg';
                if(isset($nodeRelacionado['Multiattach']) && !empty($nodeRelacionado['Multiattach']['filename'])) {
                  $imagemUrlArray = array(
                    'plugin' => 'Multiattach',
                    'controller' => 'Multiattach',
                    'action' => 'displayFile', 
                    'admin' => false,
                    'filename' => $nodeRelacionado['Multiattach']['filename'],
                    'dimension' => '270x120'
                  );
                  $imagemUrl = $this->Html->url($imagemUrlArray);
                  $imageTitle = htmlspecialchars((!empty($nodeRelacionado['Multiattach']['meta']) ? $nodeRelacionado['Multiattach']['meta'] : $nodeRelacionado['Node']['title']), ENT_QUOTES, 'UTF-8');
                }

                $keyRelacionado = $keyRelacionado + 1;
                $class_hidden = (count($nodesRelacionados) == $keyRelacionado) ? 'col-md-4 hidden-md col-lg-4 hidden-sm' : 'col-md-6 col-lg-4';
              ?>
              <div class="col-xs-12 col-sm-6 <?php echo $class_hidden; ?>">
                <article class="zm-post-lay-a2">
                  <div class="zm-post-thumb">
                     <div class="lazyload">
                          <!--
                              <a href="<?php echo $link_noticia; ?>">
                              <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                              </a>
                          -->
                      </div>
                  <div class="zm-post-dis">
                    <div class="zm-post-header">
                      <h2 class="zm-post-title h2">
                        <a href="<?php echo $link_noticia; ?>"><?php echo $nodeRelacionado['Node']['title'] ?></a>
                      </h2>
                    </div>
                  </div>
                </article>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </aside>
    </div>
  <?php endif; ?>
<?php endif; ?>

<?php
      if(isset($node['Multiattach'])){
        $aux = false;
        foreach ($node['Multiattach'] as $multi) {
          if($this->Mswi->ehArquivo($multi['Multiattach']['mime'])){
            $aux = true;
          }
        } 
        if($aux){ ?>
          <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12 mb-40 visible-xs">
            <div class="row mb-40">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-title">
                  <h2 class="h6 header-color inline-block uppercase">Arquivos</h2>
                </div>  
              </div>  
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="zm-posts"> 
                  <?php
                  foreach ($node['Multiattach'] as $attach) {
                    if($this->Mswi->ehArquivo($attach['Multiattach']['mime'])){ ?>
                      <article class="zm-post-lay-e zm-single-post clearfix" style="margin-bottom: 15px">
                        <div class="zm-post-header">
                          <h2 class="zm-post-title">
                            <a href="<?php echo Router::url('/', true).'fl/normal/'.$attach['Multiattach']['filename'] ?>" download="<?php echo $attach['Multiattach']['filename'] ?>" class="o-news_content">							
                              <span class="o-news_title"><?php echo preg_replace("/(.*-)/","",$attach['Multiattach']['filename'],1) ?></span>
                            </a>
                          </h2>  
                        </div>  
                      </article>	
                    <?php }
                  }?>
                </div>  
              </div>  
            </div>   
          </aside>  
      <?php }
      } 
    ?>
