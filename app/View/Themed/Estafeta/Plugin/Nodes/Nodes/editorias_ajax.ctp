<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  echo $this->element('node_metas'); 
?>

<?php if (!empty($nodes)): 
  $page = $this->params->query['page'];
  $proximaPagina = $page + 1;
?>
  <div class="row">
    <div class="col-md-12">
      <div class="zm-posts">
        <?php foreach ($nodes as $keyNode => $n): ?>
          <?php 
            $link_noticia = null;
            $imagemUrl = null;
            $imageTitle = null;
            $categoria = null;
            $title = null;

            $link_noticia = $n['Node']['path'];
            if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
              $link_noticia = $n['NoticiumDetail']['link_externo'];
            }

            if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
              $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
            }
            if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
              $imagemUrlArray = array(
                'plugin' => 'Multiattach',
                'controller' => 'Multiattach',
                'action' => 'displayFile', 
                'admin' => false,
                'filename' => $n['Multiattach']['filename'],
                'dimension' => '760x409'
              );
              $imagemUrl = $this->Html->url($imagemUrlArray);
              $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
            }

            $title = $n['Node']['title'];
            if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
              $title = $n['NoticiumDetail']['titulo_capa'];
            }
          ?>
          <article class="zm-post-lay-c zm-single-post clearfix">
            <div class="zm-post-thumb f-left">
              <?php if (isset($imagemUrl)): ?>
                <div class="lazyload">
                        <a href="<?php echo $link_noticia; ?>?editoria=<?php echo $n['Editoria']['title']; ?>">
                          <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                        </a>
                </div>
              <?php endif; ?>
            </div>
            <div class="zm-post-dis f-right">
              <div class="zm-post-header">
                <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>?editoria=<?php echo $n['Editoria']['title']; ?>"><?php echo $title; ?></a></h2>
                <div class="zm-post-meta">
                  <ul>
                    <li class="s-meta"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Node']['publish_start']))); ?></li>
                  </ul>
                </div>
                <div class="zm-post-content">
                  <?php echo $n['Node']['excerpt'] ?>
                </div>
              </div>
            </div>
          </article>

          <?php if ($keyNode == 2): ?>
            <?php
              //Se tem algum bloco ativo nesta região
              if($this->Regions->blocks('publicidade_lista')){
                echo $this->Regions->blocks('publicidade_lista');
              }
            ?>
          <?php endif; ?>
        <?php endforeach; ?>

        <?php 
        //se tem algum registro
        if(!empty($nodes)){
          echo $this->Mswi->verMaisAjax('editorias_ajax',$n['Editoria']['slug'], $proximaPagina, 9);
        }
        ?>
      </div>
    </div>
  </div>
<?php endif; ?>
