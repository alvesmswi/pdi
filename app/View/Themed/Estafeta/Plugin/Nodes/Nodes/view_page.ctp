<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  $this->Nodes->set($node); 

  echo $this->element('node_metas');

  // Seta variaveis de exibição padrão
  $body = $node['Node']['body'];
?>

<div class="row">
  <div class="col-md-12">
    <article class="zm-post-lay-single">
      <div class="zm-post-header" style="padding: 0px 0 15px;">
        <h2 class="zm-post-title h2"><?php echo $node['Node']['title']; ?></h2>
      </div>

      <?php if (isset($node['Multiattach']) && !empty($node['Multiattach'])): ?>
        <div class="zm-post-thumb" style="margin-bottom: 15px;">
          <?php
            $imagemUrlArray = array(
              'plugin' => 'Multiattach',
							'controller' => 'Multiattach',
							'action' => 'displayFile', 
							'admin' => false,
							'filename' => $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension' => '870x506'
						);
            $imagemUrl = $this->Html->url($imagemUrlArray);
            $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
          
            $imagemUrlArray['dimension'] = 'normal';

            //prepara a imagem para o OG
            $this->Html->meta(
              array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
              null,
              array('inline' => false)
            );
            
            //pega os detalhes da imagem
            $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

            //largura da imagem
            $this->Html->meta(
              array('property' => 'og:image:width', 'content' => $imageArray[0]),
              null,
              array('inline' => false)
            );
            //Altura da imagem
            $this->Html->meta(
              array('property' => 'og:image:height', 'content' => $imageArray[1]),
              null,
              array('inline' => false)
            );
            //Mime da imagem
            $this->Html->meta(
              array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
              null,
              array('inline' => false)
            );
            //Alt da imagem
            $this->Html->meta(
              array('property' => 'og:image:alt', 'content' => $imageTitle),
              null,
              array('inline' => false)
            );
          ?>
            <div class="lazyload">
                <!--
                    <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                -->
            </div>
      <?php endif; ?>

      <div class="zm-post-dis">
        <div class="zm-post-content">
          <?php echo $this->Mswi->lerCodigos($body); ?>
        </div>

        <?php if (isset($node['Multiattach']) && count($node['Multiattach']) > 1): ?>
          <div class="zm-single-product">
            <div class="tab-content">
              <?php foreach ($node['Multiattach'] as $key_gallery => $foto): ?>
                <?php
                  $key_gallery = $key_gallery + 1;
                  $class_gallery = ($key_gallery == 1) ? ' in active' : null;
                ?>
                <div id="zm-product-<?php echo $key_gallery; ?>" class="tab-pane fade<?php echo $class_gallery; ?>">
                  <?php
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $foto['Multiattach']['filename'],
                      'dimension'	=> '870x580'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($foto['Multiattach']['meta']) ? $foto['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                  
                    $imagemUrlArray['dimension'] = 'normal';
                    $imagemLightUrl = $this->Html->url($imagemUrlArray);
                  ?>
                <div class="lazyload">
                  <!--
                      <a href="<?php echo $imagemLightUrl; ?>" data-lightbox="roadtrip" data-title="<?php echo $imageTitle; ?>">
                        <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                      </a>
                  -->
                </div>
              <?php endforeach; ?>
            </div>
            <div class="zm-sin-por-nav-mar">
              <div class="zm-sin-por-nav clearfix">
                <?php foreach ($node['Multiattach'] as $key_gallery => $foto): ?>
                  <?php
                    $key_gallery = $key_gallery + 1;
                    $class_gallery = ($key_gallery == 1) ? ' is-checked' : null;
                  ?>
                  <div class="zm-sin-pro<?php echo $class_gallery; ?>">
                    <?php
                      $imagemUrlArray = array(
                        'plugin' => 'Multiattach',
                        'controller' => 'Multiattach',
                        'action' => 'displayFile', 
                        'admin' => false,
                        'filename' => $foto['Multiattach']['filename'],
                        'dimension'	=> '166x111'
                      );
                      $imagemUrl = $this->Html->url($imagemUrlArray);
                      $imageTitle = htmlspecialchars((!empty($foto['Multiattach']['meta']) ? $foto['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                    ?>
                      <div class="lazyload">
                          <!--
                            <a data-toggle="tab" href="#zm-product-<?php echo $key_gallery; ?>">
                              <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                            </a>
                          -->
                      </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        <?php endif; ?>

        <div class="entry-meta-small clearfix ptb-40 mtb-40 border-top border-bottom">
          <?php echo $this->element('view_share'); ?>
        </div>
      </div>
    </article>

    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('publicidade_interna')){
        echo $this->Regions->blocks('publicidade_interna');
      }
    ?>
  </div>
</div>
