<?php if (!empty($nodesList)): ?>
  <?php 
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  ?>
  <aside class="zm-post-lay-a2-area sm-mt-50 xs-mt-50">
    <div class="row mb-40">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">Colunas</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="zm-posts responsive-fix">
          <?php foreach ($nodesList as $n): ?>
            <?php 
              $link_noticia = null;
              $imagemUrl = null;
              $imageTitle = null;
              $categoria = null;
              $title = null;
              
              $link_noticia = $n['Node']['path'];
              if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                $link_noticia = $n['NoticiumDetail']['link_externo'];
              }

              if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
                $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
              }
              if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                $imagemUrlArray = array(
                  'plugin' => 'Multiattach',
                  'controller' => 'Multiattach',
                  'action' => 'displayFile', 
                  'admin' => false,
                  'filename' => $n['Multiattach']['filename'],
                  'dimension' => '251x118'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
              }

              $categoria = $this->Mswi->categoryName($n['Node']['terms']);
              if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
                $categoria = $n['NoticiumDetail']['chapeu'];
              }

              $title = $n['Node']['title'];
              if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
                $title = $n['NoticiumDetail']['titulo_capa'];
              }
            ?>
            <article class="zm-post-lay-a2">
              <?php if (isset($imagemUrl)): ?>
                <div class="zm-post-thumb">
                    <div class="lazyload">
                      <!--
                        <a href="<?php echo $link_noticia; ?>">
                        <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                      </a>
                      -->
                    </div>
                </div>
              <?php endif; ?>
              <div class="zm-post-dis">
                <div class="zm-post-header">
                  <h2 class="zm-post-title h2"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                  <div class="zm-post-meta">
                    <ul>
                      <li class="s-meta">
                        <a class="zm-date"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Node']['publish_start']))); ?></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </aside>
<?php endif; ?>
