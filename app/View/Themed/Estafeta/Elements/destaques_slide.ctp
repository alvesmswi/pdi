<div class="zm-section bg-white pt-50 xs-pt-30 sm-pt-30">
  <div class="container">
    <div class="row">
      <?php if (!empty($nodesList)): ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
          <div class="trending-post-area owl-trending bg-dark controls-1 autoplay mb-70">
            <?php for ($i=0; $i < 3; $i++): ?>
              <?php 
                $link_noticia = null;
                $imagemUrl = null;
                $imageTitle = null;
                $categoria = null;
                $categoria_slug = null;
                $categoria_color = null;
                $title = null;

                $n = $nodesList[$i];
                $link_noticia = $n['Node']['path'].'?editoria='.$n['Editoria']['title'];                
                if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                  $link_noticia = $n['NoticiumDetail']['link_externo'];
                }

                $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/12.jpg';
                if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                  $imagemUrlArray = array(
                    'plugin' => 'Multiattach',
                    'controller' => 'Multiattach',
                    'action' => 'displayFile', 
                    'admin' => false,
                    'filename' => $n['Multiattach']['filename'],
                    'dimension' => '758x475'
                  );
                  $imagemUrl = $this->Html->url($imagemUrlArray);
                  $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                }

                $categoria = $this->Mswi->categoryName($n['Node']['terms']);
                if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
                  $categoria = $n['NoticiumDetail']['chapeu'];
                }
                $categoria_slug = $this->Mswi->categorySlug($n['Node']['terms']);
                $categoria_color = $this->Mswi->categoryColor($n['Node']['terms']);

                $title = $n['Node']['title'];
                if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
                  $title = $n['NoticiumDetail']['titulo_capa'];
                }
              ?>
              <article class="zm-trending-post zm-lay-c large zm-single-post" data-scrim-bottom="9" data-effict-zoom="1">
                <div class="zm-post-thumb">
                    <div class="lazyload">
                        <!--
                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                        -->
                    </div>
                </div>
                <div class="zm-post-dis text-white">
                  <div class="zm-category">
                    <a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>">
                      <?php echo $categoria; ?>
                    </a>
                  </div>
                  <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                </div>
              </article>
            <?php endfor; ?>
          </div>
        </div>
      <?php endif; ?>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 sidebar-warp">
        <div class="zm-posts">
          <?php for ($i=3; $i < count($nodesList); $i++): ?>
            <?php 
              $link_noticia = null;
              $imagemUrl = null;
              $imageTitle = null;
              $categoria = null;
              $categoria_url = null;
              $categoria_color = null;
              $title = null;

              $node = $nodesList[$i];
              $link_noticia = $node['Node']['path'].'?editoria='.$node['Editoria']['title'];

              if(!isset($node['Multiattach']['filename']) && isset($node['Multiattach'][0]['Multiattach'])) {
                $node['Multiattach'] = $node['Multiattach'][0]['Multiattach'];
              }
              if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])) {
                $imagemUrlArray = array(
                  'plugin' => 'Multiattach',
                  'controller' => 'Multiattach',
                  'action' => 'displayFile', 
                  'admin' => false,
                  'filename' => $node['Multiattach']['filename'],
                  'dimension' => '230x115'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
              }

              $title = $node['Node']['title'];
            ?>
            <!-- Start single post layout D -->
            <article class="zm-post-lay-d clearfix">
              <?php if (isset($imagemUrl)): ?>
                <div class="zm-post-thumb f-left">
                    <div class="lazyload">
                        <!--
                          <a href="<?php echo $link_noticia; ?>">
                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                          </a>
                         -->
                    </div>
                </div>
              <?php endif; ?>
              <div class="zm-post-dis f-right">
                <div class="zm-post-header">
                  <div class="zm-category"><a class="bg-dark cat-btn"><?php echo $node['Editoria']['title']; ?></a></div>
                  <h2 class="zm-post-title">
                    <a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a>
                  </h2>
                  <div class="zm-post-meta">
                      <ul>
                          <li><a class="zm-date"><?php echo date('d-m-Y', strtotime($node['Node']['publish_start'])); ?></a></li>
                      </ul>
                  </div>
                </div>
              </div>
            </article>
            <!-- End single post layout D -->
          <?php endfor; ?>
        </div>
      </div>

    </div>
  </div>
</div>
