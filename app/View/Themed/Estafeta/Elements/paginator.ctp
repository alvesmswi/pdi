<?php if ($this->Paginator->params()['pageCount'] > 1): ?>
  <style>
    .zm-pagination .page-numbers li {
      display: inline-block !important;
    }
    .zm-pagination .page-numbers li .page-numbers {
      display: inline-block !important;
    }
  </style>

  <div class="row hidden-xs">
    <div class="zm-pagination-wrap mt-70">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav class="zm-pagination ptb-40 text-center">
                <ul class="page-numbers">
                    <li>
                      <?php echo $this->Paginator->prev(__d('croogo', 'Anterior'), array('class' => 'prev page-numbers')); ?>
                    </li>
                    <li>
                      <?php echo $this->Paginator->numbers(array('class' => 'page-numbers', 'separator' => null)); ?>
                    </li>
                    <li>
                      <?php echo $this->Paginator->next(__d('croogo', 'Próximo'), array('class' => 'next page-numbers')); ?>
                    </li>
                </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
