<?php if (!empty($menu['threaded'])): ?>
  <div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
    <div class="zm-widget pr-50 pl-20">
      <h2 class="h6 zm-widget-title uppercase text-white mb-30">Categorias</h2>
      <div class="zm-widget-content">
        <div class="zm-category-widget zm-category-1">
          <ul>
            <?php foreach ($menu['threaded'] as $item): ?>
              <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>
              <li><a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
