<div id="menutop" class="header-top-bar bg-dark ptb-10">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hidden-xs">
        <div class="header-top-left">          
          <?php if (!empty($menu['threaded'])): ?>
            <nav class="header-top-menu zm-secondary-menu">
              <ul>
                <?php foreach ($menu['threaded'] as $item): ?>
                  <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>
                  <li><a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a></li>
                <?php endforeach; ?>
              </ul>
            </nav>
          <?php endif; ?>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="header-top-right clierfix text-right">
          <div class="header-social-bookmark topbar-sblock hidden-xs">
            <ul>
              <?php if (!empty(Configure::read('Social.facebook'))) { ?>
                <li><a href="<?php echo Configure::read('Social.facebook') ?>"><i class="fa fa-facebook"></i></a></li>
              <?php } ?>
              <?php if (!empty(Configure::read('Social.youtube'))) { ?>
                <li><a href="<?php echo Configure::read('Social.youtube') ?>"><i class="fa fa-youtube"></i></a></li>
              <?php } ?>
              <?php if (!empty(Configure::read('Social.twitter'))) { ?>
                <li><a href="<?php echo Configure::read('Social.twitter') ?>"><i class="fa fa-twitter"></i></a></li>
              <?php } ?>
              <?php if (!empty(Configure::read('Social.instagram'))) { ?>
                <li><a href="<?php echo Configure::read('Social.instagram') ?>"><i class="fa fa-instagram"></i></a></li>
              <?php } ?>
              <?php if (!empty(Configure::read('Social.whatsapp'))) { ?>
                <li><a href="<?php echo Configure::read('Social.whatsapp') ?>"><i class="fa fa-whatsapp"></i></a></li>
              <?php } ?>
              <li><a href="/noticia.rss"><i class="fa fa-rss"></i></a></li>
            </ul>
          </div>

          <?php if (CakePlugin::loaded('Weather')): ?>
            <div class="topbar-sblock">
              <ul>
                <?php echo $this->requestAction('/weather', array('return')); ?>
              </ul>
            </div>
          <?php endif; ?>

          <div class="zmaga-calendar topbar-sblock hidden-xs">
            <span class="calendar uppercase">
              <?php
                if(Configure::read('Site.cidade')){
                  echo Configure::read('Site.cidade').', '.htmlentities(strftime('%d de %B de %Y'));
                }else{
                  echo htmlentities(strftime('%A, %d de %B de %Y'));
                }
              ?>
            </span>
          </div>

          <div class="user-accoint topbar-sblock">
            <?php
              //se tem usuário logado
              if(AuthComponent::user('id')){
                echo '<span class="login-btn uppercase">Olá '.AuthComponent::user('name').'</span>';
              }else{
                echo '<span class="login-btn uppercase">Login</span>';
              }

              echo $this->element('login_box');
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
