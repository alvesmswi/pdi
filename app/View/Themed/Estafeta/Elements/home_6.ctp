<?php
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
?>
<div class="zm-section bg-white ptb-70">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 columns">
        <?php if (!empty($nodesList)): ?>
          <?php 
            if($block['Block']['show_title']){
              $title_block = $block['Block']['title'];
            }else{
              $title_block = $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
            }
          ?>
          <div class="row mb-40">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="section-title">
                <h2 class="h6 header-color inline-block uppercase"><?php echo $title_block; ?></h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="zm-posts clearfix">
              <?php $contador = 1; ?>
              <?php foreach ($nodesList as $keyNode => $n): ?>
                <?php
                  $link_noticia = null;
                  $imagemUrl = null;
                  $imageTitle = null;
                  $title = null;

                  $link_noticia = $n['Node']['path'];
                  if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $n['NoticiumDetail']['link_externo'];
                  }

                  if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $n['Multiattach']['filename'],
                      'dimension' => '420x247'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $title = $n['Node']['title'];
                  if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
                    $title = $n['NoticiumDetail']['titulo_capa'];
                  }
                ?>

                <?php
                  if ($contador == 1) {
                    echo '<div class="row" style="margin-left:0px;margin-right:0px;">';
                  }
                ?>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                  <article class="zm-post-lay-a1">
                    <?php if (isset($imagemUrl)): ?>
                      <div class="zm-post-thumb">
                          <div class="lazyload">
                              <!--
                                <a href="<?php echo $link_noticia; ?>"><img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>"></a>
                                -->
                          </div>
                      </div>
                    <?php endif; ?>
                    <div class="zm-post-dis">
                      <div class="zm-post-header">
                          <h2 class="zm-post-title h2"><a href="<?php echo $link_noticia ?>"><?php echo $title ?></a></h2>
                          <div class="zm-post-meta">
                            <ul>
                              <li class="s-meta">
                                <a class="zm-date">
                                  <?php echo htmlentities(strftime('%d de %B', strtotime($n['Node']['publish_start']))); ?>
                                </a>
                              </li>
                            </ul>
                          </div>
                      </div>
                    </div>
                  </article>
                </div>

                <?php
                  if ($contador == 2) {
                    echo '</div>';
                    $contador = 0;
                  }

                  $contador++;
                ?>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
        
        <?php
          //Se tem algum bloco ativo nesta região
          if($this->Regions->blocks('publicidade_home_4')){
            echo $this->Regions->blocks('publicidade_home_4');
          }
        ?>   
      </div>

      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 columns">
        <aside class="zm-post-lay-e-area hidden-sm">
        <?php 
          if ($this->Regions->blocks('right_home1')) {
            echo $this->Regions->blocks('right_home1');
          }
          ?>
          <?php echo $this->Regions->block('block-charge-do-dia'); ?>
          <?php echo $this->Regions->block('block-imagem-dia'); ?>
          
        </aside>
      </div>
    </div>
  </div>
</div>
