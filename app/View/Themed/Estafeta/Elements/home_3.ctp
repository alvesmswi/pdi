<?php if (!empty($nodesList)): ?>
<div class="video-post-area ptb-70 bg-dark">
  <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  ?>
  <div class="container">
    <div class="row mb-40">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase"><?php echo Configure::read('Site.title');?> TV</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="zm-video-post-list zm-posts clearfix">
          <?php 
            $principal = $nodesList[0];

            if(isset($principal['Multiattach']) && !empty($principal['Multiattach']['filename'])) {
              $imagemUrlArray = array(
                'plugin' => 'Multiattach',
                'controller' => 'Multiattach',
                'action' => 'displayFile', 
                'admin' => false,
                'filename' => $principal['Multiattach']['filename'],
                'dimension' => '645x326'
              );
              $imagemUrl = $this->Html->url($imagemUrlArray);
              $imageTitle = htmlspecialchars((!empty($principal['Multiattach']['meta']) ? $principal['Multiattach']['meta'] : $principal['Node']['title']), ENT_QUOTES, 'UTF-8');
            }

            $link_noticia = $principal['Node']['path'];
            $title = $principal['Node']['title'];
            $youtube_link = $principal['VideoDetail']['youtube'];
            $excerpt = $principal['Node']['excerpt'];
            $date = $principal['Node']['publish_start'];
          ?>
          <div class="zm-video-post zm-video-lay-b zm-single-post">
            <div class="zm-video-thumb"  data-dark-overlay="2.5">
              <?php if (isset($imagemUrl)): ?>
                <div class="lazyload">
                       <!--
                       <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                       -->
                </div>
              <?php endif; ?>
              <a class="video-activetor" data-type="youtube" data-autoplay="true" href="<?php echo $youtube_link; ?>">
                <i class="fa fa-play-circle-o"></i>
              </a>
            </div>
            <div class="zm-video-info text-white">
              <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title ?></a></h2>
              <div class="zm-post-meta">
                <ul>
                  <li class="s-meta"><a class="zm-date"><?php echo htmlentities(strftime('%d de %B', strtotime($date))); ?></a></li>
                </ul>
                <p><?php echo $excerpt; ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 sm-mt-70 xs-mt-50">
        <div class="zm-video-post-list layout-c zm-posts zm-scrollbar clearfix">
          <?php for ($i=1; $i < count($nodesList); $i++): ?>
            <?php 
              $link_noticia = null;
              $imagemUrl = null;
              $imageTitle = null;
              $youtube_link = null;
              $excerpt = null;
              $date = null;
              $title = null;
              
              $node = $nodesList[$i];

              if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])) {
                $imagemUrlArray = array(
                  'plugin' => 'Multiattach',
                  'controller' => 'Multiattach',
                  'action' => 'displayFile', 
                  'admin' => false,
                  'filename' => $node['Multiattach']['filename'],
                  'dimension' => '221x144'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
              }

              $link_noticia = $node['Node']['path'];
              $title = $node['Node']['title'];
              $youtube_link = $node['VideoDetail']['youtube'];
              $excerpt = $node['Node']['excerpt'];
              $date = $node['Node']['publish_start'];
            ?>
            <div class="zm-video-post zm-video-lay-c zm-single-post clearfix">
              <div class="zm-video-thumb"  data-dark-overlay="2.5" >
                  <div class="lazyload">
                    <!--
                        <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                    -->
                  </div>
                <a class="video-activetor" data-type="youtube" data-autoplay="true" href="<?php echo $youtube_link; ?>">
                  <i class="fa fa-play-circle-o"></i>
                </a>
              </div>
              <div class="zm-video-info text-white">
                <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title ?></a></h2>
                <div class="zm-post-meta">
                  <ul>
                    <li class="s-meta"><a class="zm-date"><?php echo htmlentities(strftime('%d de %B', strtotime($date))); ?></a></li>
                  </ul>
                </div>
              </div>
            </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
