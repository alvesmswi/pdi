<?php
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
?>
<div class="zm-section bg-gray pt-50 xs-pt-30 sm-pt-30 pb-70">
  <div class="container">
    <div class="row">
      <?php if (!empty($nodesList)): ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 columns">
          <div class="row mb-40">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="section-title">
                <h2 class="h6 header-color inline-block uppercase">Últimas Notícias</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="zm-posts">
                <?php foreach ($nodesList as $keyNode => $n): ?>
                  <?php 
                    $link_noticia = null;
                    $imagemUrl = null;
                    $imageTitle = null;
                    $categoria = null;
                    $categoria_color = null;
                    $categoriaUrl = null;
                    $term = null;
                    $title = null;

                    $link_noticia = $n['Node']['path'].'?editoria='.$n['Editoria']['title'];
                    if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                      $link_noticia = $n['NoticiumDetail']['link_externo'];
                    }

                    if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
                      $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
                    }
                    if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                      $imagemUrlArray = array(
                        'plugin' => 'Multiattach',
                        'controller' => 'Multiattach',
                        'action' => 'displayFile', 
                        'admin' => false,
                        'filename' => $n['Multiattach']['filename'],
                        'dimension' => '760x409'
                      );
                      $imagemUrl = $this->Html->url($imagemUrlArray);
                      $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                    }

                    $categoria = $n['Editoria']['title'];
                    if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
                      $categoria = $n['NoticiumDetail']['chapeu'];
                    }
                    $categoria_color = $this->Mswi->categoryColor($n['Node']['terms']);

                    $title = $n['Node']['title'];
                    if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
                      $title = $n['NoticiumDetail']['titulo_capa'];
                    }

                    $term = $n['Editoria']['slug'];
                    $categoriaUrl = '/ultimas-noticias/'.$term;
                  ?>
                  <article class="zm-post-lay-c zm-single-post clearfix">
                    <?php if (isset($imagemUrl)): ?>
                      <div class="zm-post-thumb f-left">
                          <div class="lazyload">
                                <!--
                                    <a href="<?php echo $link_noticia; ?>">
                                      <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                                    </a>
                                -->
                          </div>
                      </div>
                    <?php endif; ?>
                    <div class="zm-post-dis f-right">
                      <div class="zm-post-header">
                        <div class="zm-category">
                          <a href="<?php echo $categoriaUrl; ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>"><?php echo $categoria; ?></a>
                        </div>
                        <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                        <div class="zm-post-meta">
                          <ul>
                            <li class="s-meta"><a class="zm-date"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Node']['publish_start']))); ?></a></li>
                          </ul>
                        </div>
                        <div class="zm-post-content">
                          <?php echo $n['Node']['excerpt'] ?>
                        </div>
                      </div>
                    </div>
                  </article>

                  <?php if ($keyNode == 2): ?>
                    <?php
                      //Se tem algum bloco ativo nesta região
                      if($this->Regions->blocks('publicidade_lista')){
                        echo $this->Regions->blocks('publicidade_lista');
                      }
                    ?>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 sidebar-warp columns">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 columns md-mt-40">
            <?php echo $this->Regions->block('block-home-2-direita-1'); ?>
          </div>
          <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 columns md-mt-40">
            <?php echo $this->Regions->block('ultimos-posts'); ?>
          </div>
        </div>
        <div class="row hidden-md">
          <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12 columns">
            <?php echo $this->Regions->block('block-home-2-direita'); ?>
          </div>
          <div class="col-md-6  col-sm-6 col-lg-8 col-xs-12 columns xs-mt-40">
            <?php 
            if ($this->Regions->blocks('right_home1')) {
              echo $this->Regions->blocks('right_home1');
            }
            ?>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

