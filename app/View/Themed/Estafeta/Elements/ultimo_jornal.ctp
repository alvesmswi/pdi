<?php if (!empty($nodesList)): ?>
  <?php
    $n = $nodesList[0];
    $linkpublish = $n['Node']['path'];
    $target = '_self';
    if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
      $imagemUrlArray = array(
        'plugin'        => 'Multiattach',
        'controller'    => 'Multiattach',
        'action'        => 'displayFile', 
        'admin'         => false,
        'filename'      => $n['Multiattach']['filename'],
        'dimension'     => '420'
      );
      $imagemUrl = $this->Html->url($imagemUrlArray);
    }
    if (isset($n['PageFlip']) && !empty($n['PageFlip'])) {
      $imagemUrl = $n['PageFlip']['capa'];
    }
    if (isset($n['ImpressoDetail']['link']) && !empty($n['ImpressoDetail']['link'])) {
      $linkpublish = $n['ImpressoDetail']['link'];
      $target = '_blank';
    }
  ?>
  <aside class="zm-post-lay-f-area col-sm-6 col-md-12 col-lg-12 sm-mt-50 xs-mt-50 mb-40">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-40">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">Edição Impressa</h2>
        </div>
      </div>
    </div>
    <div class="row">                                        
      <div class="col-xs-12">
        <div class="advertisement">
            <div class="lazyload">
                <!--
                   <a href="<?php echo $linkpublish; ?>" target="<?php echo $target; ?>">
                   <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
                   </a>
                   -->
            </div>
        </div>
      </div>
    </div>    
  </aside>
<?php endif; ?>
