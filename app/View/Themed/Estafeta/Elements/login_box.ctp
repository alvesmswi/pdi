<div class="login-form-wrap bg-white">
  <?php if (AuthComponent::user('id')): ?>
    <a href="/users/users">Meus Dados</a><br/>
    <a href="/users/users/reset/<?php echo $this->Session->read('Auth.User.username') ?>/<?php echo $this->Session->read('Auth.User.activation_key') ?>">Resetar Senha</a><br/>
    <a href="/users/users/logout">Sair</a>
  <?php else: ?>
    <?php
      echo $this->Form->create('User', array(
        'class' => 'm-signin-form text-left', 
        'url' => '/users/users/login'
      ));

      echo $this->Form->input(Configure::read('User.campo_usuario'), array(
        'label' => false, 
        'class' => 'zm-form-control username',
        'placeholder' => Configure::read('User.label_usuario')
      ));

      echo $this->Form->input('password', array(
        'label' => false,
        'class' => 'zm-form-control password',
        'placeholder' => 'Senha'
      ));
    ?>
      <div class="zm-submit-box clearfix  mt-20">
        <input type="submit" value="Login">
        <?php if(CakePlugin::loaded('Paywall')) { ?>
          <a href="/paywall/paywall/cadastro">Registrar</a>
        <?php }else{ ?>
          <a href="/contact/pedido-assinatura">Registrar</a>
        <?php } ?>
      </div>
      <a href="/users/users/forgot" class="zm-forget">Recuperar a senha</a>
    <?php echo $this->Form->end(); ?>
  <?php endif; ?>
</div>
