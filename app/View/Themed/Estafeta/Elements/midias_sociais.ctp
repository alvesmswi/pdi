<?php
  $mt = 'mt-50';
  if ($this->here == '/') {
    $mt = null;
  }
?>
<aside class="zm-post-lay-f-area col-sm-6 col-md-12 col-lg-12 sm-mt-50 xs-mt-50 mt-40">
  <div class="row mb-40 <?php echo $mt ?>">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="section-title">
        <h2 class="h6 header-color inline-block uppercase">Mídias Sociais</h2>
      </div>
    </div>
  </div>
  <div class="row">                                        
    <div class="col-xs-12">
      <div class="followus-area small-columns">
        <?php if (!empty(Configure::read('Social.facebook'))) { ?>
          <a href="<?php echo Configure::read('Social.facebook') ?>" class="social-btn large bg-facebook block"><span class="btn_text"><i class="fa fa-facebook"></i>Curta a nossa página</span></a>
        <?php } ?>
        <?php if (!empty(Configure::read('Social.twitter'))) { ?>
          <a href="<?php echo Configure::read('Social.twitter') ?>" class="social-btn large bg-twitter block"><span class="btn_text"><i class="fa fa-twitter"></i>Siga-nos no Twitter</span></a>
        <?php } ?>
        <?php if (!empty(Configure::read('Social.youtube'))) { ?>
          <a href="<?php echo Configure::read('Social.youtube') ?>" class="social-btn large bg-youtube block"><span class="btn_text"><i class="fa fa-youtube-play"></i>Conheça o nosso canal</span></a>
        <?php } ?>
        <?php if (!empty(Configure::read('Social.instagram'))) { ?>
          <a href="<?php echo Configure::read('Social.instagram') ?>" class="social-btn large bg-instagram block"><span class="btn_text"><i class="fa fa-instagram"></i>Siga-nos no Instagram</span></a>
        <?php } ?>
        <a href="/noticia.rss" class="social-btn large bg-rss block"><span class="btn_text"><i class="fa fa-rss"></i>Assine o nosso Feed</span></a>
      </div>
    </div>
  </div>    
</aside>
