<?php if (isset($posts) && !empty($posts)): ?>
  <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  ?>
  <aside class="zm-post-lay-a4-area sm-mt-50">
    <div class="row mb-40">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">Blogs</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="zm-posts">
          <?php foreach ($posts as $n): ?>
            <article class="zm-post-lay-a4">
              <div class="zm-post-dis">
                <div class="zm-post-header">
                  <h2 class="zm-post-title h2">
                    <a href="/blog/<?php echo $n['Blog']['slug']; ?>/post/<?php echo $n['Post']['slug']; ?>">
                      <?php echo $n['Post']['title'] ?>
                    </a>
                  </h2>
                  <div class="zm-post-meta">
                    <ul>
                      <li class="s-meta">
                        <a href="/blog/<?php echo $n['Blog']['slug']; ?>" class="zm-author"><?php echo $n['Blog']['title']; ?></a>
                      </li>
                      <li class="s-meta">
                        <a class="zm-date"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Post']['publish_start']))); ?></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="zm-post-content">
                  <?php echo $n['Post']['excerpt']; ?>
                </div>
              </div>
            </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </aside>
<?php endif; ?>
