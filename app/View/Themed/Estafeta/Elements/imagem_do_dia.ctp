<?php if (!empty($nodesList)): ?>
  <aside class="zm-post-lay-a-area col-sm-6 col-md-12 col-lg-12 sm-mt-50 xs-mt-50">
    <div class="row mb-40 mt-50">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="section-title">
          <h2 class="h6 header-color inline-block uppercase">Imagem do dia</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="zm-posts">
          <?php foreach ($nodesList as $n): ?>
            <?php 
              $imagemUrlArray = array(
                'plugin'        => 'Multiattach',
                'controller'    => 'Multiattach',
                'action'        => 'displayFile', 
                'admin'         => false,
                'filename'      => $n['Multiattach']['filename'],
                'dimension'     => '420x244'
              );
              $linkImagem = $this->Html->url($imagemUrlArray);
              
              //prepara a imagem para o lightbox
              $imagemUrlArray['dimension'] = 'normal';
              $imagemLightUrl = $this->Html->url($imagemUrlArray);
            ?>
            <article class="zm-post-lay-a sidebar">
              <div class="zm-post-thumb">
                  <div class="lazyload">
                      <!--
                        <a data-lightbox="imagem-do-dia" data-title="<?php echo $n['Node']['title'] ?>" href="<?php echo $imagemLightUrl.'?node_id='.$n['Node']['id']; ?>">
                        <img src="<?php echo $linkImagem?>" alt="<?php echo $n['Node']['title'] ?>">
                        </a>
                        -->
                  </div>
              </div>
              <div class="zm-post-dis">
                <div class="zm-post-header">
                  <h2 class="zm-post-title h2"><?php echo $n['Node']['title'] ?></h2>
                </div>
              </div>
            </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </aside>
<?php endif; ?>
