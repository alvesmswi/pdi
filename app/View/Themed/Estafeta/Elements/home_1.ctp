<?php if (!empty($nodesList)): ?>
  <!-- Folha Gente - Start Popular News [layout A+D]  -->
  <div class="zm-section bg-white ptb-70">
    <div class="container">
      <?php 
        $this->Helpers->load('Medias.MediasImage');

        if(isset($block['Block']['show_title']) && $block['Block']['show_title']){
          $title_block = $block['Block']['title'];
          $link_block = '#';
        }else{
          //$title_block = $nodesList[0]['Blog']['title'];
          $title_block = $nodesList[0]['Editoria']['title'];
          $editoriaSlug = $nodesList[0]['Editoria']['slug'];
          if(isset($nodesList[0]['Node']['type'])){
            $nodeType = $nodesList[0]['Node']['type'];
          }else{
            $nodeType = 'coluna';
          }          
          $link_block = '/ultimas-noticias/'.$editoriaSlug;
        }
      ?>
      <div class="row mb-40">
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
          <div class="section-title">
            <a href="<?php echo $link_block; ?>">
              <h2 class="h6 header-color inline-block uppercase"><?php echo $title_block; ?></h2>
            </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-lg-6">
          <?php if (isset($nodesList[0])): ?>
            <?php 
              $principal = $nodesList[0];

              $link_noticia = $principal['Node']['path'].'?editoria='.$title_block;

              if(!isset($principal['Multiattach']['filename']) && isset($principal['Multiattach'][0]['Multiattach'])) {
                $principal['Multiattach'] = $principal['Multiattach'][0]['Multiattach'];
              }
              if(isset($principal['Multiattach']) && !empty($principal['Multiattach']['filename'])) {
                $imagemUrlArray = array(
                  'plugin' => 'Multiattach',
                  'controller' => 'Multiattach',
                  'action' => 'displayFile', 
                  'admin' => false,
                  'filename' => $principal['Multiattach']['filename'],
                  'dimension' => '645x408'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                $imageTitle = htmlspecialchars((!empty($principal['Multiattach']['meta']) ? $principal['Multiattach']['meta'] : $principal['Node']['title']), ENT_QUOTES, 'UTF-8');
              }

              $title = $principal['Node']['title'];
            ?>
            <div class="zm-posts">
              <article class="zm-post-lay-a">
                <div class="zm-post-thumb">
                    <div class="lazyload">
                        <!--
                          <a href="<?php echo $link_noticia; ?>">
                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                          </a>
                        -->
                    </div>
                </div>
                <div class="zm-post-dis">
                  <div class="zm-post-header">
                    <h2 class="zm-post-title h2">
                      <a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a>
                    </h2>
                  </div>
                  <div class="zm-post-content">
                    <p><?php echo $principal['Node']['excerpt']; ?></p>
                  </div>
                </div>
              </article>
            </div>
          <?php endif; ?>
        </div>
        <div class="col-md-7 col-sm-12 col-xs-12 col-lg-6">
          <div class="zm-posts">
            <?php for ($i=1; $i < count($nodesList); $i++): ?>
              <?php 
                $link_noticia = null;
                $imagemUrl = null;
                $imageTitle = null;
                $categoria = null;
                $categoria_url = null;
                $categoria_color = null;
                $title = null;

                $node = $nodesList[$i];
                $link_noticia = $node['Node']['path'].'?editoria='.$title_block;

                if(!isset($node['Multiattach']['filename']) && isset($node['Multiattach'][0]['Multiattach'])) {
                  $node['Multiattach'] = $node['Multiattach'][0]['Multiattach'];
                }
                if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])) {
                  $imagemUrlArray = array(
                    'plugin' => 'Multiattach',
                    'controller' => 'Multiattach',
                    'action' => 'displayFile', 
                    'admin' => false,
                    'filename' => $node['Multiattach']['filename'],
                    'dimension' => '230x115'
                  );
                  $imagemUrl = $this->Html->url($imagemUrlArray);
                  $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                }

                $title = $node['Node']['title'];
              ?>
              <!-- Start single post layout D -->
              <article class="zm-post-lay-d clearfix">
                <?php if (isset($imagemUrl)): ?>
                  <div class="zm-post-thumb f-left">
                        <div class="lazyload">
                            <!--
                                <a href="<?php echo $link_noticia; ?>">
                                    <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                                </a>
                            -->
                        </div>
                  </div>
                <?php endif; ?>
                <div class="zm-post-dis f-right">
                  <div class="zm-post-header">
                    <h2 class="zm-post-title">
                      <a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a>
                    </h2>
                  </div>
                </div>
              </article>
              <!-- End single post layout D -->
            <?php endfor; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
