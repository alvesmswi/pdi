<div class="col-md-12 col-lg-12 col-sm-6 mt-60 sm-mb-50">
  <aside class="subscribe-form bg-dark text-center sidebar">
      <h3 class="uppercase zm-post-title">Newsletter</h3>
      <div class="zm-widget-content">
          <div class="subscribe-form subscribe-footer">
              <p>Assine nossa Newsletter</p>
              <form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
                  <input type="email" placeholder="Digite o seu e-mail" name="email" required="required">
                  <input type="submit" value="Inscrever-se">
              </form>
          </div>
      </div>
  </aside>
</div>
