<?php if(!empty($posts)){?>
  <?php 
      $foto = '/img/foto_user/default.png';
      if(isset($posts[0]['Blog']['image']) && !empty($posts[0]['Blog']['image'])){
          $foto = $posts[0]['Blog']['image'];
      }
    $facebook = $posts[0]['Blog']['Blogueiros'][0]['User']['facebook']; 
    $twitter = $posts[0]['Blog']['Blogueiros'][0]['User']['twitter'];
    $instagram = $posts[0]['Blog']['Blogueiros'][0]['User']['instagram'];
  ?>
  <div class="container" id="autor" style="margin-bottom:20px;">
    <div class="row">
      <div class="col-sm-2 col-md-2">
          <div class="lazyload">
            <!--
                <img src="<?php echo $foto?>"
                alt="" class="img-circle img-responsive" style="margin-bottom:15px;" />
            -->
          </div>
      </div>
      <div class="col-sm-4 col-md-4">
        <blockquote>
          <p><?php echo $posts[0]['Blog']['Blogueiros'][0]['User']['name']?></p> <small><cite ><?php echo $posts[0]['Blog']['Blogueiros'][0]['User']['bio']?></cite></small>
        </blockquote>
        <p>
          <a href="mailto:<?php echo $posts[0]['Blog']['Blogueiros'][0]['User']['email']?>"><i class="glyphicon glyphicon-envelope" style="margin-right:10px;"></i></a>
          <?php if($facebook){?>
            <a href="<?php echo $facebook?>"><i class="fa fa-facebook-official" style="margin-right:10px;"></i></a>
          <?php }
          if($twitter){?>  
            <a href="<?php echo $twitter?>"><i class="fa fa-twitter" style="margin-right:10px;"></i></a>
          <?php }
          if($instagram){?>  
            <a href="<?php echo $instagram?>"><i class="fa fa-instagram"></i></a>
          <?php }?>
        </p>
      </div>
    </div>
  </div>
<?php }?>  
