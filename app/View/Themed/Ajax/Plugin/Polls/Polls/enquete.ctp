<?php
if (!empty($poll_array)):
  echo $this->Html->css('Polls.poll');
  ?>
  <section class="u-spacer_bottom">
    <header class="o-header">
      <span class="o-header_title" id="bloco-enquete">Enquete</span>
    </header>
    <br />

    <div class="addthis_inline_share_toolbox hidden-sm"></div><br />

    <aside class="subscribe-form">
        <h4><?php echo $poll_array['question'] ?></h4><br />
        <?php if(!empty($poll_array['description'])){?>
          <small style="font-size:15px;"><?php echo $poll_array['description'] ?></small><br />
        <?php }?>  
          <?php 
          echo $this->Form->create(null, 
            array(
              'method'=>'POST', 
              'url'=>'/polls/polls/submit_poll'
            )
          ); 
          echo $this->Form->hidden('PollVote.poll_id', array('value' => $poll_array['poll_id'])); 
          if ($poll_array['graph']): ?>
            <table>
              <?php foreach ($poll_array['answers'] as $key => $option): ?>
                <tr>
                  <td>
                      <label for='<?php echo $option['id'];?>'><?php echo $option['answer'];?></label>
                  </td>
                </tr>
                <tr>
                  <td style='text-align:center'>
                      <div class="progress progress-info" style="width:300px;">
                          <div class="bar" style="width: <?php echo $option['percent'];?>%"><?php echo $option['percent'];?>%</div>
                      </div>
                  </td>
                </tr>
              
            <?php endforeach; ?>
            </table>

            <div class="single-input">
              <a href="/polls/polls/index/<?php echo $poll_array['poll_id']; ?>">Resultado Parcial</a>
            </div>
          <?php else: ?>
            <?php foreach ($poll_array['answers'] as $key => $option): ?>
              <div class="radio">
                <label>
                  <input type="radio" name="data[PollVote][poll_option_id]" id="optionsRadios<?php echo $key; ?>" value="<?php echo $option['id']; ?>">
                  <?php echo $option['answer']; ?>
                </label>
              </div>
            <?php endforeach; ?>

            <div class="single-input">
              <input value="Enviar Voto" type="submit">
            </div>
          <?php endif; ?>
      <?php echo $this->Form->end(); ?>
    </aside>
</section>  

<br />
<?php endif; ?>
