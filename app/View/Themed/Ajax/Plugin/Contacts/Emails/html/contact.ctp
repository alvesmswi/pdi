<table>
	<tr>
		<td class="header">
			<div class="logo">
	    		<a href="/" class="c-banner_logo-link">
	    			<?php 
			      	//Caminho absoluto do arquivo de CSS customizado
					$themeLogo = WWW_ROOT . 'img' . DS . 'logo.png';
			      	//se o arquivo de CSS personalizado existe
			      	if(file_exists($themeLogo)){
			      		$themeLogo = '/img/logo.png';
			      	}else{
						$themeLogo = '/theme/' . Configure::read('Site.theme').'/img/logo.png';
			      	}
			      	?>
	    			<img src="<?php echo $themeLogo; ?>" class="c-banner_logo-image">
	    		</a>
	 		</div>
		</td>
		<td class="titulo">
			<h1><?php echo $title_for_layout; ?></h1>
			<h2><?php echo Configure::read('Site.title'); ?></h2>
		</td>
	</tr>
	<tr>
		<td class="corpo" colspan="2">
			<hr />
			<?php
			$url = Router::url(array(
				'controller' => 'contacts',
				'action' => 'view',
				$contact['Contact']['alias'],
			), true);
			echo __d('croogo', 'You have received a new message at: %s', $url) . "\n \n";
			echo __d('croogo', 'Name: %s', $message['Message']['name']) . "\n";
			echo __d('croogo', 'Email: %s', $message['Message']['email']) . "\n";
			echo __d('croogo', 'Subject: %s', $message['Message']['title']) . "\n";
			echo __d('croogo', 'IP Address: %s', $_SERVER['REMOTE_ADDR']) . "\n";
			echo __d('croogo', 'Message: %s', $message['Message']['body']) . "\n";
			?>
		</td>
	</tr>
	<tr>
		<td class="rodape" colspan="2">
			
		</td>
	</tr>
</table>