<header class="o-header">
	<span class="o-header_title">Assinante</span>
</header>
<div class="o-login_content">
   <h1 class="o-news_title c-article_title o-login_title">
   	Esqueci minha senha
   </h1>
   <?php 
	echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'forgot')));
   ?>
      <div class="row">
         <div class="col-xs-3 col-sm-4">
         	<?php 
         	//Exibe o último jornal
         	echo $this->Regions->block('ultimo-jornal');?>
         </div>
         <div class="col-xs-9 col-sm-8 col-md-6">
            <div class="o-login_cta">
               <p>Ainda não é assinante? <a href="/contact/pedido-assinatura" class="btn btn-success">Assine agora</a></p>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input('email', array('label' => 'Email', 'class'=>'form-control')); ?>
            </div>
            <div class="row">
               <div class="col-xs-6 text-right"><button type="submit" class="btn btn-lg btn-primary">Enviar para o meu email</button></div>
            </div>
         </div>
      </div>
     <?php echo $this->Form->end(); ?> 
   </form>
</div>