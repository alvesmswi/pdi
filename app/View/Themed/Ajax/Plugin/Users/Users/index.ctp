<div class="users index">
	<h2><?php echo $title_for_layout; ?></h2>

	<?php if($this->Session->check('Auth.User')): ?>
		<p class="text-right"><?php echo __d('croogo', 'You are currently logged in as:') . ' ' . $this->Session->read('Auth.User.username'); ?></p>

		<ul class="nav nav-pills" style="margin-bottom: 10px;">
			<li role="presentation" class="active">
				<a href="/users/users">Meus Dados</a>
			</li>
			<li role="presentation">
				<a href="/users/users/reset/<?php echo $this->Session->read('Auth.User.username') ?>/<?php echo $this->Session->read('Auth.User.activation_key') ?>">
					Resetar Senha
				</a>
			</li>
		</ul>

		<table class="table table-striped">
			<tr>
				<td>Nome de usuário</td>
				<td><?php echo $this->Session->read('Auth.User.username'); ?></td>
			</tr>
			<tr>
				<td>Nome</td>
				<td><?php echo $this->Session->read('Auth.User.name'); ?></td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td><?php echo $this->Session->read('Auth.User.email'); ?></td>
			</tr>
			<tr>
				<td>Data de criação</td>
				<td><?php echo date('d/m/Y H:i', strtotime($this->Session->read('Auth.User.created'))); ?></td>
			</tr>

			<tr>
				<td>Grupo</td>
				<td><?php echo $this->Session->read('Auth.User.Role.title'); ?></td>
			</tr>
			<?php if ($this->Session->read('Auth.User.role_id') == 4 && !empty($this->Session->read('Auth.User.data_expiracao'))): ?>
				<tr>
					<td>Data de expiração</td>
					<td><?php echo date('d/m/Y H:i', strtotime($this->Session->read('Auth.User.data_expiracao'))); ?></td>
				</tr>
			<?php endif; ?>
		</table>
	<?php endif; ?>
</div>