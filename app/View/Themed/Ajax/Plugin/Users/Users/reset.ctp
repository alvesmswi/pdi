<?php if($this->Session->check('Auth.User')): ?>
	<div class="users index">
		<h2><?php echo $title_for_layout; ?></h2>

		<p class="text-right"><?php echo __d('croogo', 'You are currently logged in as:') . ' ' . $this->Session->read('Auth.User.username'); ?></p>

		<ul class="nav nav-pills" style="margin-bottom: 10px;">
			<li role="presentation">
				<a href="/users/users">Meus Dados</a>
			</li>
			<li role="presentation" class="active">
				<a href="/users/users/reset/<?php echo $this->Session->read('Auth.User.username') ?>/<?php echo $this->Session->read('Auth.User.activation_key') ?>">
					Resetar Senha
				</a>
			</li>
		</ul>

		<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
			<div class="form-group">
				<label for="UserPassword" class="col-sm-2 control-label"><?php echo __d('croogo', 'New password') ?></label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('password', array('label' => false, 'class'=>'form-control','minlength'=>8)); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="UserVerifyPassword" class="col-sm-2 control-label"><?php echo __d('croogo', 'Verify Password') ?></label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('verify_password', array('label' => false, 'class'=>'form-control','minlength'=>8, 'type'=>'password')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Trocar Senha</button>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
<?php else: ?>
	<header class="o-header">
		<span class="o-header_title">Assinante</span>
	</header>
	<div class="o-login_content">
		<h1 class="o-news_title c-article_title o-login_title">
			Trocar Senha
		</h1>
		<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
			<div class="row">
				<div class="col-xs-9 col-sm-8 col-md-6">
					<div class="form-group">
						<?php echo $this->Form->input('password', array('label' =>  __d('croogo', 'New password'), 'class'=>'form-control','minlength'=>8)); ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('verify_password', array('label' => __d('croogo', 'Verify Password'), 'class'=>'form-control','minlength'=>8, 'type'=>'password')); ?>
					</div>
					<div class="row">
						<div class="col-xs-6 text-right"><button type="submit" class="btn btn-lg btn-primary">
							Trocar Senha	
						</button></div>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?> 
		</form>
	</div>
<?php endif; ?>
