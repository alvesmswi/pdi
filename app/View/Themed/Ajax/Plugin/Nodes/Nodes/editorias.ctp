<?php echo $this->element('node_metas'); ?>
<section class="u-spacer_bottom">
    <header class="o-header">
        <span class="o-header_title">
            <?php echo $title; ?>
        </span>
    </header>
    <div class="u-spacer_bottom">
        <?php 
        echo $this->Mswi->verMaisAjax('editorias_ajax', $slug, 1, 9);
        ?>
    </div>
</section>
