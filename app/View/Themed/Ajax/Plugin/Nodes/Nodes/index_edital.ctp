<?php echo $this->element('node_metas'); ?>
<section class="u-spacer_bottom">
    <header class="o-header">
    	<span class="o-header_title">
	    	Editais
	    </span>
    </header>
    
    <?php 
    //Chama o elemento que filtra por periodo
    echo $this->element('filtro_edital')?>
	
	<!--nocache-->

    <div class="u-spacer_bottom">
       <div class="row">
       	<?php
		//se tem algum registro
		if(!empty($nodes)){
			$contador = 0;
			$total = count($nodes);
			//percorre os nodes
			foreach($nodes as $n){ 
				$contador++;
				$linkTarget 	= '_self';
				$linkpublish 	= $n['Node']['path'];
				$imagemUrl 		= '';
				?>
	          <div class="col-sm-4">
	             <div class="o-news"> 
	             	<?php 					
					//se tem attach
			        if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
						//percorre os anexos
			            foreach ($n['Multiattach'] as $key => $attach) {
							
							//se se imagem
			                if (stristr($attach['Multiattach']['mime'], 'image/')) { 
			                    $imagemUrlArray = array(
			                        'plugin'        => 'Multiattach',
			                        'controller'    => 'Multiattach',
			                        'action'        => 'displayFile', 
			                        'admin'         => false,
			                        'filename'      => $attach['Multiattach']['filename'],
			                        'dimension'     => '89x67'
			                    );
			                    $imagemUrl = $this->Html->url($imagemUrlArray);
							}
							
							//se tem pdf
			                if (stristr($attach['Multiattach']['mime'], '/pdf')) {
			                    $linkUrlArray = array(
			                        'plugin'        => 'Multiattach',
			                        'controller'    => 'Multiattach',
			                        'action'        => 'displayFile', 
			                        'admin'         => false,
			                        'filename'      => $attach['Multiattach']['filename'],
			                        'dimension'     => 'normal',
			                    );
								
								//se é para fazer download
								if(isset($download) && $download){
									$linkpublish = $this->Html->url($linkUrlArray);
									$linkTarget 	= '_blank';
								}
			                }
			            }
					}
					?>
					
					<?php
					if($n['Node']['exclusivo'] == 1 && !AuthComponent::user('id')){
						$linkpublish = '/users/users/login';
					}?>
					
					<a href="<?php echo $linkpublish; ?>" class="o-news_category" target="<?php echo $linkTarget; ?>">
						<?php echo $n['EditalDetail']['cidade'].'-'.$n['EditalDetail']['uf']; ?>
					</a>
	             	<a href="<?php echo $linkpublish; ?>" class="o-news_content" target="<?php echo $linkTarget; ?>">
	             		<?php if(isset($imagemUrl)){?>
	             			<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left" /> 
	             		<?php } ?>
	             		<span class="o-news_title">
	             			<?php echo $n['Node']['title']; ?>
	             		</span>
	             		<div class="c-article_pubdate">
	             			<time datetime="<?php echo $n['Node']['publish_start'];?>">
	             				<?php echo date('d/m/Y', strtotime($n['Node']['publish_start'])); ?>
	             			</time>
	             		</div>
					 </a>	
					 				
	             </div>
	          </div>
          <?php if($contador == 3){ $contador++; ?>
          	<div class="clearfix"></div>
          <?php }
          } 
		}else{
  			echo __d('croogo', 'Nenhum registro encontrado.');
  		} ?>
	   </div>
       <?php //Insere o paginado
		echo $this->element('paginator');?>
	</div>
	
	<!--/nocache-->
 </section>