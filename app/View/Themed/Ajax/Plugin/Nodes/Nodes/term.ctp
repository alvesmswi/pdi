<?php echo $this->element('node_metas'); ?>
<section class="u-spacer_bottom">
    <header class="o-header">
    	<span class="o-header_title">
	    	<?php
			//se teve uma pesquisa
			if(isset($this->params->named['slug']) && !empty($this->params->named['slug'])){
				echo __d('croogo', $this->Mswi->slugName($this->params->named['slug']));
			}else{
				echo __d('croogo', 'Resultado da pesquisa');
			}
			?>
	    </span>
    </header>
    <div class="u-spacer_bottom">
       	<div class="row">
       	<?php
		//se tem algum registro
		if(!empty($nodes)){ 
			$contador = 0;
			$contadorGeral = 0;
			$total = count($nodes);
			$imageDimension = '66x66';
			$imageClass = "o-news_thumb o-news_thumb--small";
			//percorre os nodes
			foreach($nodes as $n){ 
				$contador++;
				$contadorGeral++;

				if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
					if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
						$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
					}
				}
				?>
				<div class="col-sm-4">

					<?php 
					//Se as 3 primeiras
					if($contadorGeral < 4){ ?>
						<div class="o-news" style="margin-bottom:10px;">
							<?php
								$link_noticia = $this->Html->url($n['Node']['path']);
								if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
									$link_noticia = $n['NoticiumDetail']['link_externo'];
								}
							?>
							<a href="<?php echo $link_noticia ?>" class="o-news_content">
								<?php 
								//se tem imagem
								if(isset($n['Multiattach']) && (!empty($n['Multiattach']['filename']))){
									$imagemUrlArray = array(
										'plugin'        => 'Multiattach',
										'controller'    => 'Multiattach',
										'action'        => 'displayFile', 
										'admin'         => false,
										'filename'      => $n['Multiattach']['filename'],
										'dimension'     => '344x258'
									);
									$linkImagem = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
								
									?>
									<img src="<?php echo $linkImagem?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
								<?php } ?>
								<span class="o-news_title o-news_title--large"><?php echo $n['Node']['title'] ?></span> 
							</a>
						</div>
					<?php 
					//as demais
					}else{ ?>
						<div class="o-news">
							<?php
								$link_noticia = $n['Node']['path'];
								if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
									$link_noticia = $n['NoticiumDetail']['link_externo'];
								}
							?>
							<a href="<?php echo $link_noticia; ?>" class="o-news_content">
								<?php 
								//se tem imagem
								if(isset($n['Multiattach']) && (!empty($n['Multiattach']['filename']))){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> $imageDimension
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
									?>
									<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="<?php echo $imageClass; ?>" alt="<?php echo $imageTitle ?>" /> 
								<?php } ?>
								<span class="o-news_category">
									<?php 
									//se tem chapey
									if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
										echo $n['NoticiumDetail']['chapeu'];
									}else{
										echo $this->Mswi->categoryName($n['Node']['terms']);
									}; 
									?>
								</span>
								<span class="o-news_title"><?php echo $n['Node']['title']; ?></span>

							</a>	        
						</div>
					<?php } ?>
				</div>
			<?php if($contador == 3){ $contador = 0; ?>
				<div class="clearfix"></div>
			<?php }
			} 
		} else { ?>
			<div class="col-sm-12">
				<?php echo __d('croogo', 'Nenhum registro encontrado.'); ?>
			</div>
		<?php } ?>
	</div>
	<?php //Insere o paginado
		echo $this->element('paginator');?>
	</div>
 </section>