<?php echo $this->element('node_metas'); ?>
<section class="u-spacer_bottom">
    <header class="o-header">
    	<span class="o-header_title">
	    	<?php echo $type['Type']['title'];?>
	    </span>
    </header>
    
    <?php 
    //Chama o elemento que filtra por periodo
    echo $this->element('filtro_periodo')?>
    
    <div class="c-collection c-collection_xs--6 c-collection_sm--4 c-collection_md--3 u-spacer_bottom">
       <div class="row">
       	<?php
		//se tem algum registro
		if(!empty($nodes)){
			$contador = 0;
			$total = count($nodes);
			//percorre os nodes
			foreach($nodes as $n){ $contador++;?>
				<div class="col-xs-6 col-sm-4 col-md-3">
                	<div class="o-news">
                		<?php 
						$imagemUrl 		= 'http://placehold.it/195x314';
						$linkTarget 	= '_self';
						$linkpublish 	= '/'.$n['Node']['type'].'/'.$n['Node']['slug'];
						//se tem attach
				        if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
				            foreach ($n['Multiattach'] as $key => $attach) {
				                if (strpos($attach['Multiattach']['mime'], 'image/') !== false) { 
				                    $imagemUrlArray = array(
				                        'plugin'        => 'Multiattach',
				                        'controller'    => 'Multiattach',
				                        'action'        => 'displayFile', 
				                        'admin'         => false,
				                        'filename'      => $attach['Multiattach']['filename'],
				                        'dimension'     => 'impressoMin'
				                    );
				                    $imagemUrl = $this->Html->url($imagemUrlArray);
				                }
				                if (strpos($attach['Multiattach']['mime'], '/pdf') !== false) {
				                    $linkUrlArray = array(
				                        'plugin'        => 'Multiattach',
				                        'controller'    => 'Multiattach',
				                        'action'        => 'displayFile', 
				                        'admin'         => false,
				                        'filename'      => $attach['Multiattach']['filename'],
				                        'dimension'     => 'normal',
				                    );
				                   //se é para fazer download
									if(isset($download) && $download){
										$linkpublish = $this->Html->url($linkUrlArray);
										$linkTarget 	= '_blank';
									}
				                }
				            }
						}
						
						if (isset($n['PageFlip']) && !empty($n['PageFlip'])) {
							$imagemUrl = $n['PageFlip']['capa'];
				        }
						?>
						<a class="o-news_title" href="<?php echo $linkpublish; ?>" target="<?php echo $linkTarget; ?>">
	                		<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--center"> 
                			<?php echo $n['Node']['title']; ?>
						</a>
                	</div>
              	</div>
          <?php if($contador == 3){ $contador++; ?>
          	<!-- <div class="clearfix"></div> -->
          <?php }
          } 
		}else{
  			echo __d('croogo', 'Nenhum registro encontrado.');
  		} ?>
       </div>
       <?php //Insere o paginado
		echo $this->element('paginator');?>
    </div>
 </section>