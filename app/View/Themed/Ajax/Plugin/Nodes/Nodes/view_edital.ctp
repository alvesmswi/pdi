<?php
echo $this->element('node_metas');
//se é para exibir com PAGEFLIP
if(isset($pageflip) && $pageflip){

	$this->Html->meta(
		array('property' => 'og:title', 'content' => $node['Node']['title']),
		null,
		array('inline'=>false)
	);

	$this->Html->meta(
		array('property' => 'og:description', 'content' => (isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt']) ? $node['Node']['excerpt'] : "")),
		null,
		array('inline'=>false)
	);

	$this->Html->meta(
		array('property' => 'og:url', 'content' => Router::url(null, true)),
		null,
		array('inline'=>false)
	);

	$this->Html->meta(
		array('name' => 'twitter:card', 'content' => 'summary'),
		null,
		array('inline'=>false)
	);
	?>
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/pdf.css">
	<header class="o-header">
		<a href="/" class="o-header_title o-header_title--large">
			<?php echo $node['Node']['title']; ?>
		</a>
	</header>
	<?php 
	//Corpo da noticia
	echo $node['Node']['body'];
	//pr($node);
	//Procura o anexo
	if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
		//percorre os anexos
		foreach($node['Multiattach'] as $anexo){

			//verifica o tipo de anexo
			$arrayMime = explode('pdf', $anexo['Multiattach']['mime']);
			//se tem PDF anexado
			if(count($arrayMime) > 1){
				$linkUrlArray = array(
									'plugin'        => 'Multiattach',
									'controller'    => 'Multiattach',
									'action'        => 'displayFile', 
									'admin'         => false,
									'filename'      => $anexo['Multiattach']['filename'],
									'dimension'     => 'normal'
							);
						$pdfPath = $this->Html->url($linkUrlArray);
			}
			
			//verifica o tipo de anexo
			$arrayMime = explode('image', $anexo['Multiattach']['mime']);
			//se tem IMG anexado
			if(count($arrayMime) > 1){
				$linkUrlArray = array(
									'plugin'        => 'Multiattach',
									'controller'    => 'Multiattach',
									'action'        => 'displayFile', 
									'admin'         => false,
									'filename'      => $anexo['Multiattach']['filename'],
									'dimension'     => 'normal'
							);
						$imgPath = $this->Html->url($linkUrlArray);
						$this->Html->meta(
								array('property' => 'og:image', 'content' => $this->Html->url($linkUrlArray, true)),
								null,
								array('inline' => false)
							);
			}
		}
	}

	//se NÃO encontrou PDF
	// if(!isset($pdfPath) && isset($imgPath)){
		
	// 	$link = false;
	// 	//se tem link
	// 	if(isset($node['ImpressoDetail']['link']) && !empty($node['ImpressoDetail']['link'])){
	// 		$link = $node['ImpressoDetail']['link'];
	// 	}
	// 	//exibe a imagem
	// 	echo $this->Html->image(
	// 		$imgPath,
	// 		array(
	// 			'url' => $link
	// 		)
	// 	);
		
	// //Se encontrou PDf	
	// }else 
		if(isset($pdfPath)){	
	?>
		<iframe src="<?php echo $pdfPath ?>" width="100%" height="780" style="border: none;"></iframe>
		
	<?php }

//exibição COMUM
}else{ ?>

	<section class="s-article">
    <article class="c-article">
    	<header class="o-header">
    		<span class="o-header_title o-header_title--large">
    			<?php echo $this->Mswi->categoryName($node['Node']['terms']); ?>
    		</span>
    	</header>
       	<header class="o-news_header c-article_header">
			<h1 class="o-news_title c-article_title"><?php echo $node['Node']['title']; ?></h1>
			<?php if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])): ?>
				<div class="o-news_summary c-article_summary">
					<?= $node['Node']['excerpt']; ?>
				</div>
			<?php endif ?>
          	<div class="c-article_byline">
             	<div class="c-article_sharebar">
               		<div class="addthis_inline_share_toolbox"></div>
             	</div>
             	<div class="c-article_pubdate">
	             	<time datetime="<?php echo $node['Node']['publish_start']; ?>">
	             		<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
	             		<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
	             	</time> 
	             	<?php 
	             	//se foi atualizado
	             	if($node['Node']['publish_start'] > $node['Node']['updated']){ ?>
	             		– Atualizado em 
		             	<time datetime="<?php echo $node['Node']['updated']; ?>">
		             		<?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
		             		<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>		
		             	</time>
	             	<?php } 
	             	//se tem o usuario
	             	if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
	             		echo "- por {$node['NoticiumDetail']['jornalista']}";
	             	}else{
	             		echo "- por {$node['User']['name']}";
	             	} ?>
	             </div>
          	</div>
       </header>
       
       
       <div class="row">
          
          <div class="col-md-9 col-md-push-3">
             <div class="c-article_content">
             	<?php 
             	//Configure::write('debug',1);pr($node);
             	if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
             		$imagemUrl = 'http://placehold.it/344x258';
					//se tem imagem
					if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> '344x258'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						
						$imagemUrlOg = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> 'normal'
						);
						$this->Html->meta(
							array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlOg, true)),
							null,
							array('inline' => false)
						);
						
						if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
							$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
						}
						if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
							$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
						}
					} ?>
             		<figure class="c-article_figure c-article_figure--left">
	                	<img src="<?php echo $imagemUrl; ?>" class="c-article_image">
	                	<figcaption class="c-article_figcaption">
		                	<?php if(isset($figCaption)){
		                		echo $figCaption; 
							}
		                	if(isset($figAutor)){
		                		echo ' (Foto: '.$figAutor.')'; 
		                	} ?>
	                	</figcaption>
	              	</figure>
             	<?php }	
             	
				//Corpo da noticia
             	//echo $this->Nodes->body();
				echo $node['Node']['body'];
             	
				//Galeria de imagem
             	if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
             		<div class="c-gallery">
                   		<div class="js-slick c-gallery_large">	
             		
			             	<?php 
			             	
			             	foreach($node['Multiattach'] as $foto){ 
			         			$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $foto['Multiattach']['filename'],
									'dimension'		=> '644x402'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
			         			?>
			                      <div class="c-gallery_item c-gallery_item--large">
			                         <figure class="c-gallery_figure">
			                         	<img src="<?php echo $imagemUrl; ?>" class="c-gallery_image">
			                         </figure>
			                      </div>
	                     <?php } ?>
	                   </div>
	                </div>
                <?php } ?>
             </div>
          </div>
          <div class="col-md-3 col-md-pull-9">
            <?php
            if ($this->Regions->blocks('publicidade-interna-noticia')) {
              echo $this->Regions->blocks('publicidade-interna-noticia');
            }
            ?>
          </div>
       </div>
    </article>
 </section>

<?php } ?>