<?php echo $this->element('node_metas');?>
<section class="s-gallery">
  <article class="c-article">
    <!-- <header class="o-header"><a href="/" class="o-header_title o-header_title--large">Título</a></header> -->
    <header class="o-news_header c-article_header">
      <h1 class="o-news_title c-article_title"><?php echo $node['Node']['title'] ?></h1>
    </header>
    <div class="c-article_content">
      <div class="c-gallery">
        <div class="js-slick c-gallery_nav">
          <?php foreach ($node['Multiattach'] as $attach) { ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <?php
                $imagemUrlArray = array(
                  'plugin'    => 'Multiattach',
                  'controller'=> 'Multiattach',
                  'action'    => 'displayFile', 
                  'admin'     => false,
                  'filename'  => $attach['Multiattach']['filename'],
                  'dimension' => '1920x1200'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                ?>
                <img src="<?php echo $imagemUrl ?>" class="c-gallery_image">
              </figure>
            </div>
          <?php } ?>
        </div>

        <div class="o-widget">
          <header class="o-header o-header_ad o-header_ad--right">
            
          </header>
        </div>

<?php //pr($node)?>
        <div class="js-slick c-gallery_large">
          <?php 
          //Pega a legenda dos detalhes
          if(!empty($node['GaleriumDetail']['ficha_tecnica'])){
            $figCaption = $node['GaleriumDetail']['ficha_tecnica'];
          }
          if(!empty($node['GaleriumDetail']['creditos'])){
            $figAutor = $node['GaleriumDetail']['creditos'];
          }

          foreach ($node['Multiattach'] as $attach) {//pr($attach); 
            //Pega a legenda de cada foto
            if(!empty($attach['Multiattach']['comment'])){
              $figCaption = $attach['Multiattach']['comment'];
            }
            if(!empty($attach['Multiattach']['metaDisplay'])){
              $figAutor = $attach['Multiattach']['metaDisplay'];              
            }
            ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <?php
                $imagemUrlArray = array(
                  'plugin'    => 'Multiattach',
                  'controller'=> 'Multiattach',
                  'action'    => 'displayFile', 
                  'admin'     => false,
                  'filename'  => $attach['Multiattach']['filename'],
                  'dimension' => '1920x1200'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                ?>
                <img src="<?php echo $imagemUrl ?>" class="c-gallery_image">
                <figcaption class="c-article_figcaption">
                  <?php if(isset($figCaption)){
                    echo $figCaption; 
                  }
                  if(isset($figAutor)){
                    if(isset($figCaption) && !empty($figCaption)){
                      echo ' - ';
                    }
                    echo $figAutor; 
                  } ?>
                </figcaption>
              </figure>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="c-article_sharebar">
      <div class="addthis_inline_share_toolbox"></div>
    </div>

    <!-- <div class="c-article_comentarios u-spacer_bottom-200">
      <header class="o-header_title o-header_title--medium">Comentários</header>
    </div> -->
  </article>
</section>
