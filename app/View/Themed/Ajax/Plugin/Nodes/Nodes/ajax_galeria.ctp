<section class="s-gallery">
  <article class="c-article">
    <div class="c-article_content">
      <div class="c-gallery">
        <div class="js-slick c-gallery_nav">
          <?php foreach ($node['Multiattach'] as $attach) { ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <?php
                $imagemUrlArray = array(
                  'plugin'    => 'Multiattach',
                  'controller'=> 'Multiattach',
                  'action'    => 'displayFile', 
                  'admin'     => false,
                  'filename'  => $attach['Multiattach']['filename'],
                  'dimension' => 'square-thumb'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                ?>
                <img src="<?php echo $imagemUrl ?>" class="c-gallery_image">
              </figure>
            </div>
          <?php } ?>
        </div>

        <div class="o-widget">
          <header class="o-header o-header_ad o-header_ad--right">
            
          </header>
        </div>

        <div class="js-slick c-gallery_large">
          <?php 
          //Pega a legenda dos detalhes
          if(!empty($node['GaleriumDetail']['ficha_tecnica'])){
            $figCaption = $node['GaleriumDetail']['ficha_tecnica'];
          }
          if(!empty($node['GaleriumDetail']['creditos'])){
            $figAutor = $node['GaleriumDetail']['creditos'];
          }

          foreach ($node['Multiattach'] as $attach) {//pr($attach); 
            //Pega a legenda de cada foto
            if(!empty($attach['Multiattach']['comment'])){
              $figCaption = $attach['Multiattach']['comment'];
            }
            if(!empty($attach['Multiattach']['metaDisplay'])){
              $figAutor = $attach['Multiattach']['metaDisplay'];              
            }
            ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <?php
                $imagemUrlArray = array(
                  'plugin'    => 'Multiattach',
                  'controller'=> 'Multiattach',
                  'action'    => 'displayFile', 
                  'admin'     => false,
                  'filename'  => $attach['Multiattach']['filename'],
                  'dimension' => '640x400'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
                ?>
                <img src="<?php echo $imagemUrl ?>" class="c-gallery_image">
                <figcaption class="c-article_figcaption">
                  <?php if(isset($figCaption)){
                    echo $figCaption; 
                  }
                  if(isset($figAutor)){
                    if(isset($figCaption) && !empty($figCaption)){
                      echo ' - ';
                    }
                    echo $figAutor; 
                  } ?>
                </figcaption>
              </figure>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </article>
</section>
