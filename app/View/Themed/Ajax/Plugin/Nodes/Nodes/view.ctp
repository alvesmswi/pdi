<?php $this->Nodes->set($node); 

//se tem chamada
if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
	$description = $node['Node']['excerpt'];
}else{
	$description = substr($node['Node']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);
?>
<section class="s-article">
    <article class="c-article">
       <header class="o-header"><span class="o-header_title o-header_title--large">
       	<?php echo $node['Node']['title']; ?>
       </span></header>
       <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-9">
             <div class="c-article_content">
             	<?php 
             	if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
             		$imagemUrl = 'http://placehold.it/344x258';
					//se tem imagem
					if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> '344x258'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
					} ?>
             		<figure class="c-article_figure c-article_figure--left">
	                	<img src="<?php echo $imagemUrl; ?>" class="c-article_image">
	              	</figure>
             	<?php }	
             	
				//Corpo da noticia
             	echo $this->Nodes->body();
             	
				//Galeria de imagem
             	if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
             		<div class="c-gallery">
                   		<div class="js-slick c-gallery_large">	
             		
			             	<?php 
			             	foreach($node['Multiattach'] as $foto){ 
			         			$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $foto['Multiattach']['filename'],
									'dimension'		=> '644x402'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
			         			?>
			                      <div class="c-gallery_item c-gallery_item--large">
			                         <figure class="c-gallery_figure">
			                         	<img src="<?php echo $imagemUrl; ?>" class="c-gallery_image">
			                         </figure>
			                      </div>
	                     <?php } ?>
	                   </div>
	                </div>
                <?php } ?>
             </div>
          </div>
       </div>
    </article>
 </section>

<div id="comments" class="node-comments">
<?php
	$type = $types_for_layout[$this->Nodes->field('type')];

	if ($type['Type']['comment_status'] > 0 && $this->Nodes->field('comment_status') > 0) {
		echo $this->element('Comments.comments');
	}

	if ($type['Type']['comment_status'] == 2 && $this->Nodes->field('comment_status') == 2) {
		echo $this->element('Comments.comments_form');
	}
?>
</div>