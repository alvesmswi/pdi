<?php $this->Nodes->set($node); 

echo $this->element('node_metas');
?>

<section class="s-article">
    <article class="c-article">
    	<header class="o-header">
    		<span class="o-header_title o-header_title--large">
    			Imagem do Dia
    		</span>
    	</header>
       	<header class="o-news_header c-article_header">
			<h1 class="o-news_title c-article_title"><?php echo $node['Node']['title']; ?></h1>
			<?php if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])): ?>
				<div class="o-news_summary c-article_summary">
					<?= $node['Node']['excerpt']; ?>
				</div>
			<?php endif ?>
          	<div class="c-article_byline">
             	<div class="c-article_sharebar">
               		<div class="addthis_inline_share_toolbox"></div>
             	</div>
             	<div class="c-article_pubdate">
	             	<time datetime="<?php echo $node['Node']['publish_start']; ?>">
	             		<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
	             		<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
	             	</time> 
	             	<?php 
	             	//se foi atualizado
	             	if($node['Node']['publish_start'] > $node['Node']['updated']){ ?>
	             		– Atualizado em 
		             	<time datetime="<?php echo $node['Node']['updated']; ?>">
		             		<?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
		             		<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>		
		             	</time>
	             	<?php } 
	             	//se tem o usuario
	             	if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
	             		echo "- por {$node['NoticiumDetail']['jornalista']}";
	             	}else{
	             		echo "- por {$node['User']['name']}";
	             	} ?>
	             </div>
          	</div>
       </header>
      
              <div class="c-article_content">
                <?php 
             	//Configure::write('debug',1);pr($node);
             	if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
             		$imagemUrl = 'http://placehold.it/344x258';
					//se tem imagem
					if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> 'normal'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						
						$imagemUrlOg = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> 'normal'
						);
						$this->Html->meta(
							array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlOg, true).'?node_id='.$node['Node']['id']),
							null,
							array('inline' => false)
						);
						
						if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
							$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
						}
						if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
							$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
						}
					} ?>
             		<figure class="c-article_figure">
	                	<img src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" class="c-article_image">
	                	<figcaption class="c-article_figcaption">
		                	<?php if(isset($figCaption)){
		                		echo $figCaption; 
							}
		                	if(isset($figAutor)){
		                		echo ' (Foto: '.$figAutor.')'; 
		                	} ?>
	                	</figcaption>
	              	</figure>
             	<?php }	
             	
				//Corpo da noticia
             	//echo $this->Nodes->body();
				echo $node['Node']['body'];
             	
				//Galeria de imagem
             	if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
             		<div class="c-gallery">
                   		<div class="js-slick c-gallery_large">	
             		
			             	<?php 
			             	
			             	foreach($node['Multiattach'] as $foto){ 
			         			$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $foto['Multiattach']['filename'],
									'dimension'		=> '644x402'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
			         			?>
			                      <div class="c-gallery_item c-gallery_item--large">
			                         <figure class="c-gallery_figure">
			                         	<img src="<?php echo $imagemUrl; ?>" class="c-gallery_image">
			                         </figure>
			                      </div>
	                     <?php } ?>
	                   </div>
	                </div>
                <?php } ?>
             </div>
    </article>
 </section>

<div id="comments" class="node-comments">
<?php
	$type = $types_for_layout[$this->Nodes->field('type')];

	if ($type['Type']['comment_status'] > 0 && $this->Nodes->field('comment_status') > 0) {
		echo $this->element('Comments.comments');
	}

	if ($type['Type']['comment_status'] == 2 && $this->Nodes->field('comment_status') == 2) {
		echo $this->element('Comments.comments_form');
	}
?>
</div>