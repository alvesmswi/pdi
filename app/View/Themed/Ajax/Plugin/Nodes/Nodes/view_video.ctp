<?php $this->Nodes->set($node); 

echo $this->element('node_metas');
?>

<section class="s-article">
    <article class="c-article">
    	<header class="o-header">
    		<span class="o-header_title o-header_title--large">
    			<?php echo $this->Mswi->categoryName($node['Node']['terms']); ?>
    		</span>
    	</header>
       	<header class="o-news_header c-article_header">
			<h1 class="o-news_title c-article_title"><?php echo $node['Node']['title']; ?></h1>
			<?php if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])): ?>
				<div class="o-news_summary c-article_summary">
					<?= $node['Node']['excerpt']; ?>
				</div>
			<?php endif ?>
          	<div class="c-article_byline">
             	<div class="c-article_sharebar">
               		<div class="addthis_inline_share_toolbox"></div>
             	</div>
             	<div class="c-article_pubdate">
	             	<time datetime="<?php echo $node['Node']['publish_start']; ?>">
	             		<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
	             		<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
	             	</time> 
	             	<?php 
	             	//se foi atualizado
	             	if($node['Node']['publish_start'] > $node['Node']['updated']){ ?>
	             		– Atualizado em 
		             	<time datetime="<?php echo $node['Node']['updated']; ?>">
		             		<?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
		             		<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>		
		             	</time>
	             	<?php } 
	             	//se tem o usuario
	             	if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
	             		echo "- por {$node['NoticiumDetail']['jornalista']}";
	             	}else{
	             		echo "- por {$node['User']['name']}";
	             	} ?>
	             </div>
          	</div>
       </header>
       
       
       <div class="row">
          
          <div class="col-md-9 col-md-push-3">
             <div class="c-article_content">
                <?php if (!empty($node['VideoDetail']['youtube'])) {
                    $youtube =  $node['VideoDetail']['youtube']; // $video recebe o valor da url do youtube (link)
                    preg_match('/(v\/|\?v=)([^&?\/]{5,15})/', $youtube, $video); // Faz o preg_match para retirar caracters ?>

                    <figure class="o-news_embed o-news_embed--16by9">
                        <iframe src="https://www.youtube.com/embed/<?php echo $video[2] ?>" frameborder="0" allowfullscreen></iframe>
                    </figure>
                <?php
                }
             	
				//Corpo da noticia
             	//echo $this->Nodes->body();
				echo $node['Node']['body'];
             	
				//Galeria de imagem
             	if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
             		<div class="c-gallery">
                   		<div class="js-slick c-gallery_large">	
             		
			             	<?php 
			             	
			             	foreach($node['Multiattach'] as $foto){ 
			         			$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $foto['Multiattach']['filename'],
									'dimension'		=> '644x402'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
			         			?>
			                      <div class="c-gallery_item c-gallery_item--large">
			                         <figure class="c-gallery_figure">
			                         	<img src="<?php echo $imagemUrl; ?>" class="c-gallery_image">
			                         </figure>
			                      </div>
	                     <?php } ?>
	                   </div>
	                </div>
                <?php } ?>
             </div>
          </div>
          <div class="col-md-3 col-md-pull-9">
            <?php
            if ($this->Regions->blocks('publicidade-interna-noticia')) {
              echo $this->Regions->blocks('publicidade-interna-noticia');
            }
            ?>
          </div>
       </div>
    </article>
 </section>

<div id="comments" class="node-comments">
<?php
	$type = $types_for_layout[$this->Nodes->field('type')];

	if ($type['Type']['comment_status'] > 0 && $this->Nodes->field('comment_status') > 0) {
		echo $this->element('Comments.comments');
	}

	if ($type['Type']['comment_status'] == 2 && $this->Nodes->field('comment_status') == 2) {
		echo $this->element('Comments.comments_form');
	}
?>
</div>