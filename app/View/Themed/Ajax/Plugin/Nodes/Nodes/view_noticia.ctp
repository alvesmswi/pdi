<?php 
//se foi informado a sidebar
if(isset($sidebar)){
	$sidebar = $sidebar;
}else{
	$sidebar = true;
} 

$this->Nodes->set($node); 

echo $this->element('node_metas');

// Seta variaveis de exibição padrão
$body = $node['Node']['body'];
$exibirImagens = true;
?>

<!--nocache-->
<?php
$exibirAssineJa = false;
// Se for para Bloquear a matéria Parcialmente
if ((isset($autorizado) && !$autorizado) && 
	(isset($user_agent) && !$user_agent) && $node['Node']['exclusivo'] && Configure::read('Paywall.PaywallBloqueioParcial')) {
	// Script de overflow que mostra o bloco e faz o efeito de shadow
	echo '<style>.corpo-noticia {opacity: .3;}</style>';

	// PHP para bloquear o Corpo da Notícia e as Imagens
	$text_plain = strip_tags($body, '<p><br><b>');
	$len = (strlen($text_plain) > Configure::read('Paywall.PaywallQtdeCaracteres')) ? Configure::read('Paywall.PaywallQtdeCaracteres') : (strlen($text_plain) / 2);
	$body = substr($text_plain, 0, $len);
	$exibirImagens = false;
	$exibirAssineJa = true;
}
?>
<!--/nocache-->

<section class="s-article">
    <article class="c-article">
		<?php
		//Se tem algum bloco ativo nesta região
		if ($this->Regions->blocks('publicidade_mobile_destaque_interna')){
			echo $this->Regions->blocks('publicidade_mobile_destaque_interna');
		}
		?>
    	<header class="o-header">
    		<span class="o-header_title o-header_title--large">
				<?php 
				if(isset($node['Editorias']) && !empty($node['Editorias'])){
					echo $node['Editorias'][0]['title'];
				}else{
					echo $this->Mswi->categoryName($node['Node']['terms']);
				}				 
				?>
    		</span>
    	</header>
       	<header class="o-news_header c-article_header">
			<h1 class="o-news_title c-article_title"><?php echo $node['Node']['title']; ?></h1>
			<?php if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])): ?>
				<div class="o-news_summary c-article_summary">
					<?= $node['Node']['excerpt']; ?>
				</div>
			<?php endif ?>
          	<div class="c-article_byline">
             	<div class="c-article_sharebar">
               		<div class="addthis_inline_share_toolbox"></div>
             	</div>
             	<div class="c-article_pubdate">
	             	<time datetime="<?php echo $node['Node']['publish_start']; ?>">
	             		<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
	             		<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
	             	</time> 
	             	<?php 
	             	//se foi atualizado
	             	if($node['Node']['publish_start'] > $node['Node']['updated']){ ?>
	             		– Atualizado em 
		             	<time datetime="<?php echo $node['Node']['updated']; ?>">
		             		<?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
		             		<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>		
		             	</time>
	             	<?php } 
	             	//se tem o usuario
	             	if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
	             		echo "- por {$node['NoticiumDetail']['jornalista']}";
	             	}else{
	             		echo "- por {$node['User']['name']}";
	             	} ?>
	             </div>
          	</div>
       </header>
       
       
       <div class="row">
          
		<?php 
		if($sidebar){
			echo '<div class="col-md-9 col-md-push-3">';
		}else{
			echo '<div class="col-12">';
		} 
		?>
             <div class="c-article_content">
             	<?php 
				 //Configure::write('debug',1);pr($node);
				$cont = 0;				 				
					// pr($n);exit;
				if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
					foreach($node['Multiattach'] as $n){
						if($n['Multiattach']['mime'] != 'application/pdf' &&
						   $n['Multiattach']['mime'] != 'text/plain' &&
						   $n['Multiattach']['mime'] != 'application/msword' &&
						   $n['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' &&
						   $n['Multiattach']['mime'] != 'application/vnd.ms-excel' && $cont == 0){
							$cont = 1;
							$imagemUrl = 'http://placehold.it/344x258';
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach'] && $n['Multiattach']['mime'] != 'application/pdf')){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '344x258'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);

								//prepara a imagem para o lightbox
								$imagemUrlArray['dimension'] = 'normal';
								$imagemLightUrl = $this->Html->url($imagemUrlArray);

								//prepara a imagem para o OG
								$this->Html->meta(
									array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
									null,
									array('inline' => false)
								);
								
								//pega os detalhes da imagem
								$imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

								//largura da imagem
								$this->Html->meta(
									array('property' => 'og:image:width', 'content' => $imageArray[0]),
									null,
									array('inline' => false)
								);
								//Altura da imagem
								$this->Html->meta(
									array('property' => 'og:image:height', 'content' => $imageArray[1]),
									null,
									array('inline' => false)
								);
								//Mime da imagem
								$this->Html->meta(
									array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
									null,
									array('inline' => false)
								);
								//Alt da imagem
								$this->Html->meta(
									array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
									null,
									array('inline' => false)
								);

								if(!empty($n['Multiattach']['metaDisplay'])){
									$figCaption = $n['Multiattach']['metaDisplay'];
								}
								if(!empty($n['Multiattach']['comment'])){
									$figAutor = $n['Multiattach']['comment'];
								}
							} 
							if($sidebar){
							?>
								<figure class="c-article_figure c-article_figure--left">
									<a data-featherlight="image" href="<?php echo $imagemLightUrl.'?node_id='.$node['Node']['id']; ?>">
										<img src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" class="c-article_image">
									</a>
									<figcaption class="c-article_figcaption">
										<?php if(isset($figCaption)){
											echo $figCaption; 
										}
										if(isset($figAutor)){
											echo ' (Foto: '.$figAutor.')'; 
										} ?>
									</figcaption>
								</figure>
						<?php }	
						}
					}
				}

				//Corpo da noticia
				echo '<div class="corpo-noticia">';
					echo $this->Mswi->lerCodigos($body);
				echo '</div>';

				if ($exibirAssineJa) {
					// Chamar element de bloco de Assine agora!
					echo $this->element('paywall_bloqueio_parcial');
				}

				if ($exibirImagens):
					$contagem = 0;
					if(isset($node['Multiattach'])){
						foreach ($node['Multiattach'] as $images) {
							if($images['Multiattach']['mime'] != 'application/pdf' && $images['Multiattach']['mime'] != 'text/plain'
							&& $images['Multiattach']['mime'] != 'application/msword' && $images['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
							&& $images['Multiattach']['mime'] != 'application/vnd.ms-excel'){
								$contagem += 1;
							}
						}
					}
					//Galeria de imagem				
					if(isset($node['Multiattach']) && $contagem > 1){ 
						//  echo '<ul>';
						//  //percorre os anexos procurando pdfs
						//  foreach($node['Multiattach'] as $anexo){ 
						// 	//se é pdf
						// 	if($anexo['Multiattach']['mime']=='application/pdf'){
						// 		$anexoUrlArray = array(
						// 			'plugin'		=> 'Multiattach',
						// 			'controller'	=> 'Multiattach',
						// 			'action'		=> 'displayFile', 
						// 			'admin'			=> false,
						// 			'filename'		=> $anexo['Multiattach']['filename'],
						// 			'dimension'		=> 'normal'
						// 		);
						// 		$anexoUrl = $this->Html->url($anexoUrlArray);
						// 		echo '<li><a href="'.$anexoUrl.'" target="_blank">'.$anexo['Multiattach']['filename'].'</a></li>';
						// 	}
						//  }
						//  echo '</ul>';
						?>

						<div class="c-gallery">
							<div class="js-slick c-gallery_large">	             		
								<?php 
								foreach($node['Multiattach'] as $foto){ 
									//se é não pdf
									if($foto['Multiattach']['mime']!='application/pdf' && $foto['Multiattach']['mime']!='text/plain'
									&& $foto['Multiattach']['mime']!='application/msword' && $foto['Multiattach']['mime']!='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
									&& $foto['Multiattach']['mime']!='application/vnd.ms-excel'){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $foto['Multiattach']['filename'],
											'dimension'		=> '644x402'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										?>
										<div class="c-gallery_item c-gallery_item--large">
											<figure class="c-gallery_figure">
											<img src="<?php echo $imagemUrl; ?>" class="c-gallery_image">
											</figure>
										</div>
							<?php 	} 
								} ?>
							</div>
						</div>
					<?php } ?>
					<!--COMENTÁRIOS DO FACEBOOK-->
					<div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].'/'.$node['Node']['type'].'/'.$node['Node']['slug']?>" data-numposts="5"></div>
				<?php endif; ?>
			 </div>
          </div>

		  	<?php 
			//se tem sidebar
			if($sidebar){
				echo '<div class="col-md-3 col-md-pull-9">';

				if ($this->Regions->blocks('publicidade-interna-noticia')) {
					echo $this->Regions->blocks('publicidade-interna-noticia');
				}
				if(isset($relacionadas) && !empty($relacionadas)){
				?>
					<div class="o-widget">
						<header class="o-header"> 
							<span class="o-header_title">Relacionadas</span>
						</header>
						<?php foreach($relacionadas as $relacionada){ ?>
							<div class="o-news">
								<a href="<?php echo $relacionada['Node']['path'] ?>" class="o-news_content">							
									<span class="o-news_title"><?php echo $relacionada['Node']['title'] ?></span>
								</a>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
				<?php
				if(isset($node['Multiattach'])){
					$aux = false;
					foreach ($node['Multiattach'] as $multi) {
						if($multi['Multiattach']['mime'] == 'application/pdf' || $multi['Multiattach']['mime'] == 'text/plain'
						|| $multi['Multiattach']['mime'] == 'application/msword' || $multi['Multiattach']['mime'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
						|| $multi['Multiattach']['mime'] == 'application/vnd.ms-excel'){
							$aux = true;
						}
					} 
					if($aux){ ?>
						<div class="o-widget">
							<header class="o-header"> 
								<span class="o-header_title">Arquivos</span>
							</header>
							<?php
							foreach ($node['Multiattach'] as $attach) {
								if($attach['Multiattach']['mime'] == 'application/pdf' || $attach['Multiattach']['mime'] == 'text/plain'
								|| $attach['Multiattach']['mime'] == 'application/msword' || $attach['Multiattach']['mime'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
								|| $attach['Multiattach']['mime'] == 'application/vnd.ms-excel'){ ?>
									<div class="o-news">
										<a href="<?php echo Router::url('/', true).'fl/normal/'.$attach['Multiattach']['filename'] ?>" download="<?php echo $attach['Multiattach']['filename'] ?>" class="o-news_content">							
											<span class="o-news_title"><?php echo $attach['Multiattach']['filename'] ?></span>
										</a>
									</div>	
								<?php }
							}?> 
						</div>
				<?php }
			} ?>
          </div>
		<?php } ?>
       </div>
    </article>
 </section>