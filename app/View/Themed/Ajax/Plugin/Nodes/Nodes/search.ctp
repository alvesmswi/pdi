<?php echo $this->element('node_metas'); ?>
<?php 
$cxGoogle = Configure::read('Site.busca_google');
if(!$cxGoogle){?>
	<section class="u-spacer_bottom">
		<header class="o-header">
			<span class="o-header_title">
				<?php 
				//se teve uma pesquisa
				if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
					echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
				}else{
					echo __d('croogo', 'Resultado da pesquisa');
				}
				?>
			</span>
		</header>
		<div class="u-spacer_bottom">
		<div class="row">
			<?php
			//se tem algum registro
			if(!empty($nodes)){
				$contador = 0;
				$total = count($nodes);
				//percorre os nodes
				foreach($nodes as $n){ $contador++;?>
				<div class="col-sm-4">
					<div class="o-news">
						<?php
							$link_noticia = $n['Node']['path'];
							if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
								$link_noticia = $n['NoticiumDetail']['link_externo'];
							}
						?>
						<a href="<?php echo $link_noticia; ?>" class="o-news_content">
							<?php 
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> $imageDimension
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
								?>
								<img src="<?php echo $imagemUrl; ?>" class="<?php echo $imageClass; ?>" alt="<?php echo $imageTitle ?>" /> 
							<?php } ?>
							<span class="o-news_category">
								<?php 
								//se tem chapey
								if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
									echo $n['NoticiumDetail']['chapeu'];
								}else{
									echo $this->Mswi->categoryName($n['Node']['terms']);
								}; 
								?>
							</span>
							<span class="o-news_title"><?php echo $n['Node']['title']; ?></span>

						</a>
				
					</div>
				</div>
			<?php if($contador == 3 || $contador == $total){ $contador=0; ?>
				<div class="clearfix"></div>
			<?php }
			} 
				//Insere o paginado
				echo $this->element('paginator');
			} else { ?>
				<div class="col-sm-12">
					<?php echo __d('croogo', 'Nenhum registro encontrado.'); ?>
				</div>
			<?php } ?>
		</div>
		</div>
	<!--     <div class="o-pagination">
		<nav aria-label="Navegação"> -->
	</section>
<?php }else{?>
	<header class="o-header">
		<span class="o-header_title">
			<?php 
			//se teve uma pesquisa
			if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
				echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
			}else{
				echo __d('croogo', 'Resultado da pesquisa');
			}
			?>
		</span>
	</header>
	<style>
		.gsc-control-cse{
			line-height: normal;
		}
		.gsc-control-cse form{
			line-height: 0;
		}
		.gsc-adBlock{
			display: none;
		}
		.gsc-control-cse table{
			margin: 0;
		}
	</style>
	<script>
		(function() {
			var cx = '<?php echo Configure::read('Site.busca_google')?>';
			var gcse = document.createElement('script');
			gcse.type = 'text/javascript';
			gcse.async = true;
			gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(gcse, s);
		})();
	</script>
	<gcse:search></gcse:search>
<?php }?>	