<?php 
	$url_blogs = (Configure::read('Site.url_blogs')) ? Configure::read('Site.url_blogs') : 'blog';
?>
<?php if (!empty($posts)) { ?>
	<section class="u-spacer_bottom">
	  	<header class="u-spacer_bottom o-header">
	  		<?php if (isset($posts[0]['Blog']['capa'])) { ?>
	  		<img src="<?php echo $posts[0]['Blog']['capa'] ?>" alt="<?php echo $title_for_layout; ?>">
	  		<?php } else { ?>
	  			<span class="o-header_title o-header_title--large"><?php echo $title_for_layout; ?></span>
	  		<?php } ?>
	  	</header>
	  	<?php foreach ($posts as $post) { ?>
			  <article class="o-post">
			    <header class="o-post_header">
			    	<a href="<?php echo "/{$url_blogs}/{$post['Blog']['slug']}/post/{$post['Post']['slug']}" ?>" class="o-post_title"><?php echo $post['Post']['title'] ?></a>
			      <div class="o-post_byline">
							<time class="o-post_datetime"><?php echo date('d/m/Y H:i', strtotime($post['Post']['publish_start'])) ?></time> 
							<?php 
							if(isset($post['User']['name'])){ 
							?>
								<a href="/" class="o-post_author"><?php echo $post['User']['name'] ?></a>
							<?php } ?>
			      </div>
			    </header>
			    <div class="o-news_content o-post_content">
						<?php 
						if (isset($post['Images'][0]['filename']) && !empty($post['Images'][0]['filename'])) { 
							$this->Helpers->load('Medias.MediasImage'); ?>
			    			<img src="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename'], '167'); ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $post['Images'][0]['alt']; ?>">
			    	<?php } ?>
					<?php echo $post['Post']['body'] ?>
			    </div>
			    <!-- <footer class="o-post_footer">
			    	<a href="<?php //echo $this->Html->url($post['Post']['url']) ?>" class="o-post_read-more">
			    		Leia Mais <i class="i-icon i-icon_caret-right"></i>
			    	</a>
			    </footer> -->
			  </article>
		  <?php } ?>

		  <?php 
		  $this->Paginator->options['url'] = array('controller' => 'blogs', 'action' => 'view', 'slug' => $this->params['slug']);
		  echo $this->element('paginator');
		  ?>
	</section>
<?php } ?>