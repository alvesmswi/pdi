<?php 
if (!empty($nodesList)) { ?>
    
    <?php 
    //Se for a Home
    if ($this->here == '/') { ?>
        <section class="s-gallery-list o-layout_s" id="galeriadeimagens">
            <header class="o-header">
                <a href="/galeria" class="o-header_title">Galeria de Imagens</a>
            </header>
            <?php foreach($nodesList as $n){ ?>
                <div class="o-news">
                    <?php 
                    $imagemUrlArray = array(
                        'plugin'        => 'Multiattach',
                        'controller'    => 'Multiattach',
                        'action'        => 'displayFile', 
                        'admin'         => false,
                        'filename'      => $n['Multiattach']['filename'],
                        'dimension'     => '640x400'
                    );
                    $linkImagem = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                    ?>
                    <a href="<?php echo $this->Html->url($n['Node']['path']) ?>" class="o-news_content">
                        <div class="lazyload">
                            <!--
                            <img src="<?php echo $linkImagem?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
                            -->
                        </div>
                        <span class="o-news_title o-news_title--large">
                            <?php echo $n['Node']['title'] ?>
                        </span>
                    </a>
                </div>
            <?php } ?>
        </section>
    <?php 
    }else{ ?>    

        <section class="s-gallery-list u-spacer_bottom">
            <header class="o-header"><a href="/galeria" class="o-header_title">Galeria de Imagens </a></header>
            <div class="row">
              <?php foreach($nodesList as $n){ ?>
              <div class="col-sm-4">
                <div class="o-news">
                  <a href="<?php echo $this->Html->url($n['Node']['path']) ?>" class="o-news_content">
                    <?php 
                    $imagemUrlArray = array(
                        'plugin'        => 'Multiattach',
                        'controller'    => 'Multiattach',
                        'action'        => 'displayFile', 
                        'admin'         => false,
                        'filename'      => $n['Multiattach']['filename'],
                        'dimension'     => '640x400'
                    );
                    $linkImagem = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                    ?>
                    <div class="lazyload">
                        <!--
                        <img src="<?php echo $linkImagem?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
                        -->
                    </div>
                    <span class="o-news_title o-news_title--large"><?php echo $n['Node']['title'] ?></span> 
                  </a>
                </div>
              </div>
              <?php } ?>
            </div>
        </section>
    <?php } ?>
<?php } ?>