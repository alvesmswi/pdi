
<?php
//pr($nodesList);
//se tem algum registro
if(!empty($nodesList)){
    foreach($nodesList as $n){ 
        if (isset($n['VideoDetail']['youtube']) && !empty($n['VideoDetail']['youtube'])) {
        ?>
        <style>
        .videoWrapper {
            position: relative;
            padding-bottom: 56.25%; /* 16:9 */
            padding-top: 25px;
            height: 0;
        }
        .videoWrapper iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        </style>
        <section class="s-list u-spacer_bottom o-news">
            <div class="videoWrapper">
                <?php 
                $youtubeUrl = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$n['VideoDetail']['youtube']);
                echo $youtubeUrl; 
                ?>
            </div>
            <a class="o-news_title o-news_title--large" href="<?php echo $n['Node']['path']; ?>"><?php echo $n['Node']['title']; ?></a>
        </section>
    <?php } ?>
<?php } 
}?>