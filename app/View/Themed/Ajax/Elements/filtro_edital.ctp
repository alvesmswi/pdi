<div class="c-printed">
	<form method="GET" class="c-printed_form">
		<fieldset class="c-printed_fieldset">
	    	<div class="c-printed_flex">
	      		<legend class="c-printed_block">Filtrar:</legend>
	      		<div class="c-printed_block">
	            	<select name="uf" class="form-control selectpicker">
						<option disabled selected value>UF</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='AC'){echo 'selected="selected"';}?> value="AC">AC</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='AL'){echo 'selected="selected"';}?> value="AL">AL</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='AM'){echo 'selected="selected"';}?> value="AM">AM</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='AP'){echo 'selected="selected"';}?> value="AP">AP</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='BA'){echo 'selected="selected"';}?> value="BA">BA</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='CE'){echo 'selected="selected"';}?> value="CE">CE</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='DF'){echo 'selected="selected"';}?> value="DF">DF</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='ES'){echo 'selected="selected"';}?> value="ES">ES</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='GO'){echo 'selected="selected"';}?> value="GO">GO</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='MA'){echo 'selected="selected"';}?> value="MA">MA</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='MG'){echo 'selected="selected"';}?> value="MG">MG</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='MS'){echo 'selected="selected"';}?> value="MS">MS</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='MT'){echo 'selected="selected"';}?> value="MT">MT</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='PA'){echo 'selected="selected"';}?> value="PA">PA</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='PB'){echo 'selected="selected"';}?> value="PB">PB</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='PE'){echo 'selected="selected"';}?> value="PE">PE</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='PI'){echo 'selected="selected"';}?> value="PI">PI</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='PR'){echo 'selected="selected"';}?> value="PR">PR</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='RJ'){echo 'selected="selected"';}?> value="RJ">RJ</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='RN'){echo 'selected="selected"';}?> value="RN">RN</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='RS'){echo 'selected="selected"';}?> value="RS">RS</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='RO'){echo 'selected="selected"';}?> value="RO">RO</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='RR'){echo 'selected="selected"';}?> value="RR">RR</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='SC'){echo 'selected="selected"';}?> value="SC">SC</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='SE'){echo 'selected="selected"';}?> value="SE">SE</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='SP'){echo 'selected="selected"';}?> value="SP">SP</option>
						<option <?php if(isset($this->params->query['uf']) && $this->params->query['uf']=='TO'){echo 'selected="selected"';}?> value="TO">TO</option>
					</select>
	          	</div>
	      		<div class="c-printed_block">
	      			<?php 
	      			$cidade = '';
	      			if(isset($this->params->query['cidade'])){
	      				$cidade = $this->params->query['cidade'];
	      			}
	      			?>
	            	<input class="form-control" type="text" name="cidade" placeholder="Cidade" value="<?php echo $cidade; ?>">
	          	</div>
	          	<div class="c-printed_block">
	            	<select name="ano" class="form-control selectpicker">
	            		<option disabled selected value>Ano</option>
	            		<?php 
	            		$i = 0;
						while ($i <= 5) {
							$ano = date('Y')-$i;
							$selected = '';
							if(isset($this->params->query['ano']) && $this->params->query['ano'] == $ano){
								$selected = 'selected="selected"';
							}
							echo '<option '.$selected.' value="'.$ano.'">'.$ano.'</option>';
							$i++;
						}
	            		?>
	            	</select>
	          	</div>
	      		<div class="c-printed_block">
	        		<select name="mes" class="form-control selectpicker">
	        			<option disabled selected value>Mês</option>
	          			<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='01'){echo 'selected="selected"';}?> value="01">Janeiro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='02'){echo 'selected="selected"';}?> value="02">Fevereiro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='03'){echo 'selected="selected"';}?> value="03">Março</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='04'){echo 'selected="selected"';}?> value="04">Abril</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='05'){echo 'selected="selected"';}?> value="05">Maio</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='06'){echo 'selected="selected"';}?> value="06">Junho</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='07'){echo 'selected="selected"';}?> value="07">Julho</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='08'){echo 'selected="selected"';}?> value="08">Agosto</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='09'){echo 'selected="selected"';}?> value="09">Setembro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='10'){echo 'selected="selected"';}?> value="10">Outubro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='11'){echo 'selected="selected"';}?> value="11">Novembro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='12'){echo 'selected="selected"';}?> value="12">Dezembro</option>
	        		</select>
	      		</div>
	      		<div class="c-printed_block">
	        		<button type="submit" class="btn btn-default"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M14 4q.4 0 .7.3l7 7q.3.3.3.7t-.3.7l-7 7q-.3.3-.7.3-.4 0-.7-.3T13 19q0-.4.3-.7l5.3-5.3H3q-.4 0-.7-.3T2 12t.3-.7.7-.3h15.6l-5.3-5.3Q13 5.4 13 5q0-.4.3-.7T14 4z" fill="currentColor"/></svg><span class="sr-only">Pesquisar</span></button>
	      			<a href="<?php echo $this->here?>" class="btn btn-default">Limpar</a>
	      		</div>
	    	</div>
	  	</fieldset>
	</form>
</div>