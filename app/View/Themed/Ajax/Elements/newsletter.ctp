<!-- Newsletter box -->
<div>
	<form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
		<div class="form-group">
			<header class="o-header"><a href="/newsletter"><span class="o-header_title o-header_title--medium">Receba nossas notícias</span></a></header>
			<input id="email-newsletter" name="email" class="form-control" type="email" required="true" placeholder="Informe o seu e-mail">
		</div>
		<button type="submit" class="btn btn-lg btn-primary">Assinar</button>						
	</form>
</div>