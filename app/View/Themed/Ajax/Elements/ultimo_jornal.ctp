<?php
//se tem algum registro
if(!empty($nodesList)){
    foreach($nodesList as $n){ ?>    
    <section class="s-list u-spacer_bottom">    
        <?php 
            $type = $n['Node']['type'];
            switch ($type) {
                case 'revista':
                    $header_impressos = (Configure::read('Impresso.revista_title')) ? Configure::read('Impresso.revista_title') : 'Revista';
                break;

                case 'especiais':
                    $header_impressos = (Configure::read('Impresso.jornal_title')) ? Configure::read('Impresso.jornal_title') : 'Especiais';
                break;

                case 'anuario':
                    $header_impressos = (Configure::read('Impresso.anuario_title')) ? Configure::read('Impresso.anuario_title') : 'Anuário';
                break;
                
                default:
                    $header_impressos = 'Edição Impressa';
                break;
            }
        ?>
        <header class="o-header">
            <span class="o-header_title o-header_title--small"><?php echo $header_impressos ?></span>
        </header>
        <?php
        $imagemUrl      = 'http://placehold.it/195x314';
        $linkpublish    = $n['Node']['path'];
        $target = '';
        //se tem attach
        if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
            $imagemUrlArray = array(
                'plugin'        => 'Multiattach',
                'controller'    => 'Multiattach',
                'action'        => 'displayFile', 
                'admin'         => false,
                'filename'      => $n['Multiattach']['filename'],
                'dimension'     => '300largura'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
        }
        if (isset($n['PageFlip']) && !empty($n['PageFlip'])) {
            $imagemUrl = $n['PageFlip']['capa'];
        }
        if(isset($n['ImpressoDetail']['link']) && !empty($n['ImpressoDetail']['link'])){
            $linkpublish = $n['ImpressoDetail']['link'];
            $target = '_blank';
        }?>
        <!--nocache-->        
        <?php
        if($n['Node']['exclusivo'] == 1 && !AuthComponent::user('id')){
            $linkpublish = '/users/users/login';
        }
        ?>
        <!--/nocache-->
        <a href="<?php echo $linkpublish ?>" target="<?php echo $target; ?>" class="o-news_content">
            <div class="lazyload">
                <!--
                <img src="<?php echo $imagemUrl; ?>" title="<?php echo $n['Node']['title'];?>" alt="<?php echo $n['Node']['title'];?>"> 
                -->
            </div>
        </a>        
    </section>
<?php } 
}?>