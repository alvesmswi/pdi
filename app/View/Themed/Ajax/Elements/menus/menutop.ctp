<?php 
//Se tem menus
if (!empty($menu['threaded'])) {
	//percorre os itens 
	foreach ($menu['threaded'] as $item) { 
		//trata o link
		$link = $this->Mswi->linkToUrl($item['Link']['link']);
		?>
		<li class="c-header_item">
			<a href="<?php echo $this->Html->url($link); ?>" class="<?php echo $item['Link']['class']; ?>">
	  			<i class="o-icon">
	  				<svg width="12" height="10" viewBox="0 0 12 10" xmlns="http://www.w3.org/2000/svg">
	  					<path d="<?php echo $item['Link']['description']; ?>" fill="currentColor" fill-rule="evenodd"/>
  					</svg>
				</i> <?php echo $item['Link']['title']; ?>
			</a>
		</li>
  	<?php } ?>
<?php } ?>