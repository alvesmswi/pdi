<?php
//Se tem itens no menu
if(!empty($menu['threaded'])){ ?>
	<nav class="c-navigation_nav">
		<ul class="c-navigation_list">
			<?php 
			//Percorre os itens do menu
			foreach($menu['threaded'] as $item){ ?>
				<?php
				$active = (isset($this->request->params['slug']) && $this->request->params['slug'] === $item['Link']['class']) ? 'is--active' : null; 
				$style = '';
				$class = '';
				//se tem cores
				if(isset($item['Link']['color']) && !empty($item['Link']['color'])){
					$style = 'color: '.$item['Link']['color'];
					$class = 'c-navigation_item--colored';
				}
				//Se tem filhos	
				if (!empty($item['children'])) { ?>
					<li class="c-navigation_item <?php echo $class ?> <?php echo $active ?>" style="<?php echo $style;?>">
						<?php $url = $this->Mswi->linkToUrl($item['Link']['link']) ?>
						<a href="<?php echo $this->Html->url($url) ?>" class="c-navigation_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $item['Link']['title'] ?>
						</a>
						<i class="i-icon i-icon_caret-down"></i>
						<ul class="c-navigation_menu">
							<?php foreach ($item['children'] as $child) { ?>
								<li class="c-navigation_item">
									<?php echo $this->Html->link($child['Link']['title'], $this->Mswi->linkToUrl($child['Link']['link']), array('class' => 'c-navigation_button ' . $child['Link']['class'])); ?>
								</li>
							<?php } ?>
						</ul>
					</li>
				<?php } else { ?>
					<li class="c-navigation_item <?php echo $class ?> <?php echo $active ?>" style="<?php echo $style;?>">
						<?php echo $this->Html->link($item['Link']['title'], $this->Mswi->linkToUrl($item['Link']['link']), array('class' => 'c-navigation_button ' . $item['Link']['class'])); ?>
					</li>
				<?php } ?>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>