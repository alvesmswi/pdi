<?php if (!empty($menu['threaded'])) { ?>
  <div class="c-sitemap">
    <div class="c-sitemap_collection">
      <div class="row">
        <?php foreach ($menu['threaded'] as $item) { ?>
          <div class="col-xs-6 col-md-4 col-lg-2">
            <header class="o-header c-sitemap_header">
              <span class="o-header_title c-sitemap_title"><?php echo $item['Link']['title'] ?></span>
            </header>
            <?php if (!empty($item['children'])) { ?>
              <ul class="c-sitemap_list">
                <?php foreach ($item['children'] as $child) { ?>
                  <?php $link = $this->Mswi->linkToUrl($child['Link']['link']); ?>
                  <li><a href="<?php echo $this->Html->url($link) ?>"><?php echo $child['Link']['title'] ?></a></li>
                <?php } ?>
              </ul>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>