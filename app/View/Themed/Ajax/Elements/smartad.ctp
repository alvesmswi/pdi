<?php
/*
Deve ser criado um bloco para cada formato de SmartAd
Colocar no header <script type="application/javascript" src="//ced.sascdn.com/tag/1696/smart.js" async></script>
As informações no bloco devem estar na aba Parametros:
	siteId=88157
	formatId=56208
	termo=economia:615068,esporte:615069
	slug=home:615086
	type=noticia:615086
	plugin=classificados:615086
*/

//se foi informado os dados basico para o smart ad
if((isset($block['Params']['siteId']) && !empty($block['Params']['siteId'])) && (isset($block['Params']['formatId']) && !empty($block['Params']['formatId']))){
	
	//prepara as variaveis
	$siteId 		= $block['Params']['siteId'];
	$formatId 		= $block['Params']['formatId'];

	if(isset($node['Editorias'])){
		$termo 		= $node['Editorias'][0]['slug'];
	}else{
		$termo 		= $this->params['slug'];
	}

	//se foi informado o termo
	if(isset($block['Params']['termo']) && !empty($block['Params']['termo']) && !isset($pageId)){		
		//separa os registros
		$arrayRegistros = explode(',', $block['Params']['termo']);		
		//se encontrou registros
		if(!empty($arrayRegistros)){			
			//percorre cada um deles
			foreach($arrayRegistros as $registro){				
				//pega o termo e o pageId
				$arrayInfos = explode(':', $registro);				
				//se encontrou
				if(!empty($arrayInfos)){
					//percorre os registros
					foreach($arrayInfos as $info){	
						//se este termo tem nesta pagina
						if($arrayInfos[0] == $termo){					
							$pageId = trim($arrayInfos[1]);
							break;
						}
					}
				}				
			}
		}	
	}

	//Se foi informado slug
	if(isset($block['Params']['slug']) && !empty($block['Params']['slug']) && !isset($pageId)){
		//separa os registros
		$arrayRegistros = explode(',', $block['Params']['slug']);
		//se encontrou registros
		if(!empty($arrayRegistros)){			
			//percorre cada um deles
			foreach($arrayRegistros as $registro){				
				//pega o termo e o pageId
				$arrayInfos = explode(':', $registro);				
				//se encontrou
				if(!empty($arrayInfos)){
					//percorre os registros
					foreach($arrayInfos as $info){						
						//se este termo tem nesta pagina
						if($arrayInfos[0] == $this->params['slug']){							
							$pageId = trim($arrayInfos[1]);
							break;
						}
					}					
				}				
			}
		}
	}

	//Se foi informado o tipo de conteúdo
	if(isset($block['Params']['type']) && !empty($block['Params']['type']) && isset($this->params['named']['type']) && !isset($pageId)){
		//separa os registros
		$arrayRegistros = explode(',', $block['Params']['type']);
		//se encontrou registros
		if(!empty($arrayRegistros)){			
			//percorre cada um deles
			foreach($arrayRegistros as $registro){				
				//pega o termo e o pageId
				$arrayInfos = explode(':', $registro);				
				//se encontrou
				if(!empty($arrayInfos)){
					//percorre os registros
					foreach($arrayInfos as $info){						
						//se este termo tem nesta pagina
						if($arrayInfos[0] == $this->params['named']['type']){							
							$pageId = trim($arrayInfos[1]);
							break;
						}
					}					
				}				
			}
		}
	}

	//Se foi informado o tiop de conteúdo
	if(isset($block['Params']['plugin']) && !empty($block['Params']['plugin']) && isset($this->params['plugin']) && !isset($pageId)){
		//separa os registros
		$arrayRegistros = explode(',', $block['Params']['plugin']);
		//se encontrou registros
		if(!empty($arrayRegistros)){			
			//percorre cada um deles
			foreach($arrayRegistros as $registro){				
				//pega o termo e o pageId
				$arrayInfos = explode(':', $registro);				
				//se encontrou
				if(!empty($arrayInfos)){
					//percorre os registros
					foreach($arrayInfos as $info){						
						//se este termo tem nesta pagina
						if($arrayInfos[0] == $this->params['plugin']){							
							$pageId = trim($arrayInfos[1]);
							break;
						}
					}					
				}				
			}
		}
	}

	//se encontrou a página
	if(isset($pageId)){ ?>
		<!--SMARTAD-->		
			
		<script type="application/javascript">
			sas.cmd.push(function() {
				sas.call("onecall", {
					siteId: <?php echo $siteId; ?>,
					pageId: <?php echo $pageId; ?>,
					formatId: <?php echo $formatId; ?>
				});
			});
		</script>
		<div id="sas_<?php echo $formatId; ?>"></div>
		<script type="application/javascript">
			sas.cmd.push(function() {
				sas.render("<?php echo $formatId; ?>");
			});
		</script>
		
<?php } 
}
?>