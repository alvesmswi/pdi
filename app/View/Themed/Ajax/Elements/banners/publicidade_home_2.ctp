<?php
//se tem algum registro
if(!empty($nodesList)){

	shuffle($nodesList);
	/*Configure::write('debug',1);
	print '<div style="display:none;">';
	pr($nodesList);
	print '</div>';*/
	$item = 0;
	foreach($nodesList as $n){ 
		$item++;
	?>
		<div class="c-banner_wrapper">
	    	<?php 
	    	$link = '#';
	    	if(isset($n['BannerDetail']['link']) && !empty($n['BannerDetail']['link'])){
				$link = $n['BannerDetail']['link'];
	    	}
	    	?>
    		<a href="<?php echo $link; ?>" class="c-banner_link" target="_blank">
    			<?php 
				$imagemUrl = 'http://placehold.it/300x150';
				//se tem imagem
				if(isset($n['Multiattach']) && !empty($n['Multiattach'])){

					//se for para usar o preset
					if(!empty($n['Node']['body'])){
						$imagemUrl = $n['Node']['body'];
					}else{
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> 'normal'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
					}					
				}
				?>
				<div class="lazyload">
					<!--
					<img src="<?php echo $imagemUrl; ?>" title="<?php echo $n['Node']['title'];?>" alt="<?php echo $n['Node']['title'];?>" style="margin:0;"> 
					-->
				</div>
			</a>
		</div>
		<?php 
		if($item==1){
			break;
		}
	}	
}?>