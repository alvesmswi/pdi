<?php
//se tem algum registro
if(!empty($nodesList)){
	foreach($nodesList as $n){ ?>
	<section class="s-list u-spacer_bottom">
		<header class="o-header o-header_ad o-header_ad--right">Publicidade</header>
		<a href="<?php echo $n['BannerDetail']['link'] ?>" class="o-news_content">
			<?php 
			$imagemUrl = 'http://placehold.it/265x265';
			//se tem imagem
			if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
				$imagemUrlArray = array(
			        'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
					'dimension'		=> '265x265'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
			}
			?>
			<div class="lazyload">
				<!--
				<img src="<?php echo $imagemUrl; ?>" title="<?php echo $n['Node']['title'];?>" alt="<?php echo $n['Node']['title'];?>"> 
				-->
			</div>
		</a>
	</section>
<?php } 
}?>