<?php 
if (isset($posts) && !empty($posts)) { ?>
	<div class="o-widget">
	  	<header class="o-header"><a href="/blogs" class="o-header_title o-header_title--medium">Colunistas</a></header>
	  	<?php foreach($posts as $n){ ?>
	  		<div class="o-news o-news--blog">
              	<div class="o-news_content o-news_content--blog">
              		<?php 
        			if(isset($n['Blog']['image']) && !empty($n['Blog']['image'])){ ?>
	                	<figure class="o-news_figure o-news_figure--left o-news_figure--blog">
							<div class="lazyload">
								<!--
								<img src="<?php echo $n['Blog']['image']; ?>" alt="<?php echo $n['Blog']['title']; ?>" class="o-news_image">
								-->
							</div>
						</figure>
	                <?php } ?>
                	<a href="/blog/<?php echo $n['Blog']['slug']?>" class="o-news_author o-news_author--blog">
                		<?php echo $n['Blog']['title']; ?>
                	</a>
                	<a href="/blog/<?php echo $n['Blog']['slug']?>/post/<?php echo $n['Post']['slug']?>" class="o-news_title o-news_title--blog">
                		<?php echo $n['Post']['title'] ?>
                	</a>
              	</div>
            </div>
	  	<?php } ?>
	</div>
<?php } ?>