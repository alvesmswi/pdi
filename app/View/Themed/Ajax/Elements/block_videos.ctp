<?php if (!empty($nodesList)): ?>
    <div class="o-widget">
	  <header class="o-header"><a href="/video" class="o-header_title o-header_title--medium">Vídeos</a></header>
		
	  <?php $add_video = true;
	  	foreach($nodesList as $n): ?>
		<div class="o-news">
            <?php 
                if (!empty($n['VideoDetail']['youtube']) && $add_video):
                    $youtube =  $n['VideoDetail']['youtube']; // $video recebe o valor da url do youtube (link)
					preg_match('/(v\/|\?v=)([^&?\/]{5,15})/', $youtube, $video); // Faz o preg_match para retirar caracters 
					//se encontrou a parte do video
					if(isset($video[2]) && !empty($video[2])){
					?>
					
					<div class="lazyload">
						<!--
						<figure class="o-news_embed o-news_embed--16by9">
							<iframe src="https://www.youtube.com/embed/<?php echo $video[2] ?>" frameborder="0" allowfullscreen></iframe>
						</figure>
						-->
					</div>
                <?php
					}
                $add_video = false;
                endif;
            ?>
			<?php 
			$block_videos =  Configure::read('Site.block_videos_menor');
			if(empty($block_videos)){?>
				<a href="<?php echo $n['Node']['path'] ?>" class="o-news_content">
					<?php 
						//se tem imagem
						if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> '66x66'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
							?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" /> 
								-->
							</div>
						<?php } 
					?>
					<span class="o-news_category">
						<?php 
						if (isset($n['VideoDetail']['chapeu']) && !empty($n['VideoDetail']['chapeu'])) {
							echo $n['VideoDetail']['chapeu'];
						} else {
							echo $this->Mswi->categoryName($n['Node']['terms']);
						}; 
						?>
					</span>
					<span class="o-news_title"><?php echo $n['Node']['title'] ?></span>
				</a>
			<?php }
			if(!empty($block_videos)){
				break;
			}
			?>	
		</div>
	  <?php endforeach; ?>

	  <a class="o-btn o-archive_btn" href="/video">Veja mais vídeos</a>
	</div>
<?php endif; ?>