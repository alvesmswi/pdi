<?php 

if (CakePlugin::loaded('Youtube')) {
    $live = json_decode($this->requestAction('/youtube/youtube/live'));

    if (!empty($live)) {
        $video_id = $live[0]->id->videoId;
        echo '<div class="o-widget">';
            echo '<header class="o-header"><span class="o-header_title o-header_title--medium">Ao Vivo</span></header>';
            echo '<iframe width="300" height="200" src="https://www.youtube.com/embed/'.$video_id.'?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        echo '</div>';
    }
}
