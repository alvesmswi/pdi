<?php
//se tem algum registro
if(!empty($nodesList)){ ?>
	<section class="s-archive u-spacer_bottom">
        <header class="o-header"><span class="o-header_title">Últimas Notícias</span></header>
        <div class="o-archive">
			<?php foreach($nodesList as $n){ ?>
                <?php
                    $link_noticia = $n['Node']['path'];
                    if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                        $link_noticia = $n['NoticiumDetail']['link_externo'];
                    }
                ?>
               <a href="<?php echo $link_noticia; ?>" class="o-archive_item">
                  <time datetime="<?php echo $n['Node']['publish_start']?>" class="o-archive_datetime">
                  	<span class="o-archive_time"><?php echo date('H:i', strtotime($n['Node']['publish_start']))?></span> 
                  	<span class="o-archive_date"><?php echo date('d/m', strtotime($n['Node']['publish_start']))?></span>
                  </time>
                  <div class="o-archive_content">
                  	<span class="o-archive_category">
                      <?php 
                      if(isset($n['Editoria']['title']) && !empty($n['Editoria']['title'])){
                        echo $n['Editoria']['title']; 
                      }
                      ?>
                    </span> 
                  	<span class="o-archive_title">
                  		<?php echo $n['Node']['title']; ?>
                  	</span>
                  </div>
               </a>
           <?php } ?>
           <a href="/ultimas-noticias" class="o-btn o-archive_btn">
           	Veja mais notícias
           </a>
        </div>
     </section>
<?php } ?>