<?php
//se tem algum registro
if(!empty($nodesList)){ 
	?>

	<section class="s-list u-spacer_bottom">

		<header class="o-header">
			<a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[0]['Node']['terms']); ?>" class="o-header_title o-header_title--medium">
				<?php 
				if($block['Block']['show_title']){
					echo $block['Block']['title'];
				}else{
					echo $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
				}
				?>
			</a>
		</header>
	    <div class="row">
	    	<?php
			$contador 	= 0;
			$total = count($nodesList);
			//percorre os nodes
			foreach ($nodesList as $n) {
				$contador++;
				if($contador == 1) { ?>
					<div class="col-sm-4">
                    	<div class="o-news">
                    		<?php if(isset($n['Multiattach']) && !empty($n['Multiattach'])){ ?>
	                    		<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
	                    			<?php 
									$imagemUrl = 'http://placehold.it/269x144';
									//se tem imagem
									if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
										$imagemUrlArray = array(
									        'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n['Multiattach']['filename'],
											'dimension'		=> '269x144'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
									}
									?>
									<div class="lazyload">
										<!--
										<img src="<?php echo $imagemUrl; ?>" class="o-news_image"> 
										-->
									</div>
	                			</a>
                			<?php } ?>
							<?php
								$link_noticia = $n['Node']['path'];
								if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
									$link_noticia = $n['NoticiumDetail']['link_externo'];
								}
							?>
                			<a href="<?php echo $link_noticia; ?>" class="o-news_category">
                				<?php 
				             		//se tem chapey
				             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
				             			echo $n['NoticiumDetail']['chapeu'];
				             		}else{
				             			echo $this->Mswi->categoryName($n['Node']['terms']);
				             		}; 
			             		?>
                			</a> 
							<a href="<?php echo $link_noticia; ?>" class="o-news_content">
                				<span class="o-news_title">
                					<?php 
									//se tem titulo da capa
									if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									  echo $n['NoticiumDetail']['titulo_capa'];
									}else{
									  echo $n['Node']['title'];
									}
									?>
                				</span>
                			</a>
                		</div>
                   	</div>
					<div class="col-sm-8">
		       	<?php }else{ ?>
			       	
	          		<?php if($contador == 2){ ?>
						<div class="row">
							<div class="col-sm-6">
			                	<div class="o-news">
			                   		<div class="o-news">
									    <?php
											$link_noticia = $n['Node']['path'];
											if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
												$link_noticia = $n['NoticiumDetail']['link_externo'];
											}
										?>
			                   			<a href="<?php echo $link_noticia; ?>" class="o-news_category">
			                   			<?php 
						             		//se tem chapey
						             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
						             			echo $n['NoticiumDetail']['chapeu'];
						             		}else{
						             			echo $this->Mswi->categoryName($n['Node']['terms']);
						             		}; 
					             		?>
			                   			</a> 
			                   			<a href="<?php echo $link_noticia; ?>" class="o-news_content">
		                   					<?php 
											$imagemUrl = 'http://placehold.it/89x67';
											//se tem imagem
											if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
												$imagemUrlArray = array(
											        'plugin'		=> 'Multiattach',
													'controller'	=> 'Multiattach',
													'action'		=> 'displayFile', 
													'admin'			=> false,
													'filename'		=> $n['Multiattach']['filename'],
													'dimension'		=> '89x67'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
											}
											?>
											<div class="lazyload">
												<!--
												<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left"> 
												-->
											</div>
		                   					<span class="o-news_title">
		                   						<?php 
												//se tem titulo da capa
												if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
												  echo $n['NoticiumDetail']['titulo_capa'];
												}else{
												  echo $n['Node']['title'];
												}
												?>
		                   					</span>
		                   				</a>
		                   			</div>
			                	</div>
	                <?php } ?>
	                <?php if($contador == 3){ ?>
			                <div class="o-news">
			                   <div class="o-news">
							   		<?php
										$link_noticia = $n['Node']['path'];
										if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
											$link_noticia = $n['NoticiumDetail']['link_externo'];
										}
									?>
				                   	<a href="<?php echo $link_noticia; ?>" class="o-news_category">
				                   		<?php 
						             		//se tem chapey
						             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
						             			echo $n['NoticiumDetail']['chapeu'];
						             		}else{
						             			echo $this->Mswi->categoryName($n['Node']['terms']);
						             		}; 
					             		?>
				                   	</a> 
				                   	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
			                   			<?php 
										$imagemUrl = 'http://placehold.it/89x67';
										//se tem imagem
										if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
											$imagemUrlArray = array(
										        'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $n['Multiattach']['filename'],
												'dimension'		=> '89x67'
											);
											$imagemUrl = $this->Html->url($imagemUrlArray);
										}
										?>
										<div class="lazyload">
											<!--
											<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left"> 
											-->
										</div>
			                   			<span class="o-news_title">
			                   				<?php 
											//se tem titulo da capa
											if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
											  echo $n['NoticiumDetail']['titulo_capa'];
											}else{
											  echo $n['Node']['title'];
											}
											?>
			                   			</span>
			                   		</a>
			                   	</div>
			                </div>
		              	</div>
                	<?php } ?>
					<?php if($contador == 4){?>
							<div class="col-sm-6">
		                		<div class="o-news">
		                   			<div class="o-news">
									   	<?php
											$link_noticia = $n['Node']['path'];
											if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
												$link_noticia = $n['NoticiumDetail']['link_externo'];
											}
										?>
		                   				<a href="<?php echo $link_noticia; ?>" class="o-news_category">
			                   				<?php 
							             		//se tem chapey
							             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
							             			echo $n['NoticiumDetail']['chapeu'];
							             		}else{
							             			echo $this->Mswi->categoryName($n['Node']['terms']);
							             		}; 
						             		?>
		                   				</a> 
		                   				<a href="<?php echo $link_noticia; ?>" class="o-news_content">
		                   					<?php 
											$imagemUrl = 'http://placehold.it/89x67';
											//se tem imagem
											if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
												$imagemUrlArray = array(
											        'plugin'		=> 'Multiattach',
													'controller'	=> 'Multiattach',
													'action'		=> 'displayFile', 
													'admin'			=> false,
													'filename'		=> $n['Multiattach']['filename'],
													'dimension'		=> '89x67'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
											}
											?>
											<div class="lazyload">
												<!--
												<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left"> 
												-->
											</div>
		                   					<span class="o-news_title">
		                   						<?php 
												//se tem titulo da capa
												if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
												  echo $n['NoticiumDetail']['titulo_capa'];
												}else{
												  echo $n['Node']['title'];
												}
												?> 
		                   					</span>
		                   				</a>
		                   			</div>
		                		</div>
                	<?php } ?>
                	<?php if($contador == 5){ ?>
			                <div class="o-news">
			                   	<div class="o-news">
								   	<?php
										$link_noticia = $n['Node']['path'];
										if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
											$link_noticia = $n['NoticiumDetail']['link_externo'];
										}
									?>
			                   		<a href="<?php echo $link_noticia; ?>" class="o-news_category">
			                   			<?php 
						             		//se tem chapey
						             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
						             			echo $n['NoticiumDetail']['chapeu'];
						             		}else{
						             			echo $this->Mswi->categoryName($n['Node']['terms']);
						             		}; 
					             		?>
			                   		</a>
			                   		<a href="<?php echo $link_noticia; ?>" class="o-news_content">
			                   			<?php 
										$imagemUrl = 'http://placehold.it/89x67';
										//se tem imagem
										if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
											$imagemUrlArray = array(
										        'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $n['Multiattach']['filename'],
												'dimension'		=> '89x67'
											);
											$imagemUrl = $this->Html->url($imagemUrlArray);
										}
										?>
										<div class="lazyload">
											<!--
											<img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left"> 
											-->
										</div>
			                   			<span class="o-news_title">
			                   				<?php 
											//se tem titulo da capa
											if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
											  echo $n['NoticiumDetail']['titulo_capa'];
											}else{
											  echo $n['Node']['title'];
											}
											?>
			                   			</span>
			                   		</a>
			                   	</div>
			                </div>
						</div>
					</div>
	             	<?php } ?>
			<?php } ?>
			<?php if($contador == $total){?>
				</div>
			<?php } ?>
		<?php }	?>
		</div>
	</section>
<?php } ?>