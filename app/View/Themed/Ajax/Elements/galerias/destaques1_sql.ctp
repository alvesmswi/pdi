<?php
//se tem algum registro
if(!empty($nodesList)){
	//array que vai guardar as noticias em destaques
	$arrayDestaques = array();
	$contador = 0;
	$maximo = 5;
	//percorre os nodes
	foreach($nodesList as $key => $nd){
		//pega a ordenação
		$ordem = $nd['Node']['ordem'];
		//se ainda não pasosu por este item
		if(!isset($arrayDestaques[$ordem])){
			//se tem ordenação
			$arrayDestaques[$ordem] = $nodesList[$key];
			$contador++;
		}
		//se chegou no maximo
		if($contador == $maximo){
			break;
		}
	}
	//substitui os dados selecionados
	$nodesList = array_values($arrayDestaques);
?>

	<div class="s-highlights destaques-1">
		<div class="row o-layout_row">
			<div class="col-md-6 col-lg-7 o-layout_c">
				<?php $n = $nodesList[0]; $c = count($nodesList); ?>
				<div class="o-news">
					<?php
						$link_noticia = $n['Node']['path'];
						if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
							$link_noticia = $n['NoticiumDetail']['link_externo'];
						}
					?>
	            	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
						<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '474largura'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $imagemUrl; ?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
								-->
							</div>
						<?php endif; ?>

	              		<span class="o-news_category">
	              			<?php 
			             		//se tem chapey
			             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
			             			echo $n['NoticiumDetail']['chapeu'];
			             		}else{
			             			echo $n['Editoria']['title'];
			             		} 
		             		?>
	              		</span>
	              		<span class="o-news_title o-news_title--large">
	              			<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	              		</span>
	              		<?php if(isset($n['Node']['excerpt']) && !empty($n['Node']['excerpt'])): ?>
	              		<span class="o-news_summary o-news_summary--small">
	              			<?php echo $n['Node']['excerpt']; ?>
	              		</span>
	              		<?php endif; ?>
	            	</a>
					<?php
					//Se tem algum bloco ativo nesta região
					if ($this->Regions->blocks('publicidade_mobile_destaque_home')){
						echo $this->Regions->blocks('publicidade_mobile_destaque_home');
					}
					?>
	          	</div>
	          	<?php if($c >= 4):
	          		$n = $nodesList[3]; ?>
	          		<div class="o-news">
					  	<?php
							$link_noticia = $n['Node']['path'];
							if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
								$link_noticia = $n['NoticiumDetail']['link_externo'];
							}
						?>
		            	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
		            		<?php 
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) && $n['Multiattach']['mime'] != 'application/pdf'
							   && $n['Multiattach']['mime'] != 'text/plain' && $n['Multiattach']['mime'] != 'application/msword'
							   && $n['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $n['Multiattach']['mime'] != 'application/vnd.ms-excel'):
									$imagemUrlArray = array(
								        'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '66x66'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
								<div class="lazyload">
									<!--
									<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" />
									-->
								</div>
							<?php endif; ?>

		                	<span class="o-news_category">
		                		<?php 
			             		//se tem chapey
			             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
			             			echo $n['NoticiumDetail']['chapeu'];
			             		}else{
									echo $n['Editoria']['title'];
			             		}; 
			             		?>
		                	</span>
		                	<span class="o-news_title">
		                		<?php 
								//se tem titulo da capa
								if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									echo $n['NoticiumDetail']['titulo_capa'];
								}else{
									echo $n['Node']['title'];
								}
								?>
		                	</span>
		            	</a>
			        </div>
	          	<?php endif; // $c >= 4 ?>
			</div>
			<?php if($c > 1): ?>
			<div class="col-md-6 col-lg-5 o-layout_c">
				<?php 
				//região para bloco de video
				echo $this->Regions->blocks('video_destacado');
				foreach($nodesList as $i => $n): 
	    				if($i == 0 || $i == 3) continue;
	    				if($i == 1):
		    	?>
		    	<div class="o-news">
					<?php
						$link_noticia = $n['Node']['path'];
						if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
							$link_noticia = $n['NoticiumDetail']['link_externo'];
						}
					?>
	    			<a href="<?php echo $link_noticia; ?>" class="o-news_content">
			    		<span class="o-news_category">
	                		<?php 
		             		//se tem chapey
		             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
		             			echo $n['NoticiumDetail']['chapeu'];
		             		}else{
								echo $n['Editoria']['title'];
		             		}; 
		             		?>
	                	</span>
	                	<span class="o-news_title o-news_title--large">
	                		<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	                	</span>
		    		

		              	<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) && $n['Multiattach']['mime'] != 'application/pdf'
							   && $n['Multiattach']['mime'] != 'text/plain' && $n['Multiattach']['mime'] != 'application/msword'
							   && $n['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $n['Multiattach']['mime'] != 'application/vnd.ms-excel'):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '329largura'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $imagemUrl; ?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
								-->
							</div>
						<?php endif; ?>
					</a>
		    	</div>
		    	<?php else: // $i == 1 ?>
		    	<div class="o-news">
					<?php
						$link_noticia = $n['Node']['path'];
						if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
							$link_noticia = $n['NoticiumDetail']['link_externo'];
						}
					?>
	            	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
	            		<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) && $n['Multiattach']['mime'] != 'application/pdf'
							   && $n['Multiattach']['mime'] != 'text/plain' && $n['Multiattach']['mime'] != 'application/msword'
							   && $n['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $n['Multiattach']['mime'] != 'application/vnd.ms-excel'):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '66x66'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" />
								-->
							</div>
						<?php endif; ?>

	                	<span class="o-news_category">
	                		<?php 
		             		//se tem chapey
		             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
		             			echo $n['NoticiumDetail']['chapeu'];
		             		}else{
								echo $n['Editoria']['title'];
								
		             		}; 
		             		?>
	                	</span>
	                	<span class="o-news_title">
	                		<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	                	</span>
	            	</a>
		        </div>
		    	<?php endif; // $i == 1 ?>
		    	<?php endforeach; // $nodesList as $i => $n ?>
			</div>
			<?php endif; // $c > 1 ?>
		</div>
	</div>
<?php } ?>