<?php 
//pr($nodesList);
if(!empty($nodesList)){
	$imageDimension = ($block['Region']['alias'] == 'home_right') ? '66x66' : '130x130';
	$imageClass = "o-news_thumb" . (($block['Region']['alias'] == 'home_right') ? ' o-news_thumb--small' : '');
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}	
?>
	<div class="s-list o-layout_s" id="<?php echo $editoriaSlug; ?>">
		<header class="o-header">
			<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>" class="o-header_title o-header_title--medium">
				<?php 
				if($block['Block']['show_title']){
					echo $block['Block']['title'];
				}else{
					echo $editoriaTitle;					
				}
				?>
			</a>
		</header>
		<?php
		// $contador = 0;
		// $total = count($nodesList);
		//percorre os nodes
		foreach($nodesList as $n) {?>
			<div class="o-news">
				<?php
					$link_noticia = $n['Node']['path'];
					if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
						$link_noticia = $n['NoticiumDetail']['link_externo'];
					}
				?>
		    	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
	      			<?php 
					//se tem imagem
					if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) && $n['Multiattach']['mime'] != 'application/pdf'
							   && $n['Multiattach']['mime'] != 'text/plain' && $n['Multiattach']['mime'] != 'application/msword'
							   && $n['Multiattach']['mime'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $n['Multiattach']['mime'] != 'application/vnd.ms-excel'){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> $imageDimension
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
						?>
						<div class="lazyload">
							<!--
							<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="<?php echo $imageClass; ?>" alt="<?php echo $imageTitle ?>" /> 
							-->
						</div>
					 <?php } ?>

             		<span class="o-news_category">
         				<?php 
	             		//se tem chapey
	             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
	             			echo $n['NoticiumDetail']['chapeu'];
	             		}else{
							 if(isset($n['Editoria']['title'])){
								echo $n['Editoria']['title'];
							 }else{
								 echo 'Não informada';
							 }	             			
	             		}; 
	             		?>
         			</span>
                  	<span class="o-news_title">
                  		<?php 
						//se tem titulo da capa
						if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
						  echo $n['NoticiumDetail']['titulo_capa'];
						}else{
						  echo $n['Node']['title'];
						}
						?>
					</span>
		    	</a>
		  	</div>	
      	<?php } ?>	
	</div>
<?php } ?>