<?php
//pr($nodesList);
//se tem algum registro
if(!empty($nodesList)){ 
	?>
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick.css">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick-theme.css">
	<style>
		.foto{
			border: 1px solid #ddd;
			border-radius: 4px;
			-webkit-transition: border .2s ease-in-out;
			padding: 4px;
			margin:0 auto;
			text-align: center;
		}
		.item{
			margin:0 auto;
			text-align: center;
			padding:10px;
		}
		.titulo{
			font-weight:bold;
		}
		.valor{
			font-weight:bold;
		}
		.anunciante{
			font-style:italic;
			margin:0 auto;
			text-align: center;
		}
	</style>

	<section>

		<header class="o-header">
			<span class="o-header_title o-header_title--medium">
				Shopping
			</span>
		</header>
		<div id="carrossel">
			<?php
			//percorre os nodes
			foreach ($nodesList as $n) { ?>
				<div class="item">
					<?php if(isset($n['Multiattach']) && !empty($n['Multiattach'])){ ?>
						<a href="<?php echo $n['ShoppingDetail']['link']; ?>">
							<?php 
							$imagemUrl = 'http://placehold.it/130x130';
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '130x130'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
							}
							?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $imagemUrl; ?>" class="foto"> 
								-->
							</div>
						</a>
					<?php } ?>
					<div class="titulo">
						<a href="<?php echo $n['ShoppingDetail']['link']; ?>">
							<?php 
							echo $n['Node']['title'];							 
							?>	
						</a> 
					</div>
					<?php 
					if(isset($n['ShoppingDetail']['valor']) && !empty($n['ShoppingDetail']['valor'])){ ?>						
						<div class="valor">
							<a href="<?php echo $n['ShoppingDetail']['link']; ?>">
								<?php echo 'R$ '.$n['ShoppingDetail']['valor']; ?>
							</a>
						</div>
					<?php } ?>
					<?php 
					if(isset($n['ShoppingDetail']['anunciante']) && !empty($n['ShoppingDetail']['anunciante'])){ ?>						
						<div class="anunciante">
							<a href="<?php echo $n['ShoppingDetail']['link']; ?>">
								<?php echo $n['ShoppingDetail']['anunciante']; ?>
							</a>
						</div>
					<?php } ?>
				</div>
			<?php }	?>
			</div>
	</section>
<?php } ?>

<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick.min.js"></script>
  
<script>
	 $('#carrossel').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: true,
		centerMode: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					centerMode: true,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					centerMode: true,
					slidesToShow: 1
				}
			}
		]
	  });
</script>