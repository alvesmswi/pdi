<?php
//se tem algum registro
if(!empty($nodesList)){ ?>
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick.css">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick-theme.css">
	<style>
		.foto{
			border: 1px solid #ddd;
			border-radius: 4px;
			-webkit-transition: border .2s ease-in-out;
			padding: 4px;
			margin:0 auto;
			text-align: center;
		}
		.item{
			margin:0 auto;
			text-align: center;
			padding:10px;
		}
		.titulo{
			font-weight:bold;
		}
		.valor{
			font-weight:bold;
		}
		.anunciante{
			font-style:italic;
			margin:0 auto;
			text-align: center;
		}
	</style>

	<section>
		<header class="o-header">
			<span class="o-header_title o-header_title--medium">
				Classificados
			</span>
		</header>
		<div id="carrosselClassificados">
			<?php
			//percorre os nodes
			foreach ($nodesList as $n) { ?>
				<div class="item">
					<?php if(isset($n['Image']) && !empty($n['Image'])){ ?>
						<a href="/classificados/<?php echo $n['Classificado']['slug']; ?>/secao/<?php echo $n['Categoria']['slug']; ?>/anuncio/<?php echo $n['Node']['slug']; ?>">
							<?php $this->Helpers->load('Medias.MediasImage'); ?>
							<div class="lazyload">
								<!--
								<img src="<?php echo $this->MediasImage->imagePreset($n['Image']['filename'], '130x130'); ?>" class="foto"> 
								-->
							</div>
						</a>
					<?php } ?>

					<div class="titulo">
						<a href="/classificados/<?php echo $n['Classificado']['slug']; ?>/secao/<?php echo $n['Categoria']['slug']; ?>/anuncio/<?php echo $n['Node']['slug']; ?>">
							<?php echo $n['Node']['title']; ?>	
						</a> 
					</div>
						
                    <div class="valor">
                        <a href="/classificados/<?php echo $n['Classificado']['slug']; ?>/secao/<?php echo $n['Categoria']['slug']; ?>/anuncio/<?php echo $n['Node']['slug']; ?>">
                            <?php echo 'R$ '.$n['Node']['preco']; ?>
                        </a>
                    </div>
					<?php 
					if(!empty($n['Node']['anunciante_nome'])){ ?>						
						<div class="anunciante">
							<a href="/classificados/<?php echo $n['Classificado']['slug']; ?>/secao/<?php echo $n['Categoria']['slug']; ?>/anuncio/<?php echo $n['Node']['slug']; ?>">
								<?php echo $n['Node']['anunciante_nome']; ?>
							</a>
						</div>
					<?php } ?>
				</div>
			<?php }	?>
			</div>
	</section>
<?php } ?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/slick/slick.min.js"></script>
  
<script>
    $('#carrosselClassificados').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        centerMode: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    slidesToShow: 1
                }
            }
        ]
    });
</script>