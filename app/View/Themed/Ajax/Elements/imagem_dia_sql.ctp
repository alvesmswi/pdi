<?php if (!empty($nodesList)) { 
$titulo = Configure::read('Site.titulo_imagem_do_dia');
if(!empty($titulo)){
	$titulo = $titulo;
}else{
	$titulo = "Imagem do dia";
}	
?>
	<div class="o-widget o-widget_image">
	  <header class="o-header">
	  	<span class="o-header_title o-header_title--medium"><?php echo $titulo?></span>
	  </header>
	  <?php foreach($nodesList as $n){ ?>
		  <figure>
		  	<?php 
		    $imagemUrlArray = array(
		        'plugin'        => 'Multiattach',
		        'controller'    => 'Multiattach',
		        'action'        => 'displayFile', 
		        'admin'         => false,
		        'filename'      => $n['Multiattach']['filename'],
		        'dimension'     => '300largura'
		    );
				$linkImagem = $this->Html->url($imagemUrlArray);
				
				//prepara a imagem para o lightbox
				$imagemUrlArray['dimension'] = 'normal';
				$imagemLightUrl = $this->Html->url($imagemUrlArray);
				?>
				<a data-featherlight="image" href="<?php echo $imagemLightUrl.'?node_id='.$n['Node']['id']; ?>" title="<?php echo $n['Node']['title'] ?>">
				<div class="lazyload">
					<!--	
					<img src="<?php echo $linkImagem?>">
					-->
				</div>
				</a>
		    <figcaption><?php echo $n['Node']['title'] ?></figcaption>
		  </figure>
	  <?php } ?>
	</div>
<?php } ?>