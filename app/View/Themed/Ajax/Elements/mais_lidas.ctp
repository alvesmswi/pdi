<?php if (!empty($nodesList)) { ?>
	<div class="o-widget">
	  <header class="o-header"><a href="/" class="o-header_title o-header_title--medium">Mais lidas</a></header>
	  <?php foreach($nodesList as $n){ ?>
		  <div class="o-news">
		  	<?php
				$link_noticia = $n['Node']['path'];
				if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
					$link_noticia = $n['NoticiumDetail']['link_externo'];
				}
			?>
		  	<a href="<?php echo $link_noticia ?>" class="o-news_content">
		  		<span class="o-news_category">
		  			<?php 
	         		//se tem chapey
	         		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
	         			echo $n['NoticiumDetail']['chapeu'];
	         		}else{
	         			echo $n['Editoria']['title'];
	         		}; 
	         		?>
		  		</span>
		  		<span class="o-news_title"><?php echo $n['Node']['title'] ?></span>
		  	</a>
		  </div>
	  <?php } ?>
	</div>
<?php } ?>