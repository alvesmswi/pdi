<?php 
//se encontrou alguma resultado
if(isset($vejatambem) && !empty($vejatambem)){ ?>
	<section class="s-list u-spacer_bottom">
		<header class="o-header">
			<span class="o-header_title">
				<?php echo 'Veja Também'?>
			</span>
		</header>
	    <div class="row">
	    	<?php
			//se tem algum registro
			if(!empty($vejatambem)){
				$contador = 0;
				$total = count($vejatambem);
				//percorre os nodes
				foreach($vejatambem as $n){ $contador++;?>
		          <div class="col-sm-4">
		             <div class="o-news">
					 	<?php
							$link_noticia = $n['Node']['path'];
							if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
								$link_noticia = $n['NoticiumDetail']['link_externo'];
							}
						?>
		             	<a href="<?php echo $link_noticia; ?>" class="o-news_content">
		             		<?php 
							$imagemUrl = 'http://placehold.it/89x67';
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '89x67'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
							//}
							if(!isset($n['Multiattach']['filename']) || empty($n['Multiattach']['filename'])){
								$imgVeja = Configure::read('Site.logo');
								if(!empty($imgVeja)){
									$imagemUrl = '/'.$imgVeja;
								}else{
									$imagemUrl = '/img/logo.png';
								}
							}
							?>
							<div class="lazyload">
								<!--
								 <img src="<?php echo $imagemUrl; ?>" class="o-news_image o-news_image--left"> 
								-->
							</div>
		             		<?php } ?>
		             		<span class="o-news_title">
		             			<?php echo $n['Node']['title']; ?>
		             		</span>
		             	</a>
		             </div>
		          </div>
	          <?php if($contador == 3){ $contador++; ?>
	          	<div class="clearfix"></div>
	          <?php }
	          } 
	  		} ?>
		</div>
	</section>
<?php } ?>