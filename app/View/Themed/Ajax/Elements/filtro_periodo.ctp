<div class="c-printed">
	<form method="GET" class="c-printed_form">
		<fieldset class="c-printed_fieldset">
	    	<div class="c-printed_flex">
	      		<legend class="c-printed_block">Filtrar por período:</legend>
	          	<div class="c-printed_block">
	            	<select name="ano" class="form-control selectpicker">
	            		<?php 
	            		$i = 0;
						while ($i <= 5) {
							$ano = date('Y')-$i;
							$selected = '';
							if(isset($this->params->query['ano']) && $this->params->query['ano'] == $ano){
								$selected = 'selected="selected"';
							}
							echo '<option '.$selected.' value="'.$ano.'">'.$ano.'</option>';
							$i++;
						}
	            		?>
	            	</select>
	          	</div>
	      		<div class="c-printed_block">
	        		<select name="mes" class="form-control selectpicker">
	        			<option disabled selected value>Todos os meses</option>
	          			<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='01'){echo 'selected="selected"';}?> value="01">Janeiro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='02'){echo 'selected="selected"';}?> value="02">Fevereiro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='03'){echo 'selected="selected"';}?> value="03">Março</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='04'){echo 'selected="selected"';}?> value="04">Abril</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='05'){echo 'selected="selected"';}?> value="05">Maio</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='06'){echo 'selected="selected"';}?> value="06">Junho</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='07'){echo 'selected="selected"';}?> value="07">Julho</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='08'){echo 'selected="selected"';}?> value="08">Agosto</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='09'){echo 'selected="selected"';}?> value="09">Setembro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='10'){echo 'selected="selected"';}?> value="10">Outubro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='11'){echo 'selected="selected"';}?> value="11">Novembro</option>
		              	<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='12'){echo 'selected="selected"';}?> value="12">Dezembro</option>
	        		</select>
	      		</div>
	      		<div class="c-printed_block">
	        		<button type="submit" class="btn btn-default"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M14 4q.4 0 .7.3l7 7q.3.3.3.7t-.3.7l-7 7q-.3.3-.7.3-.4 0-.7-.3T13 19q0-.4.3-.7l5.3-5.3H3q-.4 0-.7-.3T2 12t.3-.7.7-.3h15.6l-5.3-5.3Q13 5.4 13 5q0-.4.3-.7T14 4z" fill="currentColor"/></svg><span class="sr-only">Pesquisar</span></button>
	      			<a href="<?php echo $this->here?>" class="btn btn-default">Limpar</a>
	      		</div>
	    	</div>
	  	</fieldset>
	</form>
</div>