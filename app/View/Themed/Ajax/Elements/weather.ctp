<li class="c-header_item">
<div class="c-weather">
  <div class="c-weather_temp"><span class="c-weather_icon c-weather_icon--small c-weather_icon--normal"></span> 16º C</div>
  <div class="c-weather_panel">
    <header class="c-weather_content c-weather_header"><h4>Curitiba, 15 de Dezembro de 2016</h4></header>
    <div class="c-weather_content">
      <h4 class="c-weather_header">Hoje</h4>
      <span class="c-weather_icon c-weather_icon--large c-weather_icon--normal"></span>
      <div class="c-weather_description">
        <h3 class="c-weather_title">
          <span>16º C</span>
          <span>Nublado</span>
        </h3>
        <span class="c-weather_temperature"><span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> 19º C</span>
        <span class="c-weather_temperature"><span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> 12º C</span>
      </div>
    </div>
    <div class="c-weather_content">
      <h4 class="c-weather_header">Esta semana</h4>
      <div class="c-weather_forecast">
        <time datetime="" class="c-weather_date">
          <span>SEX</span>
          <span>16/12</span>
        </time>
        <span class="c-weather_icon c-weather_icon--large c-weather_icon--sun-clouds"></span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> 19º C</span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> 12º C</span>
      </div>
      <div class="c-weather_forecast">
        <time datetime="" class="c-weather_date">
          <span>SÁB</span>
          <span>17/12</span>
        </time>
        <span class="c-weather_icon c-weather_icon--large c-weather_icon--raining"></span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> 19º C</span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> 12º C</span>
      </div>
      <div class="c-weather_forecast">
        <time datetime="" class="c-weather_date">
          <span>DOM</span>
          <span>18/12</span>
        </time>
        <span class="c-weather_icon c-weather_icon--large c-weather_icon--snowing"></span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> 19º C</span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> 12º C</span>
      </div>
      <div class="c-weather_forecast">
        <time datetime="" class="c-weather_date">
          <span>SEG</span>
          <span>19/12</span>
        </time>
        <span class="c-weather_icon c-weather_icon--large c-weather_icon--sunny"></span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--high"></span> 19º C</span>
        <span class="c-weather_temperature c-weather_temperature--small"><span class="c-weather_icon c-weather_icon--small c-weather_icon--low"></span> 12º C</span>
      </div>
    </div>
  </div>
</div>
</li>