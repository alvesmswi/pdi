var login = $('.c-header_search a').first().text(); // Pega a verificação se o usuário está ou não logado (temporário)
var uri = window.location.pathname; // Pega o PATH (URI) que está acessando
var titulo = 'Atenção! Esta notícia é exclusiva para assinantes!'; // Pegar de algum lugar aqui

// Se for usuário anonimo
if (login.trim() === 'Login') {
    // Adiciona as modificações de HTML
    setHtml();
}

// Função que contém as modificações HTML
async function setHtml() {
    $('.corpo-noticia').css('opacity', .3);
    $('.corpo-noticia').css('height', 180);
    $('.corpo-noticia').css('overflow', 'hidden');
    $('.corpo-noticia').after('<style>.bloco_assine_ja{box-shadow:0 0 30px hsla(0,0%,80%,.9);width:100%;position:relative;clear:both}.bloco_assine_ja:before{content:"";height:150px;width:100%;position:absolute;top:-151px;background:linear-gradient(180deg,hsla(0,0%,100%,0),hsla(0,0%,100%,.6) 33%,hsla(0,0%,100%,.9) 80%,hsla(0,0%,100%,.5));filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#00ff0000", endColorstr="#00ff0000", GradientType=0)}.bloco_assine_ja .title{border-bottom:1px solid #e6e6e6;padding:35px;text-align:center}.buttons_assine{padding:25px 0}.buttons_assine:after,.buttons_assine:before{content:" ";display:table}.buttons_assine .login-normal{width:50%;float:left}.buttons_assine .login-facebook-container,.buttons_assine .login-normal-container{width:50%;margin:0 auto;text-align:center}.buttons_assine .login-facebook{width:50%;float:right;border-left:1px solid #e6e6e6}</style>');
    $('.corpo-noticia').after('<div class="bloco_assine_ja"><div class="title"><h2>'+titulo+'</h2></div><div class="buttons_assine"><div class="login-normal"><div class="login-normal-container"><h3>Já é assinante?</h3> <a href="/users/users/login?redirect='+uri+'" class="btn">Entrar</a></div></div><div class="login-facebook"><div class="login-facebook-container"><h3>Não sou assinante</h3> <a href="/paywall/paywall/cadastro" class="btn">Cadastre-se já!</a></div></div><div style="clear:both;"></div></div></div>');
    $('.fb-comments').remove();
}
