"use strict";

$(document).ready(function () {
    $('#corpo div[data-oembed-url] div').css("max-width", "100%");
});

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        $('.go-top').show();
    } else {
        $('.go-top').hide();
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

//Layload - 08/12/2018 - Alves
function load(img)
{
    img.fadeOut(0, function() {
        img.fadeIn(1000);
    });
}
$('.lazyload').lazyload({load: load});