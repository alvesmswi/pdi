var logado = localStorage.getItem('logado'); // Pega se está logado do localStorage
var login = $('.c-header_search a').first().text(); // Pega a verificação se o usuário está ou não logado (temporário)
var uri = window.location.pathname; // Pega o PATH (URI) que está acessando
var qtdePayWall = 1; // Pegar de algum lugar aqui
var titulo = 'Atenção! Você já atingiu o limite diário de visualização de notícias. Torne-se um assinante!'; // Pegar de algum lugar aqui

// Se for usuário anonimo
if (login.trim() === 'Login') {
    // Se não estiver definida a const logado, seta como não logado, para não setar sempre que o cara recarregar a página
    if (typeof logado === 'object' && logado !== 'false') {
        localStorage.setItem('logado', false);
    }

    // Chama a função que grava quais e quantas notícias leu
    setLeuNode();

    // Pega a quantidade lida, se ela for igual ou maior que a permitida pelo PayWall, bloquea
    var lidas = localStorage.getItem('lidasLength');
    if (lidas >= qtdePayWall) {
        setExclusivo();
    }
} else {
    // Se não estiver logado, seta como logado, para não setar sempre que o cara recarregar a página
    if (typeof logado === 'object' && logado !== 'true') {
        localStorage.setItem('logado', true);
    }
}

// Função que vai incrementar quantas notícias o Usuário já leu
async function setLeuNode() {
    var lidasJson = JSON.parse(localStorage.getItem('lidasJson')) || []; // Cria um array novo ou pega do localStorage

    // Verifica se o PATH (URI), é de um Node do tipo Notícia, se for adiciona no Contador e verificador
    if (uri.indexOf('/noticia/') !== -1) {
        // Se a URI não existir no JSON. Assim evita duplicados, para quando o PayWall for mais que 1 Node para ler
        if ((lidasJson.length === 0 || lidasJson.indexOf(uri) === -1) && lidasJson.length < qtdePayWall) {
            lidasJson.push(uri);
        }
    }

    localStorage.setItem('lidasJson', JSON.stringify(lidasJson)); // Seta um JSON com os Nodes que já leu (URL)
    localStorage.setItem('lidasLength', lidasJson.length); // Seta a Quantidade de Nodes já lidos
}

// Função que vai setar o box de Exclusiva, e pedir para ele se cadastrar
async function setExclusivo() {
    var lidasJson = JSON.parse(localStorage.getItem('lidasJson')); // Pega as Lidas

    // Se é um Node que não está na Lista que ele já leu, bloqueia. Assim ele consegue ler as anteriores de bloquear
    if (lidasJson.indexOf(uri) === -1 && uri.indexOf('/noticia/') !== -1) {
        // Adiciona as modificações de HTML
        setHtml();
    }
}

// Função que contém as modificações HTML
async function setHtml() {
    $('.corpo-noticia').css('opacity', .3);
    $('.corpo-noticia').css('height', 180);
    $('.corpo-noticia').css('overflow', 'hidden');
    $('.corpo-noticia').after('<style>.bloco_assine_ja{box-shadow:0 0 30px hsla(0,0%,80%,.9);width:100%;position:relative;clear:both}.bloco_assine_ja:before{content:"";height:150px;width:100%;position:absolute;top:-151px;background:linear-gradient(180deg,hsla(0,0%,100%,0),hsla(0,0%,100%,.6) 33%,hsla(0,0%,100%,.9) 80%,hsla(0,0%,100%,.5));filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#00ff0000", endColorstr="#00ff0000", GradientType=0)}.bloco_assine_ja .title{border-bottom:1px solid #e6e6e6;padding:35px;text-align:center}.buttons_assine{padding:25px 0}.buttons_assine:after,.buttons_assine:before{content:" ";display:table}.buttons_assine .login-normal{width:50%;float:left}.buttons_assine .login-facebook-container,.buttons_assine .login-normal-container{width:50%;margin:0 auto;text-align:center}.buttons_assine .login-facebook{width:50%;float:right;border-left:1px solid #e6e6e6}</style>');
    $('.corpo-noticia').after('<div class="bloco_assine_ja"><div class="title"><h2>'+titulo+'</h2></div><div class="buttons_assine"><div class="login-normal"><div class="login-normal-container"><h3>Já é assinante?</h3> <a href="/users/users/login?redirect='+uri+'" class="btn">Entrar</a></div></div><div class="login-facebook"><div class="login-facebook-container"><h3>Não sou assinante</h3> <a href="/paywall/paywall/cadastro" class="btn">Cadastre-se já!</a></div></div><div style="clear:both;"></div></div></div>');
    $('.fb-comments').remove();
}
