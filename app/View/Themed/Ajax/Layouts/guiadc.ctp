<?php
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
?>
<!doctype html>
<html class="no-js" lang="pt-BR">

<head>
	<meta charset="utf-8">
	<?php
	echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
	echo $this->fetch('meta');
	?>

	<title>
		<?php echo Configure::read('Site.title'); ?> |
		<?php echo $title_for_layout; ?>
	</title>

	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<!-- Place favicon.ico in the root directory -->
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/vendor.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Source+Sans+Pro:400,700" rel="stylesheet">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/main.css">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/classificados.css">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/pdf.css">
	<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/featherlight/featherlight.css">

	<link rel="stylesheet" href="/guiadc/guia.css">

	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/vendor/modernizr.js"></script>

	<?php
	//Se foi inofmrado o user do analytics
	if (!empty(Configure::read('Service.analytics'))) {
		$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			<?php foreach ($arrayAnalytics as $key => $analytics) { ?>
			ga('create', '<?= trim($analytics) ?>', 'auto', {
				'name': 'id<?= $key ?>'
			});
			ga('id<?= $key ?>.send', 'pageview');
			<?php } ?>

			/**
				* Function that tracks a click on an outbound link in Analytics.
				* This function takes a valid URL string as an argument, and uses that URL string
				* as the event label. Setting the transport method to 'beacon' lets the hit be sent
				* using 'navigator.sendBeacon' in browser that support it.
				*/
			var trackOutboundLink = function (url) {
				ga('send', 'event', 'outbound', 'click', url, {
					'transport': 'beacon',
					'hitCallback': function () {
						document.location = url;
					}
				});
			}
		</script>
	<?php } ?>

	<script type='text/javascript'>
		var googletag = googletag || {};
		googletag.cmd = googletag.cmd || [];
		(function () {
			var gads = document.createElement('script');
			gads.async = true;
			gads.type = 'text/javascript';
			var useSSL = 'https:' == document.location.protocol;
			gads.src = (useSSL ? 'https:' : 'http:') +
				'//www.googletagservices.com/tag/js/gpt.js';
			var node = document.getElementsByTagName('script')[0];
			node.parentNode.insertBefore(gads, node);
		})();
	</script>

	<?php
	//Se foi informado o tempo de refresh
	if (!empty(Configure::read('Site.refresh')) && is_numeric(Configure::read('Site.refresh'))) { ?>
		<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh') ?>" />
	<?php } ?>

	<?php echo Configure::read('Service.header'); ?>

	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

	<?php 
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('header_scripts')){
		echo $this->Regions->blocks('header_scripts');
	}
	?>

</head>

	<body class="loadingInProgress" data-disable-copy>
		<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
		<div class="container-fluid">
			<section class="c-header">
				<div class="o-wrapper">
					<?php
						if ($this->Regions->blocks('super_banner_top')) {
							echo $this->Regions->blocks('super_banner_top');
						}
					?>
				</div>
				<div class="o-wrapper">
					<div class="c-header_flex">
						<div class="c-header_info">
							<ul class="c-header_list">
								<li class="c-header_item">
									<?php echo htmlentities(strftime('%A, %d de %B de %Y')) ?>
								</li>

								<?php echo $this->Menus->menu('menutop', array('element' => 'menus/menutop')); ?>

								<li class="c-header_item">
									<div class="o-social">
										<?php if (!empty(Configure::read('Social.twitter'))) { ?>
										<a href="<?php echo Configure::read('Social.twitter') ?>" class="o-social_link">
											<i class="o-icon o-social_icon">
												<svg width="14" height="12" viewBox="0 0 14 12" xmlns="http://www.w3.org/2000/svg">
													<g fill="none" fill-rule="evenodd">
														<path d="M0-1h14v14H0z" />
														<path d="M14 1.31544c-.56376.18792-1.03356.37584-1.69128.4698.56376-.37584 1.03356-.9396 1.31544-1.50336-.56376.28188-1.22148.56376-1.8792.65772C11.18122.37584 10.42954 0 9.5839 0 7.98657 0 6.7651 1.22148 6.7651 2.8188c0 .1879 0 .4698.09396.6577C4.51006 3.38256 2.349 2.25504.9396.56377c-.28188.4698-.37584.9396-.37584 1.4094 0 1.03355.56376 1.8792 1.31543 2.349-.4698 0-.9396-.09397-1.31545-.37585 0 1.4094 1.03356 2.53692 2.25503 2.72484-.18793.09396-.4698.09396-.7517.09396h-.56374c.37583 1.12752 1.4094 1.97316 2.72483 1.97316-.9396.75167-2.25505 1.22147-3.57048 1.22147H0c1.31544.84564 2.8188 1.22148 4.4161 1.22148 5.26175 0 8.1745-4.32214 8.1745-7.98657v-.4698c.56376-.28188 1.03356-.84564 1.4094-1.4094z"
															fill="#03B3EE" />
													</g>
												</svg>
											</i>
											<span class="sr-only">Twitter</span>
										</a>
										<?php } ?>
										<?php if (!empty(Configure::read('Social.facebook'))) { ?>
										<a href="<?php echo Configure::read('Social.facebook') ?>" class="o-social_link">
											<i class="o-icon o-social_icon">
												<svg width="8" height="14" viewBox="0 0 8 14" xmlns="http://www.w3.org/2000/svg">
													<g fill="none" fill-rule="evenodd">
														<path d="M-3 0h14v14H-3z" />
														<path d="M4.7 14V7.6H7V5H4.7V3.6c0-.6.2-1 1.2-1h1V0H5C3.6 0 2 1.2 2 3.4V5H0v2.6h2V14h2.7z"
															fill="#3B5998" />
													</g>
												</svg>
											</i>
											<span class="sr-only">Facebook</span>
										</a>
										<?php } ?>
										<?php if (!empty(Configure::read('Social.instagram'))) { ?>
										<a href="<?php echo Configure::read('Social.instagram') ?>" class="o-social_link">
											<i class="o-icon o-social_icon">
												<svg viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
													<defs>
														<radialGradient cx="14.9%" cy="100.3%" fx="14.9%" fy="100.3%" r="127.8%"
															id="a">
															<stop stop-color="#FFB140" offset="0%" />
															<stop stop-color="#FF5445" offset="25.6%" />
															<stop stop-color="#FC2B82" offset="59.9%" />
															<stop stop-color="#8E40B7" offset="100%" />
														</radialGradient>
													</defs>
													<g fill="none" fill-rule="evenodd">
														<path d="M0 0h14v14H0z" />
														<path d="M11.6 3.3c0 .4-.4.8-1 .8-.3 0-.7-.3-.7-.7 0-.5.3-1 .7-1 .5 0 1 .5 1 1zM7 9.3c-1.3 0-2.3-1-2.3-2.3 0-1.3 1-2.3 2.3-2.3 1.3 0 2.3 1 2.3 2.3 0 1.3-1 2.3-2.3 2.3zm0-6C5 3.4 3.4 5 3.4 7S5 10.6 7 10.6 10.6 9 10.6 7 9 3.4 7 3.4zm0-2h2.8c.7 0 1 0 1.3.2.5.2.7.3 1 .6l.5 1s.2.5.2 1.2v5.6c0 .7 0 1-.2 1.3-.2.5-.3.7-.6 1l-1 .5s-.5.2-1.2.2H4.2c-.7 0-1 0-1.3-.2-.5-.2-.7-.3-1-.6l-.5-1s-.2-.5-.2-1.2V7 4.2c0-.7 0-1 .2-1.3.2-.5.3-.7.6-1l1-.5s.5-.2 1.2-.2H7zM7 0H4c-.6 0-1 .2-1.6.4-.4 0-.8.4-1.2.8-.4.4-.7.8-.8 1.2L0 4v6c0 .6.2 1 .4 1.6 0 .4.4.8.8 1.2.4.4.8.7 1.2.8L4 14h6c.6 0 1-.2 1.6-.4.4 0 .8-.4 1.2-.8.4-.4.7-.8.8-1.2L14 10V7 4c0-.6-.2-1-.4-1.6 0-.4-.4-.8-.8-1.2-.4-.4-.8-.7-1.2-.8L10 0H7z"
															fill="url(#a)" />
													</g>
												</svg>
											</i>
											<span class="sr-only">Instagram</span>
										</a>
										<?php } ?>
										<?php if (!empty(Configure::read('Social.youtube'))) { ?>
										<a href="<?php echo Configure::read('Social.youtube') ?>" class="o-social_link">
											<i class="o-icon o-social_icon">
												<svg viewBox="0 0 14 10" xmlns="http://www.w3.org/2000/svg">
													<g fill="none" fill-rule="evenodd">
														<path d="M0-2h14v14H0z" />
														<path d="M13.86 2.1807s-.137-.9715-.5565-1.3993C12.771.2197 12.1743.217 11.901.1844 9.9417.0417 7.003.0417 7.003.0417h-.006s-2.9387 0-4.898.1427c-.2738.0326-.87.0353-1.403.597C.2766 1.2094.14 2.1807.14 2.1807S0 3.322 0 4.4627v1.0697c0 1.141.14 2.282.14 2.282s.1365.9715.556 1.3993c.533.5617 1.2325.544 1.544.6027 1.12.1084 4.76.142 4.76.142s2.9418-.0045 4.901-.1472c.2733-.033.87-.0357 1.4025-.5974.4196-.428.5565-1.3994.5565-1.3994s.14-1.1407.14-2.282V4.4626c0-1.1406-.14-2.282-.14-2.282zm-8.3055 4.648L5.554 2.8673 9.3368 4.855 5.5545 6.8286z"
															fill="#CE1312" />
													</g>
												</svg>
											</i>
											<span class="sr-only">Youtube</span>
										</a>
										<?php } ?>
										<a href="/noticia.rss" class="o-social_link">
											<i class="o-icon o-social_icon">
												<svg width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
													<g fill="none" fill-rule="evenodd">
														<path d="M-1-1h14v14H-1z" />
														<path d="M3.25 10.35c0 .45-.15.85-.5 1.15-.3.3-.7.5-1.15.5-.45 0-.8-.15-1.1-.5-.3-.35-.5-.7-.5-1.15C0 9.9.15 9.5.5 9.2c.35-.3.7-.5 1.15-.5.45 0 .85.15 1.15.5.3.3.45.7.45 1.15zm4.4 1.05c0 .15-.05.3-.15.4-.1.1-.25.2-.4.2H5.95c-.15 0-.25-.05-.35-.15-.1-.1-.15-.2-.15-.35-.15-1.3-.65-2.4-1.55-3.35C3 7.25 1.85 6.7.55 6.6.4 6.6.3 6.55.2 6.45c-.1-.1-.2-.25-.2-.4V4.9c0-.15.05-.3.2-.4.1-.1.2-.15.35-.15H.6c.9.05 1.8.3 2.6.7.85.4 1.55.9 2.2 1.55.65.65 1.15 1.4 1.55 2.2.4.85.6 1.7.7 2.6zm4.35.05c0 .15-.05.3-.15.4-.1.1-.25.15-.4.15h-1.2c-.15 0-.25-.05-.4-.15-.1-.1-.15-.2-.15-.35-.05-1.2-.35-2.4-.85-3.5S7.7 5.95 6.9 5.15c-.8-.8-1.75-1.45-2.85-1.95S1.75 2.35.5 2.3c-.15 0-.25-.05-.35-.15-.1-.1-.15-.25-.15-.4V.55C0 .4.05.25.15.15.25.05.4 0 .55 0H.6c1.5.05 2.9.4 4.25 1S7.4 2.45 8.5 3.5c1.05 1.05 1.9 2.25 2.5 3.65.6 1.4.95 2.8 1 4.3z"
															fill="#F07B22" />
													</g>
												</svg>
											</i>
											<span class="sr-only">RSS</span>
										</a>
									</div>
								</li>
							</ul>
						</div>
						<div class="c-header_search">
							<?php 
							//se tem usuário logado
							if(AuthComponent::user('id')){
								echo 'Olá <b>'.AuthComponent::user('name').'</b> | <a href="/users/users/logout">Sair</a>&nbsp;&nbsp;';
							}
							?>
						</div>
						<div class="c-header_search">
							<form class="c-header_search-form" action="/search">
								<label class="sr-only" for="buscaTopo">Buscar no site</label>
								<input type="search" id="buscaTopo" class="o-input c-header_search-input" placeholder="Busque no site" name="q" />
								<button type="submit" class="o-btn c-header_search-btn">
									<i class="o-icon">
										<svg width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
											<path d="M12 10.7143L9.34286 8.05713c.6-.77143.94285-1.8.94285-2.91428C10.2857 2.3143 7.97144 0 5.14287 0 2.3143 0 0 2.3143 0 5.14286c0 2.82857 2.3143 5.14285 5.14286 5.14285 1.11428 0 2.14285-.34284 2.91428-.94284L10.7143 12 12 10.7143zM5.145 8.73c-1.99167 0-3.585-1.59333-3.585-3.585S3.15333 1.56 5.145 1.56c1.99166 0 3.585 1.59333 3.585 3.585S7.13666 8.73 5.145 8.73z"
												fill="#979797" fill-rule="evenodd" />
										</svg>
									</i>
									<span class="hidden-xs sr-only">Buscar</span>
								</button>
								<div class="c-header_search-focus" aria-hidden></div>
							</form>
						</div>
					</div>
				</div>
			</section>

			<section class="c-banner">
				<div class="o-wrapper">
					<div class="c-banner_flex">
						<div class="c-banner_toggle c-banner_toggle_menu">
							<button type="button" class="o-btn c-banner_menu-toggle">
								<i class="o-icon">
									<svg width="12" height="10" viewBox="0 0 12 10" xmlns="http://www.w3.org/2000/svg">
										<g fill="none" fill-rule="evenodd">
											<path d="M-1-2h14v14H-1z" />
											<path class="cc" fill="#555" d="M0 0h12v2H0zM0 4h12v2H0zM0 8h12v2H0z" />
										</g>
									</svg>
								</i>
								<span class="sr-only">Menu</span>
							</button>
						</div>
						<div class="c-banner_logo">
							<a href="/guia" class="c-banner_logo-link">
								<img src="/guiadc/logo.png" class="c-banner_logo-image" alt="<?php echo Configure::read('Site.title'); ?>">
							</a>
						</div>
						<div class="c-banner_toggle c-banner_toggle_search">
							<button type="button" class="o-btn c-banner_search-toggle">
								<i class="o-icon">
									<svg width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
										<path d="M12 10.7143L9.34286 8.05713c.6-.77143.94285-1.8.94285-2.91428C10.2857 2.3143 7.97144 0 5.14287 0 2.3143 0 0 2.3143 0 5.14286c0 2.82857 2.3143 5.14285 5.14286 5.14285 1.11428 0 2.14285-.34284 2.91428-.94284L10.7143 12 12 10.7143zM5.145 8.73c-1.99167 0-3.585-1.59333-3.585-3.585S3.15333 1.56 5.145 1.56c1.99166 0 3.585 1.59333 3.585 3.585S7.13666 8.73 5.145 8.73z"
											class="cc" fill="#979797" fill-rule="evenodd" />
									</svg>
								</i>
								<span class="sr-only">Abrir busca do site</span>
							</button>
						</div>
						<div class="c-banner_banner hidden-xs" style="height:90px; width:728px;">
							<?php
							//Se tem algum bloco ativo nesta região
							if ($this->Regions->blocks('publicidade_top')){
								echo $this->Regions->blocks('publicidade_top');
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="c-navigation">
				<div class="o-wrapper">
					<div class="c-navigation_container">
						<?php
						//Se tem algum bloco ativo nesta região
						/*if ($this->Regions->blocks('menu_principal')) {
						echo $this->Regions->blocks('menu_principal');
						}*/
						?>
						<?php echo $this->Menus->menu('menuguia', array('element' => 'menus/main/main')); ?>
					</div>
				</div>
			</section>

			<?php echo $this->fetch('classificados_header'); ?>

			<section class="s-main">
				<div class="o-wrapper">					

					<?php				
					//vairaveis padrão
					$classe = 'o-main';

					//se foi informado a sidebar
					if(isset($sidebar)){
						$sidebar = $sidebar;
						$classe = '';
					}else{							
						$sidebar = true;
						echo '<div class="row">';
					}

					//se é um node
					if(isset($node['Node']['type'])){
						//se é para exibir com PAGEFLIP
						if((isset($pageflip) && $pageflip)){
							$classe = '';
							$sidebar = false;
						}
					}
					
					?>
					<div class="<?php echo $classe; ?>">
						<?php
						//se tem alguma mensagem de retorno positivo do Form
						if(isset($this->params->query['enviado']) && $this->params->query['enviado']){
							echo '<div class="alert alert-success">Mensagem enviada com sucesso!</div>';
						}

						//Exibe as mensagens
						echo $this->Layout->sessionFlash();

						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('destaques')){
							echo $this->Regions->blocks('destaques');
						}

						//Conteudo do node
						echo $content_for_layout;						

						if($this->Regions->blocks('banner_destaques')){
							echo $this->Regions->blocks('banner_destaques');
						}
						?>

						<div class="row">

							<?php
							//Se tem algum bloco ativo nesta região
							if($this->Regions->blocks('region2')){
								echo $this->Regions->blocks('region2');
							}
							?>

						</div>

						<?php
						if($this->Regions->blocks('banner_bottom')){
							echo $this->Regions->blocks('banner_bottom');
						}
						?>
					</div>

				<?php
				//Se é para exibir a leteral
				if($sidebar){ ?>
					<aside class="o-aside">
						<?php
						if ($this->request->controller == 'classificados' && $this->request->action == 'view_anuncio') {
							echo $this->element('Classificados.anuncio_contato');
						}
						?>

						<?php
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('publicidade_home_3')){
							echo $this->Regions->blocks('publicidade_home_3');
						}
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('publicidade_interna_3')){
							echo $this->Regions->blocks('publicidade_interna_3');
						}
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('right')){
							echo $this->Regions->blocks('right');
						}
						?>
					</aside>
				<?php } ?>

			</div>
		</div>
		<div class="o-wrapper">
			<?php
			if ($this->Regions->blocks('super_banner_bottom')) {
				echo $this->Regions->blocks('super_banner_bottom');
			}
			?>
		</div>
	</section>

	<section class="c-footer">
		<div class="o-wrapper">
			<?php echo $this->Menus->menu('sitemap', array('element' => 'menus/sitemap')); ?>
			<div class="c-footer_content">
				<div class="c-footer_center">
					<?php
				if($this->Regions->blocks('footer')){
				echo $this->Regions->blocks('footer');
				}
				?>
				</div>
			</div>
			<div class="c-footer_content">
				<div class="c-footer_center">
					<div class="c-footer_byline">©
						<?php echo date('Y') ?> - É proibido a reprodução de qualquer conteúdo do site em qualquer meio de comunicação sem
						autorização por escrito de
						<?php echo Configure::read('Site.title') ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/vendor.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/turn.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/pdf.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/pageflip.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/plugins.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/main.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.lazyload-any.min.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/custom.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/featherlight/featherlight.js"></script>
	<?php if(!empty(Configure::read('Service.addthis'))): ?>
	<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis') ?>"></script>
	<?php endif; ?>
	<?php 
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('footer_scripts')){
		echo $this->Regions->blocks('footer_scripts');
	}
	?>

	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/ResizeSensor.min.js"></script>
	<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/theia-sticky-sidebar.min.js"></script>

	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.o-layout_c, .o-main, .o-aside').theiaStickySidebar({
				// Settings
				additionalMarginTop: 30
			});
		});
	</script>

	<?php
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('footer_scripts')){
		echo $this->Regions->blocks('footer_scripts');
	}
	echo $this->fetch('script');
	echo $this->Js->writeBuffer(); // Write cached scripts
	?>

	</body>

</html>