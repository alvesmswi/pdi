<?php if (!empty($nodesList)): ?>
  <div class="row">
    <?php foreach ($nodesList as $n): ?>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="c-poster">
          <?php
            $linkpublish = $n['Node']['path'];
            $target = '_self';
            if (isset($n['ImpressoDetail']['link']) && !empty($n['ImpressoDetail']['link'])) {
              $linkpublish = $n['ImpressoDetail']['link'];
              $target = '_blank';
            }
          ?>
          <a class="c-poster-wrap" href="<?php echo $linkpublish; ?>" target="<?php echo $target; ?>">
            <div class="c-poster__header">
              <div class="c-poster__edtion">
                <?php echo date('d/m/Y', strtotime($n['Node']['publish_start'])); ?>
              </div>
              <div class="c-poster__title">
                <h2>
                  <?php echo $n['Node']['title']; ?>
                  <span class="o-icon glyphicon glyphicon-chevron-right pull-right"></span>
                </h2>
              </div>
            </div>

            <?php
              $imagemUrl = null;
              if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
                $imagemUrlArray = array(
                  'plugin'        => 'Multiattach',
                  'controller'    => 'Multiattach',
                  'action'        => 'displayFile', 
                  'admin'         => false,
                  'filename'      => $n['Multiattach']['filename'],
                  'dimension'     => '622x799'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
              }
              if (isset($n['PageFlip']['images']) && !empty($n['PageFlip']['images'])) {
                $images = json_decode($n['PageFlip']['images'], true);
                $imagemUrl = '/pageflip/pdfs/622x799/'.$n['PageFlip']['id'].'/'.$images[0]['image'];
              }
            ?>
            <img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title']; ?>">
          </a>
        </div>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <?php
          if (isset($n['PageFlip']['images']) && !empty($n['PageFlip']['images'])):
            $pageflipPages = json_decode($n['PageFlip']['images'], true);
            unset($pageflipPages[0]);
        ?>
          <div class="thumbs">
            <ul class="list-group c-list-thumbs">
              <?php foreach ($pageflipPages as $page): ?>
                <li class="list-group-item c-list-thumbs__item">
                  <a href="<?php echo $linkpublish; ?>" title="<?php echo $n['Node']['title']; ?>" target="<?php echo $target; ?>">
                    <?php
                      $imagemUrl = '/pageflip/pdfs/622x799/'.$n['PageFlip']['id'].'/'.$page['image'];
                    ?>
                    <img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title']; ?>">
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
