<?php if (!empty($menu['threaded'])): ?>
  <div class="c-navbar-toggle visible-xs">
    <button class="btn btn-navbar-toggle">Menu
      <span class="caret"></span>
    </button>
  </div>

  <nav class="navbar">
    <ul class="nav list-group">
      <?php foreach ($menu['threaded'] as $item): ?>
        <li class="list-group-item">
          <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>
          <a href="<?php echo $this->Html->url($link) ?>">
            <?php echo $item['Link']['title'] ?>
            <span class="o-icon glyphicon glyphicon-chevron-right pull-right"></span>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </nav>
<?php endif; ?>
