<div class="c-search">
  <form action="/search">
    <div class="input-group input-group-lg">
      <?php
        $q = null;
        if (isset($this->params->query['q']) && !empty($this->params->query['q'])) {
          $q = $this->params->query['q'];
        }
      ?>
      <input class="form-control" type="text" placeholder="Pesquisar" name="q" value="<?php echo $q ?>">
      <span class="input-group-addon">
        <button class="btn" type="submit">
          <span class="sr-only">Pesquisar</span>
          <span class="o-icon glyphicon glyphicon-search"></span>
        </button>
      </span>
    </div>
  </form>
</div>
