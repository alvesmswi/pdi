<nav class="c-navtag">
  <div class="c-navtag__label">Edições:</div>
  <div class="container-fluid">
    <div class="row c-navtag-wrap">
      <a class="c-tag" href="#">000</a>
      <a class="c-tag" href="#">001</a>
      <a class="c-tag" href="#">002</a>
      <a class="c-tag" href="#">003</a>
      <a class="c-tag" href="#">004</a>
      <a class="c-tag" href="#">005</a>
      <a class="c-tag" href="#">006</a>
      <a class="c-tag" href="#">007</a>
      <a class="c-tag" href="#">008</a>
      <a class="c-tag" href="#">009</a>
      <a class="c-tag" href="#">0010</a>
      <a class="c-tag" href="#">0011</a>
      <a class="c-tag" href="#">0012</a>
      <a class="c-tag" href="#">0013</a>
    </div>
  </div>
  <div class="c-paginator pull-right">
    <div class="c-paginator__display panel panel-default">
      <span class="c-paginator-display__range">1-30 </span>de
      <span class="c-paginator-display__total">60</span>
    </div>
    <div class="c-paginator__nav">
      <a class="btn btn-default c-prev" href="#">
        <span class="o-icon glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="btn btn-default c-next" href="#">
        <span class="o-icon glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
  </div>
</nav>
