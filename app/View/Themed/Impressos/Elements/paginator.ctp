<?php if($this->Paginator->params()['pageCount'] > 1): ?>
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <li>
        <?php 
          echo $this->Paginator->prev('&laquo;', array('escape' => false));
        ?>
      </li>
      <li><?php echo $this->Paginator->numbers(array('separator' => null)); ?></li>
      <li>
        <?php 
          echo $this->Paginator->next('&raquo;', array('escape' => false)); 
        ?>
      </li>
    </ul>
  </nav>
<?php endif; ?>