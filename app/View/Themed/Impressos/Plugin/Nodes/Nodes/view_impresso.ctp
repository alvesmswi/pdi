<?php

if (isset($node['PageFlip']) && !empty($node['PageFlip'])) {

    echo $this->element('PageFlip.view_impresso', array(
        'pageflip' => $node['PageFlip'],
        'download' => ((isset($pageflip_download)) ? $pageflip_download : false) 
    ));
    
} else {
    $imagemUrl = null;
    if (isset($node['Multiattach']) && !empty($node['Multiattach'])) {
      $node['Multiattach'] = $node['Multiattach'][0]['Multiattach'];

      $imagemUrlArray = array(
        'plugin'        => 'Multiattach',
        'controller'    => 'Multiattach',
        'action'        => 'displayFile', 
        'admin'         => false,
        'filename'      => $node['Multiattach']['filename'],
        'dimension'     => '622x799'
      );
      $imagemUrl = $this->Html->url($imagemUrlArray);
    }
    if (isset($node['PageFlip']['images']) && !empty($node['PageFlip']['images'])) {
      $images = json_decode($node['PageFlip']['images'], true);
      $imagemUrl = '/pageflip/pdfs/622x799/'.$node['PageFlip']['id'].'/'.$images[0]['image'];
    }
    
    $this->startIfEmpty('destaque');
        echo '<div class="c-poster">';
          echo '<img class="img-responsive" src="'.$imagemUrl.'" alt="'.$node['Node']['title'].'">';
        echo '</div>';
    $this->end();
}
