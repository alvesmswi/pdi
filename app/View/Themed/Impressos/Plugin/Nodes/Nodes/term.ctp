<?php if (!empty($nodes)): ?>
  <?php $cont = 1; ?>
  <?php foreach ($nodes as $k => $n): ?>
    <?php
      $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];

      $linkpublish = $n['Node']['path'];
      $target = '_self';
      if (isset($n['ImpressoDetail']['link']) && !empty($n['ImpressoDetail']['link'])) {
        $linkpublish = $n['ImpressoDetail']['link'];
        $target = '_blank';
      }
      
      if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
        $imagemUrlArray = array(
          'plugin'        => 'Multiattach',
          'controller'    => 'Multiattach',
          'action'        => 'displayFile', 
          'admin'         => false,
          'filename'      => $n['Multiattach']['filename'],
          'dimension'     => '622x799'
        );
        $imagemUrl = $this->Html->url($imagemUrlArray);
      }
      if (isset($n['PageFlip']['images']) && !empty($n['PageFlip']['images'])) {
        $images = json_decode($n['PageFlip']['images'], true);
        $imagemUrl = '/pageflip/pdfs/622x799/'.$n['PageFlip']['id'].'/'.$images[0]['image'];
      }
    ?>
    <?php if ($k == 0): ?>
      <?php
        $nodesList[0] = $n;
        $this->startIfEmpty('destaque');
          echo $this->element('destaque', array('nodesList' => $nodesList));
        $this->end();
      ?>
    <?php elseif ($k > 0 && $k <= 3): ?>
      <?php
        if ($cont == 2) {
          echo '<div class="row">';
        }
      ?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="c-poster">
          <a class="c-poster-wrap" href="<?php echo $linkpublish; ?>" target="<?php echo $target; ?>">
            <div class="c-poster__header">
              <div class="c-poster__edtion">
                <?php echo date('d/m/Y', strtotime($n['Node']['publish_start'])); ?>
              </div>
              <div class="c-poster__title">
                <h2>
                  <?php echo $n['Node']['title']; ?>
                  <span class="o-icon glyphicon glyphicon-chevron-right pull-right"></span>
                </h2>
              </div>
            </div>
            <img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title']; ?>">
          </a>
        </div>
      </div>
      <?php
        if ($cont == 4 || (($k+1) == count($nodesList))) {
          echo '</div>';
          $cont = 0;
        }
      ?>
    <?php else: ?>
      <?php
        if ($cont == 1) {
          echo '<div class="row">';
        }
      ?>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
        <div class="c-poster">
          <a class="c-poster-wrap" href="<?php echo $linkpublish; ?>" target="<?php echo $target; ?>">
            <div class="c-poster__header">
              <div class="c-poster__edtion">
                <?php echo date('d/m/Y', strtotime($n['Node']['publish_start'])); ?>
              </div>
              <div class="c-poster__title">
                <h2>
                  <?php echo $n['Node']['title']; ?>
                  <span class="o-icon glyphicon glyphicon-chevron-right pull-right"></span>
                </h2>
              </div>
            </div>
            <img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title']; ?>">
          </a>
        </div>
      </div>
      <?php
        if ($cont == 4 || (($k+1) == count($nodes))) {
          echo '</div>';
          $cont = 0;
        }
      ?>
    <?php endif; ?>
    <?php $cont++; ?>
  <?php endforeach; ?>

  <?php echo $this->element('paginator'); ?>
<?php else: ?>
  <?php
    $this->startIfEmpty('destaque');
        echo '<div class="c-poster">';
          echo '<div class="alert alert-info" role="alert">Nenhum registro encontrado.</div>';
        echo '</div>';
    $this->end();
  ?>
<?php endif; ?>
