$(() => {

   $('.btn-navbar-toggle').bind('click', () => {
      $('.navbar, .c-search').toggle();
      $('.btn-navbar-toggle').toggleClass('actived');
   });

   // Set vars
   var wSize, thumbWidth, thumbLength;

   //Check the windows and thumb size and the quantity of thumbs and with that multiply and gets the width to list
   function thumbAreaCheck() {
      wSize = $(window).width();
      thumbWidth = $('.c-list-thumbs__item').width() + 15;
      thumbLength = $('.c-list-thumbs').children('li').length;

      if (wSize < 768) {
         $('.c-list-thumbs').css('width', thumbLength * thumbWidth + 'px');
      } else {
         $('.navbar, .c-search').show();
         $('.c-list-thumbs').removeAttr('style');
      }
   }
   thumbAreaCheck();

   $(window).on('resize', () => {
      thumbAreaCheck();
   });
   // console.log(wSize);
   // console.log(thumbWidth);
   // console.log(thumbLength);
});