<?php
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  date_default_timezone_set('America/Sao_Paulo');
?>

<!DOCTYPE html>
<html class="no-js" lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php echo $this->fetch('meta'); ?>

    <title><?php echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); ?></title>

    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/styles/main.css">

    <?php if (!empty(Configure::read('Service.analytics'))):
      $arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
      <script>
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
          ga('create', '<?php echo trim($analytics) ?>', 'auto', {
            'name': 'id<?php echo $key ?>'
          });
          ga('id<?php echo $key ?>.send', 'pageview');
        <?php } ?>

        /**
          * Function that tracks a click on an outbound link in Analytics.
          * This function takes a valid URL string as an argument, and uses that URL string
          * as the event label. Setting the transport method to 'beacon' lets the hit be sent
          * using 'navigator.sendBeacon' in browser that support it.
          */
        var trackOutboundLink = function (url) {
          ga('send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon',
            'hitCallback': function () {
              document.location = url;
            }
          });
        }
      </script>
    <?php endif; ?>
    
    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function () {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <?php if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))): ?>
      <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php endif; ?>
    <?php if ($this->here != '/' && !empty(Configure::read('Site.refresh_internas')) && is_numeric(Configure::read('Site.refresh_internas'))): ?>
      <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
    <?php endif; ?>

    <?php echo Configure::read('Service.header'); ?>

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <?php 
      // Bloco Padrão do CMSWI
      if ($this->Regions->blocks('header_scripts')) {
        echo $this->Regions->blocks('header_scripts');
      }
    ?>
  </head>

  <body itemscope itemtype="http://schema.org/WebPage">
    <!--[if lt IE 10]>
        <p class="browserupgrade">
            Você está usando um navegador <strong>Desatualizado</strong>. 
            Por favor<a href="https://browsehappy.com/?locale=pt-br">se atualize aqui</a> para melhorar sua experiência na internet.
        </p>
    <![endif]-->

    <?php if ($this->Regions->blocks('publicidade_300x100_topo')) { ?>
      <div class="c-ads c-ads--leaderboard-mobile visible-xs">
        <?php echo $this->Regions->blocks('publicidade_300x100_topo'); ?>
      </div>
    <?php } ?>
    <?php if ($this->Regions->blocks('publicidade_728x90_topo')) { ?>
      <div class="c-ads c-ads--leaderboard visible-sm">
        <?php echo $this->Regions->blocks('publicidade_728x90_topo'); ?>
      </div>
    <?php } ?>
    <?php if ($this->Regions->blocks('publicidade_970x90_topo')) { ?>
      <div class="c-ads c-ads--leaderboard-super hidden-xs hidden-sm">
        <?php echo $this->Regions->blocks('publicidade_970x90_topo'); ?>
      </div>
    <?php } ?>

    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
          <header>
            <div class="c-brand">
              <div class="c-box-wrapper">
                <div class="c-brand__img">
                  <?php
                    //Caminho absoluto do arquivo de CSS customizado
                    $themeLogo = WWW_ROOT . 'img' . DS . 'logo.png';
                    
                    //se o arquivo de CSS personalizado existe
                    if (file_exists($themeLogo)) {
                      $themeLogo = '/img/logo.png';
                    } else {
                      $themeLogo = '/theme/' . Configure::read('Site.theme').'/img/logo.png';
                    }
                  ?>
                  <a href="/">
                    <img class="img-responsive" src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
                  </a>
                </div>

                <div class="c-brand__text">
                  <div class="c-date-wrapper">
                    <?php if (Configure::read('Site.cidade')): ?>
                      <span class="c-local"><?php echo Configure::read('Site.cidade').',' ?></span>
                    <?php endif; ?>
                    <span class="c-date"><?php echo htmlentities(strftime('%d')); ?></span> de
                    <span class="c-month"><?php echo htmlentities(strftime('%B')); ?></span>
                    <span class="c-year"><?php echo htmlentities(strftime('%Y')); ?></span>
                  </div>
                </div>
              </div>
            </div>

            <?php echo $this->Menus->menu('main', array('element' => 'menus/main')); ?>

            <?php echo $this->element('search'); ?>

            <?php #echo $this->element('edicoes'); ?>
          </header>
          
          <?php if ($this->Regions->blocks('publicidade_300x250_menu')) { ?>
            <div class="row">
              <div class="c-ads c-ads--retangle visible-xs">
                <?php echo $this->Regions->blocks('publicidade_300x250_menu'); ?>
              </div>
            </div>
          <?php } ?>
        </div>

        <div class="c-poster--highlight col-lg-9 col-md-9 col-sm-8 col-xs-12">
          <?php
            if ($this->here == '/') {
              // Se tem algum bloco ativo nesta região
              if ($this->Regions->blocks('destaque')) {
                echo $this->Regions->blocks('destaque');
              }
            }
          ?>

          <?php echo $this->fetch('destaque'); ?>
        </div>
      </div>

      <?php
        //Exibe as mensagens
        echo $this->Layout->sessionFlash();

        //Conteudo do node
        echo $content_for_layout;
      ?>
    </div>
    
    <?php if ($this->Regions->blocks('publicidade_300x250_rodape')) { ?>
      <div class="c-ads c-ads--retangle visible-xs">
        <?php echo $this->Regions->blocks('publicidade_300x250_rodape'); ?>
      </div>
    <?php } ?>
    <?php if ($this->Regions->blocks('publicidade_728x90_rodape')) { ?>
      <div class="c-ads c-ads--leaderboard visible-sm">
        <?php echo $this->Regions->blocks('publicidade_728x90_rodape'); ?>
      </div>
    <?php } ?>
    <?php if ($this->Regions->blocks('publicidade_970x250_rodape')) { ?>
      <div class="c-ads c-ads--billboard hidden-xs hidden-sm">
        <?php echo $this->Regions->blocks('publicidade_970x250_rodape'); ?>
      </div>
    <?php } ?>
    
    <footer>
      <div class="container text-center text-muted" style="background-color: #efefef; padding-top: 10px;">
        <p>©
          <?php echo date('Y') ?> - É proibido a reprodução de qualquer conteúdo do site em qualquer meio de comunicação sem
          autorização por escrito do <?php echo Configure::read('Site.title') ?>.
        </p>
        <p>Desenvolvido por <a href="https://mswi.com.br/cmswi/">MSWI</a></p>
      </div>
    </footer>

    <script src="/theme/<?php echo Configure::read('Site.theme')?>/scripts/vendor.js"></script>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/scripts/main.js"></script>

    <?php if (!empty(Configure::read('Service.addthis'))): ?>
      <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis'); ?>"></script>
    <?php endif; ?>

    <?php 
      // Bloco Padrão do CMSWI
      if ($this->Regions->blocks('footer_scripts')) {
        echo $this->Regions->blocks('footer_scripts');
      }
    ?>
    
    <?php echo $this->element('sql_dump')?>
  </body>
</html>
