//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar-principal").offset().top > 100) {
        $(".navbar-principal").addClass("top-nav-collapse");
        $(".navbar-principal").css("top","0");
    } else {
        $(".navbar-principal").removeClass("top-nav-collapse");
        $(".navbar-principal").css("top","unset");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});
