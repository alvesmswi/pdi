<?php if(!empty($node['User']['bio'])){?>
    <header>
        <h2>Autor</h2>
    </header>
    <?php 
        $foto = '/img/foto_user/default.png';
        if(isset($node['User']['foto']) && !empty($node['User']['foto'])){
            $foto = '/img/foto_user/' . $node['User']['foto'];
        }
    ?>
    <div class="container" id="autor">
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <img src="<?php echo $foto?>"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-4 col-md-4">
                <blockquote>
                    <p><?php echo $node['User']['name']?></p> <small><cite ><?php echo $node['User']['bio']?></cite></small>
                </blockquote>
                <p><i class="glyphicon glyphicon-envelope"></i><?php echo $node['User']['email']?></p>
            </div>
        </div>
    </div>
<?php }?>    