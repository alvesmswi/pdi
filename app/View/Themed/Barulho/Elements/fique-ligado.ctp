<div class="ab_content" id="about">
  <div class="container">
    <h3 class="tittle_w3ls">Fique Ligado!</h3>
    <div class="inner_sec_info_agile-w3ls">
      <div class="col-md-4 ab-bottom-grid">
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/cultura"><i class="fas fa-globe"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
            <a href="/noticia/term/cultura"><h4>Cultura</h4></a>
            <p>Todas as notícias sobre música, arte, eventos, cinema e todas aquelas coisas que seus pais acham que é besteira.</p>
          </div>
        </div>
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/politica"> <i class="far fa-file-alt"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
            <a href="/noticia/term/politica"><h4>Política</h4></a>
            <p>Resistência, movimentos sociais, divulgação de atos e problematização. Vamos fazer barulho!</p>
          </div>
        </div>
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/trampo"> <i class="far fa-file-alt"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
            <a href="/noticia/term/trampo"><h4>Trampo</h4></a>
            <p>Tudo para você aprender como sobreviver à vida sofrida de universitário e trabalhador (sem surtar).</p>
          </div>
      </div>
      </div>
      <div class="col-md-4 ab-bottom-grid img">
        <img src="/theme/barulho/images/banner.jpg" alt=" " class="img-responsive" />
      </div>
      <div class="col-md-4 ab-bottom-grid second">
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/geek-pop"><i class="fas fa-gift"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
           <a href="/noticia/term/geek-pop"><h4>Geek Pop</h4></a>
            <p>O universo que une desde os jogadores de RPG até os fãs de séries e filmes, uma editoria de nerds para nerds.</p>
          </div>
        </div>
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/lifestyle"><i class="far fa-money-bill-alt"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
           <a href="/noticia/term/lifestyle"><h4>Lifestyle</h4></a>
            <p>Tudo bom, meninas? Desde maquiagem até dicas de comércio para uma vida mais tranquila e saudável.</p>
          </div>
        </div>
        <div class="ab-bottom-text-info">
          <div class="ab-bottom-grid-icon">
           <a href="/noticia/term/night-out"><i class="far fa-file-alt"></i></a>
          </div>
          <div class="ab-bottom-grid-text">
           <a href="/noticia/term/night-out"><h4>Night Out</h4></a>
            <p>Baladas, restaurantes, festas e tudo o que rodeia a noite agitada e mística da cidade modelo.</p>
          </div>
        </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>