<!-- /newsletter_w3ls_agile -->
	<div class="newsletter_w3ls_agile">
		<div class="newsleft_w3_agileinfo-inner">
			<div class="newsleft_w3_agileinfo">
				<h3>NewsLetter</h3>

				<p>Assine para receber Notícias!</p>
			</div>
			<div class="newsright_agileits_wthree">
				<form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
					<input type="email" placeholder="Seu E-mail..." name="email" required="">
					<input type="submit" value="Enviar">
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- //newsletter_w3ls_agile -->