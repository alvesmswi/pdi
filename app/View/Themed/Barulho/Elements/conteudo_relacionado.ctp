<?php 

//se encontrou alguma resultado
if(isset($nodesRelacionados) && !empty($nodesRelacionados)){ ?>
	<section id="vejaTbm">
		<header>
			<h2><?php echo 'Veja Também'?></h2>
		</header>
		<?php
		//se tem algum registro
		if(!empty($nodesRelacionados)){
			$contador = 0;
			$total = count($nodesRelacionados);
			//percorre os nodes
			foreach($nodesRelacionados as $n){ $contador++;?>
				<div class="row">
					<div class="col-sm-4">
						<?php
							$link_noticia = $n['Node']['path'];
							if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
								$link_noticia = $n['NoticiumDetail']['link_externo'];
							}
						?>
						<a href="<?php echo $link_noticia; ?>" class="o-news_content">
							<?php 
							$imagemUrl = 'http://placehold.it/89x67';
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> 'vejaTbm'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
							//}
							if(!isset($n['Multiattach']['filename']) || empty($n['Multiattach']['filename'])){
								$imgVeja = Configure::read('Site.logo');
								if(!empty($imgVeja)){
									$imagemUrl = '/'.$imgVeja;
								}else{
									$imagemUrl = '/img/logo.png';
								}
							}
							?>
								<img src="<?php echo $imagemUrl; ?>" class="thumbnail"> 
							<?php } ?>								
						</a>							
					</div>
					<div class="col-sm-8">
						<div class="titulo">
							<a href="<?php echo $link_noticia; ?>"><h4><?php echo $n['Node']['title']; ?></h4></a>
						</div>
						<div class="chamada">
							<?php echo $n['Node']['excerpt']; ?>
						</div>
					</div>	
				</div>
			<?php if($contador == 3){ $contador++; ?>
			<div class="clearfix"></div>
			<?php }
			} 
		} ?>
	</section>
<?php } ?>