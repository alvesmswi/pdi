<?php 
//pr($nodesList);
if(!empty($nodesList)){ ?>
    <div class="banner_top_wthree_agileinfo">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php 
                $count = count($nodesList);
                $i = 0;
                while ($i < $count){ ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i==0) ? 'active' : ''; ?>"></li>
                <?php 
                $i++;
                } ?>
            </ol>

            <div class="carousel-inner" role="listbox">
                <?php 
                //Percorre os nodes
                foreach($nodesList as $key => $node){ 
                    //testamos se tem endereço customizado, link externo
                    $link_noticia = $node['Node']['path'];
                    if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                        $link_noticia = $node['NoticiumDetail']['link_externo'];
                    }
                    //Se tem imagem
                    if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])){
                        $imagemUrlArray = array(
                            'plugin'		=> 'Multiattach',
                            'controller'	=> 'Multiattach',
                            'action'		=> 'displayFile', 
                            'admin'			=> false,
                            'filename'		=> $node['Multiattach']['filename'],
                            'dimension'		=> 'normal'
                        );
                        $imagemUrl = $this->Html->url($imagemUrlArray);
                    }
                    ?>
                    <div class="item <?php echo ($key==0) ? ' active' : 'item'.$key;?>" style="background:url('<?php echo $imagemUrl; ?>') no-repeat; background-size: cover;">
                        <div class="container">
                            <div class="carousel-caption">
                                <h3><?php echo $node['Node']['title']; ?></h3>
                                <div class="bnr-button">
                                    <a class="act" href="<?php echo $link_noticia; ?>">Leia Mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
            <!-- The Modal -->
        </div>
    </div>
<?php }?>