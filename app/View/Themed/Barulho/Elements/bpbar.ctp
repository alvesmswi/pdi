<style>
  #bpbar{
    background: #ffc800;
    padding: 0px;
    border: 0px;
  }
  #bpbar .icon-bar{
      background-color: #f58220;
  }
  #bpbar .navbar-toggle{
      padding: 0px;
  }
  #bpbar .nav a{
    color: #000;
  }
  #bpbar .navbar-form button {
    border-radius: 0;
    background-color: rgba(0, 0, 0, 0.5);
    color: white;
}
</style>
<nav id="bpbar" class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="https://bemparana.com.br" target="_blank" style="padding:0px;">
        <img src="/theme/barulho/images/bp-logo.png" class="img-logo img-responsive" style="height:100%" />
      </a>
    </div>

    <div id="navbar" class="navbar-collapse collapse navbar-left">
      <ul class="nav navbar-nav">
        <li><a href="https://bemparana.com.br/noticia" target="_blank">Notícias</a></li>
        <li><a href="https://bemparana.com.br/noticia/term/esportes" target="_blank">Esportes</a></li>
        <li><a href="https://bemparana.com.br/guia_cidade/term/cinema" target="_blank">Diversão e Arte</a></li>
        <li><a href="https://bemparana.com.br/noticia/term/politica" target="_blank">Política</a></li>
      </ul>
    </div><!--/.nav-collapse -->

    <form action="https://bemparana.com.br/search" target="_blank" class="navbar-collapse collapse navbar-form navbar-right">
      <div class="form-group">
        <input type="search" class="search form-control" placeholder="Buscar no Bem Paraná" name="q" required="true">
      </div>
      <button type="submit" class="btn">
        <span class="icon fa fa-search" aria-label="Buscar" role="icon"></span>
      </button>
    </form>
  </div>
</nav>
