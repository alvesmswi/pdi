<!--/news-->
<?php if(!empty($nodesList)){?>
	<div class="ab_content">
		<div class="container">
			<h3 class="tittle_w3ls">Últimas Notícias</h3>
			<?php foreach($nodesList as $key => $node){
				if($key > 1){
					break;
				}				
                //Se tem imagem
                if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])){
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $node['Multiattach']['filename'],
                        'dimension'		=> '570x400'
					);
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                }	
				$about_main  = ($key == 0) ? "about-main" : "about-main sec";
				$about_left  = ($key == 0) ? "about-left" : "about-left two";
				$about_right = ($key == 0) ? "about-right" : "about-right two";	
			?>
				<div class="<?php echo $about_main?>">
					<?php if($key == 0){?>
						<div class="col-md-6 <?php echo $about_left?>" style="background: url('<?php echo $imagemUrl?>')no-repeat 0px 0px;"></div>
					<?php }?>
					<div class="col-md-6 <?php echo $about_right?>">
						<h3><?php echo $node['Node']['title']?></h3>
						<p class="paragraph"><?php echo $node['Node']['excerpt']?></p>
						<div class="event-bottom">
							<p class="comment">
								<span class="far fa-calendar-alt"></span><?php 
								$date = date_create($nodesList[0]['Node']['publish_start']);
								echo date_format($date, 'd/m/Y')?>
							</p>
							<ul>
								<!-- <li>
									<a href="">
										<span class="far fa-thumbs-up"></span> 15</a>
								</li> -->
								<li style="color:rgb(133, 134, 134);">
										<span class="fa fa-eye"></span> <?php echo $node['Node']['lida']?></a>
								</li>
								<a class="button-noticia" href="<?php echo $node['Node']['path']?>">Leia Mais</a>
							</ul>
						</div>
					</div>
					<?php if($key == 1){?>
						<div class="col-md-6 <?php echo $about_left?>" style="background: url('<?php echo $imagemUrl?>')no-repeat 0px 0px;"></div>
					<?php }?>
					<div class="clearfix"> </div>
				</div>
			<?php }?>
		</div>
	</div>
<?php }?>
<!--//news-->