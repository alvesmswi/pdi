<!-- /portfolio-->
<?php if(!empty($nodesList)){?>
	<div class="portfolio-project ab_content" id="gallery">
		<div class="container">
			<h3 class="tittle_w3ls"><?php echo $nodesList[0]['Node']['title']?></h3>
			<div class="inner_sec_info_agile-w3ls">
				<div class="col-md-6 portfolio-grids_left">
					<?php foreach($nodesList[0]['Multiattach'] as $image){				
					$bloco_image = explode(':', $image['Multiattach']['comment']);
					if($bloco_image[1] == 'pequena'){
						$size_image =  '285x198';
					}elseif($bloco_image[1] == 'media'){
						$size_image =  '285x396';
					}
					if($bloco_image[0] == 'bloco1'){?>
						<div class="col-md-6 portfolio-grids" data-aos="zoom-in">
							<a href="/fl/640x400/<?php echo $image['Multiattach']['filename']?>" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="/fl/<?php echo $size_image?>/<?php echo $image['Multiattach']['filename']?>" class="img-responsive" alt=" " />
								<div class="b-wrapper">
									<h4><?php echo $image['Multiattach']['metaDisplay']?></h4>
								</div>
							</a>
						</div>
					<?php }}?>
				</div>
				<div class="col-md-6 portfolio-grids sec_img" data-aos="zoom-in">
					<?php foreach($nodesList[0]['Multiattach'] as $image){				
					$bloco_image = explode(':', $image['Multiattach']['comment']);
					if($bloco_image[1] == 'grande'){
						$size_image =  '570x396';
					}					
					if($bloco_image[0] == 'bloco2'){?>
							<a href="/fl/640x400/<?php echo $image['Multiattach']['filename']?>" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="/fl/<?php echo $size_image?>/<?php echo $image['Multiattach']['filename']?>" class="img-responsive" alt=" " />
								<div class="b-wrapper">
									<h4><?php echo $image['Multiattach']['metaDisplay']?></h4>
								</div>
							</a>
					<?php }}?>
				</div>
				<div class="col-md-6 portfolio-grids sec_img" data-aos="zoom-in">
					<?php foreach($nodesList[0]['Multiattach'] as $image){				
					$bloco_image = explode(':', $image['Multiattach']['comment']);
					if($bloco_image[1] == 'grande'){
						$size_image =  '570x396';
					}					
					if($bloco_image[0] == 'bloco3'){?>
							<a href="/fl/640x400/<?php echo $image['Multiattach']['filename']?>" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="/fl/<?php echo $size_image?>/<?php echo $image['Multiattach']['filename']?>" class="img-responsive" alt=" " />
								<div class="b-wrapper">
									<h4><?php echo $image['Multiattach']['metaDisplay']?></h4>
								</div>
							</a>
					<?php }}?>
				</div>
				<div class="col-md-6 portfolio-grids_left">
					<?php foreach($nodesList[0]['Multiattach'] as $image){				
					$bloco_image = explode(':', $image['Multiattach']['comment']);
					if($bloco_image[1] == 'pequena'){
						$size_image =  '285x198';
					}elseif($bloco_image[1] == 'media'){
						$size_image =  '285x396';
					}
					if($bloco_image[0] == 'bloco4'){?>
						<div class="col-md-6 portfolio-grids" data-aos="zoom-in">
							<a href="/fl/640x400/<?php echo $image['Multiattach']['filename']?>" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="/fl/<?php echo $size_image?>/<?php echo $image['Multiattach']['filename']?>" class="img-responsive" alt=" " />
								<div class="b-wrapper">
									<h4><?php echo $image['Multiattach']['metaDisplay']?></h4>
								</div>
							</a>
						</div>
					<?php }}?>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<?php }?>		