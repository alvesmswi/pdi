<?php //pr($posts);exit;?>
	<div class="reviews_sec works" id="testimonials">
		<div class="carousel-reviews broun-block">
			<div class="reviews_main">
				<h3 class="tittle_w3ls cen">Opiniões</h3>
				<div class="inner_sec_info_agile-w3ls">
					<div id="carousel-reviews" class="carousel slide reviews" data-ride="carousel">

						<div class="carousel-inner">
						
						<?php						 
						$countCarousel = 0;
						foreach($posts as $post){
							$countCarousel += 1;
						}
						?>
							<div class="item active">								
								<?php 
								$this->Helpers->load('Medias.MediasImage');
								foreach($posts as $key => $post){
									if($key > 2){
										break;
									}
									$class = "";
									if($key == 0){
										$class = "col-md-4 col-sm-6";
									}elseif($key == 1){
										$class = "col-md-4 col-sm-6 hidden-xs";
									}elseif($key == 2){
										$class = "col-md-4 col-sm-6 hidden-sm hidden-xs";
									}
									$imagemUrl = "";
									//Se tem imagem no post
									if(isset($post['Images']['filename']) && !empty($post['Images']['filename'])){										
										$imagemUrl = $this->MediasImage->imagePreset($post['Images']['filename'], '70x70');
									}
									?>
									<div class="<?php echo $class?>">
										<div class="block-text rel zmin" style="height:260px">
											<a title="" href="#"><?php echo $post['Post']['title']?></a></br></br>
											<p><?php echo $post['Post']['excerpt']?></p>
											<ins class="ab zmin sprite sprite-i-triangle block"></ins>
										</div>									
										<div class="person-text rel">
											<img src="<?php echo $imagemUrl;?>" class="img-responsive" alt=" ">
											<a title="" href="#"><?php echo $post['Post']['jornalista']?></a>
										</div>
									</div>	
								<?php }?>																
							</div>
							<?php
								//count para aparecer ou não as setas esquerda/direita
								$count = 0; 
								//if para não dar efeito carousel se tiver menos de 4 posts
								if($countCarousel > 3){?>
									<div class="item">
										<?php								 
										foreach($posts as $key => $post){
											$count += 1;
											if($key > 5){
												break;
											}
											if($key < 3){
												continue;
											}
											$class = "";
											if($key == 3){
												$class = "col-md-4 col-sm-6";
											}elseif($key == 4){
												$class = "col-md-4 col-sm-6 hidden-xs";
											}elseif($key == 5){
												$class = "col-md-4 col-sm-6 hidden-sm hidden-xs";
											}
											$imagemUrl2 = "";
											//Se tem imagem no post
											if(isset($post['Images']['filename']) && !empty($post['Images']['filename'])){										
												$imagemUrl2 = $this->MediasImage->imagePreset($post['Images']['filename'], '70x70');
											}		
											?>
											<div class="<?php echo $class?>">
												<div class="block-text rel zmin" style="height:260px">
													<a title="" href="#"><?php echo $post['Post']['title']?></a></br></br>
													<p><?php echo $post['Post']['excerpt']?></p>
													<ins class="ab zmin sprite sprite-i-triangle block"></ins>
												</div>									
												<div class="person-text rel">
													<img src="<?php echo $imagemUrl2;?>" class="img-responsive" alt=" ">
													<a title="" href="#"><?php echo $post['Post']['jornalista']?></a>
												</div>
											</div>	
										<?php }?>
									</div>
							<?php }?>
						</div>
						<?php if($count > 3){?>
							<a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="far fa-arrow-alt-circle-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
								<span class="far fa-arrow-alt-circle-right"></span>
							</a>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>