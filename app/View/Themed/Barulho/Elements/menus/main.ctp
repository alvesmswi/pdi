<nav class="navbar navbar-default navbar-fixed-top navbar-principal" style="top:unset;">
    
    <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Chronicle</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="row">
            <div class="col-xs-4 col-md-12 col-sm-6">
                <a href="/"><img id="logo" src="/img/logo.png" class="img-responsive" title="<?php echo Configure::read('Site.title'); ?>"></a>
            </div>
        </div>    
    </div>

    <div class="main-nav-agile-w3layouts">
        <div class="search">
            <form action="/search" method="get">
                <input class="serch" type="search" name="q" id="buscaTopo" placeholder="Buscar..." required="">
                <button type="submit" class="btn1">
                    <i class="fas fa-search"></i>
                </button>
            </form>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse nav-right">
            <?php
            //Se tem itens no menu
            if(!empty($menu['threaded'])){ ?>
                <ul class="nav navbar-nav navbar-right cl-effect-15">
                    <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li>
                    <?php 
                    //Percorre os itens do menu
                    foreach($menu['threaded'] as $item){ 
                        //Se tem filhos	
				        if (!empty($item['children'])) { 
                            $url = $this->Mswi->linkToUrl($item['Link']['link']);
                            ?>
                            <li class="dropdown">
                                <a href="<?php echo $this->Html->url($url) ?>" class="dropdown-toggle effect-3" data-toggle="dropdown">
                                    <?php echo $item['Link']['title'] ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php foreach ($item['children'] as $child) { ?>
                                        <li>
                                            <?php 
                                            echo $this->Html->link(
                                                $child['Link']['title'], 
                                                $this->Mswi->linkToUrl( $child['Link']['link']), 
                                                array(
                                                    'class' => $child['Link']['class']
                                                )
                                            ); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <?php 
                                echo $this->Html->link(
                                    $item['Link']['title'], 
                                    $this->Mswi->linkToUrl($item['Link']['link']), 
                                    array(
                                        'class' => $item['Link']['class']
                                    )
                                ); ?>
                            </li>
                        <?php } ?>
                    <?php } ?>  
                </ul>
            <?php } ?>
        </div>
    </div>
</nav>