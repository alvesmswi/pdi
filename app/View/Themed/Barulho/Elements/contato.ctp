<!-- /contact -->
	<div class="ab_content contact" id="contact">
		<div class="container">
			<h3 class="tittle_w3ls">Envie uma mensagem</h3>
			<div class="inner_sec_info_agile-w3ls">
				<h6>Coloque seus dados aí embaixo!</h6>
				<div class="contcat-form-w3layouts">
					<?php 
						echo $this->Form->create('Message', array(
							'url' => array(
								'plugin' => 'contacts',
								'controller' => 'contacts',
								'action' => 'view',
								// $contact['Contact']['alias'],
							),
						));
					?>
					<div class="user-tp">
						<?php echo $this->Form->input('Message.name', array('label'=>array('class'=>'head', 'text'=>'Nome'), 'div'=>false, 'required'=> true)); ?>
					</div>
					<div class="user-tp">
						<?php echo $this->Form->input('Message.email', array('label'=>array('class'=>'head', 'text'=>'E-mail'), 'type'=>'email', 'div'=>false, 'required'=> true)); ?>
					</div>
					<div class="user-tp">
						<?php echo $this->Form->input('Message.fone', array('label'=>array('class'=>'head', 'text'=>'Telefone'), 'div'=>false, 'required'=> true)); ?>
					</div>																	
					<div class="user-tp frame">
						<?php echo $this->Form->input('Message.title', array('label'=>array('class'=>'head', 'text'=>'Assunto'), 'div'=>false, 'required'=> true)); ?>
					</div>					
					<div class="clearfix"></div>																		
					<div class="user-tp frame">
						<?php echo $this->Form->input('Message.body', array('label'=>array('class'=>'head', 'text'=>'Sua Mensagem'), 'type'=>'textarea', 'div'=>false, 'required'=> true)); ?>
					</div>						
					<input type="submit" value="Enviar">
					<?php echo $this->Form->end();?>  
				</div>
			</div>
		</div>
	</div>
<!-- //contact -->