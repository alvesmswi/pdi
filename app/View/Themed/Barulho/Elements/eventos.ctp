<!--/artist-->
<?php //pr($nodesList); exit;?>
<?php if(!empty($nodesList)){?>
	<div class="ab_content artist" id="team">
		<div class="container">
			<h3 class="tittle_w3ls cen">É o que tem pra Hoje :)</h3>
			<div class="inner_sec_info_agile-w3ls">
				<?php foreach($nodesList as $key => $node){
					//se tem mais de 4 eventos sai do foreach
					if($key > 3){
						break;
					}
					//testamos se tem endereço customizado, link externo
                    $link_noticia = $node['Node']['path'];
                    if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                        $link_noticia = $node['NoticiumDetail']['link_externo'];
                    }
                    //Se tem imagem
                    if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])){
                        $imagemUrlArray = array(
                            'plugin'		=> 'Multiattach',
                            'controller'	=> 'Multiattach',
                            'action'		=> 'displayFile', 
                            'admin'			=> false,
                            'filename'		=> $node['Multiattach']['filename'],
                            'dimension'		=> '270x289'
                        );
                        $imagemUrl = $this->Html->url($imagemUrlArray);
                    }	
				?>
					<div class="col-md-6 team-grid">
						<?php if($key == 0 || $key == 3){?>
							<div class="col-md-6 team-img1">
								<img src="<?php echo $imagemUrl; ?>" alt="">
							</div>
						<?php }?>	
							<div class="col-md-6 team-info">
								<h4><?php echo $node['Node']['title']?></h4>
								<p><?php echo $node['Node']['excerpt']?></p>
                                <a class="button-evento" href="#">Leia Mais</a>
							</div>
						<?php if($key == 1 || $key == 2){?>
							<div class="col-md-6 team-img1">
								<img src="<?php echo $imagemUrl; ?>" alt="">
							</div>
						<?php }?>
						<div class="clearfix"></div>					
					</div>
				<?php }?>				
			</div>
		</div>
	</div>
<?php }?>
	<!--//artist-->