<?php 
//se tem keywords
if(isset($node['NoticiumDetail']['keywords']) && !empty($node['NoticiumDetail']['keywords'])){
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $node['NoticiumDetail']['keywords']
		),
		null,
		array('inline'=>false)
	);
	//pega cada palavras
	$arrayKeywords = explode(',', $node['NoticiumDetail']['keywords']);
	?>
	
	<header>
		<h2>TAGS</h2>
	</header>
	<div id="tags">	
		<?php foreach($arrayKeywords as $keywords){ 
			//pelo Google
			$link = '/search?q='.urlencode(trim($keywords));
			//Pelo CMS
			$link = '/nodes/nodes/keywords/'.urlencode(trim($keywords));
			?>
				<div class="badge">
					<a href="<?php echo $link; ?>" class="hashtag" itemprop="keyword">
						<?php echo '#'.$keywords; ?>
					</a>
				</div>
		<?php } ?>
	</div>
		
<?php } ?>