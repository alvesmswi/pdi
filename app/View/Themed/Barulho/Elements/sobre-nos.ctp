<div class="banner_bottom_content">
  <div class="bottom-inn">
    <div class="container">
      <h3 class="tittle_w3ls cen">Sobre Nós</h3>
      <div class="inner_sec_info_agile-w3ls">

        <div class="col-md-4 ser-first-grid text-center">
          <span class="fas fa-music" aria-hidden="true"></span>
          <h3>Música</h3>
          <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Mauris ac tristique arcu..</p>
        </div>
        <div class="col-md-4 ser-first-grid text-center">
          <span class="fas fa-headphones" aria-hidden="true"></span>
          <h3>Discografia</h3>
          <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Mauris ac tristique arcu.</p>
        </div>
        <div class="col-md-4 ser-first-grid text-center">
          <span class="fas fa-microphone" aria-hidden="true"></span>
          <h3>Podcasts</h3>
          <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Mauris ac tristique arcu.</p>
        </div>

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>