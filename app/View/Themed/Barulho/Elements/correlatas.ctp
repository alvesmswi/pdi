<?php if(isset($relacionadas) && !empty($relacionadas)){?>
    <div class="o-widget">
        <header> 
            <h2>Relacionadas</h2>
        </header>
        <ul style="margin-left:10px">
        <?php foreach($relacionadas as $relacionada){ ?>
            <li type="circle">
                <a href="<?php echo $relacionada['Node']['path'] ?>" class="o-news_content">							
                    <h4 class="o-news_title"><?php echo $relacionada['Node']['title'] ?></h4>
                </a>
            </li>        
        <?php } ?>
        </ul>
    </div>
<?php } ?>