<div class="banner-inner-page" style="background: url('/theme/Barulho/images/banner22.jpg')no-repeat 0px 0px">
</div>
<?php if (!empty($posts)) { ?>
	<div class="ab_content read_page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h3 class="tittle_w3ls">
						<?php if (isset($posts[0]['Blog']['capa'])) { ?>
						<img src="<?php echo $posts[0]['Blog']['capa'] ?>" alt="<?php echo $title_for_layout; ?>">
						<?php } else { ?>
							<span class="o-header_title o-header_title--large"><?php echo $title_for_layout; ?></span>
						<?php } ?>
					</h3>
					<?php foreach ($posts as $post) { ?>
						<div class="row" id="lista">
							<div class="col-md-4">
								<?php 
									if (isset($post['Images'][0]['filename']) && !empty($post['Images'][0]['filename'])) { 
										$this->Helpers->load('Medias.MediasImage'); ?>
										<a href="<?php echo $this->Html->url($post['Post']['url']) ?>" class="titulo">
											<img src="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename'], '300'); ?>" class="img-responsive thumbnail" alt="<?php echo $post['Images'][0]['alt']; ?>">
										</a>
									<?php } ?>
							</div>	
							<div class="col-md-8">	
								<header class="o-post_header">
									<h3><a href="<?php echo $this->Html->url($post['Post']['url']) ?>" class="titulo"><?php echo $post['Post']['title'] ?></a></h3>
									<div class="o-post_byline">
										<time class="o-post_datetime"><?php echo date('d/m/Y H:i', strtotime($post['Post']['publish_start'])) ?></time> 								
									</div>
								</header>								
								<p><?php echo $post['Post']['excerpt'] ?></p>
							</div>
						</div>	
						<?php } ?>
					<?php 
					$this->Paginator->options['url'] = array('controller' => 'blogs', 'action' => 'view', 'slug' => $this->params['slug']);
					echo $this->element('paginator');
					?>
				</div>
			</div>	
		</div>	
	</div>
<?php } ?>