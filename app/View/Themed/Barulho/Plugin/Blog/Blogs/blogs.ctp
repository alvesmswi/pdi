<div class="banner-inner-page" style="background: url('/theme/Barulho/images/banner22.jpg')no-repeat 0px 0px">
</div>
<div class="ab_content read_page">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h3 class="tittle_w3ls"><?php echo $title_for_layout; ?></h3>
				<div class="row" id="lista">
					<?php 
					//se não está vazio
					if(!empty($blogs)){
						//percorre os nodes
            foreach($blogs as $key => $blog){ ?>
              <?php $url = ((isset($blog['Post']['url'])) ? $this->Html->url($blog['Post']['url']) : "/blog/{$blog['Blog']['slug']}/post/{$blog['Post']['slug']}"); ?>
							<div class="row" id="lista">
								<div class="col-md-4">
                  <a href="<?php echo $this->Html->url($blog['Blog']['url']); ?>">
										<img src="<?php echo $blog['Blog']['image'] ?>" class="img-responsive thumbnail" alt="<?php echo $blog['Blog']['title'] ?>" /> 									
									</a>
								</div>
								<!--coluna direita da lista-->
								<div class="col-md-8">
									<h3 class="titulo">
										<a href="<?php echo $url; ?>">
											<?php echo $blog['Blog']['title'] ?>
										</a>
									</h3>
									<div class="chamada">
                    <a href="<?php echo $url; ?>">
                      <?php echo $blog['Post']['title']; ?>
										</a>
                  </div>
                  <div class="chamada">
                      <p><?php echo $blog['Post']['excerpt']; ?></p>
									</div>
								</div>
							</div>
						<?php	
						}
						//Insere o paginado
						echo $this->element('paginator');
					}else{
						echo __d('croogo', 'Nenhum registro encontrado.');
					} ?>
				</div>
			</div>
			<div class="col-md-4">
				<?php 
					//Se tem algum bloco ativo nesta região
					if($this->Regions->blocks('banner-lista')){
						echo $this->Regions->blocks('banner-lista');
					} 
				?>
			</div>
		</div>
	</div>
</div>
