<div class="banner-inner-page" style="background: url('/img/curitiba_topo.jpg')no-repeat 0px 0px">
</div>
<?php echo $this->element('node_metas'); ?>
<div class="ab_content read_page">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<header class="o-header">
					<h3 class="tittle_w3ls">
						<?php
						//se teve uma pesquisa
						if(isset($this->params->named['slug']) && !empty($this->params->named['slug'])){
							echo __d('croogo', $this->Mswi->slugName($this->params->named['slug']));
						}else{
							echo __d('croogo', 'Resultado da pesquisa');
						}
						?>
					</h3>
				</header>
				<div class="u-spacer_bottom">
					<div class="row">
						<?php
						//se tem algum registro
						if(!empty($nodes)){ 					
							//percorre os nodes
							foreach($nodes as $n){
								if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
									if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
										$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
									}
								}
								?>
								<div class="row" id="lista">
									<div class="col-sm-4">
										<div class="o-news">
											<?php
												$link_noticia = $n['Node']['path'];
												if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
													$link_noticia = $n['NoticiumDetail']['link_externo'];
												}
											?>
											<a href="<?php echo $link_noticia; ?>" class="o-news_content">
												<?php 
												//se tem imagem
												if(isset($n['Multiattach']) && (!empty($n['Multiattach']['filename']))){
													$imagemUrlArray = array(
														'plugin'		=> 'Multiattach',
														'controller'	=> 'Multiattach',
														'action'		=> 'displayFile', 
														'admin'			=> false,
														'filename'		=> $n['Multiattach']['filename'],
														'dimension'		=> 'thumLista'
													);
													$imagemUrl = $this->Html->url($imagemUrlArray);
													$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
													?>
													<img src="<?php echo $imagemUrl ?>" class="thumbnail" alt="<?php echo $imageTitle ?>" /> 
												<?php } ?>											
											</a>	        
										</div>
									</div>
									<div class="col-sm-8">
										<a href="<?php echo $link_noticia; ?>" class="titulo"><h3 class="titulo"><?php echo $n['Node']['title']; ?></h3></a>
										<p><?php echo $n['Node']['excerpt']; ?></p>
									</div>
								</div>
							<?php } 
						} else { ?>
							<div class="col-sm-12">
								<?php echo __d('croogo', 'Nenhum registro encontrado.'); ?>
							</div>
						<?php } ?>
					</div>
				<?php //Insere o paginado
					echo $this->element('paginator');?>
				</div>
			</div>
			<div class="col-md-4">
				<?php 
					//Se tem algum bloco ativo nesta região
					if($this->Regions->blocks('banner-lista')){
						echo $this->Regions->blocks('banner-lista');
					} 
				?>
			</div>
		</div>	
	</div>
</div>