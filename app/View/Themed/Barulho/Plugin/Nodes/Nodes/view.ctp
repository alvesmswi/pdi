<!-- banner -->
<?php 
echo $this->element('node_metas');
if(isset($node['Multiattach'][0]['Multiattach']['filename'])){
		$imagemUrlArray = array(
			'plugin'		=> 'Multiattach',
			'controller'	=> 'Multiattach',
			'action'		=> 'displayFile', 
			'admin'			=> false,
			'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
			'dimension'		=> 'normal'
		);
	$imagemUrl = $this->Html->url($imagemUrlArray);

	//prepara a imagem para o OG
	$this->Html->meta(
		array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
		null,
		array('inline' => false)
	);

	//pega os detalhes da imagem
	$imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

	//largura da imagem
	$this->Html->meta(
		array('property' => 'og:image:width', 'content' => $imageArray[0]),
		null,
		array('inline' => false)
	);
	//Altura da imagem
	$this->Html->meta(
		array('property' => 'og:image:height', 'content' => $imageArray[1]),
		null,
		array('inline' => false)
	);
	//Mime da imagem
	$this->Html->meta(
		array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
		null,
		array('inline' => false)
	);
	//Alt da imagem
	$this->Html->meta(
		array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
		null,
		array('inline' => false)
	);

	
} ?>
<div class="banner-inner-page" style="background: url('/img/curitiba_topo.jpg')no-repeat 0px 0px">
</div>
<!--//banner -->
<!--/single-->
	<div class="ab_content read_page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h3 class="tittle_w3ls"><?php echo $node['Node']['title'] ?></h3>
					<div>
						<time class="o-post_datetime">
							<?php echo date('d/m/Y H:i', strtotime($node['Node']['publish_start'])) ?>
						</time>
						<?php 
						//se foi atualizado
						if($node['Node']['updated'] > $node['Node']['publish_start']){ ?>
								- Atualizado em <?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
									<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>
						<?php } ?>
					</div>
					<?php if(!empty($node['NoticiumDetail']['jornalista'])){
						echo 'Por - <author>' . $node['NoticiumDetail']['jornalista'] . '</author>';
					}
					?>
					<div class="addthis_inline_share_toolbox" style="margin-top:10px;"></div>
					<div class="inner_sec_info_agile-w3ls" style="margin-top:10px">
						<div class="read_img">							
								<img src="<?php echo $imagemUrl;?>" alt=" " class="img-responsive">
								<figcaption>
									<?php
									$figCaption = isset($node['Multiattach'][0]['Multiattach']['metaDisplay']) ? $node['Multiattach'][0]['Multiattach']['metaDisplay'] : '';
									$figAutor = isset($node['Multiattach'][0]['Multiattach']['comment']) ? $node['Multiattach'][0]['Multiattach']['comment'] : '';
									if(!empty($figCaption)){
											echo $figCaption; 
									}
									if(!empty($figAutor)){
										echo ' (Foto: '.$figAutor.')'; 
									} ?>
								</figcaption>
							<p class="read_para"><?php echo $node['Node']['body'] ?></p>
						</div>				
						<div class="clearfix"></div>
					</div>

					<?php 
					if(isset($node['Multiattach'][0]['Multiattach']['filename']) && count($node['Multiattach']) > 1){  ?>
						<div class="slider-for">
							<?php foreach($node['Multiattach'] as $image) { ?>
								<div class="slick-item">
									<?php
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $image['Multiattach']['filename'],
										'dimension'		=> 'fotoGaleria'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
										
									?>
									<figure>
										<img src="<?php echo $imagemUrl ?>" style="width:100%">										
									</figure>
								</div>
							<?php } ?>
						</div>
						<br />
						<div class="slider-nav">
							<?php foreach ($node['Multiattach'] as $attach) { ?>
								<div class="slider-item">
									<figure>
										<?php
										$imagemUrlArray = array(
										'plugin'    => 'Multiattach',
										'controller'=> 'Multiattach',
										'action'    => 'displayFile', 
										'admin'     => false,
										'filename'  => $attach['Multiattach']['filename'],
										'dimension' => 'thumGaleria'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										?>
										<img src="<?php echo $imagemUrl ?>" style="max-width:100%">
									</figure>
								</div>    
							<?php } ?>
						</div>
					<?php } 

					echo $this->element('keywords_links');
					echo $this->element('box_autor');		
					echo $this->element('facebook_comentarios');					

					//Se tem algum bloco ativo nesta região
					if($this->Regions->blocks('content_footer')){
						echo $this->Regions->blocks('content_footer');
					} 
					?>

				</div>
				 <div class="col-md-4">
					<?php 
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('right')){
						echo $this->Regions->blocks('right');
						} 
					?>
        		</div>	
			</div>
		</div>
	</div>
<!--//single-->