<?php echo $this->element('node_metas'); ?>
<div class="banner-inner-page" style="background: url('/theme/Barulho/images/banner22.jpg')no-repeat 0px 0px">
</div>
<?php 
$cxGoogle = Configure::read('Site.busca_google');
if(!$cxGoogle){?>
	<div class="ab_content read_page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h3 class="tittle_w3ls">
						<?php 
						//se teve uma pesquisa
						if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
							echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
						}else{
							echo __d('croogo', 'Resultado da pesquisa');
						}
						?>
					</h3>
					<div class="row" id="lista">
						<?php 
						//se não está vazio
						if(!empty($nodes)){
							//percorre os nodes
							foreach($nodes as $key => $n){ ?>
								<div class="row" id="lista">
									<div class="col-md-4">
										<?php
											$link_noticia = $n['Node']['path'];
											//Se tem link externo
											if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
												$link_noticia = $n['NoticiumDetail']['link_externo'];
											}
										?>
										<a href="<?php echo $link_noticia; ?>">
											<?php 
											//Se tem imagem
											if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
												if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
													$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
												}
											}
											//se tem imagem
											if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
												$imagemUrlArray = array(
													'plugin'		=> 'Multiattach',
													'controller'	=> 'Multiattach',
													'action'		=> 'displayFile', 
													'admin'			=> false,
													'filename'		=> $n['Multiattach']['filename'],
													'dimension'		=> 'thumLista'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
												$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
												?>
												<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="thumbnail" alt="<?php echo $imageTitle ?>" /> 
											<?php } ?>									
										</a>
									</div>
									<!--coluna direita da lista-->
									<div class="col-md-8">
										<div class="chapeu badge">
											<?php 
											//se tem chapeu
											if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
												echo $n['NoticiumDetail']['chapeu'];
											}else{
												echo $this->Mswi->categoryName($n['Node']['terms']);
											}; 
											?>
										</div>
										<h3 class="titulo">
											<a href="<?php echo $link_noticia; ?>">
												<?php echo $n['Node']['title']; ?>
											</a>
										</h3>
										<div class="chamada">
											<a href="<?php echo $link_noticia; ?>">
												<?php echo $n['Node']['excerpt']; ?>
											</a>
										</div>
									</div>
								</div>
							<?php	
							}
							//Insere o paginado
							echo $this->element('paginator');
						}else{
							echo __d('croogo', 'Nenhum registro encontrado.');
						} ?>
					</div>
				</div>
				<div class="col-md-4">
					<?php 
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('banner-lista')){
							echo $this->Regions->blocks('banner-lista');
						} 
					?>
				</div>
			</div>
		</div>
	</div>
<?php }else{?>
	<div class="ab_content read_page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h3 class="tittle_w3ls">
						<?php 
						//se teve uma pesquisa
						if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
							echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
						}else{
							echo __d('croogo', 'Resultado da pesquisa');
						}
						?>
					</h3>
					<style>
						.gsc-control-cse{
							line-height: normal;
						}
						.gsc-control-cse form{
							line-height: 0;
						}
						.gsc-adBlock{
							display: none;
						}
						.gsc-control-cse table{
							margin: 0;
						}
					</style>
					<script>
						(function() {
							var cx = '<?php echo Configure::read('Site.busca_google')?>';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
						})();
					</script>
					<gcse:search></gcse:search>
				</div>
				<div class="col-md-4">
					<?php 
						//Se tem algum bloco ativo nesta região
						if($this->Regions->blocks('banner-lista')){
							echo $this->Regions->blocks('banner-lista');
						} 
					?>
				</div>	
			</div>	
		</div>	
	</div>
<?php }?>	