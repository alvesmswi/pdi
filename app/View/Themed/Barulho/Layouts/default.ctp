<?php
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
        <?php echo Configure::read('Site.title'); ?> | 
        <?php echo $title_for_layout; ?>
    </title>
    <?php echo $this->fetch('meta');?>
	<script>
		/*addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);*/

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>

    <link rel="icon" href="/theme/<?php echo Configure::read('Site.theme')?>/favicon.ico" />
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Molengo" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Arimo:400,400i,700,700i" rel="stylesheet">
    <!-- //web-fonts -->

    <?php
    echo $this->Html->css(
        array(
            'bootstrap', 
            'style', 
            'fontawesome-all',
            'plyr',
            'chocolat',
            'wickedpicker',
            'jquery-ui',
			'simplyCountdown',
			'slick/slick',
			'slick/slick-theme',
			'custom'			
        )
    );
		//Se foi inofmrado o user do analytics
	if (!empty(Configure::read('Service.analytics'))) {
		$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
            ga('create', '<?= trim($analytics) ?>', 'auto', {
                'name': 'id<?= $key ?>'
            });
            ga('id<?= $key ?>.send', 'pageview');
            <?php } ?>

            /**
                * Function that tracks a click on an outbound link in Analytics.
                * This function takes a valid URL string as an argument, and uses that URL string
                * as the event label. Setting the transport method to 'beacon' lets the hit be sent
                * using 'navigator.sendBeacon' in browser that support it.
                */
            var trackOutboundLink = function (url) {
                ga('send', 'event', 'outbound', 'click', url, {
                    'transport': 'beacon',
                    'hitCallback': function () {
                        document.location = url;
                    }
                });
            }
        </script>
    <?php } ?>

    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function () {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>
        
    <?php
    //Se foi informado o tempo de refresh para a Home
    if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
      <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php }
    //Se foi informado o tempo de refresh para as internas
    if ($this->here != '/' && !empty(Configure::read('Site.refresh_internas')) && is_numeric(Configure::read('Site.refresh_internas'))) { ?>
      <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
    <?php }
      echo Configure::read('Service.header')
    ?>
	
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <?php
    //Se tem algum bloco ativo nesta região
    if($this->Regions->blocks('header_scripts')){
      echo $this->Regions->blocks('header_scripts');
    }
    ?>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<?php echo $this->element('bpbar'); ?>
	<?php echo $this->Menus->menu('main', array('element' => 'menus/main')); ?>

	<?php 
	//Exibe as mensagens
	echo $this->Layout->sessionFlash();

	//Conteudo do node
	echo $content_for_layout;

	?>
									
	<!-- /footer -->
	<div class="social_footer">
		<?php 
		echo $this->Html->script('jquery-2.2.3.min');
        echo $this->Html->script('bootstrap');        
        echo $this->Html->script('all');
        echo $this->Html->script('search');
        echo $this->Html->script('scroll_menu');
        echo $this->Html->script('jquery.chocolat');
        echo $this->Html->script('move-top');
        echo $this->Html->script('easing');
        echo $this->Html->script('wickedpicker');
        echo $this->Html->script('jquery-ui');
        echo $this->Html->script('html5media.min');
		echo $this->Html->script('slick/slick.min');
        ?>
		<div class="social-nav">
			<div class="col-md-3 ft">
				<a href="<?php echo Configure::read('Social.facebook')?>">
					<span class="fab fa-facebook-f"></span>
				</a>
			</div>
			<div class="col-md-3 ft tweet">
				<a href="<?php echo Configure::read('Social.twitter')?>">
					<span class="fab fa-twitter"></span>
				</a>
			</div>
			<div class="col-md-3 ft gm">
				<a href="<?php echo Configure::read('Social.instagram')?>">
					<span class="fab fa-instagram" style="font-size:36px"></span>
				</a>
			</div>
			<div class="col-md-3 ft pin">
				<a href="<?php echo Configure::read('Social.youtube')?>">
					<span class="fab fa-youtube"></span>
				</a>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //footer -->
	<div class="cpy-right-agile-w3ls">
		<p>© 2018 <?php echo Configure::read('Site.title'); ?> | Desenvolvido por
			<a href="http://mswi.com.br/pdi/" target="_blank"> PDi.</a>
		</p>
	</div>

	<!--search jQuery-->
	<!-- //fixed-scroll-nav-js -->

	<script>
		$(".hover").mouseleave(
			function () {
				$(this).removeClass("hover");
			}
		);
	</script>
	<!--//scripts-->
	<!--light-box-files -->
	<script type="text/javascript ">
		$(function () {
			$('.portfolio-grids a').Chocolat();
		});
	</script>
	<!-- /js for portfolio lightbox -->

	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->
	<!-- Time -->

	<script type="text/javascript">
		$('.timepicker').wickedpicker({
			twentyFour: false
		});
	</script>
	<!-- //Time -->
	<!-- Calendar -->

	<script>
		$(function () {
			$("#datepicker,#datepicker1,#datepicker2,#datepicker3").datepicker();
		});
	</script>
	<!-- //Calendar -->

<!--js-->
	<!-- dropdown nav -->
	<script>
		$(document).ready(function () {
			$(".dropdown").hover(
				function () {
					$('.dropdown-menu', this).stop(true, true).slideDown("fast");
					$(this).toggleClass('open');
				},
				function () {
					$('.dropdown-menu', this).stop(true, true).slideUp("fast");
					$(this).toggleClass('open');
				}
			);
		});
	</script>
	<!-- //dropdown nav -->
	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
			    containerID: 'toTop', // fading element id
			    containerHoverID: 'toTopHover', // fading element hover id
			    scrollSpeed: 1200,
			    easingType: 'linear' 
			};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

			$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots: false,
				centerMode: true,
				focusOnSelect: true
			});
		});
	</script>

	<!-- //smooth-scrolling-of-move-up -->
	<a href="#" id="toTop" style="display: block;">
		<span id="toTopHover" style="opacity: 1;"> </span>
	</a>
    <?php 
    
    if(!empty(Configure::read('Service.addthis'))): ?>
        <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis') ?>"></script>
    <?php endif; ?>
    <?php
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('footer_scripts')){
		echo $this->Regions->blocks('footer_scripts');
	}
	echo $this->element('sql_dump');
	?>
</body>

</html>