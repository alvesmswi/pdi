<?php
//se o plugin de Pagseguro está ativo
if(CakePlugin::loaded('Pagseguro')){
  //prepara o array para enviar os dados para o elemento de compra
  $arraycheckout = array();
  $arraycheckout['tipo'] = 'classificado';
  $arraycheckout['items'][] = array(
    'id' => $anuncio['Anuncio']['id'],
    'valor' => $anuncio['Anuncio']['preco'],
    'descricao' => $anuncio['Anuncio']['title'],
    'quantidade' => 1
  );
  //chama o elemento de compra
  echo $this->element('Pagseguro.botao_pagar', array('checkout'=>$arraycheckout));
}
?>
<style>
  .btn-primary{
    width: -webkit-fill-available!important
  }
</style>
<div class="c-classificados_contact">
  <div class="c-classificados_contact-group">
    Anúncio por: <strong><?php echo $anuncio['Anuncio']['anunciante_nome'] ?></strong>   
    <a href="#" class="c-classificados_contact-btn" style="width: auto;"><?php echo $anuncio['Anuncio']['anunciante_telefone'] ?></a>
  </div>
  <div class="c-classificados_contact-group">
    <?php echo $this->Form->create('Contato', array('url' => array('plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'contato'))); ?>
      <input type="hidden" name="node_id" value="<?php echo $anuncio['Anuncio']['id'] ?>">
      <div class="form-group">
        <label for="classificadosContactName">Nome</label>
        <input type="text" name="nome" class="form-control" id="classificadosContactName" placeholder="Seu nome" required="required">
      </div>
      <div class="form-group">
        <label for="classificadosContactEmail">E-mail</label>
        <input type="email" name="email" class="form-control" id="classificadosContactEmail" placeholder="Seu Email" required="required">
      </div>
      <div class="form-group">
        <label for="classificadosContactTelefone">Telefone</label>
        <input type="text" name="telefone" class="form-control" id="classificadosContactTelefone" placeholder="Seu Telefone" required="required">
      </div>
      <div class="form-group">
        <label for="classificadosContactMensagem">Mensagem</label>
        <textarea id="classificadosContactMensagem" name="mensagem" class="form-control" rows="3" required="required"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="c-classificados_contact-btn" style="width:-webkit-fill-available;">Enviar</button>
      </div>
    <?php echo $this->Form->end(); ?>
  </div>
</div>