<?php 
$this->start('classificados_header');
  echo '<div class="o-wrapper">';
    echo $this->element('Classificados.big_banner'); 
  echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<?php foreach ($classificados as $key => $c) { ?>
  <?php if (!empty($c['Anuncio'])) { ?>
    <section class="s-list u-spacer_bottom">
    <header class="block-head-b">
      <span class="title"><?php echo $c['Classificado']['title'] ?></span> 
      <a href="<?php echo $this->Html->url($c['Classificado']['url']) ?>" class="view-all">
        Veja todos <i class="i-icon i-icon_caret-right"></i>
      </a>
    </header>
    <div class="row">
      <?php foreach ($c['Anuncio'] as $key => $a) { ?>
        <div class="col-sm-3 col-xs-6 col-3">
          <div class="o-classificados">
            <span class="cat-label cf" style="position:inherit;">
              <a href="<?php echo $this->Html->url($a['Categoria']['url']) ?>" class="category" style="margin-bottom:10px;">
                <?php echo $a['Categoria']['title'] ?>
              </a>
            </span>  
            <a href="<?php echo $this->Html->url($a['Anuncio']['url']) ?>" class="o-classificados_content">
              <?php if(isset($a['Images'][0])) { ?>
                <img src="<?php echo $this->MediasImage->imagePreset($a['Images'][0]['filename'], '186'); ?>" class="o-classificados_image"> 
              <?php } ?>
              <p>
              <span class="o-classificados_title"><?php echo $a['Anuncio']['title'] ?></span>
              <span class="o-classificados_price"><?php echo $a['Anuncio']['preco'] ?></span>
              </p>
            </a>
          </div>
        </div>
      <?php } ?>
    </div>
  </section>
  <?php } ?>
<?php } ?>
