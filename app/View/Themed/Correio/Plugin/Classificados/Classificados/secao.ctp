<?php 
$this->start('classificados_header');
  echo '<div class="o-wrapper">'; ?>
    <div class="o-classificados_header" style="border-bottom-width:0px">
      <header class="block-head-b" style="margin-bottom:0px;margin-bottom: 10px;">
        <i class="i-icon i-icon_<?php echo $classificado['Classificado']['icone'] ?>"></i>
        <span class="title" style="font-size:30px;"><?php echo $secao ?></span>
      </header>
      <form method="GET" action="/classificados/search" class="o-classificados_header-form">
        <label class="sr-only" for="classificadosHeaderSearch">Pesquisar</label>
        <input type="text" class="o-classificados_header-input" name="q" id="classificadosHeaderSearch" placeholder="Pesquisar nos classificados">
        <button type="submit" class=""><i class="i-icon i-icon_search"></i>Pesquisar</button>
        <div class="o-classificados_header-backdrop" aria-hidden="true"></div>
      </form>
    </div>
  <?php
  echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<div class="o-classificados_list">
  <header class="o-classificados_list-header">
    <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Ordenar por <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <?php 
        $classificado = $this->params['classificado'];
        $secao = $this->params['slug'];
        ?>
        <li><a href="/classificados/<?php echo $classificado ?>/secao/<?php echo $secao ?>/sort:publish_start/direction:desc">Mais recentes</a></li>
        <li><a href="/classificados/<?php echo $classificado ?>/secao/<?php echo $secao ?>/sort:preco/direction:asc">Menor valor</a></li>
        <li><a href="/classificados/<?php echo $classificado ?>/secao/<?php echo $secao ?>/sort:preco/direction:desc">Maior valor</a></li>
      </ul>
    </div><span class="o-classificados_n-anuncios"><?php echo $total_anuncios ?> Anúncios</span>
  </header>
  <div class="row">
    <?php foreach ($anuncios as $key => $anuncio) { ?>
      <div class="col-sm-4 col-xs-6 col-4">
        <div class="o-classificados">
          <a href="<?php echo $this->Html->url($anuncio['Anuncio']['url']) ?>" class="o-classificados_content">
            <?php if(isset($anuncio['Images'][0])) { ?>
              <img src="<?php echo $this->MediasImage->imagePreset($anuncio['Images'][0]['filename'], '258'); ?>" class="o-classificados_image"> 
            <?php } ?>
            <span class="o-classificados_title"><?php echo $anuncio['Anuncio']['title'] ?></span>
            <span class="o-classificados_price"><?php echo $anuncio['Anuncio']['preco'] ?></span>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php 
  $this->Paginator->options['url'] = array(
    'controller' => 'classificados', 'action' => 'view', 
    'classificado' => $this->params['classificado'], 'slug' => $this->params['slug']
  );
  echo $this->element('paginator');
  ?>
</div>
