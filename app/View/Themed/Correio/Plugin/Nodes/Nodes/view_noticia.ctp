<?php 
//se tem chamada
if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
	$description = $node['Node']['excerpt'];
}else{
	$description = substr($node['Node']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);

echo $this->element('node_metas');

//Se foi informado o nome do jornalista
if(isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])){
    $autor = $node['NoticiumDetail']['jornalista'];
}else{
    $autor = $node['User']['name'];
}
?>
<section class="cf block news-block">
    <div class="block-content ts-row">
        <div class="col-12 large cf">
            <article class="grid-post post type-post format-standard has-post-thumbnail <?php echo $node['Node']['type']; ?>">
                <?php if(isset($node['Editorias'])){ ?>
                    <span class="cat-label cf" style="position: unset;"> 
                        <a href="/ultimas-noticias/<?php echo $node['Editorias'][0]['slug']; ?>" class="category" style="margin-bottom: 10px;">
                            <?php echo $node['Editorias'][0]['title']; ?>
                        </a> 
                    </span>
                <?php }?>
                <div class="meta-title">
                    <div class="post-meta post-meta-c">                                                                    
                        <h2 class="post-title-alt" style="font-size: 24px;"><?php echo $node['Node']['title']; ?></h2> 
                    </div>
                </div>
                <div class="post-content post-excerpt" style="text-align: left;">
                    <p style="margin:0px;"><?php echo $node['Node']['excerpt']; ?></p>
                    <div style="margin: 10px 0;font-size: 14px;">
                        <time class="post-date" 
                            datetime="<?php echo $node['Node']['publish_start']; ?>">
                            <?php echo date('d/m/Y H:m', strtotime($node['Node']['publish_start'])); ?>
                        </time> - 
                        <span class="post-author"><span class="Por">Por <?php echo $autor; ?></span> 
                    </div>
                    <div class="addthis_inline_share_toolbox"></div>
                </div>
                <?php                 
                if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
                        'dimension'		=> 'normal'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    if(isset($node['Multiattach'][0]['Multiattach']['comment']) && !empty($node['Multiattach'][0]['Multiattach']['comment'])){
                        $imagemAutor = $node['Multiattach'][0]['Multiattach']['comment'];
                    }                        
                    if(isset($node['Multiattach'][0]['Multiattach']['metaDisplay']) && !empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
                        $imagemTitle = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
                    }
                    //prepara a imagem para o OG
                    $this->Html->meta(
                        array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
                        null,
                        array('inline' => false)
                    );
                    
                    //pega os detalhes da imagem
                    $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

                    //largura da imagem
                    $this->Html->meta(
                        array('property' => 'og:image:width', 'content' => $imageArray[0]),
                        null,
                        array('inline' => false)
                    );
                    //Altura da imagem
                    $this->Html->meta(
                        array('property' => 'og:image:height', 'content' => $imageArray[1]),
                        null,
                        array('inline' => false)
                    );
                    //Mime da imagem
                    $this->Html->meta(
                        array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
                        null,
                        array('inline' => false)
                    );
                    //Alt da imagem
                    $this->Html->meta(
                        array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
                        null,
                        array('inline' => false)
                    );
                    ?>
                    <div class="post-thumb">
                        <div class="pre-img">
                        <div class="lazyload">
                            <!--
                            <img 
                            src="<?php echo $imagemUrl; ?>" 
                            class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
                            title="<?php echo $node['Node']['title']; ?>"/>
                              <?php if(isset($imagemTitle) || isset($imagemAutor)) { ?>
                              <?php //TODO:  OBS: <div class="divcaption"> existe para melhor manipular o layout ta tag <figcaption> ?>
                                <div  class="divcaption">
                                    <figcaption  class="caption" style="text-align: left;">
                                    <?php if(isset($imagemTitle)) {
                                    echo $imagemTitle.' ';
                                }
                                    if(isset($imagemAutor)){
                                        echo '(Foto: '.$imagemAutor .')';
                                    }
                                    ?></span>
                                    </figcaption>
                                </div>
                            <?php } ?>
                            -->
                        </div>

                        </div>
                    </div>
                <?php } ?>

                <?php if(isset($relacionadas) && !empty($relacionadas)){ ?>

                    <div  class="relacionado-contain">
                        <ul>
                            <?php foreach ($relacionadas as $key  => $relacionada) {?>
                                <li>
                                    <h5><a   href=<?php echo $relacionada['Node']['path'] ?>><?php echo $relacionada['Node']['title'] ?></a></h5>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                <?php }?>

                <div class="post-content cf corpo">




                    <?php
                    //Corpo da noticia
             	    echo $node['Node']['body'];

             	    // loop para galeria de imagem,
                    // se houver mais de uma imagem anexado inicia  o looṕ
                    if(isset($node['Multiattach']['1']) && !empty($node['Multiattach']['1'])){?>

                        <?php /* loop para galeria*/?>
                    <div class="slick-slider">
                        <?php foreach($node['Multiattach'] as $key => $imagem){?>
                            <?php
                            $imagemUrlArray = array(
                                'plugin'		=> 'Multiattach',
                                'controller'	=> 'Multiattach',
                                'action'		=> 'displayFile',
                                'admin'			=> false,
                                'filename'		=> $node['Multiattach'][$key]['Multiattach']['filename'],
                                'dimension'		=> 'normal'
                            );
                            $imagemUrl_galeria = $this->Html->url($imagemUrlArray);
                            ?>
                            <div>
                                <img data-lazy="<?php echo $imagemUrl_galeria; ?>" />
                                <?php if(isset($imagem['Multiattach']['comment'])  || isset($imagem['Multiattach']['comment'])) { ?>
                                    <?php if(!empty($imagem['Multiattach']['comment']) || !empty($imagem['Multiattach']['comment'])) {
                                        ?>
                                        <?php //TODO:  OBS: <div class="divcaption"> existe para melhor manipular o layout ta tag <figcaption> ?>
                                        <div  class="divcaption">
                                            <figcaption  class="caption" style="text-align: left;">
                                                <?php if(isset($imagem['Multiattach']['metaDisplay'])) {
                                                    echo $imagem['Multiattach']['metaDisplay'].'  ';
                                                }
                                                if(isset($imagem['Multiattach']['comment']) && !empty($imagem['Multiattach']['comment'])){
                                                    echo '(Foto: '.$imagem['Multiattach']['comment'] .')';
                                                }
                                                ?>
                                            </figcaption>
                                        </div>
                                    <?php }
                                }?>
                            </div>
                        <?php }; ?>
                    </div>
                    <div class="slick-nav">
                                <?php foreach($node['Multiattach'] as $key => $imagem) { ?>
                                    <?php
                                    $imagemUrlArray = array(
                                        'plugin' => 'Multiattach',
                                        'controller' => 'Multiattach',
                                        'action' => 'displayFile',
                                        'admin' => false,
                                        'filename' => $node['Multiattach'][$key]['Multiattach']['filename'],
                                        'dimension' => 'normal'
                                    );
                                    $imagemUrl_galeria = $this->Html->url($imagemUrlArray);
                                    ?>

                                    <div>
                                        <img data-lazy="<?php echo $imagemUrl_galeria; ?>"/>
                                    </div>
                                <?php /* fim de galeria foreach */  } ?>
                    </div>

                        <script
                                src="http://code.jquery.com/jquery-2.2.4.min.js"
                                integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                                crossorigin="anonymous"></script>
                        <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/slick.min.js"></script>

                        <script type="text/javascript">

                            /* galeria de imagem*/

                            jQuery('.slick-slider').slick({
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: true,
                                fade: true,
                                asNavFor: '.slick-nav'
                            });
                            jQuery('.slick-nav').slick({
                                slidesToShow: 4,
                                slidesToScroll: 3,
                                asNavFor: '.slick-slider',
                                dots: false,
                                centerMode: true,
                                focusOnSelect: true,
                                arrows: false
                            });

                        </script>

                                <?php /* fim galeria if */ } ?>
                    <div>
                        <!--COMENTÁRIOS DO FACEBOOK-->
                        <div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].'/'.$node['Node']['type'].'/'.$node['Node']['slug']?>" data-numposts="5"></div>

                    </div>
            </article>
        </div>                                                
    </div>                                            
</section>