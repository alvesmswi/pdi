<?php echo $this->element('node_metas'); ?>
<section class="cf block blog">
    <div class="block-head-b">
		<h4 class="title">
			<?php
			//se teve uma pesquisa
			if(isset($this->params['type']) && !empty($this->params['type'])) {
				echo$this->Mswi->typeName($this->params['type']);
			}else{
				echo'Resultado da pesquisa';
			}
			?>
		</h4>
	</div>
    <div class="block-content">
        <div class="posts-dynamic posts-container ts-row list count-0">
            <div class="posts-wrap">
				<?php
				//se tem algum registro
				if(!empty($nodes)){
					//percorre os nodes
					foreach($nodes as $n){ 
						//pr($n);
						if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])) {
							if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
								$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
							}
						}
						?>
						<div class="col-12">
							<article id="post-32" class="list-post">
								<?php                 
								if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '300largura'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									if(isset($n['Multiattach']['comment']) && !empty($n['Multiattach']['comment'])){
										$imagemAutor = $n['Multiattach']['comment'];
									}                        
									if(isset($n['Multiattach']['metaDisplay']) && !empty($n['Multiattach']['metaDisplay'])){
										$imagemTitle = $n['Multiattach']['metaDisplay'];
									}
									?>
									<div class="post-thumb">
										<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
											<img width="270" height="180" 
											src="<?php echo $imagemUrl; ?>" 
											class="attachment-cheerup-list size-cheerup-list wp-post-image" 
											title="<?php echo isset($imagemTitle) ? $imagemTitle : $n['Node']['title'] ?>"  />
										</a>
									</div>
								<?php } ?>
								<div class="content">					
									<div class="post-meta post-meta-c">
										<h2 class="post-title">
											<a href="<?php echo $n['Node']['path']; ?>">
												<?php echo $n['Node']['title']; ?>
											</a>
										</h2> 
										<span class="cat-label cf"> 
											<?php if(isset($n['Editorias'][0]['title'])){ ?>
												<a href="/ultimas-noticias/<?php echo $n['Editorias'][0]['slug']; ?>" class="category">
													<?php echo $n['Editorias'][0]['title']; ?>
												</a> 
											<?php }else{ ?>
												<a href="<?php echo $n['Node']['type']; ?>/terms/<?php echo $this->Mswi->categoryName($n['Node']['slug']); ?>" class="category">
													<?php 
													//se tem chapey
													if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
														echo $n['NoticiumDetail']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
													?>
												</a> 
											<?php } ?>
										</span>
									</div>
									<div class="post-content post-excerpt cf">
										<a href="<?php echo $n['Node']['path']; ?>">
											<p><?php echo $n['Node']['excerpt']; ?></p>
										</a>
									</div>
								</div>
							</article>
						</div>
					<?php
          			} 
				}else{
					echo 'Nenhum registro encontrado.';
				} ?>
			</div>
		</div>
       <?php //Insere o paginado
		echo $this->element('paginator');?>
    </div>
 </section>