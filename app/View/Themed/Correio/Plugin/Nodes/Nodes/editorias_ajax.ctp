<?php
//se tem algum registro
if(!empty($nodes)){ 
	$page = $this->params->query['page'];
	$proximaPagina = $page + 1;

	//percorre os nodes
	foreach($nodes as $n){ 
		?>	
		<div class="col-12">
			<article id="post-32" class="list-post">
				<?php                 
                if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $n['Multiattach']['filename'],
                        'dimension'		=> '300largura'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    if(isset($n['Multiattach']['comment']) && !empty($n['Multiattach']['comment'])){
                        $imagemAutor = $n['Multiattach']['comment'];
                    }                        
                    if(isset($n['Multiattach']['metaDisplay']) && !empty($n['Multiattach']['metaDisplay'])){
                        $imagemTitle = $n['Multiattach']['metaDisplay'];
                    }
                    ?>
					<div class="post-thumb">
						<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
							<img width="270" height="180" 
							src="<?php echo $imagemUrl; ?>" 
							class="attachment-cheerup-list size-cheerup-list wp-post-image" 
							title="<?php echo isset($imagemTitle) ? $imagemTitle : $n['Node']['title'] ?>"  />
						</a>
					</div>
				<?php } ?>
				<div class="content">					
					<div class="post-meta post-meta-c">
						<h2 class="post-title">
							<a href="<?php echo $n['Node']['path']; ?>">
								<?php echo $n['Node']['title']; ?>
							</a>
						</h2> 
						<span class="cat-label cf"> 
							<a href="/ultimas-noticias/<?php echo $n['Editoria']['slug']; ?>" class="category">
								<?php echo $n['Editoria']['title']; ?>
							</a> 
						</span>
					</div>
					<div class="post-content post-excerpt cf">
						<a href="<?php echo $n['Node']['path']; ?>">
							<p><?php echo $n['Node']['excerpt']; ?></p>
						</a>
					</div>
				</div>
			</article>
		</div>	
	<?php	
	}
}

//se tem algum registro
if(!empty($nodes)){
	echo $this->Mswi->verMaisAjax('editorias_ajax',$this->params->query['slug'], $proximaPagina, 6);
}
?>