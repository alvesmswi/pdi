<?php 
//se tem chamada
if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
	$description = $node['Node']['excerpt'];
}else{
	$description = substr($node['Node']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);
?>
<section class="cf block news-block">
    <div class="block-content ts-row">
        <div class="col-12 large cf">
            <article class="grid-post post type-post format-standard has-post-thumbnail <?php echo $node['Node']['type']; ?>">
                <div class="meta-title">
                    <div class="post-meta post-meta-c">                                                                    
                        <h2 class="post-title-alt" style="font-size: 24px;">
                            <?php echo $node['Node']['title']; ?>
                        </h2> 
                    </div>
                </div>
                <?php
                //Se tem PDF
                if(isset($node['Multiattach']) && !empty($node['Multiattach'])){ ?>
                    <embed 
                    src="/fl/normal/<?php echo $node['Multiattach'][0]['Multiattach']['filename']; ?>" 
                    width="100%" 
                    height="800px" 
                    type='application/pdf'>
                <?php } ?>
            </article>
        </div>                                                
    </div>                                            
</section>