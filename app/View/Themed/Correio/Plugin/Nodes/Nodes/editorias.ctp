<?php echo $this->element('node_metas'); ?>
<section class="cf block blog">
    <div class="block-head-b">
        <h4 class="title">
            <?php echo $title; ?>
        </h4>
    </div>
    <div class="block-content">
        <div class="posts-dynamic posts-container ts-row list count-0">
            <div class="posts-wrap">
                <?php 
                echo $this->Mswi->verMaisAjax('editorias_ajax', $slug, 1, 6);
                ?>
            </div>
        </div>
    </div>
</section>