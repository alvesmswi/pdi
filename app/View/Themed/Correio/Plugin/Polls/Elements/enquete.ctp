<?php
$poll = $this->requestAction('/polls/polls/get_poll');
$poll_array = json_decode($poll, true);
if(!empty($poll_array)){
  echo $this->Html->css('Polls.poll'); 
?>
    <style type="text/css">

        .o-header_title, .o-header_title--medium {
            font-size: 14px;
            line-height: 120%;
            color: #333;
            font-weight: bold;
            padding: 5px 0px 15px 0px;
            position: relative;
        }
        .o-poll_options {
            padding: 10px 0px 10px 0px;
            border-top: 1px solid #e8e8e8;
            border-bottom: 1px solid #e8e8e8;
        }
        .o-poll_options input {
            box-sizing: border-box;
            width: auto;
            height: auto;
        .o-poll_footer-item {
            padding: 10px 0px 10px 0px;
        }
    </style>
  <div class="o-widget widget widget-subscribe">
    <header class="o-header">
      <span class="o-header_title o-header_title--medium">Enquete</span>
    </header>
    <?php 
    echo $this->Form->create(
      null, 
      array(
        'class'=>'o-poll', 
        'method'=>'POST', 
        'url'=>'/polls/polls/submit_poll'
      )
    );
    echo $this->Form->hidden('PollVote.poll_id', array('value' => $poll_array['poll_id'])); 
    ?>
      <fieldset class="o-poll_fieldset">
        <legend class="o-poll_title"><?php echo $poll_array['question'] ?></legend>
        <div class="o-poll_options">
          <?php 
          //se já foi votado
          if($poll_array['graph']){
            foreach ($poll_array['answers'] as $key => $option) {
            ?>
              <tr>
                  <td>
                      <label for='<?php echo $option['id'];?>'><?php echo $option['answer'];?></label>
                  </td>
                  <td style='text-align:center'>
                      <div class="progress progress-info">
                          <div class="bar" style="width: <?php echo $option['percent'];?>%"><?php echo $option['percent'];?>%</div>
                      </div>
                  </td>
              </tr>
            <?php
            }
          }else{
            //percorre as respostas  
            foreach ($poll_array['answers'] as $key => $option) { ?>
              <div class="o-poll_option">
                <div class="radio">
                  <label>
                  <input type="radio" name="data[PollVote][poll_option_id]" id="optionsRadios<?php echo $key; ?>" value="<?php echo $option['id']; ?>">
                    <?php echo $option['answer']; ?>
                  </label>
                </div>
              </div>
          <?php 
            } 
          } ?>        
        </div>

        <footer class="o-poll_footer">
          <?php 
          //se já foi votado
          if($poll_array['graph']){
          ?>
            <div class="o-poll_footer-item">
              <a href="/polls/polls/index/<?php echo $poll_array['poll_id']; ?>">Resultado Parcial</a>
            </div>
          <?php }else{ ?>
            <div class="o-poll_footer-item">
              <button type="submit" class="o-poll_btn"><span class="js-onsubmit_text">Enviar</span></button>
            </div>
          <?php } ?>          
        </footer>
      </fieldset>
    <?php echo $this->Form->end();?>
  </div>
<?php } ?>