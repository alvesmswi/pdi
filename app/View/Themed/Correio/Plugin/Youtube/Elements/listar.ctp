<?php 
$results = Cache::read('videos_youtube_home');
if(empty($results)){
	App::uses('HttpSocket', 'Network/Http');
	$API_key    = Configure::read('Youtube.api_key');
	$maxResults = Configure::read('Youtube.max_lista');
	$channelID = Configure::read('Youtube.id_canal');

	$HttpSocket = new HttpSocket();

	$results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
		'order' => 'date',
		'part' => 'snippet',
		'channelId' => $channelID,
		'maxResults' => $maxResults,
		'key' => $API_key
	));
	$results = json_decode($results->body);
	Cache::write('videos_youtube_home', $results); 
}
?>

<!--Carrossel-->
<section class="block posts-carousel has-sep" style="margin-top: 20px;">
	<h3 class="block-heading">
		<img src="/theme/Correio/img/tv-correio.png" class="title" style="height: 50px;" />
	</h3>
	<div class="the-carousel">
		<div class="posts slick-slider">
			<?php 
			if(!empty($results)){
				foreach($results->items as $item){
					if(isset($item->id->videoId)){
			?>
				<div class="lazyload">
					<!--
					<article class="post">
						<iframe width="270" height="223" src="https://www.youtube.com/embed/<?php echo $item->id->videoId; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen ></iframe>
					</article>
					-->
				</div>
			<?php 
					}
				}
			}
			?>
		</div>
		<div class="navigate">
			<a class="prev-post slick-arrow"><i class="fa fa-angle-left"></i></a>
			<a class="next-post slick-arrow"><i class="fa fa-angle-right"></i></a>
		</div>
	</div>
</section>
<br />