<?php 
//pr($nodesList);
if(!empty($nodesList)){
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}	
?>
	<section class="cf block blog">
		<div class="block-head-b">
			<h4 class="title">
				<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>">
					<?php 
					if($block['Block']['show_title']){
						echo $block['Block']['title'];
					}else{
						echo $editoriaTitle;					
					}
					?>
				</a>
			</h4> 
			<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>" class="view-all">Ver Todos</a>
		</div>
		<div class="block-content">
			<div class="posts-dynamic posts-container ts-row list">
				<div class="posts-wrap">
					<?php foreach($nodesList as $n){ 
						$editoriaTitle = 'Não informado';
						$editoriaSlug = '';
						if(isset($n['Editoria']['title']) && !empty($n['Editoria']['title'])){
							$editoriaTitle = $n['Editoria']['title'];
							$editoriaSlug = $n['Editoria']['slug'];
						}
						?>
						<div class="col-12">
							<article id="post-32" class="list-post">
								<?php 
								//se tem imagem
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '300largura'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
									?>
									<div class="post-thumb"> 
										<div class="lazyload">
											<!--
											<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
												<img width="270" height="180" 
												src="<?php echo $imagemUrl; ?>" 
												class="attachment-cheerup-list size-cheerup-list wp-post-image" 
												alt="" 
												title="<?php echo $imageTitle; ?>" 
												/> 
											</a>
											-->
										</div>
									</div>
								<?php } ?>
								<div class="content">
									<div class="post-meta post-meta-c">
										<h2 class="post-title">
											<a href="<?php echo $n['Node']['path']; ?>">
												<?php echo $n['Node']['title']; ?>
											</a>
										</h2> 
										<span class="cat-label cf"> 
											<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>" class="category">
												<?php echo $editoriaTitle; ?>
											</a> 
										</span>
									</div>
									<div class="post-content post-excerpt cf">
										<p><?php echo $n['Node']['excerpt']; ?></p>
									</div>
								</div>
							</article>
						</div>
					<?php } ?>					
				</div>
			</div>
		</div>
	</section>
<?php } ?>