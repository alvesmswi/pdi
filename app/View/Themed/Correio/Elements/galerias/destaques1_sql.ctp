<?php
//se tem algum registro
if(!empty($nodesList)){
	//array que vai guardar as noticias em destaques
	$arrayDestaques = array();
	$contador = 0;
	$maximo = 10;
	//percorre os nodes
	foreach($nodesList as $key => $nd){
		//pega a ordenação
		$ordem = $nd['Node']['ordem'];
		//se ainda não pasosu por este item
		if(!isset($arrayDestaques[$ordem])){
			//se tem ordenação
			$arrayDestaques[$ordem] = $nodesList[$key];
			$contador++;
		}
		//se chegou no maximo
		if($contador == $maximo){
			break;
		}
	}
	//substitui os dados selecionados
	$n = array_values($arrayDestaques);
?>

	<div class="ts-row cf">
		<div class="col-8 main-content cf">
			<div class="the-post the-page page-content">                            
				<div class="ts-row blocks cf wpb_row">
						
					<?php 
					//Se tem imagem
					if(isset($n[0]['Multiattach']) && !empty($n[0]['Multiattach']['filename'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n[0]['Multiattach']['filename'],
							'dimension'		=> '800largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((!empty($n[0]['Multiattach']['meta']) ? $n[0]['Multiattach']['meta'] : $n[0]['Node']['title']), ENT_QUOTES, 'UTF-8'); 
					} ?>

					<!--Destaque - Centro-->
					<div class="wpb_column vc_column_container vc_col-sm-8 col-8">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<section class="cf block news-block">
									<div class="block-content ts-row">
										<div class="col-12 large cf">
											<article class="grid-post post type-post format-standard has-post-thumbnail">
												<div class="post-header">
													<span class="cat-label cf" style="position:initial"> 
														<a href="/ultimas-noticias/<?php echo $n[0]['Editoria']['slug']; ?>" class="category" style="margin-bottom:10px">
															<?php 
															//se tem chapey
															if(isset($n[0]['NoticiumDetail']['chapeu']) && !empty($n[0]['NoticiumDetail']['chapeu'])){
																echo $n[0]['NoticiumDetail']['chapeu'];
															}else{
																echo $n[0]['Editoria']['title'];
															} 
															?>
														</a> 
													</span>
													<?php if(isset($imagemUrl)) { ?>
														<div class="post-thumb"> 
															<a href="<?php echo $n[0]['Node']['path']; ?>" class="image-link"> 
																<div class="lazyload">
																	<!--
																	<img src="<?php echo $imagemUrl; ?>" class="attachment-cheerup-grid size-cheerup-grid wp-post-image" alt="<?php echo $imageTitle; ?>" title="<?php echo $imageTitle; ?>" /> 
																	-->
																</div>
															</a>                                                                 
														</div>
													<?php } ?>
													<div class="meta-title">
														<div class="post-meta post-meta-c">															
															<h2 class="post-title-alt"> 
																<a href="<?php echo $n[0]['Node']['path']; ?>">
																	<?php 
																	//se tem titulo da capa
																	if(isset($n[0]['NoticiumDetail']['titulo_capa']) && !empty($n[0]['NoticiumDetail']['titulo_capa'])){
																		echo $n[0]['NoticiumDetail']['titulo_capa'];
																	}else{
																		echo $n[0]['Node']['title'];
																	}
																	?>
																</a>
															</h2> 
														</div>
													</div>
												</div>
												<div class="post-content post-excerpt cf">
													<p><?php echo $n[0]['Node']['excerpt']; ?></p>
												</div>
											</article>
										</div>                                                
									</div>                                            
								</section>                                                                  
							</div>
						</div>
					</div>

					<!--Destaque - Direita-->
					<div class="wpb_column vc_column_container vc_col-sm-4 col-4 destaque-direita">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<div class="sidebar wpb_content_element sticky-col">
									<div class="wpb_wrapper">
										<ul>
											<li id="bunyad-posts-widget-2" class="widget widget-posts">
												<ul class="posts cf">
													<?php if(isset($n[1])) { ?>
														<li class="post cf">
															<div class="content">
																<div class="post-meta post-meta-a">
																	<span class="post-cat">	
																	<a href="/ultimas-noticias/<?php echo $n[1]['Editoria']['slug'];?>" class="category">
																		<?php 
																		//se tem chapey
																		if(isset($n[1]['NoticiumDetail']['chapeu']) && !empty($n[1]['NoticiumDetail']['chapeu'])){
																			echo $n[1]['NoticiumDetail']['chapeu'];
																		}else{
																			echo $n[1]['Editoria']['title'];
																		} 
																		?>
																	</a>
																	</span>
																	<span class="meta-sep"></span>
																</div>
																<a href="<?php echo $n[1]['Node']['path']; ?>" class="post-title" title="<?php echo $n[1]['Node']['title']; ?>">
																	<?php echo $n[1]['Node']['title']; ?>
																</a>
																<div class="excerpt">
																	<p></p>
																</div>
															</div>
														</li>
													<?php } ?>
													<?php if(isset($n[2])) { ?>
														<li class="post cf">
															<div class="content">
																<div class="post-meta post-meta-a">
																	<span class="post-cat">	
																		<a href="/ultimas-noticias/<?php echo $n[2]['Editoria']['slug'];?>" class="category">
																			<?php 
																			//se tem chapey
																			if(isset($n[2]['NoticiumDetail']['chapeu']) && !empty($n[2]['NoticiumDetail']['chapeu'])){
																				echo $n[2]['NoticiumDetail']['chapeu'];
																			}else{
																				echo $n[2]['Editoria']['title'];
																			} 
																			?>
																		</a>
																		</span>
																	<span class="meta-sep"></span>
																</div>
																<a href="<?php echo $n[2]['Node']['path']; ?>" class="post-title" title="<?php echo $n[2]['Node']['title']; ?>">
																	<?php echo $n[2]['Node']['title']; ?>
																</a>
																<div class="excerpt">
																	<p></p>
																</div>
															</div>
														</li>
													<?php } ?>
													<?php if(isset($n[3])) { ?>	
														<li class="post cf">
															<div class="content">
																<div class="post-meta post-meta-a">
																	<span class="post-cat">	
																		<a href="/ultimas-noticias/<?php echo $n[3]['Editoria']['slug'];?>" class="category">
																			<?php 
																			//se tem chapey
																			if(isset($n[3]['NoticiumDetail']['chapeu']) && !empty($n[3]['NoticiumDetail']['chapeu'])){
																				echo $n[3]['NoticiumDetail']['chapeu'];
																			}else{
																				echo $n[3]['Editoria']['title'];
																			} 
																			?>
																		</a>
																		</span>
																	<span class="meta-sep"></span>
																</div>
																<a href="<?php echo $n[3]['Node']['path']; ?>" class="post-title" title="<?php echo $n[3]['Node']['title']; ?>">
																	<?php echo $n[3]['Node']['title']; ?>
																</a>
																<div class="excerpt">
																	<p></p>
																</div>
															</div>
														</li>
													<?php } ?>
													<?php if(isset($n[4])) { ?>
														<li class="post cf">
															<div class="content">
																<div class="post-meta post-meta-a">
																	<span class="post-cat">	
																		<a href="/ultimas-noticias/<?php echo $n[4]['Editoria']['slug'];?>" class="category">
																			<?php 
																			//se tem chapey
																			if(isset($n[4]['NoticiumDetail']['chapeu']) && !empty($n[4]['NoticiumDetail']['chapeu'])){
																				echo $n[4]['NoticiumDetail']['chapeu'];
																			}else{
																				echo $n[4]['Editoria']['title'];
																			} 
																			?>
																		</a>
																		</span>
																	<span class="meta-sep"></span>
																</div>
																<a href="<?php echo $n[4]['Node']['path']; ?>" class="post-title" title="<?php echo $n[4]['Node']['title']; ?>">
																	<?php echo $n[4]['Node']['title']; ?>
																</a>
																<div class="excerpt">
																	<p></p>
																</div>
															</div>
														</li>
													<?php } ?>
													<?php if(isset($n[5])) { ?>
														<li class="post cf">
															<div class="content">
																<div class="post-meta post-meta-a">
																	<span class="post-cat">	
																		<a href="/ultimas-noticias/<?php echo $n[5]['Editoria']['slug'];?>" class="category">
																			<?php 
																			//se tem chapey
																			if(isset($n[5]['NoticiumDetail']['chapeu']) && !empty($n[5]['NoticiumDetail']['chapeu'])){
																				echo $n[5]['NoticiumDetail']['chapeu'];
																			}else{
																				echo $n[5]['Editoria']['title'];
																			} 
																			?>
																		</a>
																		</span>
																	<span class="meta-sep"></span>
																</div>
																<a href="<?php echo $n[5]['Node']['path']; ?>" class="post-title" title="<?php echo $n[5]['Node']['title']; ?>">
																	<?php echo $n[5]['Node']['title']; ?>
																</a>
																<div class="excerpt">
																	<p></p>
																</div>
															</div>
														</li>
													<?php } ?>
												</ul>
											</li>                                              
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	
	if(isset($n[7])) { ?>
		<section class="cf block blog">
			<div class="block-content">
				<div class="posts-dynamic posts-container ts-row grid count-0">
					<div class="posts-wrap">
						<div class="col-4">
							<article id="post-356" class="grid-post post-356 post type-post status-publish format-standard has-post-thumbnail category-Região-2 tag-beauty tag-fashion tag-Economia has-excerpt">
								<div class="post-header cf">
									<?php 
									//Se tem imagem
									if(isset($n[7]['Multiattach']) && !empty($n[7]['Multiattach']['filename'])){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n[7]['Multiattach']['filename'],
											'dimension'		=> '474largura'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										$imageTitle = htmlspecialchars((!empty($n[7]['Multiattach']['meta']) ? $n[7]['Multiattach']['meta'] : $n[7]['Node']['title']), ENT_QUOTES, 'UTF-8'); 
									} ?>
									<?php if(isset($imagemUrl)) {?>
										<div class="post-thumb"> 
											<a href="<?php echo $n[7]['Node']['path']; ?>" class="image-link"> 
												<div class="lazyload">
													<!--
													<img width="370" height="247" 
													src="<?php echo $imagemUrl; ?>" 
													class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
													title="<?php echo $imageTitle; ?>" 
													srcset="<?php echo $imagemUrl; ?> 500w" 
													sizes="(max-width: 370px) 100vw, 370px" /> 
													-->
												</div>
											</a> 										
										</div>
									<?php } ?>
									<div class="meta-title">
										<div class="post-meta post-meta-c">
											<span class="cat-label cf"> 
												<a href="/ultimas-noticias/<?php echo $n[7]['Editoria']['slug'];?>" class="category">
												<?php 
												//se tem chapey
												if(isset($n[7]['NoticiumDetail']['chapeu']) && !empty($n[7]['NoticiumDetail']['chapeu'])){
													echo $n[7]['NoticiumDetail']['chapeu'];
												}else{
													echo $n[7]['Editoria']['title'];
												} 
												?>
												</a> 
											</span>
											<h2 class="post-title-alt">
												<a href="<?php echo $n[7]['Node']['path']; ?>">
													<?php echo $n[7]['Node']['title']; ?>
												</a>
											</h2> 
										</div>
									</div>
								</div>
								<div class="post-content post-excerpt cf">
									<p><?php echo $n[7]['Node']['excerpt']; ?></p>
								</div>
							</article>
						</div>
						<?php if(isset($n[8])) { ?>
							<div class="col-4">
								<article id="post-374" class="grid-post post-374 post type-post status-publish format-standard has-post-thumbnail category-Região-2 tag-diy tag-fashion tag-featured has-excerpt">
									<div class="post-header cf">
										<?php 
										//Se tem imagem
										if(isset($n[8]['Multiattach']) && !empty($n[8]['Multiattach']['filename'])){
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $n[8]['Multiattach']['filename'],
												'dimension'		=> '474largura'
											);
											$imagemUrl = $this->Html->url($imagemUrlArray);
											$imageTitle = htmlspecialchars((!empty($n[8]['Multiattach']['meta']) ? $n[8]['Multiattach']['meta'] : $n[8]['Node']['title']), ENT_QUOTES, 'UTF-8'); 
										} ?>
										<?php if(isset($imagemUrl)) {?>
											<div class="post-thumb"> 
												<a href="<?php echo $n[8]['Node']['path']; ?>" class="image-link"> 
													<div class="lazyload">
														<!--
														<img width="370" height="247" 
														src="<?php echo $imagemUrl; ?>" 
														class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
														title="<?php echo $imageTitle; ?>" 
														srcset="<?php echo $imagemUrl; ?> 500w" 
														sizes="(max-width: 370px) 100vw, 370px" /> 
														-->
													</div>
												</a> 										
											</div>
										<?php } ?>
										<div class="meta-title">
											<div class="post-meta post-meta-c">
													<span class="cat-label cf"> 
														<a href="/ultimas-noticias/<?php echo $n[8]['Editoria']['slug'];?>" class="category">
														<?php 
														//se tem chapey
														if(isset($n[8]['NoticiumDetail']['chapeu']) && !empty($n[8]['NoticiumDetail']['chapeu'])){
															echo $n[8]['NoticiumDetail']['chapeu'];
														}else{
															echo $n[8]['Editoria']['title'];
														} 
														?>
														</a> 
													</span>
												<h2 class="post-title-alt">
													<a href="<?php echo $n[8]['Node']['path']; ?>">
														<?php echo $n[8]['Node']['title']; ?>
													</a>
												</h2> 
											</div>
										</div>
									</div>
									<div class="post-content post-excerpt cf">
										<p><?php echo $n[8]['Node']['excerpt']; ?></p>
									</div>
								</article>
							</div>
						<?php } ?>
						<?php if(isset($n[9])) { ?>
							<div class="col-4">
								<article id="post-376" class="grid-post post-376 post type-post status-publish format-standard has-post-thumbnail category-Região-2 tag-diy tag-fashion tag-featured has-excerpt">
									<div class="post-header cf">
										<?php 
										//Se tem imagem
										if(isset($n[9]['Multiattach']) && !empty($n[9]['Multiattach']['filename'])){
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $n[9]['Multiattach']['filename'],
												'dimension'		=> '474largura'
											);
											$imagemUrl = $this->Html->url($imagemUrlArray);
											$imageTitle = htmlspecialchars((!empty($n[9]['Multiattach']['meta']) ? $n[9]['Multiattach']['meta'] : $n[9]['Node']['title']), ENT_QUOTES, 'UTF-8'); 
										} ?>
										<?php if(isset($imagemUrl)) {?>
											<div class="post-thumb"> 
												<a href="<?php echo $n[9]['Node']['path']; ?>" class="image-link"> 
													<div class="lazyload">
														<!--
														<img width="370" height="247" 
														src="<?php echo $imagemUrl; ?>" 
														class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
														title="<?php echo $imageTitle; ?>" 
														srcset="<?php echo $imagemUrl; ?> 500w" 
														sizes="(max-width: 370px) 100vw, 370px" /> 
														-->
													</div>
												</a> 										
											</div>
										<?php } ?>
										<div class="meta-title">
											<div class="post-meta post-meta-c">
												<span class="cat-label cf"> 
													<a href="/ultimas-noticias/<?php echo $n[9]['Editoria']['slug'];?>" class="category">
													<?php 
													//se tem chapey
													if(isset($n[9]['NoticiumDetail']['chapeu']) && !empty($n[9]['NoticiumDetail']['chapeu'])){
														echo $n[9]['NoticiumDetail']['chapeu'];
													}else{
														echo $n[9]['Editoria']['title'];
													} 
													?>
													</a> 
												</span>
												<h2 class="post-title-alt">
													<a href="<?php echo $n[9]['Node']['path']; ?>"><?php echo $n[9]['Node']['title']; ?></a></h2>
											</div>
										</div>
									</div>
									<div class="post-content post-excerpt cf">
										<p><?php echo $n[9]['Node']['excerpt']; ?></p>
									</div>
								</article>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
<?php } 

} ?>