<?php 
if(!empty($nodesList)){
?>
	<ul>
		<li id="bunyad-posts-widget-2" class="widget widget-posts">
			<h5 class="widget-title"><span>Últimas Notícias</span></h5>
			<ul class="posts cf">
				<?php 
				foreach($nodesList as $n){
					$editoriaTitle = 'Não informado';
					$editoriaSlug = '';
					if(isset($n['Editoria']['title']) && !empty($n['Editoria']['title'])){
						$editoriaTitle = $n['Editoria']['title'];
						$editoriaSlug = $n['Editoria']['slug'];
					}
					?>
					<li class="post cf">
						<?php 
						//se tem imagem
						if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> '89x67'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
							?>
								<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
									<div class="lazyload">
										<!--
										<img width="87" height="67" 
										src="<?php echo $imagemUrl; ?>" 
										class="attachment-cheerup-thumb size-cheerup-thumb wp-post-image"
										title="<?php echo $imageTitle; ?>">				
										-->
									</div>	
								</a>
						<?php } ?>
						<div class="content">
							<div class="post-meta post-meta-a">
								<span class="post-cat">	
									<a href="<?php echo $n['Node']['path']; ?>" class="category">
										<?php echo $editoriaTitle; ?>
									</a>
								</span>
								<span class="meta-sep"></span>
							</div>
							<a href="<?php echo $n['Node']['path']; ?>" class="post-title">
								<?php echo $n['Node']['title']; ?>
							</a>
							<div class="excerpt">
								<p><?php echo $n['Node']['excerpt']; ?></p>
							</div>
						</div>
					</li>
				<?php } ?>
			</ul>
		</li>
	</ul>
				
<?php } ?>