<div class="top-bar dark top-bar-b cf">
	<div class="top-bar-content" data-sticky-bar="1">
		<div class="wrap cf"> <span class="mobile-nav"><i class="fa fa-bars"></i></span>
			<div class="posts-ticker">
				<?php echo Configure::read('Site.cidade'); ?>, 
				<?php echo strftime('%d de %B de %Y', strtotime('today'));?>
			</div>
			<div class="actions">
				<div class="search-action cf">
					<form method="get" class="search-form" action="/search"> 
						<button type="submit" class="search-submit"><i class="fa fa-search"></i></button> 
						<input type="search" class="search-field" name="q" placeholder="Buscar..." value="" required />
					</form>
				</div>
			</div>
			<ul class="social-icons cf">
				<?php if(Configure::read('Social.facebook')) { ?>
					<li><a href="<?php echo Configure::read('Social.facebook'); ?>" class="fa fa-facebook" target="_blank"><span class="visuallyhidden">Facebook</span></a></li>
				<?php } ?>
				<?php if(Configure::read('Social.twitter')) { ?>
					<li><a href="<?php echo Configure::read('Social.twitter'); ?>" class="fa fa-twitter" target="_blank"><span class="visuallyhidden">Twitter</span></a></li>
				<?php } ?>
				<?php if(Configure::read('Social.google')) { ?>
					<li><a href="<?php echo Configure::read('Social.google'); ?>" class="fa fa-google-plus" target="_blank"><span class="visuallyhidden">Google Plus</span></a></li>
				<?php } ?>
				<?php if(Configure::read('Social.instagram')) { ?>
					<li><a href="<?php echo Configure::read('Social.instagram'); ?>" class="fa fa-instagram" target="_blank"><span class="visuallyhidden">Instagram</span></a></li>
				<?php } ?>
				<?php if(Configure::read('Social.pinterest')) { ?>
					<li><a href="<?php echo Configure::read('Social.pinterest'); ?>" class="fa fa-pinterest-p" target="_blank"><span class="visuallyhidden">Pinterest</span></a></li>
				<?php } ?>
				<?php if(Configure::read('Social.vimeo')) { ?>
					<li><a href="<?php echo Configure::read('Social.vimeo'); ?>" class="fa fa-vimeo" target="_blank"><span class="visuallyhidden">Vimeo</span></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>