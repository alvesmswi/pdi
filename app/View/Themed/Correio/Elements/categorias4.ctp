<?php 
//pr($nodesList);
if(!empty($nodesList)){
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}	
?>
	<section class="cf block loop-grid-3">
		<div class="block-head-b">
			<h4 class="title">
				<?php 
				if($block['Block']['show_title']){
					echo $block['Block']['title'];
				}else{
					echo $editoriaTitle;					
				}
				?>
			</h4>
			<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>" class="view-all">Ver Todos</a>
		</div>
		<div class="block-content">
			<div class="posts-dynamic posts-container ts-row grid count-0">
				<div class="posts-wrap">					
					<?php
					//percorre os nodes
					foreach($nodesList as $key => $n) { ?>
						<div class="col-4">
							<article class="grid-post post-10 post type-post status-publish format-standard has-post-thumbnail no-excerpt">
								<div class="post-header cf">
									<?php 
									//se tem imagem
									if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n['Multiattach']['filename'],
											'dimension'		=> '474largura'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
										?>
										<div class="post-thumb">
											<a href="<?php echo $n['Node']['path'];?>" class="image-link">
												<div class="lazyload">
													<!--
													<img width="370" height="247" 
													src="<?php echo $imagemUrl;?>" 
													class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
													title="<?php echo $imageTitle;?>"/>
													-->
												</div>
											</a>
										</div>
									<?php } ?>
									<div class="meta-title">
										<div class="post-meta post-meta-c">
											<h2 class="post-title-alt">
												<a href="<?php echo $n['Node']['path'];?>">
													<?php echo $n['Node']['title'];?>
												</a>
											</h2>                                                                             
										</div>
									</div>
								</div>
							</article>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>