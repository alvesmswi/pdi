<?php 
//pr($nodesList);
if(!empty($nodesList)){
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}	
?>
	<section class="cf block news-block">
		<div class="block-head-b">
			<h4 class="title">
				<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>">
					<?php 
					if($block['Block']['show_title']){
						echo $block['Block']['title'];
					}else{
						echo $editoriaTitle;					
					}
					?>
				</a>
			</h4>
			<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>" class="view-all">Ver todos</a>
		</div>
		<div class="block-content ts-row">
			<?php
			//percorre os nodes
			foreach($nodesList as $key => $n) { 
				if($key == 0){
				?>
				<div class="col-6 large cf">				
					<article class="grid-post post-7 post type-post status-publish format-standard has-post-thumbnail">
						<div class="post-header">
							<?php 
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '300largura'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
								?>
								<div class="post-thumb">
									<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
										<div class="lazyload">
											<!--
											<img width="370" height="247" 
											src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" 
											class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
											sizes="(max-width: 370px) 100vw, 370px"
											title="<?php echo $imageTitle ?>",
											alt="<?php echo $imageTitle ?>"
											> 
											-->
										</div>
									</a> 
								</div>
							<?php } ?>
							<div class="meta-title">
								<div class="post-meta post-meta-c">
									<h2 class="post-title-alt"> 
										<a href="<?php echo $n['Node']['path']; ?>">
											<?php echo $n['Node']['title']; ?>
										</a>
									</h2>
								</div>
							</div>
						</div>
						<div class="post-content post-excerpt cf">
							<p><?php echo $n['Node']['excerpt']; ?></p>
						</div>
					</article>
				</div>
				<div class="col-6 posts-list">
					<div class="ts-row">
						<?php }else{ ?>
							<article class="col-6 small-post cf">								
								<?php 
								//se tem imagem
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> 'minCat2'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
									?>
									<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
										<div class="lazyload">
											<!--
											<img 
											src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" 
											class="attachment-cheerup-thumb size-cheerup-thumb wp-post-image" 
											title="<?php echo $n['Node']['title'];?>"
											>
											-->
										</div>
									</a>
								<?php } ?>
									
								<div class="content">
									<a href="<?php echo $n['Node']['path'];?>" class="post-title" title="<?php echo $n['Node']['title'];?>">
										<?php echo substr($n['Node']['title'],0,50);?>...
									</a>
								</div>
							</article>
						<?php } ?>					
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

