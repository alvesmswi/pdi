<!-- Newsletter box -->
<ul>
	<li id="bunyad-widget-subscribe-2" class="widget widget-subscribe">
		<h5 class="widget-title">Suas atualizações diárias</h5>
		<form method="post" action="/mail_chimp/news/subscribe/ultimas-noticias" class="form" target="_blank">
			<div class="fields">
				<p class="message">Inscreva-se agora para receber as notícias por e-mail</p>
				<p><input type="email" name="email" placeholder="Seu endereço de e-mail.." required></p>
				<p><input type="submit" value="Cadastre-se"></p>
			</div>
		</form>
	</li>  
</ul>