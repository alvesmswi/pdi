<div class="navigation-wrap inline">
	<nav class="navigation inline light" data-sticky-bar="1">
		<div class="menu-main-menu-container">
			<ul id="menu-main-menu" class="menu">
				<?php
				//Se tem itens no menu
				if(!empty($menu['threaded'])){ 
					//Percorre os itens do menu
					foreach($menu['threaded'] as $item){ 
						//Se tem filhos	
						if (!empty($item['children'])) { ?>

							<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children only-full">
								<?php $url = $this->Mswi->linkToUrl($item['Link']['link']) ?>
								<a href="<?php echo $this->Html->url($url) ?>">
									<?php echo $item['Link']['title'] ?>
								</a>
								<ul class="sub-menu">
									<?php foreach ($item['children'] as $child) { ?>
										<li id="menu-item-<?php echo $child['Link']['id']; ?>" class="menu-item menu-item-type-custom menu-item-object-custom only-full">
											<?php echo $this->Html->link($child['Link']['title'], $this->Mswi->linkToUrl($child['Link']['link']), array('class' => $child['Link']['class'])); ?>                                       
										</li>
									<?php } ?>
								</ul>
							</li>
						<?php 
						//Se NÃO tem filhos
						} else { ?>
							<li id="menu-item-<?php echo $item['Link']['id']; ?>" class="menu-item menu-item-type-custom menu-item-object-custom only-full">
								<?php echo $this->Html->link($item['Link']['title'], $this->Mswi->linkToUrl($item['Link']['link']), array('class' => $item['Link']['class'])); ?>                                       
							</li>
						<?php 
						}  
					} 
					
					//Menu que só exibe quando rola a tela para baixo:
					?>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children only-roll">
						<a>Editorias</a>
						<ul class="sub-menu">
							<?php
							//Percorre os itens do menu
							foreach($menu['threaded'] as $item){ 
							?>
								<li id="menu-item-<?php echo $item['Link']['id']; ?>" class="menu-item menu-item-type-custom menu-item-object-custom">
									<?php echo $this->Html->link($item['Link']['title'], $this->Mswi->linkToUrl($item['Link']['link']), array('class' => $item['Link']['class'])); ?>  
								</li>
						<?php } ?>
						</ul>
					</li>
				<?php } ?>
				<li class="nav-icons"> <a title="Search" class="search-link"><i class="fa fa-search"></i></a>
					<div class="search-box-overlay">
						<form method="get" class="search-form" action="/search"> 
							<button type="submit" class="search-submit"><i class="fa fa-search"></i></button> 
							<input type="search" class="search-field" name="q" placeholder="Digite alguma coisa..." value="" required />
						</form>
					</div>
				</li>
			</ul>
		</div>
	</nav>
</div>