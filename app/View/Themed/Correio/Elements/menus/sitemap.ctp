<?php if (!empty($menu['threaded'])) { ?>
    <?php foreach ($menu['threaded'] as $item) { ?>
        <li id="bunyad-posts-widget-3" class="widget column col-2 widget-posts">
            <h5 class="widget-title" style="margin-bottom: 10px;"><?php echo $item['Link']['title'] ?></h5>
            <?php if (!empty($item['children'])) { ?>
                <ul class="posts cf meta-below" style="margin-bottom: 10px;">
                    <?php foreach ($item['children'] as $child) { ?>
                        <?php $link = $this->Mswi->linkToUrl($child['Link']['link']); ?>
                        <li>
                            <a href="<?php echo $this->Html->url($link) ?>"><?php echo $child['Link']['title'] ?></a>
                        </li>
                    <?php } ?>
                </ul>	
            <?php } ?>	
        </li>
    <?php } ?>
<?php } ?>