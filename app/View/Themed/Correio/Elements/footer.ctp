<?php 
//pr(Configure::read('Social'));
?>
<ul class="widgets ts-row cf">
	<li id="bunyad-widget-about-2" class="widget column col-4 widget-about">
		<div class="author-image" style="color: white; font-size: 40px" >
                <?php
                /*
                 *  TODO:  logo_branca.png precisa estar em  ../public/img/logo_branca_footer.png
                 *  se imagem não existir,  mostrar Nome do site
                */
                $webroot = new Folder(WWW_ROOT . 'img');
                $logo_footer= $webroot->find('logo_branca_footer.png', true);
                if(isset($logo_footer) && !empty($logo_footer)){
                    echo '<div class="lazyload"><!-- <div class="logo-footer"></div> --></div>';
                }else{
			        echo Configure::read('Site.title');
                }
                ?>

        </div>
		<div class="text about-text">
			<p><?php echo Configure::read('Site.tagline');?></p>
		</div>
		<div class="social-icons">
			<?php if(Configure::read('Social.facebook')) { ?>
				<a href="<?php echo Configure::read('Social.facebook'); ?>" class="social-btn" target="_blank">
					<i class="fa fa-facebook"></i>
					<span class="visuallyhidden">Facebook</span>
				</a> 
			<?php } ?>
			<?php if(Configure::read('Social.twitter')) { ?>
				<a href="<?php echo Configure::read('Social.twitter'); ?>" class="social-btn" target="_blank">
					<i class="fa fa-twitter"></i> 
					<span class="visuallyhidden">Twitter</span>
				</a> 
			<?php } ?>
			<?php if(Configure::read('Social.google')) { ?>
				<a href="<?php echo Configure::read('Social.google'); ?>" class="social-btn" target="_blank">
					<i class="fa fa-google-plus"></i> 
					<span class="visuallyhidden">Google Plus</span>
				</a> 
			<?php } ?>
			<?php if(Configure::read('Social.instagram')) { ?>
				<a href="<?php echo Configure::read('Social.instagram'); ?>" class="social-btn" target="_blank">
					<i class="fa fa-instagram"></i> 
					<span class="visuallyhidden">Instagram</span>
				</a> 
			<?php } ?>
		</div>
		<div class="text about-text below">
			<p>Copyright © <?php echo Configure::read('Site.title');?>. Por <a href="http://mswi.com.br/pdi">PDi</a>.</p>
		</div>
	</li>
	<?php echo $this->Menus->menu('sitemap', array('element' => 'menus/sitemap')); ?>
</ul>