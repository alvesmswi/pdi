<ul>
	<li id="bunyad-social-2" class="widget widget-social-b">
		<h5 class="widget-title">Siga-nos</h5>
		<ul class="social-follow cf" itemscope itemtype="http://schema.org/Organization">
			<link itemprop="url" >
			<li class="service">
				<a href="#" class="service-link facebook cf" target="_blank" itemprop="sameAs">
					<i class="the-icon fa fa-facebook"></i>
					<span class="count">952</span>
					<span class="label">Fãs</span>
				</a>
			</li>
			<li class="service">
				<a href="#" class="service-link pinterest cf" target="_blank" itemprop="sameAs">
					<i class="the-icon fa fa-pinterest-p"></i>
					<span class="count">6K</span>
					<span class="label">Fãs</span>
				</a>
			</li>
			<li class="service">
				<a href="#" class="service-link twitter cf" target="_blank" itemprop="sameAs">
					<i class="the-icon fa fa-twitter"></i>
					<span class="count">65.5K</span>
					<span class="label">Seguidores</span>
				</a>
			</li>
			<li class="service">
				<a href="#" class="service-link instagram cf" target="_blank" itemprop="sameAs">
					<i class="the-icon fa fa-instagram"></i>
					<span class="label">Seguidores</span>
				</a>
			</li>
		</ul>
	</li>
</ul>