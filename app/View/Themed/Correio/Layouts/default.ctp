<?php
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
?>
<!DOCTYPE html>
<html lang="pt-BR">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<title>
		<?php echo Configure::read('Site.title'); ?> |
		<?php echo $title_for_layout; ?>
	</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />	
    <?php echo $this->fetch('meta');?>
    
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/style.css">
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/slick.css">
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/slick-theme.css">
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/classificados.css">
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/custom.css">    
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>    

    <?php
    $this->Js->JqueryEngine->jQueryObject = '$';
    echo $this->Html->scriptBlock(
        'var $ = jQuery.noConflict();',
        array('inline' => true)
    );
    //Se foi inofmrado o user do analytics
    if (!empty(Configure::read('Service.analytics'))) {
        $arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
            ga('create', '<?= trim($analytics) ?>', 'auto', {
                'name': 'id<?= $key ?>'
            });
            ga('id<?= $key ?>.send', 'pageview');
            <?php } ?>

            /**
                * Function that tracks a click on an outbound link in Analytics.
                * This function takes a valid URL string as an argument, and uses that URL string
                * as the event label. Setting the transport method to 'beacon' lets the hit be sent
                * using 'navigator.sendBeacon' in browser that support it.
                */
            var trackOutboundLink = function (url) {
                ga('send', 'event', 'outbound', 'click', url, {
                    'transport': 'beacon',
                    'hitCallback': function () {
                        document.location = url;
                    }
                });
            }
        </script>
    <?php } ?>

    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function () {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>

    <?php
    //Se foi informado o tempo de refresh para a Home
    if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
        <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php }

    //Se foi informado o tempo de refresh para as internas
    if (		
    $this->here != '/' && 
    (isset($node['Node']['type']) && $node['Node']['type'] == 'noticia') && 
    !empty(Configure::read('Site.refresh_internas')) && 
    is_numeric(Configure::read('Site.refresh_internas'))) { 
    ?>
        <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
    <?php }
    
    echo Configure::read('Service.header');

    //Se tem algum bloco ativo nesta região
    if($this->Regions->blocks('header_scripts')){
        echo $this->Regions->blocks('header_scripts');
    }
    ?>
</head>

<body class="home page-template page-template-page-blocks page-template-page-blocks-php page page-id-853  no-sidebar skin-magazine has-slider has-slider-grid-tall home-loop-1st-large wpb-js-composer js-comp-ver-5.4.5 vc_responsive">
	<?php 
	//Se está logado
	if(isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])){
		//echo $this->element('admin_bar');
	} 
	$siteLogo = Configure::read('Site.logo');
	if(!empty($siteLogo)){
		$themeLogo = Router::url('/', true). $siteLogo;
	}else{								
		//Caminho absoluto do arquivo de CSS customizado
		$themeLogo = WWW_ROOT . 'img' . DS .$themeLogo;
		//se o arquivo de CSS personalizado existe
		if(file_exists($themeLogo)){
			$themeLogo = '/img/'.$themeLogo;
		}else{
			$themeLogo = '/theme/' . Configure::read('Site.theme').'/img/'.$themeLogo;
		}
	}
	?>
	<div class="main-wrap">
        <header id="main-head" class="main-head head-nav-below search-alt compact">
            <?php echo $this->element('bartop'); ?>
            <div class="inner inner-head" data-sticky-bar="1">
                <div class="wrap cf">
                    <div class="title"> 
                        <a  href="/" title="<?php echo Configure::read('Site.name'); ?>" rel="home">       
                            <img class="mobile-logo" src="<?php echo $themeLogo; ?>" 
                            width="205" height="26" alt="<?php echo Configure::read('Site.name'); ?>" /> 
                            <img src="<?php echo $themeLogo; ?>" class="logo-image" alt="<?php echo Configure::read('Site.name'); ?>" 
                            srcset="<?php echo $themeLogo; ?>" />
                        </a>
					</div>

                    <?php echo $this->Menus->menu('main', array('element' => 'menus/main')); ?>
                </div>
            </div>
        </header>        

        <?php if($this->Regions->blocks('super_banner_top')) { ?>
            <div class="widget-a-wrap">
                <div class="the-wrap head">
                    <?php echo $this->Regions->blocks('super_banner_top'); ?>
                </div>
            </div>
        <?php } ?>

        <div class="main wrap">
			<?php echo $this->fetch('classificados_header'); ?>
			<?php echo $this->Regions->blocks('destaques'); ?>   
          
            <!--Conteúdo padrão-->
            <div class="ts-row cf">
                <div class="col-8 main-content cf">
                    <div class="the-post the-page page-content">
                        
                        <div class="ts-row blocks cf wpb_row">
                            <div class="wpb_column vc_column_container vc_col-sm-8 col-8">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">

                                        <?php 
                                        //Exibe as mensagens
                                        echo $this->Layout->sessionFlash();

                                        //se NÃO é a Home
                                        if($this->here != '/'){                                            
                                            //Conteudo do node
                                            echo $content_for_layout;
                                        }
                        
                                        echo $this->Regions->blocks('region2'); 
                                        ?>                               
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <!--Conteúdo padrão - Direita-->                            
                            <div class="wpb_column vc_column_container vc_col-sm-4 col-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="sidebar wpb_content_element sticky-col">
                                            <div class="wpb_wrapper">
                                                <?php
                                                    if ($this->request->controller == 'classificados' && $this->request->action == 'view_anuncio') {
                                                        echo $this->element('Classificados.anuncio_contato');
                                                    }
                                                ?>
                                                <?php echo $this->Regions->blocks('right'); ?>                                                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--região que ocupa as 12 colunas-->
                        <div class="ts-row blocks cf wpb_row">
                            <div class="wpb_column vc_column_container vc_col-sm-12 col-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <?php echo $this->Regions->blocks('region3'); ?>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <footer class="main-footer dark classic">
            <div class="bg-wrap">
                <section class="upper-footer">
                    <div class="wrap">
                        <?php echo $this->Regions->blocks('footer'); ?>                          
                    </div>
                </section>
                <section class="lower-footer cf">
                    <div class="wrap">
                        <div class="bottom cf">
                            <p class="copyright">Desenvolvido por MSWI.</p>
                            <div class="to-top"> <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i> Top</a></div>
                        </div>
                    </div>
                </section>
            </div>
        </footer>
    </div>
    <div class="mobile-menu-container off-canvas"> <a  class="close"><i class="fa fa-times"></i></a>
        <div class="logo"> <img class="mobile-logo" src="<?php echo $themeLogo; ?>" width="205" height="26" alt="" /></div>
        <ul class="mobile-menu"></ul>
    </div>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/scripts.js"></script>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.lazyload-any.min.js"></script>    
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/plugins.js"></script>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/custom.js"></script>
    
    <?php
    
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('footer_scripts')){
		echo $this->Regions->blocks('footer_scripts');
	}
	echo $this->fetch('script');
	echo $this->Js->writeBuffer(); // Write cached scripts
	?>
</body>
<?php echo $this->element('sql_dump')?>
</html>