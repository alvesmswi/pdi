<!-- Services Section -->
<?php if(!empty($nodesList)) { ?>
  <div id="<?php echo $nodesList[0]['Node']['slug'] ?>" class="services">
    <div class="container">
      <div class="col-md-10 col-md-offset-1 section-title text-center">
        <h2><?php echo $nodesList[0]['Node']['title'] ?></h2>
        <hr>
        <p><?php echo $nodesList[0]['Node']['body'] ?></p>
      </div>
      <div class="row">
      <?php foreach($nodesList[0]['Multiattach'] as $image){?>
        <div class="col-xs-12 col-sm-4 service"> 
          <a href="<?php echo $image['Multiattach']['metaDisplay']?>">
            <img src="/fl/300x200/<?php echo $image['Multiattach']['filename'] ?>" class="img-responsive" alt="<?php echo $image['Multiattach']['comment']?>">
          </a>
          <h3><?php echo $image['Multiattach']['comment']?></h3>
        </div>
      <?php }?>        
      </div>
    </div>
  </div>
<?php }?>