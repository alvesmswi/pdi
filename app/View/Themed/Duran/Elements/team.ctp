<!-- Team Section -->
<?php if(!empty($nodesList)){ ?>
  <div id="<?php echo $nodesList[0]['Node']['slug'] ?>" class="text-center equipe">
    <div class="overlay">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 section-title">
          <h2><?php echo $nodesList[0]['Node']['title'] ?></h2>
          <hr>
          <p><?php echo $nodesList[0]['Node']['body'] ?></p>
        </div>
        <div id="row">
          <?php foreach($nodesList[0]['Multiattach'] as $image){ ?>
            <div class="col-md-4 col-sm-6 team">
              <div class="thumbnail"> <img src="/fl/200x200/<?php echo $image['Multiattach']['filename'] ?>" alt="<?php echo $image['Multiattach']['comment'] ?>" class="team-img">
                <div class="caption">
                  <h3><?php echo isset($image['Multiattach']['comment']) ? $image['Multiattach']['comment'] : '' ?></h3>
                  <p><?php echo isset($image['Multiattach']['metaDisplay']) ? $image['Multiattach']['metaDisplay'] : '' ?></p>
                </div>
              </div>
            </div>
          <?php }?>          
        </div>
      </div>
    </div>
  </div>
<?php }?>