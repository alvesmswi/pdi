<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="intro-text">
            <?php 
            $title = Configure::read('Site.title');
            if(!empty($title)){
              $title = explode('/', $title);
            }
            ?>
            <h1><strong><?php echo isset($title[0]) ? $title[0] : '' ?></strong> <?php 
              if(count($title > 1)){  
                  echo isset($title[1]) ? "<span><br /></span>" . $title[1] : ''; 
                }?> 
            </h1>
            <p><?php echo Configure::read('Site.tagline') ?></p>
            <?php if(!empty($nodesList)){?>
              <a href="#<?php echo $nodesList[0]['Node']['slug'] ?>" class="btn btn-custom btn-lg page-scroll">Saiba Mais</a> </div>
            <?php }?>  
        </div>
      </div>
    </div>
  </div>
</header>