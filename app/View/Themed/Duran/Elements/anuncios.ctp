<?php 
$query = '
SELECT 
Node.id, Node.title, Node.slug, Node.path,
Multiattach.filename, Multiattach.real_filename 
FROM nodes AS Node 
LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
WHERE Node.status = 1 AND Node.type = "anuncio"
GROUP BY Node.id 
LIMIT 3;';

$anuncios = $this->Mswi->query($query, 'anuncios');

if(!empty($anuncios)){ ?>
    <div id="nossos-servicos" class="services">
        <div class="container">
            <div class="section-title text-center center">
                <h2>Anúncios</h2>
                <hr>
            </div>
            <div class="categories">
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="portfolio-items">
                    <?php foreach($anuncios as $key => $anuncio) { ?>
                        <div class="col-xs-12 col-sm-4 service">
                            <?php 
                            //Se tem imagem
                            if(isset($anuncio['Multiattach']) && !empty($anuncio['Multiattach'])) {
                                if(!isset($anuncio['Multiattach']['filename']) && isset($anuncio['Multiattach'][0]['Multiattach'])) {
                                    $anuncio['Multiattach'] = $anuncio['Multiattach'][0]['Multiattach'];
                                }
                            }
                            //se tem imagem
                            if(isset($anuncio['Multiattach']) && !empty($anuncio['Multiattach']['filename'])){
                                $imagemUrlArray = array(
                                    'plugin'		=> 'Multiattach',
                                    'controller'	=> 'Multiattach',
                                    'action'		=> 'displayFile', 
                                    'admin'			=> false,
                                    'filename'		=> $anuncio['Multiattach']['filename'],
                                    'dimension'		=> '300x200'
                                );
                                $imagemUrl = $this->Html->url($imagemUrlArray);
                                ?>
                            <?php } ?>
                            <a href="<?php echo $anuncio['Node']['path']; ?>">                                
                                <img src="<?php echo $imagemUrl; ?>" class="img-responsive"> 
                                <h3><?php echo $anuncio['Node']['title']; ?></h3>
                            </a> 
                        </div>
                    <?php } ?>            
                </div>
            </div>
        </div>
    </div>
<?php } ?>
