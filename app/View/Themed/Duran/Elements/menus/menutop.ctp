<!-- Collect the nav links, forms, and other content for toggling -->
  <?php if (!empty($menu['threaded'])){ ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
      <?php foreach($menu['threaded'] as $item) {
        // pr($item['children']);exit;
        if(!empty($item['children'])){ ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $item['Link']['title']?><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <?php foreach ($item['children'] as $child) { ?>
                    <li><a href="<?php echo $child['Link']['link']?>"><?php echo $child['Link']['title'];?></a></li>
                <?php } ?>
              </ul>
					</li>
        <?php }else{
          if($this->here == '/'){ ?>
            <li><a href="<?php echo $item['Link']['link']?>" class="page-scroll"><?php echo $item['Link']['title']?></a></li>
          <?php }else{ ?>
            <li><a href="/<?php echo $item['Link']['link']?>" class="page-scroll"><?php echo $item['Link']['title']?></a></li>
      <?php }}}?>   
      <div class="search-form">
        <form method="get" class="search form-inline" action="/search">
          <input type="search" class="form-control input-sm" name="q" placeholder="Digite alguma coisa..." value="" required />
          <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
        </form>
      </div>       
      </ul>
    </div>
  <?php }?>
<!-- /.navbar-collapse --> 

