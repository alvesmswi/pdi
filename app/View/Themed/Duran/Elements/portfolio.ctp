<!-- Portfolio Section -->
<?php if(!empty($nodesList)){ ?>
  <div id="<?php echo $nodesList[0]['Node']['slug'] ?>" class="portfolio">
    <div class="container">
      <div class="section-title text-center center">
        <h2><?php echo $nodesList[0]['Node']['title'] ?></h2>
        <hr>
        <p><?php echo $nodesList[0]['Node']['body'] ?></p>
      </div>
      <div class="categories">
        <ul class="cat">
          <li>
            <ol class="type">
              <li><a href="#" data-filter="*" class="active">Todos os Projetos</a></li>              
              <?php
              $arrVerify = array(); 
              foreach($nodesList[0]['Multiattach'] as $fotoTitle){                 
                if(!in_array($fotoTitle['Multiattach']['metaDisplay'], $arrVerify)){
                  $arrVerify[] .= $fotoTitle['Multiattach']['metaDisplay'];
              ?>
                <li><a href="#" data-filter=".<?php echo strtolower($fotoTitle['Multiattach']['metaDisplay'])?>"><?php echo $fotoTitle['Multiattach']['metaDisplay']?></a></li>              
              <?php }}?>
            </ol>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="row">
        <div class="portfolio-items">
        <?php foreach($nodesList[0]['Multiattach'] as $image){?>
            <div class="col-sm-6 col-md-4 col-lg-4 <?php echo strtolower($image['Multiattach']['metaDisplay'])?>">
              <div class="portfolio-item">
                <div class="hover-bg"> <a href="/fl/1920x1200/<?php echo $image['Multiattach']['filename'] ?>" title="<?php echo $image['Multiattach']['comment'] ?>" data-lightbox-gallery="gallery1">
                  <div class="hover-text">
                    <h4><?php echo $image['Multiattach']['comment'] ?></h4>
                  </div>
                  <img src="/fl/360x240/<?php echo $image['Multiattach']['filename'] ?>" class="img-responsive" alt="<?php echo $image['Multiattach']['comment'] ?>"> </a> </div>
              </div>
            </div>
          <?php }?>         
        </div>
      </div>
    </div>
  </div>
<?php }?>