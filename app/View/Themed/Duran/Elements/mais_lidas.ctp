<?php 
$query = '
SELECT id, title, slug, path, lida FROM nodes AS Node
WHERE id <> 1 AND STATUS = 1 
ORDER BY lida DESC LIMIT 8;';

$maisLidas = $this->Mswi->query($query, 'maislidas');

if(!empty($maisLidas)){ ?>
<div class="mais-lidas block" style="margin-top: 20px;">
    <h2 class="title text-center">Mais Lidas</h2>
    <hr />
    <div class="content"> 
        <ol type="1" style="list-style-type:decimal-leading-zero;padding-left: 20px;">
            <?php 
            foreach($maisLidas as $key => $node){
                echo '<li>';
                    echo '<a href="'.$node['Node']['path'].'">'.$node['Node']['title'].'</a>';
                echo '</li>';
            }
            ?>  
        </ol>   
    </div>
</div>

<?php } ?>