<!-- About Section -->
<?php if(!empty($nodesList)){ 
  $image = isset($nodesList[0]['Multiattach'][0]['Multiattach']['filename']) ? $nodesList[0]['Multiattach'][0]['Multiattach']['filename'] : '';
  ?>
  <div id="<?php echo $nodesList[0]['Node']['slug'] ?>" class="about">
    <div class="container">
      <div class="section-title text-center center">
        <h2><?php echo $nodesList[0]['Node']['title'] ?></h2>
        <hr>
      </div>
      <div class="row">
      <?php /*if(!empty($image)){?>    
        <div class="col-xs-12 col-md-6 text-center"> <img src="/fl/500x331/<?php echo $image ?>" class="img-responsive" alt=""> </div>
      <?php } */?>
        <div class="col-xs-12 col-md-12">
          <div class="about-text">
            <?php echo $nodesList[0]['Node']['body'] ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }?>