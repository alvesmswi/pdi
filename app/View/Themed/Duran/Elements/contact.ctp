<!-- Contact Section -->
<?php if(!empty($nodesList)){ ?>
  <div id="contact">
    <div class="container">
      <div class="section-title text-center">
        <h2><?php echo $nodesList[0]['Node']['title'] ?></h2>
        <hr>
        <p><?php echo $nodesList[0]['Node']['body'] ?></p>
      </div>
      <div class="col-md-4">
        <h3>Informações de Contato</h3>
        <div class="contact-item"> <span>Endereço</span>
          <p><?php echo Configure::read('Site.endereco')?><br>
            <?php echo Configure::read('Site.estado')?>, <?php echo Configure::read('Site.cidade')?></p>
        </div>
        <div class="contact-item"> <span>Email</span>
          <p><?php echo Configure::read('Site.email')?></p>
        </div>
        <div class="contact-item"> <span>Telefone</span>
          <p></p>
        </div>
      </div>
      <div class="col-md-8">
        <h3>Envie uma Mensagem</h3>
        <form name="sentMessage" id="contactForm" novalidate>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" id="name" class="form-control" placeholder="Name" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="email" id="email" class="form-control" placeholder="Email" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
            <p class="help-block text-danger"></p>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-custom btn-lg">Enviar Mensagem</button>
        </form>
      </div>
    </div>
  </div>
<?php }?>