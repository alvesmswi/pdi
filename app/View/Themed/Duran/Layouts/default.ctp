<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ambiente Duran</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="/theme/<?php echo Configure::read('Site.theme')?>/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="/theme/<?php echo Configure::read('Site.theme')?>/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="/theme/<?php echo Configure::read('Site.theme')?>/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/theme/<?php echo Configure::read('Site.theme')?>/img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="/theme/<?php echo Configure::read('Site.theme')?>/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/theme/<?php echo Configure::read('Site.theme')?>/fonts/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css"  href="/theme/<?php echo Configure::read('Site.theme')?>/css/style.css">
<link rel="stylesheet" type="text/css" href="/theme/<?php echo Configure::read('Site.theme')?>/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="/theme/<?php echo Configure::read('Site.theme')?>/css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
        $this->Js->JqueryEngine->jQueryObject = '$';
        echo $this->Html->scriptBlock(
            'var $ = jQuery.noConflict();',
            array('inline' => true)
        );
        
        if (!empty(Configure::read('Service.analytics'))) {
        $arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
            ga('create', '<?= trim($analytics) ?>', 'auto', {
                'name': 'id<?= $key ?>'
            });
            ga('id<?= $key ?>.send', 'pageview');
            <?php } ?>

            /**
                * Function that tracks a click on an outbound link in Analytics.
                * This function takes a valid URL string as an argument, and uses that URL string
                * as the event label. Setting the transport method to 'beacon' lets the hit be sent
                * using 'navigator.sendBeacon' in browser that support it.
                */
            var trackOutboundLink = function (url) {
                ga('send', 'event', 'outbound', 'click', url, {
                    'transport': 'beacon',
                    'hitCallback': function () {
                        document.location = url;
                    }
                });
            }
        </script>
    <?php } ?>

    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function () {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>

  <?php
    //Se foi informado o tempo de refresh para a Home
    if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
        <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php }

    //Se foi informado o tempo de refresh para as internas
    if (		
    $this->here != '/' && 
    (isset($node['Node']['type']) && $node['Node']['type'] == 'noticia') && 
    !empty(Configure::read('Site.refresh_internas')) && 
    is_numeric(Configure::read('Site.refresh_internas'))) { 
    ?>
        <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
    <?php }
    
    echo Configure::read('Service.header');

    //Se tem algum bloco ativo nesta região
    if($this->Regions->blocks('header_scripts')){
        echo $this->Regions->blocks('header_scripts');
    }
    ?>
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<?php 
if(isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])){
  echo $this->element('admin_bar'); ?>
  <nav id="menu" class="navbar navbar-default" style="margin: 0px;">
<?php 
} else {
?>  
  <nav id="menu" class="navbar navbar-default navbar-fixed-top">
<?php } ?>
<!-- Navigation
    ==========================================-->
  <div class="container">  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a href="/">
        <img src="/theme/<?php echo Configure::read('Site.theme')?>/img/logo-horizontal.png" style="width:220px">
      </a>
       </div>
    
      <?php echo $this->Menus->menu('menutop', array('element' => 'menus/menutop')); ?>

  </div>
</nav>

  <?php
    
  ?>
  <?php 
  //Exibe as mensagens
  echo $this->Layout->sessionFlash();

  //Conteudo do node
  echo $content_for_layout;
  ?>                  

<!-- Footer -->

 <div id="footer">
  <div class="container text-center">
    <div class="social">
      <ul>
        <?php if (!empty(Configure::read('Social.facebook'))) { ?>
          <li><a href="<?php echo Configure::read('Social.facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
        <?php }?>
        <?php if (!empty(Configure::read('Social.twitter'))) { ?>
          <li><a href="<?php echo Configure::read('Social.twitter'); ?>"><i class="fa fa-twitter"></i></a></li>
        <?php }?>
        <?php if (!empty(Configure::read('Social.youtube'))) { ?>
          <li><a href="<?php echo Configure::read('Social.youtube'); ?>"><i class="fa fa-youtube"></i></a></li>
        <?php }?>
        <?php if (!empty(Configure::read('Social.instagram'))) { ?>
          <li><a href="<?php echo Configure::read('Social.instagram'); ?>"><i class="fa fa-instagram"></i></a></li>
        <?php }?>
        <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-pinterest"></i></a></li> -->        
      </ul>
    </div>
    <div>
      <p>&copy; <?php echo date("Y");?> Ambiente Duran. All rights reserved. Designed by <a href="http://www.mswi.com.br" rel="nofollow">MSWI</a></p>
    </div>
  </div>
</div>

<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/bootstrap.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.isotope.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/contact_me.js"></script> 
<script type="text/javascript" src="/theme/<?php echo Configure::read('Site.theme')?>/js/main.js"></script>
<?php
//Se tem algum bloco ativo nesta região
if($this->Regions->blocks('footer_scripts')){
  echo $this->Regions->blocks('footer_scripts');
}
echo $this->fetch('script');
echo $this->Js->writeBuffer(); // Write cached scripts
?>
</body>
</html>
