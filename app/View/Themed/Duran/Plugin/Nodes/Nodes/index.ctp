	<?php 
	//se não está vazio
	if(!empty($nodes)){ ?>
	<div class="container" style="margin-top:120px">
		<div class="col-md-8">
			<hr class="titulo-noticias">
			<h1>
				<?php
				//se teve uma pesquisa
				if(isset($this->params['type']) && !empty($this->params['type'])) {
					echo __d('croogo', $this->Mswi->typeName($this->params['type']));
				}else{
					echo __d('croogo', 'Resultado da pesquisa');
				}
				?>
			</h1>
			<?php foreach($nodes as $key => $n){ ?>
				<div class="row">
					<div class="col-md-4">																	
						<?php
							$link_noticia = $n['Node']['path'];
							//Se tem link externo
							if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
								$link_noticia = $n['NoticiumDetail']['link_externo'];
							}
						?>
						<a href="<?php echo $link_noticia; ?>">
							<?php 
							//Se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
								if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
									$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
								}
							}
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> 'thumLista'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
								?>
								<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="thumbnail" alt="<?php echo $imageTitle ?>" /> 
							<?php } ?>									
						</a>
					</div>
					<!--coluna direita da lista-->
					<div class="col-md-8" id="col-right">
					<?php if(!empty($n['Node']['terms'])){?>
						<div class="chapeu badge">
							<?php 
							//se tem chapeu
							if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
								echo $n['NoticiumDetail']['chapeu'];
							}else{
								echo $this->Mswi->categoryName($n['Node']['terms']);
							}; 
							?>
						</div>
					<?php }?>
						<h3 class="titulo">
							<a href="<?php echo $link_noticia; ?>">
								<?php echo $n['Node']['title']; ?>
							</a>
						</h3>
						<div class="chamada">
							<a href="<?php echo $link_noticia; ?>">
								<?php echo $n['Node']['excerpt']; ?>
							</a>
						</div>
					</div>
				</div>
			<?php	
			}
			//Insere o paginado
			echo $this->element('paginator');
			}else{
				echo __d('croogo', 'Nenhum registro encontrado.');
			} ?>
		</div>
		<div class="col-md-4">
            <?php 
                //Se tem algum bloco ativo nesta região
                if($this->Regions->blocks('banner')){
                echo $this->Regions->blocks('banner');
                } 
            ?>
        </div>	
	</div>