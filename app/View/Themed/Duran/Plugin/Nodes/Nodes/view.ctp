<article class="container" style="margin-top:130px">
    <div class="row">
        <div class="col-md-8">
                <h2 class="text-center"><?php echo $node['Node']['title']; ?></h2>
                 <hr>
                 <?php echo $node['Node']['body']; ?>
                 <hr style="width:100%">
        </div>
        <div class="col-md-4">
            <?php 
                //Se tem algum bloco ativo nesta região
                if($this->Regions->blocks('banner')){
                echo $this->Regions->blocks('banner');
                } 
            ?>
        </div>
    </div>    
</article>