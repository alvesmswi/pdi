<?php echo $this->element('node_metas'); ?>
<?php 
	$cxGoogle = Configure::read('Site.busca_google');
?>
<article class="container" style="margin-top:130px">
    <div class="row">
        <div class="col-md-8">
			<h2 class="text-center">            
				<?php 
				//se teve uma pesquisa
				if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
					echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
				}else{
					echo __d('croogo', 'Resultado da pesquisa');
				}
				?>
				<hr>
			</h2>	
			</span>
			<style>
				.gsc-control-cse{
					line-height: normal;
				}
				.gsc-control-cse form{
					line-height: 0;
				}
				.gsc-adBlock{
					display: none;
				}
				.gsc-control-cse table{
					margin: 0;
				}
			</style>
			<script>
				(function() {
					var cx = '<?php echo Configure::read('Site.busca_google')?>';
					var gcse = document.createElement('script');
					gcse.type = 'text/javascript';
					gcse.async = true;
					gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(gcse, s);
				})();
			</script>
			<gcse:search></gcse:search>
		</div>
		 <div class="col-md-4">
            <?php 
                //Se tem algum bloco ativo nesta região
                if($this->Regions->blocks('banner')){
                echo $this->Regions->blocks('banner');
                } 
            ?>
        </div>
	</div>
</article>