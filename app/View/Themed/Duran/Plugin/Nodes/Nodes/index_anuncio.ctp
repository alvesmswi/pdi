<article class="container" style="margin-top:130px">
    <div class="row">
        <div class="col-md-8">
            <h2 class="text-center">Anúncios</h2>
            <hr style="width:100%">
            <div class="lista">
                <?php 
                Configure::write('debug',1);
                //pr($nodes);
                if(!empty($nodes)){
                    foreach($nodes as $n){ ?>
                        <a href="<?php echo $n['Node']['path']; ?>">
                            <div class="row">
                                <div class="col-sm-4 item">
                                    <?php 
                                    //Se tem imagem
                                    if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
                                        if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
                                            $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
                                        }
                                    }
                                    //se tem imagem
                                    if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
                                        $imagemUrlArray = array(
                                            'plugin'		=> 'Multiattach',
                                            'controller'	=> 'Multiattach',
                                            'action'		=> 'displayFile', 
                                            'admin'			=> false,
                                            'filename'		=> $n['Multiattach']['filename'],
                                            'dimension'		=> 'thumLista'
                                        );
                                        $imagemUrl = $this->Html->url($imagemUrlArray);
                                        $imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                                        ?>
                                        <img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="thumbnail" alt="<?php echo $imageTitle ?>" /> 
                                    <?php } ?>
                                </div>                        
                                <div class="col-sm-8 item">
                                    <div class="titulo"><?php echo $n['Node']['title']; ?></div>
                                    <div class="valor">Valor: <?php echo $n['AnuncioDetail']['valor']; ?></div>
                                    <div class="valor">Responsável: <?php echo $n['AnuncioDetail']['responsavel']; ?></div>
                                    <div class="valor">Telefone: <?php echo $n['AnuncioDetail']['telefone']; ?></div>
                                    <div class="valor">Publicação: <?php echo date('d/m/Y H:i', strtotime($n['Node']['publish_start'])); ?></div>
                                </div>
                            </div>
                        </a>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php 
                //Se tem algum bloco ativo nesta região
                if($this->Regions->blocks('banner')){
                    echo $this->Regions->blocks('banner');
                } 
            ?>
        </div>
    </div>
</article>