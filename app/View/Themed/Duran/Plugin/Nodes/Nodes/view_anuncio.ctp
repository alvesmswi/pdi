<article class="container anuncio" style="margin-top:130px">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-sm-6 item">
                    <?php 
                   // pr($node);
                    //se tem imagem
                    if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
                        ?>
                        <div class="galeria">
                            <?php 
                            
                                foreach($node['Multiattach'] as $key => $imagem){
                                    $imagemUrlArray = array(
                                        'plugin'		=> 'Multiattach',
                                        'controller'	=> 'Multiattach',
                                        'action'		=> 'displayFile', 
                                        'admin'			=> false,
                                        'filename'		=> $imagem['Multiattach']['filename'],
                                        'dimension'		=> '300largura'
                                    );
                                    $imagemUrl = $this->Html->url($imagemUrlArray);
                                    $imagemUrlArray = array(
                                        'plugin'		=> 'Multiattach',
                                        'controller'	=> 'Multiattach',
                                        'action'		=> 'displayFile', 
                                        'admin'			=> false,
                                        'filename'		=> $imagem['Multiattach']['filename'],
                                        'dimension'		=> 'normal'
                                    );
                                    $imagemUrlFull = $this->Html->url($imagemUrlArray);
                                    $imageTitle = htmlspecialchars((isset($node['Multiattach']['meta']) && !empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
                                    $display = '';
                                    if($key > 0){
                                        $display = 'display:none;';
                                    }
                                ?>
                                <a data-lightbox-gallery="galeria1" href="<?php echo $imagemUrlFull ?>" style="<?php echo $display; ?>">
                                    <img src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" class="thumbnail" alt="<?php echo $imageTitle ?>" /> 
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>                        
                <div class="col-sm-6 item">
                    <h2><?php echo $node['Node']['title']; ?></h2>
                    <div class="valor">Valor: <?php echo $node['AnuncioDetail']['valor']; ?></div>
                    <div class="valor">Responsável: <?php echo $node['AnuncioDetail']['responsavel']; ?></div>
                    <div class="telefone">Telefone: <?php echo $node['AnuncioDetail']['telefone']; ?></div>
                    <div class="valor">Publicação: <?php echo date('d/m/Y H:i', strtotime($node['Node']['publish_start'])); ?></div>
                    <br />
                    <div class="corpo"><?php echo $node['Node']['body']; ?></div>
                </div>
            </div>           
            
        </div>
        <div class="col-md-4">
            <?php 
                //Se tem algum bloco ativo nesta regi�o
                if($this->Regions->blocks('banner')){
                echo $this->Regions->blocks('banner');
                } 
            ?>
        </div>
    </div>    
</article>