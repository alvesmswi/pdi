
$(document).ready(function() {

    // check if arg (paragraph) is empty
    function isEmpty(arg) {
        return $(arg).text().replace(/^\s+|\s+$/g, '').length == 0;
    }
    // Looping over each paragraph inside content's articles
    $('article .content').find('p').each(function(){
        // remove styles inline
        $(this).removeAttr('style');

        // remove empty paragraphs
        if (isEmpty($(this)) ) {
            $(this).remove();
        }

        var content = '';
        // Looping over each span
        $(this).find('span').each(function(){

            // remove styles inline
            $(this).removeAttr('style');

            if(!isEmpty($(this))){
                // Check if this node has span child node
                if ($(this).has('span').length) {
                    // mark span to be deleted
                    $(this).attr('delete', true);
                } else {
                    // Concat content inside a var
                    //  content += $(this).text();
                    content += $(this).html();
                    //  After store content' span, that will be removed
                    $(this).remove();
                }
            
            } else {
                // Remove empty's paragraphs
                $(this).remove();
            }
        });
        $('[delete=true]').remove();
        // insert content the current paragrah
        $(this).append(content);

    });


    // Looping over each div in content's article
    $('article .content').find('div[style]').each(function () {
        // remove styles inline
        $(this).removeAttr('style');

        if (!isEmpty($(this))) {
            console.log(isEmpty($(this)));
            var imgContent = '';
            $(this).find('figure').each(function(){
                $(this).find('img').each(function () {
                    $(this).removeAttr('style width height');
                });
                var figCap = '';
                if ($(this).has('figcaption').length){
                    figCap += $(this).find('figcaption').text();
                    $(this).find('figcaption').remove();
                }
                imgContent += '<figure><div class="img-responsive">';
                imgContent += $(this).html();
                imgContent += '</div><figcaption>' + figCap +'</figcaption></figure>';
                $(this).remove();
            });
            $(this).append(imgContent);

        } else {
            // Remove empty's paragraphs
            $(this).remove();
        }
    });
    // looping div
    
});
// end document ready

