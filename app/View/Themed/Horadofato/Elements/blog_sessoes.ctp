<?php 
$arrayKeywords = array();
//se é Post
if(isset($post) && (isset($post['Post']['keywords']) && !empty($post['Post']['keywords']))){
	$arrayKeywords = explode(',',$post['Post']['keywords']);
//se for blog	
}else if(isset($posts) && (isset($posts[0]['Blog']['keywords']) && !empty($posts[0]['Blog']['keywords']))){
	$arrayKeywords = explode(',',$posts[0]['Blog']['keywords']);
}
if(!empty($arrayKeywords)){ ?>
	<div class="blog__sessoes accordion" id="accordionSessao">
		<div class="container-fluid blog__sessoes__content">
			<h3 class="headover--label">Sessões</h3>
			<ul class="content-accordion collapse" id="contentAccordionSessao" aria-expanded="false">
				<?php					
				foreach($arrayKeywords as $tag){
					echo '<li><a href="/search?q='.trim($tag).'">'.trim($tag).'</a></li>';
				}
				?>
			</ul>
		</div>
		<div class="container sessoes--btn-expand text-center">
			<button class="btn btn--expand btn__icon collapsed" data-toggle="collapse" data-target="#contentAccordionSessao" data-parent="#accordionSessao" aria-expanded="false"
			aria-controls="contentAccordionSessao">
				<span class="fa fa-chevron-down"></span>
			</button>
		</div>
	</div>
<?php } ?>