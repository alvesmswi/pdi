<?php 
//Se tiver mais de uma página
if($this->Paginator->params()['pageCount'] > 1){ ?>
	<div class="row">
		<div class="col-lg-12 text-center">
			<ul class="pagination bp-pagination">
				<?php
				echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
				echo $this->Paginator->next('>', array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				?>
			</ul>
		</div>
	</div>
<?php } ?>