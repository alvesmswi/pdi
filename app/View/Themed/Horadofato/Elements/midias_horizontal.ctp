<div class="container sem-padding">
	<div class="col-lg-4 col-md-6 col-sm-12 sem-padding">
		<div class="newsletter-box" data-pg-collapsed="">
			<form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
				<div class="form-group" data-pg-collapsed="">
					<label class="headover--label" for="to-comment">Assine nossa newsletter</label>
					<input id="email-newsletter" name="email" class="form-control" type="email" required="true"> 
				</div>
				<div class="text-center" data-pg-collapsed="">
					<button type="submit" class="btn btn-default">Receber</button>
				</div>
			</form>
		</div>
		<!-- Newsletter box -->
	</div>
	<div class="col-lg-4 col-md-6 col-sm-12 sem-padding" data-pg-collapsed="">
		<div class="buttons-box" data-pg-collapsed="">
			<a href="<?php echo Configure::read('Social.facebook');?>" class="btn btn-default btn--facebook" target="_blank"><span class="icon fa fa-lg fa-facebook"></span>Curta nossa página</a>
			<a href="<?php echo Configure::read('Social.twitter');?>" class="btn btn-default btn--twitter" target="_blank"><span class="icon fa fa-lg fa-twitter"></span>Siga-nos no twitter</a>
		</div>
		<!-- Botões curtir e compartilhar -->
	</div>
	<div class="col-lg-4 col-md-6 col-sm-12 sem-padding" data-pg-collapsed="">
		<div id="impresso-box">
			<!-- Botões curtir e compartilhar -->
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div>
					<div class="text-center" data-pg-collapsed="">
						<a href="/assinatura/" class="btn btn-default">Assinatura digital</a>
					</div>
					<div class="text-center">
						<a href="/edital" class="btn btn-default">Publicidade legal</a>
					</div>
					<div class="text-center">
						<a href="/jornaldoestado/impresso" class="btn btn-default">Edição impressa</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<?php 
				//se está autenticado
				if(AuthComponent::user('id')){
					$link = $nodesList[0]['Node']['path'];
				}else{
					$link = '/users/users/login?redirect='.$nodesList[0]['Node']['path'];
				}
				?>
				<a href="<?php echo $link; ?>" target="_blank">
					<?php 
					$imagemUrlArray = array(
						'plugin'        => 'Multiattach',
						'controller'    => 'Multiattach',
						'action'        => 'displayFile', 
						'admin'         => false,
						'filename'      => $nodesList[0]['Multiattach']['filename'],
						'dimension'     => '300largura'
					);
					$imagemUrl = $this->Html->url($imagemUrlArray);
					?>
					<img src="<?php echo $imagemUrl; ?>">
				</a>
			</div>
		</div>
		<!-- Botões curtir e compartilhar -->
	</div>
</div>