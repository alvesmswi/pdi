<style>
    .bloco_assine_ja {
        box-shadow: 0 0 30px hsla(0,0%,80%,.9);
        width: 100%;
        position: relative;
        /* border: 1px solid #ccc; */
        clear: both;
        margin-bottom: 30px;
    }

    .bloco_assine_ja:before {
        content: "";
        height: 150px;
        width: 100%;
        position: absolute;
        top: -151px;
        background: linear-gradient(180deg,hsla(0,0%,100%,0),hsla(0,0%,100%,.6) 33%,hsla(0,0%,100%,.9) 80%,hsla(0,0%,100%,.5));
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#00ff0000",endColorstr="#00ff0000",GradientType=0);
    }

    .bloco_assine_ja .title {
        border-bottom: 1px solid #e6e6e6;
        padding: 35px;
        text-align: center;
    }

    .buttons_assine {
        padding: 25px 0;
    }

    .buttons_assine:after, .buttons_assine:before {
        content: " ";
        display: table;
    }

    .buttons_assine .login-normal {
        width: 50%;
        float: left;
    }
    .buttons_assine .login-normal-container, 
    .buttons_assine .login-facebook-container {
        width: 50%;
        margin: 0 auto;
        text-align: center;
    }

    .buttons_assine .login-facebook {
        width: 50%;
        float: right;
        border-left: 1px solid #e6e6e6;
    }
    
</style>

<div class="bloco_assine_ja">
    <div class="title">
        <h2><?php echo Configure::read('Paywall.PaywallMensagem'); ?></h2>
    </div>
    <div class="buttons_assine">
        <div class="login-normal">
            <div class="login-normal-container">
                <h3>Já é assinante?</h3>
                <a href="/users/users/login?redirect=<?php echo $this->here ?>">Entrar</a>
            </div>
        </div>
        <div class="login-facebook">
            <div class="login-facebook-container">
                <h3>Não sou assinante</h3>
                <!-- <button type="button" class="btn">ENTRAR COM FACEBOOK</button> -->
                <a href="/paywall/paywall/cadastro">Cadastre-se já!</a>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>