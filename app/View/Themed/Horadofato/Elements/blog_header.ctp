<?php 
//se for um blog ou post    
if($this->action != 'keywords' && (isset($post) || isset($posts))){
    if(isset($posts) && isset($posts[0])){
        $blogName       = $posts[0]['Blog']['title'];
        $blogDescr      = $posts[0]['Blog']['descricao'] ? $posts[0]['Blog']['descricao'] : $posts[0]['Blog']['title'];
        $blogName       = $posts[0]['Blog']['title'];
        $blogImage      = $posts[0]['Blog']['capa'] ? $posts[0]['Blog']['capa'] : '';
        $blogaAvatar    = $posts[0]['Blog']['image'] ? $posts[0]['Blog']['image'] : '/theme/'.Configure::read('Site.theme').'/images/sem-avatar.png';
    }
?>
    <article class="blog__header container-fluid">
        <figure class="blog-header__bg bp-media" style="background-color: #ffc800; ">
            <div class="img-responsive">
                <?php if(!empty($blogImage)) { ?>
                    <img itemprop="image" src="<?php echo $blogImage; ?>" alt="<?php echo $blogName; ?>" />
                <?php } ?>
            </div>
        </figure>
        <header class="container">
            <figure class="blog-header__avatar">
                <img src="<?php echo $blogaAvatar; ?>" alt="">
            </figure>
            <div class="blog-header__title">
                <div class="container-fluid">
                    <div class="title-bg row">
                        <div class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4">
                            <h1><?php echo $blogName; ?></h1>
                        </div>
                    </div>
                    <div class="subtitle-bg row">
                        <div class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4">
                            <h2><?php echo $blogDescr; ?></h2>
                        </div>
                    </div>
                </div>        
            </div>
        </header>
    </article>
<?php } ?>