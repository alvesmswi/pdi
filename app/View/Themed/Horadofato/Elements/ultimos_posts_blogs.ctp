<?php 
if (isset($posts) && !empty($posts)) { ?>
		<h1 class="section__title purple">Últimas de Blogs</h1>

		<ul class="list-group">
			<?php foreach($posts as $n){ ?>
				<li class="list-group-item">
					<article>
						<a href="/blog/<?php echo $n['Blog']['slug']?>">
							<header>
								<div class="heading" role="heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="blog-box__avatar pull-left">
												<figure class="bp-avatar">
													<div class="img-responsive">
														<img src="<?php echo $n['Blog']['image']; ?>" alt="<?php echo $n['Blog']['title']; ?>">
													</div>
												</figure>
											</div>
											<div class="blog-box__heading-txt pull-left">
												<h3 class="blog-box__name"><?php echo $n['Blog']['title']; ?></h3>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<span class="headover"><?php echo date('d/m H:i', strtotime($n['Post']['publish_start'])); ?></span>
										</div>
									</div>
									<div class="row">
										<a href="/blog/<?php echo $n['Blog']['slug']?>/post/<?php echo $n['Post']['slug']?>">
											<?php 
											$classTxt1 = '';
											if(isset($n['Images'][0]['url']) && !empty($n['Images'][0]['url'])){ 
												$classTxt1 = 'col-lg-6';
												?>
												<div class="<?php echo $classTxt1;?> col-md-12 col-sm-12">
													<div class="container">
														<figure class="bp-media">
															<div class="img-responsive">
																<img src="<?php echo $n['Images'][0]['url']; ?>" alt="<?php echo $n['Post']['title']; ?>">
															</div>
														</figure>
													</div>
												</div>
											<?php } ?>
											<div class="<?php echo $classTxt1;?>  col-md-12 col-sm-12">
												<div class="container">												
													<h1><?php echo $n['Post']['title']; ?></h1>
												</div>
											</div>
										</a>
									</div>								
								</div>
							</header>
						</a>
					</article>
				</li>	
			<?php } ?>		
		</ul>
<?php } ?>