<!-- Botões curtir e compartilhar -->
<div class="buttons-box">
	<a href="<?php echo Configure::read('Social.facebook');?>" target="_blank" class="btn btn-default btn--facebook">
		<span class="icon fa fa-lg fa-facebook"></span>Curta nossa página
	</a>
	<a href="<?php echo Configure::read('Social.twitter');?>" target="_blank" class="btn btn-default btn--twitter">
		<span class="icon fa fa-lg fa-twitter"></span>Siga-nos no twitter
	</a>
	<a href="<?php echo Configure::read('Social.instagram');?>" target="_blank" class="btn btn-default btn--instagram">
		<span class="icon fa fa-lg fa-instagram"></span>Siga-nos no Instagram
	</a>
	<?php 
	$link = 'mailto:#';
	if(isset($posts[0]['Blog']['Blogueiros'][0]['User']['email']) && !empty($posts[0]['Blog']['Blogueiros'][0]['User']['email'])){ 
		$link = 'mailto:'.$posts[0]['Blog']['Blogueiros'][0]['User']['email'];
	}	
	?>
	<a href="<?php echo $link; ?>" class="btn btn-default btn--email">
		<span class="icon fa fa-lg fa-envelope-o"></span>Envie um email
	</a>
</div>