<?php 
App::import('Model', 'Vocabulary');
$Vocabulary = new Vocabulary();
$sql = "
SELECT 
Term.id, Term.title, Term.slug 
FROM terms AS Term
LEFT JOIN taxonomies AS Tax ON(Tax.term_id = Term.id) 
WHERE Tax.vocabulary_id = 11
ORDER BY Term.title ASC;
";
$cacheName = 'lista_cinemas';
$cacheConfig = 'nodes_term';
$arrayCinemas = Cache::read($cacheName, $cacheConfig);
if (!$arrayCinemas) {
	$arrayCinemas = $Vocabulary->query($sql);
	Cache::write($cacheName, $arrayCinemas, $cacheConfig);
}

if(!empty($arrayCinemas)){ ?>
	<div class="blog__sessoes accordion" id="accordionSessao">
		<div class="container-fluid blog__sessoes__content">
			<h3 class="headover--label">Cinemas</h3>
			<ul class="content-accordion collapse" id="contentAccordionSessao" aria-expanded="false">
				<?php					
				foreach($arrayCinemas as $cinema){
					echo '<li><a href="/guia_cidade/term/'.trim($cinema['Term']['slug']).'">'.trim($cinema['Term']['title']).'</a></li>';
				}
				?>
			</ul>
		</div>
		<div class="container sessoes--btn-expand text-center">
			<button class="btn btn--expand btn__icon collapsed" data-toggle="collapse" data-target="#contentAccordionSessao" data-parent="#accordionSessao" aria-expanded="false"
			aria-controls="contentAccordionSessao">
				<span class="fa fa-chevron-down"></span>
			</button>
		</div>
	</div>
<?php } ?>