<?php
//Se tem itens no menu
if(!empty($menu['threaded'])){ ?>
    <!-- Menu de navegação -->
    <div id="navbar" class="navbar-second">
        <ul class="nav navbar-nav">
            <?php         
            $i = 0;
            //Percorre os itens do menu
            foreach($menu['threaded'] as $item){ 
                //pr($item);exit();
                $active     = (isset($this->request->params['slug']) && $this->request->params['slug'] === $item['Link']['class']) ? 'active' : 'list-group-item'; 
                $url        = $this->Mswi->linkToUrl($item['Link']['link']);
                ?>
                <li class="<?php echo $active; ?>">
                    <a href="<?php echo $this->Html->url($url) ?>">
                        <?php echo $item['Link']['title'] ?>
                    </a>
                </li>
            <?php 
                $i++;
                //no primeiro nível exibe apenas 4 items
                if($i == 4){
                    break;
                }
            } ?>   
            <!--<li class="list-group-item">
                <a href="/noticia/term/politica">Eleições</a>
            </li>  -->     
        </ul>
        <button type="button" class="megamenu-toggle" data-toggle="collapse" data-target="#megamenu" aria-expanded="false" aria-controls="megamenu">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-plus"></span>
            <span class="fa fa-bars"></span>    
        </button>
    </div>

    <!-- Mega Menu -->
    <div id="megamenu" class="container collapse">
        <div class="row col-lg-11 col-lg-offset-1 col-md-12">
            <?php
            $i = 0;
            //Percorre os itens do menu
            foreach($menu['threaded'] as $item){ 
                $active     = (isset($this->request->params['slug']) && $this->request->params['slug'] === $item['Link']['class']) ? 'active' : 'list-group-item'; 
                $url        = $this->Mswi->linkToUrl($item['Link']['link']);                
                //Ajustes de clearfix
                if($i==4){
                    echo '<div class="clearfix visible-md visible-sm visible-xs"></div>';
                }
                if($i==5){
                    echo '<div class="clearfix visible-lg"></div>';
                }
                if($i==6){
                    echo '<div class="clearfix visible-sm visible-xs"></div>';
                }
                $i++;
                ?>
                <div id="<?php echo $item['Link']['id'] ?>" class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                    <h1><?php echo $item['Link']['title'] ?></h1>
                    <?php
                    //Se tem filhos	
				    if (!empty($item['children'])) { ?>
                        <ul class="list-group">
                            <?php                             
                            //percorre os filhos
                            foreach ($item['children'] as $child) { ?>
                                <li class="list-group-item">
                                    <?php echo $this->Html->link($child['Link']['title'], $this->Mswi->linkToUrl($child['Link']['link'])); ?>
                                </li>
                                <?php                                 
                            } ?>
                        </ul>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>