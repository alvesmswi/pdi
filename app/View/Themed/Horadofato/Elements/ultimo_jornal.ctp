<?php
//se tem algum registro
if(!empty($nodesList)){
    foreach($nodesList as $n){ ?>
    <section class="s-list u-spacer_bottom">
        <header class="o-header">
            <span class="o-header_title o-header_title--small">Edição impressa</span>
        </header>
        <?php
        $imagemUrl      = 'http://placehold.it/195x314';
        $linkpublish    = $n['Node']['path'];
        $target = '';
        //se tem attach
        if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
            $imagemUrlArray = array(
                'plugin'        => 'Multiattach',
                'controller'    => 'Multiattach',
                'action'        => 'displayFile', 
                'admin'         => false,
                'filename'      => $n['Multiattach']['filename'],
                'dimension'     => '300largura'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
        }
        if(isset($n['ImpressoDetail']['link']) && !empty($n['ImpressoDetail']['link'])){
            $linkpublish = $n['ImpressoDetail']['link'];
            $target = '_blank';
        }
        ?>
        <a href="<?php echo $linkpublish ?>" target="<?php echo $target; ?>" class="o-news_content">
            <img src="<?php echo $imagemUrl; ?>" title="<?php echo $n['Node']['title'];?>" alt="<?php echo $n['Node']['title'];?>"> 
        </a>
    </section>
<?php } 
}?>