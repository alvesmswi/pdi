<div class="guide__navbar row">
	<div class="col-lg-12">
			<form method="GET">
				<div class="container-fluid form-group">
					<legend>Filtrar por categoria:</legend>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12">
							<select name="categoria" class="form-control">
								<?php 
								echo '<option value="">Selecione uma categoria</option>';															
								foreach($categoriaEmpresas as $value => $name){
									$selected = '';	
									if(isset($this->params->query['categoria']) && $this->params->query['categoria'] == $value){
										$selected = 'selected';	
									}
									echo '<option '.$selected.' value="'.$value.'">'.$name.'</option>';
								}
								?>
							</select>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<?php 
							$value = '';
							if(isset($this->params->query['termo'])){
								$value = $this->params->query['termo'];	
							}
							echo $this->Form->input(
								'termo',
								array(
									'class' => 'form-control',
									'label' => false,
									'div' => false,
									'placeholder' => 'Digite um termo',
									'name' => 'termo',
									'value' => $value
								)
							);?>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="container">
								<div class="row">
									<div class="col-sm-6">
										<button type="submit" class="btn btn-half">Filtrar</button>
									</div>
									<div class="col-sm-6">
										<a href="<?php echo $this->here?>" class="btn btn-half gray">Limpar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
	</div>
</div>