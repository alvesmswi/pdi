<?php 
//se tem nodes
if (!empty($nodesList)) { ?>
	<!-- Mais lidas do dia  -->
	<div class="col-box col-box--top-daily-news container-fluid">
		<h2 class="col-box__title">
			<span>Mais Lidas</span>
		</h2>
		<ol type="1" class="list-group">
			<?php foreach($nodesList as $n){ ?>
				<li class="list-group-item">
					<article>
						<header class="container">
							<div class="heading" role="heading">
								<a href="<?php echo $n['Node']['path'] ?>">
									<span class="headover">
										<?php 
										echo $this->Mswi->categoryName($n['Node']['terms']);
										?>
									</span>
									<h1>											
										<?php echo $n['Node']['title'] ?>											
									</h1>
								</a>
							</div>
						</header>
					</article>
				</li>
			<?php } ?>					
		</ol>
	</div>
<?php } ?>