<?php 
if(!empty($nodesList)){

	$this->Helpers->load('Medias.MediasImage');

	$imagemUrl = '';
	$imagemBgUrl = '';
	//se tem imagem
	if(isset($nodesList[0]['Multiattach']['filename']) && !empty($nodesList[0]['Multiattach']['filename'])){
		//se é um post
		if($nodesList[0]['Node']['type'] == 'post'){
			if(isset($nodesList[0]['Multiattach']['filename']) && !empty($nodesList[0]['Multiattach']['filename'])){
				$imagemUrl = $this->MediasImage->imagePreset($nodesList[0]['Multiattach']['filename'], '600');	
				$imagemBgUrl = $this->MediasImage->imagePreset($nodesList[0]['Multiattach']['filename'], '1024');
			}				
		//Se é notícia
		}else{
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[0]['Multiattach']['filename'],
				'dimension'		=> '600largura'
			);
			$imagemUrl = $this->Html->url($imagemUrlArray);	
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[0]['Multiattach']['filename'],
				'dimension'		=> '1024largura'
			);
			$imagemBgUrl = $this->Html->url($imagemUrlArray);	
		}		
	} ?>	 

	<div class="row">
		<!-- COBERTURA - Imagem no Fundo -->
		<section class="section section--bg-img coverage-news" style="background-image:url(<?php echo $imagemBgUrl;?>)">
			<div class="section--black-opacity">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-12 col-sm-12">
							<div class="bg-article">
								<a href="<?php echo $nodesList[0]['Node']['path']; ?>">
									<article>
										<header>
											<div class="heading" role="heading">
												<span class="headover">
													<?php 
													//se tem chapey
													if(isset($nodesList[0]['Node']['chapeu']) && !empty($nodesList[0]['Node']['chapeu'])){
														echo $nodesList[0]['Node']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
													}
													?>
												</span>
												<?php
												//se tem imagem
												if(isset($nodesList[0]['Multiattach']['filename']) && !empty($nodesList[0]['Multiattach']['filename'])){ ?>											
													<figure class="bp-media">
														<div class="img-responsive">
															<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[0]['Node']['title']; ?>" />
														</div>
													</figure>
												<?php } ?>
												<h1><?php echo $nodesList[0]['Node']['title']; ?></h1>
											</div>
										</header>
										<div class="clearfix"></div>
									</article>
								</a>
							</div>
						</div>
						<div class="col-box col-lg-4 col-md-12 col-sm-12">
							<div class="row">
								<div class="col-lg-12 col-md-6 col-sm-12">
									<div class="bg-article">
										<?php if(isset($nodesList[1])){ ?>
											<a href="<?php echo $nodesList[1]['Node']['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[1]['Node']['chapeu']) && !empty($nodesList[1]['Node']['chapeu'])){
																	echo $nodesList[1]['Node']['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[1]['Node']['terms']);
																}
																?>
															</span>
															<h1><?php echo $nodesList[1]['Node']['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
									</div>
								</div>
								<div class="col-lg-12 col-md-6 col-sm-12">
									<div class="bg-article">
										<?php if(isset($nodesList[2])){ ?>
											<a href="<?php echo $nodesList[2]['Node']['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[2]['Node']['chapeu']) && !empty($nodesList[2]['Node']['chapeu'])){
																	echo $nodesList[2]['Node']['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[2]['Node']['terms']);
																}
																?>
															</span>
															<?php
															//se é um post
															if($nodesList[2]['Node']['type'] == 'post'){
																if(isset($nodesList[2]['Multiattach']['filename']) && !empty($nodesList[2]['Multiattach']['filename'])){
																	$imagemUrl = $this->MediasImage->imagePreset($nodesList[2]['Multiattach']['filename'], '300');	
																}				
															//Se é notícia
															}else{
																//se tem imagem
																if(isset($nodesList[2]['Multiattach']['filename']) && !empty($nodesList[2]['Multiattach']['filename'])){
																	$imagemUrlArray = array(
																		'plugin'		=> 0,
																		'controller'	=> 0,
																		'action'		=> 'displayFile', 
																		'admin'			=> false,
																		'filename'		=> $nodesList[2]['Multiattach']['filename'],
																		'dimension'		=> '300largura'
																	);
																	$imagemUrl = $this->Html->url($imagemUrlArray);	
																}
															}
															//se tem imagem
															if(!empty($imagemUrl)){
																?>
																<figure class="bp-media">
																	<div class="img-responsive">
																		<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[2]['Node']['title']; ?>" />
																	</div>
																</figure>
															<?php } ?>
															<h1><?php echo $nodesList[2]['Node']['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- ANUNCIO -->
	<?php
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('publicidade_top')){ ?>
		<div class="container">
			<div class="ads ads--super-banner hidden-sm">
				<span class="headover-ads rows--x2">Publicidade</span>
				<div class="content w728">
					<?php echo $this->Regions->blocks('publicidade_top');?>
				</div>
			</div>
		</div>			
	<?php }	?>
    <?php
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('publicidade_mobile_top')){ ?>
        <div class="container">
            <div class="ads ads--rectangle-medium--half visible-sm">
                <span class="headover-ads">Publicidade</span>
                <div class="content">
                    <?php echo $this->Regions->blocks('publicidade_mobile_top');?>
                </div>
            </div>		
        </div>	
    <?php }	?>
	
<?php } ?>