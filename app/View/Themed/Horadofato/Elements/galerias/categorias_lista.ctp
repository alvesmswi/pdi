<?php	
//se tem algum registro
if(!empty($nodesList)){
	$this->Helpers->load('Medias.MediasImage');
	?>
	<h1 class="section__title">
		<?php 
		if(Configure::read('Home.home_lista_title')){
			echo Configure::read('Home.home_lista_title');
		}else{
		?>
			<a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[0]['Node']['terms']);?>">
				<?php echo $this->Mswi->categoryName($nodesList[0]['Node']['terms']);?>
			</a>
		<?php } ?>
	</h1>
	<div class="container">	
		<div class="row">			
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($nodesList as $n){
					//pr($n);
					$imagemUrl = '';
					$class = '';

					//se é um pos
					if(isset($n['Node']['type']) && $n['Node']['type'] == 'post'){
						//se tem imagem
						if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
							$imagemUrl = $this->MediasImage->imagePreset($n['Multiattach']['filename'], '300');
						}						
					//se é notícia
					}else{
						//se tem imagem
						if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 0,
								'controller'	=> 0,
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> '300largura'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							$class = 'row';
						}
					}
					
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">
								<a href="<?php echo $n['Node']['path'];?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n['Node']['chapeu']) && !empty($n['Node']['chapeu'])){
														echo $n['Node']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
													?>
												</span>
											</div>
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Node']['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
												if(isset($n['Node']['chapeu']) && !empty($n['Node']['chapeu'])){
													echo $n['Node']['chapeu'];
												}else{
													echo $this->Mswi->categoryName($n['Node']['terms']);
												}
												?>
											</span>
											<h1><?php echo $n['Node']['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		</div>
	</div>
<?php } ?>