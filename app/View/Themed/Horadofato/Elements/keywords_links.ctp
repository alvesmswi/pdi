<?php 
//se tem keywords
if(isset($node['NoticiumDetail']['keywords']) && !empty($node['NoticiumDetail']['keywords'])){
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $node['NoticiumDetail']['keywords']
		),
		null,
		array('inline'=>false)
	);
	//pega cada palavras
	$arrayKeywords = explode(',', $node['NoticiumDetail']['keywords']);
	?>
	<div class="container">
		<h4 class="headover--label">Assuntos</h4>
		<ul class="keywords list-group " itemscope>
			<?php foreach($arrayKeywords as $keywords){ 
				//pelo Google
				$link = '/search?q='.urlencode(trim($keywords));
				//Pelo CMS
				$link = '/nodes/nodes/keywords/'.urlencode(trim($keywords));
				?>
				<li class="list-group-item">
					<a href="<?php echo $link; ?>" class="hashtag" itemprop="keyword">
						<?php echo $keywords; ?>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>