<?php 
echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	'Assinante', 
	$this->here
);
?>

<div class="section container feed-lastnews">
    <div class="row">        
        <div class="col-lg-6 col-md-12 col-sm-12">
            <span class="headover">Já tenho conta</span>
            <?php 
            echo $this->Form->create('User', array('url' => array('plugin' => 'users', 'controller' => 'users', 'action' => 'login')));
            //se tem redirecionamento
            if(isset($this->params->query['redirect']) && !empty($this->params->query['redirect'])){
                $redirect = $this->params->query['redirect'];
                echo $this->Form->hidden('redirect', array('value'=>$redirect));
            }
            ?>
            <div class="form-group">
                <?php 
                echo $this->Form->input(Configure::read('User.campo_usuario'), array('label' => Configure::read('User.label_usuario'), 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('password', array('label' =>'Senha', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('remember', array('label' =>'Lembrar a senha', 'type'=>'checkbox', 'div'=>'form-group', 'style'=>'margin-right: 5px;'));
                ?>
            </div>
            <div><button type="submit" class="btn btn-default">Entrar</button></div>
            <div><a href="/users/users/forgot" class="btn-link">Recuperar a senha</a></div>
            
            <?php echo $this->Form->end(); ?>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12" style="border-left:1px solid #eee;">
            <span class="headover">Criar nova conta</span>
            <?php 
            echo $this->Form->create('Paywall', array('url' => array('plugin' => 'paywall', 'controller' => 'paywall', 'action' => 'cadastro')));
            //se tem redirecionamento
            if(isset($this->params->query['redirect']) && !empty($this->params->query['redirect'])){
                $redirect = $this->params->query['redirect'];
                echo $this->Form->hidden('redirect', array('value'=>$redirect));
            }
            ?>
            <div class="form-group">
                <?php 
                echo $this->Form->input('name', array('label'=>'Seu nome completo', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required')); 
                echo $this->Form->input('telefone', array('label'=>'Celular', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('password', array('label'=>'Senha', 'type' => 'password', 'class'=>'form-control', 'value' => '', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('verify_password', array('label'=>'Confirme a Senha', 'type' => 'password', 'value' => '', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                ?>
            </div>           
            <button type="submit" class="btn btn-default">Enviar Cadastro</button>
            <?php echo $this->Form->end();?>   
        </div>

    </div>
</div>