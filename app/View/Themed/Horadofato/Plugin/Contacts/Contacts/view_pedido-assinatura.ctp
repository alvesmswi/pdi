<header class="o-header"><span class="o-header_title">Formulário</span></header>
<div class="o-login_content">
	<h1 class="o-news_title c-article_title o-login_title">
   		<?php echo $contact['Contact']['title']; ?>
   	</h1>
   	<?php 
	echo $this->Form->create('Message', array(
		'url' => array(
			'plugin' => 'contacts',
			'controller' => 'contacts',
			'action' => 'view',
			$contact['Contact']['alias'],
		),
	));

	$this->Form->inputDefaults(array(
		'label' => false,
		'class' => 'form-control',
		'div' => array('class' => 'form-group'),
	));
   	?>
    <div class="row">
       <div class="col-xs-3 col-sm-4">
         	<?php 
         	//Exibe o último jornal
         	echo $this->Regions->block('ultimo-jornal');?>
         </div>
		<div class="col-xs-9 col-sm-8 col-md-6">	
     		<?php echo $contact['Contact']['body']; ?>
     	
	        <div class="form-group">
	        	<?php echo $this->Form->input('Message.name', array('label'=>'Nome de Usuário', 'class'=>'form-control')); ?>
	        </div>
	       	<div class="form-group">
	        	<?php echo $this->Form->input('Message.title', array('label'=>'Telefone', 'class'=>'form-control')); ?>
	        </div>
	        <div class="form-group">
	        	<?php echo $this->Form->input('Message.email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control')); ?>
	        </div>
	         <div class="form-group">
	        	<?php echo $this->Form->input('Message.body', array('label'=>'Informe os seus dados', 'type'=>'textarea','class'=>'form-control')); ?>
	        </div>
	        <?php 
	        if ($contact['Contact']['message_captcha']):
				echo $this->Recaptcha->display_form();
			endif; 
	        ?>
	        <div class="text-right"><button type="submit" class="btn btn-lg btn-primary">Enviar Solicitação</button></div>
		</div>
	</div>
   	<?php echo $this->Form->end();?>   
</div>
<br />
