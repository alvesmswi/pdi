<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	'Editais', 
	'/edital'
); 
$this->Html->addCrumb(
	$node['Node']['title'], 
	$this->here
); 
?>
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<h1 itemprop="name">
					<?php echo $node['Node']['title']; ?>
				</h1>
				<?php 
				//se tem chapeu
				if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){ ?>
					<summary><?php echo $node['Node']['excerpt']; ?></summary>
				<?php } ?>			
			</div>

		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">
	
					<?php
					//pr($node['Multiattach']);
					
					if (isset($node['Multiattach']) && !empty($node['Multiattach'])) {
						
						foreach ($node['Multiattach'] as $key => $n){
							
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> '600x276'
							);
							//se é uma imagem
							if ($n['Multiattach']['mime'] != 'application/pdf') {	
								//miniatura							
								$minUrl = $this->Html->url($imagemUrlArray);
								//grande
								$imagemUrlArray['dimension'] = 'normal';
								$imagemUrl = $this->Html->url($imagemUrlArray);
								?>
									<figure>
										<div class="img-responsive">									
											<a href="<?php echo $imagemUrl; ?>" data-featherlight="image" >
												<img itemprop="image" src="<?php echo $minUrl; ?>" alt="<?php echo $n['Multiattach']['comment'] ?>">
											</a>
										</div>
									</figure>
								<?php
							//PDFs
							} else{
								$imagemUrlArray['dimension'] = 'normal';
								$link = $this->Html->url($imagemUrlArray); ?>
								<div align="center">
									<a target="_blank" href="<?php echo $link; ?>">Baixar <?php echo $n['Multiattach']['filename']; ?></a>
								</a>
							<?php
							}
						}
					}
					//Corpo da noticia
					echo strip_tags($node['Node']['body'],'<img><a>');
					?>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php echo $this->element('keywords_links');?>		
			<?php echo $this->element('facebook_comentarios');?>
		</footer>
	</article>
</section>