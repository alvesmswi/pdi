<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	$node['Node']['title'], 
	$this->here
);
//se não é home
if($this->here != '/'){
?>
	<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
		<article id="article-content" class="article__content" itemprop="content">
			<header class="container" itemprop="headline">
				<div class="heading" role="heading">
					<h1 itemprop="name">
						<?php echo $node['Node']['title']; ?>
					</h1>
					<?php					 
					//se tem chapeu
					if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){ ?>
						<summary><?php echo $node['Node']['excerpt']; ?></summary>
					<?php } ?>			
				</div>

			</header>
			<div>
				<span>Endereço: <?= $node['EmpresaDetail']['endereco'] ?></span></br>
				<span>Número: <?= $node['EmpresaDetail']['numero'] ?></span></br>
				<span>Bairro: <?= $node['EmpresaDetail']['bairro'] ?></span></br>
				<span>Cidade: <?= $node['EmpresaDetail']['cidade'] ?></span></br>
				<span>CEP: <?= $node['EmpresaDetail']['cep'] ?></span></br>
				<span>Fone: <?= $node['EmpresaDetail']['fone1'] ?></span></br>
				<span>E-mail: <?= $node['EmpresaDetail']['email'] ?></span></br>
				<span>Site: <a href= <?= $node['EmpresaDetail']['site']?>> <?= $node['EmpresaDetail']['site'] ?> </span></a></br>
				<span>Palavras-chave: <?= $node['EmpresaDetail']['keywords'] ?></span></br><hr>
			</div>
			<div class="container">
				<div class="row">
					<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

						<?php echo $this->element('share');?>

						<div class="">
							<div>
								<span>Endereço: <?= $node['EmpresaDetail']['endereco'] ?></span></br>
								<span>Número: <?= $node['EmpresaDetail']['numero'] ?></span></br>
								<span>Bairro: <?= $node['EmpresaDetail']['bairro'] ?></span></br>
								<span>Cidade: <?= $node['EmpresaDetail']['cidade'] ?></span></br>
								<span>CEP: <?= $node['EmpresaDetail']['cep'] ?></span></br>
								<span>Fone: <?= $node['EmpresaDetail']['fone1'] ?></span></br>
								<span>E-mail: <?= $node['EmpresaDetail']['email'] ?></span></br>
								<span>Site: <a href= <?= $node['EmpresaDetail']['site']?>> <?= $node['EmpresaDetail']['site'] ?> </span></a></br>
								<span>Palavras-chave: <?= $node['EmpresaDetail']['keywords'] ?></span></br><hr>
							</div>
							<?php 
							//Corpo da noticia
							echo $node['Node']['body'];
							?>
						</div>
						
						
					</div>
				</div>
			</div>
			<!-- Footer do artigo -->
			<footer>
				<?php echo $this->element('keywords_links');?>		
				<?php echo $this->element('facebook_comentarios');?>
			</footer>
		</article>
	</section>
<?php } ?>