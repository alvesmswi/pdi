<?php
echo $this->element('node_metas');
//Configura o breadcrumb
$this->Html->addCrumb($title_for_layout, $this->here);
?>

<section id="list-news" class="section list-news col-lg-8 col-md-12 col-sm-12">

	<div class="container">
		<?php		
		//se tem algum registro
		if(!empty($nodes)){
			?>
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($nodes as $n){
					$imagemUrl = '';
					$class = '';
					//se tem imagem
					if(isset($n['Multiattach'][0]['Multiattach']['filename']) && !empty($n['Multiattach'][0]['Multiattach']['filename'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
							'dimension'		=> '300largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$class = 'row';
					}
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">
								<a href="<?php echo $n['Node']['path'];?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
														echo $n['NoticiumDetail']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
													?>
												</span>
											</div>
											<div class="row">										
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<figure class="bp-media">
															<div class="img-responsive">
																<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
															</div>
														</figure>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Node']['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
												if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
													echo $n['NoticiumDetail']['chapeu'];
												}else{
													echo $this->Mswi->categoryName($n['Node']['terms']);
												}
												?>
											</span>
											<h1><?php echo $n['Node']['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		<?php }else{ 
			echo 'Nenhum registro encontrado';
		} ?>	
		<?php 
		//Insere o paginado
		echo $this->element('paginator');
		?>
	</div>
</section>