<?php
echo $this->element('node_metas');
$titulo = 'Guia de Empresas';
//Configura o breadcrumb
$this->Html->addCrumb($titulo, $this->here);
?>

<section id="list-news" class="section list-news col-lg-8 col-md-12 col-sm-12">

	<div class="container">
		<div class="guide__navbar row">
			<div class="col-lg-12">
			<?php 
			echo $this->element('filtro_empresas')?>
			</div>
		</div>
		<?php		
		//se tem algum registro
		if(!empty($nodes)){
			?>
			<ul class="list-group companies__addr">
				<?php foreach($nodes as $node) { ?>
					<li class="list-group-item" itemscope itemtype="http://schema.org/LocalBusiness">
						<div class="container-fluid">
							<a href="/empresa/<?php echo $node['Node']['slug'];?>" itemprop="name" class="name">
								<h3><?php echo $node['Node']['title'];?></h3>
							</a>
							<a href="tel:4132634277" class="telephone"> Tel.:
								<span itemprop="telephone">
									<?php echo $node['EmpresaDetail']['fone1'];?>
								</span>
							</a>
							<div class="clearfix"></div>
							<address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<span itemprop="streetAddress"><?php echo $node['EmpresaDetail']['endereco'];?></span>,
								<span itemprop="addressLocality"><?php echo $node['EmpresaDetail']['bairro'];?></span>,
								<span itemprop="addressRegion"><?php echo $node['EmpresaDetail']['cidade'];?></span>
							</address>
						</div>
				</li>      
				<?php } ?>         
			</ul>			
		<?php }else{ 
			echo 'Nenhum registro encontrado';
		} ?>	
		<?php 
		//Insere o paginado
		echo $this->element('paginator');
		?>
	</div>
</section>