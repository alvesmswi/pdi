<section id="list-news" class="section list-news col-lg-8 col-md-12 col-sm-12">
<?php
echo $this->element('node_metas');
//se listou um conteúdo
if(isset($this->params->named['slug']) && !empty($this->params->named['slug'])) {
	$titulo = $this->Mswi->slugName($this->params->named['slug']); 
}else{
	$titulo = 'Resultado da pesquisa';
}
//Configura o breadcrumb
$this->Html->addCrumb($titulo, $this->here);
?>

<div class="container">
	<?php		
	//se tem algum registro
	if(!empty($nodes)){
		?>
		<ul class="list-group">
			<?php
			//percorre os nodes
			foreach($nodes as $n){
				$imagemUrl = '';
				$class = '';
				//se tem imagem
				if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
					$imagemUrlArray = array(
						'plugin'		=> 'Multiattach',
						'controller'	=> 'Multiattach',
						'action'		=> 'displayFile', 
						'admin'			=> false,
						'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
						'dimension'		=> '300largura'
					);
					$imagemUrl = $this->Html->url($imagemUrlArray);
					$class = 'row';
				}
				?>
				<li class="list-group-item">
					<?php if (isset($n['Taxonomy'][0]['parent_id']) && !empty($n['Taxonomy'][0]['parent_id']) && $n['Taxonomy'][0]['parent_id'] == Configure::read('colunas_parent_id')): ?>
						<article>
							<header class="<?php echo $class; ?>">
								<div class="heading" role="heading">
									<?php 
									//se tem imagem
									if (!empty($imagemUrl)) { ?>
										<a href="/coluna/<?php echo $this->Mswi->categorySlug($n['Node']['terms']) ?>">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
														echo $n['NoticiumDetail']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
													?>
												</span>
											</div>
										</a>
										<a href="<?php echo $n['Node']['path']; ?>">
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Node']['title'];?></h1>
													</div>
												</div>										
											</div>
										</a>
									<?php } else { ?>
										<a href="/coluna/<?php echo $this->Mswi->categorySlug($n['Node']['terms']) ?>">
											<span class="headover">
												<?php 
													if (isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])) {
														echo $n['NoticiumDetail']['chapeu'];
													} else {
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
												?>
											</span>
										</a>
										<a href="<?php echo $n['Node']['path']; ?>">
											<h1><?php echo $n['Node']['title']; ?></h1>
										</a>
									<?php } ?>	
								</div>
							</header>
						</article>
					<?php else: ?>
						<article>
							<header class="<?php echo $class; ?>">
								<a href="<?php echo $n['Node']['path'];?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
														echo $n['NoticiumDetail']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n['Node']['terms']);
													}
													?>
												</span>
											</div>
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Node']['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
												if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
													echo $n['NoticiumDetail']['chapeu'];
												}else{
													echo $this->Mswi->categoryName($n['Node']['terms']);
												}
												?>
											</span>
											<h1><?php echo $n['Node']['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					<?php endif; ?>
				</li>	
			<?php } ?>
		</ul>
	<?php }else{ 
		echo 'Nenhum registro encontrado';
	} ?>	
	<?php 
	//Insere o paginado
	echo $this->element('paginator');
	?>
</div>
</section>