<?php
//se é para exibir com PAGEFLIP
if(isset($pageflip) && $pageflip){

    echo $this->element('node_metas');
    ?>
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/pdf.css">
    <header class="o-header">
        <a href="/" class="o-header_title o-header_title--large">
            <?php echo $node['Node']['title']; ?>
        </a>
    </header>
    <?php 
    //Corpo da noticia
    echo $node['Node']['body'];
    //pr($node);
    //Procura o anexo
    if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
        //percorre os anexos
        foreach($node['Multiattach'] as $anexo){
            
            //verifica o tipo de anexo
            $arrayMime = explode('pdf', $anexo['Multiattach']['mime']);
            //se tem PDF anexado
            if(count($arrayMime) > 1){
                $linkUrlArray = array(
                                    'plugin'        => 'Multiattach',
                                    'controller'    => 'Multiattach',
                                    'action'        => 'displayFile', 
                                    'admin'         => false,
                                    'filename'      => $anexo['Multiattach']['filename'],
                                    'dimension'     => 'normal'
                            );
                        $pdfPath = $this->Html->url($linkUrlArray);
            }
            
            //verifica o tipo de anexo
            $arrayMime = explode('image', $anexo['Multiattach']['mime']);
            //se tem IMG anexado
            if(count($arrayMime) > 1){
                $linkUrlArray = array(
                                    'plugin'        => 'Multiattach',
                                    'controller'    => 'Multiattach',
                                    'action'        => 'displayFile', 
                                    'admin'         => false,
                                    'filename'      => $anexo['Multiattach']['filename'],
                                    'dimension'     => 'normal'
                            );
                        $imgPath = $this->Html->url($linkUrlArray);
                        $this->Html->meta(
                                array('property' => 'og:image', 'content' => $this->Html->url($linkUrlArray, true)),
                                null,
                                array('inline' => false)
                            );
            }
        }
    }

    //se NÃO encontrou PDF
    if(!isset($pdfPath) && isset($imgPath)){
        
        $link = false;
        //se tem link
        if(isset($node['ImpressoDetail']['link']) && !empty($node['ImpressoDetail']['link'])){
            $link = $node['ImpressoDetail']['link'];
        }
        //exibe a imagem
        echo $this->Html->image(
            $imgPath,
            array(
                'url' => $link
            )
        );
        
    //Se encontrou PDf	
    }else if(isset($pdfPath)){	
    ?>
        <div id="PageFlip" data-pdf="<?php echo $pdfPath; ?>">
                <div id="PFButtons">
                        <div id="PFButtonsContainer">
                                <button type="button" class="PFButton" id="FirstPage"><span class="sr-only">Capa</span><i class="PFicon PFicon_first_page"></i></button>
                                    <button type="button" class="PFButton" id="ZoomPlus"><span class="sr-only">Mais Zoom</span><i class="PFicon PFicon_zoom_in"></i></button>
                                    <button type="button" class="PFButton" id="ZoomMinus"><span class="sr-only">Menos Zoom</span><i class="PFicon PFicon_zoom_out"></i></button>
                                    <button type="button" class="PFButton" id="ZoomRestore"><span class="sr-only">Restaurar Zoom</span><i class="PFicon PFicon_youtube_searched_for"></i></button>
                                    <button type="button" class="PFButton" id="PrevPage"><span class="sr-only">Página Anterior</span><i class="PFicon PFicon_keyboard_arrow_left"></i></button>
                                    <button type="button" class="PFButton" id="NextPage"><span class="sr-only">Próxima Página</span><i class="PFicon PFicon_keyboard_arrow_right"></i></button>
                                    <button type="button" class="PFButton" id="FullScreen"><span class="sr-only">Tela Cheia</span><i class="PFicon PFicon_fullscreen"></i></button>
                                    <?php 
                                    //se pode você pode baixar
                                    if(isset($pageflip_download)){  
                                    ?>
                                        <a href="<?php echo $pdfPath; ?>" target="_blank" class="PFButton" id="Download" pageflip_download="<?php echo $pdfPath; ?>">
                                                <span class="sr-only">Download</span>
                                                <i class="PFicon PFicon_download"></i>
                                        </a>
                                    <?php } ?>
                            </div>
                    </div>
                    <div id="PFFlip">
                        <div id="PFContainer">
                                <div id="PFWrapper">
                                        <div id="PFBook">
                        
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <div id="PFLoading">
                        <div class="PFLoader">
                                <div class="loader">Carregando...</div>
                        </div>
                    </div>
            </div>
        
    <?php }

//exibição COMUM
}else{ ?>

    <section class="s-article">
    <article class="c-article">
        <header class="o-header">
            <span class="o-header_title o-header_title--large">
                <?php echo $this->Mswi->categoryName($node['Node']['terms']); ?>
            </span>
        </header>
           <header class="o-news_header c-article_header">
            <h1 class="o-news_title c-article_title"><?php echo $node['Node']['title']; ?></h1>
            <?php if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])): ?>
                <div class="o-news_summary c-article_summary">
                    <?= $node['Node']['excerpt']; ?>
                </div>
            <?php endif ?>
              <div class="c-article_byline">
                 <div class="c-article_sharebar">
                       <div class="addthis_inline_share_toolbox"></div>
                 </div>
                 <div class="c-article_pubdate">
                     <time datetime="<?php echo $node['Node']['publish_start']; ?>">
                         <?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
                         <?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
                     </time> 
                     <?php 
                     //se foi atualizado
                     if($node['Node']['publish_start'] > $node['Node']['updated']){ ?>
                         – Atualizado em 
                         <time datetime="<?php echo $node['Node']['updated']; ?>">
                             <?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
                             <?php echo date('H:i', strtotime($node['Node']['updated'])); ?>		
                         </time>
                     <?php } 
                     //se tem o usuario
                     if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
                         echo "- por {$node['NoticiumDetail']['jornalista']}";
                     }else{
                         echo "- por {$node['User']['name']}";
                     } ?>
                 </div>
              </div>
       </header>
       
       <div class="row">
          
          <div class="col-md-9 col-md-push-3">
             <div class="c-article_content">
                 <?php 
                 
                //Corpo da noticia
                echo $node['Node']['body'];
                 
                //Galeria de imagem
                if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){                         
                    foreach($node['Multiattach'] as $foto){ 
                        if (strpos($foto['Multiattach']['mime'], 'image/') !== false) { 
                            $imagemUrlArray = array(
                                'plugin'        => 'Multiattach',
                                'controller'    => 'Multiattach',
                                'action'        => 'displayFile', 
                                'admin'         => false,
                                'filename'      => $foto['Multiattach']['filename'],
                                'dimension'     => 'normal'
                            );
                            $imagemUrl = $this->Html->url($imagemUrlArray);
                        }
                        if (strpos($foto['Multiattach']['mime'], '/pdf') !== false) {
                            $linkUrlArray = array(
                                'plugin'        => 'Multiattach',
                                'controller'    => 'Multiattach',
                                'action'        => 'displayFile', 
                                'admin'         => false,
                                'filename'      => $foto['Multiattach']['filename'],
                                'dimension'     => 'normal',
                            );
                            //se é para fazer download
                            if(isset($download) && $download){
                                $linkDownload = $this->Html->url($linkUrlArray);
                            }
                        }
                    } ?>
                    
                <?php } elseif (isset($node['Multiattach']) && count($node['Multiattach']) == 1) {
                    if (strpos($node['Multiattach'][0]['Multiattach']['mime'], 'image/') !== false) { 
                        $imagemUrlArray = array(
                            'plugin'        => 'Multiattach',
                            'controller'    => 'Multiattach',
                            'action'        => 'displayFile', 
                            'admin'         => false,
                            'filename'      => $node['Multiattach'][0]['Multiattach']['filename'],
                            'dimension'     => 'normal'
                        );
                        $imagemUrl = $this->Html->url($imagemUrlArray);
                    }
                }

                //Se tem download
                if(isset($linkDownload)){
                    echo '<a href="'.$linkDownload.'" target="_blank" class="btn btn-default">Baixar o PDF</a>';
                }
                ?>
                <img src="<?php echo $imagemUrl; ?>">
             </div>
          </div>
          <div class="col-md-3 col-md-pull-9">
            <?php
            if ($this->Regions->blocks('publicidade-interna-noticia')) {
              echo $this->Regions->blocks('publicidade-interna-noticia');
            }
            ?>
          </div>
       </div>
    </article>
 </section>

<?php } ?>