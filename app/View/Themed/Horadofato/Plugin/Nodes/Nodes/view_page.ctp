<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

//configura os breadscrumbs
/*$this->Html->addCrumb(
	$node['Node']['title'], 
	$this->here
); */
//se não é home
if($this->here != '/'){
?>
	<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
		<article id="article-content" class="article__content" itemprop="content">
			<header class="container" itemprop="headline">
				<div class="heading" role="heading">
					<h1 itemprop="name">
						<?php echo $node['Node']['title']; ?>
					</h1>
					<?php 
					//se tem chapeu
					if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){ ?>
						<summary><?php echo $node['Node']['excerpt']; ?></summary>
					<?php } ?>			
				</div>

				<?php 
				//se tem imagem
				if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
					$imagemUrlArray = array(
						'plugin'		=> 'Multiattach',
						'controller'	=> 'Multiattach',
						'action'		=> 'displayFile', 
						'admin'			=> false,
						'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
						'dimension'		=> 'normal'
					);
					$imagemUrl = $this->Html->url($imagemUrlArray);
					
					//prepara a imagem para o OG
					$this->Html->meta(
						array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
						null,
						array('inline' => false)
					);
					
					if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
						$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
					}
					if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
						$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
					}?>

					<figure class="bp-media legended">
						<div class="img-responsive">
							<img itemprop="image" src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" alt="<?php echo $node['Node']['title']; ?>" />
						</div>
						<figcaption class="c-article_figcaption">
							<?php if(isset($figCaption)){
								echo $figCaption; 
							}
							if(isset($figAutor)){
								echo ' (Foto: '.$figAutor.')'; 
							} ?>
						</figcaption>
					</figure>

				<?php }	?>

			</header>
			<div class="container">
				<div class="row">
					<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

						<?php echo $this->element('share');?>

						<div id="corpo">
							<?php 
							//Corpo da noticia
							echo $node['Node']['body'];
							?>
							<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
							<script>
								$( document ).ready(function() {
									$('#corpo img').unwrap('p').wrap('<figure>').addClass('img-responsive');
								});								
							</script>
						</div>
						
						<?php 
						if (isset($node['Multiattach']) && count($node['Multiattach']) > 1){  
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
								'dimension'		=> 'normal'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>

							<!-- GALERIA DE IMAGENS - Slider-->
							<div class="container-fluid gallery">

								<!-- CAROUSEL -->
								<div class="gallery__visualization">
									<div class="fade in" id="viewbtn0">
										<figure class="bp-media">
											<div class="img-responsive">
												<img itemprop="image" src="<?php echo $imagemUrl;?>" alt="<?php echo $node['Multiattach'][0]['Multiattach']['filename']; ?>">
											</div>
										</figure>
									</div>
								</div>

								<!-- SLIDER -->
								<div class="gallery__nav-thumbs">
									<?php
									//percorre para montar as miniaturas
									foreach($node['Multiattach'] as $key => $min){
										//se não é um PDF
										if ($min['Multiattach']['mime'] != 'application/pdf') {
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $min['Multiattach']['filename'],
												'dimension'		=> 'normal'
											);
											$imgUrl = $this->Html->url($imagemUrlArray);
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $min['Multiattach']['filename'],
												'dimension'		=> '300largura'
											);
											$minUrl = $this->Html->url($imagemUrlArray);
										} 
										?>
										<div id="btn<?php echo $key;?>" class="col-lg-4 col-md-4 col-sm-6"  data-load-img ="<?php echo $imgUrl; ?>" data-alt-img = "<?php echo $min['Multiattach']['comment']; ?>">
											<figure>
												<div class="img-responsive">
													<img itemprop="image" src="<?php echo $minUrl; ?>" alt="<?php echo $min['Multiattach']['comment']; ?>">
												</div>
											</figure>
										</div>
									<?php } ?>
									
								</div>
							</div>
						<?php } ?>
						
					</div>
				</div>
			</div>
			<!-- Footer do artigo -->
			<footer>
				<?php echo $this->element('keywords_links');?>		
				<?php echo $this->element('facebook_comentarios');?>
			</footer>
		</article>
	</section>
<?php } ?>