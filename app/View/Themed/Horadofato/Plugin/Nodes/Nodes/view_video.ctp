<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

//configura os breadscrumbs
/*$this->Html->addCrumb(
	$node['Node']['title'], 
	$this->here
); */
?>
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<h1 itemprop="name">
					<?php echo $node['Node']['title']; ?>
				</h1>
				<?php 
				//se tem chapeu
				if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){ ?>
					<summary><?php echo $node['Node']['excerpt']; ?></summary>
				<?php } ?>			
			</div>

			<?php 
			//se tem imagem
			if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
					'dimension'		=> 'normal'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				
				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
					null,
					array('inline' => false)
				);
				
				if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
					$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
				}
				if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
					$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
				}?>

				<figure class="bp-media legended">
					<div class="img-responsive">
						<img itemprop="image" src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" alt="<?php echo $node['Node']['title']; ?>" />
					</div>
					<figcaption class="c-article_figcaption">
						<?php if(isset($figCaption)){
							echo $figCaption; 
						}
						if(isset($figAutor)){
							echo ' (Foto: '.$figAutor.')'; 
						} ?>
					</figcaption>
				</figure>

			<?php }	?>

		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

					<?php echo $this->element('share');?>

					<div class="">
						<?php 
						//Corpo da noticia
						echo $node['Node']['body'];
						?>
					</div>
					
					<!-- CAROUSEl -->
					<?php if (isset($node['Multiattach']) && count($node['Multiattach']) > 1): ?>
						<div class="container-fluid">
							<div id="bp-carousel" class="carousel slide" data-ride="carousel">

								<div class="carousel-inner ">
									<?php foreach ($node['Multiattach'] as $key => $foto):
										if ($foto['Multiattach']['mime'] != 'application/pdf') {
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $foto['Multiattach']['filename'],
												'dimension'		=> '600x276'
											);

											$imagemUrl = $this->Html->url($imagemUrlArray);
										} 
										$active_class_gallery = ($key == 0) ? 'active' : null;
										?>
										<div class="item <?php echo $active_class_gallery; ?>">
											<figure class="">
												<div class="img-responsive">
													<img itemprop="image" src="<?php echo $imagemUrl; ?>" alt="<?php echo $foto['Multiattach']['comment'] ?>">
												</div>
											</figure>
										</div>
									<?php endforeach; ?>
								</div>
							
								<!-- Left and right controls -->
								<a class="left carousel-control" href="#bp-carousel" data-slide="prev">
									<span class="fa fa-chevron-left"></span>
									<span class="sr-only">Próximo</span>
								</a>
								<a class="right carousel-control" href="#bp-carousel" data-slide="next">
									<span class="fa fa-chevron-right"></span>
									<span class="sr-only">Anterior</span>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php echo $this->element('keywords_links');?>		
			<?php echo $this->element('facebook_comentarios');?>
		</footer>
	</article>
</section>