<section id="all-blogs" class="section list-all-blogs col-lg-8 col-md-12 col-sm-12">
	<?php
	//Configura o breadcrumb
	$this->Html->addCrumb('Blogs', $this->here);

  echo $this->requestAction('/blogs/ultimos_posts',array('element'=>'ultimos_posts_blogs'));
  ?>
  <h1 class="section__title purple">Todos os Blogs</h1>
		<?php		
		//se tem algum registro
		if(!empty($blogs)){ ?>
        <!-- blog box -->
        <div class="row">

          <?php foreach($blogs as $n){ ?>
                <div class="col-md-6">
                  <div class="col-box col-blog__stories container">
                    <article class="blog-box">
                      <header class="blog-box__header">
                        <a href="/blog/<?php echo $n['Blog']['slug'] ?>">
                          <div class="blog-box__avatar">
                            <figure class="bp-avatar">
                              <div class="img-responsive">
                                <?php 
                                if(isset($n['Blog']['image']) && !empty($n['Blog']['image'])){ 
                                  $imagemBlog = $n['Blog']['image'];
                                }else{
                                  $imagemBlog = '/theme/'.Configure::read('Site.theme').'/images/sem-foto.jpg';
                                } 
                                ?>
                                <img src="<?php echo $imagemBlog; ?>" alt="<?php echo $n['Blog']['title']; ?>" />
                              </div>
                            </figure>
                          </div>
                          <div class="blog-box__heading-txt">
                            <h3 class="blog-box__name"><?php echo $n['Blog']['title']; ?></h3>
                          </div>
                        </a>
                      </header>
                      <?php if (!empty($n['Post'])): ?>
                        <div class="blog-box__content row">
                          <a href="/blog/<?php echo $n['Blog']['slug'] ?>/post/<?php echo $n['Post']['slug'] ?>">
                            <?php 
                            $classTxt = 'col-sm-12';
                            ?>
                            <div class="blog-box__content-txt <?php echo $classTxt;?>">
                              <h1><?php echo $n['Post']['title']; ?></h1>
                            </div>
                          </a>
                        </div>
                      <?php endif; ?>
                    </article>	
                  </div>
                </div>             	
          <?php } ?>
        </div>
        
		<?php }else{ 
			echo 'Nenhum registro encontrado';
		} ?>	
		<?php 
		//Insere o paginado
		echo $this->element('paginator');
		?>
</section>