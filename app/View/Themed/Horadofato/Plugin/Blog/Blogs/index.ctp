<?php
//Configura o breadcrumb
$this->Html->addCrumb('Blogs', '/blogs');
$this->Html->addCrumb($posts[0]['Blog']['title'], $this->here);
?>

<section id="list-news" class="section list-news col-lg-8 col-md-12 col-sm-12">

	<div class="container">
		<?php		
		//se tem algum registro
		if(!empty($posts)){
			?>
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($posts as $n){
					$class = '';
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">															
								<div class="heading" role="heading">																					
									<span class="headover">
										<?php 
										echo date('d/m/Y', strtotime($n['Post']['publish_start']));
										?>
									</span>
									<a href="/blog/<?php echo $n['Blog']['slug'] ?>/post/<?php echo $n['Post']['slug'] ?>">	
										<h1><?php echo $n['Post']['title'];?></h1>

										<?php 
										//se tem imagem
										if(isset($n['Images']) && !empty($n['Images'])){
											if(!empty($n['Images'][0]['legenda'])){
												$figCaption = $n['Images'][0]['legenda'];
											}
											if(!empty($n['Images'][0]['credito'])){
												$figAutor = $n['Images'][0]['credito'];
											}
										?>
											<figure class="legended">
												<div class="img-responsive">
													<img itemprop="image" src="<?php echo $n['Images'][0]['url'].'?node_id='.$n['Post']['id']; ?>" alt="<?php echo $n['Post']['title']; ?>" />
												</div>
												<figcaption class="c-article_figcaption">
													<?php if(isset($figCaption)){
														echo $figCaption;
													}
													if(isset($figAutor)){
														echo ' (Foto: '.$figAutor.')';
													} ?>
												</figcaption>
											</figure>
										<?php } ?>
									</a>
									<div id="corpo" style="color:#000; margin-bottom:20px;">
										
										<?php
										$corpo = $n['Post']['body'];
										//se o conteúdo é mais antigo que a data da importação
										if($n['Post']['publish_start'] < '2018-04-02 14:23:00'){
											//remove tags antigas e desnecessárias
											$corpo = $this->Mswi->removeTagsWP($corpo);
											$corpo = $this->Mswi->limpaTags($corpo);
										}		
										echo $this->Mswi->removeStyle($corpo);											
										?>												
									</div>
								</div>								
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		<?php }else{ 
			echo 'Nenhum registro encontrado';
		} ?>	
		<?php 
		$this->Paginator->options['url'] = array('controller' => 'blogs', 'action' => 'view', 'slug' => $this->params['slug']);
		echo $this->element('paginator');
		?>
	</div>
</section>