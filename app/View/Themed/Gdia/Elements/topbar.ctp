<div class="topbar text-light bg-dark">
    <div class="container d-flex flex-wrap">
        <nav class="nav flex-fill flex-nowrap">
            <button type="button" class="btn btn-link nav-link toggle-offcanvas"><i class="fas fa-bars"></i></button>
            <button type="button" class="btn btn-link nav-link toggle-searchbar"><i class="fas fa-search"></i></button>
        </nav>
        <span class="navbar-text text-right flex-fill d-none d-sm-none d-md-block">
            Portal de Informações e Negócios de Foz do Iguaçu, Região Trinacional e Mercosul
        </span>
    </div>
</div>