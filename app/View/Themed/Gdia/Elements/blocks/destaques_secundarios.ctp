<div class="destaques-secundarios container text-light bg-blue py-3">
    <div class="row">
        <!-- Destaques Secundários Topo -->
        <?php echo $this->Regions->blocks('destaques_secundarios_topo', array('cache' => true)); ?>
        <!-- Destaques Secundários Topo -->
    </div>
    <div class="row">
        <!-- Destaques Secundários Categorias -->
        <?php echo $this->Regions->blocks('destaques_secundarios_categorias', array('cache' => true)); ?>
        <!-- Destaques Secundários Categorias -->
    </div>
</div>