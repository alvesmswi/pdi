<?php if (!empty($nodesList)): ?>
    <div class="destaques container">
        <div class="row">
            <div class="col-sm-8">
                <?php
                    $node1 = $nodesList[0];
                    $link_noticia_1 = $this->Html->url($node1['Node']['path']);
                    if (isset($node1['NoticiumDetail']['link_externo']) && !empty($node1['NoticiumDetail']['link_externo'])) {
                        $link_noticia_1 = $node1['NoticiumDetail']['link_externo'];
                    }
                ?>
                <div class="news">
                    <h1><?php echo $node1['Node']['title']; ?></h1>
                    <p><?php echo $node1['Node']['excerpt']; ?> <a class="read-more" href="<?php echo $link_noticia_1; ?>">Leia mais</a> <a class="read-more" href="#"><i class="fas fa-share-alt"></i></a></p>
                </div>

                <div id="carouselDestaques" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php foreach ($nodesList as $key => $node): ?>
                            <?php if ($key < 2) continue; ?>
                            <?php $class_active = ($key == 2) ? 'class="active"' : null; ?>
                            <li data-target="#carouselDestaques" data-slide-to="<?php echo $key; ?>" <?php echo $class_active; ?>></li>
                        <?php endforeach; ?>
                    </ol>
                    <div class="carousel-inner">
                        <?php foreach ($nodesList as $key => $node): ?>
                            <?php if ($key < 2) continue; ?>
                            <?php
                                $link_noticia = $this->Html->url($node['Node']['path']);
                                if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
                                    $link_noticia = $node['NoticiumDetail']['link_externo'];
                                }

                                $imagemUrlArray = array(
                                    'plugin'		=> 'Multiattach',
                                    'controller'	=> 'Multiattach',
                                    'action'		=> 'displayFile', 
                                    'admin'			=> false,
                                    'filename'		=> $node['Multiattach']['filename'],
                                    'dimension'		=> '730x550'
                                );
                                $imagemUrl = $this->Html->url($imagemUrlArray);

                                $active = ($key == 2) ? ' active' : null;
                            ?>
                            <div class="carousel-item<?php echo $active; ?>">
                                <a href="<?php echo $link_noticia; ?>">
                                    <img src="<?php echo $imagemUrl; ?>" class="d-block w-100" alt="<?php echo $node['Node']['title'] ?>">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5><?php echo $node['Node']['title'] ?></h5>
                                        <p><?php echo $node['Node']['excerpt'] ?></p>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselDestaques" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselDestaques" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Próximo</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <?php
                    $node2 = $nodesList[1];
                    $link_noticia_2 = $this->Html->url($node2['Node']['path']);
                    if (isset($node2['NoticiumDetail']['link_externo']) && !empty($node2['NoticiumDetail']['link_externo'])) {
                        $link_noticia_2 = $node2['NoticiumDetail']['link_externo'];
                    }

                    $imagemUrlArray2 = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $node2['Multiattach']['filename'],
                        'dimension'		=> '350x200'
                    );
                    $imagemUrl2 = $this->Html->url($imagemUrlArray2);
                ?>
                <div class="news-box">
                    <a href="<?php echo $link_noticia_2; ?>">
                        <h1 class="h3"><?php echo $node2['Node']['title']; ?></h1>
                        <img src="<?php echo $imagemUrl2; ?>" class="img-fluid" alt="<?php echo $node2['Node']['title']; ?>">
                    </a>
                </div>
                
                <!-- Destaque direita -->
                <?php echo $this->Regions->blocks('destaque_direita', array('cache' => true)); ?>
                <!-- Destaque direita -->
            </div>
        </div>
    </div>
<?php endif; ?>
