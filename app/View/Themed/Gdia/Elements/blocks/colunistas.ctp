<div class="col-sm-4">
    <div class="box text-center">
        <p class="title-box">Colunas e Blogues</p>
        <div id="carouselColunasBlogues" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-6">
                            <img src="https://placeimg.com/160/275/any/grayscale" alt="...">
                        </div>
                        <div class="col-6">
                            <p class="title-box">Fábio Campana</p>
                            <p class="text-left">As novas do Paraná e tudo sobre o ambiente político no cenário brasileiro</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-6">
                            <img src="https://placeimg.com/160/275/any/grayscale" alt="...">
                        </div>
                        <div class="col-6">
                            <p class="title-box">Chico de Alencar</p>
                            <p class="text-left">As novas do Paraná e tudo sobre o ambiente político no cenário brasileiro</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-6">
                            <img src="https://placeimg.com/160/275/any/grayscale" alt="...">
                        </div>
                        <div class="col-6">
                            <p class="title-box">Rogério Bonato</p>
                            <p class="text-left">As novas do Paraná e tudo sobre o ambiente político no cenário brasileiro</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselColunasBlogues" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#carouselColunasBlogues" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
</div>