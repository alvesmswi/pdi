<?php
    $impresso = $nodesList[0];
    $link_impresso = $this->Html->url($impresso['Node']['path']);
    if (isset($impresso['ImpressoDetail']['link']) && !empty($impresso['ImpressoDetail']['link'])) {
        $link_impresso = $impresso['ImpressoDetail']['link'];
    }

    $imagemUrlArray = array(
        'plugin'		=> 'Multiattach',
        'controller'	=> 'Multiattach',
        'action'		=> 'displayFile', 
        'admin'			=> false,
        'filename'		=> $impresso['Multiattach']['filename'],
        'dimension'		=> '350x400'
    );
    $imagemUrl = $this->Html->url($imagemUrlArray);
?>

<div class="edicao-impresso-box mt-2">
    <p class="title-box">Edição impressa</p>
    <a href="<?php echo $link_impresso; ?>">
        <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $impresso['Node']['title']; ?>">
    </a>
</div>
