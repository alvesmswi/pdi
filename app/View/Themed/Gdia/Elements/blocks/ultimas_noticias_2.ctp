<?php if (!empty($nodesList)): ?>

    <?php foreach ($nodesList as $key => $node): ?>
    <?php if ($key < 4) continue; ?>

    <?php if ($key % 2 === 0): ?>
        <div class="row">
    <?php endif; ?>

        <div class="col-sm-6">
            <?php
                $link_noticia = $this->Html->url($node['Node']['path']);
                if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node['NoticiumDetail']['link_externo'];
                }

                $imagemUrlArray = array(
                    'plugin'		=> 'Multiattach',
                    'controller'	=> 'Multiattach',
                    'action'		=> 'displayFile', 
                    'admin'			=> false,
                    'filename'		=> $node['Multiattach']['filename'],
                    'dimension'		=> '350x200'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
            ?>
            <div class="news">
                <a class="editoria" href="/ultimas-noticias/<?php echo $node['Editoria']['slug']; ?>"><?php echo $node['Editoria']['title']; ?></a>
                <h1 class="h3"><?php echo $node['Node']['title']; ?></h1>
                <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
                <p><?php echo $node['Node']['excerpt']; ?> <a class="read-more" href="<?php echo $link_noticia; ?><?php echo $link_noticia; ?>">Leia mais</a> <a class="read-more" href="#"><i class="fas fa-share-alt"></i></a></p>
            </div>
        </div>

    <?php if ($key % 2 === 1 || $key + 1 === count($nodesList)): ?>
        </div>
    <?php endif; ?>

    <?php endforeach; ?>

<?php endif; ?>
