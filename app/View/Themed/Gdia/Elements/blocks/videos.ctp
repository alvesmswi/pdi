<div class="col-sm-4">
    <div class="box text-center">
        <p class="title-box">GdiaTV</p>
        <?php 
            $results = Cache::read('videos_youtube_home');
            if (empty($results)) {
                App::uses('HttpSocket', 'Network/Http');

                $API_key = Configure::read('Youtube.api_key');
                $maxResults = Configure::read('Youtube.max_lista');
                $channelID = Configure::read('Youtube.id_canal');

                $HttpSocket = new HttpSocket();

                $results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
                    'order' => 'date',
                    'part' => 'snippet',
                    'channelId' => $channelID,
                    'maxResults' => $maxResults,
                    'key' => $API_key
                ));
                $results = json_decode($results->body);
                Cache::write('videos_youtube_home', $results); 
            }

            if (!empty($results)) {
                foreach($results->items as $item) {
                    if (isset($item->id->videoId)) {
                        echo '<div class="youtube-video">
                                <iframe width="350" height="275" src="https://www.youtube.com/embed/'.$item->id->videoId.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>';
                    }
                }
            }
        ?>
    </div>
</div>

