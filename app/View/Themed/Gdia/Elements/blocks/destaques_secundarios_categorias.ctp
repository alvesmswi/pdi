<?php if ($nodesList): ?>
    <?php
        $node = $nodesList[0];

        $imagemUrlArray = array(
            'plugin'		=> 'Multiattach',
            'controller'	=> 'Multiattach',
            'action'		=> 'displayFile', 
            'admin'			=> false,
            'filename'		=> $node['Multiattach']['filename'],
            'dimension'		=> '350x275'
        );
        $imagemUrl = $this->Html->url($imagemUrlArray);
    ?>
    <div class="col-sm-4">
        <div class="box text-center">
            <p class="title-box"><?php echo $node['Editoria']['title']; ?></p>
            <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
            <div class="carousel-caption box-content">
                <h1 class="h3"><?php echo $node['Node']['title']; ?></h1>
            </div>
        </div>
    </div>
<?php endif; ?>
