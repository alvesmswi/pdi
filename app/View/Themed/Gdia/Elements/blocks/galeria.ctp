<?php if (!empty($nodesList)): ?>
    <div class="galeria container text-light bg-blue py-3">
        <h1 class="h3 title-gallery">Galeria</h1>
        <div id="carouselGaleria" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach ($nodesList as $key => $img): ?>
                <?php
                    $link_noticia = $this->Html->url($img['Node']['path']);
                    if (isset($img['NoticiumDetail']['link_externo']) && !empty($img['NoticiumDetail']['link_externo'])) {
                        $link_noticia = $img['NoticiumDetail']['link_externo'];
                    }
                    
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $img['Multiattach']['filename'],
                        'dimension'		=> '750x550'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);

                    if (isset($img_destaque['metaDisplay']) && !empty($img_destaque['metaDisplay'])) {
                        $figCaption = $img_destaque['metaDisplay'];
                    }
                    if (!empty($img_destaque['comment'])) {
                        $figAutor = '<i>(Foto: '.$img_destaque['comment'].')</i>';
                    }

                    $active = ($key == 0) ? ' active' : null;
                ?>
                <div class="carousel-item text-center<?php echo $active; ?>">
                    <a href="<?php echo $link_noticia; ?>">
                        <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $img['Node']['title']; ?>">
                        <p><?php echo $img['Node']['title']; ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselGaleria" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselGaleria" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próxima</span>
        </a>
        </div>
    </div>
<?php endif; ?>
