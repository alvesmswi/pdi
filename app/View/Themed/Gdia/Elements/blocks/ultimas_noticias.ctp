<?php if (!empty($nodesList)): ?>
    <div class="main-news">
        <p class="title-box">Noticiário</p>
        <?php foreach ($nodesList as $key => $node): ?>
            <?php
                $link_noticia = $this->Html->url($node['Node']['path']);
                if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node['NoticiumDetail']['link_externo'];
                }

                $imagemUrlArray = array(
                    'plugin'		=> 'Multiattach',
                    'controller'	=> 'Multiattach',
                    'action'		=> 'displayFile', 
                    'admin'			=> false,
                    'filename'		=> $node['Multiattach']['filename'],
                    'dimension'		=> '225x175'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
            ?>
            <div class="row">
                <div class="col-sm-4 text-center">
                    <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
                </div>
                <div class="col-sm-8">
                    <a class="editoria" href="/ultimas-noticias/<?php echo $node['Editoria']['slug']; ?>"><?php echo $node['Editoria']['title']; ?></a>
                    <h1 class="h3"><?php echo $node['Node']['title']; ?></h1>
                    <p><?php echo $node['Node']['excerpt']; ?> <a class="read-more" href="<?php echo $link_noticia; ?>">Leia mais</a> <a class="read-more" href="#"><i class="fas fa-share-alt"></i></a></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
