<div class="brand">
    <a href="/">
        <?php
            $siteLogo = Configure::read('Site.logo');
            if (!empty($siteLogo)) {
                $themeLogo = Router::url('/', true).$siteLogo;
            } else {								
                $themeLogo = WWW_ROOT . 'img' . DS . 'logo.png';
                $themeLogo = (file_exists($themeLogo)) ? '/img/logo.png' : '/theme/'.Configure::read('Site.theme').'/img/logo.png';
            }
        ?>
        <img class="logo img-fluid" src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
    </a>
</div>