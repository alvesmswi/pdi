<div class="searchbar container text-light bg-dark">
    <form action="/search">
        <label class="sr-only" for="search">Pesquisar</label>
        <input type="text" id="search" name="q" class="form-control" placeholder="Pesquisar">
    </form>
</div>