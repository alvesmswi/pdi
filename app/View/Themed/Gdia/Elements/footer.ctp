<div class="footer text-light bg-dark text-center">
    <?php echo $this->Menus->menu('footer', array('element' => 'menu_footer')); ?>
    
    <!-- Região Footer -->

    <hr class="hr-light" />

    <button type="button" class="btn btn-link go-top" title="Voltar ao topo">
        <i class="fas fa-chevron-up"></i>
    </button>
    <span class="d-block py-3">GDia e Gazeta de Foz do Iguaçu são produtos da Gazeta News Empreendimentos Informativos</span>
</div>