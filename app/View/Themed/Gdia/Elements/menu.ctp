<?php if (!empty($menu['threaded'])): ?>
  <div class="offcanvas-menu text-light bg-dark">
    <button type="button" class="close-offcanvas close" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <nav class="nav flex-column">
      <?php foreach ($menu['threaded'] as $key => $menu): ?>
        <?php $active = (isset($this->request->params['slug']) && $this->request->params['slug'] === $menu['Link']['class']) ? ' active' : null; ?>
        <a class="nav-link<?php echo $active; ?>" href="<?php echo $menu['Link']['link'] ?>">
          <?php echo $menu['Link']['title'] ?>
        </a>
      <?php endforeach; ?>
    </nav>
    
  </div>
<?php endif; ?>