<div class="menu-footer container d-flex align-items-center flex-wrap">
    <?php							
        $logoFooter = WWW_ROOT . 'img' . DS . 'logo_footer.png';
        $logoFooter = (file_exists($logoFooter)) ? '/img/logo_footer.png' : '/theme/'.Configure::read('Site.theme').'/img/logo_footer.png';
    ?>
    <img class="logo-footer mr-auto img-fluid" src="<?php echo $logoFooter; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
    <?php if (!empty($menu['threaded'])): ?>
        <nav class="nav">
            <?php foreach ($menu['threaded'] as $key => $menu): ?>
                <a class="nav-link" href="<?php echo $menu['Link']['link'] ?>">
                    <?php echo $menu['Link']['title'] ?>
                </a>
            <?php endforeach; ?>
        </nav>
    <?php endif; ?>
</div>