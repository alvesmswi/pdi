$('.go-top').click(() => {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
});

$('.toggle-offcanvas').click(() => {
    if ($('.c-topbar-admin').length > 0) {
        $('.offcanvas-menu').css('top', '45px');
    }
    
    $('.offcanvas-menu').toggle(500);
});
$('.close-offcanvas').click(() => {
    $('.offcanvas-menu').hide(500);
});

$('.toggle-searchbar').click(() => {
    if ($('.c-topbar-admin').length > 0) {
        $('.searchbar').css('top', '45px');
    }
    
    $('.searchbar').toggle(500);
});
