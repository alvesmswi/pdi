<?php
    setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
    date_default_timezone_set( 'America/Sao_Paulo' );
?>

<!doctype html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php echo $this->fetch('meta'); ?>

        <title><?php echo Configure::read('Site.title') . ' | ' . $title_for_layout; ?></title>

        <link rel="apple-touch-icon" sizes="57x57" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/theme/<?php echo Configure::read('Site.theme')?>/icon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/theme/<?php echo Configure::read('Site.theme')?>/icon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/theme/<?php echo Configure::read('Site.theme')?>/icon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/fontawesome.min.css">
        <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/theme.css">

        <?php if (!empty(Configure::read('Service.analytics'))):
			$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
			<script>
				(function (i, s, o, g, r, a, m) {
					i['GoogleAnalyticsObject'] = r;
					i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
					a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a, m)
				})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

				<?php foreach ($arrayAnalytics as $key => $analytics) { ?>
                    ga('create', '<?php echo trim($analytics); ?>', 'auto', {
                        'name': 'id<?php echo $key; ?>'
                    });
                    ga('id<?php echo $key; ?>.send', 'pageview');
				<?php } ?>

                /**
                * Function that tracks a click on an outbound link in Analytics.
                * This function takes a valid URL string as an argument, and uses that URL string
                * as the event label. Setting the transport method to 'beacon' lets the hit be sent
                * using 'navigator.sendBeacon' in browser that support it.
                */
				var trackOutboundLink = function (url) {
					ga('send', 'event', 'outbound', 'click', url, {
						'transport': 'beacon',
						'hitCallback': function () {
							document.location = url;
						}
					});
				}
			</script>
		<?php endif; ?>
	
		<script type='text/javascript'>
			var googletag = googletag || {};
			googletag.cmd = googletag.cmd || [];
			(function () {
				var gads = document.createElement('script');
				gads.async = true;
				gads.type = 'text/javascript';
				var useSSL = 'https:' == document.location.protocol;
				gads.src = (useSSL ? 'https:' : 'http:') +
					'//www.googletagservices.com/tag/js/gpt.js';
				var node = document.getElementsByTagName('script')[0];
				node.parentNode.insertBefore(gads, node);
			})();
		</script>

        <!--nocache-->
		<?php if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))): ?>
            <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home'); ?>" />
        <?php endif; ?>

		<?php if ($this->here != '/' && (isset($node['Node']['type']) && $node['Node']['type'] == 'noticia') && !empty(Configure::read('Site.refresh_internas')) && is_numeric(Configure::read('Site.refresh_internas'))): ?>
			<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
        <?php endif; ?>
		<!--/nocache-->

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<?php
		    echo Configure::read('Service.header');
		    echo $this->Regions->blocks('header_scripts', array('cache' => true));
		?>
    </head>

    <body>
        <!--nocache-->
		<?php 
            if (isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])) {
                echo $this->element('admin_bar');
            } 
		?>
		<!--/nocache-->

        <?php echo $this->element('topbar'); ?>
        <?php echo $this->Menus->menu('main', array('element' => 'menu')); ?>
        <?php echo $this->element('search'); ?>
        <?php echo $this->element('brand'); ?>
        
        <!-- Região 1 -->
        <?php echo $this->Regions->blocks('regiao1', array('cache' => true)); ?>
        <!-- Região 1 -->
        
        <main class="container py-2">
            <div class="row">
                <div class="col-lg-8">
                    <?php echo $this->Layout->sessionFlash(); ?>
                    <?php echo $content_for_layout; ?>
                    <!-- Conteúdo -->
                    <?php echo $this->Regions->blocks('content', array('cache' => true)); ?>
                    <!-- Conteúdo -->
                </div>
                <div class="col-lg-4">
                    <!-- Right -->
                    <?php echo $this->Regions->blocks('right', array('cache' => true)); ?>
                    <!-- Right -->
                </div>
            </div>
        </main>
        
        <!-- Região 2 -->
        <section class="container">
            <div class="row">
                <?php echo $this->Regions->blocks('regiao2', array('cache' => true)); ?>
            </div>
        </section>
        <!-- Região 2 -->
        
        <?php if ($this->Regions->blocks('regiao3') || $this->Regions->blocks('regiao4')): ?>
            <section class="container py-2">
                <div class="row">
                    <div class="col-sm-8">
                        <!-- Região 3 -->
                        <?php echo $this->Regions->blocks('regiao3', array('cache' => true)); ?>
                        <!-- Região 3 -->
                    </div>
                    <div class="col-sm-4">
                        <!-- Região 4 -->
                        <?php echo $this->Regions->blocks('regiao4', array('cache' => true)); ?>
                        <!-- Região 4 -->
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <!-- Região 5 -->
        <?php echo $this->Regions->blocks('regiao5', array('cache' => true)); ?>
        <!-- Região 5 -->

        <?php echo $this->element('footer'); ?>

        <?php echo $this->element('sql_dump'); ?>

        <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.min.js"></script>
        <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/bootstrap.bundle.min.js"></script>
        <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/theme.js"></script>
        <?php
            if (!empty(Configure::read('Service.addthis'))) {
                echo '<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid='.Configure::read('Service.addthis').'"></script>';
            }

            echo $this->Regions->blocks('footer_scripts', array('cache' => true));		
            echo $this->fetch('script');
            echo $this->Js->writeBuffer(); // Write cached scripts
		?>
    </body>
</html>