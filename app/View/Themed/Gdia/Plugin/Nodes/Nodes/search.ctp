<?php 
    echo $this->element('node_metas');
    $cxGoogle = Configure::read('Site.busca_google');
?>

<?php if (!$cxGoogle): ?>
    <h1><?php echo (isset($this->params->query['q']) && !empty($this->params->query['q'])) ? __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"') : __d('croogo', 'Resultado da pesquisa'); ?></h1>

    <hr />

    <?php if (!empty($nodes)): ?>
        <?php foreach ($nodes as $key => $node): ?>
            <?php
                $link_noticia = $this->Html->url($node['Node']['path']);
                if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node['NoticiumDetail']['link_externo'];
                }

                $imagemUrlArray = array(
                    'plugin'		=> 'Multiattach',
                    'controller'	=> 'Multiattach',
                    'action'		=> 'displayFile', 
                    'admin'			=> false,
                    'filename'		=> $node['Multiattach']['filename'],
                    'dimension'		=> 'normal'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
            ?>
            <div class="row">
                <div class="col-sm-4 text-center">
                    <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
                </div>
                <div class="col-sm-8">
                    <h1 class="h3"><?php echo $node['Node']['title']; ?></h1>
                    <span class="date"><?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?></span>
                    <p>
                        <?php echo $node['Node']['excerpt']; ?> 
                        <a class="read-more" href="<?php echo $link_noticia; ?>">Leia mais</a> 
                        <a class="read-more" href="#"><i class="fas fa-share-alt"></i></a>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>

        <?php echo $this->element('paginator'); ?>
    <?php endif; ?>
<?php else: ?>
    <h1><?php echo (isset($this->params->query['q']) && !empty($this->params->query['q'])) ? __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"') : __d('croogo', 'Resultado da pesquisa'); ?></h1>

    <hr />

	<style>
		.gsc-control-cse {
			line-height: normal;
		}
		.gsc-adBlock {
			display: none;
		}
		.gsc-control-cse table {
			margin: 0;
        }
        .gsc-table-result, 
        .gsc-thumbnail-inside, 
        .gsc-url-top {
            padding-left: 0;
            padding-right: 0;
        }
        .gsc-result-info {
            padding: 8px 0;
        }
        .cse .gsc-control-cse, 
        .gsc-control-cse {
            padding: 0;
        }
	</style>
	<script>
		(function() {
			var cx = '<?php echo Configure::read('Site.busca_google')?>';
			var gcse = document.createElement('script');
			gcse.type = 'text/javascript';
			gcse.async = true;
			gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(gcse, s);
		})();
	</script>
	<gcse:search></gcse:search>
<?php endif; ?>