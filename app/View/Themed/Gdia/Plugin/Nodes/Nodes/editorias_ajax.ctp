<?php if (!empty($nodes)): ?>
    <?php
        $page = $this->params->query['page'];
		$proximaPagina = $page + 1;
    ?>
    <?php foreach ($nodes as $key => $node): ?>
        <?php
            $link_noticia = $this->Html->url($node['Node']['path']);
            if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
                $link_noticia = $node['NoticiumDetail']['link_externo'];
            }

            $imagemUrlArray = array(
                'plugin'		=> 'Multiattach',
                'controller'	=> 'Multiattach',
                'action'		=> 'displayFile', 
                'admin'			=> false,
                'filename'		=> $node['Multiattach']['filename'],
                'dimension'		=> 'normal'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
        ?>
        <div class="row">
            <div class="col-sm-4 text-center">
                <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
            </div>
            <div class="col-sm-8">
                <h1 class="h3"><?php echo $node['Node']['title']; ?></h1>
                <span class="date"><?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?></span>
                <p>
                    <?php echo $node['Node']['excerpt']; ?> 
                    <a class="read-more" href="<?php echo $link_noticia; ?>">Leia mais</a> 
                    <a class="read-more" href="#"><i class="fas fa-share-alt"></i></a>
                </p>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php echo (!empty($nodes)) ? $this->Mswi->verMaisAjax('editorias_ajax', $node['Editoria']['slug'], $proximaPagina, 8) : null; ?>
