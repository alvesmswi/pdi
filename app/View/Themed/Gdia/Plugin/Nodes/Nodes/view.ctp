<?php
    $this->Nodes->set($node); 

    echo $this->element('node_metas');
?>

<h1><?php echo $node['Node']['title']; ?></h1>

<hr />

<div class="py-3 text-right">
    <time datetime="<?php echo $node['Node']['publish_start']; ?>">
        <?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
        <?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
    </time>
    <?php
        if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
            echo "- <i>Por: {$node['NoticiumDetail']['jornalista']}</i>";
        } else {
            echo "- <i>Por: {$node['User']['name']}</i>";
        }
    ?>
</div>

<hr />

<?php if (!empty($node['Multiattach']) && strpos($node['Multiattach'][0]['Multiattach']['mime'], 'image/') !== false): ?>
    <?php
        $img_destaque = $node['Multiattach'][0]['Multiattach'];
        $imagemUrlArray = array(
            'plugin'		=> 'Multiattach',
            'controller'	=> 'Multiattach',
            'action'		=> 'displayFile', 
            'admin'			=> false,
            'filename'		=> $img_destaque['filename'],
            'dimension'		=> '730x350'
        );
        $imagemUrl = $this->Html->url($imagemUrlArray);

        //prepara a imagem para o OG
        $imagemUrlArray['dimension'] = 'normal';
        $this->Html->meta(
            array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
            null,
            array('inline' => false)
        );
        
        //pega os detalhes da imagem
        $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

        //largura da imagem
        $this->Html->meta(
            array('property' => 'og:image:width', 'content' => $imageArray[0]),
            null,
            array('inline' => false)
        );
        //Altura da imagem
        $this->Html->meta(
            array('property' => 'og:image:height', 'content' => $imageArray[1]),
            null,
            array('inline' => false)
        );
        //Mime da imagem
        $this->Html->meta(
            array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
            null,
            array('inline' => false)
        );
        //Alt da imagem
        $this->Html->meta(
            array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
            null,
            array('inline' => false)
        );

        $figCaption = null;
        if (isset($img_destaque['metaDisplay']) && !empty($img_destaque['metaDisplay'])) {
            $figCaption = $img_destaque['metaDisplay'];
        }
        $figAutor = null;
        if (!empty($img_destaque['comment'])) {
            $figAutor = '<i>(Foto: '.$img_destaque['comment'].')</i>';
        }
    ?>
    <figure class="figure">
        <img src="<?php echo $imagemUrl; ?>" class="figure-img img-fluid" alt="<?php echo $node['Node']['title']; ?>">
        <?php if ((isset($figCaption) || isset($figAutor)) && (!empty($figCaption) || !empty($figAutor))): ?>
            <figcaption class="figure-caption text-right"><?php echo $figCaption . $figAutor; ?></figcaption>
        <?php endif; ?>
    </figure>
<?php endif; ?>

<?php echo $this->Mswi->lerCodigos($node['Node']['body']); ?>

<?php if (!empty($node['Multiattach']) && count($node['Multiattach']) > 1): ?>
    <div id="carouselView" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php foreach ($node['Multiattach'] as $key => $img): ?>
                <?php if ($key < 1) continue; ?>
                <?php if (strpos($img['Multiattach']['mime'], 'image/') !== false): ?>
                    <?php
                        $class_active = ($key == 1) ? 'class="active"' : null;
                    ?>
                    <li data-target="#carouselView" data-slide-to="<?php echo $key; ?>" <?php echo $class_active; ?>></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ol>
        <div class="carousel-inner">
            <?php foreach ($node['Multiattach'] as $key => $img): ?>
                <?php if ($key < 1) continue; ?>
                <?php if (strpos($img['Multiattach']['mime'], 'image/') !== false): ?>
                    <?php
                        $imagemUrlArray = array(
                            'plugin'		=> 'Multiattach',
                            'controller'	=> 'Multiattach',
                            'action'		=> 'displayFile', 
                            'admin'			=> false,
                            'filename'		=> $img['Multiattach']['filename'],
                            'dimension'		=> '730x350'
                        );
                        $imagemUrl = $this->Html->url($imagemUrlArray);

                        if (isset($img_destaque['metaDisplay']) && !empty($img_destaque['metaDisplay'])) {
                            $figCaption = $img_destaque['metaDisplay'];
                        }
                        if (!empty($img_destaque['comment'])) {
                            $figAutor = '<i>(Foto: '.$img_destaque['comment'].')</i>';
                        }

                        $active = ($key == 1) ? ' active' : null;
                    ?>
                    <div class="carousel-item<?php echo $active; ?>">
                        <img src="<?php echo $imagemUrl; ?>" class="img-fluid" alt="<?php echo $node['Node']['title']; ?>">
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselView" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselView" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próxima</span>
        </a>
    </div>
<?php endif; ?>
