<?php

echo $this->element('node_metas');

if (isset($node['PageFlip']) && !empty($node['PageFlip'])) {

    echo $this->element('PageFlip.view_impresso', array(
        'pageflip' => $node['PageFlip'],
        'download' => ((isset($pageflip_download)) ? $pageflip_download : false) 
    ));
    
}
