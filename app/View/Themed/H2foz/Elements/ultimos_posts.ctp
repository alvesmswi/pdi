<?php

$this->Helpers->load('Medias.MediasImage');

if (isset($posts) && !empty($posts)):
  $countPost = 0;
  foreach($posts as $post):
    if($post['Blog']['slug'] != 'aonde-ir' && $countPost < 1){
      $countPost += 1;
    ?>
      <div class="col-sm-6">
        <div class="news--collection">
          <header class="news--header">
            <a href="/blogs" class="news--header__title"><?php echo "Blogueiros"?></a>
          </header>
          <div class="news--block">
            <?php if (isset($post['images']['filename']) && !empty($post['images']['filename'])): ?>
              <a href="/blog/<?php echo $post['Blog']['slug'] ?>/post/<?php echo $post['Post']['slug'] ?>">
                <img src="<?php echo $this->MediasImage->imagePreset($post['images']['filename'], '400x225'); ?>" alt="<?php echo $post['images']['alt']; ?>" class="img--responsive news--headline__image" />
              </a>
            <?php endif; ?>
            <div class="news--headline">
              <div class="news--metadata">
                <a href="/blog/<?php echo $post['Blog']['slug'] ?>" class="headline--category"><i class="icons icons--tags"></i> 
                  <?php echo $post['Blog']['title']; ?>
                </a>
              </div>
              <a href="/blog/<?php echo $post['Blog']['slug'] ?>/post/<?php echo $post['Post']['slug'] ?>" class="headline--title">
                <h2>
                  <?php echo $post['Post']['title'] ?>
                </h2>
              </a>
              <?php if (isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){ ?>
                <p class="headline--excerpt"><?php echo $post['Post']['excerpt'] ?></p>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    <?php }
    endforeach;
endif; 
if(isset($postAgenda) && !empty($postAgenda)){
  $countAgenda = 0;
  foreach ($postAgenda as $agenda):    
    if($agenda['Blog']['slug'] == 'aonde-ir' && $countAgenda < 1){
      $countAgenda += 1;
      // pr($agenda);exit;
    ?>
      <div class="col-sm-6">
        <div class="news--collection">
          <header class="news--header">
            <a href="/blogs" class="news--header__title"><?php echo "Agenda"?></a>
          </header>
          <div class="news--block">
            <?php if (isset($agenda['Images']['filename']) && !empty($agenda['Images']['filename'])): ?>
              <a href="/blog/<?php echo $agenda['Blog']['slug'] ?>/post/<?php echo $agenda['Post']['slug'] ?>">
                <img src="<?php echo $this->MediasImage->imagePreset($agenda['Images']['filename'], '400x225'); ?>" alt="<?php echo $agenda['Images']['alt']; ?>" class="img--responsive news--headline__image" />
              </a>
            <?php endif; ?>
            <div class="news--headline">
              <div class="news--metadata">
                <a href="/blog/<?php echo $agenda['Blog']['slug'] ?>" class="headline--category"><i class="icons icons--tags"></i> 
                  <?php echo $agenda['Blog']['title']; ?>
                </a>
              </div>
              <a href="/blog/<?php echo $agenda['Blog']['slug'] ?>/post/<?php echo $agenda['Post']['slug'] ?>" class="headline--title">
                <h2>
                  <?php echo $agenda['Post']['title'] ?>
                </h2>
              </a>
              <?php if (isset($agenda['Post']['excerpt']) && !empty($agenda['Post']['excerpt'])){ ?>
                <p class="headline--excerpt"><?php echo $agenda['Post']['excerpt'] ?></p>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    <?php }?>
  <?php endforeach; 
}
?>
