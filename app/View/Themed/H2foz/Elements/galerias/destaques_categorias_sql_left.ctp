<?php 

if(!empty($nodesList)){
?>
	<div class="s-list o-layout_s" id="<?php echo $this->Mswi->categorySlug($nodesList[0]['Node']['terms']); ?>">
		<header class="o-header">
			<a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[0]['Node']['terms']); ?>" class="o-header_title o-header_title--medium">
				<?php 
				if($block['Block']['show_title']){
					echo $block['Block']['title'];
				}else{
					echo $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
				}
				?>
			</a>
		</header>
		<?php
		// $contador = 0;
		// $total = count($nodesList);
		//percorre os nodes
		foreach($nodesList as $n) {?>
			<div class="o-news">
		    	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
	      			<?php 
					//se tem imagem
					if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> '130x130'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
						?>
             			<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" class="o-news_thumb" alt="<?php echo $imageTitle ?>"> 
             		<?php } ?>

             		<span class="o-news_category">
         				<?php 
	             		//se tem chapey
	             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
	             			echo $n['NoticiumDetail']['chapeu'];
	             		}else{
	             			echo $this->Mswi->categoryName($n['Node']['terms']);
	             		}; 
	             		?>
         			</span>
                  	<span class="o-news_title">
                  		<?php 
						//se tem titulo da capa
						if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
						  echo $n['NoticiumDetail']['titulo_capa'];
						}else{
						  echo $n['Node']['title'];
						}
						?>
					</span>
		    	</a>
		  	</div>	
      	<?php } ?>	
	</div>
<?php } ?>