<?php
//se tem algum registro
if(!empty($nodesList)){
	//array que vai guardar as noticias em destaques
	$arrayDestaques = array();
	$contador = 0;
	$maximo = 8;
	//percorre os nodes
	foreach($nodesList as $key => $nd){
		//pega a ordenação
		$ordem = $nd['Node']['ordem'];
		//se ainda não pasosu por este item
		if(!isset($arrayDestaques[$ordem])){
			//se tem ordenação
			$arrayDestaques[$ordem] = $nodesList[$key];
			$contador++;
		}
		//se chegou no maximo
		if($contador == $maximo){
			break;
		}
	}
	//substitui os dados selecionados
	$nodesList = array_values($arrayDestaques);
?>

	<div class="s-highlights">
		<div class="row o-layout_row">
			<div class="col-md-6 col-lg-7 o-layout_c">
				<?php $n = $nodesList[0]; $c = count($nodesList); ?>
				<div class="o-news">
	            	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
						<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '474largura'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
						<img src="<?php echo $imagemUrl; ?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
						<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

	              		<span class="o-news_category">
	              			<?php 
		             		//se tem chapey
		             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['titulo_capa'])){
		             			echo $n['NoticiumDetail']['chapeu'];
		             		}else{
		             			echo $this->Mswi->categoryName($n['Node']['terms']);
		             		}; 
		             		?>
	              		</span>
	              		<span class="o-news_title o-news_title--large">
	              			<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	              		</span>
	            	</a>
	          	</div>
	          	<?php if($c >= 3):
	          		$n = $nodesList[2]; ?>
	          		<div class="o-news">
		            	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
		            		<?php 
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
									$imagemUrlArray = array(
								        'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '130x130'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" />
							<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

		                	<span class="o-news_category">
		                		<?php 
			             		//se tem chapey
			             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
			             			echo $n['NoticiumDetail']['chapeu'];
			             		}else{
			             			echo $this->Mswi->categoryName($n['Node']['terms']);
			             		}; 
			             		?>
		                	</span>
		                	<span class="o-news_title">
		                		<?php 
								//se tem titulo da capa
								if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									echo $n['NoticiumDetail']['titulo_capa'];
								}else{
									echo $n['Node']['title'];
								}
								?>
		                	</span>
		            	</a>
			        </div>
	          	<?php endif; // $c >= 3 ?>
	          	<?php if($c >= 6):
	          		$n = $nodesList[5]; ?>
	          		<div class="o-news">
		            	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
		            		<?php 
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
									$imagemUrlArray = array(
								        'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '130x130'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" />
							<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

		                	<span class="o-news_category">
		                		<?php 
			             		//se tem chapey
			             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
			             			echo $n['NoticiumDetail']['chapeu'];
			             		}else{
			             			echo $this->Mswi->categoryName($n['Node']['terms']);
			             		}; 
			             		?>
		                	</span>
		                	<span class="o-news_title">
		                		<?php 
								//se tem titulo da capa
								if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									echo $n['NoticiumDetail']['titulo_capa'];
								}else{
									echo $n['Node']['title'];
								}
								?>
		                	</span>
		            	</a>
			        </div>
	          	<?php endif; // $c >= 6 ?>
			</div>
			<?php if($c > 1): ?>
			<div class="col-md-6 col-lg-5 o-layout_c">
				<?php foreach($nodesList as $i => $n): 
	    				if($i == 0 || $i == 2 || $i == 5) continue;
	    				if($i == 1):
		    	?>
		    	<div class="o-news">
	    			<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
	    				<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '329largura'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
						<img src="<?php echo $imagemUrl; ?>" class="o-news_image" alt="<?php echo $imageTitle ?>" />
						<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>
			    		<span class="o-news_category">
	                		<?php 
		             		//se tem chapey
		             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
		             			echo $n['NoticiumDetail']['chapeu'];
		             		}else{
		             			echo $this->Mswi->categoryName($n['Node']['terms']);
		             		}; 
		             		?>
	                	</span>
	                	<span class="o-news_title o-news_title--large">
	                		<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	                	</span>
					</a>
		    	</div>
		    	<?php else: // $i == 1 ?>
		    	<div class="o-news">
	            	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
	            		<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
								$imagemUrlArray = array(
							        'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '66x66'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
						<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>" />
						<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

	                	<span class="o-news_category">
	                		<?php 
		             		//se tem chapey
		             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
		             			echo $n['NoticiumDetail']['chapeu'];
		             		}else{
		             			echo $this->Mswi->categoryName($n['Node']['terms']);
		             		}; 
		             		?>
	                	</span>
	                	<span class="o-news_title">
	                		<?php 
							//se tem titulo da capa
							if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								echo $n['NoticiumDetail']['titulo_capa'];
							}else{
								echo $n['Node']['title'];
							}
							?>
	                	</span>
	            	</a>
		        </div>
		    	<?php endif; // $i == 1 ?>
		    	<?php endforeach; // $nodesList as $i => $n ?>
			</div>
			<?php endif; // $c > 1 ?>
		</div>
	</div>
<?php } ?>