<?php
//se tem algum registro
if(!empty($nodesList)){
	//array que vai guardar as noticias em destaques
	$arrayDestaques = array();
	$contador = 0;
	$maximo = 6;
	//percorre os nodes
	foreach($nodesList as $key => $nd){
		//pega a ordenação
		$ordem = $nd['Node']['ordem'];
		//se ainda não pasosu por este item
		if(!isset($arrayDestaques[$ordem])){
			//se tem ordenação
			$arrayDestaques[$ordem] = $nodesList[$key];
			$contador++;
		}
		//se chegou no maximo
		if($contador == $maximo){
			break;
		}
	}
	//substitui os dados selecionados
	$nodesList = array_values($arrayDestaques);

?>
	<div class="s-highlights">
		<?php $n = $nodesList[0]; $c = count($nodesList); ?>
		<div class="o-news">
	    	<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
	    		<?php 
					if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> '834largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
				?>
				<img src="<?php echo $imagemUrl; ?>" class="o-news_image" alt="<?php echo $imageTitle ?>"  />
				<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

	        	<span class="o-news_category">
					<?php 
					//se tem chapeu
             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
             			echo $n['NoticiumDetail']['chapeu'];
             		}else{
             			echo $this->Mswi->categoryName($n['Node']['terms']);
             		};
					?>
				</span>
	        	<span class="o-news_title o-news_title--huge">
	        		<?php 
					//se tem titulo da capa
					if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
					  echo $n['NoticiumDetail']['titulo_capa'];
					}else{
					  echo $n['Node']['title'];
					}
					?>
        		</span>
        		<?php if(isset($n['Node']['excerpt']) && !empty($n['Node']['excerpt'])): ?>
        		<span class="o-news_summary"><?php echo $n['Node']['excerpt']; ?></span>
        		<?php endif; ?>
	        </a>
	    </div>
	    <?php if($c >= 2): ?>
	    	<div class="row o-layout_row u-spacer_top">
	    		<div class="col-md-6 col-lg-7 o-layout_c">
	    			<?php $n = $nodesList[1]; ?>
	    			<div class="o-news">
		    			<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
		    				<?php 
								if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
									$imagemUrlArray = array(
								        'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '130x130'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
									$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
							<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb" alt="<?php echo $imageTitle ?>"  />
							<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>

		                    <span class="o-news_category">
		                    	<?php 
			             		//se tem chapey
			             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
			             			echo $n['NoticiumDetail']['chapeu'];
			             		}else{
			             			echo $this->Mswi->categoryName($n['Node']['terms']);
			             		}; 
			             		?>
		                    </span>
		                    <span class="o-news_title">
		                    	<?php 
								//se tem titulo da capa
								if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
								  echo $n['NoticiumDetail']['titulo_capa'];
								}else{
								  echo $n['Node']['title'];
								}
								?>
		                    </span>
		                </a>
		            </div>
		            <?php if ($c >= 5): 
			            $n = $nodesList[4]; ?>
		    			<div class="o-news">
			    			<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
			                	<?php 
									if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
										$imagemUrlArray = array(
									        'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n['Multiattach']['filename'],
											'dimension'		=> '130x130'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
								<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb" alt="<?php echo $imageTitle ?>"  />
								<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>
			                    <span class="o-news_category">
			                    	<?php 
				             		//se tem chapey
				             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
				             			echo $n['NoticiumDetail']['chapeu'];
				             		}else{
				             			echo $this->Mswi->categoryName($n['Node']['terms']);
				             		}; 
				             		?>
			                    </span>
			                    <span class="o-news_title">
			                    	<?php 
									//se tem titulo da capa
									if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									  echo $n['NoticiumDetail']['titulo_capa'];
									}else{
									  echo $n['Node']['title'];
									}
									?>
			                    </span>
			                </a>
			            </div>

		            <?php endif; ?>
	    		</div>
	    		<?php if($c > 2): ?>
	    		<div class="col-md-6 col-lg-5 o-layout_c">
	    			<?php foreach($nodesList as $i => $n): 
	    				if($i == 0 || $i == 1 || $i == 4) continue;
		    			?>
		    			<div class="o-news">
			    			<a href="<?php echo $n['Node']['path']; ?>" class="o-news_content">
			    				<?php 
			    					if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])):
										$imagemUrlArray = array(
									        'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n['Multiattach']['filename'],
											'dimension'		=> '66x66'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
								<img src="<?php echo $imagemUrl; ?>" class="o-news_thumb o-news_thumb--small" alt="<?php echo $imageTitle ?>"  />
								<?php endif; // isset($n['Multiattach']) && !empty($n['Multiattach']['filename']) ?>
			                	
			                    <span class="o-news_category">
			                    	<?php 
				             		//se tem chapey
				             		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
				             			echo $n['NoticiumDetail']['chapeu'];
				             		}else{
				             			echo $this->Mswi->categoryName($n['Node']['terms']);
				             		}; 
				             		?>
			                    </span>
			                    <span class="o-news_title">
			                    	<?php 
									//se tem titulo da capa
									if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
									  echo $n['NoticiumDetail']['titulo_capa'];
									}else{
									  echo $n['Node']['title'];
									}
									?>
			                    </span>
			                </a>
			            </div>
	    			<?php endforeach; ?>
	    		</div>
	    		<?php endif; ?>
	    	</div>
	    <?php endif; // count($nodesList) >= 2 ?>
	</div>
<?php } ?>