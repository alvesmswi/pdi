<?php
//se tem algum registro
if (!empty($nodesList)) {
	//array que vai guardar as noticias em destaques
	$arrayDestaques = array();
	$contador = 0;
	$maximo = 5;
	//percorre os nodes
	foreach($nodesList as $key => $nd){
		//pega a ordenação
		$ordem = $nd['Node']['ordem'];
		//se ainda não pasosu por este item
		if(!isset($arrayDestaques[$ordem])){
			//se tem ordenação
			$arrayDestaques[$ordem] = $nodesList[$key];
			$contador++;
		}
		//se chegou no maximo
		if($contador == $maximo){
			break;
		}
	}
	//substitui os dados selecionados
	$nodesList = array_values($arrayDestaques); ?>

	<div class="region region--mosaic">
		<div class="content--wrapper">
			<div class="mosaic--wrapper">
				<?php foreach($nodesList as $i => $n) { ?>
					<?php 
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> '600largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8'); 
					?>
					<?php if ($i == 0) { ?>
					<div class="mosaic--block">
						<div class="mosaic--news mosaic--news__main" style="background-image: url(<?php echo $imagemUrl; ?>)">
							<div class="mosaic--headline">
								<?php if (!empty($n['Editoria'])): ?>
									<a href="/ultimas-noticias/<?php echo $n['Editoria']['slug'] ?>" class="headline--category"><i class="icons icons--tags"></i> 
										<?php 
											//se tem chapeu
											if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
												echo $n['NoticiumDetail']['chapeu'];
											}else{
												echo $n['Editoria']['title'];
											}; 
										?>
									</a>
								<?php endif; ?>
								<a href="<?php echo $n['Node']['path']; ?>" class="headline--title">
									<h1>
										<?php 
											//se tem titulo da capa
											if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
												echo $n['NoticiumDetail']['titulo_capa'];
											}else{
												echo $n['Node']['title'];
											}
										?>
									</h1>
								</a>
								<p class="headline--excerpt">
									<?php echo $n['Node']['excerpt']; ?>
								</p>
							</div>
						</div>
					</div>
					<?php } else { 
						if ($i == 1) { ?>
							<div class="mosaic--collection">
						<?php } ?>
						<div class="mosaic--block">
							<div class="mosaic--news" style="background-image: url(<?php echo $imagemUrl; ?>)">
								<div class="mosaic--headline">
									<?php if (!empty($n['Editoria'])): ?>
										<a href="/ultimas-noticias/<?php echo $n['Editoria']['slug'] ?>" class="headline--category"><i class="icons icons--tags"></i> 
											<?php 
												//se tem chapey
												if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
													echo $n['NoticiumDetail']['chapeu'];
												}else{
													echo $n['Editoria']['title'];
												}; 
											?>
										</a>
									<?php endif; ?>
									<a href="<?php echo $n['Node']['path']; ?>" class="headline--title">
										<h2>
											<?php 
												//se tem titulo da capa
												if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
													echo $n['NoticiumDetail']['titulo_capa'];
												}else{
													echo $n['Node']['title'];
												}
											?>
										</h2>
									</a>
									<p class="headline--excerpt">
									</p>
								</div>
							</div>
						</div>
						<?php if ($i == 4) { ?>
							</div>
					<?php }} ?>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>