<?php
if(!empty($nodesList)){ ?>
	<div class="col-sm-6">
		<div class="news--collection">
			<header class="news--header">
				<a href="/ultimas-noticias/<?php echo $nodesList[0]['Editoria']['slug']; ?>" class="news--header__title">
					<?php 
						if ($block['Block']['show_title']) {
							echo $block['Block']['title'];
						} else {
							echo $nodesList[0]['Editoria']['title'];
						}
					?>
				</a>
			</header>

			<?php
				foreach($nodesList as $n) { ?> 
				<div class="news--block">
					<?php 
					//se tem imagem
					if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
						$imagemUrlArray = array(
					        'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> '400x225'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
						?>
						<a href="<?php echo $n['Node']['path']; ?>">
							<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" alt="<?php echo $imageTitle ?>" class="img--responsive news--headline__image" />
						</a>
             		<?php } ?>
					<div class="news--headline">
						<div class="news--metadata" style="display:none;">
							<a href="<?php echo $n['Node']['path']; ?>" class="headline--category"><i class="icons icons--tags"></i> 
								<?php 
								//se tem chapey
								if (isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])) {
									echo $n['NoticiumDetail']['chapeu'];
								} else {
									echo $n['Editoria']['title'];
								}
								?>
							</a>
						</div>
						<a href="<?php echo $n['Node']['path']; ?>" class="headline--title">
							<h2>
								<?php 
									//se tem titulo da capa
									if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
										echo $n['NoticiumDetail']['titulo_capa'];
									}else{
										echo $n['Node']['title'];
									}
								?>
							</h2>
						</a>
						<p class="headline--excerpt">
							<?php echo $n['Node']['excerpt']; ?>
						</p>
					</div>
				</div>
			
			<?php
			break;
			}
			?>
		</div>
	</div>
<?php } ?>