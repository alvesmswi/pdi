<?php 
//Se tiver mais de uma página
if($this->Paginator->params()['pageCount'] > 1){ ?>
	<div class="o-pagination">
       <nav aria-label="Navegação">
          <ul class="o-pagination_list">
				<li class="o-pagination_item">
					<?php 
					echo $this->Paginator->prev(__d('croogo', '<<'),array('class' => 'o-pagination_link')); ?>
				</li>
				<li class="o-pagination_item">
					<?php echo $this->Paginator->numbers(array('class' => 'o-pagination_link', 'separator'=>null)); ?>
				</li>
				<li class="o-pagination_item">	
					<?php echo $this->Paginator->next(__d('croogo', '>>'),array('class' => 'o-pagination_link')); ?>
				</li>
	      	</ul>
		</nav>
	</div>
<?php } ?>