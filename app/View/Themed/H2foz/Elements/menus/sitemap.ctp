<?php if (!empty($menu['threaded'])) { ?>
    <div class="region region--sitemap">
        <div class="content--wrapper">
            <?php foreach ($menu['threaded'] as $item) { ?>
                <div class="col-xs-2">
                    <ul class="sitemap--list">
                        <li class="sitemap--header">
                            <span><?php echo $item['Link']['title'] ?></span>
                        </li>

                        <?php if (!empty($item['children'])) { ?>
                            <?php foreach ($item['children'] as $child) { ?>
                                <?php $link = $this->Mswi->linkToUrl($child['Link']['link']); ?>
                                <li>
                                    <a href="<?php echo $this->Html->url($link) ?>"><?php echo $child['Link']['title'] ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>