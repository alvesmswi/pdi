<?php
//Se tem itens no menu
if(!empty($menu['threaded'])){ ?>
	<nav class="nav--wrapper">
		<ul class="nav--list">
			<?php foreach($menu['threaded'] as $item){ ?>
				<li>
					<?php echo $this->Html->link($item['Link']['title'], $this->Mswi->linkToUrl($item['Link']['link']), array('class' => 'nav--button')); ?>
					<?php if (!empty($item['children'])) { ?>
						<div class="nav--dropdown">
							<ul class="nav--dropdown__list">
								<?php foreach ($item['children'] as $child) { ?>
									<li>
										<?php echo $this->Html->link($child['Link']['title'], $this->Mswi->linkToUrl($child['Link']['link']), array('class' => 'c-navigation_button ' . $child['Link']['class'])); ?>
									</li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</li>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>