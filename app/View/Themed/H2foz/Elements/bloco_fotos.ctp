<?php if(!empty($nodesList)): ?>
<div class="row" data-slicked="slider">
    <?php foreach($nodesList as $n): ?>
        <div class="col-sm-6 col-md-3" data-slicked="slide">
            <?php
                $imagemUrl = null;
                if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
                    if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
                        $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
                    }

                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $n['Multiattach']['filename'],
                        'dimension'		=> '600x600'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                }
            ?>
            <div class="media--block media--block__photo" style="background-image: url(<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>)">
                <a href="<?php echo $n['Node']['path']; ?>" class="media--headline"><span class="media--headline__title">
                    <?php echo $n['Node']['title']; ?>
                </span></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<a href="/ultimas-noticias/galerias-de-fotos" class="media--link__archive">Ver mais fotos</a>
<?php endif; ?>
