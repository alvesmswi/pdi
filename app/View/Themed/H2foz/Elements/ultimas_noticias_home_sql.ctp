<?php
//se tem algum registro
if(!empty($nodesList)){ ?>
	<section class="s-archive u-spacer_bottom">
        <header class="o-header"><span class="o-header_title">Últimas Notícias</span></header>
        <div class="o-archive">
			<?php foreach($nodesList as $n){ ?>
               <a href="<?php echo $n['Node']['path']; ?>" class="o-archive_item">
                  <time datetime="<?php echo $n['Node']['publish_start']?>" class="o-archive_datetime">
                  	<span class="o-archive_time"><?php echo date('H:i', strtotime($n['Node']['publish_start']))?></span> 
                  	<span class="o-archive_date"><?php echo date('d/m', strtotime($n['Node']['publish_start']))?></span>
                  </time>
                  <div class="o-archive_content">
                  	<span class="o-archive_category"><?php echo $this->Mswi->categoryName($n['Node']['terms']); ?></span> 
                  	<span class="o-archive_title">
                  		<?php echo $n['Node']['title']; ?>
                  	</span>
                  </div>
               </a>
           <?php } ?>
           <a href="/noticia" class="o-btn o-archive_btn">
           	Veja mais notícias
           </a>
        </div>
     </section>
<?php } ?>