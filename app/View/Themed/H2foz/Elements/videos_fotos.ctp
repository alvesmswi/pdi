<div class="region region--media">
	<div class="content--wrapper">
		<ul class="tabs--wrapper media--block__tabs" role="tablist">
			<li role="presentation" class="is--active">
				<a href="#media-block-videos" class="tabs--tab media--block__tab " data-toggle="tab">Vídeos</a>
			</li>
			<li role="presentation">
				<a href="#media-block-fotos" class="tabs--tab media--block__tab" data-toggle="tab">Fotos</a>
			</li>
			<li role="presentation">
				<a href="https://www.facebook.com/h2foz/publishing_tools/?section=VIDEOS&filtering[0][field]=live_status&filtering[0][operator]=EQUAL&filtering[0][value]=was_live_broadcast&sort[0]=created_time_descending" target="_blank" class="tabs--tab media--block__tab">LIVE's</a>
			</li>
		</ul>
		<div class="tabs--content media--block__content">

			<div id="media-block-videos" class="tabs--strip media--block__strip is--active" data-slicked="wrapper">
				<?php echo $this->Regions->block('bloco_videos'); ?>
			</div>
			
			<div id="media-block-fotos" class="tabs--strip media--block__strip"  data-slicked="wrapper">
				<?php echo $this->Regions->block('bloco_fotos'); ?>
			</div>
			
		</div>
	</div>
</div>