<?php if (!empty($nodesList)) { ?>
	<div class="o-widget">
	  <header class="o-header"><a href="/" class="o-header_title o-header_title--medium">Mais lidas</a></header>
	  <?php foreach($nodesList as $n){ ?>
		  <div class="o-news">
		  	<a href="<?php echo $n['Node']['path'] ?>" class="o-news_content">
		  		<span class="o-news_category">
		  			<?php 
	         		//se tem chapey
	         		if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
	         			echo $n['NoticiumDetail']['chapeu'];
	         		}else{
	         			echo $this->Mswi->categoryName($n['Node']['terms']);
	         		}; 
	         		?>
		  		</span>
		  		<span class="o-news_title"><?php echo $n['Node']['title'] ?></span>
		  	</a>
		  </div>
	  <?php } ?>
	</div>
<?php } ?>