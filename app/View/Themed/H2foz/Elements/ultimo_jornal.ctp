<?php
//se tem algum registro
if(!empty($nodesList)){
    foreach($nodesList as $n){ ?>
    <section class="s-list u-spacer_bottom">
        <header class="news--header">
            <span class="news--header__title">Edição impressa</span>
        </header>
        <?php
        $imagemUrl      = 'http://placehold.it/195x314';
        $linkpublish    = $n['Node']['path'];
        //se tem attach
        if (isset($n['Multiattach']) && !empty($n['Multiattach'])) {
            $imagemUrlArray = array(
                'plugin'        => 'Multiattach',
                'controller'    => 'Multiattach',
                'action'        => 'displayFile', 
                'admin'         => false,
                'filename'      => $n['Multiattach']['filename'],
                'dimension'     => '300largura'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
        }
        ?>
        <a href="<?php echo $linkpublish ?>" class="o-news_content">
            <img src="<?php echo $imagemUrl; ?>" title="<?php echo $n['Node']['title'];?>" alt="<?php echo $n['Node']['title'];?>">
            <span class="atrativos--headline__title"><?php echo $n['Node']['title'];?></span> 
        </a>
    </section>
<?php } 
}?>