<?php if (!empty($nodesList)) { ?>
    <div class="region region--atrativos">
        <div class="content--wrapper">
            <header class="default--header atrativos--header">
                <?php 
                    if ($block['Block']['show_title']) {
                        echo $block['Block']['title'];
                    } else {
                        echo 'Atrativos';
                    }
                ?>
            </header>
            <div class="atrativos--collection">
                <?php foreach ($nodesList as $i => $node) { 
                    $block = ($i > 0 && $i < 5) ? 'block__small' : 'block__large';
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
                        'dimension'		=> '500x225'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray); ?>
                    <a href="<?php echo $node['Node']['path'] ?>" class="atrativos--block atrativos--<?= $block ?>" style="background-image: url(<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>)">
                        <span class="atrativos--headline__title"><?php echo $node['Node']['title'] ?></span>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
