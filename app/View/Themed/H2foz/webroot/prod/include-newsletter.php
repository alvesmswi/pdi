<div class="region region--subscribe">
	<div class="content--wrapper">
		<div class="subscribe--wrapper">
			<form class="subscribe--form">
				<label class="subscribe--label" for="subscribe">Receba as últimas novidades no seu e-mail:</label>
				<div class="subscribe--input__wrapper">
					<div class="input--group input--group__merge">
						<input id="subscribe" type="email" class="subscribe--input input--field input--field__email" name="email" placeholder="Seu e-mail" />
						<span class="input--group__btn">
							<button type="submit" class="btn btn-default subscribe--submit">
								<i class="icons icons--share"></i> Quero Receber
							</button> </span>
					</div>
					<!-- <small class="subscribe--small">Respeitamos sua privacidade: jamais enviaremos spam.</small> -->
				</div>
			</form>
		</div>
	</div>
</div>