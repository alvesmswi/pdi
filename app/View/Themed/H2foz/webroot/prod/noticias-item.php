<div class="region region--split">
  <div class="content--wrapper">
    <div class="row">
      <div class="region--split__main">
        <div class="region region--article">
          <ol class="article--breadcrumbs">
            <li><a href="/">Home</a></li>
            <li><a href="/pt/noticias">Notícias</a></li>
            <? for($i=0; $i<(sizeof($migalhas)-1); $i++) { ?>
            	<li><a href="<?=$migalhas[$i]['link'];?>"><?=$migalhas[$i]['tit'];?></a></li>
            <? } ?>
          </ol>
          <header class="article--header">
            <div class="article--metadata">
              <? if (isset($item['subcategoria_id']) && isset($item['subcategoria'])) { ?><a href="<?=$item['subcategoria']['link'];?>" class="headline--category"><i class="icons icons--tags"></i> <?=$item['subcategoria']['tit'];?></a>
              <? } elseif (isset($item['categoria_id']) && isset($item['categoria'])) { ?><a href="<?=$item['categoria']['link'];?>" class="headline--category"><i class="icons icons--tags"></i> <?=$item['categoria']['tit'];?></a><? } ?>

              <time datetime="<?= date("Y-m-d\TH:i:s", strtotime($item['datainicio'])); ?>" class="headline--datetime"><i class="icons icons--calendar"></i> <?= date('d/m/Y', strtotime($item['datainicio'])); ?></time>
              <div class="headline--fontes">
                <button type="button" class="fontes--btn" data-fonte="1" data-rel="#article">A&plus;</button>
                <button type="button" class="fontes--btn" data-fonte="-1" data-rel="#article">A&minus;</button>
              </div>
            </div>
            <h1 class="article--title">
              <a class="headline--title"><?= $item['tit'] ?></a>
            </h1>
            <div class="share-this">
              <div class="addthis_inline_share_toolbox"></div>
            </div>
          </header>
          <article class="article--block" id="article">
          	<? if(trim($item['img2'])) { ?>
	            <figure>
	              <a href="/upload/image/noticia/<?= $item['img2'] ?>" data-lightbox>
	                <img src="/upload/image/noticia/<?= $item['img2'] ?>" alt="<?= $item['tit'] ?>" />
	              </a>
	              <?php if(!empty($item['legenda']) || !empty($item['credito'])): ?>
	              <figcaption><?= $item['legenda'].((!empty($item['credito'])) ? ' - '.$item['credito'] : null) ?></figcaption>
	              <?php endif ?>
	            </figure>
	        <? } ?>

            <?php if (!empty($item['relacionadas'])) { ?>
                <ul class="article--relacionadas">
                    <?php foreach ($item['relacionadas'] as $key => $relacionada) { ?>
                        <li><a href="<?= $relacionada['link'] ?>"><?= $relacionada['tit'] ?></a></li>
                    <?php } ?>
                  <!-- <li><div class="banner--block banner--block__300"><img src="http://placehold.it/300x280" /></div></li> -->
                </ul>
            <?php } ?>

            <?= $item['txt2'] ?>

            <?php
            if (!empty($item['galeria'])) { ?>
                <div class="article--carousel">
                  <div class="carousel carousel-for" data-for="#carouselNav1" id="carouselFor1" data-carousel>
                      <?php foreach ($item['galeria'] as $key => $galeria) { ?>
                          <div class="carousel--slide">
                            <figure>
                              <a href="/upload/image/noticia/galeria/<?= $galeria['img1'] ?>">
                                  <img src="/upload/image/noticia/galeria/<?= $galeria['img1'] ?>" alt="<?= $galeria['tit'] ?>" />
                              </a>
                              <figcaption><?= $galeria['tit'].((!empty($galeria['credito'])) ? ' - '.$galeria['credito'] : null) ?></figcaption>
                            </figure>
                          </div>
                      <?php } ?>
                  </div>

                  <div class="carousel carousel-nav" data-for="#carouselFor1" id="carouselNav1" data-carousel>
                      <?php foreach ($item['galeria'] as $key => $galeria) { ?>
                        <div class="carousel--slide">
                          <img src="/upload/image/noticia/galeria/tb/<?= $galeria['img1'] ?>" alt="<?= $galeria['tit'] ?>" />
                        </div>
                      <?php } ?>
                  </div>
                </div>
            <?php } ?>

            <div class="share-this" style="padding-top:50px;">
              <div class="addthis_inline_share_toolbox"></div>
            </div>

            <div class="fb-comments" data-href="<?= $ogurl ?>" data-width="100%" data-numposts="5"></div>

            <!-- BANNER h728 -->
            <div style="padding-top: 25px;">
            <?php
            if (!empty($banners['banners']['h800'])):
              $h800 = $banners['banners']['h800']; ?>
              <div class="banner--block banner--block__728">
                  <a href="<?= $h800[0]['link'] ?>" target="<?= $h800[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/800/<?= $h800[0]['img1'] ?>" alt="<?= $h800[0]['tit'] ?>" /></a>
              </div>
            <?php elseif (!empty($banners['banners']['h728'])): 
              $h728 = $banners['banners']['h728'];
            ?>
              <div class="banner--block banner--block__728">
                  <a href="<?= $h728[0]['link'] ?>" target="<?= $h728[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/728/<?= $h728[0]['img1'] ?>" alt="<?= $h728[0]['tit'] ?>" /></a>
              </div>
            <?php endif; ?>
            </div>
            <!-- / BANNER h728 -->

            <div style="padding-top: 25px;">
            	<div class="addthis_relatedposts_inline"></div>
            </div>

          </article>
        </div>
      </div>

      <?php include_once 'include-sidebar.php'; ?>
    </div>
  </div>
</div>
