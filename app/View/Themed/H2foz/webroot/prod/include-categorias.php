<?php
foreach ($categorias as $key => $categoria) {
?>

<div class="col-sm-6">
	<div class="news--collection">
		<header class="news--header">
			<a href="<?= $categoria['link'] ?>" class="news--header__title"><?= $categoria['tit'] ?></a>
		</header>
		<?php
		
			foreach ($categoria['Noticias'] as $key => $noticia) { if(!is_array($noticia)) continue; ?> 
			<div class="news--block">
				<? if (trim($noticia['img1'])) { ?>
					<a href="<?= $noticia['link'] ?>">
						<img src="/upload/image/post/tb2/<?= $noticia['img1'] ?>" alt="<?= $noticia['tit'] ?>" class="img--responsive news--headline__image" />
					</a>
				<? } ?>
				<div class="news--headline">
					<div class="news--metadata" style="display:none;">
						<a href="<?= $categoria['link'] ?>" class="headline--category"><i class="icons icons--tags"></i> <?= $categoria['tit'] ?></a>
						<!--
						<time datetime="<?= date("Y-m-d\TH:i:s", strtotime($noticia['datainicio'])); ?>" class="headline--datetime">
							<i class="icons icons--calendar"></i> <?= date('d-m-Y', strtotime($noticia['datainicio'])) ?>
						</time>
						-->
					</div>
					<a href="<?= $noticia['link'] ?>" class="headline--title">
						<h2><?= $noticia['tit'] ?></h2>
					</a>
					<p class="headline--excerpt">
						<?= $noticia['txt'] ?>
					</p>
				</div>
			</div>
		
		<?php
		break;
		}
		?>
	</div>
</div>

<?php
}