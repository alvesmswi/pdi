<?php include_once 'include-destaque.php'; ?>

<?php include 'include-fullbanner.php'; ?>

<div class="region region--split">
	<div class="content--wrapper">
		<div class="row">
			<div class="region--split__main">
				<div class="region region--news">
					<div class="row">
						<?php include_once 'include-categorias.php' ?>
					</div>
				</div>
				
				<!-- BANNER h728 -->
                <?php
                if (!empty($banners['banners']['h800'])):
                	$h800 = $banners['banners']['h800']; ?>
	                <div class="banner--block banner--block__728">
	                    <a href="<?= $h800[0]['link'] ?>" target="<?= $h800[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/800/<?= $h800[0]['img1'] ?>" alt="<?= $h800[0]['tit'] ?>" /></a>
	                </div>
                <?php elseif (!empty($banners['banners']['h728'])): 
                	$h728 = $banners['banners']['h728'];
                ?>
	                <div class="banner--block banner--block__728">
	                    <a href="<?= $h728[0]['link'] ?>" target="<?= $h728[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/728/<?= $h728[0]['img1'] ?>" alt="<?= $h728[0]['tit'] ?>" /></a>
	                </div>
				<?php endif; ?>
				<!-- / BANNER h728 -->
				
			</div>
			<?php include_once 'include-sidebar.php'; ?>
		</div>
	</div>
</div>

<?php include_once 'include-videos-fotos.php'; ?>

<?php if (!empty($atrativos)) { ?>
	<div class="region region--atrativos">
		<div class="content--wrapper">
			<header class="default--header atrativos--header">
				Atrativos
			</header>
			<div class="atrativos--collection">
			    <?php foreach ($atrativos as $key => $atrativo) {  
			        $block = ($key > 0 && $key < 5) ? 'block__small' : 'block__large' ; ?>
			        <a href="<?= $atrativo['link'] ?>" class="atrativos--block atrativos--<?= $block ?>" style="background-image: url(/upload/image/post/tb2/<?= $atrativo['img1'] ?>)">
	                    <span class="atrativos--headline__title"><?= $atrativo['tit'] ?></span>
	                </a>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>