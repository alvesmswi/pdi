<?php
$manchete_ativa = null;
?>
<div class="region region--mosaic">
	<div class="content--wrapper">
		<div class="mosaic--wrapper">
			<?php
			foreach ($manchete as $m) { $manchete_ativa = $m['noticia_id']; 
			?>
			<div class="mosaic--block">
				<div class="mosaic--news mosaic--news__main" style="background-image: url(/upload/image/noticia/<?= $m['img1'] ?>)">
					<div class="mosaic--headline">
						<a href="<?=$m['categoria']['link'];?>" class="headline--category"><i class="icons icons--tags"></i> <?= $m['categoria']['tit'] ?></a>
						<a href="<?= $m['link'] ?>" class="headline--title">
							<h1><?= $m['tit'] ?></h1>
						</a>
						<p class="headline--excerpt">
							<?= $m['txt'] ?>
						</p>
					</div>
				</div>
			</div>
			<?php
			}
			?>
			<div class="mosaic--collection">
				<?php
				for ($i=0; $i <= 4; $i++) {
					if (!empty($destaques[$i]) && $manchete_ativa != $destaques[$i]['noticia_id']) { // Se não for a manchete ativa: ?>
					<div class="mosaic--block">
						<div class="mosaic--news" style="background-image: url(/upload/image/noticia/<?= $destaques[$i]['img1'] ?>)">
							<div class="mosaic--headline">
								<a href="<?=$destaques[$i]['categoria']['link'] ?>" class="headline--category"><i class="icons icons--tags"></i> <?= $destaques[$i]['categoria']['tit'] ?></a>
								<a href="<?= $destaques[$i]['link'] ?>" class="headline--title">
									<h2><?= $destaques[$i]['tit'] ?></h2>
								</a>
								<p class="headline--excerpt">
									<?= $destaques[$i]['txt'] ?>
								</p>
							</div>
						</div>
					</div>
					<?php
					}
					
				}
				?>
			</div>
		</div>
	</div>
</div>