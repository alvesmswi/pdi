<footer class="region region--footer">
	<?php include 'include-newsletter.php'; ?>
	
	<div class="region region--sitemap">
		<div class="content--wrapper">
			<?php
			foreach ($menu['bottom'] as $key => $m) { ?>
			<div class="col-xs-2">
				<ul class="sitemap--list">
					<li class="sitemap--header">
						<span><?= $m['tit'] ?></span>
					</li>
					<?php
					foreach ($m['submenu'] as $key => $s) { ?>
					<li>
						<a href="<?= $s['link'] ?>"><?= $s['tit'] ?></a>
					</li>
					<?php	
					}
					?>
				</ul>
			</div>
			<?php	
			}
			?>
		</div>
	</div>
	<div class="region region--copyright">
		<div class="content--wrapper">
			&copy; 2003-<?= date('Y') ?> Portal H2FOZ - Todos os direitos reservados.
		</div>
	</div>
</footer>
</div>

	<script type="text/javascript" src="/dist/js/production.js"></script>
	<script>
		function assinarNewsletter(Event) {
		    Event.preventDefault();
		    var form = this;
		    var data = form.querySelector('.subscribe--input').value;
		    
		    $.ajax({
		        type: 'POST',
		        url: '/newsletter/enviar',
		        data: {email: data},
		        success: function(data){
		           console.log(data);
		        },
		    });
		}
		Array.prototype.forEach.call(document.querySelectorAll('.subscribe--form'), function(el) { el.addEventListener('submit', assinarNewsletter)})
	</script>

	<!-- <script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "96502e02-033f-4f29-9c2e-58177ee6568a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script> -->

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=h2foz"></script> 

	<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-10205417-1', 'auto');
        ga('send', 'pageview');
     </script>
</body>
</html>