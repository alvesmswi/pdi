<aside class="region--split__aside">
    <?php if (isset($item['confira']) && !empty($item['confira'])) { ?>
    <div class="guia--sugestoes">
      <header class="guia--sugestoes__header">
        <h4>Outros <span>Eventos</span></h4>
      </header>
      <?php foreach ($item['confira'] as $key => $confira) { ?>
          <a href="<?= $confira['link'] ?>" class="guia--sugestoes__block">
            <?= ((!empty($confira['img'])) ? '<img src="/upload/image/evento/tb/'.$confira['img'].'" />' : null ) ?>
            <div class="guia--sugestoes__data">
              <span class="guia--sugestoes__title"><?= $confira['tit'] ?></span>
            </div>
          </a>
      <?php } ?>
    </div>
    <?php } ?>
    
    <!-- BANNERS l300 -->
    <?php
    if (!empty($banners['banners']['l300'])) { 
    foreach ($banners['banners']['l300'] as $key => $banner) { ?>
    <div class="banner--block banner--block__300">
        <a href="<?= $banner['link'] ?>" target="<?= $banner['link_target'] ?>" rel="nofollow">
            <img src="/upload/image/banner/300/<?= $banner['img1'] ?>" alt="<?= $banner['tit'] ?>" />
        </a>
    </div>
    <?php   
    }}
    ?>
</aside>