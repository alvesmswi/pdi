<div class="region region--media">
	<div class="content--wrapper">
		<ul class="tabs--wrapper media--block__tabs" role="tablist">
			<li role="presentation" class="is--active">
				<a href="#media-block-videos" class="tabs--tab media--block__tab " data-toggle="tab">Vídeos</a>
			</li>
			<li role="presentation">
				<a href="#media-block-fotos" class="tabs--tab media--block__tab" data-toggle="tab">Fotos</a>
			</li>
		</ul>
		<div class="tabs--content media--block__content">
			<div id="media-block-videos" class="tabs--strip media--block__strip is--active" data-slicked="wrapper">
				<div class="row" data-slicked="slider">
					<?php
					foreach ($videos as $video) { 
					?>
					<div class="col-sm-6 col-md-3" data-slicked="slide">
						<div class="media--block media--block__video" style="background-image: url(/upload/image/video/<?= $video['img2'] ?>)">
							<a href="<?= $video['link'] ?>" class="media--headline"><span class="media--headline__title"><?= $video['tit'] ?></span></a>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				<a href="/<?php echo $this->idioma; ?>/videos" class="media--link__archive">Ver mais vídeos</a>
			</div>
			
			<div id="media-block-fotos" class="tabs--strip media--block__strip"  data-slicked="wrapper">
				<div class="row" data-slicked="slider">
					<?php
					foreach ($fotos as $foto) {
					?>
					<div class="col-sm-6 col-md-3" data-slicked="slide">
						<div class="media--block media--block__photo" style="background-image: url(/upload/image/foto/<?= $foto['img2'] ?>)">
							<a href="<?= $foto['link'] ?>" class="media--headline"><span class="media--headline__title"><?= $foto['tit'] ?></span></a>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				<a href="/<?php echo $this->idioma; ?>/fotos/" class="media--link__archive">Ver mais fotos</a>
			</div>
			
		</div>
	</div>
</div>