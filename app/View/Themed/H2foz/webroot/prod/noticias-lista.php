<div class="region region--split">
  <div class="content--wrapper">
    <div class="row">
      <div class="region--split__main">
        <div class="region region--atrativos">
          <header class="default--header atrativos--header"><?=($titulo_secao)?$titulo_secao:'Notícias';?></header>
          <div class="atrativos--collection">
              <?php for ($i=0; isset($lista[$i]) && $i <= 5 && $pagn == 1; $i++) { 
                  if ($lista[$i]['destaque'] && trim($lista[$i]['img1'])) { ?>
                      <a href="<?= $lista[$i]['link'] ?>" class="atrativos--block atrativos--block__large" style="background-image: url(<?= (!empty($lista[$i]['img1'])) ? '/upload/image/noticia/'.$lista[$i]['img1'] : null; ?>)"><span class="atrativos--headline__title"><?= $lista[$i]['tit'] ?></span></a>
              <?php unset($lista[$i]); } } ?>
          </div>
        </div>
        
        <!-- BANNER h728 -->
        <?php if (!empty($banners['banners']['h800'])):
          $h800 = $banners['banners']['h800']; ?>
          <div class="banner--block banner--block__728">
              <a href="<?= $h800[0]['link'] ?>" target="<?= $h800[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/800/<?= $h800[0]['img1'] ?>" alt="<?= $h800[0]['tit'] ?>" /></a>
          </div>
        <?php elseif (!empty($banners['banners']['h728'])): 
          $h728 = $banners['banners']['h728'];
        ?>
          <div class="banner--block banner--block__728">
              <a href="<?= $h728[0]['link'] ?>" target="<?= $h728[0]['link_target'] ?>" rel="nofollow"><img src="/upload/image/banner/728/<?= $h728[0]['img1'] ?>" alt="<?= $h728[0]['tit'] ?>" /></a>
          </div>
        <?php endif; ?>
        <!-- / BANNER h728 -->
        
        <div class="region region--lista">
            <?php foreach ($lista as $key => $noticia) { 
                $image = (!empty($noticia['img1'])) ? true : false; ?>
                <div class="news--block">
                    <?php if ($image) { ?>
                      <a href="<?=$noticia['link'];?>" class="headline--category">
                        <img src="/upload/image/noticia/tb2/<?= $noticia['img1'] ?>" alt="<?= $noticia['tit'] ?>" class="img--responsive news--headline__image" />
                      </a>
                    <?php } ?>
                    <div class="news--headline">
                      <div class="news--metadata">
                        <a href="<?=$noticia['categoria']['link'];?>" class="headline--category"><i class="icons icons--tags"></i> <?=$noticia['categoria']['tit'] ?></a>
                        <time datetime="<?= date("Y-m-d\TH:i:s", strtotime($noticia['datainicio'])); ?>" class="headline--datetime"><i class="icons icons--calendar"></i> <?= date('d/m/Y', strtotime($noticia['datainicio'])); ?></time>
                      </div>
                      <a href="<?=$noticia['link'];?>" class="headline--title"><?= $noticia['tit'] ?></a>
                      <p class="headline--excerpt"><?=$noticia['txt'];?></p>
                    </div>
                </div>
            <?php } ?>
          
          <? if(isset($pagenext) || isset($pageprev)) { ?>
	          <div class="news--pagination">
	            <? if(isset($pageprev)){ ?><a href="<?=$pageprev;?>" class="news--pagination__btn">‹ página anterior</a><? } ?>
	            <? if(isset($pagenext)){ ?><a href="<?=$pagenext;?>" class="news--pagination__btn">próxima página ›</a><? } ?>
	          </div>
          <? } ?>
          
        </div>
      </div>
     
     <?php include_once 'include-sidebar.php'; ?>
    </div>
  </div>
</div>