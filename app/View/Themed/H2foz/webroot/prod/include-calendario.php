<?php
$array = array(
	"mes" => array(
		"1" => "JAN",
		"2" => "FEV",
		"3" => "MAR",
		"4" => "ABR",
		"5" => "MAI",
		"6" => "JUN",
		"7" => "JUL",
		"8" => "AGO",
		"9" => "SET",
		"10" => "OUT",
		"11" => "NOV",
		"12" => "DEZ"
	)
);
$arrayDiaSemana = array(
	"1" => "SEG",
	"2" => "TER",
	"3" => "QUA",
	"4" => "QUI",
	"5" => "SEX",
	"6" => "SAB",
	"7" => "DOM",
);
?>
<div class="region region--agenda">
	<header class="news--header">
    <a href="/pt/eventos" class="news--header__title news--header__title--large">Agenda</a>
  </header>
  
  <div class="agenda--categorias__list">
  	<? for($i=0; isset($eventos_categorias[$i]); $i++){ ?>
    	<a href="<?=$eventos_categorias[$i]['link'];?>"><?=$eventos_categorias[$i]['tit'];?></a>
    <? } ?>
  </div>
	
  <div id="agendaWrapper">
    <ol id="monthWrapper">
    	<?php
    	foreach ($array['mes'] as $key => $mes) {
    		if ($key == date('n')) { ?>
    		<li class="agenda--month is--active"><button type="button" data-target="#mes-<?= strtolower($mes) ?>" class="agenda--month__btn"><?= $mes ?></button></li>
    		<?php
    		} else { ?>
    		<li class="agenda--month"><button type="button" data-target="#mes-<?= strtolower($mes) ?>" class="agenda--month__btn"><?= $mes ?></button></li>
    		<?php
    		}
		}
    	?>
    </ol>
    <div class="agenda--table">
      <div id="daysWrapper">
        <ol class="agenda--days">
        	<?php
        	foreach ($array['mes'] as $mes_numero => $mes) {
			$dias = cal_days_in_month(CAL_GREGORIAN, $mes_numero, date('Y'));
			?>
			<li class="agenda--marker" id="mes-<?= strtolower($mes) ?>" data-mes="<?= $mes_numero ?>"></li>
			<?php
			if ($mes_numero == 1) {
				$dia_semana_num = date('N', strtotime(date('1-1-Y')));
				$dias_semana = $dia_semana_num - 0;
				for ($i=1; $i <= $dias_semana; $i++) { ?>
					<li class="agenda--days__empty"></li>
				<?php
				}
			}
			for ($i=1; $i <= $dias; $i++) {
				$data = date($i.'-'.$mes_numero.'-Y');
				$dia_semana_num = date('N', strtotime($data));
				if ($mes_numero == date('n')) { ?>
					<li data-mes="<?= $mes_numero ?>" class="agenda--day is--month" data-semana="<?= $arrayDiaSemana[$dia_semana_num] ?>">
						<?php
						if (isset($eventos[$data])) { ?>
							<button type="button" data-target="#<?= $data ?>" class="agenda--day__btn"><?= $i ?></button>
						<?php	
						} else { ?>
							<span class="agenda--day__btn"><?= $i ?></span>
						<?php
						}
						?>
					</li>
				<?php
				} else { ?>
					<li data-mes="<?= $mes_numero ?>" class="agenda--day" data-semana="<?= $arrayDiaSemana[$dia_semana_num] ?>">
						<?php
						if (isset($eventos[$data])) { ?>
							<button type="button" data-target="#<?= $data ?>" class="agenda--day__btn"><?= $i ?></button>
						<?php	
						} else { ?>
							<span class="agenda--day__btn"><?= $i ?></span>
						<?php
						}
						?>
					</li>
				<?php
				} ?>
			<?php
			}
			}
        	?>
        </ol>
      </div>
      <div id="eventsWrapper">
        <div class="agenda--events">
        	<?php
        	foreach ($eventos as $data => $evento) { $mes = date('n', strtotime($data)); $dia = date('j', strtotime($data)); ?>
        	<div class="agenda--events__day" id="<?= $data ?>">
        		<header class="agenda--events__header"><span class="agenda--events__header-month"><?= $array['mes'][$mes] ?></span><span class="agenda--events__header-day"><?= $dia ?></span></header>
	            <?php
	            foreach ($evento as $key => $eve) { ?>
	            <div class="agenda--events__block"><p><a href="<?= $eve['link'] ?>"><?= $eve['tit'] ?></a></p></div>
				<?php
				}
	            ?>
	        </div>
			<?php	
			}
        	?>
        </div>
      </div>
    </div>
  </div>
</div>