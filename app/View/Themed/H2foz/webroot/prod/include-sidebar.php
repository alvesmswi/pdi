<aside class="region--split__aside">
    <div class="region region--social">
      <div class="row">
      	
         
        <div class="addthis_inline_follow_toolbox"></div>
        
      </div>
    </div>
    
    <!-- BANNERS l300 -->
    <?php
    if (!empty($banners['banners']['l300'])) { 
    foreach ($banners['banners']['l300'] as $key => $banner) { ?>
    <div class="banner--block banner--block__300">
        <a href="<?= $banner['link'] ?>" target="<?= $banner['link_target'] ?>" rel="nofollow">
            <img src="/upload/image/banner/300/<?= $banner['img1'] ?>" alt="<?= $banner['tit'] ?>" />
        </a>
    </div>
    <?php   
    }}
    ?>
    
    <!-- ENQUETE -->
    <?php include_once 'include-enquete.php' ?>
</aside>