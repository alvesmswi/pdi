<?php
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title><?=$headtitle;?></title>
		<meta name="title" content="<?=$headtitle;?>">
		<meta name="description" content="<?=$headdesc;?>"/>
		<meta name="keywords" content="<?=$headkeys;?>"/>

		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@H2FOZ" />
		<meta property="og:url" content="<?= $ogurl; ?>" />
		<meta property="og:type" content="<?= $ogtype; ?>"/>
		<meta property="og:title" content="<?= $ogtitle; ?>" />
		<meta property="og:description" content="<?=$ogdescription;?>" />
		<meta property="og:image" content="<?= $ogimage; ?>"/>

		<meta property="fb:app_id" content="321419201624688" />

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/dist/css/main.min.css" media="all" />
	</head>
	<body>
		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
			    FB.init({
			      appId      : '321419201624688',
			      xfbml      : true,
			      version    : 'v2.9'
			    });
			    FB.AppEvents.logPageView();
		  	};

			(function(d, s, id){
			    var js, fjs = d.getElementsByTagName(s)[0];
			    if (d.getElementById(id)) {return;}
			    js = d.createElement(s); js.id = id;
			    js.src = "//connect.facebook.net/pt_BR/sdk.js";
			    fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

		<div class="container-fluid">
			<header class="region region--header">
				<div class="content--wrapper">
					<div class="header--table">
					<div class="logo--wrapper"><a href="/<?= $idioma ?>/"> <img src="/dist/img/Logo H2foz - oficial.svg" alt="H2FOZ"> </a></div>
					<?php
					if (!empty($banners['banners']['t728'])) {
						$items = $banners['banners']['t728'];
						$random = array_rand($items, 1); ?>
						<div class="banner--block banner--shopping banner--block__728">
							<a href="<?= $items[$random]['link'] ?>" target="<?= $items[$random]['link_target'] ?>" rel="nofollow">
								<img src="/upload/image/banner/728/<?= $items[$random]['img1'] ?>" alt="<?= $items[$random]['tit'] ?>" />
							</a>
						</div>
					<?php
					}
					?>
					</div>
				</div>
			</header>
			<div class="region region--nav">
				<div class="content--wrapper">
					<div class="nav--flex">
						<button type="button" id="navToggle" class="nav--toggle">
							<span class="sr-only">Menu</span><i class="icons icons--bars"></i>
						</button>
						<nav class="nav--wrapper">
							<ul class="nav--list">
								<?php
								foreach ($menu['top'] as $key => $m):
								?>
								<li>
									<a href="<?= $m['link'] ?>" target="<?= $m['link_target'] ?>" class="nav--button"><?= $m['tit'] ?></a>
									<?php if(isset($m['submenu'])): ?>
									<div class="nav--dropdown">
										<ul class="nav--dropdown__list">
										<?php
										$menu_count_break = max(6, round(sizeof($m['submenu']) / 3, 0, PHP_ROUND_HALF_DOWN));
										foreach ($m['submenu'] as $i=>$s):
										if($i % $menu_count_break == 0 && $i > 0): ?>
										</ul><ul class="nav--dropdown__list">
										<?php endif; ?>

										<li>
											<a href="<?= $s['link'] ?>" target="<?= $s['link_target'] ?>"><?= $s['tit'] ?></a>
											<?php if (isset($s['subsubmenu']) && !empty($s['subsubmenu'])): ?>
											<div class="nav--dropdown__submenu">
												<ul class="nav--dropdown__sublist">
												<?php
													$submenu_count_break = max(6, round(sizeof($s['subsubmenu']) / 3, 0, PHP_ROUND_HALF_DOWN));
													foreach ($s['subsubmenu'] as $k=>$ss):
													if($k % $submenu_count_break == 0 && $k > 0): ?>
													</ul><ul class="nav--dropdown__sublist">
													<?php endif; ?>

													<li class="nav--dropdown__sublistitem"><a href="<?= $ss['link'] ?>" target="<?= $ss['link_target'] ?>"><?= $ss['tit'] ?></a></li>
												<?php endforeach; ?>
												</ul>
											</div>
											<?php endif; endforeach; ?>
										</li>

										</ul>
									</div>
									<?php endif ?>
								</li>
								<?php endforeach ?>
							</ul>
						</nav>
						<div class="nav--search">
							<form class="search--form" method="GET" action="/pt/busca/lista">
								<div class="input--group input--group__merge">
									<input id="buscakey" type="search" class="search--input input--field input--field__search" name="buscakey" placeholder="Busque" value="<?=$buscakey;?>" />
									<label for="navSearch" class="sr-only">Busca</label>
									<span class="input--group__btn">
										<button type="submit" class="btn btn-default search--submit">
											<i class="icons icons--search"></i><span class="sr-only">Buscar</span>
										</button> </span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
