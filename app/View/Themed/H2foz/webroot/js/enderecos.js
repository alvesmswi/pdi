(function($) {
  $(document).ready(function() {
    $('[data-processa-endereco]').closest('form').on('submit', function(e) {
      if (!e.processed) {
        
        // Processa os endereços
        var $this = $(this);
        var address = $this.find('[data-processa-endereco]').val();
        if ($.trim(address).length) {
          e.preventDefault();
          $.ajax({
            url: 'http://maps.google.com/maps/api/geocode/json',
            data: {
              address: address,
              sensor: false
            }
          }).always(function(data, textStatus, xhr) {
            if (data.length && data.results.length) {
              var latitude = data.results[0].geometry.location.lat;
              var longitude = data.results[0].geometry.location.lng;
              var place_id = data.results[0].place_id;

              if ($('#latitude').length) {
                $('#latitude').val(latitude);
              } else {
                $('<input type="hidden" name="latitude" id="latitude">').val(latitude).appendTo($this);
              }

              if ($('#longitude').length) {
                $('#longitude').val(longitude);
              } else {
                $('<input type="hidden" name="longitude" id="longitude">').val(longitude).appendTo($this);
              }

              if ($('#place_id').length) {
                // ChIJDREcLMPj3JQRWAALzNkBKEQ
                $('#place_id').val(place_id);
              } else {
                $('<input type="hidden" name="place_id" id="place_id">').val(place_id).appendTo($this);
              }
            }
            var event = $.Event('submit', {processed: true});
            return $this.trigger(event);
          });
        }
      };
    });
  });
})(jQuery);