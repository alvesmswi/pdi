<header class="o-header">
	<span class="o-header_title"><?php echo $contact['Contact']['title']; ?></span>
</header>
<div class="o-form_content">
   	<?php 
	echo $this->Form->create('Message', array(
		'url' => array(
			'plugin' => 'contacts',
			'controller' => 'contacts',
			'action' => 'view',
			$contact['Contact']['alias'],
		),
	));

	$this->Form->inputDefaults(array(
		'label' => false,
		'class' => 'form-control',
		'div' => array('class' => 'form-group'),
	));
   	?>
         	
	<?php echo $contact['Contact']['body']; ?>
	
	<div class="form-group">
		<?php echo $this->Form->input('Message.name', array('label'=>'Nome de Usuário', 'class'=>'form-control')); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->input('Message.title', array('label'=>'Assunto', 'class'=>'form-control')); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->input('Message.email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control')); ?>
	</div>
		<div class="form-group">
		<?php echo $this->Form->input('Message.body', array('label'=>'Digite a sua mensagem', 'type'=>'textarea','class'=>'form-control')); ?>
	</div>
	<?php 
	if ($contact['Contact']['message_captcha']):
		echo $this->Recaptcha->display_form();
	endif; 
	?>
	<div class="text-right"><button type="submit" class="btn btn-lg btn-primary">Enviar</button></div>

   <?php echo $this->Form->end();?>   
</div>
<br />
