<?php
$poll = $this->requestAction('/polls/polls/get_poll');
$poll_array = json_decode($poll, true);
if(!empty($poll_array)){
  echo $this->Html->css('Polls.poll'); 
?>
    <div class="poll--wrapper">
        <header class="news--header">
            <span class="news--header__title">Enquete</span>
        </header>
        <div class="poll--block">
            <p class="poll--question">
                <?php echo $poll_array['question'] ?>
            </p>
            
            <?php if($poll_array['graph']){ ?>
                <ul class="poll--list">
                    <?php foreach ($poll_array['answers'] as $key => $option) { ?>
                        <li>
                            <div class="poll--options">
                                <span><?php echo $option['answer'];?></span>
                                <span class="poll--percent"><?php echo $option['percent'];?>%</span>
                            </div>
                            <div class="poll--bar">
                                <div class="poll--bar__loaded" style="width: <?php echo $option['percent'];?>%"></div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="poll--footer">
                    <a href="/polls/polls/index/<?php echo $poll_array['poll_id']; ?>">Resultado Parcial</a>
                </div>
            <?php } else { ?>
                <?php 
                    echo $this->Form->create(
                        null, 
                        array(
                            'class' => 'poll--form', 
                            'method' => 'POST', 
                            'url' => '/polls/polls/submit_poll'
                        )
                    );
                    echo $this->Form->hidden('PollVote.poll_id', array('value' => $poll_array['poll_id'])); 
                ?>
                <div class="poll--radios">
                    <?php
                        //percorre as respostas  
                        foreach ($poll_array['answers'] as $key => $option) { ?>
                            <div class="form--group__radio">
                                <input type="radio" id="optionsRadios<?php echo $key; ?>" value="<?php echo $option['id']; ?>" name="data[PollVote][poll_option_id]" class="form--radio">
                                <label class="form--option" for="optionsRadios<?php echo $key; ?>"><?php echo $option['answer']; ?></label>
                            </div>
                        <?php } ?>
                </div>
                <div class="poll--footer">
                    <button type="submit" class="input--btn btn-default btn-azul">Votar</button>
                </div>
                <?php echo $this->Form->end(); ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>