<div class="region region--atrativos">
	<header class="default--header atrativos--header">
		<?php echo $title_for_layout; ?>
	</header>
	<div class="atrativos--collection">
	</div>
</div>

<div class="region region--lista">
    <?php
        if (!empty($blogs)) {
            $contador = 0;
            $total    = count($blogs);
            $blogs = array_reverse($blogs);
            foreach ($blogs as $blog){ $contador++; ?>
                <div class="news--block">
					<?php if (isset($blog['Blog']['image'])) { ?>
                        <a href="/blog/<?php echo $blog['Blog']['slug']; ?>">
                            <img src="<?php echo $blog['Blog']['image'] ?>" alt="<?php echo $blog['Blog']['title']; ?>" class="img--responsive news--headline__image" style="max-width: 25% !important" />
                        </a>
                    <?php } ?>

					<div class="news--headline">
						<div class="news--metadata">
                            <a href="/blog/<?php echo $blog['Blog']['slug']; ?>" class="headline--category">
                                <i class="icons icons--tags"></i> 
                                <?php echo $blog['Blog']['title']; ?>
                            </a>
						</div>
                        <?php $url = ((isset($blog['Post']['url'])) ? $this->Html->url($blog['Post']['url']) : "/blog/{$blog['Blog']['slug']}/post/{$blog['Post']['slug']}"); ?>
						<?php if (!empty($blog['Post'])) { ?>
						    <a href="<?php echo $url ?>" class="headline--title"><?php echo $blog['Post']['title']; ?></a>
                        <?php } ?>
					</div>
				</div>
            <?php 
            } 
        } else {
            echo __d('croogo', 'Nenhum registro encontrado.');
        } 
    ?>
	
	<?php if(isset($this->request->paging['Node']['prevPage']) || isset($this->request->paging['Node']['nextPage'])) { ?>
		<div class="news--pagination">
			<?php echo $this->Paginator->prev('‹ página anterior', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
			<?php echo $this->Paginator->next('próxima página ›', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
		</div>
	<? } ?>
</div>


<?php #echo $this->element('paginator');?>
