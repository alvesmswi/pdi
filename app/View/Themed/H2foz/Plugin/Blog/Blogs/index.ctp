<?php if (!empty($posts)) { ?>
    <?php if (isset($posts[0]['Blog']['capa'])) { ?>
        <img src="<?php echo $posts[0]['Blog']['capa'] ?>" alt="<?php echo $title_for_layout; ?>" class="img--responsive">
    <? } ?>
    <div class="region region--atrativos">
        <header class="default--header atrativos--header">
            <?php echo $title_for_layout; ?>
        </header>
        <div class="atrativos--collection">
        </div>
    </div>

    <div class="region region--lista">
        <?php foreach ($posts as $post) { ?>
            <div class="news--block">
                <?php 
                    if (isset($post['Images'][0]['filename']) && !empty($post['Images'][0]['filename'])) { 
                        $this->Helpers->load('Medias.MediasImage'); ?>
                <a href="<?php echo $this->Html->url($post['Post']['url']) ?>">
                    <img src="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename'], '400x225'); ?>" alt="<?php echo $post['Images'][0]['alt']; ?>" class="img--responsive news--headline__image" />
                </a>
                <?php } ?>

                <div class="news--headline">
                    <div class="news--metadata">
                        <time datetime="<?= date("Y-m-d\TH:i:s", strtotime($post['Post']['publish_start'])); ?>" class="headline--datetime">
                            <i class="icons icons--calendar"></i> <?php echo date('d/m/Y H:i', strtotime($post['Post']['publish_start'])) ?>
                        </time>
                    </div>
                    <a href="<?php echo $this->Html->url($post['Post']['url']) ?>" class="headline--title"><?php echo $post['Post']['title'] ?></a>
                    <p class="headline--excerpt"><?php echo $post['Post']['excerpt'] ?></p>
                </div>
            </div>
        <?php } ?>
        
        <?php if(isset($this->request->paging['Node']['prevPage']) || isset($this->request->paging['Node']['nextPage'])) { ?>
            <div class="news--pagination">
                <?php 
                $this->Paginator->options['url'] = array('controller' => 'blogs', 'action' => 'view', 'slug' => $this->params['slug']);
                #echo $this->element('paginator');
                ?>
                <?php echo $this->Paginator->prev('‹ página anterior', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
                <?php echo $this->Paginator->next('próxima página ›', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
            </div>
        <? } ?>
    </div>
<?php } ?>