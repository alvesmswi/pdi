<?php
  $this->Html->meta(
    array('property' => 'og:title', 'content' => $post['Post']['title']),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:description', 'content' => (isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt']) ? $post['Post']['excerpt'] : "")),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:url', 'content' => Router::url(null, true)),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('name' => 'twitter:card', 'content' => 'summary'),
    null,
    array('inline'=>false)
  );
  //se tem chamada
  if(isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){
    $description = $post['Post']['excerpt'];
  }else{
    $description = substr($post['Post']['body'],0,128);
    $description = strip_tags($description);
  }
  //gera o description
  $this->Html->meta(
    array(
      'name' => 'description ', 
      'content' => $description
    ),
    null,
    array('inline'=>false)
  );
?>

<div class="region region--article">
	<ol class="article--breadcrumbs">
	<li><a href="/">Home</a></li>
	<li><a href="/blog/<?php echo $post['Blog']['slug']; ?>"><?php echo $post['Blog']['title']; ?></a></li>
	</ol>
	<header class="article--header">
	<div class="article--metadata">
		<time datetime="<?php echo $post['Post']['publish_start']; ?>" class="headline--datetime"><i class="icons icons--calendar"></i> 
            <?php echo date('d/m/Y', strtotime($post['Post']['publish_start'])); ?>
        </time>
		<div class="headline--fontes">
		<button type="button" class="fontes--btn" data-fonte="1" data-rel="#article">A&plus;</button>
		<button type="button" class="fontes--btn" data-fonte="-1" data-rel="#article">A&minus;</button>
		</div>
	</div>
	<h1 class="article--title">
		<a class="headline--title"><?php echo $post['Post']['title'] ?></a>
	</h1>
	<div class="share-this">
		<div class="addthis_inline_share_toolbox"></div>
	</div>
	</header>
	<article class="article--block" id="article">
	    <?php
        //se tem imagem
        $this->Helpers->load('Medias.MediasImage');

        if (isset($post['Images'][0])) { 
        //prepara a imagem para o OG
            $this->Html->meta(
                array('property' => 'og:image', 'content' => Router::url($post['Images'][0]['url'], true)),
                null,
                array('inline' => false)
            );
            if(!empty($post['Images'][0]['legenda'])){
                $figCaption = $post['Images'][0]['legenda'];
            }
            if(!empty($post['Images'][0]['credito'])){
                $figAutor = $post['Images'][0]['credito'];
            }
        ?>
        <figure>
            <a href="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename']); ?>" data-lightbox>
                <img src="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename'], '822x549'); ?>" alt="<?php echo $post['Images'][0]['alt']; ?>" />
            </a>
            <figcaption>
                <?php if(isset($figCaption)){
                    echo $figCaption; 
                }
                if(isset($figAutor)){
                    echo ' (Foto: '.$figAutor.')'; 
                } ?>
            </figcaption>
        </figure>
		<?php }	?>

	<?php echo $post['Post']['body']; ?>

	<?php 
		//Galeria de imagem
		if(isset($post['Images']) && count($post['Images']) > 1){ ?>
			<div class="article--carousel">
				<div class="carousel carousel-for" data-for="#carouselNav1" id="carouselFor1" data-carousel>
					<?php foreach($post['Images'] as $image){ ?>
                        <div class="carousel--slide">
                            <figure>
                                <a href="<?php echo $this->MediasImage->imagePreset($image['filename']); ?>">
                                    <img src="<?php echo $this->MediasImage->imagePreset($image['filename'], '822x549'); ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>
                                <figcaption>
                                    <?php 
                                    if(!empty($image['legenda'])){
                                        $figCaption = $image['legenda'];
                                    }
                                    if(!empty($image['credito'])){
                                        $figAutor = $image['credito'];
                                    }
                                    if(isset($figCaption)){
                                        echo $figCaption; 
                                    }
                                    if(isset($figAutor)){
                                        echo ' (Foto: '.$figAutor.')'; 
                                    } ?>
                                </figcaption>
                            </figure>
                        </div>
					<?php } ?>
				</div>

				<div class="carousel carousel-nav" data-for="#carouselFor1" id="carouselNav1" data-carousel>
					<?php foreach($post['Images'] as $image){ ?>
                        <div class="carousel--slide">
                            <img src="<?php echo $this->MediasImage->imagePreset($image['filename']); ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

	<div class="share-this" style="padding-top:50px;">
		<div class="addthis_inline_share_toolbox"></div>
	</div>

	<!--COMENTÁRIOS DO FACEBOOK-->
	<div class="fb-comments" data-href="<?php echo Router::url(null, true) ?>" data-width="100%" data-numposts="5"></div>

	<div style="padding-top: 25px;">
		<div class="addthis_relatedposts_inline"></div>
	</div>

	</article>
</div>
