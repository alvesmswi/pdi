<?php $this->Nodes->set($node); 

$this->Html->meta(
	array('property' => 'og:title', 'content' => $node['Node']['title']),
	null,
	array('inline'=>false)
);

$this->Html->meta(
  array('property' => 'og:description', 'content' => (isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt']) ? $node['Node']['excerpt'] : "")),
  null,
  array('inline'=>false)
);

$this->Html->meta(
	array('property' => 'og:url', 'content' => Router::url(null, true)),
	null,
	array('inline'=>false)
);

$this->Html->meta(
	array('name' => 'twitter:card', 'content' => 'summary'),
	null,
	array('inline'=>false)
);

?>

<div class="region region--article">
	<ol class="article--breadcrumbs">
	<li><a href="/">Home</a></li>
	<li><a href="<?php echo $this->here ?>"><?php echo $node['Node']['title']; ?></a></li>
	<? for($i=0; $i<(sizeof($migalhas)-1); $i++) { ?>
		<li><a href="<?=$migalhas[$i]['link'];?>"><?=$migalhas[$i]['tit'];?></a></li>
	<? } ?>
	</ol>
	<header class="article--header">
	<div class="article--metadata">
		<div class="headline--fontes">
		<button type="button" class="fontes--btn" data-fonte="1" data-rel="#article">A&plus;</button>
		<button type="button" class="fontes--btn" data-fonte="-1" data-rel="#article">A&minus;</button>
		</div>
	</div>
	<h1 class="article--title">
		<a class="headline--title"><?php echo $node['Node']['title']; ?></a>
	</h1>
	<div class="share-this">
		<div class="addthis_inline_share_toolbox"></div>
	</div>
	</header>
	<article class="article--block" id="article">
	<?php
		if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
			//se tem imagem
			if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
					'dimension'		=> '822x549'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);

				//prepara a imagem para o lightbox
				$imagemUrlArray['dimension'] = 'normal';
				$imagemLightUrl = $this->Html->url($imagemUrlArray);
				
				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true).'?node_id='.$node['Node']['id']),
					null,
					array('inline' => false)
				);
				
				if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
					$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
				}
				if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
					$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
				}
			} ?>
			<figure>
				<a href="<?php echo $imagemLightUrl.'?node_id='.$node['Node']['id']; ?>" data-lightbox>
					<img src="<?php echo $imagemUrl.'?node_id='.$node['Node']['id']; ?>" alt="<?php echo $node['Node']['title']; ?>" />
				</a>
				<figcaption>
					<?php if(isset($figCaption)){
						echo $figCaption; 
					}
					if(isset($figAutor)){
						echo ' (Foto: '.$figAutor.')'; 
					} ?>
				</figcaption>
			</figure>
		<?php }	?>
		

	<!-- <ul class="article--relacionadas">
		<li><div class="banner--block banner--block__300"><img src="http://placehold.it/300x280" /></div></li>
		<li><div class="banner--block banner--block__300"><img src="http://placehold.it/300x280" /></div></li>
	</ul> -->

	<?php echo $node['Node']['body']; ?>

	<?php 
		//Galeria de imagem
		if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
			<div class="article--carousel">
				<div class="carousel carousel-for" data-for="#carouselNav1" id="carouselFor1" data-carousel>
					<?php foreach($node['Multiattach'] as $foto){ ?>
						<?php 
						//se é não pdf
						if($foto['Multiattach']['mime']!='application/pdf'){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $foto['Multiattach']['filename'],
								'dimension'		=> '822x549'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="carousel--slide">
								<figure>
									<a href="<?php echo $imagemUrl; ?>">
										<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $node['Node']['title']; ?>" />
									</a>
									<figcaption></figcaption>
								</figure>
							</div>
						<?php } ?>
					<?php } ?>
				</div>

				<div class="carousel carousel-nav" data-for="#carouselFor1" id="carouselNav1" data-carousel>
					<?php foreach($node['Multiattach'] as $foto){ ?>
						<?php 
						//se é não pdf
						if($foto['Multiattach']['mime']!='application/pdf'){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $foto['Multiattach']['filename'],
								'dimension'		=> '822x549'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="carousel--slide">
								<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $node['Node']['title']; ?>>" />
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

	<?php 
		// Se tem algum bloco ativo nesta região
		echo $this->Regions->blocks('corpo_rodape');
	?>

	<div class="share-this" style="padding-top:50px;">
		<div class="addthis_inline_share_toolbox"></div>
	</div>

	<div id="comments" class="node-comments">
		<?php
			$type = $types_for_layout[$this->Nodes->field('type')];

			if ($type['Type']['comment_status'] > 0 && $this->Nodes->field('comment_status') > 0) {
				echo $this->element('Comments.comments');
			}

			if ($type['Type']['comment_status'] == 2 && $this->Nodes->field('comment_status') == 2) {
				echo $this->element('Comments.comments_form');
			}
		?>
	</div>

	<div style="padding-top: 25px;">
		<div class="addthis_relatedposts_inline"></div>
	</div>

	</article>
</div>
