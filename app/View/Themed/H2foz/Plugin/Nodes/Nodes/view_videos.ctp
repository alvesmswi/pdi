<?php $this->Nodes->set($node); 

$this->Html->meta(
	array('property' => 'og:title', 'content' => $node['Node']['title']),
	null,
	array('inline'=>false)
);

$this->Html->meta(
  array('property' => 'og:description', 'content' => (isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt']) ? $node['Node']['excerpt'] : "")),
  null,
  array('inline'=>false)
);

$this->Html->meta(
	array('property' => 'og:url', 'content' => Router::url(null, true)),
	null,
	array('inline'=>false)
);

$this->Html->meta(
	array('name' => 'twitter:card', 'content' => 'summary'),
	null,
	array('inline'=>false)
);

?>

<div class="region region--article">
	<ol class="article--breadcrumbs">
	<li><a href="/">Home</a></li>
	<li><a href="/videos">Vídeos</a></li>
	<? for($i=0; $i<(sizeof($migalhas)-1); $i++) { ?>
		<li><a href="<?=$migalhas[$i]['link'];?>"><?=$migalhas[$i]['tit'];?></a></li>
	<? } ?>
	</ol>
	<header class="article--header">
	<div class="article--metadata">
		<?php if (!empty($node['Editorias'])): ?>
			<a href="#" class="headline--category"><i class="icons icons--tags"></i> <?php echo $node['Editorias'][0]['title']; ?></a>
		<?php endif; ?>
		<time datetime="<?php echo $node['Node']['publish_start']; ?>" class="headline--datetime"><i class="icons icons--calendar"></i> <?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?></time>
		<div class="headline--fontes">
		<button type="button" class="fontes--btn" data-fonte="1" data-rel="#article">A&plus;</button>
		<button type="button" class="fontes--btn" data-fonte="-1" data-rel="#article">A&minus;</button>
		</div>
	</div>
	<h1 class="article--title">
		<a class="headline--title"><?php echo $node['Node']['title']; ?></a>
	</h1>
	<div class="share-this">
		<div class="addthis_inline_share_toolbox"></div>
	</div>
	</header>
	<article class="article--block" id="article">
	<?php
		if (isset($node['VideoDetail']['video']) && !empty($node['VideoDetail']['video'])) { ?>
			<figure>
				<?php if (!empty($node['VideoDetail']['video'])) { 
					$video = $node['VideoDetail']['video']; // $video recebe o valor da url do youtube (link)
					preg_match('/(v\/|\?v=)([^&?\/]{5,15})/', $video, $x); // Faz o preg_match para retirar caracters ?>
					<div class="embed-responsive embed-responsive-16by9">
						<object>
							<param name="movie" value="//www.youtube.com/v/<?php print end($x); ?>?hl=pt_BR&amp;version=3&amp;rel=0"></param>
							<param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>
							<embed src="//www.youtube.com/v/<?php print end($x); ?>?hl=pt_BR&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed>
						</object>
					</div>
				<?php } ?>
			</figure>
		<?php }	?>
		

	<!-- <ul class="article--relacionadas">
		<li><div class="banner--block banner--block__300"><img src="http://placehold.it/300x280" /></div></li>
		<li><div class="banner--block banner--block__300"><img src="http://placehold.it/300x280" /></div></li>
	</ul> -->

	<?php echo $node['Node']['body']; ?>

	<?php 
		//Galeria de imagem
		if(isset($node['Multiattach']) && count($node['Multiattach']) > 1){ ?>
			<div class="article--carousel">
				<div class="carousel carousel-for" data-for="#carouselNav1" id="carouselFor1" data-carousel>
					<?php foreach($node['Multiattach'] as $foto){ ?>
						<?php 
						//se é não pdf
						if($foto['Multiattach']['mime']!='application/pdf'){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $foto['Multiattach']['filename'],
								'dimension'		=> '822x549'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="carousel--slide">
								<figure>
									<a href="<?php echo $imagemUrl; ?>">
										<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $node['Node']['title']; ?>" />
									</a>
									<figcaption></figcaption>
								</figure>
							</div>
						<?php } ?>
					<?php } ?>
				</div>

				<div class="carousel carousel-nav" data-for="#carouselFor1" id="carouselNav1" data-carousel>
					<?php foreach($node['Multiattach'] as $foto){ ?>
						<?php 
						//se é não pdf
						if($foto['Multiattach']['mime']!='application/pdf'){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $foto['Multiattach']['filename'],
								'dimension'		=> '822x549'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="carousel--slide">
								<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $node['Node']['title']; ?>>" />
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

	<?php 
		// Se tem algum bloco ativo nesta região
		echo $this->Regions->blocks('corpo_rodape');
	?>

	<div class="share-this" style="padding-top:50px;">
		<div class="addthis_inline_share_toolbox"></div>
	</div>

	<div id="comments" class="node-comments">
		<?php
			$type = $types_for_layout[$this->Nodes->field('type')];

			if ($type['Type']['comment_status'] > 0 && $this->Nodes->field('comment_status') > 0) {
				echo $this->element('Comments.comments');
			}

			if ($type['Type']['comment_status'] == 2 && $this->Nodes->field('comment_status') == 2) {
				echo $this->element('Comments.comments_form');
			}
		?>
	</div>

	<div style="padding-top: 25px;">
		<div class="addthis_relatedposts_inline"></div>
	</div>

	</article>
</div>
