<div class="region region--atrativos">
	<header class="default--header atrativos--header">
		<?php
			//se teve uma pesquisa
			if(isset($this->params['type']) && !empty($this->params['type'])) {
                echo __d('croogo', $this->Mswi->typeName($this->params['type'])).' - ';
                echo __d('croogo', $this->Mswi->slugName($this->params->named['slug']));
			}else{
				echo __d('croogo', 'Resultado da pesquisa');
			}
		?>
	</header>
	<div class="atrativos--collection">
        <?php foreach ($nodes as $n) {
            $imagemUrl = null;
            if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
                if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
                    $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
                }

                $imagemUrlArray = array(
                    'plugin'		=> 'Multiattach',
                    'controller'	=> 'Multiattach',
                    'action'		=> 'displayFile', 
                    'admin'			=> false,
                    'filename'		=> $n['Multiattach']['filename'],
                    'dimension'		=> '400x225'
                );
                $imagemUrl = $this->Html->url($imagemUrlArray);
            }
            ?>
            <a href="<?php echo $n['Node']['path']; ?>" class="atrativos--block atrativos--block__large media--block__video" style="background-image: url(<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>)">
                <span class="atrativos--headline__title"><?php echo $n['Node']['title']; ?></span>
            </a>
        <?php } ?>
	</div>
</div>
	
<?php if(isset($this->request->paging['Node']['prevPage']) || isset($this->request->paging['Node']['nextPage'])) { ?>
    <div class="news--pagination">
        <?php echo $this->Paginator->prev('‹ página anterior', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
        <?php echo $this->Paginator->next('próxima página ›', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
    </div>
<? } ?>
