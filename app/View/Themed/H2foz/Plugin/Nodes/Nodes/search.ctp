<div class="region region--atrativos">
	<header class="default--header atrativos--header">
		<?php
			//se teve uma pesquisa
			if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
				echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
			}else{
				echo __d('croogo', 'Resultado da pesquisa');
			}
		?>
	</header>
</div>

<?php 
$cxGoogle = Configure::read('Site.busca_google');
if(!$cxGoogle){?>
	<div class="region region--lista">
		<?php
			//se tem algum registro
			if(!empty($nodes)){
				$contador = 0;
				$total = count($nodes);
				//percorre os nodes
				foreach($nodes as $n){ $contador++;?>
					<div class="news--block">
						<?php 
							if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
								if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
									$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
								}
							}
							//se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> '400x225'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
								?>
								<a href="<?php echo $n['Node']['path']; ?>">
									<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" alt="<?php echo $imageTitle ?>" class="img--responsive news--headline__image" />
								</a>
							<?php } ?>

						<div class="news--headline">
							<div class="news--metadata">
							<?php if (!empty($n['Node']['terms'])): ?>
								<a href="/<?php echo $n['Node']['type'] ?>/term/<?php echo strtolower(Inflector::slug($this->Mswi->categoryName($n['Node']['terms']), '-')) ?>" class="headline--category">
									<i class="icons icons--tags"></i> 
									<?php 
										//se tem chapeu
										if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
											echo $n['NoticiumDetail']['chapeu'];
										}else{
											echo $this->Mswi->categoryName($n['Node']['terms']);
										} 
									?>
								</a>
							<?php endif; ?>
							
							<time datetime="<?= date("Y-m-d\TH:i:s", strtotime($n['Node']['publish_start'])); ?>" class="headline--datetime"><i class="icons icons--calendar"></i> <?= date('d/m/Y', strtotime($n['Node']['publish_start'])); ?></time>
							</div>
							<a href="<?php echo $n['Node']['path']; ?>" class="headline--title"><?php echo $n['Node']['title']; ?></a>
							<p class="headline--excerpt"><?php echo $n['Node']['excerpt']; ?></p>
						</div>
					</div>
			<?php } ?>
		<?php } ?>
		
		<?php if(isset($this->request->paging['Node']['prevPage']) || isset($this->request->paging['Node']['nextPage'])) { ?>
			<div class="news--pagination">
				<?php echo $this->Paginator->prev('‹ página anterior', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
				<?php echo $this->Paginator->next('próxima página ›', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
			</div>
		<? } ?>
	</div>
<?php }else{?>
	<style>
		.gsc-control-cse{
			line-height: normal;
		}
		.gsc-control-cse form{
			line-height: 0;
		}
		.gsc-adBlock{
			display: none;
		}
		.gsc-control-cse table{
			margin: 0;
		}
	</style>
	<script>
		(function() {
			var cx = '<?php echo Configure::read('Site.busca_google')?>';
			var gcse = document.createElement('script');
			gcse.type = 'text/javascript';
			gcse.async = true;
			gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(gcse, s);
		})();
	</script>
	<gcse:search></gcse:search>
<?php }?>
