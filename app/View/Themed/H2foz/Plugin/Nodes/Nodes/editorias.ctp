<?php echo $this->element('node_metas'); ?>
<div class="region region--atrativos">
    <header class="default--header atrativos--header">
        <?php
            echo __d('croogo', $this->Mswi->slugName($slug));
        ?>
    </header>       
</div>
<div class="u-spacer_bottom">
    <?php 
    echo $this->Mswi->verMaisAjax('editorias_ajax', $slug, 1, 9);
    ?>
</div>
