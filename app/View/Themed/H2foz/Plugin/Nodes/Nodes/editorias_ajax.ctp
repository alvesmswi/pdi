<?php 
$page = $this->params->query['page'];
$proximaPagina = $page + 1;
if($page == 1){ ?>
	<div class="region region--atrativos">	
		<div class="atrativos--collection">
			<?php 			
			for ($i=0; isset($nodes[$i]) && $i <= 5; $i++) {
				if (!empty($nodes[$i]['Multiattach'])) { 
					$imagemUrlArray = array(
						'plugin'		=> 'Multiattach',
						'controller'	=> 'Multiattach',
						'action'		=> 'displayFile', 
						'admin'			=> false,
						'filename'		=> $nodes[$i]['Multiattach']['filename'],
						'dimension'		=> '400x225'
					);
					$imagemUrl = $this->Html->url($imagemUrlArray);
					$imageTitle = htmlspecialchars((isset($nodes[$i]['Multiattach']['meta']) && !empty($nodes[$i]['Multiattach']['meta']) ? $nodes[$i]['Multiattach']['meta'] : $nodes[$i]['Node']['title']), ENT_QUOTES, 'UTF-8'); ?>
					<a href="<?= $nodes[$i]['Node']['path'] ?>" class="atrativos--block atrativos--block__large" style="background-image: url(<?php echo $imagemUrl.'?node_id='.$nodes[$i]['Node']['id']; ?>)"><span class="atrativos--headline__title"><?= $nodes[$i]['Node']['title'] ?></span></a>
			<?php unset($nodes[$i]); } } ?>
		</div>
	</div>

	<!-- BANNER h728 -->
	<div class="banner--block banner--block__728">
		<a href="#" target="_blank" rel="nofollow"><img src="http://via.placeholder.com/728x90" alt="alt" /></a>
	</div>
	<!-- / BANNER h728 -->
<?php }?>
<div class="region region--lista">
	<?php
		//se tem algum registro
		if(!empty($nodes)){			
			$contador = 0;
			$total = count($nodes);
			//percorre os nodes
			foreach($nodes as $n){ $contador++;?>
				<div class="news--block">
					<?php 
	             		if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
	             			if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
	             				$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
	             			}
	             		}
						//se tem imagem
						if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
							$imagemUrlArray = array(
						        'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> '400x225'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							$imageTitle = htmlspecialchars((isset($n['Multiattach']['meta']) && !empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
							?>
							<a href="<?php echo $n['Node']['path']; ?>">
								<img src="<?php echo $imagemUrl.'?node_id='.$n['Node']['id']; ?>" alt="<?php echo $imageTitle ?>" class="img--responsive news--headline__image" />
							</a>
	             		<?php } ?>

					<div class="news--headline">
						<div class="news--metadata">
						<?php if (!empty($n['Editoria']['title'])): ?>
							<a href="/ultimas-noticias/<?php echo $n['Editoria']['slug'] ?>" class="headline--category">
								<i class="icons icons--tags"></i> 
								<?php 
									//se tem chapeu
									if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
										echo $n['NoticiumDetail']['chapeu'];
									}else{
										echo $n['Editoria']['title'];
									} 
								?>
							</a>
						<?php endif; ?>
						<time datetime="<?= date("Y-m-d\TH:i:s", strtotime($n['Node']['publish_start'])); ?>" class="headline--datetime"><i class="icons icons--calendar"></i> <?= date('d/m/Y', strtotime($n['Node']['publish_start'])); ?></time>
						</div>
						<a href="<?php echo $n['Node']['path']; ?>" class="headline--title"><?php echo $n['Node']['title']; ?></a>
						<p class="headline--excerpt"><?php echo $n['Node']['excerpt']; ?></p>
					</div>
				</div>
          <?php } ?>
	<?php } ?>

	<?php 
		//se tem algum registro
		if(!empty($nodes)){
			echo $this->Mswi->verMaisAjax('editorias_ajax',$n['Editoria']['slug'], $proximaPagina, 9);
		}
	?>
	
	<?php //if(isset($this->request->paging['Node']['prevPage']) || isset($this->request->paging['Node']['nextPage'])) { ?>
		<!-- <div class="news--pagination"> -->
			<?php //echo $this->Paginator->prev('‹ página anterior', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
			<?php //echo $this->Paginator->next('próxima página ›', array('tag' => false), null, array('class' => 'news--pagination__btn')); ?>
		<!-- </div> -->
	<? //} ?>
</div>

<style>
	#conteudo .btn{
		background-color: #119bce;
		color: #fff;
		margin-bottom: 20px;
	}
</style>
