<header class="o-header"><span class="o-header_title">Assinante</span></header>
<div class="o-login_content">
   <h1 class="o-news_title c-article_title o-login_title">Solicitação de Cadastro de Usuário</h1>
   <?php //echo $this->Form->create('User');?>
   <form action="/">
      <div class="row">
         <div class="col-xs-3 col-sm-4">
         	<?php 
         	//Exibe o último jornal
         	echo $this->Regions->block('ultimo-jornal');?>
         </div>
         <div class="col-xs-9 col-sm-8 col-md-6">
            <div class="form-group">
            	<?php echo $this->Form->input('username', array('label'=>'Nome de Usuário', 'class'=>'form-control')); ?>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input('password', array('label'=>'Senha', 'type' => 'password', 'class'=>'form-control', 'value' => '')); ?>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input('verify_password', array('label'=>'Confirme a Senha', 'type' => 'password', 'value' => '', 'class'=>'form-control')); ?>
            </div>
           	<div class="form-group">
            	<?php echo $this->Form->input('name', array('label'=>'Seu nome completo', 'class'=>'form-control')); ?>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input('email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control')); ?>
            </div>
            <div class="text-right"><button type="submit" class="btn btn-lg btn-primary">Enviar Solicitação</button></div>
         </div>
      </div>
   <?php echo $this->Form->end();?>   
</div>
<br />