<header class="o-header">
	<span class="o-header_title">Entrar</span>
</header>
<div class="o-login_content">
   <h1 class="o-news_title c-article_title o-login_title">
   	Área exclusiva para assinantes
   </h1>
   <?php 
	echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login')));
	//se tem redirecionamento
   	if(isset($this->params->query['redirect']) && !empty($this->params->query['redirect'])){
		$redirect = $this->params->query['redirect'];
		echo $this->Form->hidden('redirect', array('value'=>$redirect));
	}
   ?>
      <div class="row">
         <div class="col-xs-3 col-sm-4">
         	<?php 
         	//Exibe o último jornal
         	echo $this->Regions->block('ultimo-jornal');?>
         </div>
         <div class="col-xs-9 col-sm-8 col-md-6">
            <div class="o-login_cta">
               <p>Ainda não é assinante? <a href="/contact/pedido-assinatura" class="btn btn-success">Assine agora</a></p>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input(Configure::read('User.campo_usuario'), array('label' => Configure::read('User.label_usuario'), 'class'=>'form-control')); ?>
            </div>
            <div class="form-group">
            	<?php echo $this->Form->input('password', array('label' =>'Senha', 'class'=>'form-control')); ?>
            </div>
            <div class="row">
               <div class="col-xs-6"><a href="/users/users/forgot" class="btn-link">Esqueci minha senha</a></div>
               <div class="col-xs-6 text-right"><button type="submit" class="btn btn-lg btn-primary">Entrar</button></div>
            </div>
         </div>
      </div>
     <?php echo $this->Form->end(); ?>
</div>
