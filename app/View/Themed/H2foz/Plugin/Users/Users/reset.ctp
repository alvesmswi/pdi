<header class="o-header">
	<span class="o-header_title">Assinante</span>
</header>
<div class="o-login_content">
	<h1 class="o-news_title c-article_title o-login_title">
   		Trocar Senha
   	</h1>
   	<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
		<div class="row">
         	<div class="col-xs-9 col-sm-8 col-md-6">
	            <div class="form-group">
	            	<?php echo $this->Form->input('password', array('label' =>  __d('croogo', 'New password'), 'class'=>'form-control','minlength'=>8)); ?>
	            </div>
	            <div class="form-group">
	            	<?php echo $this->Form->input('verify_password', array('label' => __d('croogo', 'Verify Password'), 'class'=>'form-control','minlength'=>8, 'type'=>'password')); ?>
	            </div>
	            <div class="row">
	               	<div class="col-xs-6 text-right"><button type="submit" class="btn btn-lg btn-primary">
						Trocar Senha	
	               	</button></div>
	            </div>
         	</div>
      	</div>
     	<?php echo $this->Form->end(); ?> 
   	</form>
</div>