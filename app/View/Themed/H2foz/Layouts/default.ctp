<?php
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
?>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<?php
			echo $this->Html->charset();
			echo $this->Html->meta('description', Configure::read('Site.tagline'));
			echo $this->Html->meta('keywords', Configure::read('Meta.keywords'));
			echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
			echo $this->fetch('meta'); 

			
			//Se foi informado o tempo de refresh
			if (!empty(Configure::read('Site.refresh')) && is_numeric(Configure::read('Site.refresh'))) {
				echo $this->Html->meta(array('http-equiv' => 'refresh', 'content' => Configure::read('Site.refresh')));
			}
		?>

		<title>
			<?php echo Configure::read('Site.title'); ?> |
			<?php echo $title_for_layout; ?>
		</title>

		<link rel="icon" href="/theme/<?php echo Configure::read('Site.theme')?>/favicon.ico" />

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/main.min.css">
		<link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/mswi.css">

		<?php
		//Caminho absoluto do arquivo de CSS customizado
		$themeCss = WWW_ROOT . 'css' . DS . 'theme.css';
		//se o arquivo de CSS personalizado existe
		if(file_exists($themeCss)){
			$themeCss = '/css/theme.css';
			?><link rel="stylesheet" href="<?php echo $themeCss; ?>"><?php
		}
		?>		

		<?php
			//Se foi inofmrado o user do analytics
			if (!empty(Configure::read('Service.analytics'))):
				$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
				<script>
					(function (i, s, o, g, r, a, m) {
						i['GoogleAnalyticsObject'] = r;
						i[r] = i[r] || function () {
							(i[r].q = i[r].q || []).push(arguments)
						}, i[r].l = 1 * new Date();
						a = s.createElement(o),
							m = s.getElementsByTagName(o)[0];
						a.async = 1;
						a.src = g;
						m.parentNode.insertBefore(a, m)
					})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

					<?php foreach ($arrayAnalytics as $key => $analytics) { ?>
					ga('create', '<?= trim($analytics) ?>', 'auto', {
						'name': 'id<?= $key ?>'
					});
					ga('id<?= $key ?>.send', 'pageview');
					<?php } ?>

					/**
						* Function that tracks a click on an outbound link in Analytics.
						* This function takes a valid URL string as an argument, and uses that URL string
						* as the event label. Setting the transport method to 'beacon' lets the hit be sent
						* using 'navigator.sendBeacon' in browser that support it.
						*/
					var trackOutboundLink = function (url) {
						ga('send', 'event', 'outbound', 'click', url, {
							'transport': 'beacon',
							'hitCallback': function () {
								document.location = url;
							}
						});
					}
				</script>
		<?php endif; ?>

		<script type='text/javascript'>
			var googletag = googletag || {};
			googletag.cmd = googletag.cmd || [];
			(function () {
				var gads = document.createElement('script');
				gads.async = true;
				gads.type = 'text/javascript';
				var useSSL = 'https:' == document.location.protocol;
				gads.src = (useSSL ? 'https:' : 'http:') +
					'//www.googletagservices.com/tag/js/gpt.js';
				var node = document.getElementsByTagName('script')[0];
				node.parentNode.insertBefore(gads, node);
			})();
		</script>

		<?php echo Configure::read('Service.header'); ?>

		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

		<?php 
			//Se tem algum bloco ativo nesta região
			echo $this->Regions->blocks('header_scripts');
		?>
	</head>

	<body data-disable-copy>

		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
			    FB.init({
			      appId      : '321419201624688',
			      xfbml      : true,
			      version    : 'v2.9'
			    });
			    FB.AppEvents.logPageView();
		  	};

			(function(d, s, id){
			    var js, fjs = d.getElementsByTagName(s)[0];
			    if (d.getElementById(id)) {return;}
			    js = d.createElement(s); js.id = id;
			    js.src = "//connect.facebook.net/pt_BR/sdk.js";
			    fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

		<div class="container-fluid">
			<header class="region region--header">
				<div class="content--wrapper">
					<div class="header--table">
						<div class="logo--wrapper">
							<?php 
								$themeLogo = WWW_ROOT . 'img' . DS . 'logo.png';
								
								if(file_exists($themeLogo)){
									$themeLogo = '/img/logo.png';
								}else{
									$themeLogo = '/theme/' . Configure::read('Site.theme').'/img/logo.png';
								} 
							?>
							<a href="/"><img src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>"></a>
						</div>

						<?php 
							// Se tem algum bloco ativo nesta região
							echo $this->Regions->blocks('publicidade_top');
						?>
					</div>
				</div>
			</header>

			<div class="region region--nav">
				<div class="content--wrapper">
					<div class="nav--flex">
						<button type="button" id="navToggle" class="nav--toggle">
							<span class="sr-only">Menu</span><i class="icons icons--bars"></i>
						</button>
						<?php
							// Se tem algum bloco ativo nesta região
							echo $this->Regions->blocks('menu_principal');
						?>

						<div class="nav--search">
							<form class="search--form" method="GET" action="/search">
								<div class="input--group input--group__merge">
									<input id="buscakey" type="search" class="search--input input--field input--field__search" name="q" placeholder="Busque" />
									<label for="navSearch" class="sr-only">Busca</label>
									<span class="input--group__btn">
										<button type="submit" class="btn btn-default search--submit">
											<i class="icons icons--search"></i><span class="sr-only">Buscar</span>
										</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<?php				

				// Se tem algum bloco ativo nesta região
				echo $this->Regions->blocks('destaques');

				echo $this->Regions->blocks('publicidade_destaques');
			?>

			<div class="region region--split">
				<div class="content--wrapper">
					<div class="row">
						<div class="region--split__main">
							<?php
								//Exibe as mensagens
								echo $this->Layout->sessionFlash();
								//se tem alguma mensagem de retorno positivo do Form
								if(isset($this->params->query['enviado']) && $this->params->query['enviado']){
									echo '<div class="alert alert-success">Mensagem enviada com sucesso!</div>';
								}

								//Conteudo do node
								echo $content_for_layout;
							?>

							<?php if ($this->here == '/') { ?>
								<div class="region region--news">
									<div class="row">
										<?php
											//Se tem algum bloco ativo nesta região
											echo $this->Regions->blocks('categorias');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
						
						<?php
							//se foi informado a sidebar
							if (isset($sidebar)) {
								$sidebar = $sidebar;
							} else {							
								$sidebar = true;
							}

							// Se é para exibir a leteral
							if ($sidebar) { ?>
								<aside class="region--split__aside">
									<div class="region region--social">
										<div class="row">
											<div class="addthis_inline_follow_toolbox"></div>
										</div>
									</div>

									<?php
										//Se tem algum bloco ativo nesta região
										echo $this->Regions->blocks('right');
									?>
								</aside>
						<?php } ?>
					</div>
				</div>
			</div>
					
			<?php 
				// Se tem algum bloco ativo nesta região
				echo $this->Regions->blocks('region2');
			?>

			<footer class="region region--footer">
				<?php
					echo $this->Regions->blocks('footer');
				?>
				
				<?php echo $this->Menus->menu('sitemap', array('element' => 'menus/sitemap')); ?>
				
				<div class="region region--copyright">
					<div class="content--wrapper">
						&copy; 2003-<?php echo date('Y') ?> <?php echo Configure::read('Site.title') ?> - Todos os direitos reservados.
					</div>
				</div>
			</footer>
		</div>

		<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/production.js"></script>
		<script src="/theme/<?php echo Configure::read('Site.theme')?>/js/custom.js"></script>
		<script>
			function assinarNewsletter(Event) {
				Event.preventDefault();
				var form = this;
				var data = form.querySelector('.subscribe--input').value;
				
				$.ajax({
					type: 'POST',
					url: '/newsletter/enviar',
					data: {email: data},
					success: function(data){
					console.log(data);
					},
				});
			}
			Array.prototype.forEach.call(document.querySelectorAll('.subscribe--form'), function(el) { el.addEventListener('submit', assinarNewsletter)})
		</script>

		<?php if(!empty(Configure::read('Service.addthis'))): ?>
			<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis') ?>"></script>
		<?php endif; ?>

		<?php
			//Se tem algum bloco ativo nesta região
			if($this->Regions->blocks('footer_scripts')){
				echo $this->Regions->blocks('footer_scripts');
			}
			echo $this->fetch('script');
			echo $this->Js->writeBuffer(); // Write cached scripts
		?>

	</body>
</html>