$(document).ready(function () {
    // Quando houver o clique no botao megamenu-toggle chama a função
    $('body').find('button.megamenu-toggle').bind("click", function (e) {
        $("#megamenu").on('show.bs.collapse', function () {
            $("#navbar").find('ul.navbar-nav').addClass("slide-up-hide");
            $(".topbar").find('.navbar-header').addClass("shrink");
        });

        $("#megamenu").on('hide.bs.collapse', function () {
            $("#navbar").find('ul.navbar-nav').removeClass("slide-up-hide");
            $(".topbar").find('.navbar-header').removeClass("shrink");
        });

    });
});

// Botão buscar para mobile
$(document).ready(function () {
    $('.navbar-form button').click(function(){
        var mobileScreen = $(window).width();
        var activedSearchField = $(this).closest('.navbar-form').hasClass("mobile");
        var hasInputData = $(this).closest('.navbar-form').find('input').val() === null;

        // Verifica se esta no mobile
        if(mobileScreen < 640) {
             if  ( activedSearchField && !hasInputData) {
                $(this).closest('.navbar-form').toggleClass("mobile");
             } else {
                 var actionUrl = $(this).closest('.navbar-form').attr("action");
                 $(this).closest('.navbar-form').removeAttr("action").toggleClass("mobile").attr("action", actionUrl);
                 return false;
             }
        }
        
    });
});

// Gallery-slick
$(document).ready(function () {

    $('.gallery__nav-thumbs').slick({
       centerMode: true,
    // infinite: false,
       centerPadding: '0px',
       slidesToShow: 4,
       focusOnSelect: true,
       adaptiveHeight: true,
       lazyLoad: 'ondemand',
       responsive: [
           {
               breakpoint: 960,
               settings: {
                   arrows: true,
                   centerMode: true,
                   centerPadding: '0px',
                   focusOnSelect: true,
                   adaptiveHeight: true,
                   lazyLoad: 'ondemand',
                   slidesToShow: 4
               }
           },
           {
               breakpoint: 640,
               settings: {
                   arrows: true,
                   centerMode: true,
                   centerPadding: '0px',
                   focusOnSelect: true,
                   adaptiveHeight: true,
                   lazyLoad: 'ondemand',
                   slidesToShow: 2
               }
           }
       ]
   });
});

//Load images in visualization
$(document).ready(function () {

    
    $('.gallery__nav-thumbs').find('.slick-slide').bind('click', function () {
             var imgSrc = $(this).data('load-img');
             var idBtn = $(this).attr('id');
             var imgAlt = $(this).data('alt-img');
             var author = $(this).data('by');
             var legenda = $(this).data('legend');
             var divAdd = "<div id='view" + idBtn + "' class='hidden fade' itemscope itemtype='http://schema.org/ImageObject'>";
             divAdd += "<figure>";
             divAdd += "<div class='img-responsive'>";
             divAdd += "<img itemprop='contentUrl' src='"+ imgSrc+"' alt='"+imgAlt+"'>";
             divAdd += "</div><figcaption><span itemprop='description'>"+ legenda +"</span>. <cite itemprop='author'>" + author + "</cite></figcaption></figure></div>";

            var btnClicked = $('.gallery__visualization').find('.fade').is('#view'+idBtn);

            if (!btnClicked) {
               // PRELOADER
               $('.gallery__visualization').find(".ajax-loader").removeClass("hidden");
               $('.gallery__visualization').append(divAdd).ready(function () {
                  $('.gallery__visualization').find(".ajax-loader").addClass("hidden");
               });
            } 
            $('.gallery__visualization .fade.in').removeClass('in').addClass('hidden');
            $(".gallery__visualization div#view" + idBtn).removeClass('hidden').addClass('in');
        });


    $('.gallery__nav-thumbs').find('button').bind('click', function () {
        $('.gallery__nav-thumbs').find('.slick-slide').each(function () {
            if($(this).is('.slick-current')) {
               var idCurrent = $(this).attr('id');
               var btnClicked = $('.gallery__visualization').find('.fade').is('#view' + idCurrent);
               var imgSrc = $(this).data('load-img');
               var imgAlt = $(this).data('alt-img');
               var author = $(this).data('by');
               var legenda = $(this).data('legend');
               var divAdd = "<div id='view" + idCurrent + "' class='hidden fade' itemscope itemtype='http://schema.org/ImageObject'>";
                divAdd += "<figure>";
                divAdd += "<div class='img-responsive'>";
                divAdd += "<img itemprop='contentUrl' src='" + imgSrc + "' alt='" + imgAlt + "'>";
                divAdd += "</div><figcaption><span itemprop='description'>" + legenda + "</span>. <cite itemprop='author'>" + author + "</cite></figcaption></figure></div>";




              if (!btnClicked) {
                   $('.gallery__visualization').find(".ajax-loader").removeClass("hidden");
                   $('.gallery__visualization').append(divAdd).ready(function () {
                       $('.gallery__visualization').find(".ajax-loader").addClass("hidden");
                   });
              }
               $('.gallery__visualization .fade.in').removeClass('in').addClass('hidden');
               $(".gallery__visualization div#view" + idCurrent).removeClass('hidden').addClass('in');
            }
        });
    });
});

$(document).ready(function() {
    $('#corpo div[data-oembed-url] div').css("max-width", "100%");
});

