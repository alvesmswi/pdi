var gOverride = {
    urlBase: 'http://gridder.andreehansson.se/releases/latest/',
    gColor: 'red',
    gColumns: 12,
    gOpacity: 0.25,
    gWidth: 10,
    pColor: 'red',
    pHeight: 12,
    pOffset: 0,
    pOpacity: 0.2,
    center: true,
    gEnabled: true,
    pEnabled: true,
    setupEnabled: true,
    fixFlash: true,
    size: 960
};


$(document).ready(function () {
    // action on key up
    $(document).keyup(function (e) {
        if (e.which == 18) {
            isAlt = false;
        }
        if (e.which == 17) {
            isCtrl = false;
        }
        if (e.which == 16) {
            isShift = false;
        }
    });

    // action on key down
    $(document).keydown(function (e) {
        if (e.which == 18) {
            isAlt = true;
        }
        if (e.which == 17) {
            isCtrl = true;
        }
        if (e.which == 16) {
            isShift = true;
        }


        if (e.which == 77 && isAlt) {
            // Alt + M
            $('article').toggleClass("special");
        }

        if (e.which == 56 && isCtrl) {
            // Ctrl + 8
            $('.wrapper').toggleClass("col-lg-12");
            $('.wrapper').toggleClass("col-lg-8");
            $('.main').toggleClass("container-fluid");
            $('.main').toggleClass("container");
        }

        if (e.which == 54 && isCtrl) {
            // Ctrl + 6
            $('.wrapper').toggleClass("col-lg-12");
            $('.wrapper').toggleClass("col-lg-6");
            $('.main').toggleClass("container-fluid");
            $('.main').toggleClass("container");
        }

        if (e.which == 57 && isCtrl) {
            // Ctrl + 9

            // elementos que serão adicionados a classe .shadow
            var elementos = ['article', '.breadcrumb'];

            // Pega cada elemento e addiciona o função de troca de classe
            $.each(elementos, function (i, val) {
                $(val).toggleClass("shadow");
            });

        }

        if (e.which == 71 && isAlt) {
            // Alt + G
            $("#g-grid, #g-setup").toggle();
        }

        //Overlay 960g
        $(document).ready(function () {
            document.body.appendChild(
                document.createElement('script'))
                .src = './js/grid-overlay/960.gridder.js';
            $("#g-grid, #g-setup").hide();
        });

    });

});
