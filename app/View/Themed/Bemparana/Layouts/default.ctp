<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title>
		<?php echo Configure::read('Site.title'); ?> |
		<?php echo $title_for_layout; ?>
	</title>
	<?php    
	echo $this->fetch('meta');
	?>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
        crossorigin="anonymous">
    <link rel="stylesheet" href="/theme/bemparana/css/app.min.css">
    <link rel="stylesheet" href="/theme/bemparana/css/app-adm.css">
    <link rel="stylesheet" href="/theme/bemparana/css/featherlight/featherlight.css">
    <link rel="stylesheet" href="/css/theme.css">
	<?php
	//Se foi inofmrado o user do analytics
	if (!empty(Configure::read('Service.analytics'))) {
		$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>	
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			<?php foreach ($arrayAnalytics as $key => $analytics) { ?>
				ga('create', '<?= trim($analytics) ?>', 'auto', {
					'name': 'id<?= $key ?>'
				});
				ga('id<?= $key ?>.send', 'pageview');
			<?php } ?>

			var trackOutboundLink = function (url) {
				ga('send', 'event', 'outbound', 'click', url, {
					'transport': 'beacon',
					'hitCallback': function () {
						document.location = url;
					}
				});
			}
		</script>
	<?php } 

	//Se foi informado o tempo de refresh para a Home
	if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
		<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php } 
    
    //Se foi informado o tempo de refresh para as internas
	if ($this->here != '/' && !empty(Configure::read('Site.refresh_internas')) && is_numeric(Configure::read('Site.refresh_internas'))) { ?>
		<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
	<?php } 
	
	//snippetscustomizados
	echo Configure::read('Service.header'); 

	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('header_scripts')){
		echo $this->Regions->blocks('header_scripts');
	}
	?>
</head>

<body itemscope itemtype="http://schema.org/WebPage">
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php 
    if(isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])){
        echo $this->element('admin_bar');
    }    
    ?>
    
    <!-- Menu Principal -->
    <nav class="navbar navbar-bemparana">
        <div class="topbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- Logo -->
                    <a class="navbar-brand" href="/">
                        <figure class="logo-bem-parana">
                            <img src="/theme/bemparana/images/marca-bem-parana.svg" alt="Logo Bem Paraná" />
                        </figure>
                    </a>
                </div>

                <!-- Curta nossa página -->
                <div class="follow-us">
                    <a href="<?php echo Configure::read('Social.facebook');?>" title="Curta nossa página" class="btn btn-follow" target="_blank">
                        <span class="icon fa fa-facebook" aria-label="Ícone do Facebook" role="icon"></span>
                        <span class="text" aria-label="Curta nossa página">Curta nossa página</span>
                    </a>
                    <a href="<?php echo Configure::read('Social.twitter');?>" title="Siga-nos Twitter" class="btn btn-follow" target="_blank">
                        <span class="icon fa fa-twitter" aria-label="Ícone do Twitter" role="icon"></span>
                        <span class="text" aria-label="Siga-nos no Twitter">Siga-nos no Twitter</span>
                    </a>
                </div>
                <?php echo $this->element('busca');?>

            </div>
        </div>
        <!-- Menu de navegação -->
        <div class="container">
			<?php 
			echo $this->Menus->menu('main', array('element' => 'menus/main')); 
			?>
        </div>

    </nav>
    <!-- /nav -->

    <!-- BLOG HEADER -->
    <?php echo $this->element('blog_header');?>

	<!-- ANUNCIO -->
    <?php
    //se não é a Home
    if($this->here != '/'){
        //Se tem algum bloco ativo nesta região
        if ($this->Regions->blocks('publicidade_top')){ ?>
            <div class="container">
                <div class="ads ads--super-banner hidden-sm">
                    <span class="headover-ads rows--x2">Publicidade</span>
                    <div class="content w728">
                        <?php echo $this->Regions->blocks('publicidade_top');?>
                    </div>
                </div>
            </div>			
        <?php }	?>
        <?php
        //Se tem algum bloco ativo nesta região
        if ($this->Regions->blocks('publicidade_mobile_top')){ ?>
            <div class="container">
                <div class="ads ads--rectangle-medium--half visible-sm">
                    <span class="headover-ads">Publicidade</span>
                    <div class="content">
                        <?php echo $this->Regions->blocks('publicidade_mobile_top');?>
                    </div>
                </div>		
            </div>	
        <?php }	
    }?>

    <div id="main" class="container-fluid" role="main">

        <!-- Breadcrumb -->
        <?php    
        //se não é a Home
        if($this->here != '/'){
            //pega os breadcrumbs
            $bcs = $this->Html->_crumbs;
            //se tem bread crumb
            if(!empty($bcs)){ ?>
                <div class="container">
                    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <a href="#" class="btn btn-previous" title="Botão voltar" role="button" onclick="history.back()">
                            <span class="icon fa fa-chevron-left" role="icon"></span>
                        </a>
                        <?php 
                        //percorre osbcs
                        foreach($bcs as $key => $bc){ ?>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="<?php echo $bc[1];?>" title="Editora">
                                    <span itemprop="name">
                                        <?php echo $bc[0];?>
                                    </span>
                                </a>
                                <meta itemprop="position" content="<?php echo $key+1;?>" />
                            </li>&nbsp;
                        <?php } ?>
                    </ol>
                </div>
            <?php } 
        }?>

        <!-- CONTEUDO PRINCIPAL -->
        <main itemprop="mainEntity" itemscope itemtype="http://schema.org/Article">

            <?php 
            //Se tem algum bloco ativo nesta região
            if ($this->Regions->blocks('destaques')){ 
                echo $this->Regions->blocks('destaques');             
            } 
            ?>

            <div class="container">
                <div class="row">
                    <?php 
                    //Exibe as mensagens
                    echo $this->Layout->sessionFlash();
                    
                    //Conteudo do node
                    echo $content_for_layout;   

                    //se não for a home
                    if($this->here != '/'){
                    ?>
                        <!-- Coluna lateral -->
                        <section id="col-side" class="col-side col-lg-4 col-md-12 col-sm-12">      
                            <?php echo $this->Regions->blocks('direita_interna1'); ?> 
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <?php echo $this->Regions->blocks('direita_interna2'); ?> 
                                    </div>
                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <?php echo $this->Regions->blocks('direita_interna3'); ?> 
                                    </div>
                                </div>
                            </div> 

                        </section>    
                    <?php } ?>
 
                </div>
            </div>            
            
            <?php echo $this->Regions->blocks('secao_especial2'); ?>
            <?php echo $this->Regions->blocks('editoria_especial'); ?>

            <div class="container">
                <div class="row">
                    <section class="col-side col-lg-4 col-md-12 col-sm-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <?php echo $this->Regions->blocks('direita_home2'); ?> 
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <?php echo $this->Regions->blocks('direita_home3'); ?> 
                                </div>
                            </div>
                        </div> 
                    </section> 

                    <section class="section list-news col-lg-8 col-md-12 col-sm-12">
                        <?php echo $this->Regions->blocks('centro1'); ?> 
                    </section>
                </div>
            </div>  

        </main>

    </div>

    <section class="section">
        <?php echo $this->Regions->blocks('banner_bottom'); ?> 
    </section>

    <!-- RODAPÉ DA PÁGINA -->
    <footer class="footer">
		<?php
		//Se tem algum bloco ativo nesta região
		if ($this->Regions->blocks('footer')){
			echo $this->Regions->blocks('footer');
		}		
		echo $this->Menus->menu('sitemap', array('element' => 'menus/sitemap')); 
		?>        
    </footer>

	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script src="/theme/bemparana/js/jquery.min.js"></script>
	<script src="/theme/bemparana/js/bootstrap.min.js"></script>    
    <script src="/theme/bemparana/js/slick.min.js"></script>
    <script src="/theme/bemparana/js/main.js"></script>
    <script src="/theme/bemparana/js/featherlight/featherlight.js"></script> 
    <script src="/theme/bemparana/js/ResizeSensor.min.js"></script>
    <script src="/theme/bemparana/js/theia-sticky-sidebar.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.main-content, .col-side').theiaStickySidebar({
                // Settings
                additionalMarginTop: 30
            });
        });
    </script>

	<?php if(!empty(Configure::read('Service.addthis'))): ?>
		<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis') ?>"></script>
    <?php endif; ?>
    <script src="/js/revisao.js"></script>
	<?php 
	//Se tem algum bloco ativo nesta região
	if($this->Regions->blocks('footer_scripts')){
		echo $this->Regions->blocks('footer_scripts');
    }
    echo $this->Js->writeBuffer();
	?>
</body>

</html>
<?php echo $this->element('sql_dump')?>