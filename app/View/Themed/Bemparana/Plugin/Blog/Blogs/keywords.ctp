<?php
//Configura o breadcrumb
$this->Html->addCrumb($title_for_layout, $this->here);
?>

<section id="list-news" class="section list-news col-lg-8 col-md-12 col-sm-12">

	<div class="container">
		<?php		
		//se tem algum registro
		if(!empty($posts)){
			?>
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($posts as $n){
					$imagemUrl = '';
					$class = '';
					//se tem imagem
					if(isset($n['Post']['image']) && !empty($n['Post']['image'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Post']['image'],
							'dimension'		=> '300largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$class = 'row';
					}
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">
								<a href="/blog/<?php echo $n['Blog']['slug'] ?>/post/<?php echo $n['Post']['slug'] ?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
														echo $n['Post']['title'];
													?>
												</span>
											</div>
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Post']['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Post']['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
													echo date('d/m/Y', strtotime($n['Post']['publish_start']));
												?>
											</span>
											<h1><?php echo $n['Post']['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		<?php }else{ 
			echo 'Nenhum registro encontrado';
		} ?>	
		<?php 
		//Insere o paginado
		echo $this->element('paginator');
		?>
	</div>
</section>