<?php
$this->Helpers->load('Medias.MediasImage');
$this->Html->meta(
	array('property' => 'og:title', 'content' => $post['Post']['title']),
	null,
	array('inline'=>false)
);
$this->Html->meta(
  array('property' => 'og:description', 'content' => (isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt']) ? $post['Post']['excerpt'] : "")),
  null,
  array('inline'=>false)
);
$this->Html->meta(
	array('property' => 'og:url', 'content' => Router::url(null, true)),
	null,
	array('inline'=>false)
);
$this->Html->meta(
	array('name' => 'twitter:card', 'content' => 'summary'),
	null,
	array('inline'=>false)
);

//se tem chamada
if(isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){
	$description = $post['Post']['excerpt'];
}else{
	$description = substr($post['Post']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);

//se tem keywords
if(isset($post['Post']['keywords']) && !empty($post['Post']['keywords'])){
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $post['Post']['keywords']
		),
		null,
		array('inline'=>false)
	);
}else{
	//Gera as keywords
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $this->Mswi->gerarKeywords($post['Post']['title'].' '.$description)
		),
		null,
		array('inline'=>false)
	);
}

//configura os breadscrumbs
$this->Html->addCrumb(
	'Blogs', '/blogs'
);
$this->Html->addCrumb(
	$post['Blog']['title'], $this->Html->url($post['Blog']['url'])
);

?>
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<a href="<?php echo $this->Html->url($post['Blog']['url']) ?>" class="headover"><?php echo $post['Blog']['title'] ?></a>
				<h1 itemprop="name">
					<?php echo $post['Post']['title']; ?>
				</h1>
				<?php
				//se tem chapeu
				if(isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){ ?>
					<summary><?php echo $post['Post']['excerpt']; ?></summary>
				<?php } ?>
			</div>

			<?php
			//se tem imagem
			if(isset($post['Images']) && !empty($post['Images'])){

				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => $this->Html->url($post['Images'][0]['url'], true)),
					null,
					array('inline' => false)
				);

				//pega os detalhes da imagem
				$imageArray = @getimagesize(Router::url($post['Images'][0]['url'], true));

				//largura da imagem
				$this->Html->meta(
					array('property' => 'og:image:width', 'content' => $imageArray[0]),
					null,
					array('inline' => false)
				);
				//Altura da imagem
				$this->Html->meta(
					array('property' => 'og:image:height', 'content' => $imageArray[1]),
					null,
					array('inline' => false)
				);
				//Mime da imagem
				$this->Html->meta(
					array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
					null,
					array('inline' => false)
				);
				//Alt da imagem
				$this->Html->meta(
					array('property' => 'og:image:alt', 'content' => $post['Post']['title']),
					null,
					array('inline' => false)
				);

				if(!empty($post['Images'][0]['legenda'])){
					$figCaption = $post['Images'][0]['legenda'];
				}
				if(!empty($post['Images'][0]['credito'])){
					$figAutor = $post['Images'][0]['credito'];
				}?>

				<figure class="legended">
					<div class="img-responsive">
						<img itemprop="image" src="<?php echo $post['Images'][0]['url'].'?node_id='.$post['Post']['id']; ?>" alt="<?php echo $post['Post']['title']; ?>" />
					</div>
					<figcaption class="c-article_figcaption">
						<?php if(isset($figCaption)){
							echo $figCaption;
						}
						if(isset($figAutor)){
							echo ' (Foto: '.$figAutor.')';
						} ?>
					</figcaption>
				</figure>

			<?php 
			//se não tem imagem
			}else{
				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => Router::url('/', true).'img/bemparanafacebook.jpg'),
					null,
					array('inline' => false)
				);
			}?>

		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

					<?php echo $this->element('share');?>

					<!-- Autor e data de publicacao -->
					<div class="by-line ">
						<span class="published-date" role="date" itemprop="datePublished" content="<?php echo date('Y-m-d', strtotime($post['Post']['publish_start'])); ?>" datetime="<?php echo $post['Post']['publish_start']; ?>">
							<?php echo date('d/m/Y', strtotime($post['Post']['publish_start'])); ?> às
							<?php echo date('H:i', strtotime($post['Post']['publish_start'])); ?>
						</span>
						<?php
						//se foi atualizado
						if($post['Post']['updated'] > $post['Post']['publish_start']){ ?>
							<span class="updated-date" role="date" itemprop="dateModified" content="<?php echo $post['Post']['updated']; ?>">
								Atualizado em <?php echo date('d/m/Y', strtotime($post['Post']['updated'])); ?> às
									<?php echo date('H:i', strtotime($post['Post']['updated'])); ?>
							</span>
						<?php } ?>

						<span class="author" itemprop="author" itemscope itemtype="http://schema.org/Person">
							<span itemprop="name">
							<?php
							//se tem o usuario
							if (isset($post['Post']['jornalista']) && !empty($post['Post']['jornalista'])) {
								echo $post['Post']['jornalista'];
							}else{
								echo $post['User']['name'];
							} ?>
							</span>
						</span>
					</div>

					<div id="corpo">
						<p>
							<?php
							$corpo = $post['Post']['body'];
							//se o conteúdo é mais antigo que a data da importação
							if($post['Post']['publish_start'] < '2018-04-02 14:23:00'){
								//remove tags antigas e desnecessárias
								$corpo = $this->Mswi->removeTagsWP($corpo);
								$corpo = $this->Mswi->limpaTags($corpo);
							}		
							echo $this->Mswi->removeStyle($corpo);													
							?>
							
							<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
							<script>
								$( document ).ready(function() {
									$('#corpo img').unwrap('p').wrap('<figure>').addClass('img-responsive');
								});								
							</script>
						</p>
						
					</div>

					<div style="text-align:center;" class="blog-banners">
						<?php 
						//se tem banner em imagem
						if(isset($post['Blog']['BlogBanner']) && !empty($post['Blog']['BlogBanner'])){
							//percorre os banners
							foreach($post['Blog']['BlogBanner'] as $banner){
								echo '<div class="img-responsive" style="margin-bottom: 10px;">';
								if(!empty($banner['banner_src'])){
									echo '<a href="'.$banner['banner_link'].'" target="_blank">';
										echo '<img src="/blog_banners/'.$banner['banner_src'].'" />';
									echo '</a>';
								}else if(!empty($banner['banner_tag'])){
									echo $banner['banner_tag'];
								}
								echo '</div>';
							}							
						}	
						?>
					</div>

					<?php 
					if (isset($post['Images']) && count($post['Images']) > 1){  
						$imagemUrl = $post['Images'][0]['url'];
						?>

						<!-- GALERIA DE IMAGENS - Slider-->
						<div class="container-fluid gallery">

							<!-- CAROUSEL -->
							<div class="gallery__visualization">
								<div class="fade in" id="viewbtn0">
									<figure class="bp-media">
										<div class="img-responsive">
											<img itemprop="image" src="<?php echo $imagemUrl;?>" title="<?php echo $post['Images'][0]['legenda']; ?>" alt="<?php echo $post['Images'][0]['alt']; ?>">
										</div>
									</figure>
								</div>
							</div>

							<!-- SLIDER -->
							<div class="gallery__nav-thumbs">
								<?php						
								//percorre para montar as miniaturas
								foreach($post['Images'] as $key => $image){
									$imgUrl = $this->MediasImage->imagePreset($image['filename'], '400');
									$minUrl = $this->MediasImage->imagePreset($image['filename'], '200');
									?>
									<div id="btn<?php echo $key;?>" class="col-lg-4 col-md-4 col-sm-6" data-load-img="<?php echo $imgUrl; ?>" data-alt-img="<?php echo $image['legenda']; ?>"  data-alt-title="<?php echo $image['alt']; ?>">
										<figure>
											<div class="img-responsive">
												<img 
													itemprop="image" 
													src="<?php echo $minUrl; ?>" 
													alt="<?php echo $image['alt']; ?>" 
													title="<?php echo $image['legenda']; ?>">
											</div>
										</figure>
									</div>
								<?php } ?>
								
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php echo $this->element('facebook_comentarios');?>
		</footer>
	</article>
</section>