<?php
echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	'Assinante',
	'/users/users/login'
);
$this->Html->addCrumb(
	'Recuperar a senha',
	$this->here
);
?>
<div class="section container feed-lastnews">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<span class="headover">Recuperar a senha</span>
			<?php
			echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'forgot')));
			?>
			<div class="form-group">
				<?php 
				echo $this->Form->input(
					'email', 
					array(
						'label' => 'Digite o seu e-mail', 
						'class'=>'form-control', 
						'type' => 'email',
						'required' => true
					)
				); 
				?>
			</div>
		<button type="submit" class="btn btn-default">Enviar para o meu email</button>
	 	<?php echo $this->Form->end(); ?>
	</div>
</div>