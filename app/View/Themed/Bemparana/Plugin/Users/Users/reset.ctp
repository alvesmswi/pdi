<?php
echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	'Assinante',
	'/users/users/login'
);
$this->Html->addCrumb(
	'Alterar a senha',
	$this->here
);
?>
<div class="section container feed-lastnews">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<span class="headover">Alterar a senha</span>

			<?php if($this->Session->check('Auth.User')): ?>
				<p class="text-right"><?php echo __d('croogo', 'You are currently logged in as:') . ' ' . $this->Session->read('Auth.User.username'); ?></p>

				<ul class="nav nav-pills" style="margin-bottom: 10px;">
					<li role="presentation">
						<a href="/users/users" style="color: #333;">Meus Dados</a>
					</li>
					<li role="presentation" class="active">
						<a href="/users/users/reset/<?php echo $this->Session->read('Auth.User.username') ?>/<?php echo $this->Session->read('Auth.User.activation_key') ?>" style="background-color:#ad3189;">
							Resetar Senha
						</a>
					</li>
					<li role="presentation">
						<a href="/users/users/logout" style="color: #333;">
							Sair
						</a>
					</li>
				</ul>
			<?php endif; ?>

			<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
			<div class="form-group">
				<?php 
				echo $this->Form->input(
					'password', 
					array(
						'label' => 'Digite a nova senha', 
						'class'=>'form-control', 
						'type' => 'password',
						'required' => true
					)
				); 
				?>
			</div>
			<div class="form-group">
				<?php 
				echo $this->Form->input(
					'verify_password', 
					array(
						'label' => 'Confirme a nova senha', 
						'class'=>'form-control', 
						'type' => 'password',
						'required' => true
					)
				); 
				?>
			</div>
			<button type="submit" class="btn btn-default">Alterar a senha</button>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>