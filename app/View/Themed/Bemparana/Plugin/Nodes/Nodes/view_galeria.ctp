<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

//configura os breadscrumbs
$this->Html->addCrumb(
	'Galeria de imagens', 
	'/'.$node['Node']['type']
); 
?>
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<h1 itemprop="name">
					<?php echo $node['Node']['title']; ?>
				</h1>		
			</div>

			<?php 
			if (isset($node['Multiattach']) && count($node['Multiattach']) > 1){  
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
					'dimension'		=> '600largura'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				?>

				<!-- GALERIA DE IMAGENS - Slider-->
				<div class="container-fluid gallery">

					<!-- SLIDER -->
					<div class="gallery__nav-thumbs">
						<?php	
									
						//percorre para montar as miniaturas
						foreach($node['Multiattach'] as $key => $min){
							//se não é um PDF
							if ($min['Multiattach']['mime'] != 'application/pdf') {
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $min['Multiattach']['filename'],
									'dimension'		=> 'normal'
								);
								$imgUrl = $this->Html->url($imagemUrlArray);
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach',
									'controller'	=> 'Multiattach',
									'action'		=> 'displayFile', 
									'admin'			=> false,
									'filename'		=> $min['Multiattach']['filename'],
									'dimension'		=> '300largura'
								);
								$minUrl = $this->Html->url($imagemUrlArray);
							} 
							?>
							<div id="btn<?php echo $key;?>" class="col-lg-4 col-md-4 col-sm-6" data-load-img="<?php echo $imgUrl; ?>" data-alt-img="<?php echo $min['Multiattach']['meta']; ?>"  data-alt-title="<?php echo (isset($min['Multiattach']['metaDisplay']) ? ($min['Multiattach']['metaDisplay']) : ('')); ?>" data-by="<?php echo $min['Multiattach']['comment']; ?>" data-legend="<?php echo (isset($min['Multiattach']['metaDisplay']) ? ($min['Multiattach']['metaDisplay']) : ('')); ?>">
								<figure>
									<div class="img-responsive">
										<img 
											itemprop="image" 
											src="<?php echo $minUrl; ?>" 
											alt="<?php echo $min['Multiattach']['meta']; ?>">
									</div>
								</figure>
							</div>
						<?php } ?>
					</div>

					<!-- CAROUSEL -->
					<div class="gallery__visualization">
						<img class="ajax-loader hidden" src="/images/ajax-loader.gif" style="position: absolute; z-index:999; top: 50%; left:48%;">
						<div class="fade in" id="viewbtn0">
							<figure class="bp-text-right">
								<div class="img-responsive">
									<img itemprop="image" src="<?php echo $imagemUrl;?>" title="<?php echo $node['Multiattach'][0]['Multiattach']['comment']; ?>" alt="<?php echo $node['Multiattach'][0]['Multiattach']['meta']; ?>">
								</div>
								<figcaption>
								<?php 
									if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){ ?>
										<span itemprop="description"><?php echo $node['Multiattach'][0]['Multiattach']['metaDisplay']; ?></span>
									<?php } ?>
									<?php 
									if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){ ?>
										<cite itemprop="author"> - <?php echo 'Foto: '.$node['Multiattach'][0]['Multiattach']['comment']; ?></cite>
									<?php } ?>									
								</figcaption>
							</figure>
						</div>
					</div>
					
				</div>
			<?php } ?>	

		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

					<?php echo $this->element('share', array('formato'=>'horizontal'));?>

					<!-- Autor e data de publicacao -->
					<div class="by-line ">	
						<span class="published-date" role="date" itemprop="datePublished" content="<?php echo date('Y-m-d', strtotime($node['Node']['publish_start'])); ?>" datetime="<?php echo $node['Node']['publish_start']; ?>">
							<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
							<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
						</span>
						<?php 
						//se foi atualizado
						if($node['Node']['publish_start'] <> $node['Node']['updated']){ ?>
							<span class="updated-date" role="date" itemprop="dateModified" content="<?php echo $node['Node']['updated']; ?>"> 
								Atualizado em <?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
									<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>
							</span>
						<?php } ?>
						
						<span class="author" itemprop="author" itemscope itemtype="http://schema.org/Person">
							<span itemprop="name">
							<?php 
							//se tem o usuario
							if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
								echo $node['NoticiumDetail']['jornalista'];
							}else{
								echo $node['User']['name'];
							} ?>
							</span>
						</span>
					</div>

					<div class="">
						<?php 
						//Corpo da noticia
						echo $node['Node']['body'];
						?>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php echo $this->element('keywords_links');?>		
			<?php echo $this->element('facebook_comentarios');?>
		</footer>
	</article>
</section>