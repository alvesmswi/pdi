<?php 
echo $this->element('node_metas');
//configura os breadscrumbs
$this->Html->addCrumb(
	'Impressos', 
	$this->here
); 
?>
<!-- GUIA -->
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<div class="container guide">
		<!-- Menu guia -->
		<div class="guide__navbar row">
			<div class="col-lg-12">
			<?php 
			//Chama o elemento que filtra por periodo
			echo $this->element('filtro_periodo')?>
			</div>
		</div>

		<!-- Em Cartaz -->
		<div class="guide__in-theatre">

			<div class="row">

			<?php		
			//se tem algum registro
			if(!empty($nodes)){				
				foreach($nodes as $n){
					//se está autenticado
					if(AuthComponent::user('id')){
						$link = $n['Node']['path'];
					}else{
						$link = '/users/users/login?redirect='.$n['Node']['path'];
					}
				?>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<a href="<?php echo $link;?>" target="_blank">
						<article class="in-theatre-block">
							<header>
								<?php
								//se tem imagem
								if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
										'dimension'		=> '300largura'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
								?>
									<figure class="in-theatre__poster ">
										<img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
									</figure>
								<?php 
								} ?>							
								<div class="headline">
									<h1><?php echo $n['Node']['title'];?></h1>
									<p><?php echo $n['Node']['excerpt'];?></p>
								</div>
							</header>

						</article>
					</a>
				</div>			
			<?php 
				}
			}else{ 
				echo 'Nenhum registro encontrado';
			} 				
			?>
			</div>
			<?php echo $this->element('paginator');?>
		</div>
	</div>
</section>