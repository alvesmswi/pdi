<?php 
echo $this->element('node_metas');
//configura os breadscrumbs
$this->Html->addCrumb(
	'Guia da Cidade', 
	'/'.$this->params['type']
); 
$this->Html->addCrumb(
	$this->Mswi->slugName($this->params['named']['slug']), 
	$this->here
); 
?>
<!-- GUIA -->
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12">
	<div class="container guide">
		<!-- Menu guia -->
		<div class="guide__navbar row">
			<div class="col-lg-12">
				<nav class="navbar">
					<ul class="nav navbar-nav">
						<li class="list-group-item <?php echo $this->params['named']['slug']=='cinema' ? 'active' : ''; ?>">
							<a href="/guia_cidade/term/cinema">Cinema</a>
						</li>
						<li class="list-group-item <?php echo $this->params['named']['slug']=='teatro' ? 'active' : ''; ?>">
							<a href="/guia_cidade/term/teatro">Teatro</a>
						</li>
						<li class="list-group-item <?php echo $this->params['named']['slug']=='musica' ? 'active' : ''; ?>">
							<a href="/guia_cidade/term/musica-guia">Música</a>
						</li>
						<li class="list-group-item <?php echo $this->params['named']['slug']=='artes_visuais' ? 'active' : ''; ?>">
							<a href="/guia_cidade/term/artes-visuais">Artes Visuais</a>
						</li>
						<!--<li class="list-group-item <?php //echo $this->params['named']['slug']=='televisao' ? 'active' : ''; ?>">
							<a href="/guia_cidade/term/televisao">Televisão</a>
						</li>-->
					</ul>
				</nav>
			</div>
		</div>

		<!-- Em Cartaz -->
		<div class="guide__in-theatre">

			<?php 
			//se é cinema
			if(
				$this->params['named']['slug'] == 'cinema' || 
				$this->params['named']['slug'] == 'pre-estreias' || 
				$this->params['named']['slug'] == 'estreias' || 
				$this->params['named']['slug'] == 'em-cartaz' || 
				$this->params['named']['slug'] == 'em-breve'
			){ ?>
				<div class="row">
					<div class="col-lg-12">
						<h1>Filmes em cartaz em Curitiba</h1>
						<ul class="keywords list-group" itemscope itemtype="http://schema.org/CreativeWork">
							<h4 class="headover--label">Filtrar por:</h4>
							<li class="list-group-item">
								<a href="/guia_cidade/term/pre-estreias" class="hashtag" rel="tag" itemprop="keyword">Pré-estreias</a>
							</li>
							<li class="list-group-item">
								<a href="/guia_cidade/term/estreias" class="hashtag" rel="tag" itemprop="keyword">Estreias</a>
							</li>
							<li class="list-group-item">
								<a href="/guia_cidade/term/em-cartaz" class="hashtag" rel="tag" itemprop="keyword">Em cartaz</a>
							</li>
							<li class="list-group-item">
								<a href="/guia_cidade/term/em-breve" class="hashtag" rel="tag" itemprop="keyword">Em breve</a>
							</li>
						</ul>
					</div>
				</div>
			<?php } ?>

			<div class="container">

				<?php	
				$contador = 0;	
				//se tem algum registro
				if(!empty($nodes)){
					
					foreach($nodes as $n){
						$contador++;
						if($contador == 1){
							echo '<div class="row">';
						}
					?>				
						<div class="col-lg-3 col-md-4 col-sm-6">
							<a href="<?php echo $n['Node']['path'];?>">
								<article class="in-theatre-block">
									<header>
										<h4 class="headover--label">
											<?php 
											if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
												echo $n['NoticiumDetail']['chapeu'];
											}else{
												echo $this->Mswi->categoryName($n['Node']['terms']);
											}
											?>
										</h4>
										<?php
										//se tem imagem
										if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $n['Multiattach'][0]['Multiattach']['filename'],
												'dimension'		=> '300largura'
											);
											$imagemUrl = $this->Html->url($imagemUrlArray);
										?>
											<figure class="in-theatre__poster ">
												<img class="img-responsive" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
											</figure>
										<?php 
										} ?>							
										<div class="headline">
											<h1><?php echo $n['Node']['title'];?></h1>
											<p><?php echo $n['Node']['excerpt'];?></p>
										</div>
									</header>

								</article>
							</a>
						</div>	
					<?php 
						if($contador == 4){ 
							$contador = 0;
							echo '</div>';
						}
					}
				}else{ 
					echo 'Nenhum registro encontrado';
				} 
				if($contador <> 0 && $contador < 4){
					echo '</div>';
				}				
				?>
			</div>
			<?php echo $this->element('paginator');?>
		</div>
	</div>
</section>