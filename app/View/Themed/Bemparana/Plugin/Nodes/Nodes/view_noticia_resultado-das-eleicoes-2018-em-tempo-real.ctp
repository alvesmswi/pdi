<?php $this->Nodes->set($node); 

echo $this->element('node_metas');

// Seta variaveis de exibição padrão
$body = $node['Node']['body'];
$exibirImagens = true;
$exibirAssineJa = false;

// Se for para Bloquear a matéria Parcialmente
if ((isset($autorizado) && !$autorizado) && 
	(isset($user_agent) && !$user_agent) && $node['Node']['exclusivo'] && Configure::read('Paywall.PaywallBloqueioParcial')) {
	// Script de overflow que mostra o bloco e faz o efeito de shadow
	echo '<style>#corpo {opacity: .3;}</style>';

	// PHP para bloquear o Corpo da Notícia e as Imagens
	$text_plain = strip_tags($body, '<p><br><b>');
	$len = (strlen($text_plain) > Configure::read('Paywall.PaywallQtdeCaracteres')) ? Configure::read('Paywall.PaywallQtdeCaracteres') : (strlen($text_plain) / 2);
	$body = substr($text_plain, 0, $len);
	$exibirImagens = false;
	$exibirAssineJa = true;
}

//configura os breadscrumbs
$this->Html->addCrumb(
	'Notícias', 
	'/'.$node['Node']['type']
); 

if (isset($node['Taxonomy'][0]['parent_id']) && !empty($node['Taxonomy'][0]['parent_id']) && $node['Taxonomy'][0]['parent_id'] == Configure::read('colunas_parent_id')) {
	$this->Html->addCrumb(
		$this->Mswi->categoryName($node['Node']['terms']), 
		'/coluna/'.$this->Mswi->categorySlug($node['Node']['terms'])
	);
} else {
	$this->Html->addCrumb(
		$this->Mswi->categoryName($node['Node']['terms']), 
		'/'.$node['Node']['type'].'/term/'.$this->Mswi->categorySlug($node['Node']['terms'])
	);
}
?>
<section id="main-content" class="main-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<?php if (isset($node['Taxonomy'][0]['parent_id']) && !empty($node['Taxonomy'][0]['parent_id']) && $node['Taxonomy'][0]['parent_id'] == Configure::read('colunas_parent_id')): ?>
					<a href="/coluna/<?php echo $this->Mswi->categorySlug($node['Node']['terms']) ?>">
						<span class="headover">
							<?php 
							if(isset($node['NoticiumDetail']['chapeu']) && !empty($node['NoticiumDetail']['chapeu'])){
								echo $node['NoticiumDetail']['chapeu'];
							}else{
								echo $this->Mswi->categoryName($node['Node']['terms']);
							}
							?>
						</span>
					</a>
				<?php else: ?>
					<span class="headover">
						<?php 
						if(isset($node['NoticiumDetail']['chapeu']) && !empty($node['NoticiumDetail']['chapeu'])){
							echo $node['NoticiumDetail']['chapeu'];
						}else{
							echo $this->Mswi->categoryName($node['Node']['terms']);
						}
						?>
					</span>
				<?php endif; ?>
				<h1 itemprop="name">
					<?php echo $node['Node']['title']; ?>
				</h1>
				<?php 
				//se tem chapeu
				if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){ ?>
					<summary><?php echo $node['Node']['excerpt']; ?></summary>
				<?php } ?>	

			</div>

			<?php 
			//se tem imagem
			if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
					'dimension'		=> '600largura'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				
				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
					null,
					array('inline' => false)
				);

				//pega os detalhes da imagem
				$imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

				//largura da imagem
				$this->Html->meta(
					array('property' => 'og:image:width', 'content' => $imageArray[0]),
					null,
					array('inline' => false)
				);
				//Altura da imagem
				$this->Html->meta(
					array('property' => 'og:image:height', 'content' => $imageArray[1]),
					null,
					array('inline' => false)
				);
				//Mime da imagem
				$this->Html->meta(
					array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
					null,
					array('inline' => false)
				);
				//Alt da imagem
				$this->Html->meta(
					array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
					null,
					array('inline' => false)
				);
				
				if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
					$figCaption = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
				}
				if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){
					$figAutor = $node['Multiattach'][0]['Multiattach']['comment'];
				}
			//se não tem imagem
			}else{
				//prepara a imagem para o OG
				$this->Html->meta(
					array('property' => 'og:image', 'content' => Router::url('/', true).'img/bemparanafacebook.jpg'),
					null,
					array('inline' => false)
				);
			}?>

			<?php
			//se tem correlatas
			if(isset($relacionadas) && !empty($relacionadas)){
			?>
				<div class="related-content">
					<ul class="list-group fa-ul">
						<?php foreach($relacionadas as $relacionada) { ?>
							<li class="list-group-item">
								<i class="fa fa-li fa-arrow-right"></i>
								<a href="<?php echo $relacionada['Node']['path'];?>"><?php echo $relacionada['Node']['title'];?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
			
		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

					<?php echo $this->element('share');?>

					<!-- Autor e data de publicacao -->
					<div class="by-line ">	
						<span class="published-date" role="date" itemprop="datePublished" content="<?php echo date('Y-m-d', strtotime($node['Node']['publish_start'])); ?>" datetime="<?php echo $node['Node']['publish_start']; ?>">
							<?php echo date('d/m/Y', strtotime($node['Node']['publish_start'])); ?> às 
							<?php echo date('H:i', strtotime($node['Node']['publish_start'])); ?>
						</span>
						<?php 
						//se foi atualizado
						if($node['Node']['updated'] > $node['Node']['publish_start']){ ?>
							<span class="updated-date" role="date" itemprop="dateModified" content="<?php echo $node['Node']['updated']; ?>"> 
								Atualizado em <?php echo date('d/m/Y', strtotime($node['Node']['updated'])); ?> às 
									<?php echo date('H:i', strtotime($node['Node']['updated'])); ?>
							</span>
						<?php } ?>
						
						<span class="author" itemprop="author" itemscope itemtype="http://schema.org/Person">
							<span itemprop="name">
							<?php 
							//se tem o usuario
							if (isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])) {
								echo $node['NoticiumDetail']['jornalista'];
							}else{
								echo $node['User']['name'];
							} ?>
							</span>
						</span>
					</div>

					<div id="corpo">
						<?php 
						//se o conteúdo é mais antigo que a data da importação
						if($node['Node']['publish_start'] < '2018-04-02 14:23:00'){
							//Corpo da noticia
							$corpo = $this->Mswi->removeTagsWP($body);
							$corpo = $this->Mswi->limpaTags($corpo);
						}else{
							$corpo =  $body;
						}	
						echo $this->Mswi->lerCodigos($corpo);
						//se tem correlatas
						if(isset($nodesRelacionados) && !empty($nodesRelacionados)){
						?>
							<div class="related-news offset--x8" role="complementary">
								<h4>Leia Também</h4>
								<ul class="list-group">
									<?php foreach($nodesRelacionados as $relacionada) { ?>
										<li class="list-group-item">
											<a href="<?php echo $relacionada['Node']['path'];?>"><?php echo $relacionada['Node']['title'];?></a>
										</li>
									<?php } ?>
								</ul>
							</div>
						<?php }  ?>
						
						<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
						<script>
							$( document ).ready(function() {
								$('#corpo img').unwrap('p').wrap('<figure>').addClass('img-responsive');
							});								
						</script>
					</div>
					
					<?php 

					if ($exibirAssineJa) {
						// Chamar element de bloco de Assine agora!
						echo $this->element('paywall_bloqueio_parcial');
					}

					if ($exibirImagens):
						if (isset($node['Multiattach']) && count($node['Multiattach']) > 1){  
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach',
								'controller'	=> 'Multiattach',
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
								'dimension'		=> '600largura'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>

							<!-- GALERIA DE IMAGENS - Slider-->
							<div class="container-fluid gallery">
								<!-- CAROUSEL -->
								<div class="gallery__visualization">
									<div class="fade in" id="viewbtn0">
										<figure class="bp-media">
											<div class="img-responsive">
												<img itemprop="image" src="<?php echo $imagemUrl;?>" title="<?php echo $node['Multiattach'][0]['Multiattach']['comment']; ?>" alt="<?php echo $node['Multiattach'][0]['Multiattach']['meta']; ?>">
											</div>
										</figure>
									</div>
								</div>

								<!-- SLIDER -->
								<div class="gallery__nav-thumbs">
									<?php						
									//percorre para montar as miniaturas
									foreach($node['Multiattach'] as $key => $min){
										//se não é um PDF
										if ($min['Multiattach']['mime'] != 'application/pdf') {
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $min['Multiattach']['filename'],
												'dimension'		=> 'normal'
											);
											$imgUrl = $this->Html->url($imagemUrlArray);
											$imagemUrlArray = array(
												'plugin'		=> 'Multiattach',
												'controller'	=> 'Multiattach',
												'action'		=> 'displayFile', 
												'admin'			=> false,
												'filename'		=> $min['Multiattach']['filename'],
												'dimension'		=> '300largura'
											);
											$minUrl = $this->Html->url($imagemUrlArray);
										} 
										?>
										<div id="btn<?php echo $key;?>" class="col-lg-4 col-md-4 col-sm-6" data-load-img="<?php echo $imgUrl; ?>" data-alt-img="<?php echo $min['Multiattach']['meta']; ?>"  data-alt-title="<?php echo $min['Multiattach']['comment']; ?>">
											<figure>
												<div class="img-responsive">
													<img itemprop="image" src="<?php echo $minUrl; ?>" alt="<?php echo $min['Multiattach']['meta']; ?>" title="<?php echo $min['Multiattach']['comment']; ?>">
												</div>
											</figure>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php if ($exibirImagens): ?>
				<?php echo $this->element('keywords_links');?>		
				<?php echo $this->element('facebook_comentarios');?>
			<?php endif; ?>
		</footer>
	</article>
</section>