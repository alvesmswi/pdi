<!-- Box sociais -->
<div class="container">
	<div class="row">
	<div id="teste"></div>
		<div class="col-lg-12 col-md-6 col-sm-12">
			<!-- Newsletter box -->
			<div class="newsletter-box">
				<form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
					<div class="form-group">
						<label class="headover--label" for="to-comment">Assine nossa newsletter</label>
						<input id="email-newsletter" name="email" class="form-control" type="email" required="true">						
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-default">Receber</button>
					</div>
				</form>
			</div>
		</div>

		<div class="col-lg-12 col-md-6 col-sm-12">
			<!-- Botões curtir e compartilhar -->
			<div class="buttons-box">
				<a href="<?php echo Configure::read('Social.facebook');?>" class="btn btn-default btn--facebook" target="_blank">
					<span class="icon fa fa-lg fa-facebook"></span>Curta nossa página</a>
				<a href="<?php echo Configure::read('Social.twitter');?>" class="btn btn-default btn--twitter" target="_blank">
					<span class="icon fa fa-lg fa-twitter"></span>Siga-nos no twitter</a>
			</div>
		</div>
	</div>
</div>