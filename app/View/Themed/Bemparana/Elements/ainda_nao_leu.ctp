<?php
//se tem algum registro
if(!empty($nodesList)){ 
    ?>
    <!-- Plantão de Notícia  -->
    <div class="col-box col-box--top-daily-news container-fluid">
        <h2 class="col-box__title">
            <span>Ainda não leu?</span>
        </h2>
        <ul class="">
            <?php foreach($nodesList as $n){ 
                $imagemUrl = '';
                //se tem imagem
                if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $n['Multiattach']['filename'],
                        'dimension'		=> '300largura'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                }
                ?>
                <li class="list-group-item col-lg-12 col-md-12 col-sm-12">
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <a href="<?php echo $n['Node']['path']; ?>">  
                                    <?php 
                                    //se tem imagem
                                    if(!empty($imagemUrl)){
                                    ?>
                                        <figure class="bp-media">
                                            <div class="img-responsive">
                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
                                            </div>
                                        </figure>
                                    <?php 
                                    } ?>
                                    <h1>
                                        <?php echo $n['Node']['title']; ?>                                        
                                    </h1>
                                </a>
                            </div>
                        </header>
                    </article>
                </li>
            <?php } ?>            
        </ul>
    </div>
<?php } ?>