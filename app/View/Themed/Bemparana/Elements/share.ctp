<?php 
if(!isset($formato)){
	$formato = 'horizontal';
}
?>
<!-- Botões para compartilhar -->
<div class="share-buttons addthis_toolbox <?php echo $formato; ?>">
	<a href="#" title="Compartilhe no Facebook" class="addthis_button_facebook btn btn-share">
		<span class="fa fa-facebook" aria-label="Ícone do Facebook" role="icon"></span>
	</a>
	<a href="#" title="Compartilhe no Twitter" class="addthis_button_twitter btn btn-share">
		<span class="fa fa-twitter" aria-label="Ícone do Twitter" role="icon"></span>
	</a>
	<a href="#comentario" title="Comente" class="btn btn-share">
		<span class="fa fa-commenting" aria-label="Ícone de Comentar" role="icon"></span>
	</a>
	<a href="#" title="Compartilhe no Whatsapp" class="addthis_button_whatsapp btn btn-share">
		<span class="fa fa-whatsapp" aria-label="Ícone do Whatsapp" role="icon"></span>
	</a>
</div>