<?php
//se tem algum registro
if(!empty($nodesList)){ ?>
    <!-- Plantão de Notícia  -->
    <div class="col-box col-box--breaking-news container-fluid">
        <h2 class="col-box__title">
            <span>Plantão de Notícias</span>
        </h2>
        <ul class="list-group">
            <?php foreach($nodesList as $n){ ?>
                <li class="list-group-item">
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <span class="headover"><?php echo $this->Mswi->diasAtras(strtotime($n['Node']['publish_start']));?></span>
                                <h1>
                                    <a href="<?php echo $n['Node']['path']; ?>">    
                                        <?php echo $n['Node']['title']; ?>
                                    </a>
                                </h1>
                            </div>
                        </header>
                    </article>
                </li>
            <?php } ?>            
        </ul>
        <a href="/noticia" class="btn btn-default">Mais notícias</a>
    </div>
<?php } ?>