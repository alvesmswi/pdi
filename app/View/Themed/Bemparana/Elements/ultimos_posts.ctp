<?php 
if (isset($posts) && !empty($posts)) { ?>
	<!-- blog box -->
	<div class="col-box col-blog__stories container">
	<h2 class="col-box__title">
            <span>Blogs</span>
        </h2>
		<?php foreach($posts as $n){ ?>

			<article class="blog-box">
				<header class="blog-box__header">
					<a href="/blog/<?php echo $n['Blog']['slug']?>">
						<?php 
						if(isset($n['Blog']['image']) && !empty($n['Blog']['image'])){ ?>
							<div class="blog-box__avatar">
								<figure class="bp-avatar">
									<div class="img-responsive">
										<img src="<?php echo $n['Blog']['image']; ?>" alt="<?php echo $n['Blog']['title']; ?>" />
									</div>
								</figure>
							</div>
						<?php } ?>
						<div class="blog-box__heading-txt">
							<h3 class="blog-box__name"><?php echo $n['Blog']['title']; ?></h3>
						</div>
					</a>
				</header>
				<div class="blog-box__content row">
					<a href="/blog/<?php echo $n['Blog']['slug']?>/post/<?php echo $n['Post']['slug']?>">
						<?php 
						$classTxt = 'col-sm-12';
						?>
						<div class="blog-box__content-txt <?php echo $classTxt; ?>">
							<h1><?php echo $n['Post']['title']; ?></h1>
						</div>
					</a>
				</div>
			</article>							
		<?php } ?>
		<a href="/blogs" class="btn btn-default">Todos os Blogs</a>
	</div>
<?php } ?>