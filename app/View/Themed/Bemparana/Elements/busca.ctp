<!-- Pesquisa -->
<form class="navbar-form navbar-right" action="/search">
	<div class="form-group">
		<input type="search" class="search form-control" placeholder="Buscar" name="q" required="true">
		<button type="submit" class="btn">
			<span class="icon fa fa-search" aria-label="Buscar" role="icon"></span>
		</button>
	</div>
</form>