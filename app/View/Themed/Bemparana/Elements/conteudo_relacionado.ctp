<?php 
//se encontrou alguma resultado
if(isset($nodesRelacionados) && !empty($nodesRelacionados)){ ?>
	<h1 class="section__title">Veja também</h1>
	<div class="container">	
		<div class="row">			
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($nodesRelacionados as $n){
					$imagemUrl = '';
					$class = '';
					//se tem imagem
					if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $n['Multiattach']['filename'],
							'dimension'		=> '300largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);
						$class = 'row';
					}
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">
								<a href="<?php echo $n['Node']['path'];?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
														echo $n['NoticiumDetail']['chapeu'];
													}else{
														echo $this->Mswi->categoryName($node['Node']['terms']);
													}
													?>
												</span>
											</div>
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n['Node']['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
												if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
													echo $n['NoticiumDetail']['chapeu'];
												}else{
													echo $this->Mswi->categoryName($node['Node']['terms']);
												}
												?>
											</span>
											<h1><?php echo $n['Node']['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		</div>
	</div>
<?php } ?>