<div class="blog__author-about accordion" id="accordionAboutAuthor">
	<div class="container-fluid author-about__content">
		<h3 class="headover--label">Quem faz o blog</h3>
		<p class="content-accordion collapse" id="contentAccordionAuthor" aria-expanded="false">
			<?php
			if (isset($posts) && !empty($posts)) {
				//se tem blogueiro relacionado
				if(isset($posts[0]['Blog']['Blogueiros']) && !empty($posts[0]['Blog']['Blogueiros'])){
					foreach($posts[0]['Blog']['Blogueiros'] as $blogueiro){
						echo $blogueiro['User']['bio'];
					}
				}else{
					echo $posts[0]['Blog']['descricao'];
				}
			}
			?>
		</p>			
	</div>
	<div class="container author-about--btn-expand text-center">
		<button class="btn btn--expand btn__icon collapsed" data-toggle="collapse" data-target="#contentAccordionAuthor" data-parent="#accordionAboutAuthor" aria-expanded="false" aria-controls="contentAccordionAuthor">
			<span class="fa fa-chevron-down"></span>
		</button>
	</div>
</div>