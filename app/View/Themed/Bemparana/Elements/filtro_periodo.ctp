<div class="guide__navbar row">
	<div class="col-lg-12">
			<form method="GET">
				<div class="container-fluid form-group">
					<legend>Filtrar por período:</legend>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12">
							<select name="ano" class="form-control">
								<?php 
								$i = 0;
								while ($i <= 5) {
									$ano = date('Y')-$i;
									$selected = '';
									if(isset($this->params->query['ano']) && $this->params->query['ano'] == $ano){
										$selected = 'selected="selected"';
									}
									echo '<option '.$selected.' value="'.$ano.'">'.$ano.'</option>';
									$i++;
								}
								?>
							</select>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<select name="mes" class="form-control">
								<option disabled selected value>Todos os meses</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='01'){echo 'selected="selected"';}?> value="01">Janeiro</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='02'){echo 'selected="selected"';}?> value="02">Fevereiro</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='03'){echo 'selected="selected"';}?> value="03">Março</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='04'){echo 'selected="selected"';}?> value="04">Abril</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='05'){echo 'selected="selected"';}?> value="05">Maio</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='06'){echo 'selected="selected"';}?> value="06">Junho</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='07'){echo 'selected="selected"';}?> value="07">Julho</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='08'){echo 'selected="selected"';}?> value="08">Agosto</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='09'){echo 'selected="selected"';}?> value="09">Setembro</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='10'){echo 'selected="selected"';}?> value="10">Outubro</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='11'){echo 'selected="selected"';}?> value="11">Novembro</option>
								<option <?php if(isset($this->params->query['mes']) && $this->params->query['mes']=='12'){echo 'selected="selected"';}?> value="12">Dezembro</option>
						</select>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="container">
								<div class="row">
									<div class="col-sm-6">
										<button type="submit" class="btn btn-half">Filtrar</button>
									</div>
									<div class="col-sm-6">
										<a href="<?php echo $this->here?>" class="btn btn-half gray">Limpar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
	</div>
</div>