<?php 
//Se tem menu
if (!empty($menu['threaded'])) { ?>
    <nav id="navbar-footer" class="navbar-footer container">        
        <?php 
        $i=0;
        foreach ($menu['threaded'] as $item) {              
            //Ajustes de clearfix
            if($i==2){
                echo '<div class="clearfix visible-sm visible-xs"></div>';
            }
            if($i==3){
                echo '<div class="clearfix visible-md"></div>';
            }
            if($i==4){
                echo '<div class="clearfix visible-sm visible-xs"></div>';
            }
            $i++;
            ?>
            <div id="<?php echo $item['Link']['id'] ?>" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <h1 class="<?php echo $item['Link']['class'] ?>"><?php echo $item['Link']['title'] ?></h1>
                <ul class="list-group">
                    <?php if (!empty($item['children'])) { ?>
                        <?php foreach ($item['children'] as $child) { ?>
                            <li class="list-group-item">
                                <?php 
                                echo $this->Html->link(
                                    $child['Link']['title'], 
                                    $this->Mswi->linkToUrl($child['Link']['link']),
                                    array(
                                        'class' => $child['Link']['class']
                                    )
                                ); ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        <?php 
        } ?>        
    </nav>
<?php 
} ?>