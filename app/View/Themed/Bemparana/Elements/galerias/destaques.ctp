<?php
//se tem algum registro
if(!empty($nodesList)){
	$imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/sem-foto.jpg';
?>
	<!-- main coluna principal -->
	<div class="col-main pull-left col-lg-8 col-md-12 col-sm-12">
		<article>
			<?php 
			$n = $nodesList[0];			
			?>
			<header class="container">
				<div class="heading" role="heading">
					<a href="<?php echo $n['Node']['path'];?>">
						<span class="headover ">
							<?php 
							//se tem chapey
							if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
								echo $n['NoticiumDetail']['chapeu'];
							}else{
								echo $this->Mswi->categoryName($n['Node']['terms']);
							}; 
							?>
						</span>
						<figure class="bp-media sm-rows--x23">
							<div class="img-responsive">
								<?php 									
								//se tem imagem
								if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
									$imagemUrlArray = array(
										'plugin'		=> 'Multiattach',
										'controller'	=> 'Multiattach',
										'action'		=> 'displayFile', 
										'admin'			=> false,
										'filename'		=> $n['Multiattach']['filename'],
										'dimension'		=> '474largura'
									);
									$imagemUrl = $this->Html->url($imagemUrlArray);
								}
								?>
								<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n['Node']['title'];?>" />
							</div>
						</figure>
						<h1><?php echo $n['Node']['title'];?></h1>
					</a>
				</div>
			</header>
		</article>
	</div>

<?php } ?>