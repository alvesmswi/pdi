<?php 
if(!empty($nodesList)){

	$this->Helpers->load('Medias.MediasImage');

	//Se não foi selecionado as noticias manualmente
    if(!isset($options['destaquesHomeSelecionados'])){
        //array que vai guardar as noticias em destaques
        $arrayDestaques = array();
        $contador = 0;
        $maximo = 6;
        //percorre os nodes
        foreach($nodesList as $key => $nd){
            //pega a ordenação
            $ordem = $nd[0]['ordem'];
            //se ainda não pasosu por este item
            if(!isset($arrayDestaques[$ordem])){
                //se tem ordenação
                $arrayDestaques[$ordem] = $nodesList[$key];
                $contador++;
            }
            //se chegou no maximo
            if($contador == $maximo){
                break;
            }
        }
        //substitui os dados selecionados
        $nodesList = $arrayDestaques;
	}
	//pr($nodesList);
	$imagemUrl = '';
	$imagemBgUrl = '';
	//se tem imagem
	if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){
		//se é um post
		if($nodesList[13][0]['type'] == 'post'){
			if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){
				$imagemUrl = $this->MediasImage->imagePreset($nodesList[13][0]['filename'], '600');	
				$imagemBgUrl = $this->MediasImage->imagePreset($nodesList[13][0]['filename'], '1024');
			}				
		//Se é notícia
		}else{
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[13][0]['filename'],
				'dimension'		=> '600largura'
			);
			$imagemUrl = $this->Html->url($imagemUrlArray);	
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[13][0]['filename'],
				'dimension'		=> '1024largura'
			);
			$imagemBgUrl = $this->Html->url($imagemUrlArray);	
		}		
	} ?>

	<div class="row">
		<!-- COBERTURA - Imagem no Fundo -->
		<section class="section section--bg-img coverage-news" style="background-image:url(<?php echo $imagemBgUrl;?>)">
			<div class="section--black-opacity">
				<div class="container">
					<h1 class="section__title">
						<?php 
						if(Configure::read('Home.categoria_destaque_title')){
							echo Configure::read('Home.categoria_destaque_title');
						}else{
						?>
							<a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[13][0]['terms']);?>">
								<?php echo $this->Mswi->categoryName($nodesList[13][0]['terms']);?>
							</a>
						<?php } ?>
					</h1>
					<div class="row">
						<div class="col-lg-8 col-md-12 col-sm-12">
							<div class="bg-article">
								<a href="<?php echo $nodesList[13][0]['path']; ?>">
									<article>
										<header>
											<div class="heading" role="heading">
												<span class="headover">
													<?php 
													//se tem chapey
													if(isset($nodesList[13][0]['chapeu']) && !empty($nodesList[13][0]['chapeu'])){
														echo $nodesList[13][0]['chapeu'];
													}else{
														echo $this->Mswi->categoryName($nodesList[13][0]['terms']);
													}
													?>
												</span>
												<?php
												//se tem imagem
												if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){ ?>											
													<figure class="bp-media">
														<div class="img-responsive">
															<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[13][0]['title']; ?>" />
														</div>
													</figure>
												<?php } ?>
												<h1><?php echo $nodesList[13][0]['title']; ?></h1>
											</div>
										</header>
										<div class="clearfix"></div>
									</article>
								</a>
							</div>
						</div>
						<div class="col-box col-lg-4 col-md-12 col-sm-12">
							<div class="row">
								<div class="col-lg-12 col-md-6 col-sm-12">
									<div class="bg-article">
										<?php if(isset($nodesList[14])){ ?>
											<a href="<?php echo $nodesList[14][0]['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[14][0]['chapeu']) && !empty($nodesList[14][0]['chapeu'])){
																	echo $nodesList[14][0]['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[14][0]['terms']);
																}
																?>
															</span>
															<h1><?php echo $nodesList[14][0]['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
									</div>
								</div>
								<div class="col-lg-12 col-md-6 col-sm-12">
									<div class="bg-article">
										<?php if(isset($nodesList[15])){ ?>
											<a href="<?php echo $nodesList[15][0]['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[15][0]['chapeu']) && !empty($nodesList[15][0]['chapeu'])){
																	echo $nodesList[15][0]['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[15][0]['terms']);
																}
																?>
															</span>
															<?php
															//se é um post
															if($nodesList[15][0]['type'] == 'post'){
																if(isset($nodesList[15][0]['filename']) && !empty($nodesList[15][0]['filename'])){
																	$imagemUrl = $this->MediasImage->imagePreset($nodesList[15][0]['filename'], '300');	
																}				
															//Se é notícia
															}else{
																//se tem imagem
																if(isset($nodesList[15][0]['filename']) && !empty($nodesList[15][0]['filename'])){
																	$imagemUrlArray = array(
																		'plugin'		=> 0,
																		'controller'	=> 0,
																		'action'		=> 'displayFile', 
																		'admin'			=> false,
																		'filename'		=> $nodesList[15][0]['filename'],
																		'dimension'		=> '300largura'
																	);
																	$imagemUrl = $this->Html->url($imagemUrlArray);	
																}
															}
															//se tem imagem
															if(!empty($imagemUrl)){
																?>
																<figure class="bp-media">
																	<div class="img-responsive">
																		<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[15][0]['title']; ?>" />
																	</div>
																</figure>
															<?php } ?>
															<h1><?php echo $nodesList[15][0]['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	
<?php } ?>