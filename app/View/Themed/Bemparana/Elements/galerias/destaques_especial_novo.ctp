<?php 
if(!empty($nodesList)){

	$this->Helpers->load('Medias.MediasImage');

	//Se não foi selecionado as noticias manualmente
    if(!isset($options['destaquesHomeSelecionados'])){
        //array que vai guardar as noticias em destaques
        $arrayDestaques = array();
        $contador = 0;
        $maximo = 6;
        //percorre os nodes
        foreach($nodesList as $key => $nd){
            //pega a ordenação
            $ordem = $nd[0]['ordem'];
            //se ainda não pasosu por este item
            if(!isset($arrayDestaques[$ordem])){
                //se tem ordenação
                $arrayDestaques[$ordem] = $nodesList[$key];
                $contador++;
            }
            //se chegou no maximo
            if($contador == $maximo){
                break;
            }
        }
        //substitui os dados selecionados
        $nodesList = $arrayDestaques;
	}
	//pr($nodesList);
	$imagemUrl = '';
	$imagemBgUrl = '';
	//se tem imagem
	if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){
		//se é um post
		if($nodesList[13][0]['type'] == 'post'){
			if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){
				$imagemUrl = $this->MediasImage->imagePreset($nodesList[13][0]['filename'], '600');	
				$imagemBgUrl = $this->MediasImage->imagePreset($nodesList[13][0]['filename'], '1024');
			}				
		//Se é notícia
		}else{
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[13][0]['filename'],
				'dimension'		=> '600largura'
			);
			$imagemUrl = $this->Html->url($imagemUrlArray);	
			$imagemUrlArray = array(
				'plugin'		=> 0,
				'controller'	=> 0,
				'action'		=> 'displayFile', 
				'admin'			=> false,
				'filename'		=> $nodesList[13][0]['filename'],
				'dimension'		=> '1024largura'
			);
			$imagemBgUrl = $this->Html->url($imagemUrlArray);	
		}		
	} ?>

	<div class="row">
        <section class="section section--bg-img coverage-news" style="background-image:url(<?php echo $imagemBgUrl;?>)">
            <div class="section--black-opacity">
                <div class="container">
                    <h1 class="section__title">
                        <?php 
                        if(Configure::read('Home.categoria_destaque_title')){
                            echo Configure::read('Home.categoria_destaque_title');
                        }else{
                        ?>
                            <a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[13][0]['terms']);?>">
                                <?php echo $this->Mswi->categoryName($nodesList[13][0]['terms']);?>
                            </a>
                        <?php } ?>
                    </h1>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 submanchete">
                            <div class="bg-article">
                                <a href="<?php echo $nodesList[13][0]['path']; ?>">
									<article>
										<header>
											<div class="heading" role="heading">
												<span class="headover">
													<?php 
													//se tem chapey
													if(isset($nodesList[13][0]['chapeu']) && !empty($nodesList[13][0]['chapeu'])){
														echo $nodesList[13][0]['chapeu'];
													}else{
														echo $this->Mswi->categoryName($nodesList[13][0]['terms']);
													}
													?>
												</span>
												<?php
												//se tem imagem
												if(isset($nodesList[13][0]['filename']) && !empty($nodesList[13][0]['filename'])){ ?>											
													<figure class="bp-media">
														<div class="img-responsive">
															<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[13][0]['title']; ?>" />
														</div>
													</figure>
												<?php } ?>
												<h1><?php echo $nodesList[13][0]['title']; ?></h1>
											</div>
										</header>
										<div class="clearfix"></div>
									</article>
								</a>
                            </div>
                        </div>
                        <div class="col-box col-lg-4 col-md-12 col-sm-12" style="float:right">
                            <div class="row">
                                <div class="col-lg-12 col-md-6 col-sm-12">                                    
                                    <?php if(isset($nodesList[14])){ ?>
                                        <div class="bg-article">
                                            <a href="<?php echo $nodesList[14][0]['path']; ?>">
                                                <article>
                                                    <header class="container">
                                                        <div class="heading" role="heading">
                                                            <span class="headover">
                                                                <?php 
                                                                //se tem chapey
                                                                if(isset($nodesList[14][0]['chapeu']) && !empty($nodesList[14][0]['chapeu'])){
                                                                    echo $nodesList[14][0]['chapeu'];
                                                                }else{
                                                                    echo $this->Mswi->categoryName($nodesList[14][0]['terms']);
                                                                }
                                                                ?>
                                                            </span>
                                                            <h1><?php echo $nodesList[14][0]['title']; ?></h1>
                                                        </div>
                                                    </header>
                                                </article>
                                            </a>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="bg-article" style="background: url('https://barulhocuritiba.bemparana.com.br/img/curitiba_topo.jpg') no-repeat;">
                                            <article style="text-align:center;">
                                                <header class="container">
                                                    <a href="https://barulhocuritiba.bemparana.com.br/" target="_blank" style="display:block;">
                                                        <img src="https://www.bemparana.com.br/theme/Bemparana/images/logo-barulho.png" />
                                                    </a>
                                                </header>
                                            </article>
                                        </div>
                                    <?php } ?>                                    
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <?php 
                                    $where = "Node.type = 'guia_cidade' AND Node.status = 1 AND Node.terms LIKE '%estreias%'";
                                    $sql = "
                                    SELECT 
                                    Node.id, Node.title, Node.slug, Node.path, Multiattach.filename 
                                    FROM nodes AS Node
                                    LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
                                    WHERE $where
                                    GROUP BY Node.id 
                                    ORDER BY Node.publish_start DESC
                                    LIMIT 4;
                                    ";
                                    $filmes = $this->Mswi->query($sql, 'filmesCapa');
                                    //Se encontrou resultado
                                    if(!empty($filmes)){
                                        //se tem imagem
                                        if(isset($filmes[0]['Multiattach']['filename']) && !empty($filmes[0]['Multiattach']['filename'])){
                                            $imagemUrlArray = array(
                                                'plugin'		=> 0,
                                                'controller'	=> 0,
                                                'action'		=> 'displayFile', 
                                                'admin'			=> false,
                                                'filename'		=> $filmes[0]['Multiattach']['filename'],
                                                'dimension'		=> '300largura'
                                            );
                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                        }
                                        ?>
                                        <div id="cinema-box">
                                            <!-- Botões curtir e compartilhar -->
                                            <span class="headover">Estréias de cinema</span>
                                            <div class="col-lg-6 col-md-6 col-sm-6" style="padding:0;">
                                                <ul class="list-group">
                                                    <?php if(isset($filmes[1])) { ?>
                                                        <li class="list-group-item">
                                                            <article>
                                                                <header class="container">
                                                                    <div class="heading" role="heading">
                                                                        <h1><a href="<?php echo $filmes[1]['Node']['path']; ?>"><?php echo $filmes[1]['Node']['title']; ?></a></h1>
                                                                    </div>
                                                                </header>
                                                            </article>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if(isset($filmes[2])) { ?>
                                                        <li class="list-group-item">
                                                            <article>
                                                                <header class="container">
                                                                    <div class="heading" role="heading">
                                                                        <h1><a href="<?php echo $filmes[2]['Node']['path']; ?>"><?php echo $filmes[2]['Node']['title']; ?></a></h1>
                                                                    </div>
                                                                </header>
                                                            </article>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if(isset($filmes[3])) { ?>
                                                        <li class="list-group-item">
                                                            <article>
                                                                <header class="container">
                                                                    <div class="heading" role="heading">
                                                                        <h1><a href="<?php echo $filmes[3]['Node']['path']; ?>"><?php echo $filmes[3]['Node']['title']; ?></a></h1>
                                                                    </div>
                                                                </header>
                                                            </article>
                                                        </li>
                                                    <?php } ?>
                                                    <a href="/guia_cidade/term/cinema" class="btn btn-default">Todos os filmes</a>
                                                </ul>
                                                <!-- Botões curtir e compartilhar -->
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6" style="padding:0;">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <article>
                                                            <a href="<?php echo $filmes[0]['Node']['path']; ?>">
                                                                <figure class="bp-media">
                                                                    <div class="img-responsive">
                                                                        <?php if(isset($imagemUrl)) { ?>
                                                                            <img src="<?php echo $imagemUrl; ?>">
                                                                        <?php } ?>
                                                                    </div>
                                                                </figure>
                                                                <header class="container">
                                                                    <div class="heading" role="heading">
                                                                        <h1><a href="<?php echo $filmes[0]['Node']['path']; ?>"><b><?php echo $filmes[0]['Node']['title']; ?></b></a></h1>
                                                                    </div>
                                                                </header>
                                                            </a>
                                                        </article>
                                                    </li>
                                                </ul>
                                                <!-- Botões curtir e compartilhar -->
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="bg-article">
                                        <?php if(isset($nodesList[17])){ ?>
											<a href="<?php echo $nodesList[17][0]['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[17][0]['chapeu']) && !empty($nodesList[17][0]['chapeu'])){
																	echo $nodesList[17][0]['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[17][0]['terms']);
																}
																?>
															</span>
															<h1><?php echo $nodesList[17][0]['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="bg-article">
                                        <?php if(isset($nodesList[18])){ ?>
											<a href="<?php echo $nodesList[18][0]['path']; ?>">
												<article>
													<header class="container">
														<div class="heading" role="heading">
															<span class="headover">
																<?php 
																//se tem chapey
																if(isset($nodesList[18][0]['chapeu']) && !empty($nodesList[18][0]['chapeu'])){
																	echo $nodesList[18][0]['chapeu'];
																}else{
																	echo $this->Mswi->categoryName($nodesList[18][0]['terms']);
																}
																?>
															</span>
															<h1><?php echo $nodesList[18][0]['title']; ?></h1>
														</div>
													</header>
												</article>
											</a>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="bg-article">
                            <?php if(isset($nodesList[15])){ ?>
                                <a href="<?php echo $nodesList[15][0]['path']; ?>">
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <span class="headover">
                                                    <?php 
                                                    //se tem chapey
                                                    if(isset($nodesList[15][0]['chapeu']) && !empty($nodesList[15][0]['chapeu'])){
                                                        echo $nodesList[15][0]['chapeu'];
                                                    }else{
                                                        echo $this->Mswi->categoryName($nodesList[15][0]['terms']);
                                                    }
                                                    ?>
                                                </span>
                                                <?php
                                                $imagemUrl = '';
                                                //se é um post
                                                if($nodesList[15][0]['type'] == 'post'){
                                                    if(isset($nodesList[15][0]['filename']) && !empty($nodesList[15][0]['filename'])){
                                                        $imagemUrl = $this->MediasImage->imagePreset($nodesList[15][0]['filename'], '300');	
                                                    }				
                                                //Se é notícia
                                                }else{
                                                    //se tem imagem
                                                    if(isset($nodesList[15][0]['filename']) && !empty($nodesList[15][0]['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $nodesList[15][0]['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                }
                                                //se tem imagem
                                                if(!empty($imagemUrl)){
                                                    ?>
                                                    <figure class="bp-media">
                                                        <div class="img-responsive">
                                                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[15][0]['title']; ?>" />
                                                        </div>
                                                    </figure>
                                                <?php } ?>
                                                <h1><?php echo $nodesList[15][0]['title']; ?></h1>
                                            </div>
                                        </header>
                                    </article>
                                </a>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="bg-article">
                            <?php if(isset($nodesList[16])){ ?>
                                <a href="<?php echo $nodesList[16][0]['path']; ?>">
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <span class="headover">
                                                    <?php 
                                                    //se tem chapey
                                                    if(isset($nodesList[16][0]['chapeu']) && !empty($nodesList[16][0]['chapeu'])){
                                                        echo $nodesList[16][0]['chapeu'];
                                                    }else{
                                                        echo $this->Mswi->categoryName($nodesList[16][0]['terms']);
                                                    }
                                                    ?>
                                                </span>
                                                <?php
                                                $imagemUrl = '';
                                                //se é um post
                                                if($nodesList[16][0]['type'] == 'post'){
                                                    if(isset($nodesList[16][0]['filename']) && !empty($nodesList[16][0]['filename'])){
                                                        $imagemUrl = $this->MediasImage->imagePreset($nodesList[16][0]['filename'], '300');	
                                                    }				
                                                //Se é notícia
                                                }else{
                                                    //se tem imagem
                                                    if(isset($nodesList[16][0]['filename']) && !empty($nodesList[16][0]['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $nodesList[16][0]['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                }
                                                //se tem imagem
                                                if(!empty($imagemUrl)){
                                                    ?>
                                                    <figure class="bp-media">
                                                        <div class="img-responsive">
                                                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[16][0]['title']; ?>" />
                                                        </div>
                                                    </figure>
                                                <?php } ?>
                                                <h1><?php echo $nodesList[16][0]['title']; ?></h1>
                                            </div>
                                        </header>
                                    </article>
                                </a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php } ?>