<?php		
//se tem algum registro
if(!empty($nodesList)){
	$this->Helpers->load('Medias.MediasImage');
	//Se não foi selecionado as noticias manualmente
    if(!isset($options['destaquesHomeSelecionados'])){
        //array que vai guardar as noticias em destaques
        $arrayDestaques = array();
        $contador = 0;
        $maximo = 15;
        //percorre os nodes
        foreach($nodesList as $key => $nd){
            //pega a ordenação
            $ordem = $nd[0]['ordem'];
            //se ainda não pasosu por este item
            if(!isset($arrayDestaques[$ordem])){
                //se tem ordenação
                $arrayDestaques[$ordem] = $nodesList[$key];
                $contador++;
            }
            //se chegou no maximo
            if($contador == $maximo){
                break;
            }
        }
        //substitui os dados selecionados
        $nodesList = $arrayDestaques;
	}
	//pr($nodesList);
	?>
	<h1 class="section__title">
		<?php 
		if(Configure::read('Home.home_lista_title')){
			echo Configure::read('Home.home_lista_title');
		}else{
		?>
			<a href="/noticia/term/<?php echo $this->Mswi->categorySlug($nodesList[20][0]['terms']);?>">
				<?php echo $this->Mswi->categoryName($nodesList[20][0]['terms']);?>
			</a>
		<?php } ?>
	</h1>
	<div class="container">	
		<div class="row">			
			<ul class="list-group">
				<?php
				//percorre os nodes
				foreach($nodesList as $n){
					$imagemUrl = '';
					$class = '';

					//se é um pos
					if($n[0]['type'] == 'post'){
						//se tem imagem
						if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
							$imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
						}						
					//se é notícia
					}else{
						//se tem imagem
						if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 0,
								'controller'	=> 0,
								'action'		=> 'displayFile', 
								'admin'			=> false,
								'filename'		=> $n[0]['filename'],
								'dimension'		=> '300largura'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							$class = 'row';
						}
					}
					
					?>
					<li class="list-group-item">
						<article>
							<header class="<?php echo $class; ?>">
								<a href="<?php echo $n[0]['path'];?>">								
									<div class="heading" role="heading">
										<?php 
										//se tem imagem
										if(!empty($imagemUrl)){
										?>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span class="headover">
													<?php 
													if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
														echo $n[0]['chapeu'];
													}else{
														echo $this->Mswi->categoryName($n[0]['terms']);
													}
													?>
												</span>
											</div>
											<div class="row">										
												<?php 
												//se tem imagem
												if(!empty($imagemUrl)){
												?>
													<div class="col-lg-6 col-md-12 col-sm-12">
														<div class="container-fluid">
															<figure class="bp-media">
																<div class="img-responsive">
																	<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title'];?>">
																</div>
															</figure>
														</div>
													</div>
												<?php 
												} ?>
												<div class="col-lg-6 col-md-12 col-sm-12">
													<div class="container-fluid">
														<h1><?php echo $n[0]['title'];?></h1>
													</div>
												</div>										
											</div>
										<?php }else{ ?>
											<span class="headover">
												<?php 
												if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
													echo $n[0]['chapeu'];
												}else{
													echo $this->Mswi->categoryName($n[0]['terms']);
												}
												?>
											</span>
											<h1><?php echo $n[0]['title'];?></h1>
										<?php } ?>	
									</div>
								</a>
							</header>
						</article>
					</li>	
				<?php } ?>
			</ul>
		</div>
	</div>
<?php } ?>