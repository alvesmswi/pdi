<?php 
if(!empty($nodesList)){
	?>
	<div class="container">
		<!-- linha 1 -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<article>
					<header class="container">
						<a href="<?php echo $nodesList[0]['Node']['path']; ?>">
							<div class="heading" role="heading">
								<span class="headover">
									<?php 
									//se tem chapey
									if(isset($nodesList[0]['NoticiumDetail']['chapeu']) && !empty($nodesList[0]['NoticiumDetail']['chapeu'])){
										echo $nodesList[0]['NoticiumDetail']['chapeu'];
									}else{
										echo $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
									}
									?>
								</span>
								<?php 													
									//se tem imagem
									if(isset($nodesList[0]['Multiattach']['filename']) && !empty($nodesList[0]['Multiattach']['filename'])){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $nodesList[0]['Multiattach']['filename'],
											'dimension'		=> '600largura'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);	
									?>
									<figure class="bp-media sm-rows--x35">
										<div class="img-responsive">
											<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[0]['Node']['title']; ?>">
										</div>
									</figure>
								<?php } ?>
								<h1><?php echo $nodesList[0]['Node']['title']; ?></h1>
							</div>
						</a>
					</header>
				</article>
			</div>
		</div>
		<!-- linha 2 -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<?php 	
				if(isset($nodesList[1]) && !empty($nodesList[1])){	
					$imagemUrl = '';
					$class = 'no-img';														
					//se tem imagem
					if(isset($nodesList[1]['Multiattach']['filename']) && !empty($nodesList[1]['Multiattach']['filename'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $nodesList[1]['Multiattach']['filename'],
							'dimension'		=> '300largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);					
						$class = '';
					} ?>
					<article class="<?php echo $class; ?>">
						<header class="container">
							<div class="heading" role="heading">
								<a href="<?php echo $nodesList[1]['Node']['path']; ?>">
									<span class="headover">
										<?php 
										//se tem chapey
										if(isset($nodesList[1]['NoticiumDetail']['chapeu']) && !empty($nodesList[1]['NoticiumDetail']['chapeu'])){
											echo $nodesList[1]['NoticiumDetail']['chapeu'];
										}else{
											echo $this->Mswi->categoryName($nodesList[1]['Node']['terms']);
										}
										?>
									</span>
									<?php 
									//se tem imagem
									if(!empty($imagemUrl)) {?>
										<figure class="bp-media">
											<div class="img-responsive">
												<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[1]['Node']['title']; ?>">
											</div>
										</figure>
									<?php } ?>
									<h1><?php echo $nodesList[1]['Node']['title']; ?></h1>
								</a>
							</div>
						</header>
					</article>
				<?php } ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
			<?php 	
				if(isset($nodesList[2]) && !empty($nodesList[2])){	
					$imagemUrl = '';
					$class = 'no-img';														
					//se tem imagem
					if(isset($nodesList[2]['Multiattach']['filename']) && !empty($nodesList[2]['Multiattach']['filename'])){
						$imagemUrlArray = array(
							'plugin'		=> 'Multiattach',
							'controller'	=> 'Multiattach',
							'action'		=> 'displayFile', 
							'admin'			=> false,
							'filename'		=> $nodesList[2]['Multiattach']['filename'],
							'dimension'		=> '300largura'
						);
						$imagemUrl = $this->Html->url($imagemUrlArray);					
						$class = '';
					} ?>
					<article class="<?php echo $class; ?>">
						<header class="container">
							<div class="heading" role="heading">
								<a href="<?php echo $nodesList[2]['Node']['path']; ?>">
									<span class="headover">
										<?php 
										//se tem chapey
										if(isset($nodesList[2]['NoticiumDetail']['chapeu']) && !empty($nodesList[2]['NoticiumDetail']['chapeu'])){
											echo $nodesList[2]['NoticiumDetail']['chapeu'];
										}else{
											echo $this->Mswi->categoryName($nodesList[2]['Node']['terms']);
										}
										?>
									</span>
									<?php 
									//se tem imagem
									if(!empty($imagemUrl)) {?>
										<figure class="bp-media">
											<div class="img-responsive">
												<img src="<?php echo $imagemUrl; ?>" alt="<?php echo $nodesList[2]['Node']['title']; ?>">
											</div>
										</figure>
									<?php } ?>
									<h1><?php echo $nodesList[2]['Node']['title']; ?></h1>
								</a>
							</div>
						</header>
					</article>
				<?php } ?>
			</div>
		</div>
		<!-- n linhas ... -->
	</div>
	
<?php } ?>