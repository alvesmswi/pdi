<?php
//se tem algum registro
if(!empty($nodesList)){
    $this->Helpers->load('Medias.MediasImage');
    //Se não foi selecionado as noticias manualmente
    if(!isset($options['destaquesHomeSelecionados'])){
        //array que vai guardar as noticias em destaques
        $arrayDestaques = array();
        $contador = 0;
        $maximo = 12;
        //percorre os nodes
        foreach($nodesList as $key => $nd){
            //pega a ordenação
            $ordem = $nd[0]['ordem'];
            //se ainda não pasosu por este item
            if(!isset($arrayDestaques[$ordem])){
                //se tem ordenação
                $arrayDestaques[$ordem] = $nodesList[$key];
                $contador++;
            }
            //se chegou no maximo
            if($contador == $maximo){
                break;
            }
        }
        //substitui os dados selecionados
        $nodesList = $arrayDestaques;
    }
    //se tem nesta ordem
    if(isset($nodesList[1])){
        $n = $nodesList[1];
?>
        <!-- FULL DESTAQUE -->
        <article class="article__header home-cover special">
            <header class="container" itemprop="headline">
                <div class="heading" role="heading">
                    <span class="headover">
                        <?php 
                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                            echo $n[0]['chapeu'];
                        }else{
                            echo $this->Mswi->categoryName($n[0]['terms']);
                        }
                        ?>
                    </span>
                    <h1 itemprop="name">
                        <a href="<?php echo $n[0]['path']; ?>" rel="bookmark" title="<?php echo $n[0]['title']; ?>" href="#article-content">
                            <?php 
                            //se tem titulo de capa
                            if(isset($n[0]['titulo_capa']) && !empty($n[0]['titulo_capa'])){
                                echo $n[0]['titulo_capa'];
                            }else{
                                echo $n[0]['title']; 
                            }						
                            ?>
                        </a>
                    </h1>
                    <?php 
                    //se tem resumo
                    if(isset($node[0]['excerpt']) && !empty($node[0]['excerpt'])){ ?>
                        <summary><?php echo $node[0]['excerpt']; ?></summary>
                    <?php } ?>	

                </div>
                <div class="row">
                    <figure class="">
                        <div class="img-responsive">
                            <?php 
                            //se é um Post
                            if($n[0]['type'] == 'post'){
                                if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                    $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename']);
                                }                                
                            //Se for notícia
                            }else{
                                //se tem imagem
                                if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                    $imagemUrlArray = array(
                                        'plugin'		=> 0,
                                        'controller'	=> 0,
                                        'action'		=> 'displayFile', 
                                        'admin'			=> false,
                                        'filename'		=> $n[0]['filename'],
                                        'dimension'		=> 'normal'
                                    );
                                    $imagemUrl = $this->Html->url($imagemUrlArray);
                                }						
                            }
                            ?>
                            <img itemprop="image" src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                        </div>
                        <figcaption><?php echo $n[0]['title']; ?></figcaption>
                    </figure>
                </div>
                
            </header>
            <?php 
            $relacionadas = array();
            $relacionadas = $this->Mswi->correlatas($n[0]['id']);
            //se tem correlatas
            if(isset($relacionadas) && !empty($relacionadas)){
            ?>
                <div class="container" data-pg-collapsed="">
                    <div class="col-lg-12 col-md-12 col-sm-12 manchete related-content">
                        <div class="related-content">
                            <ul class="list-group fa-ul">
                                <?php foreach($relacionadas as $relacionada) { ?>
                                    <li class="list-group-item">
                                        <i class="fa fa-li fa-arrow-right"></i>
                                        <a href="<?php echo $relacionada['Node']['path'];?>"><?php echo $relacionada['Node']['title'];?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </article>
        
    <?php } 
    //Região de Seção Especial 1
    echo $this->Regions->blocks('secao_especial1');
    ?>

    <!-- ANUNCIO -->
	<?php
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('publicidade_top')){ ?>
		<div class="container">
			<div class="ads ads--super-banner hidden-sm">
				<span class="headover-ads rows--x2">Publicidade</span>
				<div class="content w728">
					<?php echo $this->Regions->blocks('publicidade_top');?>
				</div>
			</div>
		</div>			
	<?php }	?>
    <?php
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('publicidade_mobile_top')){ ?>
        <div class="container">
            <div class="ads ads--rectangle-medium--half visible-sm">
                <span class="headover-ads">Publicidade</span>
                <div class="content">
                    <?php echo $this->Regions->blocks('publicidade_mobile_top');?>
                </div>
            </div>		
        </div>	
    <?php }	?>
    
    <div id="feed-lastnews" class="section container feed-lastnews">

        <?php
        //se tem nesta ordem
        if(isset($nodesList[2])){
            $n = $nodesList[2];
        ?>
        
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <article>
                        <header class="container">
                            <a href="<?php echo $n[0]['path']; ?>">
                                <div class="heading" role="heading">
                                    <span class="headover">
                                        <?php 
                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                            echo $n[0]['chapeu'];
                                        }else{
                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                        }
                                        ?>
                                    </span>
                                    <?php 
                                    //se tem imagem
                                    if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                        //se é um Post
                                        if($n[0]['type'] == 'post'){
                                            if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '920');
                                            }  
                                        //Se for notícia
                                        }else{
                                            $imagemUrlArray = array(
                                                'plugin'		=> 0,
                                                'controller'	=> 0,
                                                'action'		=> 'displayFile', 
                                                'admin'			=> false,
                                                'filename'		=> $n[0]['filename'],
                                                'dimension'		=> '920largura'
                                            );
                                            $imagemUrl = $this->Html->url($imagemUrlArray);    
                                        }                                	
                                        ?>
                                        <figure class="bp-media sm-rows--x23">
                                            <div class="img-responsive">
                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                            </div>
                                        </figure>
                                    <?php } ?>
                                    <h1><?php echo $n[0]['title']; ?></h1>
                                </div>
                            </a>
                        </header>
                        <?php 
                        $relacionadas = array();
                        $relacionadas = $this->Mswi->correlatas($n[0]['id']);
                        //se tem correlatas
                        if(isset($relacionadas) && !empty($relacionadas)){
                        ?>
                            <div class="container" data-pg-collapsed="">
                                <div class="col-lg-12 col-md-12 col-sm-12 manchete related-content">
                                    <div class="related-content">
                                        <ul class="list-group fa-ul">
                                            <?php foreach($relacionadas as $relacionada) { ?>
                                                <li class="list-group-item">
                                                    <i class="fa fa-li fa-arrow-right"></i>
                                                    <a href="<?php echo $relacionada['Node']['path'];?>"><?php echo $relacionada['Node']['title'];?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </article>
                </div>
            </div>
        <?php } ?>
        
        <?php
        //se tem nesta ordem
        if(isset($nodesList[3])){
            $n = $nodesList[3];
            ?>
            <!-- main coluna principal -->
            <div class="col-main pull-left col-lg-8 col-md-12 col-sm-12">
                <article>
                    <header class="container">
                        <div class="heading" role="heading">
                            <span class="headover ">
                                <?php 
                                if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                    echo $n[0]['chapeu'];
                                }else{
                                    echo $this->Mswi->categoryName($n[0]['terms']);
                                }
                                ?>
                            </span>
                            <a href="<?php echo $n[0]['path']; ?>">
                                <?php 
                                $imagemUrl = '';
                                //se é um Post
                                if($n[0]['type'] == 'post'){
                                    if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                        $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '600');
                                    }  
                                //Se for notícia
                                }else{
                                    //se tem imagem
                                    if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                        $imagemUrlArray = array(
                                            'plugin'		=> 0,
                                            'controller'	=> 0,
                                            'action'		=> 'displayFile', 
                                            'admin'			=> false,
                                            'filename'		=> $n[0]['filename'],
                                            'dimension'		=> '600largura'
                                        );
                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                    }
                                }
                                //se tem imagem
                                if(!empty($imagemUrl)){
                                ?>
                                    <figure class="bp-media sm-rows--x23">
                                        <div class="img-responsive">
                                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                        </div>
                                    </figure>
                                <?php } ?>
                                <h1><?php echo $n[0]['title']; ?></h1>
                            </a>
                        </div>
                    </header>
                    <?php 
                    $relacionadas = array();
                    $relacionadas = $this->Mswi->correlatas($n[0]['id']);
                    //se tem correlatas
                    if(isset($relacionadas) && !empty($relacionadas)){
                    ?>
                        <div class="container" data-pg-collapsed="">
                            <div class="col-main pull-left col-lg-12 col-md-12 col-sm-12 submanchete">
                                <div class="related-content">
                                    <ul class="list-group fa-ul">
                                        <?php foreach($relacionadas as $relacionada) { ?>
                                            <li class="list-group-item">
                                                <i class="fa fa-li fa-arrow-right"></i>
                                                <a href="<?php echo $relacionada['Node']['path'];?>"><?php echo $relacionada['Node']['title'];?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </article>
            </div>
        <?php } ?>

        <!-- sidebar coluna de direita -->
        <div class="col-sidebar pull-right col-lg-4 col-md-6 col-sm-12">

            <?php echo $this->Regions->blocks('direita_home1'); ?> 

            <!-- noticias -->
            <div class="col-box container">
                <?php
                //se tem nesta ordem
                if(isset($nodesList[6])){
                    $n = $nodesList[6];
                    ?>
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <a href="<?php echo $n[0]['path']; ?>">
                                    <span class="headover">
                                        <?php 
                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                            echo $n[0]['chapeu'];
                                        }else{
                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                        }
                                        ?>
                                    </span>
                                    <?php 
                                    $imagemUrl = '';
                                    //se é um Post
                                    if($n[0]['type'] == 'post'){
                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
                                        }  
                                    //Se for notícia
                                    }else{
                                        //se tem imagem
                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                            $imagemUrlArray = array(
                                                'plugin'		=> 0,
                                                'controller'	=> 0,
                                                'action'		=> 'displayFile', 
                                                'admin'			=> false,
                                                'filename'		=> $n[0]['filename'],
                                                'dimension'		=> '300largura'
                                            );
                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                        }
                                    }

                                    //se tem imagem
                                    if(!empty($imagemUrl)){
                                    ?>
                                        <figure class="bp-media">
                                            <div class="img-responsive">
                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                            </div>
                                        </figure>
                                    <?php } ?>
                                    <h1><?php echo $n[0]['title']; ?></h1>
                                </a>
                            </div>
                        </header>
                    </article>
                <?php } ?>
                <?php
                //se tem nesta ordem
                if(isset($nodesList[9])){
                    $n = $nodesList[9];
                    ?>
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <a href="<?php echo $n[0]['path']; ?>">
                                    <span class="headover">
                                        <?php 
                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                            echo $n[0]['chapeu'];
                                        }else{
                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                        }
                                        ?>
                                    </span>
                                    <h1><?php echo $n[0]['title']; ?></h1>
                                </a>
                            </div>
                        </header>
                    </article>
                <?php } ?>
                <?php
                //se tem nesta ordem
                if(isset($nodesList[10])){
                    $n = $nodesList[10];
                    ?>
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <a href="<?php echo $n[0]['path']; ?>">
                                    <span class="headover">
                                        <?php 
                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                            echo $n[0]['chapeu'];
                                        }else{
                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                        }
                                        ?>
                                    </span>
                                    <h1><?php echo $n[0]['title']; ?></h1>
                                </a>
                            </div>
                        </header>
                    </article>
                <?php } ?>
            </div>

            <!-- ANUNCIO -->
            <?php
            //Se tem algum bloco ativo nesta região
            if ($this->Regions->blocks('publicidade_bloco_destaques')){ ?>
                <div class="ads ads--rectangle-medium">
                    <span class="headover-ads">Publicidade</span>
                    <div class="content">
                        <?php echo $this->Regions->blocks('publicidade_bloco_destaques');?>
                    </div>
                </div>
            <?php } ?>
        </div>
        

        <!-- main coluna principal - uma noticia -->
        <div class="col-main pull-left col-lg-8 col-md-6 col-sm-12">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="container-fluid">
                            <div class="row">
                            <?php
                            //se tem nesta ordem
                            if(isset($nodesList[4])){
                                $n = $nodesList[4];
                                ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php 
                                                    $imagemUrl = '';
                                                    //se é um Post
                                                    if($n[0]['type'] == 'post'){
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
                                                        }  
                                                    //Se for notícia
                                                    }else{
                                                        //se tem imagem
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrlArray = array(
                                                                'plugin'		=> 0,
                                                                'controller'	=> 0,
                                                                'action'		=> 'displayFile', 
                                                                'admin'			=> false,
                                                                'filename'		=> $n[0]['filename'],
                                                                'dimension'		=> '300largura'
                                                            );
                                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                        }
                                                    }
                
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                    ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[7])){
                                    $n = $nodesList[7];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class=" col-lg-6 ">
                        <div class="container-fluid">
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[5])){
                                    $n = $nodesList[5];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php 
                                                    $imagemUrl = '';
                                                    //se é um Post
                                                    if($n[0]['type'] == 'post'){
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
                                                        }  
                                                    //Se for notícia
                                                    }else{
                                                        //se tem imagem
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrlArray = array(
                                                                'plugin'		=> 0,
                                                                'controller'	=> 0,
                                                                'action'		=> 'displayFile', 
                                                                'admin'			=> false,
                                                                'filename'		=> $n[0]['filename'],
                                                                'dimension'		=> '300largura'
                                                            );
                                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                        }
                                                    }
                
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                    ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[8])){
                                    $n = $nodesList[8];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        //se tem nesta ordem
        if(isset($nodesList[11])){
            $n = $nodesList[11];
            ?>
                <!-- main coluna principal -->
                <div class="col-main pull-left col-lg-8 col-md-12 col-sm-12">
                    <article>
                        <header class="container">
                            <div class="heading" role="heading">
                                <span class="headover ">
                                    <?php 
                                    if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                        echo $n[0]['chapeu'];
                                    }else{
                                        echo $this->Mswi->categoryName($n[0]['terms']);
                                    }
                                    ?>
                                </span>
                                <a href="<?php echo $n[0]['path']; ?>">
                                    <?php 
                                    $imagemUrl = '';
                                    //se é um Post
                                    if($n[0]['type'] == 'post'){
                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '600');
                                        }  
                                    //Se for notícia
                                    }else{
                                        //se tem imagem
                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                            $imagemUrlArray = array(
                                                'plugin'		=> 0,
                                                'controller'	=> 0,
                                                'action'		=> 'displayFile', 
                                                'admin'			=> false,
                                                'filename'		=> $n[0]['filename'],
                                                'dimension'		=> '600largura'
                                            );
                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                        }
                                    }

                                    //se tem imagem
                                    if(!empty($imagemUrl)){
                                    ?>
                                        <figure class="bp-media sm-rows--x23">
                                            <div class="img-responsive">
                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                            </div>
                                        </figure>
                                    <?php } ?>
                                    <h1><?php echo $n[0]['title']; ?></h1>
                                </a>
                            </div>
                        </header>
                    </article>
                </div>
        <?php } ?>

        <!-- main coluna principal - uma noticia -->
        <div class="col-main pull-left col-lg-8 col-md-6 col-sm-12">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="container-fluid">
                            <div class="row">
                            <?php
                            //se tem nesta ordem
                            if(isset($nodesList[12])){
                                $n = $nodesList[12];
                                ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php 
                                                    //se é um Post
                                                    if($n[0]['type'] == 'post'){
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
                                                        }  
                                                    //Se for notícia
                                                    }else{
                                                        //se tem imagem
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrlArray = array(
                                                                'plugin'		=> 0,
                                                                'controller'	=> 0,
                                                                'action'		=> 'displayFile', 
                                                                'admin'			=> false,
                                                                'filename'		=> $n[0]['filename'],
                                                                'dimension'		=> '300largura'
                                                            );
                                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                        }
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                    ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[14])){
                                    $n = $nodesList[14];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class=" col-lg-6 ">
                        <div class="container-fluid">
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[13])){
                                    $n = $nodesList[13];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php 
                                                    //se é um Post
                                                    if($n[0]['type'] == 'post'){
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrl = $this->MediasImage->imagePreset($n[0]['filename'], '300');
                                                        }  
                                                    //Se for notícia
                                                    }else{
                                                        //se tem imagem
                                                        if(isset($n[0]['filename']) && !empty($n[0]['filename'])){
                                                            $imagemUrlArray = array(
                                                                'plugin'		=> 0,
                                                                'controller'	=> 0,
                                                                'action'		=> 'displayFile', 
                                                                'admin'			=> false,
                                                                'filename'		=> $n[0]['filename'],
                                                                'dimension'		=> '300largura'
                                                            );
                                                            $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                        }
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                    ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $n[0]['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <?php
                                //se tem nesta ordem
                                if(isset($nodesList[15])){
                                    $n = $nodesList[15];
                                    ?>
                                    <article>
                                        <header class="container">
                                            <div class="heading" role="heading">
                                                <a href="<?php echo $n[0]['path']; ?>">
                                                    <span class="headover">
                                                        <?php 
                                                        if(isset($n[0]['chapeu']) && !empty($n[0]['chapeu'])){
                                                            echo $n[0]['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($n[0]['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <h1><?php echo $n[0]['title']; ?></h1>
                                                </a>
                                            </div>
                                        </header>
                                    </article>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>