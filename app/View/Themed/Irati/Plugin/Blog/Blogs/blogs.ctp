<section class="u-spacer_bottom">
  <header class="o-header" style="position:initial">
    <span class="o-header_title">Colunistas</span>
  </header>
  <div class="u-spacer_bottom">
    <div class="row">
      <?php
      if (!empty($blogs)) {
        $contador = 0;
        $total    = count($blogs);
        foreach ($blogs as $blog){ $contador++; ?>
        <div class="col-sm-4">
          <div class="o-news">
            <a href="/coluna/<?php echo $blog['Blog']['slug']; ?>" class="o-news_category">
              <?php echo $blog['Blog']['title']; ?>
            </a>
            <?php $url = ((isset($blog['Post']['url'])) ? $this->Html->url($blog['Post']['url']) : "/coluna/{$blog['Blog']['slug']}/post/{$blog['Post']['slug']}"); ?>
            <a href="<?php echo $url ?>" class="o-news_content">
              <?php if (isset($blog['Blog']['image'])) { ?>
              <img src="<?php echo $blog['Blog']['image'] ?>" class="o-news_image o-news_image--left">
              <?php } ?>
              <?php if (!empty($blog['Post'])) { ?>
                <span class="o-news_title"><?php echo $blog['Post']['title']; ?></span>
              <?php } ?>
            </a>
          </div>
        </div>
        <?php if ($contador == 3) { 
          $contador = 0; ?>
        <div class="clearfix"></div>
        <?php }
        } 
      }else{
        echo __d('croogo', 'Nenhum registro encontrado.');
      } ?>
    </div>
    <?php echo $this->element('paginator');?>
  </div>
</section>
