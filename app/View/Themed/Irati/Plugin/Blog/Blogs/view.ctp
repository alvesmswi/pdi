<?php
  $this->Html->meta(
    array('property' => 'og:title', 'content' => $post['Post']['title']),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:description', 'content' => (isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt']) ? $post['Post']['excerpt'] : "")),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:url', 'content' => Router::url(null, true)),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('name' => 'twitter:card', 'content' => 'summary'),
    null,
    array('inline'=>false)
  );
  //se tem chamada
  if(isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){
    $description = $post['Post']['excerpt'];
  }else{
    $description = substr($post['Post']['body'],0,128);
    $description = strip_tags($description);
  }
  //gera o description
  $this->Html->meta(
    array(
      'name' => 'description ', 
      'content' => $description
    ),
    null,
    array('inline'=>false)
  );
  //Gera as keywords
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $this->Mswi->gerarKeywords($post['Post']['title'].' '.$description)
		),
		null,
		array('inline'=>false)
	);
?>

<section class="s-article">
  <article class="c-article">
    <header class="u-spacer_bottom o-header" style="position:initial">
      <?php if (isset($post['Blog']['capa'])) { ?>
      <img src="<?php echo $post['Blog']['capa'] ?>" alt="<?php echo $post['Blog']['title']; ?>">
      <?php } else { ?>
        <a href="/coluna/<?php echo $post['Blog']['slug']; ?>" class="o-header_title o-header_title--large">
          <?php echo $post['Blog']['title']; ?>
        </a>
      <?php } ?>
    </header>
    <header class="o-post_header">
      <span class="o-post_title"><?php echo $post['Post']['title'] ?></span>
      <div class="o-post_byline">
        <time class="o-post_datetime">
          <?php echo date('d/m/Y H:i', strtotime($post['Post']['publish_start'])) ?>
        </time>
        <a href="/" class="o-post_author">
          <?php echo $post['User']['name'] ?>
        </a>
      </div>
    </header>
    <div class="row">
      <div class="col-md-12">
        <div class="c-article_content">
          <?php 
          //se tem imagem
          $this->Helpers->load('Medias.MediasImage');

          if (isset($post['Images'][0])) { 
            //prepara a imagem para o OG
			$this->Html->meta(
				array('property' => 'og:image', 'content' => Router::url($post['Images'][0]['url'], true)),
				null,
				array('inline' => false)
			);

			//pega os detalhes da imagem
			$imageArray = @getimagesize(Router::url($post['Images'][0]['url'], true));

			//largura da imagem
			$this->Html->meta(
				array('property' => 'og:image:width', 'content' => $imageArray[0]),
				null,
				array('inline' => false)
			);
			//Altura da imagem
			$this->Html->meta(
				array('property' => 'og:image:height', 'content' => $imageArray[1]),
				null,
				array('inline' => false)
			);
			//Mime da imagem
			$this->Html->meta(
				array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
				null,
				array('inline' => false)
			);
			//Alt da imagem
			$this->Html->meta(
				array('property' => 'og:image:alt', 'content' => $post['Post']['title']),
				null,
				array('inline' => false)
			);

            ?>
          	<figure class="c-article_figure c-article_figure--left">
            	<img src="<?php echo $this->MediasImage->imagePreset($post['Images'][0]['filename'], '400'); ?>" alt="<?php echo $post['Images'][0]['alt']; ?>" class="c-article_image">
            	<figcaption class="c-article_figcaption">
	            	<?php 
	            	if(!empty($post['Images'][0]['legenda'])){
						$figCaption = $post['Images'][0]['legenda'];
					}
					if(!empty($post['Images'][0]['credito'])){
						$figAutor = $post['Images'][0]['credito'];
					}
	            	if(isset($figCaption)){
	            		echo $figCaption; 
					}
	            	if(isset($figAutor)){
	            		echo ' (Foto: '.$figAutor.')'; 
	            	} ?>
	        	</figcaption>
          	</figure>
          	
          <?php } 
          echo $post['Post']['body'];
              
          //Galeria de imagem
          if(isset($post['Images']) && count($post['Images']) > 1){ ?>
          <div class="c-gallery">
            <div class="js-slick c-gallery_large">
              <?php foreach($post['Images'] as $image) { ?>
              <div class="c-gallery_item c-gallery_item--large">
                <figure class="c-gallery_figure">
                  <img src="<?php echo $this->MediasImage->imagePreset($image['filename'], '834'); ?>" alt="<?php echo $image['alt']; ?>" class="c-gallery_image">
                  <figcaption class="c-article_figcaption">
	            	<?php 
	            	if(!empty($image['legenda'])){
						$figCaption = $image['legenda'];
					}
					if(!empty($image['credito'])){
						$figAutor = $image['credito'];
					}
	            	if(isset($figCaption)){
	            		echo $figCaption; 
					}
	            	if(isset($figAutor)){
	            		echo ' (Foto: '.$figAutor.')'; 
	            	} ?>
	        	</figcaption>
                </figure>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        </div>
        <!-- <footer class="o-post_footer">
          <div class="addthis_inline_share_toolbox"></div>
        </footer> -->
      </div>
    </div>
  </article>
</section>