<?php
$poll = $this->requestAction('/polls/polls/get_poll');
$poll_array = json_decode($poll, true);
if(!empty($poll_array)){
    echo $this->Html->css('Polls.poll');
    ?>
    <aside id="text-10" class="widget widget_text">
        <div class="title-section" style="    margin-bottom: 0px;">
            <h2><span>Enquete</span></h2>
        </div>
        <div class="o-poll_form">
            <legend class="o-poll_title">
                <div>
                    <p><?php echo $poll_array['question'] ?></p>
                </div>
            </legend>
            <?php if(!empty($poll_array['description'])){ // inicio if descrição?>
                <div class="o-poll_descrition">
                    <p><?php echo $poll_array['description'];?></p>
                </div>
            <?php } // fim if destrição?>
            <?php
            echo $this->Form->create(
                null,
                array(
                    'class'=>'o-poll',
                    'method'=>'POST',
                    'url'=>'/polls/polls/submit_poll'
                )
            );
            echo $this->Form->hidden('PollVote.poll_id', array('value' => $poll_array['poll_id']));
            //se já foi votado
            if($poll_array['graph']){
                foreach ($poll_array['answers'] as $key => $option) {
                    ?>

                    <label for='<?php echo $option['id'];?>'><?php echo $option['answer'];?></label>

                    <div class="progress progress-info">
                        <div class="bar" style="width: <?php echo $option['percent'];?>%"><?php echo $option['percent'];?>%</div>
                    </div>

                    <?php
                }
            }else{
                //percorre as respostas
                foreach ($poll_array['answers'] as $key => $option) { ?>
                    <div class="radio">
                        <label>
                            <input type="radio" name="data[PollVote][poll_option_id]" id="optionsRadios<?php echo $key; ?>" value="<?php echo $option['id']; ?>">
                            <p class="o-poll_label_text"><?php echo $option['answer']; ?></p>
                        </label>
                    </div>

                    <?php
                }
            } ?>
            <?php
            //se já foi votado
            if($poll_array['graph']){
                ?>
                <div class="o-poll_footer-item">
                    <a class="btn btn-success" href="/polls/polls/index/<?php echo $poll_array['poll_id']; ?>">Resultado Parcial</a>
                </div>
            <?php }else{ ?>
                <div class="o-poll_footer-item">
                    <button type="submit" class="o-poll_btn gform_button btn btn-success "><span class="js-onsubmit_text">Enviar</span></button>
                </div>
            <?php } ?>
            <?php echo $this->Form->end();?>
        </div>

    </aside>

<?php } ?>