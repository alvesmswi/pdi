<?php
// Adicionado 06/10/2017 - Para exibir somente nos termos especificados
/*if (!empty($block['Block']['params'])) { // Se o block possui params
	$bparams = explode("\n", $block['Block']['params']); // Explode na quebra de linha 
	$retorno = count($bparams); // por padrão 0, se maior que zero exibe
	foreach ($bparams as $key => $param) { // percorre os parametros
		if (strpos($param, 'term:') !== false) { // Se possui param com 'term:':
			$term = trim(substr($param, strpos($param, ':') + 1)); // guarda o termo

			if (isset($this->params->action) && $this->params->action === 'term') { // Se for uma lista:
				if ($this->params->slug !== $term) { // Se o termo for diferente dos que estão nos params:
					$retorno -= 1; // diminui
				}
			}

			if (isset($node) && !empty($node['Node']['terms'])) { // Se for um node:
				$node_terms = $node['Node']['terms']; // guarda os termos do Node

				if (strpos($node_terms, $term) === false) { // Se o termo do Node não for igual ao termo:
					$retorno -= 1; // diminui
				}
			}
		}
	}
	
	if ($retorno <= 0) { // Se o retorno for negativo ou 0
		return false;
	}
}*/

$this->set(compact('block'));
$b = $block['Block'];
$class = 'block block-' . $b['alias'];
if ($block['Block']['class'] != null) {
	$class .= ' ' . $b['class'];
}
?>
<?php
	echo $this->Layout->filter($b['body'], array(
		'model' => 'Block', 'id' => $b['id']
	));
?>
