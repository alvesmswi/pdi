<?php 
$this->start('classificados_header');
echo '<div class="container">';
  echo '<div class="o-wrapper">'; ?>
    <div class="o-classificados_header">
      <h2 class="o-classificados_header-title">
        <i class="i-icon i-icon_<?php echo $classificado['Classificado']['icone'] ?>"></i>
        <?php echo $classificado['Classificado']['title'] ?>
      </h2>
      <form method="GET" action="/classificados/search" class="o-classificados_header-form">
        <label class="sr-only" for="classificadosHeaderSearch">Pesquisar</label>
        <input type="text" class="o-classificados_header-input" name="q" id="classificadosHeaderSearch" placeholder="Pesquisar nos classificados">
        <button type="submit" class="o-classificados_header-btn"><i class="i-icon i-icon_search"></i>Pesquisar</button>
        <div class="o-classificados_header-backdrop" aria-hidden="true"></div>
      </form>
    </div>

    <!-- <div class="o-classificados_filters-panel">
      <a class="o-classificados_filters-toggle" role="button" data-toggle="collapse" href="#filtrosClassificados" aria-expanded="false" aria-controls="filtrosClassificados">Filtros <i class="i-icon i-icon_caret-down"></i></a>
      <div id="filtrosClassificados" class="collapse">
        <div class="o-classificados_filters">
          <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Transação <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">Venda</a></li>
              <li><a href="/">Locação</a></li>
              <li><a href="/">Lançamento</a></li>
            </ul>
          </div>
          <div class="dropdown"><a href="/classificados-lista" class="dropdown-toggle"><i class="i-icon i-icon_close"></i> Transação</a></div>
          <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Transação <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">Venda</a></li>
              <li><a href="/">Locação</a></li>
              <li><a href="/">Lançamento</a></li>
            </ul>
          </div>
          <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Transação <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">Venda</a></li>
              <li><a href="/">Locação</a></li>
              <li><a href="/">Lançamento</a></li>
            </ul>
          </div>
          <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Transação <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">Venda</a></li>
              <li><a href="/">Locação</a></li>
              <li><a href="/">Lançamento</a></li>
            </ul>
          </div>
          <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Transação <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">Venda</a></li>
              <li><a href="/">Locação</a></li>
              <li><a href="/">Lançamento</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div> -->
  <?php
  echo '</div>';
echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<div class="o-classificados_list">
  <header class="o-classificados_list-header">
    <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Ordenar por <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <?php $classificado = $this->params['slug']; ?>
        <li><a href="/classificados/<?php echo $classificado ?>/sort:publish_start/direction:desc">Mais recentes</a></li>
        <li><a href="/classificados/<?php echo $classificado ?>/sort:preco/direction:asc">Menor valor</a></li>
        <li><a href="/classificados/<?php echo $classificado ?>/sort:preco/direction:desc">Maior valor</a></li>
      </ul>
    </div><span class="o-classificados_n-anuncios"><?php echo $total_anuncios ?> Anúncios</span>
  </header>
  <div class="row">
    <?php foreach ($anuncios as $key => $anuncio) { ?>
      <div class="col-sm-4 col-xs-6">
        <div class="o-classificados">
          <a href="<?php echo $this->Html->url($anuncio['Anuncio']['url']) ?>" class="o-classificados_content">
            <?php if(isset($anuncio['Images'][0])) { ?>
              <img src="<?php echo $this->MediasImage->imagePreset($anuncio['Images'][0]['filename'], '258'); ?>" class="o-classificados_image"> 
            <?php } ?>
            <span class="o-classificados_title"><?php echo $anuncio['Anuncio']['title'] ?></span>
            <span class="o-classificados_price"><?php echo $anuncio['Anuncio']['preco'] ?></span>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php 
  $this->Paginator->options['url'] = array(
    'controller' => 'classificados', 'action' => 'view_classificado', 
    'classificado' => $this->params['classificado'], 'slug' => $this->params['slug']
  );
  echo $this->element('paginator');
  ?>
</div>
