<?php 
$this->start('classificados_header');
echo '<div class="container">';
  echo '<div class="o-wrapper">'; ?>
    <div class="o-classificados_header">
      <h2 class="o-classificados_header-title">
        <i class="i-icon i-icon_<?php echo $anuncio['Categoria']['Classificado']['icone'] ?>"></i>
        <?php echo $anuncio['Categoria']['Classificado']['title'] ?>
      </h2>
      <form method="GET" action="/classificados/search" class="o-classificados_header-form">
        <label class="sr-only" for="classificadosHeaderSearch">Pesquisar</label>
        <input type="text" class="o-classificados_header-input" name="q" id="classificadosHeaderSearch" placeholder="Pesquisar nos classificados">
        <button type="submit" class="o-classificados_header-btn"><i class="i-icon i-icon_search"></i>Pesquisar</button>
        <div class="o-classificados_header-backdrop" aria-hidden="true"></div>
      </form>
    </div>
  <?php
  echo '</div>';
echo '</div>';  
$this->end();
?>

<article class="c-classificados_post">
  <header class="o-news_header c-classificados_header">
    <a href="<?php echo $this->Html->url($anuncio['Categoria']['url']) ?>" class="o-news_category c-classificados_category">
      <?php echo $anuncio['Categoria']['title'] ?>
    </a>
    <div class="c-classificados_title-wrapper">
      <h1 class="c-classificados_title"><?php echo $anuncio['Anuncio']['title'] ?></h1>
      <div class="o-classificados_price c-classificados_price"><?php echo $anuncio['Anuncio']['preco'] ?></div>
    </div>
    <div class="c-classificados_byline">
      <div class="c-classificados_sharebar"></div>
      <div class="c-classificados_pubdate">
        <time datetime=""><?php echo date('d/m/Y à\s H:i', strtotime($anuncio['Anuncio']['publish_start'])) ?></time> – Atualizado em
        <time datetime=""><?php echo date('d/m/Y à\s H:i', strtotime($anuncio['Anuncio']['updated'])) ?></time>
      </div>
    </div>
  </header>
  <div class="c-classificados_post-content">
    <?php if (isset($anuncio['Images'])) { 
      $this->Helpers->load('Medias.MediasImage'); ?>
      <div class="c-gallery">
        <div class="js-slick c-gallery_large">
          <?php foreach ($anuncio['Images'] as $key => $image) { ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <img src="<?php echo $this->MediasImage->imagePreset($image['filename'], '834'); ?>" class="c-gallery_image">
              </figure>
            </div>
          <?php } ?>
        </div>
        <div class="js-slick c-gallery_nav">
          <?php foreach ($anuncio['Images'] as $key => $image) { ?>
            <div class="c-gallery_item c-gallery_item--large">
              <figure class="c-gallery_figure">
                <img src="<?php echo $this->MediasImage->imagePreset($image['filename'], '147'); ?>" class="c-gallery_image">
              </figure>
            </div>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
    <?php echo $anuncio['Anuncio']['body'] ?>
  </div>
</article>
