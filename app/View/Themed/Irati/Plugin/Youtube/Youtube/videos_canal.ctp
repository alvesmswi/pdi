
<header class="o-header" style="position:initial">
      <span class="o-header_title o-header_title--large">Vídeos do Canal</span>
</header>

<?php 
	if(!empty($results)){
        echo "<div class='row'>";
		foreach($results->items as $item){
			if(isset($item->id->videoId)){
				$data = explode('T', $item->snippet->publishedAt);
				$data = date('d/m/Y', strtotime($data[0]));
				echo '<div class="col-lg-4">
						<iframe width="230" height="130" src="https://www.youtube.com/embed/'.$item->id->videoId.'" frameborder="0" allowfullscreen></iframe>
						<div style="margin-bottom: 15px;">
							<h5 style="margin-bottom: 2px;margin-top: 2px;">' . mb_strimwidth($item->snippet->title, 0, 30, "...") . '</h5>
							<small>' . $data . '</small>	
						</div>	
					</div>';
			}
        }
        echo "</div>";
	}
?>