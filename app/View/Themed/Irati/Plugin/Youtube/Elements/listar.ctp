<?php 
$results = Cache::read('videos_youtube_home');
if(empty($results)){
	App::uses('HttpSocket', 'Network/Http');
	$API_key    = Configure::read('Youtube.api_key');
	$maxResults = Configure::read('Youtube.max_lista');
	$channelID = Configure::read('Youtube.id_canal');

	$HttpSocket = new HttpSocket();

	$results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
		'order' => 'date',
		'part' => 'snippet',
		'channelId' => $channelID,
		'maxResults' => $maxResults,
		'key' => $API_key
	));
	$results = json_decode($results->body);
	Cache::write('videos_youtube_home', $results); 
}
?>

<div class="title-section" style="margin-top:20px">
	<h1>
		<span>Vídeos</span>
	</h1>
</div>
<div class="row">
	<div class="col-md-12">
		<?php 
			if(!empty($results)){
				foreach($results->items as $item){
					if(isset($item->id->videoId)){
						$data = explode('T', $item->snippet->publishedAt);
						$data = date('d/m/Y', strtotime($data[0]));
			?>
			<div class="col-md-4" style="margin-bottom:30px">
				<div class="lazyload">
					<article class="post">
						<iframe width="230" height="130" src="https://www.youtube.com/embed/<?php echo $item->id->videoId; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen ></iframe>
						<h5><?php echo mb_strimwidth($item->snippet->title, 0, 30, "...") ?></h5>
						<small><?php echo $data ?></small>
					</article>
				</div>
			</div>	
			<?php }}?>
			<a href="/youtube/youtube/videos_canal" style="text-decoration: none;"><span class="btn_vermais">Ver mais!</span></a>
			<?php }?>
	</div>
</div>