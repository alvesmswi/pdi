<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );

//se tem chamada
if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
	$description = $node['Node']['excerpt'];
}else{
	$description = substr($node['Node']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);

echo $this->element('node_metas');

//Se foi informado o nome do jornalista
if(isset($node['NoticiumDetail']['jornalista']) && !empty($node['NoticiumDetail']['jornalista'])){
    $autor = $node['NoticiumDetail']['jornalista'];
}else{
    $autor = $node['User']['name'];
}
?>
<div class="single-post-box <?php echo $node['Node']['type']; ?>">
    <div class="title-post"> 
        <h1>
            <?php echo $node['Node']['title']; ?>
        </h1> 
        <ul class="post-tags">
            <li><i class="fa fa-clock-o"></i><?php echo strftime('%d de %B de %Y', strtotime($node['Node']['publish_start']));?></li>
            <li><i class="fa fa-user"></i>por <?php echo $autor; ?></a></li>
            <!-- <li><i class="fa fa-eye"></i><?php // echo $node['Node']['lida']; ?></li> -->
        </ul>
        <div class="addthis_inline_share_toolbox"></div>
    </div>   
    <div class="the-content"> 
        <?php                 
        if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
            $imagemUrlArray = array(
                'plugin'		=> 'Multiattach',
                'controller'	=> 'Multiattach',
                'action'		=> 'displayFile', 
                'admin'			=> false,
                'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
                'dimension'		=> '300largura'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
            if(isset($node['Multiattach'][0]['Multiattach']['comment']) && !empty($node['Multiattach'][0]['Multiattach']['comment'])){
                $imagemAutor = $node['Multiattach'][0]['Multiattach']['comment'];
            }                        
            if(isset($node['Multiattach'][0]['Multiattach']['metaDisplay']) && !empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
                $imagemTitle = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
            }
            //prepara a imagem para o OG
            $this->Html->meta(
                array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
                null,
                array('inline' => false)
            );
            
            //pega os detalhes da imagem
            $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

            //largura da imagem
            $this->Html->meta(
                array('property' => 'og:image:width', 'content' => $imageArray[0]),
                null,
                array('inline' => false)
            );
            //Altura da imagem
            $this->Html->meta(
                array('property' => 'og:image:height', 'content' => $imageArray[1]),
                null,
                array('inline' => false)
            );
            //Mime da imagem
            $this->Html->meta(
                array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
                null,
                array('inline' => false)
            );
            //Alt da imagem
            $this->Html->meta(
                array('property' => 'og:image:alt', 'content' => $node['Node']['title']),
                null,
                array('inline' => false)
            );
            ?>
            <figure class="wp-caption alignleft">
                <img 
                src="<?php echo $imagemUrl; ?>" 
                class="size-medium lazyloaded" 
                title="<?php echo $node['Node']['title']; ?>"/>
                <?php if(isset($imagemTitle) || isset($imagemAutor)) { ?>
                    <figcaption class="wp-caption-text"
                        <?php if(isset($imagemTitle)) {
                            echo $imagemTitle.' ';
                        }
                        if(isset($imagemAutor)){
                            echo '(Foto: '.$imagemAutor .')';
                        }
                        ?>
                    </figcaption>
                <?php } ?>
            </figure>
        <?php }

        //Corpo da noticia
        echo $node['Node']['body'];

        ?>
        <!--COMENTÁRIOS DO FACEBOOK-->
        <div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].'/'.$node['Node']['type'].'/'.$node['Node']['slug']?>" data-numposts="5"></div>
    </div>
</div>