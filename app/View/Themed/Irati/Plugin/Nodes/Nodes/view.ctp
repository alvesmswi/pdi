<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
//se tem chamada
if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
	$description = $node['Node']['excerpt'];
}else{
	$description = substr($node['Node']['body'],0,128);
	$description = strip_tags($description);
}
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => $description
	),
	null,
	array('inline'=>false)
);
?>
<section class="cf block news-block">
    <div class="block-content ts-row">
        <div class="col-12 large cf">
            <article class="grid-post post type-post format-standard has-post-thumbnail <?php echo $node['Node']['type']; ?>">
                <div class="meta-title">
                    <div class="post-meta post-meta-c">                                                                    
                        <h2 class="post-title-alt" style="font-size: 24px;">
                            <?php echo $node['Node']['title']; ?>
                        </h2> 
                    </div>
                </div>
                <?php                 
                if(isset($node['Multiattach']) && !empty($node['Multiattach'])){
                    $imagemUrlArray = array(
                        'plugin'		=> 'Multiattach',
                        'controller'	=> 'Multiattach',
                        'action'		=> 'displayFile', 
                        'admin'			=> false,
                        'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
                        'dimension'		=> 'normal'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    if(isset($node['Multiattach'][0]['Multiattach']['comment']) && !empty($node['Multiattach'][0]['Multiattach']['comment'])){
                        $imagemAutor = $node['Multiattach'][0]['Multiattach']['comment'];
                    }                        
                    if(isset($node['Multiattach'][0]['Multiattach']['metaDisplay']) && !empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){
                        $imagemTitle = $node['Multiattach'][0]['Multiattach']['metaDisplay'];
                    }
                    ?>
                    <div class="post-thumb"> 
                        <div class="lazyload">
                            <!--
                            <img 
                            src="<?php echo $imagemUrl; ?>" 
                            class="attachment-cheerup-grid size-cheerup-grid wp-post-image" 
                            title="<?php echo $node['Node']['title']; ?>"/> 
                            -->
                        </div>
                        <?php if(isset($imagemTitle) || isset($imagemAutor)) { ?>
                            <figcaption style="text-align: left;">
                                <?php if(isset($imagemTitle)) { 
                                    echo $imagemTitle.' '; 
                                }
                                if(isset($imagemAutor)){
                                    echo '(Foto: '.$imagemAutor .')';
                                }
                                ?>                            
                            </figcaption> 
                        <?php } ?>                                                            
                    </div>  
                <?php } ?> 
                <div class="post-content cf corpo">
                    <?php 
                    //Corpo da noticia
             	    echo $node['Node']['body'];
                    ?>        
                </div>
            </article>
        </div>                                                
    </div>                                            
</section>