<?php
//se tem algum registro
if(!empty($nodes)){ 
	$page = $this->params->query['page'];
	$proximaPagina = $page + 1;
	$item = 0;
	$contador = 0;
	$total = count($nodes);
	//percorre os nodes
	foreach($nodes as $n){ 
		//Se tem imagem
		if(isset($n['Multiattach'][0]['Multiattach']['filename']) && !empty($n['Multiattach'][0]['Multiattach']['filename'])) {
			$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
		}
		if($item == 0){
			echo '<div class="row">';
		}
		?>
		<div class="standard-post2 col-sm-12 col-md-6 col-lg-6">
			<?php                 
			if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach',
					'controller'	=> 'Multiattach',
					'action'		=> 'displayFile', 
					'admin'			=> false,
					'filename'		=> $n['Multiattach']['filename'],
					'dimension'		=> '300largura'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				if(isset($n['Multiattach']['comment']) && !empty($n['Multiattach']['comment'])){
					$imagemAutor = $n['Multiattach']['comment'];
				}                        
				if(isset($n['Multiattach']['metaDisplay']) && !empty($n['Multiattach']['metaDisplay'])){
					$imagemTitle = $n['Multiattach']['metaDisplay'];
				}
				?>
				<div class="post-gallery">
					<div class="thumb-wrap">
						<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
							<img 
							data-lazy-src="<?php echo $imagemUrl; ?>" 
							class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyloaded" 
							title="<?php echo isset($imagemTitle) ? $imagemTitle : $n['Node']['title'] ?>"  />
						</a>
					</div>
				</div>
			<?php } ?>
			<div class="post-title">					
				<h2>
					<a href="<?php echo $n['Node']['path']; ?>">
						<?php echo $n['Node']['title']; ?>
					</a>
				</h2> 
				<ul class="post-tags">
					<li><i class="fa fa-clock-o"></i><?php echo strftime('%d de %B de %Y', strtotime($n['Node']['publish_start']));?></li>
				</ul>
			</div>
		</div>
	<?php	
		$item++;
		$contador++;
		if(
			$item == 2 ||
			$contador == $total
		){
			echo '</div>';
			$item = 0;
		}
	}
}

//se tem algum registro
if(!empty($nodes)){
	echo $this->Mswi->verMaisAjax('editorias_ajax',$this->params->query['slug'], $proximaPagina, 6);
}
?>