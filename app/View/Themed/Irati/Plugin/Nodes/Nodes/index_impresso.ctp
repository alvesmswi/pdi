<?php echo $this->element('node_metas'); ?>
<section class="cf block blog">
    <div class="title-section">
			<h1>
				<span class="widget-title">
					<?php
					//se teve uma pesquisa
					if(isset($this->params['type']) && !empty($this->params['type'])) {
						echo $this->Mswi->typeName($this->params['type']);
					}else{
						echo'Resultado da pesquisa';
					}
					?>
				</span>
			</h1>
		</div>
    <div class="block-content">
        <div class="posts-dynamic posts-container ts-row list count-0">
            <div class="posts-wrap">
				<?php
				//se tem algum registro
				if(!empty($nodes)){
					//percorre os nodes
					foreach($nodes as $n){ 
						//pr($n);
						if(isset($n['Multiattach']) && !empty($n['Multiattach'])) {
							if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
								$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
							}
						}
						?>
						<div class="col-lg-4">
							<article id="post-<?php echo $n['Node']['id']; ?>" class="list-post">					
								<div class="post-meta post-meta-c">
									<h2 class="post-title">
										<a href="<?php echo $n['Node']['path']; ?>">
											<?php echo $n['Node']['title']; ?>
										</a>
									</h2> 
									<?php                 
									if(isset($n['Multiattach']) && !empty($n['Multiattach'])){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach',
											'controller'	=> 'Multiattach',
											'action'		=> 'displayFile', 
											'admin'			=> false,
											'filename'		=> $n['Multiattach']['filename'],
											'dimension'		=> '300largura'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										if(isset($n['Multiattach']['comment']) && !empty($n['Multiattach']['comment'])){
											$imagemAutor = $n['Multiattach']['comment'];
										}                        
										if(isset($n['Multiattach']['metaDisplay']) && !empty($n['Multiattach']['metaDisplay'])){
											$imagemTitle = $n['Multiattach']['metaDisplay'];
										}
										?>
										<div class="widget widget-subscribe">
											<a href="<?php echo $n['Node']['path']; ?>" class="image-link">
												<div class="lazyload">
													<img width="270" height="180" 
													src="<?php echo $imagemUrl; ?>" 
													class="attachment-cheerup-list size-cheerup-list wp-post-image" 
													title="<?php echo isset($imagemTitle) ? $imagemTitle : $n['Node']['title'] ?>"  />
												</div>
											</a>
										</div>
									<?php } ?>
								</div>
							</article>
						</div>
					<?php
          			} 
				}else{
					echo 'Nenhum registro encontrado.';
				} ?>
			</div>
		</div>
       <?php //Insere o paginado
		echo $this->element('paginator');?>
    </div>
 </section>