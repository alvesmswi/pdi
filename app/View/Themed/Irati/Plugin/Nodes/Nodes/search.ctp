<div class="title-section">
    <h2>
        <span>
            <?php 
            //se teve uma pesquisa
            if(isset($this->params->query['q']) && !empty($this->params->query['q'])){
                echo __d('croogo', 'Resultado da pesquisa para "'.$this->params->query['q'].'"');
            }else{
                echo __d('croogo', 'Resultado da pesquisa');
            }
            ?>
        </span>    
    </h2>    
</div>
<style>
    .gsc-control-cse{
        line-height: normal;
    }
    .gsc-control-cse form{
        line-height: 0;
    }
    .gsc-adBlock{
        display: none;
    }
    .gsc-control-cse table{
        margin: 0;
    }
</style>
<script>
    (function() {
        var cx = '<?php echo Configure::read('Site.busca_google')?>';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();
</script>
<gcse:search></gcse:search>