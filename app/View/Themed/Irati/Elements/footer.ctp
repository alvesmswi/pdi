<div class="container">
	<div class="footer-widgets-part">
		<div class="row">
			<div class="col-md-3">
				<aside id="media_image-2" class="widget widget_media_image">
                    <?php
                    // se imagem existe
                    if (file_exists(WWW_ROOT.Configure::read('Site.logo_footer'))){
                    echo '<div class="logo-footer" style="background-image: url('. Configure::read('Site.logo_footer') .')"></div>';
                    }else{
                        echo '<div  class="logo-footer" >'.Configure::read('Site.title').'</div>';
                    }
                    ?>
				</aside>
				<aside id="text-2" class="widget widget_text">
					<div class="textwidget">
						<p><?php echo Configure::read('Site.tagline');?></p>
					</div>
				</aside>
				<aside id="text-3" class="widget widget_text">
					<div class="textwidget">
						<ul class="social-icons">
							<?php if(Configure::read('Social.facebook')) { ?>
								<li><a href="<?php echo Configure::read('Social.facebook'); ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
							<?php } ?>
							<?php if(Configure::read('Social.youtube')) { ?>
								<li><a href="<?php echo Configure::read('Social.youtube'); ?>" class="youtube"><i class="fa fa-youtube"></i></a></li>
							<?php } ?>
							<?php if(Configure::read('Social.instagram')) { ?>
								<li><a href="<?php echo Configure::read('Social.instagram'); ?>" class="instagram"><i class="fa fa-instagram"></i></a>
							<?php } ?>
							<?php if(Configure::read('Social.twitter')) { ?>
								<li><a href="<?php echo Configure::read('Social.twitter'); ?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
							<?php } ?>
						</ul>
					</div>
				</aside>
			</div>
			<div class="col-md-3">
				<?php echo $this->Regions->blocks('footer_publicidade'); ?>  
			</div>
			<div class="col-md-3"> 
				<?php echo $this->Menus->menu('sitemap', array('element' => 'menus/sitemap')); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Regions->blocks('footer_sobre'); ?>  
				
			</div>
		</div>
	</div>
	<div class="footer-last-line">
		<div class="row">
			<div class="col-md-6">
				<p>Desenvolvido por <a href="http://www.publicadordigital.com.br/" target="_blank">MSWI</a></p>
			</div>
			<div class="col-md-6">
				<nav class="footer-nav"></nav>
			</div>
		</div>
	</div>
</div>