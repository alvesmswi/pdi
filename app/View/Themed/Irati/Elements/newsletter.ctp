<aside id="text-10" class="widget widget_text">
	<div class="title-section">
		<h2><span>Newsletter</span></h2>
	</div>
	<div class="textwidget">
		<div class='gf_browser_chrome gform_wrapper' id='gform_wrapper_1' >
			<form method='post' id='gform_1' action='/mail_chimp/news/subscribe/ultimas-noticias'>
				<div class='gform_body'>
					<label class='gfield_label' for='input_1_1'>E-mail<span class='gfield_required'>*</span></label>
					<div class='ginput_container ginput_container_email'>
						<input name='email' id='email' type='email' value='' class='large' required="true"/>
					</div>
				</div>
				<div class='gform_footer top_label'>
					<input type='submit' id='gform_submit_button_1' class='gform_button button' value='ASSINAR'/>
				</div>
			</form>
		</div>
		<br />		
		<p>Receba nossas atualizações, novidades e conteúdo exclusivo assinando a Newsletter</p>
	</div>
</aside>