<aside id="search-2" class="widget widget_search">
	<div class="title-section">
		<h2><span>PESQUISAR</span></h2>
	</div>
	<div class="search-widget">
		<form method="get" class="search-form" action="/search" >
			<input type="text" name="q" placeholder="Buscar aqui" />
			<button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
		</form>
	</div>
</aside>