<?php
//se tem algum registro
if(!empty($nodesList)){
	?>
	<div class="ticker-news-box">
		<span class="breaking-news">Urgentes</span>
		<ul id="js-news" data-rtl="ltr">
			<?php foreach($nodesList as $n){ ?>
				<li class="news-item">
					<span class="time-news">
						<?php echo date('H:i', strtotime($n['Node']['publish_start'])); ?>
					</span> 
					<a href="<?php echo $n['Node']['path']; ?>">
						<?php echo $n['Node']['title']; ?>
					</a>
				</li>	
			<?php } ?>		
		</ul>
	</div>
<?php } ?>