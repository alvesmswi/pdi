<?php 
//se encontrou alguma resultado
if(isset($vejatambem) && !empty($vejatambem)){ ?>
	<br />
	<div class="title-section">
		<h1>
			<span>Veja Também</span>
		</h1>
	</div>
	<?php
	$item = 0;
	$contador = 0;
	$total = count($vejatambem);
	//percorre os nodes
	foreach($vejatambem as $key => $n){ 
		//Se tem imagem
		if(isset($n['Multiattach']['filename']) && !empty($n['Multiattach']['filename'])) {
			if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
				$n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
			}
		}
		if($item == 0){
			echo '<div class="row">';
		}
		?>
		<div class="textwidget col-sm-12 col-md-6 col-lg-6">
			<div class="item">
				<ul class="list-posts">
					<li>
						<?php 
						//Se tem imagem
						if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
								'filename'		=> $n['Multiattach']['filename'],
								'dimension'		=> 'categoriaMin'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="thumb-wrap">
								<a href="<?php echo $n['Node']['path']; ?>">
									<img 
									src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
									data-lazy-src="<?php echo $imagemUrl; ?>" 
									class="attachment-second size-second wp-post-image" />
								</a>
							</div>
						<?php } ?>
						<div class="post-content">
							<h2>
								<a href="<?php echo $n['Node']['path']; ?>">
									<?php echo $n['Node']['title']; ?>
								</a>
							</h2>
							<ul class="post-tags">
								<li>
									<i class="fa fa-clock-o"></i>
									<?php echo strftime('%d de %B de %Y', strtotime($n['Node']['publish_start']));?>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<?php 
		$item++;
		$contador++;
		if(
			$item == 2 ||
			$contador == $total
		){
			echo '</div>';
			$item = 0;
		}			
	} ?>
<?php } ?>