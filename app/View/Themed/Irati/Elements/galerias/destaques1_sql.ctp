<?php
if(!empty($nodesList)){
	$n = $nodesList;
?>

	<div class="iso-call heading-news-box">
		<div class="image-slider snd-size">
			<span class="top-stories">Destaques do dia</span>
			<ul class="bxslider">
				<li>
					<div class="news-post image-post">
						<?php 
						//Se tem imagem
						if(isset($n[0]['Multiattach']) && !empty($n[0]['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
								'filename'		=> $n[0]['Multiattach']['filename'],
								'dimension'		=> 'destaqueFull'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="thumb-wrap">
								<img 
								src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
								data-lazy-src="<?php echo $imagemUrl; ?>" 
								class="attachment-second size-second wp-post-image" />
							</div>
						<?php } ?>
						<div class="hover-box">
							<div class="inner-hover">
								<a class="category-post <?php echo $n[0]['Editoria']['slug']; ?>" href="<?php echo $n[0]['Editoria']['slug']; ?>">
									<?php echo $n[0]['Editoria']['title']; ?>
								</a>
								<h2>
									<a href="<?php echo $n[0]['Node']['path']; ?>">
										<?php echo $n[0]['Node']['title']; ?>
									</a>
								</h2>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="news-post image-post">
						<?php 
						//Se tem imagem
						if(isset($n[1]['Multiattach']) && !empty($n[1]['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
								'filename'		=> $n[1]['Multiattach']['filename'],
								'dimension'		=> 'destaqueFull'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="thumb-wrap">
								<img 
								src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
								data-lazy-src="<?php echo $imagemUrl; ?>" 
								class="attachment-second size-second wp-post-image" />
							</div>
						<?php } ?>
						<div class="hover-box">
							<div class="inner-hover">
								<a class="category-post <?php echo $n[1]['Editoria']['slug']; ?>" href="<?php echo $n[1]['Editoria']['slug']; ?>">
									<?php echo $n[1]['Editoria']['title']; ?>
								</a>
								<h2>
									<a href="<?php echo $n[1]['Node']['path']; ?>">
										<?php echo $n[1]['Node']['title']; ?>
									</a>
								</h2>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="news-post image-post">
						<?php 
						//Se tem imagem
						if(isset($n[2]['Multiattach']) && !empty($n[2]['Multiattach']['filename'])){
							$imagemUrlArray = array(
								'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
								'filename'		=> $n[2]['Multiattach']['filename'],
								'dimension'		=> 'destaqueFull'
							);
							$imagemUrl = $this->Html->url($imagemUrlArray);
							?>
							<div class="thumb-wrap">
								<img 
								src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
								data-lazy-src="<?php echo $imagemUrl; ?>" 
								class="attachment-second size-second wp-post-image" />
							</div>
						<?php } ?>
						<div class="hover-box">
							<div class="inner-hover">
								<a class="category-post <?php echo $n[2]['Editoria']['slug']; ?>" href="<?php echo $n[2]['Editoria']['slug']; ?>">
									<?php echo $n[2]['Editoria']['title']; ?>
								</a>
								<h2>
									<a href="<?php echo $n[2]['Node']['path']; ?>">
										<?php echo $n[2]['Node']['title']; ?>
									</a>
								</h2>
							</div>
						</div>
					</div>
				</li>				
			</ul>
		</div>
		<div class="news-post image-post default-size">
			<?php 
			//Se tem imagem
			if(isset($n[3]['Multiattach']) && !empty($n[3]['Multiattach']['filename'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
					'filename'		=> $n[3]['Multiattach']['filename'],
					'dimension'		=> 'destaqueMin'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				?>
				<div class="thumb-wrap">
					<img 
					src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
					data-lazy-src="<?php echo $imagemUrl; ?>" 
					class="attachment-second size-second wp-post-image" />
				</div>
			<?php } ?>
			<div class="hover-box">
				<div class="inner-hover">
					<a class="category-post <?php echo $n[3]['Editoria']['slug']; ?>" href="<?php echo $n[3]['Editoria']['slug']; ?>">
						<?php echo $n[3]['Editoria']['title']; ?>
					</a>
					<h2>
						<a href="<?php echo $n[3]['Node']['path']; ?>">
							<?php echo $n[3]['Node']['title']; ?>
						</a>
					</h2>
				</div>
			</div>
		</div>
		<div class="news-post image-post default-size">
			<?php 
			//Se tem imagem
			if(isset($n[4]['Multiattach']) && !empty($n[4]['Multiattach']['filename'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
					'filename'		=> $n[4]['Multiattach']['filename'],
					'dimension'		=> 'destaqueMin'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				?>
				<div class="thumb-wrap">
					<img 
					src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
					data-lazy-src="<?php echo $imagemUrl; ?>" 
					class="attachment-second size-second wp-post-image" />
				</div>
			<?php } ?>
			<div class="hover-box">
				<div class="inner-hover">
					<a class="category-post <?php echo $n[4]['Editoria']['slug']; ?>" href="<?php echo $n[4]['Editoria']['slug']; ?>">
						<?php echo $n[4]['Editoria']['title']; ?>
					</a>
					<h2>
						<a href="<?php echo $n[4]['Node']['path']; ?>">
							<?php echo $n[4]['Node']['title']; ?>
						</a>
					</h2>
				</div>
			</div>
		</div>
		<div class="news-post image-post default-size">
			<?php 
			//Se tem imagem
			if(isset($n[5]['Multiattach']) && !empty($n[5]['Multiattach']['filename'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
					'filename'		=> $n[5]['Multiattach']['filename'],
					'dimension'		=> 'destaqueMin'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				?>
				<div class="thumb-wrap">
					<img 
					src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
					data-lazy-src="<?php echo $imagemUrl; ?>" 
					class="attachment-second size-second wp-post-image" />
				</div>
			<?php } ?>
			<div class="hover-box">
				<div class="inner-hover">
					<a class="category-post <?php echo $n[5]['Editoria']['slug']; ?>" href="<?php echo $n[5]['Editoria']['slug']; ?>">
						<?php echo $n[5]['Editoria']['title']; ?>
					</a>
					<h2>
						<a href="<?php echo $n[5]['Node']['path']; ?>">
							<?php echo $n[5]['Node']['title']; ?>
						</a>
					</h2>
				</div>
			</div>
		</div>
		<div class="news-post image-post default-size">
			<?php 
			//Se tem imagem
			if(isset($n[6]['Multiattach']) && !empty($n[6]['Multiattach']['filename'])){
				$imagemUrlArray = array(
					'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
					'filename'		=> $n[6]['Multiattach']['filename'],
					'dimension'		=> 'destaqueMin'
				);
				$imagemUrl = $this->Html->url($imagemUrlArray);
				?>
				<div class="thumb-wrap">
					<img 
					src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
					data-lazy-src="<?php echo $imagemUrl; ?>" 
					class="attachment-second size-second wp-post-image" />
				</div>
			<?php } ?>
			<div class="hover-box">
				<div class="inner-hover">
					<a class="category-post <?php echo $n[6]['Editoria']['slug']; ?>" href="<?php echo $n[6]['Editoria']['slug']; ?>">
						<?php echo $n[6]['Editoria']['title']; ?>
					</a>
					<h2>
						<a href="<?php echo $n[6]['Node']['path']; ?>">
							<?php echo $n[6]['Node']['title']; ?>
						</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
<?php } ?>