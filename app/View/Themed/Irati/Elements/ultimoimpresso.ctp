<?php
//se tem algum registro
if(!empty($nodesList)){
    $imagemUrl      = 'http://placehold.it/195x314';
    $linkpublish    = $nodesList[0]['Node']['path'];
    $target = '';
    //se tem attach
    if (isset($nodesList[0]['Multiattach']) && !empty($nodesList[0]['Multiattach'])) {
        $imagemUrlArray = array(
            'plugin'        => 'Multiattach',
            'controller'    => 'Multiattach',
            'action'        => 'displayFile', 
            'admin'         => false,
            'filename'      => $nodesList[0]['Multiattach']['filename'],
            'dimension'     => '300largura'
        );
        $imagemUrl = $this->Html->url($imagemUrlArray);
    }
    if (isset($nodesList[0]['PageFlip']) && !empty($nodesList[0]['PageFlip'])) {
        $imagemUrl = $nodesList[0]['PageFlip']['capa'];
    }
    if(isset($nodesList[0]['ImpressoDetail']['link']) && !empty($nodesList[0]['ImpressoDetail']['link'])){
        $linkpublish = $nodesList[0]['ImpressoDetail']['link'];
        $target = '_blank';
    }
    ?>
    <div class="title-section">
        <h1>
            <span class="widget-title">Edição Impressa</span>
        </h1>
    </div>
    <div class="widget widget-subscribe">
        <a href="<?php echo $linkpublish ?>">
            <div class="lazyload">
                <img src="<?php echo $imagemUrl; ?>"/>
            </div>
        </a>
    </div>                                                        
<?php } ?>