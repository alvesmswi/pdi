<?php 
if(!empty($nodesList)){
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}
?>
	<aside id="text-8" class="widget widget_text">
		<div class="title-section">
			<h2>
				<span>
					<a href="/ultimas-noticias/<?php echo $editoriaSlug; ?>">
						<?php 
						if($block['Block']['show_title']){
							echo $block['Block']['title'];
						}else{
							echo $editoriaTitle;					
						}
						?>
					</a>
				</span>
			</h2>
		</div>
		<div class="textwidget">
			<div class="item" id="list_load">
				<ul class="list-posts">
					<?php foreach($nodesList as $n) { ?>
						<li>
							<?php 
							//Se tem imagem
							if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])){
								$imagemUrlArray = array(
									'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
									'filename'		=> $n['Multiattach']['filename'],
									'dimension'		=> 'categoriaMin'
								);
								$imagemUrl = $this->Html->url($imagemUrlArray);
								?>
								<div class="thumb-wrap">
									<a href="<?php echo $n['Node']['path']; ?>">
										<img 
										src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
										data-lazy-src="<?php echo $imagemUrl; ?>" 
										class="attachment-second size-second wp-post-image" />
									</a>
								</div>
							<?php } ?>
							<div class="post-content">
								<h2>
									<a href="<?php echo $n['Node']['path']; ?>">
										<?php echo $n['Node']['title']; ?>
									</a>
								</h2>
								<ul class="post-tags">
									<li>
										<i class="fa fa-clock-o"></i>
										<?php echo strftime('%d de %B de %Y', strtotime($n['Node']['publish_start']));?>
									</li>
								</ul>
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</aside>				
<?php } ?>