<?php 
//Se tem menus
if (!empty($menu['threaded'])) { ?>
	<ul data-breakpoint="800" id="menu-topo" class="top-line-list top-menu">
		<?php
		//percorre os itens 
		foreach ($menu['threaded'] as $item) { 
			//trata o link
			$link = $this->Mswi->linkToUrl($item['Link']['link']);
			?>
			<li id="menu-item-<?php echo $item['Link']['id']; ?>" class="dotted-left <?php echo $item['Link']['class']; ?>">
				<a href="<?php echo $this->Html->url($link); ?>">
					<?php echo $item['Link']['title']; ?>
				</a>
			</li>
		<?php } ?>
	</ul>
<?php } ?>