<?php
//Se tem itens no menu
if(!empty($menu['threaded'])){ 
	?>
	<ul data-breakpoint="800" id="menu-principal" class="nav navbar-nav navbar-left">
		<?php foreach($menu['threaded'] as $item) { 
			$url = $this->Mswi->linkToUrl($item['Link']['link']);
			$active = '';
			if($this->here == $this->Html->url($url)){
				$active = 'active';
			}
			?>
			<li id="menu-item-<?php echo $item['Link']['id']; ?>" 
			class="menu-item menu-item-type-custom menu-item-object-custom <?php echo $active; ?>">
				<a title="<?php echo $item['Link']['title']; ?>" href="<?php echo $this->Html->url($url) ?>">
					<?php echo $item['Link']['title']; ?>
				</a>
			</li>
		<?php } ?>
	</ul>
<?php } ?>