<?php if (!empty($menu['threaded'])) { ?>
    <aside id="categories-3" class="widget widget_categories">
        <h2 class="widget-title">Escolha por categoria</h2>
        <ul>
            <?php foreach ($menu['threaded'] as $item) { 
                $link = $this->Mswi->linkToUrl($item['Link']['link']);
                ?>
                <li class="cat-item ">
                    <a href="<?php echo $this->Html->url($link) ?>" ><?php echo $item['Link']['title'] ?></a>
                </li>
            <?php } ?>
        </ul>
    </aside>
<?php } ?>
