<aside id="sfsi-widget-2" class="widget sfsi">
	<div class="title-section">
		<h2><span>Siga-nos</span></h2>
	</div>
	<div class="sfsi_widget" data-position="widget">
		<div id='sfsi_wDiv'></div>
		<div class="norm_row sfsi_wDiv"  style="width:225px;text-align:center;position:absolute;">
			<div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '>
				<div class='inerCnt'>
					<a class=' sficn' effect='fade_in' target='_blank'  href='https://folha.desenvolvimentode.site/feed/' id='sfsiid_rss' alt='RSS' style='opacity:0.6' >
						<img alt='RSS' title='RSS' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_rss.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   />
						<noscript><img alt='RSS' title='RSS' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_rss.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></noscript>
					</a>
				</div>
			</div>
			<div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '>
				<div class='inerCnt'>
					<a class=' sficn' effect='fade_in' target='_blank'  href='https://www.facebook.com/FolhaDeIrati/' id='sfsiid_facebook' alt='Facebook' style='opacity:0.6' >
						<img alt='Facebook' title='Facebook' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_facebook.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   />
						<noscript><img alt='Facebook' title='Facebook' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_facebook.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></noscript>
					</a>
					<div class="sfsi_tool_tip_2 fb_tool_bdr sfsiTlleft" style="width:62px ;opacity:0;z-index:-1;margin-left:-47.5px;" id="sfsiid_facebook">
						<span class="bot_arow bot_fb_arow"></span>
						<div class="sfsi_inside">
							<div  class='icon1'>
								<a href='https://www.facebook.com/FolhaDeIrati/' target='_blank'>
									<img alt='Facebook' title='Facebook' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/visit_icons/facebook.png'  />
									<noscript><img alt='Facebook' title='Facebook' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/visit_icons/facebook.png'  /></noscript>
								</a>
							</div>
							<div  class='icon2'>
								<div class="fb-like" data-href="https://folhadeirati.com.br" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<div  class='icon3'>
								<div class="fb-share-button" data-href="https://folhadeirati.com.br" data-layout="button"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '>
				<div class='inerCnt'>
					<a class=' sficn' effect='fade_in' target='_blank'  href='https://twitter.com/folhadeirati' id='sfsiid_twitter' alt='Twitter' style='opacity:0.6' >
						<img alt='Twitter' title='Twitter' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_twitter.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   />
						<noscript><img alt='Twitter' title='Twitter' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_twitter.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></noscript>
					</a>
				</div>
			</div>
			<div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '>
				<div class='inerCnt'>
					<a class=' sficn' effect='fade_in' target='_blank'  href='https://www.youtube.com/channel/UCKbp3RWoquVJxI7g-1LHxLg' id='sfsiid_youtube' alt='YouTube' style='opacity:0.6' >
						<img alt='YouTube' title='YouTube' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_youtube.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   />
						<noscript><img alt='YouTube' title='YouTube' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_youtube.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></noscript>
					</a>
				</div>
			</div>
			<div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '>
				<div class='inerCnt'>
					<a class=' sficn' effect='fade_in' target='_blank'  href='https://www.instagram.com/jornalfolhadeirati/' id='sfsiid_instagram' alt='Instagram' style='opacity:0.6' >
						<img alt='Instagram' title='Instagram' src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" data-lazy-src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_instagram.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   />
						<noscript><img alt='Instagram' title='Instagram' src='https://folhadeirati.com.br/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/thin/thin_instagram.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></noscript>
					</a>
				</div>
			</div>
		</div >
		<div id="sfsi_holder" class="sfsi_holders" style="position: relative; float: left;width:100%;z-index:-1;"></div >
		<script>jQuery(".sfsi_widget").each(function( index ) {
			if(jQuery(this).attr("data-position") == "widget")
			{
				var wdgt_hght = jQuery(this).children(".norm_row.sfsi_wDiv").height();
				var title_hght = jQuery(this).parent(".widget.sfsi").children(".widget-title").height();
				var totl_hght = parseInt( title_hght ) + parseInt( wdgt_hght );
				jQuery(this).parent(".widget.sfsi").css("min-height", totl_hght+"px");
			}
			});
		</script> 
		<div style="clear: both;"></div>
	</div>
</aside>