<?php 
if(!empty($nodesList)){
	$editoriaTitle = 'Não informado';
	$editoriaSlug = '';
	if(isset($nodesList[0]['Editoria']['title']) && !empty($nodesList[0]['Editoria']['title'])){
		$editoriaTitle = $nodesList[0]['Editoria']['title'];
		$editoriaSlug = $nodesList[0]['Editoria']['slug'];
	}	
?>

	<div class="title-section">
		<h1>
			<span>
				<a href="/ultimas/noticias/<?php echo $editoriaSlug; ?>">
					<?php echo $editoriaTitle; ?>
				</a>
			</span>
		</h1>
	</div>
	<div class="vc_row wpb_row vc_inner vc_row-fluid grid-box">
		<div class="container">
			<div class="row">
				<div class="wpb_column vc_column_container vc_col-sm-6">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="news-post image-post2">
								<div class="post-gallery">
									<?php 
									//Se tem imagem
									if(isset($nodesList[0]['Multiattach']) && !empty($nodesList[0]['Multiattach']['filename'])){
										$imagemUrlArray = array(
											'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
											'filename'		=> $nodesList[0]['Multiattach']['filename'],
											'dimension'		=> 'categoriaFull'
										);
										$imagemUrl = $this->Html->url($imagemUrlArray);
										?>
										<div class="thumb-wrap">
											<img 
											src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
											data-lazy-src="<?php echo $imagemUrl; ?>" 
											class="attachment-second size-second wp-post-image" />
										</div>
									<?php } ?>
									<div class="hover-box">
										<div class="inner-hover">
											<a class="category-post <?php echo $nodesList[0]['Editoria']['slug']; ?>"
												href="/ultimas-noticias/<?php echo $nodesList[0]['Editoria']['slug']; ?>">
												<?php echo $nodesList[0]['Editoria']['title']; ?>
											</a>
											<h2>
												<a href="<?php echo $nodesList[0]['Node']['path']; ?>">
													<?php echo $nodesList[0]['Node']['title']; ?>
												</a>
											</h2>
											<ul class="post-tags">
												<li>
													<i class="fa fa-clock-o"></i>
													<?php echo strftime('%d de %B de %Y', strtotime($nodesList[0]['Node']['publish_start']));?>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wpb_column vc_column_container vc_col-sm-6">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="item" id="list_load">
								<ul class="list-posts">
									<?php if(isset($nodesList[1])) { ?>
										<li>
											<?php 
											//Se tem imagem
											if(isset($nodesList[1]['Multiattach']) && !empty($nodesList[1]['Multiattach']['filename'])){
												$imagemUrlArray = array(
													'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
													'filename'		=> $nodesList[1]['Multiattach']['filename'],
													'dimension'		=> 'categoriaMin'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
												?>
												<div class="thumb-wrap">
													<a href="<?php echo $nodesList[1]['Node']['path']; ?>">
														<img 
														src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
														data-lazy-src="<?php echo $imagemUrl; ?>" 
														class="attachment-second size-second wp-post-image" />
													</a>
												</div>
											<?php } ?>
											<div class="post-content">
												<h2>
													<a href="<?php echo $nodesList[1]['Node']['path']; ?>">
														<?php echo $nodesList[1]['Node']['title']; ?>
													</a>
												</h2>
												<ul class="post-tags">
													<li>
														<i class="fa fa-clock-o"></i>
														<?php echo strftime('%d de %B de %Y', strtotime($nodesList[1]['Node']['publish_start']));?>
													</li>
												</ul>
											</div>
										</li>
									<?php } ?>
								</ul>
							</div>
							<div class="item" id="list_load">
								<ul class="list-posts">
									<?php if(isset($nodesList[2])) { ?>
										<li>
											<?php 
											//Se tem imagem
											if(isset($nodesList[2]['Multiattach']) && !empty($nodesList[2]['Multiattach']['filename'])){
												$imagemUrlArray = array(
													'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
													'filename'		=> $nodesList[2]['Multiattach']['filename'],
													'dimension'		=> 'categoriaMin'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
												?>
												<div class="thumb-wrap">
													<a href="<?php echo $nodesList[2]['Node']['path']; ?>">
														<img 
														src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
														data-lazy-src="<?php echo $imagemUrl; ?>" 
														class="attachment-second size-second wp-post-image" />
													</a>
												</div>
											<?php } ?>
											<div class="post-content">
												<h2>
													<a href="<?php echo $nodesList[2]['Node']['path']; ?>">
														<?php echo $nodesList[2]['Node']['title']; ?>
													</a>
												</h2>
												<ul class="post-tags">
													<li>
														<i class="fa fa-clock-o"></i>
														<?php echo strftime('%d de %B de %Y', strtotime($nodesList[2]['Node']['publish_start']));?>
													</li>
												</ul>
											</div>
										</li>
									<?php } ?>
								</ul>
							</div>
							<div class="item" id="list_load">
								<ul class="list-posts">
									<?php if(isset($nodesList[3])) { ?>
										<li>
											<?php 
											//Se tem imagem
											if(isset($nodesList[3]['Multiattach']) && !empty($nodesList[3]['Multiattach']['filename'])){
												$imagemUrlArray = array(
													'plugin'		=> 'Multiattach','controller' => 'Multiattach','action' => 'displayFile', 'admin' => false,
													'filename'		=> $nodesList[3]['Multiattach']['filename'],
													'dimension'		=> 'categoriaMin'
												);
												$imagemUrl = $this->Html->url($imagemUrlArray);
												?>
												<div class="thumb-wrap">
													<a href="<?php echo $nodesList[3]['Node']['path']; ?>">
														<img 
														src="data:image/gif;base64,R0lGODdhAQABAPAAAP///wAAACwAAAAAAQABAEACAkQBADs=" 
														data-lazy-src="<?php echo $imagemUrl; ?>" 
														class="attachment-second size-second wp-post-image" />
													</a>
												</div>
											<?php } ?>
											<div class="post-content">
												<h2>
													<a href="<?php echo $nodesList[3]['Node']['path']; ?>">
														<?php echo $nodesList[3]['Node']['title']; ?>
													</a>
												</h2>
												<ul class="post-tags">
													<li>
														<i class="fa fa-clock-o"></i>
														<?php echo strftime('%d de %B de %Y', strtotime($nodesList[3]['Node']['publish_start']));?>
													</li>
												</ul>
											</div>
										</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<?php } ?>