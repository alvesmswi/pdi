<?php
setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese' );
date_default_timezone_set( 'America/Sao_Paulo' );
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?php echo Configure::read('Site.title'); ?> |
            <?php echo $title_for_layout; ?>
        </title>

        <?php echo $this->fetch('meta');?>

        <link rel='stylesheet' href='/theme/Irati/css/style.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/bootstrap.min.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/jquery.bxslider.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/owl.carousel.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/owl.theme.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/hotmagazine_style.css?ver=4.9.7' type='text/css' media='all' />
        <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Titillium+Web%3A400%2C700%2C300%26subset%3Dlatin%2Clatin-ext&#038;ver=1.0.0' type='text/css' media='all' />
        
        <link rel='stylesheet' href='/theme/Irati/css/js_composer.min.css?ver=4.11.2.1' type='text/css' media='all' />
        <link rel='stylesheet' href='/theme/Irati/css/masterslider.main.css?ver=3.5.1' type='text/css' media='all' />
        <link rel="stylesheet" href="/theme/Irati/css/classificados.css">

        <script type='text/javascript' src='/theme/Irati/js/jquery.js?ver=1.12.4'></script> 
        <script type='text/javascript' src='/theme/Irati/js/jquery-migrate.min.js?ver=1.4.1'></script>  

        <link rel='stylesheet' href='/theme/Irati/css/custom.css' type='text/css'/>

        <?php
        $this->Js->JqueryEngine->jQueryObject = '$';
        echo $this->Html->scriptBlock(
            'var $ = jQuery.noConflict();',
            array('inline' => true)
        );
        //Se foi inofmrado o user do analytics
        if (!empty(Configure::read('Service.analytics'))) {
            $arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
                ga('create', '<?= trim($analytics) ?>', 'auto', {
                    'name': 'id<?= $key ?>'
                });
                ga('id<?= $key ?>.send', 'pageview');
                <?php } ?>

                /**
                    * Function that tracks a click on an outbound link in Analytics.
                    * This function takes a valid URL string as an argument, and uses that URL string
                    * as the event label. Setting the transport method to 'beacon' lets the hit be sent
                    * using 'navigator.sendBeacon' in browser that support it.
                    */
                var trackOutboundLink = function (url) {
                    ga('send', 'event', 'outbound', 'click', url, {
                        'transport': 'beacon',
                        'hitCallback': function () {
                            document.location = url;
                        }
                    });
                }
            </script>
        <?php } ?>

        <script type='text/javascript'>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function () {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                    '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
        </script>

        <?php
        //Se foi informado o tempo de refresh para a Home
        if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
            <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
        <?php }

        //Se foi informado o tempo de refresh para as internas
        if (		
        $this->here != '/' && 
        (isset($node['Node']['type']) && $node['Node']['type'] == 'noticia') && 
        !empty(Configure::read('Site.refresh_internas')) && 
        is_numeric(Configure::read('Site.refresh_internas'))) { 
        ?>
            <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
        <?php }
        
        echo Configure::read('Service.header');

        //Se tem algum bloco ativo nesta região
        if($this->Regions->blocks('header_scripts')){
            echo $this->Regions->blocks('header_scripts');
        }
        ?>

    </head>
    <body class="home page-template page-template-template-home page-template-template-home-php page default _masterslider _ms_version_3.5.1 wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive">
        <?php 
        //Se está logado
        if(isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])){
            echo $this->element('admin_bar');
        } 
        $siteLogo = Configure::read('Site.logo');
        if(!empty($siteLogo)){
            $themeLogo = Router::url('/', true). $siteLogo;
        }else{								
            //Caminho absoluto do arquivo de CSS customizado
            $themeLogo = WWW_ROOT . 'img' . DS .$themeLogo;
            //se o arquivo de CSS personalizado existe
            if(file_exists($themeLogo)){
                $themeLogo = '/img/'.$themeLogo;
            }else{
                $themeLogo = '/theme/Irati/img/'.$themeLogo;
            }
        }
        ?>
        <div id="container">
            <header class="clearfix  second-style ">
                <nav class="navbar navbar-default navbar-static-top" role="navigation">
                    <?php echo $this->element('bartop'); ?>
                    <div class="logo-advertisement">
                        <div class="container">
                            <div class="navbar-header"> 
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                                    <span class="sr-only">Toggle navigation</span> 
                                    <span class="icon-bar"></span> 
                                    <span class="icon-bar"></span> 
                                    <span class="icon-bar"></span> 
                                </button> 
                                <a class="navbar-brand"  href="/" title="<?php echo Configure::read('Site.title'); ?>"> 
                                    <img src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
                                </a>
                            </div>
                            <center>Publicidade</center>
                            <p style="text-align: center;">
                                <?php echo $this->Regions->blocks('publicidade_top'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="nav-list-container">
                        <div class="container">
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <?php echo $this->Menus->menu('main', array('element' => 'menus/main')); ?>
                                <form method="get" class="navbar-form navbar-right" action="/search" >
                                    <input type="text" name="q" placeholder="Buscar aqui" />
                                    <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>

            <?php if($this->Regions->blocks('destaques')) { ?>
                <div class="vc_row wpb_row vc_row-fluid heading-news2">
                    <div class="container">
                        <div class="row">
                            <?php echo $this->Regions->blocks('destaques'); ?> 
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if($this->Regions->blocks('destaques2')) { ?>
                <section class="ticker-news">
                    <div class="container">
                        <?php echo $this->Regions->blocks('destaques2'); ?>
                    </div>
                </section>
            <?php } ?>

            <?php echo $this->fetch('classificados_header'); ?>

            <div class="vc_row wpb_row vc_row-fluid block-wrapper">
                <div class="container">
                    <div class="row">

                        <!--CONTENT-->

                        <div class="col-sm-8 content-blocker">
                            <div class="theiaStickySidebar">
                                <div class="block-content">

                                    <?php 
                                    //Exibe as mensagens
                                    echo $this->Layout->sessionFlash();

                                    //se NÃO é a Home
                                    if($this->here != '/'){                                            
                                        //Conteudo do node
                                        echo $content_for_layout;
                                    }

                                    echo $this->Regions->blocks('region2');
                                    ?> 
                                    
                                </div>
                            </div>
                        </div>

                        <!--SIDEBAR-->

                        <div class="col-sm-4 sidebar-sticky">
                            <div class="sidebar theiaStickySidebar">
                                <?php
                                    if ($this->request->controller == 'classificados' && $this->request->action == 'view_anuncio') {
							            echo $this->element('Classificados.anuncio_contato', array(), array('cache' => true));
                                    }
                                    echo $this->Regions->blocks('right'); 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer>
                <?php echo $this->Regions->blocks('footer'); ?>
            </footer>
        </div>

        <link rel='stylesheet' href='/theme/Irati/css/ticker-style-7566413c1b89011d255dd658e7bc10e0.css' data-minify="1"  type='text/css' media='all' />
        <script type='text/javascript' src='/theme/Irati/js/bootstrap.min.js?ver=4.9.7'></script> 
        <script type='text/javascript' src='/theme/Irati/js/jquery.bxslider.min.js?ver=4.9.7'></script> 
        <script type='text/javascript' src='/theme/Irati/js/imagesloaded.min.js?ver=3.2.0'></script> 
        <script type='text/javascript' src='/theme/Irati/js/isotope.pkgd.min.js?ver=4.11.2.1'></script> 
        <script type='text/javascript' src='/theme/Irati/js/owl.carousel.min.js?ver=4.9.7'></script> 
        <script type='text/javascript' src='/theme/Irati/js/ajax-9e137ad7d60d83fb8df45c094e7dbe24.js' data-minify="1"></script> 
        <script type='text/javascript' src='/theme/Irati/js/sticky-a5511262a2f327b4d4cd1c0fed0e0dc9.js' data-minify="1"></script> 
        <script type='text/javascript' src='/theme/Irati/js/theia-sticky-sidebar-f63193b94328a13f7e5aee3e95737a4e.js' data-minify="1"></script> 
        <script type='text/javascript' src='/theme/Irati/js/script-57ac58d5af9a14b33430d79463f9e2eb.js' data-minify="1"></script> 
        <script type='text/javascript' src='/theme/Irati/js/jquery.ticker-81465ab513100bf1a0c2b55862a6a80f.js' data-minify="1"></script> 
        <script>(function(w, d){
            var b = d.getElementsByTagName("body")[0];
            var s = d.createElement("script"); s.async = true;
            var v = !("IntersectionObserver" in w) ? "8.7.1" : "10.5.2";
            s.src = "/theme/Irati/js/lazyload-" + v + ".min.js";
            w.lazyLoadOptions = {
                elements_selector: "img, iframe",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                skip_invisible: false,
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_load: function(element) {
                    if ( element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible" ) {
                        if (element.classList.contains("lazyloaded") ) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                }
            }; // Your options here. See "recipes" for more information about async.
            b.appendChild(s);
            }(window, document));
            
            // Listen to the Initialized event
            window.addEventListener('LazyLoad::Initialized', function (e) {
                // Get the instance and puts it in the lazyLoadInstance variable
            var lazyLoadInstance = e.detail.instance;
            
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    lazyLoadInstance.update();
                } );
            } );
            
            var b      = document.getElementsByTagName("body")[0];
            var config = { childList: true, subtree: true };
            
            observer.observe(b, config);
            }, false);
        </script>

        <script>
            $('[data-fone]').on('click', function(t) {
                t.preventDefault();
                $(this).text($(this).data('fone'));
            });
        </script>

        <?php        
        //Se tem algum bloco ativo nesta região
        if($this->Regions->blocks('footer_scripts')){
            echo $this->Regions->blocks('footer_scripts');
        }
        echo $this->fetch('script');
        echo $this->Js->writeBuffer(); // Write cached scripts
        ?>
    </body>
</html>