<section class="m-content">
	<div class="m-article-header">
		<div class="l-wrapper">
      <header class="b-article-header">
        <div class="b-article-header_large">
          <h2 class="b-article-header_title b-article-header_title--yellow">
            <?php echo $contact['Contact']['title']; ?>
          </h2>
        </div>        
			</header>
		</div>
    </div>

	<?php
	//Se tem algum bloco ativo nesta região
	if ($this->Regions->blocks('pagina_contato')){
		echo $this->Regions->blocks('pagina_contato');
	}
	?>
	<div class="m-facaparte u-spacer_down-400" id="contact-<?php echo $contact['Contact']['id']; ?>">
		<div class="l-wrapper">
	    	<div class="row">
				<div class="col-sm-6">
                	<article class="b-article b-article--no-margin">
                		<?php
                		//Se tem algum bloco ativo nesta região
                    if ($this->Regions->blocks('esquerda_contato')){
                      echo $this->Regions->blocks('esquerda_contato');
                    }
                		echo $contact['Contact']['body'];
                		?>                    
                	</article>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3600.786799821729!2d-48.50763268552188!3d-25.512155983750667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94db9a4f62f88d23%3A0x9e9ad9641d842ceb!2sR.+Jo%C3%A3o+Eug%C3%AAnio%2C+355+-+Costeira%2C+Paranagu%C3%A1+-+PR%2C+83203-400!5e0!3m2!1spt-BR!2sbr!4v1541438112555" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              	<div class="col-sm-6">
                	<header class="l-header l-header--small l-header--yellow-bar">
                 		<h4 class="l-header_title l-header_title--small">
                 			<?php // echo $contact['Contact']['title']; ?>
                 		</h3>
                	</header>
	                <?php if ($contact['Contact']['message_status']):  ?>
					<fieldset>
						<?php
							echo $this->Form->create('Message', array(
								'url' => array(
									'plugin' => 'contacts',
									'controller' => 'contacts',
									'action' => 'view',
									$contact['Contact']['alias'],
								),
							));

							$this->Form->inputDefaults(array(
								'label' => false,
								'class' => 'form-control',
								'div' => array('class' => 'form-group'),
							));

							echo $this->Form->input('Message.name', array('placeholder' => __('Seu nome')));
							echo $this->Form->input('Message.email', array('placeholder' => __('Seu email')));
							echo $this->Form->input('Message.title', array('placeholder' => __('Assunto')));
							echo $this->Form->input('Message.body', array('placeholder' => __('Mensagem')));
							if ($contact['Contact']['message_captcha']):
								echo $this->Recaptcha->display_form();
							endif;

							echo $this->Form->button(__('Enviar'), array('class' => 'btn btn-info'));
							echo $this->Form->end();
						?>

					</fieldset>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
