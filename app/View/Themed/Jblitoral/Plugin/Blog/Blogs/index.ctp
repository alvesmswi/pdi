<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
?>

<div class="row mb-40">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="section-title" style="text-align:center;">
      <h1 class="zm-post-title h2" style="color:#074f2c;">
        <?php echo $title_for_layout; ?>
      </h1>
    </div>
  </div>
</div>

<?php echo $this->element('box_autor')?>

<?php if (!empty($posts)):?>
  <div class="row">
    <div class="col-md-12">
      <div class="zm-posts">
        <?php foreach ($posts as $keyNode => $n): ?>
          <?php 
            $link_noticia = null;
            $imagemUrl = null;
            $imageTitle = null;
            $title = null;

            $link_noticia = $this->Html->url($n['Post']['url']);

            if(isset($n['Images'][0]['filename']) && !empty($n['Images'][0]['filename'])) {
              $this->Helpers->load('Medias.MediasImage');
              $imagemUrl = $this->MediasImage->imagePreset($n['Images'][0]['filename'], '760x409');
              $imageTitle = $n['Post']['title'];
            }

            $title = $n['Post']['title'];
          ?>
          <article class="zm-post-lay-c zm-single-post clearfix">
            <div class="zm-post-thumb f-left">
              <?php if (isset($imagemUrl)): ?>
                <div class="lazyload">
                    <!--
                        <a href="<?php echo $link_noticia; ?>">
                          <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                        </a>
                    -->
                </div>
              <?php endif; ?>
            </div>
            <div class="zm-post-dis f-right">
              <div class="zm-post-header">
                <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                <div class="zm-post-meta">
                  <ul>
                    <li class="s-meta"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Post']['publish_start']))); ?></li>
                  </ul>
                </div>
                <div class="zm-post-content">
                  <?php echo $n['Post']['excerpt'] ?>
                </div>
              </div>
            </div>
          </article>

          <?php if ($keyNode == 2): ?>
            <?php
              //Se tem algum bloco ativo nesta região
              if($this->Regions->blocks('publicidade_lista')){
                echo $this->Regions->blocks('publicidade_lista');
              }
            ?>
          <?php endif; ?>
        <?php endforeach; ?>

        <?php 
          $this->Paginator->options['url'] = array('controller' => 'blogs', 'action' => 'view', 'slug' => $this->params['slug']);
          echo $this->element('paginator'); 
        ?>
      </div>
    </div>
  </div>
<?php endif; ?>
