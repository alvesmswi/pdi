<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');

  $this->Html->meta(
    array('property' => 'og:title', 'content' => $post['Post']['title']),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:description', 'content' => (isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt']) ? $post['Post']['excerpt'] : "")),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('property' => 'og:url', 'content' => Router::url(null, true)),
    null,
    array('inline'=>false)
  );

  $this->Html->meta(
    array('name' => 'twitter:card', 'content' => 'summary'),
    null,
    array('inline'=>false)
  );

  //se tem chamada
  if(isset($post['Post']['excerpt']) && !empty($post['Post']['excerpt'])){
    $description = $post['Post']['excerpt'];
  }else{
    $description = substr($post['Post']['body'],0,128);
    $description = strip_tags($description);
  }
  //gera o description
  $this->Html->meta(
    array(
      'name' => 'description ', 
      'content' => $description
    ),
    null,
    array('inline'=>false)
  );
  //Gera as keywords
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $this->Mswi->gerarKeywords($post['Post']['title'].' '.$description)
		),
		null,
		array('inline'=>false)
  );
  
  $this->Helpers->load('Medias.MediasImage');
?>

<div class="row">
  <div class="col-md-12 columns">
    
  </div>
</div>

<div class="row">
  <div class="col-md-3 sidebar-warp columns hidden-md">
    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('interna_esquerda')){
        echo $this->Regions->blocks('interna_esquerda');
      }
    ?>
  </div>

  <div class="col-md-9 columns">    

    <article class="zm-post-lay-single">      

      <div class="section-title mb-20" style="text-align:center">
        <h1 class="zm-post-title h2" style="color:#074f2c;"><?php echo $post['Blog']['title']; ?></h1>
      </div>
      <?php echo $this->element('box_autor')?>
      <div class="zm-post-header" style="padding: 0px 0 15px;">
        <h1 class="zm-post-title h2"><?php echo $post['Post']['title'] ?></h1>
        <blockquote><?php echo $post['Post']['excerpt']; ?></blockquote>
        <div class="zm-post-meta ">
          <div class="addthis_inline_share_toolbox hidden-sm" style="float:left;"></div>
          <ul class="text-right">
            <li class="s-meta"><?php echo $post['User']['name'] ?></li>
            <li class="s-meta"><?php echo htmlentities(strftime('%d de %B de %Y', strtotime($post['Post']['publish_start']))); ?></li>
          </ul>
        </div>
      </div>

        <div class="zm-post-content">
          <?php echo $this->Mswi->lerCodigos($post['Post']['body']); ?>
        </div>

        <div class="entry-meta-small clearfix ptb-40 mtb-40 border-top border-bottom">
          <?php echo $this->element('view_tags'); ?>
          <?php echo $this->element('view_share'); ?>
          <!--COMENTÁRIOS DO FACEBOOK-->
          <div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].'/blog/'.$post['Blog']['slug'].'/'.$post['Post']['slug']?>" data-numposts="5" data-width="100%"></div>
        </div>
    </article>
  </div>
</div>
