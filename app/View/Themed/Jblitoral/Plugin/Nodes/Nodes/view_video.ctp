<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  $this->Nodes->set($node); 

  echo $this->element('node_metas');

  // Seta variaveis de exibição padrão
  $body = $node['Node']['body'];
?>

<div class="row">
  <div class="col-md-12">
    <article class="zm-post-lay-single">
      <div class="zm-post-header" style="padding: 0px 0 35px;">
        <h2 class="zm-post-title h2"><?php echo $node['Node']['title']; ?></h2>
      </div>

      <?php if (isset($node['Multiattach']) && !empty($node['Multiattach'])): ?>
        <div class="zm-post-thumb">
          <?php
            $imagemUrlArray = array(
              'plugin' => 'Multiattach',
							'controller' => 'Multiattach',
							'action' => 'displayFile', 
							'admin' => false,
							'filename' => $node['Multiattach'][0]['Multiattach']['filename'],
							'dimension' => 'normal'
						);
            $imagemUrl = $this->Html->url($imagemUrlArray);
            $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');

            //prepara a imagem para o OG
            $this->Html->meta(
              array('property' => 'og:image', 'content' => $this->Html->url($imagemUrlArray, true)),
              null,
              array('inline' => false)
            );
            
            //pega os detalhes da imagem
            $imageArray = @getimagesize($this->Html->url($imagemUrlArray, true));

            //largura da imagem
            $this->Html->meta(
              array('property' => 'og:image:width', 'content' => $imageArray[0]),
              null,
              array('inline' => false)
            );
            //Altura da imagem
            $this->Html->meta(
              array('property' => 'og:image:height', 'content' => $imageArray[1]),
              null,
              array('inline' => false)
            );
            //Mime da imagem
            $this->Html->meta(
              array('property' => 'og:image:mime', 'content' => $imageArray['mime']),
              null,
              array('inline' => false)
            );
            //Alt da imagem
            $this->Html->meta(
              array('property' => 'og:image:alt', 'content' => $imageTitle),
              null,
              array('inline' => false)
            );

            $youtube =  $node['VideoDetail']['youtube']; // $video recebe o valor da url do youtube (link)
            preg_match('/(v\/|\?v=)([^&?\/]{5,15})/', $youtube, $video); // Faz o preg_match para retirar caracters
          ?>
          <div class="addthis_inline_share_toolbox"></div>
          <iframe width="870" height="506" src="https://www.youtube.com/embed/<?php echo $video[2] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      <?php endif; ?>

      <div class="zm-post-dis">
        <div class="zm-post-content">
          <?php echo $this->Mswi->lerCodigos($body); ?>
        </div>

        <div class="entry-meta-small clearfix ptb-40 mtb-40 border-top border-bottom">
          <?php echo $this->element('view_share'); ?>
        </div>
      </div>
    </article>

    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('publicidade_interna')){
        echo $this->Regions->blocks('publicidade_interna');
      }
    ?>
  </div>
</div>
