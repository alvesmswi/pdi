<?php 
if (isset($node['Multiattach']) && count($node['Multiattach']) > 1){  
	$imagemUrlArray = array(
		'plugin'		=> 'Multiattach',
		'controller'	=> 'Multiattach',
		'action'		=> 'displayFile', 
		'admin'			=> false,
		'filename'		=> $node['Multiattach'][0]['Multiattach']['filename'],
		'dimension'		=> '600largura'
	);
	$imagemUrl = $this->Html->url($imagemUrlArray);

	?>

	<style>
		#corpo .gallery__nav-thumbs .slick-list,
		#corpo .gallery__nav-thumbs .slick-track{
			max-width:none !important;
		}
	</style>

	<!-- GALERIA DE IMAGENS - Slider-->
	<div class="container-fluid gallery">

		<!-- SLIDER -->
		<div class="gallery__nav-thumbs" style="margin-bottom:40px;">
			<?php						
			//percorre para montar as miniaturas
			foreach($node['Multiattach'] as $key => $min){
				//se não é um PDF
				if ($min['Multiattach']['mime'] != 'application/pdf') {
					$imagemUrlArray = array(
						'plugin'		=> 'Multiattach',
						'controller'	=> 'Multiattach',
						'action'		=> 'displayFile', 
						'admin'			=> false,
						'filename'		=> $min['Multiattach']['filename'],
						'dimension'		=> 'normal'
					);
					$imgUrl = $this->Html->url($imagemUrlArray);
					$imagemUrlArray = array(
						'plugin'		=> 'Multiattach',
						'controller'	=> 'Multiattach',
						'action'		=> 'displayFile', 
						'admin'			=> false,
						'filename'		=> $min['Multiattach']['filename'],
						'dimension'		=> '300largura'
					);
					$minUrl = $this->Html->url($imagemUrlArray);
				} 
				?>
				<div id="btn<?php echo $key;?>" class="col-lg-4 col-md-4 col-sm-6" data-load-img="<?php echo $imgUrl; ?>" data-alt-img="<?php echo $min['Multiattach']['meta']; ?>"  data-alt-title="<?php echo (isset($min['Multiattach']['metaDisplay']) ? ($min['Multiattach']['metaDisplay']) : ('')); ?>" data-by="<?php echo $min['Multiattach']['comment']; ?>" data-legend="<?php echo (isset($min['Multiattach']['metaDisplay']) ? ($min['Multiattach']['metaDisplay']) : ('')); ?>">
					<figure>
						<div class="img-responsive">
							<div class="lazyload">
                                <!--
                                <img
								itemprop="image" 
								src="<?php echo $minUrl; ?>" 
								alt="<?php echo $min['Multiattach']['meta']; ?>">
								-->
                            </div>
						</div>
					</figure>
				</div>
			<?php } ?>
		</div>

		<!-- CAROUSEL -->
		<div class="gallery__visualization">
            <div class="lazyload">
                <!--
                    <img class="ajax-loader hidden" src="/images/ajax-loader.gif" style="position: absolute; z-index:999; top: 50%; left:48%;">
                -->
            </div>
			<div class="fade in" id="viewbtn0">
				<figure class="bp-text-right">
					<div class="img-responsive">
                        <div class="lazyload">
                            <!--
                                <img itemprop="image" src="<?php echo $imagemUrl;?>" title="<?php echo $node['Multiattach'][0]['Multiattach']['comment']; ?>" alt="<?php echo $node['Multiattach'][0]['Multiattach']['meta']; ?>">
                                    <figcaption>
                                        <?php
                                        if(!empty($node['Multiattach'][0]['Multiattach']['metaDisplay'])){ ?>
                                            <span itemprop="description"><?php echo $node['Multiattach'][0]['Multiattach']['metaDisplay']; ?></span>
                                        <?php } ?>
                                        <?php
                                        if(!empty($node['Multiattach'][0]['Multiattach']['comment'])){ ?>
                                            <cite itemprop="author"> - <?php echo 'Foto: '.$node['Multiattach'][0]['Multiattach']['comment']; ?></cite>
                                        <?php } ?>
                                    </figcaption>
                            -->
                        </div>
				</figure>
			</div>
		</div>

		</div>
<?php } ?>	
