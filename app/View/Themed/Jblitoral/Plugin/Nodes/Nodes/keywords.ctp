<?php 
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  echo $this->element('node_metas'); 
?>

<div class="row mb-40">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="section-title">
      <h2 class="h6 header-color inline-block uppercase">
        <?php
          //se teve uma pesquisa
          if(isset($this->params['type']) && !empty($this->params['type'])) {
            echo __d('croogo', $this->Mswi->typeName($this->params['type']));
          }else{
            echo __d('croogo', 'Resultado da pesquisa');
          }
			  ?>
      </h2>
    </div>
  </div>
</div>

<?php if (!empty($nodes)): ?>
  <div class="row">
    <div class="col-md-12">
      <div class="zm-posts">
        <?php foreach ($nodes as $keyNode => $n): ?>
          <?php 
            $link_noticia = null;
            $imagemUrl = null;
            $imageTitle = null;
            $categoria = null;
            $categoria_color = null;
            $term = null;
            $categoriaUrl = null;
            $title = null;
            
            $link_noticia = $n['Node']['path'];
            if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
              $link_noticia = $n['NoticiumDetail']['link_externo'];
            }

            if(!isset($n['Multiattach']['filename']) && isset($n['Multiattach'][0]['Multiattach'])) {
              $n['Multiattach'] = $n['Multiattach'][0]['Multiattach'];
            }
            if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
              $imagemUrlArray = array(
                'plugin' => 'Multiattach',
                'controller' => 'Multiattach',
                'action' => 'displayFile', 
                'admin' => false,
                'filename' => $n['Multiattach']['filename'],
                'dimension' => '760x409'
              );
              $imagemUrl = $this->Html->url($imagemUrlArray);
              $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
            }

            $categoria = $this->Mswi->categoryName($n['Node']['terms']);
            if(isset($n['NoticiumDetail']['chapeu']) && !empty($n['NoticiumDetail']['chapeu'])){
              $categoria = $n['NoticiumDetail']['chapeu'];
            }
            $categoria_color = $this->Mswi->categoryColor($n['Node']['terms']);

            $title = $n['Node']['title'];
            if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
              $title = $n['NoticiumDetail']['titulo_capa'];
            }

            $term = $this->Mswi->categorySlug($n['Node']['terms']);
            $categoriaUrl = '/'.$n['Node']['type'].'/term/'.$term;
          ?>
          <article class="zm-post-lay-c zm-single-post clearfix">
            <div class="zm-post-thumb f-left">
              <?php if (isset($imagemUrl)): ?>
                <div class="lazyload">
                    <!--
                        <a href="<?php echo $link_noticia; ?>">
                          <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                        </a>
                    -->
                </div>
              <?php endif; ?>
            </div>
            <div class="zm-post-dis f-right">
              <div class="zm-post-header">
                <div class="zm-category"><a href="<?php echo $categoriaUrl; ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>"><?php echo $categoria; ?></a></div>
                <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                <div class="zm-post-meta">
                  <ul>
                    <li class="s-meta"><?php echo htmlentities(strftime('%d de %B', strtotime($n['Node']['publish_start']))); ?></li>
                  </ul>
                </div>
                <div class="zm-post-content">
                  <?php echo $n['Node']['excerpt'] ?>
                </div>
              </div>
            </div>
          </article>

          <?php if ($keyNode == 2): ?>
            <?php
              //Se tem algum bloco ativo nesta região
              if($this->Regions->blocks('publicidade_lista')){
                echo $this->Regions->blocks('publicidade_lista');
              }
            ?>
          <?php endif; ?>
        <?php endforeach; ?>

        <?php echo $this->element('paginator'); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
