<?php
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
  date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html class="no-js" lang="pt-BR">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <?php		
    echo $this->fetch('meta');
    ?>

    <title><?php echo Configure::read('Site.title'); ?> :: <?php echo $title_for_layout; ?></title>
    
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" href="apple-touch-icon.html">
    
    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/bootstrap.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/responsive.css">
    
    <!-- Slick -->


    <!-- Style customizer (Remove these lines please) -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/style-customizer.css">
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/color/color-3.css">

    <!-- Modernizr JS -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/vendor/modernizr-2.8.3.min.js"></script>

      <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/slick.css">
      <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/slick-theme.css">
    <?php
    //Se foi informado o user do analytics
    if (!empty(Configure::read('Service.analytics'))) {
		$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
      <script>
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        <?php foreach ($arrayAnalytics as $key => $analytics) { ?>
        ga('create', '<?php echo trim($analytics) ?>', 'auto', {
          'name': 'id<?php echo $key ?>'
        });
        ga('id<?php echo $key ?>.send', 'pageview');
        <?php } ?>

        /**
          * Function that tracks a click on an outbound link in Analytics.
          * This function takes a valid URL string as an argument, and uses that URL string
          * as the event label. Setting the transport method to 'beacon' lets the hit be sent
          * using 'navigator.sendBeacon' in browser that support it.
          */
        var trackOutboundLink = function (url) {
          ga('send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon',
            'hitCallback': function () {
              document.location = url;
            }
          });
        }
      </script>
    <?php } ?>

    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function () {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <?php
    //Se foi informado o tempo de refresh para a Home
    if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
      <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
    <?php } ?>
    
    <?php
    //Se foi informado o tempo de refresh para as internas
	  if ($this->here != '/' && !empty(Configure::read('Site.refresh_internas')) && is_numeric(Configure::read('Site.refresh_internas'))) { ?>
		  <meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />
    <?php } ?>

    <?php echo Configure::read('Service.header'); ?>

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('header_scripts')){
        echo $this->Regions->blocks('header_scripts');
      }
    ?>
    <!-- User style -->
    <link rel="stylesheet" href="/theme/<?php echo Configure::read('Site.theme')?>/css/custom.css">

    <?php
      //Caminho absoluto do arquivo de CSS customizado
      $themeCss = WWW_ROOT . 'css' . DS . 'theme.css';
      //se o arquivo de CSS personalizado existe
      if(file_exists($themeCss)){ ?>
        <link rel="stylesheet" href="/css/theme.css">
      <?php }?>

  </head>

  <body data-disable-copy>
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]--> 

    <?php 
      if(isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id'])){
        echo $this->element('admin_bar');
      } 
		?>
    
    <!--  THEME PRELOADER AREA -->
    <!-- <div id="preloader-wrapper">
      <div class="preloader-wave-effect"></div>
    </div> -->
    <!-- THEME PRELOADER AREA END -->   

    <!-- Body main wrapper start -->
    <div class="wrapper">
      <!-- Start of header area -->
      <header  class="header-area header-wrapper bg-white clearfix">
      <?php echo $this->Menus->menu('menutop', array('element' => 'menus/menutop')); ?>
        <div class="header-middle-area">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-5 col-xs-12 header-mdh">
                <div class="global-table">
                  <div class="global-row">
                    <div class="global-cell">
                      <div class="logo">
                          <div class="lazyload">
                              <!--
                                <a href="/">
                                  <?php $siteLogo = Configure::read('Site.logo'); ?>
                                  <?php
                                    if(!empty($siteLogo)){
                                      $themeLogo = Router::url('/', true).$siteLogo;
                                    }else{
                                      $themeLogo = '/theme/'.Configure::read('Site.theme').'/images/logo/logo.png?321';
                                    }
                                  ?>
                                  <img src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
                                </a>
                             -->
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8 col-lg-7 col-sm-7 col-xs-12 col-lg-offset-1 header-mdh hidden-xs">
                <div class="global-table">
                  <div class="global-row">
                    <div class="global-cell">
                      <div class="advertisement text-right">
                                <?php
                                  //Se tem algum bloco ativo nesta região
                                  if ($this->Regions->blocks('publicidade_top')){
                                    echo $this->Regions->blocks('publicidade_top');
                                  }
                                ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php echo $this->Menus->menu('main', array('element' => 'menus/main')); ?>
      </header>
      <!-- End of header area -->

      <?php
        //Exibe as mensagens
        echo $this->Layout->sessionFlash();

        //Se tem algum bloco ativo nesta região
        if($this->Regions->blocks('content_top')){
          echo $this->Regions->blocks('content_top');
        }

        ////Página Interna
        if ($this->here != '/'):
        ?>
        <div id="page-content" class="page-wrapper">
          <div class="zm-section single-post-wrap bg-white ptb-70 xs-pt-30">
            <div class="container view-master">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 columns">
                  <?php
                    //Conteudo do node
                    echo $content_for_layout;

                    if ($this->Regions->blocks('centro_interna')) {
                      echo $this->Regions->blocks('centro_interna');
                    }
                  ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-warp columns">
                  <div class="row">
                    <?php
                      if ($this->Regions->blocks('right')) {
                        echo $this->Regions->blocks('right');
                      }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; 

      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('content_footer')){
        echo $this->Regions->blocks('content_footer');
      }
      ?>

      <!-- Start footer area -->
      <footer id="footer" class="footer-wrapper footer-1">
        <!-- Start footer top area -->
        <div class="footer-top-wrap ptb-70 bg-dark">
          <div class="container">
            <div class="row">
              <?php
                if($this->Regions->blocks('footer')){
                  echo $this->Regions->blocks('footer');
                }
              ?>
            </div>
          </div>
        </div>
        <!-- End footer top area -->
        <div class="footer-buttom bg-black ptb-15">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="zm-copyright" style="display:none">
                  <p class="uppercase">&copy; <?php echo date('Y') ?> <a href="https://mswi.com.br/cmswi/">MSWI</a></p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-right hidden-xs">
                <?php echo $this->Menus->menu('menubot', array('element' => 'menus/menubot')); ?>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- End footer area -->
    </div>
    <!-- Body main wrapper end -->
    
    <!-- Placed js at the end of the document so the pages load faster -->
    <!-- jquery latest version -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/vendor/jquery-1.12.1.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/bootstrap.min.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/owl.carousel.min.js"></script>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/slick.min.js"></script>
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/plugins.js"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/main.js"></script>
    <!-- Bootstrap framework js -->

    <!-- LazyLoad jquery.lazyload-any.min.js -->
    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/jquery.lazyload-any.min.js"></script>

    <script src="/theme/<?php echo Configure::read('Site.theme')?>/js/custom.js"></script>

    <?php if(!empty(Configure::read('Service.addthis'))): ?>
      <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo Configure::read('Service.addthis') ?>"></script>
    <?php endif; ?>

    <?php echo $this->Js->writeBuffer(); ?>

    <?php
      //Se tem algum bloco ativo nesta região
      if($this->Regions->blocks('footer_scripts')){
        echo $this->Regions->blocks('footer_scripts');
      }
    ?>
    
    <?php echo $this->element('sql_dump')?>
  </body>
</html>
