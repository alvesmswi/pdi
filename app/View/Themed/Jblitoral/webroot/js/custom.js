"use strict";

$(document).ready(function () {
    $('#corpo div[data-oembed-url] div').css("max-width", "100%");
});

function disableCopy(e) {

  var ttt = 'Para compartilhar esse conteúdo, por favor utilize o link {{link}} ou as ferramentas oferecidas na página. Textos, fotos, artes e vídeos da Folha estão protegidos pela legislação brasileira sobre direito autoral. Não reproduza o conteúdo do jornal em qualquer meio de comunicação, eletrônico ou impresso, sem autorização da Folhapress (pesquisa@folhapress.com.br). As regras têm como objetivo proteger o investimento que a Folha faz na qualidade de seu jornalismo. Se precisa copiar trecho de texto da Folha para uso privado, por favor logue-se como assinante ou cadastrado.';

  function handle_copy(el) {
    var sel = window.getSelection(),
      range = sel.getRangeAt(0);
    sel.removeAllRanges();
    sel.selectAllChildren(el);

    return $.proxy(function () {
      sel.removeAllRanges();
      sel.addRange(range);
    }, this);
  }

  function handle_copy_alt(el) {
    var sel = document.selection && document.selection.createRange(),
      bm = sel.getBookmark();

    sel.moveToElementText(el);
    sel.select();

    return function () {
      sel = document.selection.createRange();
      sel.moveToBookmark(bm);
      sel.select();
    };
  }

  function get_text() {
    var l = 115;
    var selectedText,
      ellipsis,
      range;

    if (typeof window.getSelection !== 'undefined') {
      selectedText = window.getSelection().toString();
    } else {
      selectedText = document.selection;
    }

    ellipsis = (selectedText.length > l ? "…" : "");

    range = (ellipsis ? selectedText.indexOf(" ", l) : l);
    range = (range === -1 ? selectedText.length : range);

    return "\"" + selectedText.toString().substring(0, range) + ellipsis + "\", continue lendo em: " + location.href;
  }

  function copy(e) {

    var $msg,
      $text,
      reset;

    if (!$(document.body).is("[data-disable-copy]")) return;

    // create temporary box
    $msg = $(document.createElement('span')).css('text-indent', '-9999px').css('width', '1px').css('height', '1px');
    // set content                
    $msg.text(get_text());

    $(document.body).append($msg);


    if (typeof window.getSelection !== 'undefined') {
      reset = handle_copy($msg.get(0));
    } else {
      reset = handle_copy_alt($msg.get(0));
    }

    // remove temporary box
    window.setTimeout(function () {
      reset();
      $msg.remove();
    }, 0);
  }

  return copy(e);
}

$(document.body).on('copy', disableCopy);

/* inicio  lazyload */

function load(img)
{
  img.fadeOut(0, function() {
    img.fadeIn(1000);
  });
}
jQuery('.lazyload').lazyload({load: load});

/* fim lazyload*/