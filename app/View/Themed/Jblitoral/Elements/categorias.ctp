<?php if (!empty($nodesList)): ?>
  <?php 
    if($block['Block']['show_title']){
      $title_block = $block['Block']['title'];
    }else{
      $title_block = $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
    }
  ?>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <div class="section-title mb-40">
      <h2 class="h6 header-color inline-block uppercase"><?php echo $title_block; ?></h2>
    </div>
    <?php if (isset($nodesList[0])): ?>
      <?php 
        $principal = $nodesList[0];
        $link_noticia = $principal['Node']['path'];
        if (isset($principal['NoticiumDetail']['link_externo']) && !empty($principal['NoticiumDetail']['link_externo'])) {
          $link_noticia = $principal['NoticiumDetail']['link_externo'];
        }

        if(isset($principal['Multiattach']) && !empty($principal['Multiattach']['filename'])) {
          $imagemUrlArray = array(
            'plugin' => 'Multiattach',
            'controller' => 'Multiattach',
            'action' => 'displayFile', 
            'admin' => false,
            'filename' => $principal['Multiattach']['filename'],
            'dimension' => '420x314'
          );
          $imagemUrl = $this->Html->url($imagemUrlArray);
          $imageTitle = htmlspecialchars((!empty($principal['Multiattach']['meta']) ? $principal['Multiattach']['meta'] : $principal['Node']['title']), ENT_QUOTES, 'UTF-8');
        }

        $title = $principal['Node']['title'];
        if(isset($principal['NoticiumDetail']['titulo_capa']) && !empty($principal['NoticiumDetail']['titulo_capa'])){
          $title = $principal['NoticiumDetail']['titulo_capa'];
        }
      ?>
      <article class="zm-trending-post zm-lay-d zm-single-post" data-effict-zoom="1">
        <?php if (isset($imagemUrl)): ?>
          <div class="zm-post-thumb">
              <div class="lazyload">
                  <!--
                    <a href="<?php echo $link_noticia; ?>" data-dark-overlay="2.5" data-scrim-bottom="9">
                      <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                    </a>
                -->
              </div>
          </div>
        <?php endif; ?>
        <div class="zm-post-dis text-white">
          <h2 class="zm-post-title h3">
            <a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a>
          </h2>
        </div>
      </article>
    <?php endif; ?>
    <!-- start post layout E -->
    <div class="zm-post-lay-g-area">
      <?php for ($i=1; $i < count($nodesList); $i++): ?>
        <?php 
          $link_noticia = null;
          $imagemUrl = null;
          $imageTitle = null;
          $title = null;

          $node = $nodesList[$i];
          $link_noticia = $node['Node']['path'];
          if (isset($node['NoticiumDetail']['link_externo']) && !empty($node['NoticiumDetail']['link_externo'])) {
            $link_noticia = $node['NoticiumDetail']['link_externo'];
          }

          if(isset($node['Multiattach']) && !empty($node['Multiattach']['filename'])) {
            $imagemUrlArray = array(
              'plugin' => 'Multiattach',
              'controller' => 'Multiattach',
              'action' => 'displayFile', 
              'admin' => false,
              'filename' => $node['Multiattach']['filename'],
              'dimension' => '122x76'
            );
            $imagemUrl = $this->Html->url($imagemUrlArray);
            $imageTitle = htmlspecialchars((!empty($node['Multiattach']['meta']) ? $node['Multiattach']['meta'] : $node['Node']['title']), ENT_QUOTES, 'UTF-8');
          }

          $title = $node['Node']['title'];
          if(isset($node['NoticiumDetail']['titulo_capa']) && !empty($node['NoticiumDetail']['titulo_capa'])){
            $title = $node['NoticiumDetail']['titulo_capa'];
          }
        ?>
        <article class="zm-post-lay-g zm-single-post clearfix">
          <?php if (isset($imagemUrl)): ?>
            <div class="zm-post-thumb f-left">
                <div class="lazyload">
                    <!--
                      <a href="<?php echo $link_noticia; ?>">
                        <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                      </a>
                     -->
                </div>
            </div>
          <?php endif; ?>
          <div class="zm-post-dis f-right">
            <div class="zm-post-header">
              <h2 class="zm-post-title">
                <a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a>
              </h2>
            </div>
          </div>
        </article>
      <?php endfor; ?>
    </div>
    <!-- End post layout E -->
  </div>
<?php endif; ?>
