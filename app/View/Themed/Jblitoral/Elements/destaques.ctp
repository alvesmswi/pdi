<?php if (!empty($nodesList)): ?>
  <!-- Start trending post area -->
  <div class="trending-post-area">
    <div class="container-fluid">
      <div class="row">
        <div class="trend-post-list zm-lay-c-wrap zm-posts clearfix">
          <!-- Start single trend post -->
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <div class="row">
              <?php if (isset($nodesList[1])): ?>
                <?php 
                  $node_1 = $nodesList[1];
                  $link_noticia = $node_1['Node']['path'];
                  if (isset($node_1['NoticiumDetail']['link_externo']) && !empty($node_1['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node_1['NoticiumDetail']['link_externo'];
                  }

                  $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/7.jpg';
                  if(isset($node_1['Multiattach']) && !empty($node_1['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $node_1['Multiattach']['filename'],
                      'dimension' => '356x223'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($node_1['Multiattach']['meta']) ? $node_1['Multiattach']['meta'] : $node_1['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $categoria = $this->Mswi->categoryName($node_1['Node']['terms']);
                  if(isset($node_1['NoticiumDetail']['chapeu']) && !empty($node_1['NoticiumDetail']['chapeu'])){
                    $categoria = $node_1['NoticiumDetail']['chapeu'];
                  }
                  $categoria_slug = $this->Mswi->categorySlug($node_1['Node']['terms']);
                  $categoria_color = $this->Mswi->categoryColor($node_1['Node']['terms']);

                  $title = $node_1['Node']['title'];
                  if(isset($node_1['NoticiumDetail']['titulo_capa']) && !empty($node_1['NoticiumDetail']['titulo_capa'])){
                    $title = $node_1['NoticiumDetail']['titulo_capa'];
                  }
                ?>
                <div class="col-md-12 col-sm-6 col-lg-12 col-xs-12 p-0">
                  <article class="destaque1 zm-trending-post zm-lay-c zm-single-post" data-dark-overlay="0"  data-scrim-bottom="9" data-effict-zoom="1">
                    <div class="zm-post-thumb">
                        <div class="lazyload">
                            <!--
                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                            -->
                        </div>
                    </div>
                    <div class="zm-post-dis text-white">
                      <div class="zm-category">
                        <a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>">
                          <?php echo $categoria; ?>
                        </a>
                      </div>
                      <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                    </div>
                  </article>
                </div>
              <?php endif; ?>
              
              <?php if (isset($nodesList[2])): ?>
                <?php 
                  $node_2 = $nodesList[2];
                  $link_noticia = $node_2['Node']['path'];
                  if (isset($node_2['NoticiumDetail']['link_externo']) && !empty($node_2['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node_2['NoticiumDetail']['link_externo'];
                  }

                  $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/8.jpg';
                  if(isset($node_2['Multiattach']) && !empty($node_2['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $node_2['Multiattach']['filename'],
                      'dimension' => '356x223'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($node_2['Multiattach']['meta']) ? $node_2['Multiattach']['meta'] : $node_2['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $categoria = $this->Mswi->categoryName($node_2['Node']['terms']);
                  if(isset($node_2['NoticiumDetail']['chapeu']) && !empty($node_2['NoticiumDetail']['chapeu'])){
                    $categoria = $node_2['NoticiumDetail']['chapeu'];
                  }
                  $categoria_slug = $this->Mswi->categorySlug($node_2['Node']['terms']);
                  $categoria_color = $this->Mswi->categoryColor($node_2['Node']['terms']);

                  $title = $node_2['Node']['title'];
                  if(isset($node_2['NoticiumDetail']['titulo_capa']) && !empty($node_2['NoticiumDetail']['titulo_capa'])){
                    $title = $node_2['NoticiumDetail']['titulo_capa'];
                  }
                ?>
                <div class="col-md-12 col-sm-6 col-lg-12 col-xs-12 p-0">
                  <article class="destaque2 zm-trending-post zm-lay-c zm-single-post" data-dark-overlay="0"  data-scrim-bottom="9" data-effict-zoom="1">
                      <div class="zm-post-thumb">
                          <div class="lazyload">
                              <!--
                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                              -->
                          </div>
                      </div>
                      <div class="zm-post-dis text-white">
                        <div class="zm-category">
                          <a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>">
                            <?php echo $categoria; ?>
                          </a>
                        </div>
                        <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                      </div>
                  </article>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- End single trend post -->

          <!-- Start single trend post -->
          <?php if (isset($nodesList[0])): ?>
            <?php 
              $principal = $nodesList[0];
              $link_noticia = $principal['Node']['path'];
              if (isset($principal['NoticiumDetail']['link_externo']) && !empty($principal['NoticiumDetail']['link_externo'])) {
                $link_noticia = $principal['NoticiumDetail']['link_externo'];
              }

              $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/11.jpg';
              if(isset($principal['Multiattach']) && !empty($principal['Multiattach']['filename'])) {
                $imagemUrlArray = array(
							    'plugin' => 'Multiattach',
									'controller' => 'Multiattach',
									'action' => 'displayFile', 
									'admin' => false,
									'filename' => $principal['Multiattach']['filename'],
									'dimension' => '710x450'
								);
                $imagemUrl = $this->Html->url($imagemUrlArray);
                $imageTitle = htmlspecialchars((!empty($principal['Multiattach']['meta']) ? $principal['Multiattach']['meta'] : $principal['Node']['title']), ENT_QUOTES, 'UTF-8');
              }

              $categoria = $this->Mswi->categoryName($principal['Node']['terms']);
              if(isset($principal['NoticiumDetail']['chapeu']) && !empty($principal['NoticiumDetail']['chapeu'])){
                $categoria = $principal['NoticiumDetail']['chapeu'];
              }
              $categoria_slug = $this->Mswi->categorySlug($principal['Node']['terms']);
              $categoria_color = $this->Mswi->categoryColor($principal['Node']['terms']);

              $title = $principal['Node']['title'];
							if(isset($principal['NoticiumDetail']['titulo_capa']) && !empty($principal['NoticiumDetail']['titulo_capa'])){
								$title = $principal['NoticiumDetail']['titulo_capa'];
							}
            ?>
            <div class="p-0 col-md-6 col-lg-6 col-sm-12 col-xs-12">
              <article class="destaque3 zm-trending-post zm-lay-c large zm-single-post" data-dark-overlay="0"  data-scrim-bottom="9" data-effict-zoom="1">
                <div class="zm-post-thumb">
                    <div class="lazyload">
                        <!--
                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                         -->
                    </div>
                </div>
                <div class="zm-post-dis text-white">
                  <div class="zm-category"><a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>"><?php echo $categoria; ?></a></div>
                  <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                </div>
              </article>
            </div>
          <?php endif; ?>
          <!-- End single trend post -->

          <!-- Start single trend post -->
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <div class="row">
              <?php if (isset($nodesList[3])): ?>
                <?php 
                  $node_3 = $nodesList[3];
                  $link_noticia = $node_3['Node']['path'];
                  if (isset($node_3['NoticiumDetail']['link_externo']) && !empty($node_3['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node_3['NoticiumDetail']['link_externo'];
                  }

                  $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/10.jpg';
                  if(isset($node_3['Multiattach']) && !empty($node_3['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $node_3['Multiattach']['filename'],
                      'dimension' => '356x223'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($node_3['Multiattach']['meta']) ? $node_3['Multiattach']['meta'] : $node_3['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $categoria = $this->Mswi->categoryName($node_3['Node']['terms']);
                  if(isset($node_3['NoticiumDetail']['chapeu']) && !empty($node_3['NoticiumDetail']['chapeu'])){
                    $categoria = $node_3['NoticiumDetail']['chapeu'];
                  }
                  $categoria_slug = $this->Mswi->categorySlug($node_3['Node']['terms']);
                  $categoria_color = $this->Mswi->categoryColor($node_3['Node']['terms']);

                  $title = $node_3['Node']['title'];
                  if(isset($node_3['NoticiumDetail']['titulo_capa']) && !empty($node_3['NoticiumDetail']['titulo_capa'])){
                    $title = $node_3['NoticiumDetail']['titulo_capa'];
                  }
                ?>
                <div class="col-md-12 col-sm-6 col-lg-12 col-xs-12 p-0">
                  <article class="destaque4 zm-trending-post zm-lay-c zm-single-post" data-dark-overlay="0"  data-scrim-bottom="9" data-effict-zoom="1">
                    <div class="zm-post-thumb">
                        <div class="lazyload">
                            <!--
                            <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                            -->
                        </div>
                    </div>
                    <div class="zm-post-dis text-white">
                      <div class="zm-category"><a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>"><?php echo $categoria; ?></a></div>
                      <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                    </div>
                  </article>
                </div>
              <?php endif; ?>
              
              <?php if (isset($nodesList[4])): ?>
                <?php 
                  $node_4 = $nodesList[4];
                  $link_noticia = $node_4['Node']['path'];
                  if (isset($node_4['NoticiumDetail']['link_externo']) && !empty($node_4['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $node_4['NoticiumDetail']['link_externo'];
                  }

                  $imagemUrl = '/theme/'.Configure::read('Site.theme').'/images/post/trend/c/5.jpg';
                  if(isset($node_4['Multiattach']) && !empty($node_4['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $node_4['Multiattach']['filename'],
                      'dimension' => '356x223'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($node_4['Multiattach']['meta']) ? $node_4['Multiattach']['meta'] : $node_4['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $categoria = $this->Mswi->categoryName($node_4['Node']['terms']);
                  if(isset($node_4['NoticiumDetail']['chapeu']) && !empty($node_4['NoticiumDetail']['chapeu'])){
                    $categoria = $node_4['NoticiumDetail']['chapeu'];
                  }
                  $categoria_slug = $this->Mswi->categorySlug($node_4['Node']['terms']);
                  $categoria_color = $this->Mswi->categoryColor($node_4['Node']['terms']);

                  $title = $node_4['Node']['title'];
                  if(isset($node_4['NoticiumDetail']['titulo_capa']) && !empty($node_4['NoticiumDetail']['titulo_capa'])){
                    $title = $node_4['NoticiumDetail']['titulo_capa'];
                  }
                ?>
                <div class="col-md-12 col-sm-6 col-lg-12 col-xs-12 p-0">
                  <article class="destaque5 zm-trending-post zm-lay-c zm-single-post" data-dark-overlay="0"  data-scrim-bottom="9" data-effict-zoom="1">
                    <div class="zm-post-thumb">
                        <div class="lazyload">
                            <!--
                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                            -->
                        </div>
                    </div>
                    <div class="zm-post-dis text-white">
                      <div class="zm-category"><a href="/noticia/term/<?php echo $categoria_slug ?>" class="bg-dark cat-btn" style="<?php echo $categoria_color ?>"><?php echo $categoria; ?></a></div>
                      <h2 class="zm-post-title"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                    </div>
                  </article>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- End single trend post -->
        </div>
      </div>
    </div>
  </div>
  <!-- End trending post area -->
<?php endif; ?>
