<div class="zm-section bg-gray pt-70 pb-40">
  <div class="container">
    <div class="row pb-40">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 columns">
        <?php if (!empty($nodesList)): ?>
          <?php 
            if($block['Block']['show_title']){
              $title_block = $block['Block']['title'];
            }else{
              $title_block = $this->Mswi->categoryName($nodesList[0]['Node']['terms']);
            }
          ?>
          <div class="row mb-40">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="section-title">
                <h2 class="h6 header-color inline-block uppercase"><?php echo $title_block; ?></h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="zm-posts clearfix">
              <?php foreach ($nodesList as $n): ?>
                <?php 
                  $link_noticia = null;
                  $imagemUrl = null;
                  $imageTitle = null;
                  $title = null;
                  
                  $link_noticia = $n['Node']['path'];
                  if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                    $link_noticia = $n['NoticiumDetail']['link_externo'];
                  }

                  if(isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                    $imagemUrlArray = array(
                      'plugin' => 'Multiattach',
                      'controller' => 'Multiattach',
                      'action' => 'displayFile', 
                      'admin' => false,
                      'filename' => $n['Multiattach']['filename'],
                      'dimension' => '270x360'
                    );
                    $imagemUrl = $this->Html->url($imagemUrlArray);
                    $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                  }

                  $title = $n['Node']['title'];
                  if(isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])){
                    $title = $n['NoticiumDetail']['titulo_capa'];
                  }
                ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                    <?php if (isset($imagemUrl)): ?>
                      <div class="zm-post-thumb">
                          <div class="lazyload">
                              <!--
                                    <a href="<?php echo $link_noticia; ?>" data-dark-overlay="2.5"  data-scrim-bottom="9">
                                    <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle ?>">
                                    </a>
                                    -->
                          </div>
                      </div>
                    <?php endif; ?>
                    <div class="zm-post-dis text-white">
                      <h2 class="zm-post-title h3"><a href="<?php echo $link_noticia ?>"><?php echo $title ?></a></h2>
                    </div>
                  </article>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
        
        <?php
          //Se tem algum bloco ativo nesta região
          if($this->Regions->blocks('publicidade_home_5')){
            echo $this->Regions->blocks('publicidade_home_5');
          }
        ?>
      </div>
      
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 sidebar-warp columns">
        <div class="row">
          <?php
            //Se tem algum bloco ativo nesta região
            if($this->Regions->blocks('right_home2')){
              echo $this->Regions->blocks('right_home2');
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
