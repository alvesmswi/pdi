<?php if (isset($node['NoticiumDetail']['keywords']) && !empty($node['NoticiumDetail']['keywords'])): ?>
  <?php
    $this->Html->meta(
      array(
        'name' => 'keywords', 
        'content' => $node['NoticiumDetail']['keywords']
      ),
      null,
      array('inline' => false)
    );

    //pega cada palavras
	  $arrayKeywords = explode(',', $node['NoticiumDetail']['keywords']);
  ?>
  <div class="meta-list pull-left">
    <span class="post-title">Tags</span>
    <?php foreach ($arrayKeywords as $key => $keywords): ?>
      <?php
        //pelo Google
				$link = '/search?q='.urlencode(trim($keywords));
				//Pelo CMS
        $link = '/nodes/nodes/keywords/'.urlencode(trim($keywords));
        
        $sep = ($key + 1 != count($arrayKeywords)) ? ',' : null;
      ?>
      <a href="<?php echo $link; ?>"><?php echo trim($keywords); ?></a><?php echo $sep ?>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
