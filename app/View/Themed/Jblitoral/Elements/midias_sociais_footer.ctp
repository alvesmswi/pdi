<div class="col-xs-12 col-sm-4 col-md-6 col-lg-2">
  <div class="zm-widget">
    <h2 class="h6 zm-widget-title uppercase text-white mb-30">Mídias Sociais</h2>
    <div class="zm-widget-content">
      <div class="zm-social-media zm-social-1">
        <ul>
          <?php if (!empty(Configure::read('Social.facebook'))) { ?>
            <li><a href="<?php echo Configure::read('Social.facebook') ?>"><i class="fa fa-facebook"></i>Siga-nos no Facebook</a></li>
          <?php } ?>
          <?php if (!empty(Configure::read('Social.twitter'))) { ?>
            <li><a href="<?php echo Configure::read('Social.twitter') ?>"><i class="fa fa-twitter"></i>Siga-nos no Twitter</a></li>
          <?php } ?>
          <?php if (!empty(Configure::read('Social.youtube'))) { ?>
            <li><a href="<?php echo Configure::read('Social.youtube') ?>"><i class="fa fa-youtube"></i>Siga-nos no YouTube</a></li>
          <?php } ?>
          <?php if (!empty(Configure::read('Social.instagram'))) { ?>
            <li><a href="<?php echo Configure::read('Social.instagram') ?>"><i class="fa fa-instagram"></i>Siga-nos no Instagram</a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
