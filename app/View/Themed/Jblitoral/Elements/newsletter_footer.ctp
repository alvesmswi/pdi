<div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
  <div class="zm-widget">
    <h2 class="h6 zm-widget-title uppercase text-white mb-30">Newsletter</h2>
    <div class="zm-widget-content">
      <div class="subscribe-form subscribe-footer">
        <p>Assine nossa Newsletter</p>
        <form action="/mail_chimp/news/subscribe/ultimas-noticias" method="post">
          <input type="email" placeholder="Digite o seu e-mail" name="email" required="required">
          <input type="submit" value="Inscrever-se">
        </form>
      </div>
    </div>
  </div>
</div>
