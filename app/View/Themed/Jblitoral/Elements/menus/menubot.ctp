<?php if (!empty($menu['threaded'])): ?>
<nav class="footer-menu zm-secondary-menu text-right">
  <ul>
    <?php foreach ($menu['threaded'] as $item): ?>
      <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>
      <li><a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a></li>
    <?php endforeach; ?>
  </ul>
</nav>
<?php endif; ?>
