<?php if (!empty($menu['threaded'])): ?>
  <li class="mega-parent drop">
    <a href="#"><?php echo $menu['Menu']['title']?></a>
    <div class="mega-menu-area dropdown clearfix">
      <div class="zm-megamenu-sub-cats">
        <ul class="zm-megamenu-sub-tab" role="tablist">
          <li role="presentation">
            <a href="#megamenutodas" aria-controls="megamenutodas" role="tab" data-toggle="tab" class="menu-todas">Todas</a>
          </li>
          <?php foreach ($menu['threaded'] as $keyMenu => $item): ?>
            <?php
              $class = ($keyMenu == 0) ? 'active' : null;
            ?>
            <li role="presentation" class="<?php echo $class; ?>">
              <a href="#megamenu<?php echo $item['Link']['class']; ?>" aria-controls="megamenu<?php echo $item['Link']['class']; ?>" role="tab" data-toggle="tab" class="menu-<?php echo $item['Link']['class']; ?>"><?php echo $item['Link']['title']; ?></a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="zm-megamenu-content">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade" id="megamenutodas">
            <div class="mega-caro-wrap zm-posts clearfix">
              <?php
                $nodes = $this->requestAction('/nodes/nodes/megamenu');
              ?>
              <?php if (!empty($nodes)): ?>
                <?php foreach ($nodes as $key => $n): ?>
                  <?php
                    $link_noticia = $n['Node']['path'];
                    if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                      $link_noticia = $n['NoticiumDetail']['link_externo'];
                    }

                    if (isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                      $imagemUrlArray = array(
                        'plugin' => 'Multiattach',
                        'controller' => 'Multiattach',
                        'action' => 'displayFile', 
                        'admin' => false,
                        'filename' => $n['Multiattach']['filename'],
                        'dimension' => '231x108'
                      );
                      $imagemUrl = $this->Html->url($imagemUrlArray);
                      $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                    }

                    $title = $n['Node']['title'];
                    if (isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])) {
                      $title = $n['NoticiumDetail']['titulo_capa'];
                    }
                  ?>
                  <div class="single-mega-post">
                    <article class="zm-mega-post zm-post-lay-a2">
                      <?php if (isset($imagemUrl)): ?>
                        <div class="zm-post-thumb">
                            <div class="lazyload">
                                <!--
                                  <a href="<?php echo $link_noticia; ?>">
                                    <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                                  </a>
                              -->
                            </div>
                        </div>
                      <?php endif; ?>
                      <div class="zm-post-dis">
                        <div class="zm-post-header">
                          <h2 class="zm-post-title h2"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                        </div>
                      </div>
                    </article>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
          <?php foreach ($menu['threaded'] as $keyMenu => $item): ?>
            <?php
              $class = ($keyMenu == 0) ? ' in active' : null;
            ?>
            <div role="tabpanel" class="tab-pane fade<?php echo $class ?>" id="megamenu<?php echo $item['Link']['class']; ?>">
              <div class="mega-caro-wrap zm-posts clearfix">
                <?php
                  $nodes = $this->requestAction('/nodes/nodes/megamenu/'.$item['Link']['class']);
                ?>
                <?php if (!empty($nodes)): ?>
                  <?php foreach ($nodes as $key => $n): ?>
                    <?php
                      $link_noticia = $n['Node']['path'];
                      if (isset($n['NoticiumDetail']['link_externo']) && !empty($n['NoticiumDetail']['link_externo'])) {
                        $link_noticia = $n['NoticiumDetail']['link_externo'];
                      }

                      if (isset($n['Multiattach']) && !empty($n['Multiattach']['filename'])) {
                        $imagemUrlArray = array(
                          'plugin' => 'Multiattach',
                          'controller' => 'Multiattach',
                          'action' => 'displayFile', 
                          'admin' => false,
                          'filename' => $n['Multiattach']['filename'],
                          'dimension' => '231x108'
                        );
                        $imagemUrl = $this->Html->url($imagemUrlArray);
                        $imageTitle = htmlspecialchars((!empty($n['Multiattach']['meta']) ? $n['Multiattach']['meta'] : $n['Node']['title']), ENT_QUOTES, 'UTF-8');
                      }

                      $title = $n['Node']['title'];
                      if (isset($n['NoticiumDetail']['titulo_capa']) && !empty($n['NoticiumDetail']['titulo_capa'])) {
                        $title = $n['NoticiumDetail']['titulo_capa'];
                      }
                    ?>
                    <div class="single-mega-post">
                      <article class="zm-mega-post zm-post-lay-a2">
                        <?php if (isset($imagemUrl)): ?>
                          <div class="zm-post-thumb">
                              <div class="lazyload">
                                  <!--
                                    <a href="<?php echo $link_noticia; ?>">
                                      <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $imageTitle; ?>">
                                    </a>
                                -->
                              </div>
                          </div>
                        <?php endif; ?>
                        <div class="zm-post-dis">
                          <div class="zm-post-header">
                            <h2 class="zm-post-title h2"><a href="<?php echo $link_noticia; ?>"><?php echo $title; ?></a></h2>
                          </div>
                        </div>
                      </article>
                    </div>
                  <?php endforeach; ?>
                <?php endif; ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </li>
<?php endif; ?>
