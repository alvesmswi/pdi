<div class="header-bottom-area hidden-sm hidden-xs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="menu-wrapper  bg-theme clearfix">
          <div class="row">
            <div class="col-md-11">
              <div class="mainmenu-area">
                <?php if (!empty($menu['threaded'])): ?>
                  <nav class="primary-menu uppercase">
                    <ul class="clearfix">
                      <?php echo $this->Menus->menu('megamenu', array('element' => 'menus/megamenu')); ?>                                            
                      
                      <?php foreach ($menu['threaded'] as $item): ?> 
                        <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>

                        <?php if (!empty($item['children'])): ?>
                          <li class="drop">
                            <a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a>
                            <ul class="dropdown">
                              <?php foreach ($item['children'] as $child): ?>
                                <li>
                                  <a href="<?php echo $this->Html->url($this->Mswi->linkToUrl($child['Link']['link'])); ?>">
                                    <?php echo $child['Link']['title']; ?>
                                  </a>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                          </li>
                        <?php else: ?>
                          <li><a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a></li>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </ul>
                  </nav>
                <?php endif; ?>
              </div>
            </div>
            <div class="col-md-1">
              <div class="search-wrap pull-right">
                <div class="search-btn"><i class="fa fa-search"></i></div>
                <div class="search-form">
                  <form action="/search">
                    <input type="search" placeholder="Buscar..." name="q">
                    <button type="submit"><i class='fa fa-search'></i></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- mobile-menu-area start -->
<div class="mobile-menu-area hidden-md hidden-lg">
  <div class="fluid-container">
    <div class="mobile-menu">
      <div class="search-wrap mobile-search">
        <div class="mobile-search-btn"><i class="fa fa-search"></i></div>
        <div class="mobile-search-form">
          <form action="/search">
            <input type="text" placeholder="Buscar..." name="q">
            <button type="submit"><i class='fa fa-search'></i></button>
          </form>
        </div>
      </div>
      <?php if (!empty($menu['threaded'])): ?>
        <nav id="mobile_dropdown">
          <ul>
            <?php foreach ($menu['threaded'] as $item): ?>
              <?php $link = $this->Mswi->linkToUrl($item['Link']['link']); ?>

              <?php if (!empty($item['children'])): ?>
                <li>
                  <a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a>
                  <ul>
                    <?php foreach ($item['children'] as $child): ?>
                      <?php if (!empty($child['children'])): ?>
                        <li>
                          <a href="<?php echo $this->Html->url($this->Mswi->linkToUrl($child['Link']['link'])); ?>"><?php echo $child['Link']['title']; ?></a>
                          <ul>
                            <?php foreach ($child['children'] as $child2): ?>
                              <li>
                                <a href="<?php echo $this->Html->url($this->Mswi->linkToUrl($child2['Link']['link'])); ?>">
                                  <?php echo $child2['Link']['title']; ?>
                                </a>
                              </li>
                            <?php endforeach; ?>
                          </ul>
                        </li>
                      <?php else: ?>
                        <li>
                          <a href="<?php echo $this->Html->url($this->Mswi->linkToUrl($child['Link']['link'])); ?>">
                            <?php echo $child['Link']['title']; ?>
                          </a>
                        </li>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                </li>
              <?php else: ?>
                <li><a href="<?php echo $this->Html->url($link); ?>"><?php echo $item['Link']['title']; ?></a></li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </nav>
      <?php endif; ?>
    </div>       
  </div>
</div>

