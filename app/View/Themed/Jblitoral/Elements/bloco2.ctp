<section id="page-content" class="page-wrapper">
  <?php if ($this->Regions->blocks('categorias')): ?>
    <div class="zm-section bg-gray ptb-70">
      <div class="container">
        <div class="row">
          <?php echo $this->Regions->blocks('categorias'); ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
</section>