<?php 
//se está logado
$roleId = AuthComponent::user('Role.alias');
if(
	($roleId == 'admin') || 	//Admin
	($roleId == 'gerente') || 	//Gerente
	($roleId == 'editor') || 	//Editor
	($roleId == 'colunista')	//Colunista
){
?>
<style>
   @import url('https://fonts.googleapis.com/icon?family=Material+Icons');
   body {
	  margin: 0px;
	  padding-top: 45px;
   }

   #c-topbar-admin {
	  position: fixed;
	  /*margin-top: -45px;*/
	  top: 0px;
   }

   /* Objects */

   #c-topbar-admin .o-wrapper {
	  /* width: 100%; */
	  display: block;
   }

   .o-sr-only {
	  position: absolute;
	  width: 1px;
	  height: 1px;
	  padding: 0;
	  margin: -1px;
	  overflow: hidden;
	  clip: rect(0, 0, 0, 0);
	  border: 0;
   }

   .o-divider {
	  height: 1px;
	  margin: 0;
	  overflow: hidden;
	  background-color: #e5e5e5;
   }

   .material-icons {
	  font-size: 18px;
	  line-height: 40px;
	  margin: 0px 5px;
   }

   /* Elements */

   nav#c-topbar-admin a {
	  color: #fff;
	  font-family: Arial, Helvetica, sans-serif;
	  text-decoration: none;
	  font-size: 14px;
	  line-height: 45px;
	  height: 45px;
	  padding: 0px 15px;
	  /* width: 100%; */
	  display: flex;
	  align-items: center;
   }

   nav#c-topbar-admin a:hover,
   nav#c-topbar-admin a:focus {
	  background-color: lightgray;
	  color: #333;
   }

   #c-topbar-admin button {
	  border: 0px;
	  background-color: #666;
	  height: 36px;
	  margin: 5px !important;
	  line-height: 28px;
   }

   #c-topbar-admin button:hover,
   #c-topbar-admin button:focus {
	  cursor: pointer;
	  background-color: #999;
	  color: white;
   }

   nav#c-topbar-admin ul {
	  list-style-type: none;
   }

   nav#c-topbar-admin>ul {
	  float: left;
   }

   nav#c-topbar-admin ul li {
	  /* margin: 0px 10px; */
	  line-height: 45px;
   }

   /* Components */

   /* nav */

   .c-nav {
	  margin: 0px;
	  padding: 0px;
	  height: 100%;
   }

   .c-nav__item {
	  float: left;
   }

   .c-dropdown {
	  position: relative;
	  -webkit-user-select: none;
	  /* Chrome/Safari */
	  -moz-user-select: none;
	  /* Firefox
	  */
	  -ms-user-select: none;
	  /* IE10+ */
	  /* Rules below not implemented in browsers yet */
	  -o-user-select: none;
	  user-select: none;
	  cursor: pointer;
	  display: inline-block;
   }

   .c-dropdown__menu {
	  background-color: #333;
	  /* display: block; */
	  /* padding:0px; */
	  /* margin-top: 25px; */
	  border: 1px solid #fff;
	  /* left:
	  0%; */
	  position: absolute;
	  top: 45px;
	  /* display:none; */
	  margin-top: 0px;
	  border-top-left-radius: 0;
	  border-bottom-left-radius: 0;
	  border-left-color: #fff;
	  box-shadow: none;
	  overflow: hidden;
	  width: 200px;
	  height: auto;
	  display: none;
   }

   .c-dropdown.is--active .c-dropdown__menu {
	  display: block;
   }

   .c-topbar__nav.is--right .c-dropdown__menu {
	  right: 0px;
   }

   .c-dropdown__menu.c-nav .c-nav__item {
	  /* clear: both; */
	  float: none;
	  overflow: hidden;
   }

   .c-dropdown__menu.c-nav .c-nav__item a {
	  width: 100%;
	  padding-left: 20px;
   }

   /* Button */

   .c-btn {
	  padding: 5px;
   }

   /* search */

   .c-topbar__search {
	  display: inline-block;
   }

   .c-input-search {
	  font-size: 14px;
	  line-height: 36px;
	  padding: 0px 5px;
	  margin: 5px 0 0px 15px;
	  border: 0px;
	  float: left;
	  width: 60%;
	  height: 36px;
   }

   .c-topbar__search .c-btn {
	  float: left;
	  padding: 0px;
	  margin-top: 5px;
	  height: 36px;
	  border: 0px;
	  margin-left: 0px !important;
   }

   /* Topbar */

   .c-topbar-admin {
	  z-index: 9999;
   }

   .c-topbar {
	  background: #333;
	  color: #fff;
	  margin: 0;
	  box-sizing: border-box;
	  font-size: 16px;
	  display: inline-block;
	  width: 100%;
   }

   .c-topbar__header,
   .c-topbar__navbar {
	  float: left;
	  display: block;
	  height: 45px;
   }

   .c-topbar__header {
	  width: 20%;
   }

   .c-topbar__navbar {
	  width: 80%;
   }

   .c-topbar__brand {
	  padding: 2px;
	  float: left;
	  line-height: 45px;
	  height: 45px;
   }

   nav#c-topbar-admin a.c-topbar__brand:hover,
   nav#c-topbar-admin a.c-topbar__brand:focus {
	  background-color: #333;
   }

   /* Utils */

   .is--left {
	  float: left;
   }

   .is--right {
	  float: right
   }

   .is--active>a {
	  background-color: lightgray;
	  color: #333;
   }

   .c-topbar>.o-wrapper {
	  max-width: 1200px;
	  margin: 0 auto;
   }

   @media (min-width: 1200px) {
	  .o-hidden--lg {
		 display: none;
	  }
	  .c-dropdown:hover .c-dropdown__menu,
	  .c-dropdown:focus .c-dropdown__menu {
		 display: inline-block;
	  }
   }

   /* Large Devices,
	  Wide Screens */

   @media only screen and (max-width: 1200px) {
	  .c-topbar>.o-wrapper {
		 max-width: 992px;
	  }
   }

   @media (min-width: 993px) and (max-width: 1200px) {
	  .o-hidden--lg {
		 display: none;
	  }
	  .c-topbar__search {
		 width: 25%;
	  }
	  .c-dropdown:hover .c-dropdown__menu,
	  .c-dropdown:focus .c-dropdown__menu {
		 display: inline-block;
	  }
   }

   /* Medium Devices, Desktops */

   @media only screen and (max-width: 992px) {
	  .c-topbar>.o-wrapper {
		 max-width: 768px;
	  }
   }

   @media (min-width: 769px) and (max-width: 992px) {
	  .o-hidden--md {
		 display: none;
	  }
	  .c-topbar__search {
		 /* padding: 0px 0px 0px 14px; */
		 padding: 0px 0px 0px 5px;
		 position: relative;
	  }
	  .c-input-search {
		 width: 0px;
		 position: absolute;
		 right: 28px;
		 transition: width .4s cubic-bezier(0.215, 0.610, 0.355, 1);
	  }
	  .c-topbar__search:hover .c-input-search {
		 position: absolute;
		 width: 180px;
	  }
	  .c-dropdown:hover .c-dropdown__menu,
	  .c-dropdown:focus .c-dropdown__menu {
		 display: inline-block;
	  }
   }

   /* Small Devices, Tablets */

   @media only screen and (max-width: 768px) {
	  .c-topbar>.o-wrapper {
		 max-width: 480px;
	  }
	  #c-admin-topbar .navbar-form {
		 padding: 15px;
	  }
	  #c-topbar-admin--collapse {
		 display: none;
	  }
	  #c-topbar-admin--collapse.is--expanded {
		 display: block;
		 height: auto;
	  }
	  #c-topbar-admin button#js-btn--open-menu.is--actived {
		 background-color: #333;
		 color: #fff;
		 outline: 1px lightgray solid;
	  }
	  .c-topbar-admin .c-nav__item {
		 float: none;
		 width: 100%;
	  }
	  .c-topbar__search {
		 float: none;
		 width: 100%;
		 height: 45px;
		 position: absolute;
		 text-align: center;
		 top: 0;
		 left: 0;
		 background-color: #333;
		 display: flex;
	  }
	  .c-topbar__search .o-wrapper {
		 display: inline-block;
		 margin: 0px auto;
	  }
	  .c-input-search {
		 width: 180px;
		 /* margin-left: 0px; */
	  }
	  .c-topbar__search .c-btn {
		 margin-right: 0px !important;
	  }
	  .c-topbar__nav {
		 background-color: #333;
		 width: 100%;
		 height: auto;
		 float: none;
		 display: inline-block;
		 text-align: left;
		 left: 0px;
	  }
	  .c-dropdown__menu {
		 position: relative;
		 top: 0px;
		 left: 0px;
		 width: 100%;
		 border: 0px;
		 display: none;
	  }
	  .c-topbar__header {
		 width: 100%;
	  }
	  .c-topbar__header .c-btn--toggle {
		 float: right;
		 margin: 10px 5px;
	  }
	  .c-topbar__navbar {
		 height: 100%;
		 width: 280px;
		 position: absolute;
		 top: 45px;
		 right: 16%;
		 margin: 0px auto;
		 text-align: center;
		 /*padding-top: 45px;*/
	  }
   }

   /* @media (min-width: 769px) and (max-width : 992px) {} */

   /* Extra Small Devices, Phones */

   @media only screen and (max-width: 480px) {
	  .c-topbar>.o-wrapper {
		 min-width: 320px;
	  }
	  .c-topbar__navbar {
		 left: 0;
		 right: 0;
	  }
   }

   /* Custom, iPhone Retina */

   @media only screen and (max-width: 320px) {
	  .o-hidden--sm {
		 display: none;
	  }
   }
</style>

<?php 
	//se é node e não é um colunista
	if(isset($node['Node']['id']) && $node['Node']['slug'] <> 'home' && $roleId <> 'colunista') { 
		$linkEditar = '/admin/nodes/nodes/edit/'.$node['Node']['id'];
	}
	if(isset($post['Post']['id'])){
		//Autorização para editar
		if(
			($roleId == 'admin') || 	//Admin
			($roleId == 'gerente') || 	//Gerente
			($roleId == 'editor')) 		//Editor
		{ 
			$linkEditar = '/admin/blog/blogs/edit_post/'.$post['Post']['id'];
		}else if(
			isset($post['Blog']['Blogueiros']) && !empty($post['Blog']['Blogueiros'])) //dono do blog
		{
			foreach($post['Blog']['Blogueiros'] as $blogueiro){
				if($blogueiro['user_id'] == $_SESSION['Auth']['User']['id']){
					$linkEditar = '/admin/blog/blogs/edit_post/'.$post['Post']['id'];
				}
			}
		}
		
	}

?>
   <nav class="c-topbar c-topbar-admin" id="c-topbar-admin">
	  <div class="o-wrapper">

		 <!-- Brand and toggle get grouped for better mobile display -->
		 <div class="c-topbar__header">
			<button type="button" class="c-btn c-btn--toggle o-hidden--lg o-hidden--md" id="js-btn--open-menu">
			   <span>ADMIN MENU</span>
			   <span role="icon" class="c-icon c-icon--menu"></span>
			</button>
			<a class="c-topbar__brand" href="http://mswi.com.br/cmswi/" target="_blank">
			   <img src="/site/img/logo-mswi-cinza.png" height="40">
			</a>
		 </div>


		 <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="c-topbar__navbar" id="c-topbar-admin--collapse">
			<ul class="c-nav c-topbar__nav is--left">
				<li class="c-nav__item c-dropdown js-dropdown">
					<a href="#" class="c-dropdown--toggle">
						<i role="icon" class="material-icons">note_add</i>Adicionar
						<i role="icon" class="material-icons">arrow_drop_down</i>
					</a>
					<ul class="c-nav c-dropdown__menu">
						<li class="c-nav__item">
							<a href="/admin/nodes/nodes/add/noticia">Notícia</a>
						</li>
						<li class="c-nav__item">
							<a href="/admin/nodes/nodes/add/edital">Edital</a>
						</li>
						<li class="c-nav__item">
							<a href="/admin/blog/blogs/create_post">Post</a>
						</li>
					</ul>
			   	</li>
			   	<?php 
				//se tem link para editar
				if(isset($linkEditar)){ ?>
					<li class="c-nav__item">
						<a href="<?php echo $linkEditar; ?>">
							<i role="icon" class="material-icons">edit</i> Editar
						</a>
					</li>
				<?php } ?>
				<?php 
				//Se não é Colunista
				if($roleId <> 'colunista') { ?>
					<li class="c-nav__item">
						<a href="/admin/nodes">
							<i role="icon" class="material-icons">list</i>Listar Conteúdo</a>
					</li>
			   <?php } ?>
			   <li class="c-nav__item">
				  <a href="/admin/blog/posts">
					 <i role="icon" class="material-icons">list</i> Listar Posts</a>
			   </li>			   
			</ul>
			
			<ul class="c-nav c-topbar__nav is--right">
			   <li class="c-nav__item c-dropdown js-dropdown">
				  <a href="#" class="c-dropdown--toggle">
					 <span class="o-hidden--sm"><?php echo $_SESSION['Auth']['User']['name']; ?></span>
					 <i role="icon" class="material-icons">account_circle</i>
				  </a>
				  <ul class="c-dropdown__menu c-nav c-topbar__nav">
					 <li class="c-nav__item">
						<a href="/admin/users/users/edit/1/<?php echo $_SESSION['Auth']['User']['id']; ?>">
						   <i class="material-icons">assignment</i> Meus Dados</a>
					 </li>
					 <li class="c-nav__item">
						<a href="/admin">
						   <i class="material-icons">settings</i> Configurações</a>
					 </li>
					 <li class="c-nav__item">
						<a href="http://mswi.com.br/cmswi/index.php/2018/04/30/noticias/" target="_blank">
						   <i class="material-icons">help_outline</i> Ajuda</a>
					 </li>
					 <li role="separator" class="o-divider"></li>
					 <li class="c-nav__item">
						<a href="/admin/users/users/logout">
						   <i class="material-icons">exit_to_app</i> Sair</a>
					 </li>
				  </ul>
			   </li>
			</ul>
		 </div>
		 <!-- /.navbar-collapse -->
	  </div>
   </nav>
   <script>
	  var btnToggles = document.querySelectorAll('a.c-dropdown--toggle');

	  for (var i = 0; i < btnToggles.length; i++) {
		 btnToggles[i].addEventListener("click", toggle);
	  }

	  function toggle() {
		 if (window.innerWidth < 769) {
			for (var i = 0; i < btnToggles.length; i++) {
			   if (btnToggles[i].parentNode.classList.contains('is--active')) {
				  btnToggles[i].parentNode.classList.remove('is--active');
			   } else {
				  if (btnToggles[i] == this) {
					 btnToggles[i].parentNode.classList.add('is--active');
				  }

			   }
			}
		 }
		 // console.log(this.parentNode);
	  }


	  document.querySelector('#js-btn--open-menu').addEventListener("click", toggleMenuMobile);

	  function toggleMenuMobile() {
		 document.querySelector('#c-topbar-admin--collapse').classList.toggle('is--expanded');
		 this.classList.toggle('is--actived');
		 for (var i = 0; i < btnToggles.length; i++) {
			btnToggles[i].parentNode.classList.remove('is--active');
		 }
	  }

	  window.onresize = function () {
		 for (var i = 0; i < btnToggles.length; i++) {
			btnToggles[i].parentNode.classList.remove('is--active');


		 }
		 if (window.innerWidth > 769) {
		 document.querySelector('#js-btn--open-menu').classList.remove('is--actived');
		 document.querySelector('#c-topbar-admin--collapse').classList.remove('is--expanded');
		 }
	  };
   </script>
<?php } ?>