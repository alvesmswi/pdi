<?php 
$parametros = '';
//se tem filtro
if(isset($this->params->query) && !empty($this->params->query)){
    foreach($this->params->query as $key => $value){
        if($key != 'anteriorId' && $key != 'proximoId' && $key != 'voltarId'){
            if(empty($parametros)){
                $parametros .= '?'.$key.'='.$value;
            }else{
                $parametros .= '&'.$key.'='.$value;
            }
        }        
    }
}
if(isset($this->params->query['voltarId'])){
    $urlAnterior = $this->here.$parametros.'?proximoId='.$this->params->query['voltarId'];    
}else{
    //$urlAnterior = $this->here.$parametros.'?anteriorId='.$primeiroId;    
}
$urlAvancar = $this->here.$parametros.'?proximoId='.$ultimoId.'&voltarId='.$primeiroId;
?>

<div class="pagination">
    <ul class="pagination bp-pagination">
        <?php if(isset($urlAnterior)) { ?>
            <li><a href="<?php echo $urlAnterior; ?>">< Anterior</a></li>
        <?php } ?>
        <?php if(count($nodes) >= $limit) { ?>
            <li><a href="<?php echo $urlAvancar; ?>">Próximo ></a></li>
        <?php } ?>
    </ul>
</div>