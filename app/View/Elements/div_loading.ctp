<style>
	#divLoading {
		position: fixed; /* Sit on top of the page content */
		width: 100%; /* Full width (cover the whole page) */
		height: 100%; /* Full height (cover the whole page) */
		display: none;
		top: 0; 
		left: 0;
		right: 0;
		bottom: 0;
		z-index: 999; /* Specify a stack order in case you're using a different order for other elements */
		cursor: pointer; /* Add a pointer on hover */
		background: url(/details/img/loading.gif) no-repeat scroll 50% 50% rgba(0,0,0,0.5);
	}
</style>

<div id="divLoading">
	Aguarde...
</div>