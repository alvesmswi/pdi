<?php
//Este elemento cria as metas de todos os nodes
$this->Html->meta(
	array(
		'property' => 'author', 
		'content' => 'PDi Publicador Digital Inteligente - MSWI Soluções Web Inteligentes'
	),
	null,
	array('inline'=>false)
);

$this->Html->meta(
	array('name' => 'twitter:card', 'content' => 'summary'),
	null,
	array('inline'=>false)
);

//VIEW
if($this->params['action'] == 'view' && $this->here != '/'){
	//se está setado o noindex
	if(isset($nodeParams['noindex']) && $nodeParams['noindex']){
		//gera o description
		$this->Html->meta(
			array(
				'name' => 'robots', 
				'content' => 'noindex'
			),
			null,
			array('inline'=>false)
		);
	}
	$this->Html->meta(
		array(
			'property' => 'og:title', 
			'content' => $node['Node']['title']
		),
		null,
		array('inline'=>false)
	);
	$this->Html->meta(
		array(
			'property' => 'og:description', 
			'content' => (isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt']) ? $node['Node']['excerpt'] : "")
		),
		null,
		array('inline'=>false)
	);
	$this->Html->meta(
		array(
			'property' => 'og:url', 
			'content' => Router::url(null, true)
		),
		null,
		array('inline'=>false)
	);
	//se tem chamada
	if(isset($node['Node']['excerpt']) && !empty($node['Node']['excerpt'])){
		$description = $node['Node']['excerpt'];
	}else{
		$description = substr($node['Node']['body'],0,256);
		$description = strip_tags($description);
	}
	//gera o description
	$this->Html->meta(
		array(
			'name' => 'description', 
			'content' => $description
		),
		null,
		array('inline'=>false)
	);
	//Gera as keywords
	$this->Html->meta(
		array(
			'name' => 'keywords', 
			'content' => $this->Mswi->gerarKeywords($node['Node']['title'].' '.$description)
		),
		null,
		array('inline'=>false)
	);
}

//LISTA
if($this->params['action'] == 'index'){
	$this->Html->meta(
        array(
            'name' => 'description', 
            'content' => 'Lista de '.$this->params['named']['type']
        ),
        null,
        array('inline'=>false)
    );
	$this->Html->meta(
        array(
            'name' => 'keywords', 
            'content' => $this->params['named']['type'].', '.Configure::read('Site.keywords')
        ),
        null,
        array('inline'=>false)
    );
}

//TERMO
if($this->params['action'] == 'term'){
	$this->Html->meta(
        array(
            'name' => 'description', 
            'content' => $this->Mswi->slugName($this->params['named']['slug'])
        ),
        null,
        array('inline'=>false)
    );
	$this->Html->meta(
        array(
            'name' => 'keywords', 
            'content' => $this->Mswi->slugName($this->params['named']['slug']).', '.Configure::read('Site.keywords')
        ),
        null,
        array('inline'=>false)
    );
}

//Se é a Home
if($this->here == '/'){
	$this->Html->meta(
        array(
            'name' => 'description', 
            'content' => Configure::read('Site.tagline')
        ),
        null,
        array('inline'=>false)
    );
	$this->Html->meta(
        array(
            'name' => 'keywords', 
            'content' => Configure::read('Site.keywords')
        ),
        null,
        array('inline'=>false)
    );
}