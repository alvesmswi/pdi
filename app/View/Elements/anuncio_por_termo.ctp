<?php 
//pega o slug do termo do node
$termSlug = $this->Mswi->categorySlug($node['Node']['terms']);
//separa as opções de cada linha informada no bloco
$paramsArray = explode(PHP_EOL, $block['Block']['params']);
//se encontrou alguma configuração valida
if(!empty($paramsArray)){
    //percorre cada uma delas
    foreach($paramsArray as $param){
        //separo os valores
        $arrayValues = explode(':', $param);
        //se foi informado term
        if($arrayValues[0] == 'term'){
            $term = $arrayValues[0];
            $termValue = trim($arrayValues[1]);
        }
        //se foi informado term
        if($arrayValues[0] == 'dfpid'){
            $dfpid = $arrayValues[0];
            $dfpidValue = trim($arrayValues[1]);
        }
    }
}
//se foi informado o termo
if(isset($term)){
    //verifica se o node que está sendo exibido é o mesmo informado no bloco
    if($termSlug == $termValue){
        $exibir = true;
    }
}

//se é para exibir
if(isset($exibir)){
?>
        <div id='<?php echo $dfpidValue; ?>' style="width: 300px; height: 250px;">
            <script>
                googletag.cmd.push(function() { googletag.display('<?php echo $dfpidValue; ?>'); });
            </script>
        </div>
<?php } ?>