<?php
App::uses('CroogoAppController', 'Croogo.Controller');
/**
 * Base Application Controller
 *
 * @package  Croogo
 * @link     http://www.croogo.org
 */

class AppController extends CroogoAppController {
	function afterFilter() {	
		$this->_verificaAdmin();
	    if (
				$this->request->params['controller'] != "redirect"  && 
				$this->request->params['controller'] != "Multiattach"  && 
				$this->response->statusCode() == '404'
			)
	    {
			//$this->loadModel('Redirect.Redirect');
			//Procura por slug
			//$pagina404 = $this->Redirect->localizarSlug($this->request->url);
			//redireciona
			//$this->redirect($pagina404);
	    }
	}

	function _verificaAdmin(){
		$logadoId = AuthComponent::user('id');
		//mas o subdominio não é MSWI
		$dominio 			= $this->_getDominio();
		$subDominio 		= $this->_getSubDominio();
		$subdominioNome  	= Configure::read('Site.admin_subdominio');

		//se está acessando o admin
		if($this->params['prefix'] == 'admin'){				
			if(
				!empty($subdominioNome) && 
				$subDominio != 'jornal' && 
				$subDominio != $subdominioNome &&
				$dominio != 'pg1'
			){
				//Se tentar acessar direto o admin, redireciona para a Home
				return $this->redirect('/');
			}
		}else{
			//se está acessando matérias com mswi na frente
			if($this->action == 'view' && $subDominio == $subdominioNome){
				//remove o dominio com o mswi
				$dominioSemMswi = str_replace($subdominioNome.'.' ,'', $_SERVER['HTTP_HOST']);
				$urlRedirect = 'http://'.$dominioSemMswi.$this->here;
				return $this->redirect($urlRedirect);
			}
		}

	}

	function _getSubDominio(){
		$url = $_SERVER['HTTP_HOST'];
		$arrayUrl = explode('.', $url);
		return $arrayUrl[0];
	}

	function _getDominio(){
		$url = $_SERVER['HTTP_HOST'];
		$arrayUrl = explode('.', $url);
		return $arrayUrl[1];
	}
	
}

