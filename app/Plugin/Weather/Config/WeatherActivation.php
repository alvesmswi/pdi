<?php

/**
 * Weather Activation
 *
 * Activation class for Weather plugin.
 *
 * @package  Weather
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class WeatherActivation 
{
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}

	public function onActivation(Controller $controller) 
	{
        $controller->Setting->create();
        $controller->Setting->save(array(
			'key'           => 'Weather.weather_cidade',
			'value'         => '',
			'title'         => 'cidade',
			'description'   => '',
			'input_type'    => 'text',
			'editable'      => 1,
			'weight'        => 255,
			'params'        => ''
        ));
        
        $controller->Setting->create();
        $controller->Setting->save(array(
			'key'           => 'Weather.weather_uf',
			'value'         => '',
			'title'         => 'uf',
			'description'   => '',
			'input_type'    => 'text',
			'editable'      => 1,
			'weight'        => 2,
			'params'        => ''
		));
	}

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
        $controller->Setting->deleteKey('Weather');
	}
}