<?php

CroogoNav::add('sidebar', 'settings.children.weather', array(
	//'icon' => 'list',
	'title' => 'Previsão do Tempo',
	'url' => array(
		'admin' => true,
		'plugin' => 'Weather',
		'controller' => 'Weather',
		'action' => 'index'
	),
	'weight' => 40
));