<?php

CroogoRouter::connect('/admin/weather/settings', array(
	'plugin' => 'Weather', 'controller' => 'Weather', 'action' => 'index', 
	'admin' => true
));

CroogoRouter::connect('/weather', array(
	'plugin' => 'Weather', 'controller' => 'Weather', 'action' => 'weather'
));