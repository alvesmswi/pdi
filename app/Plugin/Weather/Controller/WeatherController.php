<?php
App::uses('WeatherAppController', 'Weather.Controller');
App::uses('HttpSocket', 'Network/Http');

/**
 * Weather Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class WeatherController extends WeatherAppController 
{
	public $name = 'Weather';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

		$this->Security->unlockedActions += array('admin_index');
		$this->Auth->allow(array('weather'));

		$this->loadModel('Settings.Setting');
		
        //$this->Security->validatePost = false;
	}

	/**
	 * admin_index
	 * Settings index
	 */
	public function admin_index() 
	{
		$this->set('title_for_layout', __d('croogo', 'Configurações Previsão do Tempo'));

		if (!empty($this->request->data)) {
			if ($this->Setting->deleteAll(array('Setting.key LIKE' => 'Weather.%'))) {
				foreach ($this->request->data['Setting'] as $key => $value) {
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 1;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Weather.'.$key;
                    $arraySave['value']         = strtolower($value);
					$arraySave['title']         = $key;
					$arraySave['description']   = '';
                    $arraySave['params']   		= '';
					
                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
			}
		}

		$arrayDados = $this->Setting->find('all', array(
			'conditions' => array('key LIKE' => 'Weather.%'), 
			'recursive' => -1
		));

		if (!empty($arrayDados)) {
            foreach ($arrayDados as $dados) {
				$this->request->data['Setting'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
		}
	}

	/**
	 * weather
	 * public weather data 
	 */
	public function weather()
	{
		Cache::set(array('duration' => '+1 hour'));

		$cacheName = 'weather_plugin';

		$condition 	= null;
		$forecast 	= null;
		
		$cidade = (!empty(Configure::read('Weather.weather_cidade'))) ? trim(Configure::read('Weather.weather_cidade')) : 'curitiba';
		$uf 	= (!empty(Configure::read('Weather.weather_uf'))) ? trim(Configure::read('Weather.weather_uf')) : 'pr';

		$body = Cache::read($cacheName);
		
		if (!$body) {
			$HttpSocket = new HttpSocket();

			$results = $HttpSocket->get('https://query.yahooapis.com/v1/public/yql', array(
				'q' => 'select item from weather.forecast where woeid in (select woeid from geo.places(1) where text="'.$cidade.', '.$uf.'") and u="c"',
				'format' => 'json'
			));
			
			$body = json_decode($results->body);

			Cache::write($cacheName, $body);
		}
		
		if (isset($body->query->results->channel)) {
			$channel 	= $body->query->results->channel;
			$weather 	= $channel->item;

			$condition 	= $weather->condition;
			$forecast 	= $weather->forecast;
		}
		
		$this->set('condition', $condition);
		$this->set('forecast', $forecast);
		$this->set('cidade', Inflector::humanize($cidade));
	}
}
