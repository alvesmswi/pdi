<?php

$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb(__d('croogo', 'Settings'), array(
		'admin' => true,
		'plugin' => 'settings',
		'controller' => 'settings',
		'action' => 'index',
	))
  ->addCrumb('Previsão do Tempo', $this->here);

$this->append('form-start', $this->Form->create('Setting', array(
    'class' => 'protected-form'
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#settings-weather');
    echo $this->Croogo->adminTabs();
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('settings-weather');
       echo $this->Form->input('weather_cidade', array(
            'label'         => 'Cidade',
            'type'          => 'text',
            'placeholder'   => 'curitiba',
            'required'      => true
        )) .
        $this->Form->input('weather_uf', array(
            'label'         => 'UF',
            'type'          => 'text',
            'placeholder'   => 'pr',
            'required'      => true
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Ações') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
    $this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
