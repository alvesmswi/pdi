<?php
Croogo::hookRoutes('Classificados');

// Configure Wysiwyg
Croogo::mergeConfig('Wysiwyg.actions', array(
	'Classificados/admin_create_anuncios' => array(
		array(
			'elements' => 'AnuncioBody',
		),
	),
	'Classificados/admin_edit_anuncios' => array(
		array(
			'elements' => 'AnuncioBody',
		),
	)
));