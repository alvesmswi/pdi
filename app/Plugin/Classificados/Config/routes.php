<?php
CroogoRouter::connect('/admin/classificados', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'index', 
	'admin' => true
));
CroogoRouter::connect('/admin/classificados/categorias', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'index_categorias', 
	'admin' => true
));
CroogoRouter::connect('/admin/classificados/anuncios', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'index_anuncios', 
	'admin' => true
));
CroogoRouter::connect('/admin/classificados/contatos', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'index_contatos', 
	'admin' => true
));
CroogoRouter::connect('/admin/classificados/upload', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'upload', 
	'admin' => true
));

CroogoRouter::connect('/classificados', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'classificados'
));
CroogoRouter::connect('/classificados/search/*', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'search'
));
CroogoRouter::connect('/classificados/contato/*', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'contato'
));

CroogoRouter::connect('/classificados/:classificado/secao/:secao/anuncio/:slug', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view_anuncio'
));
CroogoRouter::connect('/classificados/:classificado/secao/:slug/*', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view'
));
CroogoRouter::connect('/classificados/:slug/*', array(
	'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view_classificado'
));