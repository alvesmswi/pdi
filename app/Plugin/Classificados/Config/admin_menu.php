<?php
CroogoNav::add('sidebar', 'classificados', array(
	'icon' => 'list',
	'title' => __d('croogo', 'Classificados'),
	'url' => array(
		'admin' => true,
		'plugin' => 'classificados',
		'controller' => 'classificados',
		'action' => 'index',
	),
	'weight' => 40,
	'children' => array(
		'list' => array(
			'title' => __d('croogo', 'List'),
			'url' => array(
				'admin' => true,
				'plugin' => 'classificados',
				'controller' => 'classificados',
				'action' => 'index'
			)
		),
		'create' => array(
			'title' => __d('croogo', 'Create'),
			'url' => array(
				'admin' => true,
				'plugin' => 'classificados',
				'controller' => 'classificados',
				'action' => 'create'
			)
		),
		'categorias' => array(
			'title' => __d('croogo', 'Categorias'),
			'url' => array(
				'admin' => true,
				'plugin' => 'classificados',
				'controller' => 'classificados',
				'action' => 'index_categorias'
			)
		),
		'anuncios' => array(
			'title' => __d('croogo', 'Anúncios'),
			'url' => array(
				'admin' => true,
				'plugin' => 'classificados',
				'controller' => 'classificados',
				'action' => 'index_anuncios'
			)
		),
		'contatos' => array(
			'title' => __d('croogo', 'Contatos'),
			'url' => array(
				'admin' => true,
				'plugin' => 'classificados',
				'controller' => 'classificados',
				'action' => 'index_contatos'
			)
		)
	)
));

CroogoNav::add('sidebar', 'classificados.children.categorias.children.list', array(
	'title' => __d('croogo', 'List'),
	'url' => array(
		'admin' => true,
		'plugin' => 'classificados',
		'controller' => 'classificados',
		'action' => 'index_categorias'
	)
));
CroogoNav::add('sidebar', 'classificados.children.categorias.children.create', array(
	'title' => __d('croogo', 'Create'),
	'url' => array(
		'admin' => true,
		'plugin' => 'classificados',
		'controller' => 'classificados',
		'action' => 'create_categorias'
	)
));

CroogoNav::add('sidebar', 'classificados.children.anuncios.children.list', array(
	'title' => __d('croogo', 'List'),
	'url' => array(
		'admin' => true,
		'plugin' => 'classificados',
		'controller' => 'classificados',
		'action' => 'index_anuncios'
	)
));
CroogoNav::add('sidebar', 'classificados.children.anuncios.children.create', array(
	'title' => __d('croogo', 'Create'),
	'url' => array(
		'admin' => true,
		'plugin' => 'classificados',
		'controller' => 'classificados',
		'action' => 'create_anuncios'
	)
));