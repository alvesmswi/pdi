<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Xml', 'Utility');
App::uses('Sanitize', 'Utility');
App::uses('Croogo', 'Lib');
App::uses('CakeEmail', 'Network/Email');

/**
 * Classificados Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class ClassificadosController extends AppController {
	public $name 			= 'Classificados';
	public $pluginPrefix 	= "Classificados";
	public $uses 			= array(
		'Classificados.Classificado', 'Classificados.Categoria', 'Classificados.Anuncio', 'Classificados.Contato'
	);
	public $components 		= array(
		'Croogo.BulkProcess',
		'Croogo.Recaptcha',
		'Search.Prg' => array(
			'presetForm' => array(
				'paramType' => 'querystring'
			),
			'commonProcess' => array(
				'paramType' => 'querystring',
				'filterEmpty' => true
			)
		)
	);
	public $defaults 		= array();

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

		$this->Security->unlockedActions += array('admin_create_anuncios', 'admin_edit_anuncios', 'contato', 'admin_toggle');
		$this->Auth->allow(array('classificados', 'view', 'view_anuncio', 'view_classificado', 'search', 'contato'));
	}

	/**
	 * Toggle Node status
	 *
	 * @param string $id Node id
	 * @param integer $status Current Node status
	 * @return void
	 */
	public function admin_toggle($id = null, $status = null, $model = null) 
	{
		$Model = (!empty($model)) ? $this->{$model} : $this->{$this->modelClass};
		$this->Croogo->fieldToggle($Model, $id, $status);
	}

	/**
	 * admin_index
	 * Classificado index
	 */
	public function admin_index() 
	{
		$this->set('title_for_layout', __d('croogo', 'Classificados'));

		$alias 				= 'Classificado';
		$arrayConditions 	= array();

		if (!empty($this->data)) {
			if (!empty($this->data[$alias]['filter'])) {
				$arrayConditions['AND']['Classificado.title LIKE'] = '%'.$this->data[$alias]['filter'].'%';
			}
			if ($this->data[$alias]['status'] != "" && ($this->data[$alias]['status'] == 0 || $this->data[$alias]['status'] == 1)) {
				$arrayConditions['AND']['Classificado.status'] = $this->data[$alias]['status'];
			}
		}

		$this->paginate[$alias]['order'] = 'Classificado.title ASC';
		$this->paginate[$alias]['conditions'] = $arrayConditions;

		$classificados = $this->paginate($alias);

		$this->set(compact('classificados'));
	}

	/**
	 * admin_create
	 * Classificado create
	 */
	public function admin_create() 
	{
		if (!empty($this->request->data)) {
			if ($this->Classificado->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Classificado']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Classificado->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Classificado']['title']), 'flash', array('class' => 'error'));
			}
		}

		$this->set('title_for_layout', __d('croogo', 'Create a blog'));
		$this->render('admin_form');
	}

	/**
	 * admin_edit
	 * Classificado edit
	 */
	public function admin_edit($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->Classificado->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Classificado']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Classificado']['title']), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$data = $this->Classificado->read(null, $id);
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}
			$this->request->data = $data;
		}

		$this->set('title_for_layout', __d('croogo', 'Edit classificado: %s', $this->request->data['Classificado']['title']));
		$this->render('admin_form');
	}

	/**
	 * admin_delete
	 * Classificado delete
	 */
	public function admin_delete($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->Classificado->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Classificado id: %s has been deleted', $id), 'flash', array('class' => 'success'));
			$this->Croogo->redirect(array('action' => 'index'));
		}

		$this->autoRender = false;
	}

	/**
	 * admin_index_categorias
	 * Categorias classificado index
	 */
	public function admin_index_categorias() 
	{
		$this->set('title_for_layout', __d('croogo', 'Categorias'));

		$alias 				= 'Categoria';
		$arrayConditions 	= array();

		if (!empty($this->data)) {
			if (!empty($this->data[$alias]['filter'])) {
				$arrayConditions['AND']['Categoria.title LIKE'] = '%'.$this->data[$alias]['filter'].'%';
			}
			if (!empty($this->data[$alias]['type'])) {
				$arrayConditions['AND']['Categoria.classificado_id'] = $this->data[$alias]['type'];
			}
			if ($this->data[$alias]['status'] != "" && ($this->data[$alias]['status'] == 0 || $this->data[$alias]['status'] == 1)) {
				$arrayConditions['AND']['Categoria.status'] = $this->data[$alias]['status'];
			}
		}

		$this->paginate[$alias]['order'] = 'Categoria.title ASC';
		$this->paginate[$alias]['conditions'] = $arrayConditions;

		$categorias = $this->paginate($alias);

		$catLists = $this->Classificado->find('list');

		$this->set(compact('categorias', 'catLists'));
	}

	/**
	 * admin_create_categorias
	 * Categorias classificado create
	 */
	public function admin_create_categorias() 
	{
		$classificados = $this->Classificado->find('list');

		if (!empty($this->request->data)) {
			if ($this->Categoria->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Categoria']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit_categorias', $this->Categoria->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Categoria']['title']), 'flash', array('class' => 'error'));
			}
		}

		$this->set(compact('classificados'));
		$this->set('title_for_layout', __d('croogo', 'Create categoria'));
		$this->render('admin_form_categorias');
	}

	/**
	 * admin_edit_categorias
	 * Categorias classificado edit
	 */
	public function admin_edit_categorias($id = null) 
	{
		$classificados = $this->Classificado->find('list');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->Categoria->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Categoria']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit_post', $id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Categoria']['title']), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$data = $this->Categoria->read(null, $id);
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}
			$this->request->data = $data;
		}

		$this->set(compact('classificados'));
		$this->set('title_for_layout', __d('croogo', 'Edit categoria: %s', $this->request->data['Categoria']['title']));
		$this->render('admin_form_categorias');
	}

	/**
	 * admin_delete_categorias
	 * Categorias classificado delete
	 */
	public function admin_delete_categorias($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->Categoria->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Categoria id: %s has been deleted', $id), 'flash', array('class' => 'success'));
			$this->Croogo->redirect(array('action' => 'index'));
		}

		$this->autoRender = false;
	}

	/**
	 * admin_index_anuncios
	 * Anúncios classificado index
	 */
	public function admin_index_anuncios() 
	{
		$this->set('title_for_layout', __d('croogo', 'Anúncios'));

		$alias 				= 'Anuncio';
		$arrayConditions 	= array();

		if (!empty($this->data)) {
			if (!empty($this->data[$alias]['filter'])) {
				$arrayConditions['AND']['Anuncio.title LIKE'] = '%'.$this->data[$alias]['filter'].'%';
			}
			if (!empty($this->data[$alias]['type'])) {
				$arrayConditions['AND']['Anuncio.categoria_id'] = $this->data[$alias]['type'];
			}
			if ($this->data[$alias]['status'] != "" && ($this->data[$alias]['status'] == 0 || $this->data[$alias]['status'] == 1)) {
				$arrayConditions['AND']['Anuncio.status'] = $this->data[$alias]['status'];
			}
		}

		$this->paginate[$alias]['order'] = 'Anuncio.id DESC';
		$this->paginate[$alias]['recursive'] = 2;
		$this->paginate[$alias]['conditions'] = $arrayConditions;

		$anuncios = $this->paginate($alias);

		$anuCats = $this->Categoria->find('list');

		$this->set(compact('anuncios', 'anuCats'));
	}

	/**
	 * admin_create_anuncios
	 * Anúncios classificado create
	 */
	public function admin_create_anuncios() 
	{
		$categorias 		= array();
		$categorias_find 	= $this->Categoria->find('all', array(
			'conditions' => array(
				'Categoria.status' => 1
			)
		));
		foreach ($categorias_find as $key => $cat) {
			$categorias[$cat['Classificado']['title']][$cat['Categoria']['id']] = $cat['Categoria']['title'];
		}

		if (!empty($this->request->data)) {
			if ($this->Anuncio->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Anuncio']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit_anuncios', $this->Anuncio->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Anuncio']['title']), 'flash', array('class' => 'error'));
			}
		}

		$this->set(compact('categorias'));
		$this->set('title_for_layout', __d('croogo', 'Create anúncio'));
		$this->render('admin_form_anuncios');
	}

	/**
	 * admin_edit_anuncios
	 * Anúncios classificado edit
	 */
	public function admin_edit_anuncios($id = null) 
	{
		$categorias 		= array();
		$categorias_find 	= $this->Categoria->find('all', array(
			'conditions' => array(
				'Categoria.status' => 1
			)
		));
		foreach ($categorias_find as $key => $cat) {
			$categorias[$cat['Classificado']['title']][$cat['Categoria']['id']] = $cat['Categoria']['title'];
		}

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->Anuncio->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', $this->request->data['Anuncio']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'admin_edit_anuncios', $id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.',$this->request->data['Anuncio']['title']), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$data = $this->Anuncio->read(null, $id);
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}
			$this->request->data = $data;
		}

		$this->set(compact('categorias'));
		$this->set('title_for_layout', __d('croogo', 'Edit anúncio: %s', $this->request->data['Anuncio']['title']));
		$this->render('admin_form_anuncios');
	}

	/**
	 * admin_delete_anuncios
	 * Anúncios classificado delete
	 */
	public function admin_delete_anuncios($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->Anuncio->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Anúncio id: %s has been deleted', $id), 'flash', array('class' => 'success'));
			$this->Croogo->redirect(array('action' => 'index'));
		}

		$this->autoRender = false;
	}

	/**
	 * admin_index_contatos
	 * Contatos classificado index
	 */
	public function admin_index_contatos() 
	{
		$this->set('title_for_layout', __d('croogo', 'Contatos'));

		$contatos = $this->Contato->find('all', array(
			'contain' => array(
				'Anuncio' => array(
					'Categoria' => array(
						'Classificado' => array(
							'fields' => array('id', 'slug')
						),
						'fields' => array('id', 'slug')
					),
					'fields' => array('id', 'slug', 'title')
				)
			)
		));

		$this->set(compact('contatos'));
	}

	/**
	 * admin_index_contatos
	 * Contatos classificado index
	 */
	public function admin_upload() 
	{
		pr($this->request->data);exit;
	}

	/**
	 * classificados
	 * public classificados
	 */
	public function classificados() 
	{
		$this->set('title_for_layout', 'Classificados');

		$classificados = $this->Classificado->find('all', array(
			'conditions' => array('Classificado.status' => 1),
			'contain' => array(
				'Categoria',
			)
		));

		foreach ($classificados as $key => $c) {
			$c_id = $c['Classificado']['id'];
			$classificados[$key]['Anuncio'] = $this->Anuncio->find('all', array(
				'conditions' => array(
					'Categoria.classificado_id' => $c_id,
					'Anuncio.status' => true,
					'Anuncio.publish_start <=' => date('Y-m-d H:i:s'),
					'OR' => array(
						'Anuncio.publish_end >' => date('Y-m-d H:i:s'),
						'Anuncio.publish_end' => null,
					)
				),
				'fields' => array('id', 'title', 'slug', 'categoria_id', 'preco'),
				'contain' => array(
					'Categoria' => array(
						'Classificado' => array(
							'fields' => array('id', 'slug')
						),
						'fields' => array('id', 'title', 'slug')
					),
					'Images'
				),
				'limit' => 4,
				'order' => array('Anuncio.publish_start' => 'desc')
			));
		}
		
		$this->set(compact('classificados'));
	}

	/**
	 * view seção
	 * public view seção
	 */
	public function view() 
	{
		if (!isset($this->request->params['slug'])) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$secao 			= $this->request->params['slug'];
		$secao_find 	= $this->Categoria->findBySlug($this->request->params['slug'], ['id', 'title', 'classificado_id']);
		$this->Classificado->recursive = -1;
		$classificado 	= $this->Classificado->findById($secao_find['Categoria']['classificado_id'], ['id', 'title', 'icone']);
		$total_anuncios = $this->Anuncio->find('count', array('conditions' => array('Anuncio.status' => 1, 'Categoria.slug' => $secao)));

		$this->paginate = array(
			'conditions' 	=> array(
				'Categoria.slug' => $secao,
				'Anuncio.status' => true,
				'Anuncio.publish_start <=' => date('Y-m-d H:i:s'),
				'OR' => array(
					'Anuncio.publish_end >' => date('Y-m-d H:i:s'),
					'Anuncio.publish_end' => null,
				)
			),
			'recursive' 	=> 2,
			'limit' 		=> 12,
			'order' 		=> array('Anuncio.publish_start' => 'desc')
		);

		$anuncios = $this->paginate('Anuncio');
		if (!$anuncios) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$this->set(compact('anuncios', 'total_anuncios', 'classificado', 'secao'));
		$this->set('title_for_layout', $anuncios[0]['Categoria']['title']); // arumamr aqui para poder tirar o return de cima
		$this->render('secao');
		$this->Croogo->viewFallback(array(
			'secao_' . $secao,
		));
	}

	/**
	 * view_anuncio
	 * public view_anuncio
	 */
	public function view_anuncio() 
	{
		if (!isset($this->request->params['slug'])) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$slug = $this->request->params['slug'];
		$this->Anuncio->recursive = 2;
		$anuncio = $this->Anuncio->findBySlug($slug);
		if (!$anuncio) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$this->set(compact('anuncio'));
		$this->set('title_for_layout', $anuncio['Categoria']['title']. ' :: ' .$anuncio['Anuncio']['title']);
		$this->render('view');
		$this->Croogo->viewFallback(array(
			'view_' . $anuncio['Categoria']['slug'],
		));
	}

	/**
	 * view_classificado
	 * public view classificado
	 */
	public function view_classificado() 
	{
		if (!isset($this->request->params['slug'])) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$classificado = $this->Classificado->findBySlug($this->request->params['slug'], ['id', 'title', 'icone']);
		$c_id = $classificado['Classificado']['id'];
		$total_anuncios = $this->Anuncio->find('count', array('conditions' => array('Anuncio.status' => 1, 'Categoria.classificado_id' => $c_id)));

		$this->paginate = array(
			'conditions' 	=> array(
				'Categoria.classificado_id' => $c_id,
				'Anuncio.status' => true,
				'Anuncio.publish_start <=' => date('Y-m-d H:i:s'),
				'OR' => array(
					'Anuncio.publish_end >' => date('Y-m-d H:i:s'),
					'Anuncio.publish_end' => null,
				)
			),
			'recursive' 	=> 2,
			'limit' 		=> 12,
			'order' 		=> array('Anuncio.publish_start' => 'desc')
		);

		$anuncios = $this->paginate('Anuncio');
		if (!$anuncios) {
			return $this->redirect(array('action' => 'classificados'));
		}

		$this->set(compact('anuncios', 'total_anuncios', 'classificado'));
		$this->set('title_for_layout', $classificado['Classificado']['title']);
		$this->render('index');
		$this->Croogo->viewFallback(array(
			'index_' . $this->request->params['slug'],
		));
	}

	/**
	 * search
	 * public search
	 */
	public function search() 
	{
		$search 		= (isset($this->request->query['q'])) ? $this->request->query['q'] : null;

		$conditions['Anuncio.status'] 				= true;
		$conditions['Anuncio.publish_start <='] 	= date('Y-m-d H:i:s');
		$conditions['OR']['Anuncio.publish_end >'] 	= date('Y-m-d H:i:s');
		$conditions['OR']['Anuncio.publish_end'] 	= null;

		$conditions['Anuncio.title LIKE'] 	= "%{$search}%";
		
		if (isset($this->request->query['categoria'])) {
			$conditions['Categoria.slug'] = $this->request->query['categoria'];
		}

		if (isset($this->request->params['pass'][0])) {
			$classificado = $this->Classificado->findBySlug($this->request->params['pass'][0], ['id']);
			$c_id = $classificado['Classificado']['id'];
			$conditions['Categoria.classificado_id'] = $c_id;
		}

		$total_anuncios = $this->Anuncio->find('count', array('conditions' => $conditions));

		$this->paginate = array(
			'conditions' 	=> $conditions,
			'recursive' 	=> 2,
			'limit' 		=> 12,
			'order' 		=> array('Anuncio.publish_start' => 'desc')
		);

		$anuncios = $this->paginate('Anuncio');

		$this->set(compact('anuncios', 'total_anuncios', 'search'));
		$this->set('title_for_layout', $search);
		$this->render('search');
	}

	/**
	 * contato
	 * public contato
	 */
	public function contato() 
	{
		$this->autoRender = false;

		if (!empty($this->request->data)) {
			if ($this->Contato->save($this->request->data)) {
				// Enviar para o Email do Anunciante, se tiver
				$anuncio = $this->Anuncio->find('first', array(
					'conditions' => array(
						'Anuncio.id' => $this->request->data['node_id']
					),
					'contain' => array(
						'Categoria' => array(
							'Classificado'
						)
					)
				));
				if (!empty($anuncio) && !empty($anuncio['Anuncio']['anunciante_email'])) {
					// Enviar email
					$email = new CakeEmail();
					$siteTitle = Configure::read('Site.title');
					try {
						$email->from($this->request->data['email'])
							->to($anuncio['Anuncio']['anunciante_email'])
							->subject(__d('croogo', '[%s] - Anúncio: %s', $siteTitle, $anuncio['Anuncio']['title']))
							->template('Classificados.anuncio')
							//->emailFormat('html')
							->viewVars(array(
								'anuncio' => $anuncio,
								'message' => $this->request->data,
							));
						if ($this->theme) {
							$email->theme($this->theme);
						}

						if (!$email->send()) {
							$this->log('Erro ao enviar mensagem para o Anunciante.');
						}
					} catch (SocketException $e) {
						$this->log(sprintf('Erro ao enviar mensagem para o Anunciante: %s', $e->getMessage()));
					}
				}

				$this->Session->setFlash(__d('croogo', 'Mensagem enviada com sucesso!'), 'flash', array('class' => 'alert alert-success'));
				$this->Croogo->redirect($this->referer());
			} else {
				$this->Session->setFlash(__d('croogo', 'Contato could not be saved. Please, try again.'), 'flash', array('class' => 'error'));
			}
		}
	}
}