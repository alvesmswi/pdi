<div class="c-classificados_banner">
  <h1 class="c-classificados_banner-title">
    <?php if (Configure::read('Classificados.faleconosco')): ?>
      <a href="<?php echo Configure::read('Classificados.faleconosco'); ?>">
        <?php echo (Configure::read('Classificados.titulo')) ? Configure::read('Classificados.titulo') : 'Faça seu anúncio grátis'; ?>
      </a>
    <?php else: ?>
      <?php echo (Configure::read('Classificados.titulo')) ? Configure::read('Classificados.titulo') : 'Faça seu anúncio grátis'; ?>
    <?php endif; ?>
  </h1>
  <div class="c-classificados_search">
    <!-- Nav tabs -->
    <ul class="c-classificados_tabs" role="tablist">
      <?php foreach ($classificados as $key => $c) { ?>
      <?php $class = ($key == 0) ? 'active' : null; ?>
      <li role="presentation" class="c-classificados_tab <?php echo $class ?>">
        <a href="#busca-<?php echo $c['Classificado']['slug'] ?>" aria-controls="busca-<?php echo $c['Classificado']['slug'] ?>" role="tab" data-toggle="tab">
          <i class="i-icon i-icon_<?php echo $c['Classificado']['icone'] ?>"></i> 
          <span class="hidden-xs"><?php echo $c['Classificado']['title'] ?></span>
        </a>
      </li>
      <?php } ?>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content c-classificados_panes">
      <?php foreach ($classificados as $key => $c) { ?>
        <?php $class = ($key == 0) ? 'active' : null; ?>
        <div role="tabpanel" class="c-classificados_pane tab-pane <?php echo $class ?>" id="busca-<?php echo $c['Classificado']['slug'] ?>">
          <form action="/classificados/search/<?php echo $c['Classificado']['slug'] ?>" method="get" class="c-classificados_form">
            <?php if (!empty($c['Categoria'])) { ?>
              <div class="c-classificados_group c-classificados_select-group">
                <select class="c-classificados_select selectpicker" name="categoria">
                  <?php foreach ($c['Categoria'] as $key => $cat) { ?>
                    <option value="<?php echo $cat['slug'] ?>"><?php echo $cat['title'] ?></option>
                  <?php } ?>
                </select>
              </div>
            <?php } ?>
            <div class="c-classificados_group c-classificados_input-group">
              <label for="input-<?php echo $c['Classificado']['slug'] ?>" class="sr-only"><?php echo $c['Classificado']['title'] ?></label>
              <input type="search" class="c-classificados_input" name="q" placeholder="Digite seu termo de busca" id="input-<?php echo $c['Classificado']['slug'] ?>">
            </div>
            <div class="c-classificados_group c-classificados_submit-group">
              <button type="submit" class="c-classificados_btn"><i class="i-icon i-icon_search"></i><span class="hidden-xs">Pesquisar</span></button>
            </div>
          </form>
        </div>
      <?php } ?>
    </div>
  </div>
</div>