<div class="clearfix filter">
<?php
	echo $this->Form->create('Anuncio', array(
		'class' => 'form-inline',
		'url' => '/admin/classificados/anuncios',
		'inputDefaults' => array(
			'label' => false,
		),
	));

	echo $this->Form->input('chooser', array(
		'type' => 'hidden',
		'value' => isset($this->request->query['chooser']),
	));

	echo $this->Form->input('filter', array(
		'title' => __d('croogo', 'Search'),
		'placeholder' => __d('croogo', 'Search...'),
		'tooltip' => false,
	));

	if (!isset($this->request->query['chooser'])):

		echo $this->Form->input('type', array(
			'options' => $anuCats,
			'empty' => __d('croogo', 'Categoria'),
		));

		echo $this->Form->input('status', array(
			'options' => array(
				'1' => __d('croogo', 'Published'),
				'0' => __d('croogo', 'Unpublished'),
			),
			'empty' => __d('croogo', 'Status'),
		));

	endif;

	echo $this->Form->input(__d('croogo', 'Filter'), array(
		'type' => 'submit',
	));
	echo $this->Form->end();
?>
</div>
