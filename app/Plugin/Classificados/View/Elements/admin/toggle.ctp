<?php
if (isset($model) && !empty($model)) {
	$url = array(
		'admin' => isset($admin) ? $admin : true,
		'plugin' => isset($plugin) ? $plugin : $this->request->params['plugin'],
		'controller' => isset($controller) ? $controller : $this->request->params['controller'],
		'action' => isset($action) ? $action : 'toggle',
		$id,
		$status,
		$model
	);
} else {
	$url = array(
		'admin' => isset($admin) ? $admin : true,
		'plugin' => isset($plugin) ? $plugin : $this->request->params['plugin'],
		'controller' => isset($controller) ? $controller : $this->request->params['controller'],
		'action' => isset($action) ? $action : 'toggle',
		$id,
		$status,
	);
}

echo $this->Html->status($status, $url);