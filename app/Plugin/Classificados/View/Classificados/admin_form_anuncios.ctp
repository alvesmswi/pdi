<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Classificados.admin');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Anúncio'), array('controller' => 'classificados', 'action' => 'index_anuncios'));

if ($this->request->params['action'] == 'admin_create_anuncios') {
	$formUrl = array('controller' => 'classificados', 'action' => 'create_anuncios');
	$this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'classificados', 'action' => 'create_anuncios'));
	$publish_start = date('Y-m-d H:i:s');
}

if ($this->request->params['action'] == 'admin_edit_anuncios') {
	$formUrl = array('controller' => 'classificados', 'action' => 'edit_anuncios');
	$this->Html->addCrumb($this->request->data['Anuncio']['title'], '/' . $this->request->url);
	$publish_start = $this->request->data['Anuncio']['publish_start'];
}

$lookupUrl = $this->Html->apiUrl(array(
	'plugin' => 'users',
	'controller' => 'users',
	'action' => 'lookup',
));

$this->append('form-start', $this->Form->create('Anuncio', array(
	'url' => $formUrl,
	'class' => 'protected-form',
	'type' => 'file'
)));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Anúncio'), '#node-main');
	echo $this->Croogo->adminTab(__d('croogo', 'Detalhes'), '#node-detalhes');
	echo $this->Croogo->adminTab(__d('croogo', 'Categoria'), '#node-categoria');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	echo $this->Html->tabStart('node-main') .
		$this->Form->input('id') .
		$this->Form->input('title', array(
			'label' => __d('croogo', 'Title'),
		)) .
		$this->Form->input('slug', array(
			'label' => __d('croogo', 'Slug'),
		)) .
		$this->Form->input('excerpt', array(
			'label' => __d('croogo', 'Excerpt'),
			'class' => $inputClass,
		)) .
		$this->Form->input('body', array(
			'label' => __d('croogo', 'Body'),
			'class' => $inputClass,
		)) .
	$this->Html->tabEnd();

	echo $this->Html->tabStart('node-detalhes') .
		$this->Form->input('id') .
		$this->Form->input('preco', array(
			'label' => __d('croogo', 'Preço'),
			'type' 	=> 'text'
		)) .
		$this->Form->input('anunciante_nome', array(
			'label' => __d('croogo', 'Anunciante')
		)) .
		$this->Form->input('anunciante_telefone', array(
			'label' => __d('croogo', 'Telefone')
		)) .
		$this->Form->input('anunciante_email', array(
			'label' => __d('croogo', 'E-mail')
		)) .
	$this->Html->tabEnd();

	echo $this->Html->tabStart('node-categoria') .
		$this->Form->input('id') .
		$this->Form->input('categoria_id', array(
			'label' => __d('croogo', 'Categoria'),
		)) .
	$this->Html->tabEnd();

	echo $this->Croogo->adminTabs();
$this->end();

$username = isset($this->request->data['User']['username']) ?
	$this->request->data['User']['username'] :
	$this->Session->read('Auth.User.username');

$this->start('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
		$this->Form->input('status', array(
			'type' => 'checkbox',
			'label' => __d('croogo', 'Active'),
		)) .
		$this->Form->autocomplete('user_id', array(
			'type' => 'text',
			'label' => __d('croogo', 'Publish as '),
			'autocomplete' => array(
				'default' => $username,
				'data-displayField' => 'username',
				'data-primaryKey' => 'id',
				'data-queryField' => 'name',
				'data-relatedElement' => '#NodeUserId',
				'data-url' => $lookupUrl,
			),
		)) .
		$this->Html->div('input-daterange',
			$this->Form->input('publish_start', array(
				'label' => __d('croogo', 'Publish Start'),
				'type' 	=> 'text',
				'class' => 'input-datetime',
				'value' => $publish_start
			)) .
			$this->Form->input('publish_end', array(
				'label' => __d('croogo', 'Publish End'),
				'type' => 'text',
				'class' => 'input-datetime',
			))
		);

		//if ($this->request->params['action'] == 'admin_create_anuncios') { ?>
			<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
			<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.input-datetime').datetimepicker({
						dateFormat: 'yy-mm-dd', 
						timeFormat: 'hh:mm:ss'
					})
				;});
			</script>
	<?  //}

	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());