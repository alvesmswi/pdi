<?php
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Anúncios'), '/' . $this->request->url);

$this->append('actions');
	echo $this->Croogo->adminAction(
		__d('croogo', 'Create anúncio'),
		array('action' => 'create_anuncios'),
		array('button' => 'success')
	);
$this->end();

$this->append('search', $this->element('admin/anuncios_search'));

$this->append('form-start', $this->Form->create(
	'Anuncio',
	array(
		'url' => array('controller' => 'classificados', 'action' => 'process'),
		'class' => 'form-inline'
	)
));

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('title', __d('croogo', 'Title')),
		$this->Paginator->sort('slug', __d('croogo', 'Slug')),
		$this->Paginator->sort('slug', __d('croogo', 'Categoria')),
		$this->Paginator->sort('status', __d('croogo', 'Status')),
		''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php foreach ($anuncios as $anuncio): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('Anuncio.' . $anuncio['Anuncio']['id'] . '.id', array('class' => 'row-select')); ?></td>
		<td><?php echo $anuncio['Anuncio']['id']; ?></td>
		<td>
			<?php
				echo $this->Html->link($anuncio['Anuncio']['title'], array(
					'admin' => false,
					'controller' => 'classificados',
					'action' => 'view_anuncio',
					'classificado' => $anuncio['Categoria']['Classificado']['slug'],
					'secao' => $anuncio['Categoria']['slug'],
					'slug' => $anuncio['Anuncio']['slug']
				));
			?>
		</td>
		<td><?php echo $anuncio['Anuncio']['slug']; ?></td>
		<td><?php echo $anuncio['Categoria']['slug']; ?></td>
		<td>
			<?php
				echo $this->element('admin/toggle', array(
					'id' => $anuncio['Anuncio']['id'],
					'status' => (int)$anuncio['Anuncio']['status'],
					'model' => 'Anuncio'
				));
			?>
		</td>
		<td>
			<div class="item-actions">
			<?php
				echo $this->Croogo->adminRowActions($anuncio['Anuncio']['id']);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'edit_anuncios', $anuncio['Anuncio']['id']),
					array('icon' => $this->Theme->getIcon('update'), 'tooltip' => __d('croogo', 'Edit this item'))
				);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'delete_anuncios', $anuncio['Anuncio']['id']),
					array('icon' => $this->Theme->getIcon('delete'), 'tooltip' => __d('croogo', 'Delete this item'))
				);
			?>
			</div>
		</td>
	</tr>
<?php endforeach ?>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('Anuncio.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'publish' => __d('croogo', 'Publish'),
			'unpublish' => __d('croogo', 'Unpublish'),
			'delete' => __d('croogo', 'Delete'),
			'copy' => array(
				'value' => 'copy',
				'name' => __d('croogo', 'Copy'),
				'hidden' => true,
			),
		),
		'empty' => true,
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'button',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('Anuncio.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));

$this->end();

$this->append('form-end', $this->Form->end());