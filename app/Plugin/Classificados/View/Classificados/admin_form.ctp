<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Classificados.admin');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Classificados'), array('controller' => 'classificados', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_create') {
	$formUrl = array('action' => 'create');
	$this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'classificados', 'action' => 'create'));
}

if ($this->request->params['action'] == 'admin_edit') {
	$formUrl = array('action' => 'edit');
	$this->Html->addCrumb($this->request->data['Classificado']['title'], '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Classificado', array(
	'url' => $formUrl,
	'class' => 'protected-form',
	'type' => 'file'
)));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Classificado'), '#node-main');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	echo $this->Html->tabStart('node-main') .
		$this->Form->input('id') .
		$this->Form->input('title', array(
			'label' => __d('croogo', 'Title'),
		)) .
		$this->Form->input('slug', array(
			'label' => __d('croogo', 'Slug'),
		)) .
		$this->Form->input('icone', array(
			'label' => __d('croogo', 'Icone'),
		)) .
	$this->Html->tabEnd();

	echo $this->Croogo->adminTabs();
$this->end();

$username = isset($this->request->data['User']['username']) ?
	$this->request->data['User']['username'] :
	$this->Session->read('Auth.User.username');

$this->start('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
		$this->Form->input('status', array(
			'type' => 'checkbox',
			'label' => __d('croogo', 'Active'),
		));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());