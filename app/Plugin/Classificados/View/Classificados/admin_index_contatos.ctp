<?php
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Contatos'), '/' . $this->request->url);

$this->append('actions');
	echo $this->Croogo->adminAction(
		__d('croogo', 'Classificados'),
		array('action' => '#'),
		array('button' => 'success')
	);
$this->end();

$this->append('form-start', $this->Form->create(
	'Contato',
	array(
		'url' => array('controller' => 'classificados', 'action' => 'process'),
		'class' => 'form-inline'
	)
));

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('title', __d('croogo', 'Anúncio')),
		$this->Paginator->sort('slug', __d('croogo', 'Nome')),
		$this->Paginator->sort('slug', __d('croogo', 'E-mail')),
		$this->Paginator->sort('slug', __d('croogo', 'Telefone')),
		$this->Paginator->sort('slug', __d('croogo', 'Mensagem')),
		//$this->Paginator->sort('status', __d('croogo', 'Enviado')),
		//''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php foreach ($contatos as $contato): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('Contato.' . $contato['Contato']['id'] . '.id', array('class' => 'row-select')); ?></td>
		<td><?php echo $contato['Contato']['id']; ?></td>
		<td>
			<?php
				echo $this->Html->link($contato['Anuncio']['title'], array(
					'admin' => false,
					'controller' => 'classificados',
					'action' => 'view_anuncio',
					'classificado' => $contato['Anuncio']['Categoria']['Classificado']['slug'],
					'secao' => $contato['Anuncio']['Categoria']['slug'],
					'slug' => $contato['Anuncio']['slug']
				));
			?>
		</td>
		<td><?php echo $contato['Contato']['nome']; ?></td>
		<td><?php echo $contato['Contato']['email']; ?></td>
		<td><?php echo $contato['Contato']['telefone']; ?></td>
		<td><?php echo $contato['Contato']['mensagem']; ?></td>
		<!-- <td>
			<?php
				/*$this->element('admin/toggle', array(
					'id' => $contato['Contato']['id'],
					'status' => (int)$contato['Contato']['enviado'],
				));*/
			?>
		</td> -->
		<!-- <td>
			<div class="item-actions">
			<?php
				$this->Croogo->adminRowActions($contato['Contato']['id']);
				' ' . $this->Croogo->adminRowAction('',
					array('action' => 'edit_categorias', $contato['Contato']['id']),
					array('icon' => $this->Theme->getIcon('update'), 'tooltip' => __d('croogo', 'Edit this item'))
				);
			?>
			</div>
		</td> -->
	</tr>
<?php endforeach ?>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('Contato.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'publish' => __d('croogo', 'Publish'),
			'unpublish' => __d('croogo', 'Unpublish'),
			'delete' => __d('croogo', 'Delete'),
			'copy' => array(
				'value' => 'copy',
				'name' => __d('croogo', 'Copy'),
				'hidden' => true,
			),
		),
		'empty' => true,
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'button',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('Contato.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));

$this->end();

$this->append('form-end', $this->Form->end());