<?php
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Categorias'), '/' . $this->request->url);

$this->append('actions');
	echo $this->Croogo->adminAction(
		__d('croogo', 'Create a categoria'),
		array('action' => 'create_categorias'),
		array('button' => 'success')
	);
$this->end();

$this->append('search', $this->element('admin/categorias_search'));

$this->append('form-start', $this->Form->create(
	'Classificado',
	array(
		'url' => array('controller' => 'classificados', 'action' => 'process'),
		'class' => 'form-inline'
	)
));

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('title', __d('croogo', 'Title')),
		$this->Paginator->sort('slug', __d('croogo', 'Slug')),
		$this->Paginator->sort('slug', __d('croogo', 'Classificado')),
		$this->Paginator->sort('status', __d('croogo', 'Status')),
		''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php foreach ($categorias as $categoria): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('Categoria.' . $categoria['Categoria']['id'] . '.id', array('class' => 'row-select')); ?></td>
		<td><?php echo $categoria['Categoria']['id']; ?></td>
		<td>
			<?php
				echo $this->Html->link($categoria['Categoria']['title'], array(
					'admin' => false,
					'controller' => 'classificados',
					'action' => 'view',
					'classificado' => $categoria['Classificado']['slug'],
					'slug' => $categoria['Categoria']['slug']
				));
			?>
		</td>
		<td><?php echo $categoria['Categoria']['slug']; ?></td>
		<td><?php echo $categoria['Classificado']['slug']; ?></td>
		<td>
			<?php
				echo $this->element('admin/toggle', array(
					'id' => $categoria['Categoria']['id'],
					'status' => (int)$categoria['Categoria']['status'],
					'model' => 'Categoria'
				));
			?>
		</td>
		<td>
			<div class="item-actions">
			<?php
				echo $this->Croogo->adminRowActions($categoria['Categoria']['id']);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'edit_categorias', $categoria['Categoria']['id']),
					array('icon' => $this->Theme->getIcon('update'), 'tooltip' => __d('croogo', 'Edit this item'))
				);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'delete_categorias', $categoria['Categoria']['id']),
					array('icon' => $this->Theme->getIcon('delete'), 'tooltip' => __d('croogo', 'Delete this item'))
				);
			?>
			</div>
		</td>
	</tr>
<?php endforeach ?>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('Post.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'publish' => __d('croogo', 'Publish'),
			'unpublish' => __d('croogo', 'Unpublish'),
			'delete' => __d('croogo', 'Delete'),
			'copy' => array(
				'value' => 'copy',
				'name' => __d('croogo', 'Copy'),
				'hidden' => true,
			),
		),
		'empty' => true,
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'button',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('Post.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));

$this->end();

$this->append('form-end', $this->Form->end());