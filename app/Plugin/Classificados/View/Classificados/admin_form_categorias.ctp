<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Classificados.admin');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Categorias'), array('controller' => 'classificados', 'action' => 'index_categorias'));

if ($this->request->params['action'] == 'admin_create_categorias') {
	$formUrl = array('controller' => 'classificados', 'action' => 'create_categorias');
	$this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'classificados', 'action' => 'create_categorias'));
}

if ($this->request->params['action'] == 'admin_edit_categorias') {
	$formUrl = array('controller' => 'classificados', 'action' => 'edit_categorias');
	$this->Html->addCrumb($this->request->data['Categoria']['title'], '/' . $this->request->url);
}

$lookupUrl = $this->Html->apiUrl(array(
	'plugin' => 'users',
	'controller' => 'users',
	'action' => 'lookup',
));

$this->append('form-start', $this->Form->create('Categoria', array(
	'url' => $formUrl,
	'class' => 'protected-form',
)));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Categoria'), '#node-main');
	echo $this->Croogo->adminTab(__d('croogo', 'Classificado'), '#node-classificado');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	echo $this->Html->tabStart('node-main') .
		$this->Form->input('id') .
		$this->Form->input('title', array(
			'label' => __d('croogo', 'Title'),
		)) .
		$this->Form->input('slug', array(
			'label' => __d('croogo', 'Slug'),
		)) .
	$this->Html->tabEnd();

	echo $this->Html->tabStart('node-classificado') .
		$this->Form->input('id') .
		$this->Form->input('classificado_id', array(
			'label' => __d('croogo', 'Classificado'),
		)) .
	$this->Html->tabEnd();

	echo $this->Croogo->adminTabs();
$this->end();

$username = isset($this->request->data['User']['username']) ?
	$this->request->data['User']['username'] :
	$this->Session->read('Auth.User.username');

$this->start('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
		$this->Form->input('status', array(
			'type' => 'checkbox',
			'label' => __d('croogo', 'Active'),
		));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());