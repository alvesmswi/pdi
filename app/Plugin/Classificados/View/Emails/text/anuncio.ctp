<?php
$url = Router::url($anuncio['Anuncio']['url'], true);

echo __d('croogo', 'You have received a new message at: %s', $url) . "\n \n";
echo __d('croogo', 'Name: %s', $message['nome']) . "\n";
echo __d('croogo', 'Email: %s', $message['email']) . "\n";
echo __d('croogo', 'Telefone: %s', $message['telefone']) . "\n";
echo __d('croogo', 'Subject: %s', $anuncio['Anuncio']['title']) . "\n";
echo __d('croogo', 'Message: %s', $message['mensagem']) . "\n";
?>