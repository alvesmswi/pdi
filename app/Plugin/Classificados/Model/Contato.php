<?php
App::uses('AppModel', 'Model');

/**
 * Contato
 *
 * @category Contato.Model
 * @package  Contato.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Contato extends AppModel {
	public $name 		= 'Contato';
	public $useTable 	= 'classificado_node_contatos';
	public $validate 	= array(
		'nome' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'email' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'telefone' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'mensagem' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'node_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
	);
	public $belongsTo = array(
		'Anuncio' => array(
			'className' => 'Classificados.Anuncio',
			'foreignKey' => 'node_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		return true;
	}
}