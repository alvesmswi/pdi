<?php
App::uses('AppModel', 'Model');

/**
 * Categoria
 *
 * @category Categoria.Model
 * @package  Categoria.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Categoria extends AppModel {
	public $name 		= 'Categoria';
	public $useTable 	= 'classificado_categorias';
	public $validate 	= array(
		'title' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'slug' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This slug has already been taken.',
			),
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Slug cannot be empty.',
			),
		),
		'classificado_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
	);
	public $belongsTo = array(
		'Classificado' => array(
			'className' => 'Classificados.Classificado',
			'foreignKey' => 'classificado_id',
			'conditions' => array('Classificado.status' => 1),
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]['slug']) && isset($value['Classificado']['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view',
					'classificado' => $value['Classificado']['slug'], 'slug' => $value[$this->alias]['slug']
				);
			} elseif (isset($value[$this->alias]['slug']) && isset($value[$this->alias]['Classificado']['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view',
					'classificado' => $value[$this->alias]['Classificado']['slug'], 'slug' => $value[$this->alias]['slug']
				);
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		return true;
	}
}