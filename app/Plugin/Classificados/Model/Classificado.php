<?php
App::uses('AppModel', 'Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Classificado
 *
 * @category Classificado.Model
 * @package  Croogo.Classificado.Model
 * @version  1.0
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Classificado extends AppModel {
	public $name = 'Classificado';
	public $useTable = 'classificados';
	public $validate = array(
		'title' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'slug' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This slug has already been taken.',
			),
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Slug cannot be empty.',
			),
		),
	);
	public $hasMany = array(
		'Categoria' => array(
			'className' => 'Classificados.Categoria',
			'foreignKey' => 'classificado_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $q
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view_classificado',
					'slug' => $value[$this->alias]['slug']
				);
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['avatar']['name'])) {
			$image = $this->upload($this->data[$this->alias]['avatar']);
			if ($image) {
				$this->data[$this->alias]['image'] = $image;
			}
		}

		return true;
	}

	private function upload($file) 
	{
		$base_dir 	= WWW_ROOT.'files'.DS.'blogs'.DS.'avatar';
		$path 		= DS.'files'.DS.'blogs'.DS.'avatar';
		$dir 		= new Folder($base_dir, true, 0755);

		if ($file['error'] === 0) {
            $file_name 		= $file['name'];
            $destination 	= $path.DS.$file_name;
            $file 			= new File($file['tmp_name']);

            $file->copy($base_dir.DS.$file_name);
            $file->close();

            return $destination;
		}

		return false;
	}
}