<?php
App::uses('AppModel', 'Model');

/**
 * Anuncio
 *
 * @category Anuncio.Model
 * @package  Anuncio.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Anuncio extends AppModel {
	public $name 		= 'Anuncio';
	public $useTable 	= 'classificado_nodes';
	public $validate 	= array(
		'title' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'slug' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This slug has already been taken.',
			),
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Slug cannot be empty.',
			),
		),
		'categoria_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
	);
	public $belongsTo = array(
		'User' => array(
			'className' => 'Users.User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Categoria' => array(
			'className' => 'Classificados.Categoria',
			'foreignKey' => 'categoria_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]['slug']) && isset($value['Categoria']['Classificado']['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'classificados', 'controller' => 'classificados', 'action' => 'view_anuncio',
					'classificado' => $value['Categoria']['Classificado']['slug'], 'secao' => $value['Categoria']['slug'],
					'slug' => $value[$this->alias]['slug']
				);
			}

			if (isset($value[$this->alias]['preco'])) {
				$results[$key][$this->alias]['preco'] = number_format($value[$this->alias]['preco'], '2', ',', '.');
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		if (isset($this->data[$this->alias]['preco']) && !empty($this->data[$this->alias]['preco'])) {
			$this->data[$this->alias]['preco'] = $this->tofloat($this->data[$this->alias]['preco']);
		}

		return true;
	}

	/**
	 * tofloat function
	 *
	 * @param int $num
	 * @return void
	 */
	private function tofloat($num) 
	{
		$dotPos 	= strrpos($num, '.');
		$commaPos 	= strrpos($num, ',');

		if (($dotPos > $commaPos) && $dotPos) {
			$sep = $dotPos;
		} elseif (($commaPos > $dotPos) && $commaPos) {
			$sep = $commaPos;
		} else {
			$sep = false;
		}
	
		if (!$sep) {
			return number_format(preg_replace("/[^0-9]/", "", $num), 2, '.', '');
		} 

		return 
			preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
			preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)));
	}
}