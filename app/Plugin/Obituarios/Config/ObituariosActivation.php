<?php

class ObituariosActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
    public function beforeActivation(&$controller) 
    {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
    public function onActivation(&$controller) 
    {
		Cache::clear(false, '_cake_model_');
	}

	/**
	 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 *
 * @return boolean
 */
    public function beforeDeactivation(&$controller) 
    {
		return true;
	}

    public function onDeactivation(&$controller) 
    {
        return true;
	}
}