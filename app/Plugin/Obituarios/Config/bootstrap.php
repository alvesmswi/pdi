<?php

CroogoNav::add('sidebar', 'settings.children.obituario', array(
	'title' => 'Obituário',
	'url' => array(
		'admin' => true,
		'plugin' => 'obituarios',
		'controller' => 'obituarios',
		'action' => 'config'
	)
));

Croogo::hookRoutes('Obituarios');