<?php if (!empty($registros)): ?>
    <h2><?php echo $registros['titulo'] ?></h2>

    <table class="table">
        <?php foreach ($registros['obitos'] as $key => $registro): ?>
                <?php foreach ($registro as $coluna => $valor): ?>
                    <tr>
                        <td style="font-weight: bold; width: 30%;"><?php echo $coluna ?></td>
                        <td><?php echo $valor ?></td>
                    </tr>
                <?php endforeach; ?>
            <tr><td colspan="2"></td></tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
