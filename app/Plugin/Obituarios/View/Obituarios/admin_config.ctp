<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Obituarios', array(
    'admin' => true,
    'plugin' => 'Obituarios',
    'controller' => 'Obituarios',
    'action' => 'config'
  ));

$this->append('form-start', $this->Form->create('Obituarios', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
    echo $this->Form->input('Obituarios.obituarioUrl', array(
        'label' => 'URL do Obituário',
        'required'	=> true
    ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());