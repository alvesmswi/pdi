<?php

App::uses('ObituarAppController', 'Obituarios.Controller');
App::uses('HttpSocket', 'Network/Http');

class ObituariosController extends ObituariosAppController {

    public $name = 'Obituarios';
    public $uses = array('Settings.Setting');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        
		$this->Auth->allow('index');
    }

    public function admin_config() 
    {
        $this->set('title_for_layout', 'Obituários');
        
		if (!empty($this->request->data)) {
			if ($this->Setting->deleteAll(array('Setting.key LIKE' => 'Obituarios.%'))) {
				foreach ($this->request->data['Obituarios'] as $key => $value) {
					$arraySave = array();
					$arraySave['id']            = null;
					$arraySave['editable']      = 0;
					$arraySave['input_type']    = 'text';
					$arraySave['key']           = 'Obituarios.'.$key;
					$arraySave['value']         = $value;
					$arraySave['title']         = $key;

					$this->Setting->create();
					if ($this->Setting->saveAll($arraySave)) {
						$this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
					} else {
						$this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
					}
				}
			}			
		}

		//limpa o this->data
		$this->request->data = array();
		//Pega os dados
		$arrayDados = $this->Setting->find('all',
			array(
                'conditions' => array('key LIKE' => 'Obituarios.%'), 
                'recursive' => -1
            )
        );
        
		if (!empty($arrayDados)) {
			foreach ($arrayDados as $dados) {
				$this->request->data['Obituarios'][$dados['Setting']['title']] = $dados['Setting']['value'];
			}
        }
    }
    
    private function getObituarios()
    {
        $obituarios = array();

        $HttpSocket = new HttpSocket();
        $URLObituarios = Configure::read('Obituarios.obituarioUrl');

        if (!empty($URLObituarios)) {
            $response = $HttpSocket->get($URLObituarios);

            // Get the status code for the response.
            $code = $response->code;

            if ($code === '200') {
                $DOM = new DOMDocument();
                libxml_use_internal_errors(true); //disable libxml errors

                $DOM->loadHTML($response->body);
                libxml_clear_errors(); //remove errors for yucky html

                $DOM_xpath = new DOMXPath($DOM);

                $pagina_geral = $DOM_xpath->query('//div[@class="pagina_geral"]');

                if ($pagina_geral->length > 0) {
                    foreach ($pagina_geral as $geral) {
                        $titulo = $DOM_xpath->query('h2', $geral)->item(0)->nodeValue;
                        $obituarios['titulo'] = utf8_decode($titulo);

                        $obitos = $DOM_xpath->query('//tr', $geral);
                        $contadorObito = 0;
                        $contadorObitoVerificacao = 1;
                        foreach ($obitos as $obito) {
                            $coluna = $DOM_xpath->query('td', $obito)->item(0)->nodeValue;

                            if (!empty($coluna)) {
                                $valor = $DOM_xpath->query('td', $obito)->item(1)->nodeValue;

                                $obituarios['obitos'][$contadorObito][utf8_decode($coluna)] = utf8_decode($valor);

                                if (utf8_decode($coluna) == 'Funerária:') {
                                    $contadorObito++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $obituarios;
    }

    public function index()
    {
        if (Cache::read('obituarios')) {
            $obituarios = Cache::read('obituarios');
        } else {
            $obituarios = $this->getObituarios();
            Cache::write('obituarios', $obituarios, 'home');
        }

        $this->set('registros', $obituarios);
    }
}