<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Sanitize', 'Utility');
App::uses('Croogo', 'Lib');

App::import('Vendor', 'Medias.Slim', array('file' => 'slim.php'));

/**
 * Images Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class ImagesController extends AppController {
	public $name 			= 'Images';
	public $pluginPrefix 	= "Images";
	public $uses 			= array('Medias.Image');
	public $components 		= array('Medias.Preset');
	public $defaults 		= array();
	public $pathMedias 		= null;

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

		$this->pathMedias 		= WWW_ROOT.'medias'.DS.'files'.DS;

        $this->Security->unlockedActions += array('admin_node', 'admin_remove_image', 'admin_reorder_image');
        $this->Auth->allow(array('display_file'));
	}

	/**
	 * admin_node
	 * Image node add
	 */
	public function admin_node() 
	{
		$this->autoRender = false;
		$path = $this->pathMedias;

		if (!isset($this->request->query['node_id'])) {
		    Slim::outputJSON(array(
		        'status' => SlimStatus::FAILURE,
		        'message' => 'No node_id found'
		    ));
		    return;
		}

		// ID node
		$node_id = $this->request->query['node_id'];

		// Controller reference
		$controller = $this->request->query['controller'];

		// Get posted data, if something is wrong, exit
		try {
		    $images = Slim::getImages();
		} catch (Exception $e) {
		    // Possible solutions
		    // ----------
		    // Make sure you're running PHP version 5.6 or higher
		    Slim::outputJSON(array(
		        'status' => SlimStatus::FAILURE,
		        'message' => 'Unknown'
		    ));

		    return;
		}

		// No image found under the supplied input name
		if ($images === false) {
		    // Possible solutions
		    // ----------
		    // Make sure the name of the file input is "slim[]" or you have passed your custom
		    // name to the getImages method above like this -> Slim::getImages("myFieldName")
		    Slim::outputJSON(array(
		        'status' => SlimStatus::FAILURE,
		        'message' => 'No data posted'
		    ));

		    return;
		}

		// Should always be one image (when posting async), so we'll use the first on in the array (if available)
		$image = array_shift($images);

		// Something was posted but no images were found
		if (!isset($image)) {
		    // Possible solutions
		    // ----------
		    // Make sure you're running PHP version 5.6 or higher
		    Slim::outputJSON(array(
		        'status' => SlimStatus::FAILURE,
		        'message' => 'No images found'
		    ));

		    return;
		}

		// If image found but no output image data present
		if (!isset($image['output']['data'])) {
		    // Possible solutions
		    // ----------
		    // If you've set the data-post attribute make sure it contains the "output" value -> data-post="actions,output"
		    // If you want to use the input data and have set the data-post attribute to include "input", replace the 'output' String above with 'input'
		    Slim::outputJSON(array(
		        'status' => SlimStatus::FAILURE,
		        'message' => 'No image data'
		    ));

		    return;
		}

		// Save the file
		$registro = $this->Image->findByFilenameAndNodeIdAndController($image['input']['name'], $node_id, $controller);

		if (!empty($registro)) {
			$name = $image['input']['name'];
		} else {
			$only_name = pathinfo($image['input']['name'], PATHINFO_FILENAME);
			$ext = pathinfo($image['input']['name'], PATHINFO_EXTENSION);
			$name_format = '%s_%s.%s';
			$name = sprintf($name_format, $only_name, uniqid(), $ext);
		}

		// We'll use the output crop data
		// note: If you want to save the input data, replace the 'output' string below with 'input'
		$data = $image['output']['data'];

		// If you want to prevent Slim from adding a unique id to the file name add false as the fourth parameter.
		// $file = Slim::saveFile($data, $name, 'tmp/', false);
		$file = Slim::saveFile($data, $name, $path, false);

		if (!empty($file)) {
			$registro = $this->Image->findByFilenameAndNodeIdAndController($file['name'], $node_id, $controller);
			$id_image = (!empty($registro)) ? $registro['Image']['id'] : null;

			$image_save = array(
				'id' 		=> $id_image,
				'node_id' 	=> $node_id,
				'filename' 	=> $file['name'],
				'alt' 		=> ((isset($image['meta']->alt)) ? $image['meta']->alt : null),
				'legenda' 	=> ((isset($image['meta']->caption)) ? $image['meta']->caption : null),
				'credito' 	=> ((isset($image['meta']->credit)) ? $image['meta']->credit : null),
				'order' 	=> $image['meta']->order,
				'status' 	=> 1,
				'controller'=> $controller
			);

			if ($this->Image->save($image_save)) {
				//CakeLog::write('debug', json_encode($image_save));
			}
		}

		// Return results as JSON
		Slim::outputJSON(array(
		    'status' => SlimStatus::SUCCESS,
		    'file' => $file['name'],
		    'path' => $file['path']
		));
	}

	/**
	 * admin_remove_image
	 * Image node remove
	 */
	public function admin_remove_image() 
	{
		$this->autoRender = false;
		// ID node
		$node_id = $this->request->query['node_id'];

		// Controller reference
		$controller = $this->request->query['controller'];

		// Filename
		$filename = basename($this->request->data['filename']);
		$filename = str_replace('?', '', $filename);
		$filename = urldecode($filename);

		if (!empty($filename) && !empty($node_id) && !empty($controller)) {
			if ($this->Image->deleteAll(
				array(
					'Image.node_id' 	=> $node_id,
					'Image.filename' 	=> $filename,
					'Image.controller' 	=> $controller
				),
				false
			)) {
			    $file = new File($this->pathMedias . $filename);
			    $file->delete();
				return true;
			}
		}

		return false;
	}

	/**
	 * admin_reorder_image
	 * Image node remove
	 */
	public function admin_reorder_image() 
	{
		$this->autoRender = false;

		// ID node
		$node_id = $this->request->query['node_id'];

		// Controller reference
		$controller = $this->request->query['controller'];

		foreach ($this->request->data['elements'] as $image) {
			// Filename
			$filename = basename($image['filename']);
			$filename = str_replace('?', '', $filename);

			// Order
			$order = basename($image['order']);

			if (!empty($filename) && !empty($node_id) && !empty($controller)) {
				if ($this->Image->updateAll(
					array(
						'Image.order' => $order,
					),
					array(
						'Image.node_id' 	=> $node_id,
						'Image.filename' 	=> $filename,
						'Image.controller' 	=> $controller
					)
				)) {
					//CakeLog::write('debug', json_encode($filename));
				}
			}
		}
	}

	public function display_file($filename, $preset)
	{
		$this->autoRender = false;

		$cacheConfig = 'long_';
		$cacheName = 'medias_images_'.$preset.'_'.$filename;

		$image = Cache::read($cacheName, $cacheConfig);

		if (!$image) {
			if ($preset != 'normal') {
				$pathImage = $this->Preset->image($filename, $preset);
			} else {
				$pathImage = $this->pathMedias . $filename;
			}

			if (file_exists($pathImage)) {
				$image = $pathImage;
				Cache::write($cacheName, $image, $cacheConfig);
			} else {
				throw new NotFoundException();
			}
		}

		//$this->response->type(mime_content_type($image));
		$this->response->sharable(true, 172800);
		//$this->response->expires('+5 days');
		//$this->response->cache('-1 minute', '+5 days');
		$this->response->file($image, array('download' => false, 'name' => $filename));
		$this->response->body($image);

		return $this->response;
	}
}