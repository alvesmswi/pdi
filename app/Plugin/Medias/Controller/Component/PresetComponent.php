<?php
App::import('Vendor', 'Medias.ImageResize', array('file' => 'ImageResize.php'));

class PresetComponent extends Component 
{
	public $pathMedias = null;

    public function image($filename, $preset) 
    {
		$this->pathMedias = WWW_ROOT.'medias'.DS.'files'.DS;
		// Pasta do Preset
		$presetDir = $this->pathMedias . $preset;

		// Pega a imagem enviada
		$image = new ImageResize($this->pathMedias . $filename);

		$arrayPreset = explode('x', $preset);

		if (isset($arrayPreset[0]) && !isset($arrayPreset[1])) {
			$image->resizeToWidth($arrayPreset[0]);
		} elseif (isset($arrayPreset[1])) {
			$image->crop($arrayPreset[0], $arrayPreset[1]);
		}

		// Se o diretorio ainda não existe
		if (!is_dir($presetDir)) {
			// Cria o diretorio
			mkdir($presetDir);
		}

		// Salva a imagem
		$image->save($presetDir.DS.$filename);

		return $presetDir.DS.$filename;
	}
}
