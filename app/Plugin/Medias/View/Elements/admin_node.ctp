<?php echo $this->Html->css(array('Medias.slim.min', 'Medias.slim.cms', 'Medias.dragula.min', 'Medias.medias')); ?>

<?php 
echo $this->Form->input('id');
$node_id 	= ((isset($this->params['pass'][0])) ? $this->params['pass'][0] : rand());
$controller = $this->params['controller'];

echo $this->Form->hidden('node_id_medias', array('value'=> $node_id));
?>

<div class="slim-collection" data-order="/admin/medias/image/reorder?node_id=<?php echo $node_id ?>&controller=<?php echo $controller ?>">
<?php if (isset($this->data['Images']) && !empty($this->data['Images'])) { ?>

	<?php foreach ($this->data['Images'] as $key => $image) { ?>
	  <div class="slim" data-service="/admin/medias/image/upload?node_id=<?php echo $node_id ?>&controller=<?php echo $controller ?>" data-ratio="free" data-meta-alt="<?php echo $image['alt'] ?>" data-meta-caption="<?php echo $image['legenda'] ?>" data-meta-credit="<?php echo $image['credito'] ?>" data-remove="/admin/medias/image/upload_remove?node_id=<?php echo $node_id ?>&controller=<?php echo $controller ?>" data-nodeid="<?php echo $node_id ?>">
	    <input type="file"/>
			<?php echo $this->Html->image($image['url'], array('alt' => $image['alt'])); ?>
	  </div>
	<?php } ?>

<?php } else { ?>
  <div class="slim" data-service="/admin/medias/image/upload?node_id=<?php echo $node_id ?>&controller=<?php echo $controller ?>" data-ratio="free" data-remove="/admin/medias/image/upload_remove?node_id=<?php echo $node_id ?>&controller=<?php echo $controller ?>" data-nodeid="<?php echo $node_id ?>">
    <input type="file"/>
  </div>
<?php } ?>
</div>

<?php echo $this->Html->script(array('Medias.dragula.min', 'Medias.slim.jquery', 'Medias.slim.cms')); ?>