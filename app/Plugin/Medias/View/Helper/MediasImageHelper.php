<?php

App::uses('AppHelper', 'View/Helper');

class MediasImageHelper extends AppHelper 
{
	public $helpers = array(
		'Html'
	);
	
    public function imagePreset($filename, $preset = 'normal') 
    {
        return "/medias/images/{$preset}/{$filename}";
	}
}