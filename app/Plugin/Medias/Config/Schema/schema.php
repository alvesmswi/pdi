<?php
class MediasSchema extends CakeSchema {
	public $media_images = array(
		'id' 		=> array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'node_id' 	=> array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'filename' 	=> array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'alt' 		=> array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'legenda' 	=> array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'credito' 	=> array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'order' 	=> array('type' => 'biginteger', 'null' => false, 'default' => '0'),
		'status' 	=> array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'controller'=> array('type' => 'string', 'null' => false, 'default' => 'nodes', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' 	=> array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'node_id' => array('column' => 'node_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public function before($event = array()) 
	{
		return true;
	}

	public function after($event = array()) 
	{
	}
}