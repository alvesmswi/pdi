<?php
App::uses('CakeSchema', 'Model');
App::uses('ConnectionManager', 'Model');

/**
 * Medias Activation
 *
 * Activation class for Medias plugin.
 *
 * @package  Medias
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class MediasActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}

	public function onActivation(Controller $controller) 
	{
		$tableName = 'media_images';
		$pluginName = 'Medias';
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();

		if (!in_array(strtolower($tableName), $tables)) {
			$schema = & new CakeSchema(array(
				'name' => $pluginName,
				'path' => APP . 'Plugin' . DS . $pluginName . DS . 'Config' . DS . 'schema',
			));
			$schema = $schema->load();
			foreach ($schema->tables as $table => $fields) {
				$create = $db->createSchema($schema, $table);
				try {
					$db->execute($create);
				} catch (PDOException $e) {
					die(__('Could not create table: %s', $e->getMessage()));
				}
			}
		}
	}

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
	}
}