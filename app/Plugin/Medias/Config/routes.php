<?php
CroogoRouter::connect('/medias/images/:preset/:filename', 
	array(
		'plugin' => 'medias', 'controller' => 'images', 'action' => 'display_file', 
		'admin' => false
	),
	array(
		'pass' => array('filename', 'preset')
	)
);

CroogoRouter::connect('/admin/medias/image/upload', array(
	'plugin' => 'medias', 'controller' => 'images', 'action' => 'node', 
	'admin' => true
));
CroogoRouter::connect('/admin/medias/image/upload_remove', array(
	'plugin' => 'medias', 'controller' => 'images', 'action' => 'remove_image', 
	'admin' => true
));
CroogoRouter::connect('/admin/medias/image/reorder', array(
	'plugin' => 'medias', 'controller' => 'images', 'action' => 'reorder_image', 
	'admin' => true
));