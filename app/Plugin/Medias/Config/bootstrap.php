<?php
Croogo::hookRoutes('Medias');

///Croogo::hookAdminTab('Nodes/admin_edit', 'Imagens', 'Medias.admin_node');
/*Croogo::hookModelProperty('Node', 'hasMany', array(
	'Images' => array(
		'className' 	=> 'Medias.Image',
		'foreignKey' 	=> 'node_id',
		'dependent' 	=> true,
		'conditions' 	=> array('Images.controller' => 'nodes'),
		'order' 		=> 'Images.order ASC'
	)
));*/

Croogo::hookAdminTab('Classificados/admin_create_anuncios', 'Imagens', 'Medias.admin_node');
Croogo::hookAdminTab('Classificados/admin_edit_anuncios', 'Imagens', 'Medias.admin_node');
Croogo::hookModelProperty('Anuncio', 'hasMany', array(
	'Images' => array(
		'className' 	=> 'Medias.Image',
		'foreignKey' 	=> 'node_id',
		'dependent' 	=> true,
		'conditions' 	=> array('Images.controller' => 'classificados'),
		'order' 		=> 'Images.order ASC'
	)
));
Croogo::hookBehavior('Anuncio', 'Medias.Images');

Croogo::hookAdminTab('Blogs/admin_create_post', 'Imagens', 'Medias.admin_node');
Croogo::hookAdminTab('Blogs/admin_edit_post', 'Imagens', 'Medias.admin_node');
Croogo::hookModelProperty('Post', 'hasMany', array(
	'Images' => array(
		'className' 	=> 'Medias.Image',
		'foreignKey' 	=> 'node_id',
		'dependent' 	=> true,
		'conditions' 	=> array('Images.controller' => 'blogs'),
		'order' 		=> 'Images.order ASC'
	)
));
Croogo::hookBehavior('Post', 'Medias.Images');
