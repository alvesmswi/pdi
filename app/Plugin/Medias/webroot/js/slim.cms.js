(function() {


    function lowercaseFirstLetter(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    };
    
    function getMeta(element) {

        var dataset = $(element).data();
        var options = {};

        for (var prop in dataset) {

            var _value = dataset[prop];

            if (prop.indexOf('meta') === 0) {
                options[lowercaseFirstLetter(prop.substr(4))] = _value;
            }
        }

        return options;
    }

    var OPTIONS = {
        service: "please-override.php",
        meta: {
            alt: null,
            caption: null,
            credit: null
        },
        label: "Arraste sua imagem aqui<br/><small>(ou clique para selecioná-la)</small>",
        buttonEditLabel: "Editar",
        buttonRemoveLabel: "Excluir",
        buttonDownloadLabel: "Baixar",
        buttonUploadLabel: "Enviar",
        buttonCancelLabel: "Cancelar",
        buttonConfirmLabel: "Confirmar",
        maxFileSize: 2,
        statusFileSize: "Não foi possível enviar sua imagem. Apenas imagens com até $0 MB são aceitas.",
        statusNoSuppose: "O seu navegador não suporta edição de imagens.",
        statusImageTooSmall: "Esta imagem é muito pequena, o tamanho mínimo é $0 pixels.",
        statusContentLength: "Não foi possível processar a sua imagem. É possível que ela seja grande demais.",
        statusUnknownResponse: "Um erro desconhecido ocorreu.",
        statusUploadSuccess: "Salvo",
        didInit: function(data) {
            var buttons = $(this._btnGroup);
            buttons.find('.slim-btn-upload').hide();
        },
        didLoad: function(file, image, meta) {
            var $el = $(this.element);
            // var src = $el.children('img').attr('src');
            var buttons = $(this._btnGroup);
            var $closest = $el.slim('find');
            var data = $closest.slim('data');

            if (!$el.data('init-image')) {
                buttons.find('.slim-btn-upload').show();
            }

            if (!buttons.find('.slim-btn-meta').length) {
                var btn = $('<button class="slim-btn slim-btn-meta" title="Legenda" type="button" data-action="caption">Legenda</button>');
                var wrapper = $('<div class="slim-btn-wrapper"></div>');
                var panel = $('<div class="slim-btn-panel"><div class="form-group"><label>Alt:</label><input type="text" class="form-control" name="alt[]" data-meta="alt" /></div><div class="form-group"><label>Legenda:</label><input type="text" class="form-control" name="caption[]" data-meta="caption" /></div><div class="form-group"><label>Crédito:</label><input type="text" class="form-control" name="credit[]" data-meta="credit" /></div><span class="slim-btn-panel--arrow" aria-hidden></span></div>');

                btn.on('click', function(e) {
                    
                    e.stopPropagation();

                    var meta = {
                        alt: $el.data('meta-alt') || data.length && data[0].meta.alt || "",
                        caption: $el.data('meta-caption') || data.length && data[0].meta.caption || "",
                        credit: $el.data('meta-credit') || data.length && data[0].meta.credit || ""
                    }

                    if (panel.is(':visible')) {
                        panel.fadeOut();
                    } else {
                        $('.slim-btn-panel').filter(':visible').fadeOut();
                        panel.fadeIn();
                    }

                    $(this)
                        .siblings('.slim-btn-panel')
                        .find('input[data-meta]')
                            .each(function(i) {
                                var d = $(this).data('meta');

                                if (i === 0) {
                                    $(this).focus();
                                }
                                if (typeof meta[d] !== "undefined") {
                                    $(this).val(meta[d]);
                                }
                            });
                });

                panel.on('blur', 'input', function(e) {
                    var d = $(this).data('meta');
                    var v = $(this).val();
                    
                    if ($el.data('meta-' + d) !== v) {
                        $el.data('meta-' + d, v);
                        buttons.find('.slim-btn-upload').show();
                    }
                });

                wrapper.append(btn);
                wrapper.append(panel);
                buttons.append(wrapper);
            }

            var container = $el.closest('.slim-collection');

            if (!container.find('[data-state="empty"]').length) {
                var newEl = $('<div class="slim" style="display: none;"><input type="file" /></div>');
                var newCropper = newEl.slim($el.slim('find')[0]._options);
                newEl.fadeIn(1000);
                container.append(newCropper);
            }

            container.find('.slim[data-state]:not([data-state="empty"])').each(function(i) {
                this.dataset.metaOrder = i;

                var server = $(this).slim('data')[0].server;
                var prev = $(this).data('init-image') || "";

                if (server) prev = server.path;

                var input = container.find('[name="data[order][' + i + ']"]');

                if (!input.length) {
                    input = $('<input type="hidden" name="data[order][' + i + ']" data-file="' + prev + '" data-order="' + i + '" />').insertBefore(container.find('.slim:first'));
                }

                input.val(prev);
            });

            return true;
        },
        willRemove: function(slim, remove) {

            var filename;
            var el = $(this._element)
            var removeUrl = el.data('remove');
            var nodeId = el.data('nodeid') || el.data('meta-nodeid');
            
            if (slim.server) {
                filename = slim.server.file;
            } else {
                filename = el.children('img').first().attr('src');
            }

            if (!filename) return remove();

            console.log(filename);

            $.ajax({
                type: "POST",
                url: removeUrl || './async-remove.php',
                data: {
                    filename: filename,
                    nodeid: nodeId
                }
            });

            remove();
        },
        didUpload: function(error, data, response) {

            var $el = $(this.element);
            var container = $el.closest('.slim-collection');
            var order;

            if (container.length) {
                order = $el.attr('data-meta-order');
                var input = container.find('[name="data[order][' + order + ']"]');
                input.val(response.path);
                input.attr('data-file', response.path);
            }
        },
        didRemove: function(data) {
            var $el = $(this._element);
            var $container = $el.closest('.slim-collection');

            if ($container.length && $container.find('[data-state="empty"]').length > 1) {
                $el.slideUp(300, function() {
                    $(this).remove();
                    var inputs = $container.find('input[type=hidden][data-order]');
                    var slims = $container.find('.slim:not([data-state="empty"])');

                    inputs.each(function(i) {
                        var s = slims.eq(i);
                        var d, f;
                        if (s.length) {
                            d = s.slim('data')[0].server;
                            f = d && d.path || s.data('init-image') || null;
                            $(this).attr('data-file', f).attr('value', f);
                            s.attr('data-meta-order', i);
                        } else {
                            $(this).remove();
                        }
                    });
                });
            }
        },
        willSave: function(data, ready) {
            var meta = getMeta(this._element);
            meta = $.extend({}, data.meta, meta);
            data.meta = meta;

            ready(data);
        }
    }

    $('.slim').each(function() {
        var $this = $(this);
        var src = $this.find('img').attr('src');

        if (src && src.length && src.indexOf('data:image') !== 0) {
            $this.data('init-image', src);
        }

        $this.slim(OPTIONS);
    });

    $(document.body).on('click', function(e) {
        var target = $(e.target);
        var visible = $('.slim-btn-panel').filter(':visible');

        if (!target.hasClass('slim-btn-panel') && !target.closest('.slim-btn-panel').length && visible.length) {
            var focused = visible.find(':focus');

            if (focused) {
                focused.blur();
                visible.siblings('.slim-btn-meta').focus();
            }

            $('.slim-btn-panel').filter(':visible').fadeOut();
            
        }
    });

    var DRAGULA = dragula([].slice.call(document.querySelectorAll('.slim-collection')), {
        revertOnSpill: true,
        direction: 'vertical',
        accepts: function(el, target, source, sibling) {
            return target === source;
        },
        moves: function(el, target, source, sibling) {
            return el.dataset.state !== "empty";
        }
    });

    DRAGULA.on('drop', function(el, target, source, sibling) {
        var els = [].slice.call(target.querySelectorAll('.slim[data-state]:not([data-state="empty"])'));
        var nodeid = els[0].dataset.nodeid;
        var endpoint = target.dataset.order;
        var res = {
            nodeid: nodeid,
            elements: []
        };
        els.forEach(function(el, i) {
            var m = +el.dataset.metaOrder;
            var input, file, data, item;
            el.dataset.metaOrder = i;

            if (m !== i) {
                $el = $(el);
                data = $el.slim('data')[0].server;
                input = $el.closest('.slim-collection').find('[data-order="' + i + '"]');
                file = data && data.path || $el.data('init-image') || null;
                input.val(file);
                input.attr('data-file', file);

                item = {
                    filename: file,
                    order: i
                };
                console.log(item);
                res.elements.push(item);
            }
        });

        console.log(res);
        if (endpoint && res.elements.length) {
            $.ajax({
                type: "POST",
                url: endpoint,
                data: res
            });
        }
    });

    
}());