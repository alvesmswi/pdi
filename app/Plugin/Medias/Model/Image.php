<?php
App::uses('AppModel', 'Model');

/**
 * Image
 *
 * @category Model
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class Image extends AppModel {
	public $name 		= 'Image';
	public $useTable 	= 'media_images';
	public $validate 	= array(
		'node_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]['filename'])) {
				$results[$key][$this->alias]['url'] = '/medias/images/normal/'.$value[$this->alias]['filename'];
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		return true;
	}
}