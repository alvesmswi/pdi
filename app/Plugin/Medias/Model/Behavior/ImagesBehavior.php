<?php

App::uses('ModelBehavior', 'Model');
App::uses('Image', 'Medias.Model');

class ImagesBehavior extends ModelBehavior 
{
    public function afterSave(Model $model, $created, $options = array())
    {
        if ($created) {
            $Image = new Image();
                
            if (isset($model->data[$model->alias]['node_id_medias']) && !empty($model->data[$model->alias]['node_id_medias'])) {
                $node_id_medias = $model->data[$model->alias]['node_id_medias'];
                $node_id        = $model->data[$model->alias]['id'];

                $Image->updateAll(
                    array('Image.node_id' => $node_id),
                    array('Image.node_id' => $node_id_medias)
                );
            }
        }
    }
}
