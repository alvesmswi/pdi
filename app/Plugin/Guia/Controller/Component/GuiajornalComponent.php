<?php
class GuiajornalComponent extends Component {
    function onlyNumbers($string){
		return preg_replace("/[^0-9]/", '', $string);
	}

	function getAreaCode($phone){
		preg_match('#\((.*?)\)#', $phone, $match);
		return $match[1];
	}

	function getPhone($phone){
		$arrayPhone = explode(')',$phone);
		return $this->onlyNumbers($arrayPhone[1]);
	}

	function tratarData($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('/', '-', $valor);
			$resultValor = date('Y-m-d', strtotime($valorReplace));
		}else{
			$resultValor = date('d/m/Y', strtotime($valor));
		}
		return $resultValor;
	}

	function tratarValor($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('.', '', $valor);
			$valorReplace = str_replace(',', '.', $valorReplace);
			$resultValor = $valorReplace;
		}else{
			$resultValor = number_format($valor,2,',','.');
		}
		return $resultValor;
	}

	function arrayPrecoMedio(){
		return array(
			1 => 'até 10,00 reais',
			2 => 'de 11,00 a 25,00 reais',
			3 => 'de 26,00 a 50,00 reais',
			4 => 'acima de 50,00 reais',
			5 => 'Outros Valores',
			6 => 'Não Informado',
			7 => 'de 3,50 até 20,00 reais',
			8 => '',
		);
	}

	function arrayGeneroMusical(){
		return array(
			1 	=> 'Rock',
			3 	=> 'Eletrônico',
			10 	=> 'Forró',
			6 	=> 'Hip Hop',
			9 	=> 'Pagode',
			2 	=> 'Pop',
			21 	=> 'Pop/Rock',
			11 	=> 'Psy',
			7 	=> 'Rap',
			5 	=> 'Reggae',
			8 	=> 'Samba',
			4 	=> 'Sertanejo',
		);
	}

	function arrayOpcionais(){
		return array(
			'opcionais_a_la_carte' => 'À la carte',
			'opcionais_cadeirantes' => 'Acesso / facilidades para cadeirantes',
			'opcionais_ar_condicionado' => 'Ambiente com ar condicionado',
			'opcionais_antialergico' => 'Apartamento antialérgico',
			'opcionais_nao_fumantes' => 'Apartamento para não fumantes',
			'opcionais_fumantes' => 'Fumantes',
			'opcionais_bar' => 'Bar',
			'opcionais_buffet' => 'Buffet',
			'opcionais_business_center' => 'Business Center',
			'opcionais_churrascaria' => 'Churrascaria',
			'opcionais_cobra_10' => 'Cobra 10%',
			'opcionais_entrega_domicilio' => 'Entrega à domicílio',
			'opcionais_bilingue' => 'Equipe bilingue',
			'opcionais_espaco_criancas' => 'Espaço para as crianças',
			'opcionais_fecha_grupos' => 'Fecha para grupo',
			'opcionais_lavanderia' => 'Lavanderia',
			'opcionais_manobrista' => 'Manobrista',
			'opcionais_mapa_poltronas' => 'Mapa das poltronas',
			'opcionais_mapa_ambiente' => 'Mapa do ambiente',
			'opcionais_musica_vivo' => 'Música ao vivo',
			'opcionais_piscina' => 'Piscina',
			'opcionais_porcoes' => 'Porções',
			'opcionais_restaurante' => 'Restaurante',
			'opcionais_rodizio' => 'Rodízio',
			'opcionais_sala_bagagem' => 'Sala de bagagem',
			'opcionais_sala_ginastica' => 'Sala de ginástica',
			'opcionais_sala_numerada' => 'Sala numerada',
			'opcionais_servico_quarto' => 'Serviço de quarto',
			'opcionais_servico_transfer' => 'Serviço de transfer',
			'opcionais_telefone_quarto' => 'Telefone no quarto',
			'opcionais_tv_cabo' => 'TV a cabo',
			'opcionais_wifi' => 'Wi-fi'
		);
	}

	function arrayPagamentos(){
		return array(
			'pagamento_amex' => 'Amex',
			'pagamento_cheque' => 'Cheque',
			'pagamento_coopercred' => 'Coopercred',
			'pagamento_diners' => 'Diners',
			'pagamento_hipercard' => 'Hipercard',
			'pagamento_maestro' => 'Maestro',
			'pagamento_master' => 'Master',
			'pagamento_visa' => 'Visa',
			'pagamento_ticket' => 'Ticket',
			'pagamento_vale_refeicao' => 'Vale Refeição',
			'pagamento_visa_electron' => 'Visa Electron'
		);
	}

	function arrayCozinhas(){
		return array(
			'cozinha_alema' => 'Alemã',
			'cozinha_arabe' => 'Arabe',
			'cozinha_baiana' => 'Baiana',
			'cozinha_brasileira' => 'Brasileira',
			'cozinha_hotdog' => 'Hotdog',
			'cozinha_cafe_cha' => 'Café/Chá',
			'cozinha_carnes' => 'Carnes',
			'cozinha_chinesa' => 'Chinesa',
			'cozinha_internacional' => 'Internacional',
			'cozinha_doces' => 'Doces',
			'cozinha_fastfood' => 'Fast Food',
			'cozinha_japonesa' => 'Japonesa',
			'cozinha_italiana' => 'Italiana',
			'cozinha_mexicana' => 'Mexicana',
			'cozinha_mineira' => 'Mineira',
			'cozinha_paes' => 'Pães',
			'cozinha_pamonhas' => 'Pamonhas',
			'cozinha_pasteis' => 'Pastéis',
			'cozinha_peixes' => 'Peixes',
			'cozinha_pizzas' => 'Pizzas',
			'cozinha_portuguesa' => 'Portuguesa',
			'cozinha_prato_feito' => 'Prato Feito',
			'cozinha_quilo' => 'Por Quilo',
			'cozinha_salgados' => 'Salgador',
			'cozinha_sorvetes' => 'Sorvetes',
			'cozinha_sucos' => 'Sucos',
			'cozinha_vegetariana' => 'Vegerariana'
		);
	}

	function arrayDias(){
		return array(
			'funcionamento_segunda' => 'Segunda-Feira',
			'funcionamento_terca' => 'Terça-Feira',
			'funcionamento_quarta' => 'Quarta-Feira',
			'funcionamento_quinta' => 'Quinta-Feira',
			'funcionamento_sexta' => 'Sexta-Feira',
			'funcionamento_sabado' => 'Sábado',
			'funcionamento_domingo' => 'Domingo'
		);
	}
}
?>
