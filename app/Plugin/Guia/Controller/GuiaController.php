<?php
App::uses('GuiaAppController', 'Guia.Controller');

class GuiaController extends GuiaAppController {
	public $name = 'Guia';
	public $uses = array('Guia.Guia');
	public $components = array('Guia.Guiajornal');

	public function beforeFilter() 
	{
		parent::beforeFilter();
		//$this->set('arrayPrecoMedio', $this->Guia->arrayPrecoMedio());
	}

	public function admin_config() 
	{
		
	}
	
	public function filtro() 
	{		
		$registros = $this->Guia->find(
			'all',
			array(
				'conditions' => array(
					'Guia.termo' => $this->params['named']['termo']
				),
				'recursive' => -1
			)
		);

		$filtros = array();
		$filtros['localidade'] = array();
		$filtros['precos'] = array();
		$filtros['cozinhas'] = array();
		$filtros['opcionais'] = array();
		$filtros['pagamentos'] = array();	
		$filtros['dias'] = array();	
		$filtros['balada_genero'] = array();			

		if(!empty($registros)){
			foreach($registros as $registro){

				/*****MONTA O ARRAY DE LOCALIDADES*****/
				
				//pega o nome da cidade
				$cidade = $registro['Guia']['cidade'];
				//se ainda não tem esta cidade no array
				if(!isset($filtros['localidade'][$cidade])){
					$filtros['localidade'][$cidade] = array();
				}
				//pega o nome da bairro
				$bairro = $registro['Guia']['bairro'];
				//se ainda não tem esta bairro no array
				if(!isset($filtros['localidade'][$cidade][$bairro])){
					$filtros['localidade'][$cidade][$bairro] = array();
				}
				$filtros['localidade'][$cidade][$bairro][] = $registro['Guia']['node_id'];

				/*****MONTA O ARRAY DE PREÇO MEDIO*****/
				
				//pega o preço medio
				$preco_medio = $registro['Guia']['preco_medio'];				
				if(!empty($preco_medio)){
					//se ainda não tem esta cidade no array
					if(!isset($filtros['precos'][$preco_medio])){
						$filtros['precos'][$preco_medio] = array();
					}
					$filtros['precos'][$preco_medio][] = $registro['Guia']['node_id'];
				}

				/*****MONTA O ARRAY DE GENERO MUSICAL*****/
				
				$balada_genero = $registro['Guia']['balada_genero'];				
				if(!empty($balada_genero)){
					//se ainda não tem esta cidade no array
					if(!isset($filtros['balada_genero'][$balada_genero])){
						$filtros['balada_genero'][$balada_genero] = array();
					}
					$filtros['balada_genero'][$balada_genero][] = $registro['Guia']['node_id'];
				}
				
				/*****MONTA O ARRAY DE COZINHA*****/
				
				//pega o cozinha
				$arrayCozinhas = $this->Guiajornal->arrayCozinhas();
				//percorre as cozinhas
				foreach($arrayCozinhas as $key => $cozinha){
					//se existe este registro e selecionado
					if(isset($registro['Guia'][$key]) && $registro['Guia'][$key]){
						//se ainda não tem esta cidade no array
						if(!isset($filtros['cozinhas'][$key])){
							$filtros['cozinhas'][$key] = array();							
						}
						$filtros['cozinhas'][$key][] = $registro['Guia']['node_id'];
					}
				}

				/*****MONTA O ARRAY DE OPCIONAIS*****/
				
				//pega o cozinha
				$arrayOpcionais = $this->Guiajornal->arrayOpcionais();
				//percorre as cozinhas
				foreach($arrayOpcionais as $key => $opcional){
					//se existe este registro e selecionado
					if(isset($registro['Guia'][$key]) && $registro['Guia'][$key]){
						//se ainda não tem esta cidade no array
						if(!isset($filtros['opcionais'][$key])){
							$filtros['opcionais'][$key] = array();							
						}
						$filtros['opcionais'][$key][] = $registro['Guia']['node_id'];
					}
				}

				/*****MONTA O ARRAY DE PAGAMENTOS*****/
				
				//pega o cozinha
				$arrayPagamentos = $this->Guiajornal->arrayPagamentos();
				//percorre as cozinhas
				foreach($arrayPagamentos as $key => $pagamento){
					//se existe este registro e selecionado
					if(isset($registro['Guia'][$key]) && $registro['Guia'][$key]){
						//se ainda não tem esta cidade no array
						if(!isset($filtros['pagamentos'][$key])){
							$filtros['pagamentos'][$key] = array();							
						}
						$filtros['pagamentos'][$key][] = $registro['Guia']['node_id'];
					}
				}

				/*****MONTA O ARRAY DE DIAS*****/
				
				//pega o cozinha
				$arrayDias = $this->Guiajornal->arrayDias();
				//percorre as cozinhas
				foreach($arrayDias as $key => $dia){
					//se existe este registro e selecionado
					if(isset($registro['Guia'][$key]) && $registro['Guia'][$key]){
						//se ainda não tem esta cidade no array
						if(!isset($filtros['dias'][$key])){
							$filtros['dias'][$key] = array();							
						}
						$filtros['dias'][$key][] = $registro['Guia']['node_id'];
					}
				}				
			}	
		}

		//Envia as variaveis para a view
		$this->set('filtros',$filtros);
		$this->set('arrayCozinhas',$arrayCozinhas);
		$this->set('arrayOpcionais',$arrayOpcionais);
		$this->set('arrayPagamentos',$arrayPagamentos);
		$this->set('arrayDias',$arrayDias);
		$this->set('arrayPrecoMedio',$this->Guiajornal->arrayPrecoMedio());
		$this->set('arrayGeneroMusical',$this->Guiajornal->arrayGeneroMusical());		

		//renderiza
		$this->render('filtro','ajax');

    }

}