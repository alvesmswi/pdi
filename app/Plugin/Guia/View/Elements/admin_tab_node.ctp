<?php 

echo $this->Html->css('Guia.guia');
App::uses('GuiajornalComponent', 'Guia.Controller/Component');
$Guia = new GuiajornalComponent();
?>
<div id="guia" class="row-fluid"> 
	<?php 
	echo $this->Form->hidden('Guia.id');
	echo $this->Form->input(
		'Guia.endereco',
		array(
			'type' => 'text',
			'label' => 'Endereço',
			'class' => 'span12',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.cidade',
		array(
			'type' => 'text',
			'label' => 'Cidade',
			'class' => 'span6',
			'div' => false,
			'required' => true
		)
	);
	echo $this->Form->input(
		'Guia.bairro',
		array(
			'type' => 'text',
			'label' => 'Bairro',
			'class' => 'span6',
			'div' => false,
			'required' => true
		)
	);
	echo $this->Form->input(
		'Guia.responsavel',
		array(
			'type' => 'text',
			'label' => 'Responsável',
			'class' => 'span6',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.telefone',
		array(
			'type' => 'text',
			'label' => 'Telefone',
			'class' => 'span6 celular',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.email',
		array(
			'type' => 'email',
			'label' => 'E-mail',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.site',
		array(
			'type' => 'text',
			'label' => 'Website / Link',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.facebook',
		array(
			'type' => 'text',
			'label' => 'Facebook',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.capacidade',
		array(
			'type' => 'number',
			'label' => 'Capacidade (número de pessoas)',
			'class' => 'span6',
			'div' => false
		)
	);
	echo $this->Form->input(
		'Guia.estacionamento',
		array(
			'type' => 'number',
			'label' => 'Estacionamento / Vagas',
			'class' => 'span6',
			'div' => false
		)
	);	
	echo $this->Form->input(
		'Guia.preco_medio',
		array(
			'options' => $Guia->arrayPrecoMedio(),
			'label' => 'Preço médio (Reais)',
			'class' => 'span6',
			'div' => false,
			'empty' => true
		)
	);
	?>
	
	<h4>Show e Balada</h4>
	<?php 
	echo $this->Form->input(
		'Guia.balada_show',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Show',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.balada_balada',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Balada',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.balada_taxa_manobrista',
		array(
			'type' 	=> 'text',
			'label' => 'Taxa Manobrista:',
			'class' => 'span6 ',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.balada_valor_camarote',
		array(
			'type' 	=> 'text',
			'label' => 'Camarote:',
			'class' => 'span12',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.balada_genero',
		array(
			'options' => $Guia->arrayGeneroMusical(),
			'label' => 'Genero',
			'class' => 'span6',
			'empty' => true,
			'div' 	=> false
		)
	);
	?>
	<h4>Arte e Espetaculo</h4>
	<?php 
	echo $this->Form->input(
		'Guia.arte_arte',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Arte',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.arte_espetaculo',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Espetaculo',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.arte_teatro',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Teatro',
			'div' 	=> true
		)
	);
	?>
	<h4>Bar e Restaurante</h4>
	<?php 
	echo $this->Form->input(
		'Guia.restaurante_bar',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Bar',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.restaurante_restaurante',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Restaurante',
			'div' 	=> true
		)
	);
	?>
	<h4>Cinema</h4>
	<?php 
	echo $this->Form->input(
		'Guia.cinema_vendas_online',
		array(
			'type' 	=> 'text',
			'label' => 'Vendas Online (Link):',
			'class' => 'span12',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.cinema_salas_normais',
		array(
			'type' 	=> 'number',
			'label' => 'Quantidade salas normais:',
			'class' => 'span6',
			'div'	=> false
		)
	);
	echo $this->Form->input(
		'Guia.cinema_salas_2d',
		array(
			'type' 	=> 'number',
			'label' => 'Quantidade salas 2D:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.cinema_salas_3d',
		array(
			'type' 	=> 'number',
			'label' => 'Quantidade salas 3D:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.cinema_salas_numeradas',
		array(
			'type' 	=> 'number',
			'label' => 'Salas numeradas:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	?>
	<h4>Hotel e Motel</h4>
	<?php
	echo $this->Form->input(
		'Guia.hotel_motel',
		array(
			'type' 	=> 'checkbox',
			'label' => 'Motel?',
			'div' 	=> true
		)
	);
	echo $this->Form->input(
		'Guia.hotel_cafe',
		array(
			'type' 	=> 'text',
			'label' => 'Café da manhã (valor)',
			'class' => 'span6 ',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.hotel_almoco',
		array(
			'type' 	=> 'text',
			'label' => 'Almoço (valor)',
			'class' => 'span6 ',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.hotel_janta',
		array(
			'type' 	=> 'text',
			'label' => 'Jantar (valor)',
			'class' => 'span6 ',
			'div' 	=> false
		)
	);
	?>
	<h4>Ambiente</h4>
	<?php
	echo $this->Form->input(
		'Guia.ambiente_poltronas_obesos',
		array(
			'type' 	=> 'number',
			'label' => 'Poltronas para obesos:',
			'class' => 'span6',
			'div'	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_espaco_cadeirantes',
		array(
			'type' 	=> 'number',
			'label' => 'Espaço para cadeirantes:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_camarins',
		array(
			'type' 	=> 'number',
			'label' => 'Quantidade de camarins:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_banheiros',
		array(
			'type' 	=> 'number',
			'label' => 'Quantidade de banheiros:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_valet_parking',
		array(
			'type' 	=> 'text',
			'label' => 'Valet parking:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_capacidade_interna',
		array(
			'type' 	=> 'number',
			'label' => 'Capacidade Interna:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.ambiente_capacidade_externa',
		array(
			'type' 	=> 'number',
			'label' => 'Capacidade Externa:',
			'class' => 'span6',
			'div' 	=> false
		)
	);
	?>
	<h4>Serviços</h4>
	<?php
	echo $this->Form->input(
		'Guia.servicos_reservas_ate',
		array(
			'type' 	=> 'text',
			'label' => 'Reserva até que horas:',
			'class' => 'span6 hora',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.servicos_taxa_entrega',
		array(
			'type' 	=> 'text',
			'label' => 'Taxa de entrega:',
			'class' => 'span6 ',
			'div' 	=> false
		)
	);
	?>
	<h4>Funcionamento</h4>
	<?php
	$dias = $Guia->arrayDias();
	foreach($dias as $key => $dia){
		echo $this->Form->input(
			'Guia.'.$key,
			array(
				'type' 	=> 'checkbox',
				'label' => $dia,
				'div' 	=> true
			)
		);
	}
	echo $this->Form->input(
		'Guia.funcionamento_abertura',
		array(
			'type' 	=> 'text',
			'label' => 'Horário de abertura:',
			'class' => 'span6 hora',
			'div' 	=> false
		)
	);
	echo $this->Form->input(
		'Guia.funcionamento_fechamento',
		array(
			'type' 	=> 'text',
			'label' => 'Horário de fechamento:',
			'class' => 'span6 hora',
			'div' 	=> false
		)
	);
	?>

	<h4>Opcionais</h4>
	<?php
	$opcionais = $Guia->arrayOpcionais();
	foreach($opcionais as $key => $opcional){
		echo $this->Form->input(
			'Guia.'.$key,
			array(
				'type' 	=> 'checkbox',
				'label' => $opcional,
				'div' 	=> true
			)
		);
	}
	?>

	<h4>Pagamento</h4>
	<?php
	$pagamentos = $Guia->arrayPagamentos();
	foreach($pagamentos as $key => $pagamento){
		echo $this->Form->input(
			'Guia.'.$key,
			array(
				'type' 	=> 'checkbox',
				'label' => $pagamento,
				'div' 	=> true
			)
		);
	}
	?>

	<h4>Cozinha</h4>
	<?php
	$cozinhas = $Guia->arrayCozinhas();
	foreach($cozinhas as $key => $cozinha){
		echo $this->Form->input(
			'Guia.'.$key,
			array(
				'type' 	=> 'checkbox',
				'label' => $cozinha,
				'div' 	=> true
			)
		);
	}
	?>
</div>
<?php 
echo $this->Html->script(
	array(
		'Guia.jquery.number.min',
		'Guia.jquery.maskedinput.min',
		'Guia.guia'
	)
);
?>