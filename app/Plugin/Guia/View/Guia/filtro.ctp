<?php echo $this->Html->css('Guia.guia');?>

<div class="o-poll" id="filtros">
    <fieldset class="o-poll_fieldset">
        <legend class="o-poll_title">Localidade</legend>
        <div class="o-poll_options">
            <ul>
            <?php 
            $termo = '';
            //se tem termo
            if(isset($this->params['named']['termo']) && !empty($this->params['named']['termo'])){
                $termo = $this->params['named']['termo'];
            }
            //se tem filtro por cidades
            if(!empty($filtros['localidade'])){
                //percorre as cidades
                foreach($filtros['localidade'] as $cidade => $cidades){
                    //monta o filtro
                    $args = '?cidade='.$cidade;
                    //monta o link
                    echo '<li class="bold"><a href="/guia/term/'.$termo.$args.'">'.$cidade.'</a></li>';
                    echo '<ul>';
                    //percorre os bairros
                    foreach($cidades as $bairro => $b){
                        //monta o filtro
                        $args = '?cidade='.$cidade.'&bairro='.$bairro;
                        //monta o link
                        echo '<li><a href="/guia/term/'.$termo.$args.'">'.$bairro.' ('.count($b).')</a></li>';
                    } 
                    echo '</ul>';       
                }
            }
            ?>
            </ul>
        </div>
    </fieldset>

    <?php 
    //se tem filtro por Preços
    if(!empty($filtros['precos'])){
    ?>
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Preço Médio</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['precos'] as $id => $p){
                    //monta o filtro
                    $args = '?preco_medio='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayPrecoMedio[$id].' ('.count($filtros['precos'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>
    
    <?php 
    //se tem filtro por cozinhas
    if(!empty($filtros['cozinhas'])){
    ?>
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Cozinhas</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['cozinhas'] as $id => $p){
                    //monta o filtro
                    $args = '?cozinhaId='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayCozinhas[$id].' ('.count($filtros['cozinhas'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>
    
    <?php 
    //se tem filtro por opcionais
    if(!empty($filtros['opcionais'])){
    ?>
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Opcionais</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['opcionais'] as $id => $p){
                    //monta o filtro
                    $args = '?opcionalId='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayOpcionais[$id].' ('.count($filtros['opcionais'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>

    <?php 
    //se tem filtro por pagamentos
    if(!empty($filtros['pagamentos'])){
    ?>
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Pagamento</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['pagamentos'] as $id => $p){
                    //monta o filtro
                    $args = '?pagamentoId='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayPagamentos[$id].' ('.count($filtros['pagamentos'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>
    
    <?php 
    //se tem filtro por dias
    if(!empty($filtros['dias'])){
    ?>    
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Dias</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['dias'] as $id => $p){
                    //monta o filtro
                    $args = '?diaId='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayDias[$id].' ('.count($filtros['dias'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>
    
    <?php 
    //se tem filtro por balada_genero
    if(!empty($filtros['balada_genero'])){
    ?> 
        <fieldset class="o-poll_fieldset">
            <legend class="o-poll_title">Gênero Musical</legend>
            <div class="o-poll_options">
                <ul>
                <?php
                //percorre as cidades
                foreach($filtros['balada_genero'] as $id => $p){
                    //monta o filtro
                    $args = '?balada_genero='.$id;
                    //monta o link
                    echo '<li><a href="/guia/term/'.$termo.$args.'">'.$arrayGeneroMusical[$id].' ('.count($filtros['balada_genero'][$id]).')</a></li>';     
                }
                ?>
                </ul>
            </div>
        </fieldset>
    <?php } ?>
</div>