<?php
class GuiaActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeActivation(&$controller) {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {
		Configure::write('debug',1);

		//Cria as tabelas
		CakePlugin::load('Guia');
		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		include_once(App::pluginPath('Guia').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$GuiaSchema = new GuiaSchema();
		foreach ($GuiaSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($GuiaSchema, $table));
			}
		}

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('Guia/Guia/admin_config');
		$controller->Croogo->addAco('Guia/Guia/filtro', array('public'));

		//Configurações basicas
		//$controller->Setting->write('Guia.email','alves@mswi.com.br',array('description' => 'E-mail cadastrado no Guia','editable' => 1));

		Cache::clear(false, '_cake_model_');
	}

/**
 * onDeactivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

/**
 * Called after deactivating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onDeactivation(&$controller) {
		// ACL: remove ACOs with permissions
		$controller->Croogo->removeAco('Guia'); // GuiaController ACO and it's actions will be removed
	}
}
