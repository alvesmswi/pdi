<?php
Croogo::hookAdminTab('Nodes/admin_edit', 'Guia', 'guia.admin_tab_node', array('type' => array('guia')));
Croogo::hookAdminTab('Nodes/admin_add', 'Guia', 'guia.admin_tab_node', array('type' => array('guia')));

Croogo::hookBehavior('Node', 'Guia.Guia', array());

Croogo::hookModelProperty('Node', 'hasOne', array('Guia' => array(
    'className' => 'Guia',
    'foreignKey' => 'node_id',
    'dependent' => false
)));