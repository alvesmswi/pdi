<?php

class GuiaBehavior extends ModelBehavior {

	public function setup(Model $model, $config = array()) {
		if (is_string($config)) {
			$config = array($config);
		}

		$this->settings[$model->alias] = $config;

		$callback = array($this, 'onBeforeSaveNode');
		$eventManager = $model->getEventManager();
		$eventManager->attach($callback, 'Model.Node.beforeSaveNode');
	}

	public function onBeforeSaveNode($event) {
		if(isset($event->data['data']['TaxonomyData'])){
			$event->data['data']['Guia']['termo'] = $this->getTermByTax($event->data['data']['TaxonomyData']);
		}		
		return true;
	}

	private function getTermByTax($TaxonomyData) 
	{
		if(!empty($TaxonomyData)){
			App::uses('Taxonomy','Model');
			$Taxonomy = new Taxonomy();

			foreach($TaxonomyData as $vocId => $taxId){
				$tax = $Taxonomy->find(
					'first',
					array(
						'conditions' => array(
							'Taxonomy.id' => $taxId
						),
						'contain' => array('Term'),
						'fields' => array(
							'Term.slug'
						)
						//'recursive' => -1
					)
				);
				if(isset($tax['Term']['slug']) && !empty($tax['Term']['slug'])){
					return $tax['Term']['slug'];
				}else{
					return true;
				}
			}
		}else{
			return true;
		}		
	}

}
