<?php 
echo $this->Html->css('Paywall.paywall');
$banner = Configure::read('Paywall.banner');
if(!empty($banner)){
    echo $this->Html->image(Router::url('/', true).$banner);
}
?>
<div class="row margin-top margin-bottom">
    <div class="col-sm-6 border-right">
        <div class="o-login_content">
            <header class="o-header">
                <span class="o-header_title o-header_title--large">Criar sua conta</span>
            </header>
            <?php 
            // echo $this->Form->create('User', array('url' => array('plugin' => 'users', 'controller' => 'users', 'action' => 'add')));
            echo $this->Form->create();
            //se tem redirecionamento
            if(isset($this->params->query['redirect']) && !empty($this->params->query['redirect'])){
                $redirect = $this->params->query['redirect'];
                echo $this->Form->hidden('redirect', array('value'=>$redirect));
            }
            ?>
            <div class="form-group">
                <?php 
                echo $this->Form->input('name', array('label'=>'Seu nome completo', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required')); 
                echo $this->Form->input('telefone', array('label'=>'Celular', 'class'=>'form-control celular', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('password', array('label'=>'Senha', 'type' => 'password', 'class'=>'form-control', 'value' => '', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('verify_password', array('label'=>'Confirme a Senha', 'type' => 'password', 'value' => '', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                ?>
            </div>           
            <div class="text-right"><button type="submit" class="btn btn-lg btn-primary">Enviar Cadastro</button></div>
            <?php echo $this->Form->end();?>   
        </div>
    </div>
    <div class="col-sm-6">
        <div class="o-login_content">
            <header class="o-header">
                <span class="o-header_title o-header_title--large">Já tenho conta</span>
            </header>
            <?php 
            echo $this->Form->create('User', array('url' => array('plugin' => 'users', 'controller' => 'users', 'action' => 'login')));
            //se tem redirecionamento
            if(isset($this->params->query['redirect']) && !empty($this->params->query['redirect'])){
                $redirect = $this->params->query['redirect'];
                echo $this->Form->hidden('redirect', array('value'=>$redirect));
            }
            ?>
            <div class="form-group">
                <?php 
                echo $this->Form->input(Configure::read('User.campo_usuario'), array('label' => Configure::read('User.label_usuario'), 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                echo $this->Form->input('password', array('label' =>'Senha', 'class'=>'form-control', 'div'=>'form-group input-medium', 'required'=>'required'));
                ?>
            </div>
            <div class="col-xs-6"><a href="/recuperar-senha" class="btn-link">Esqueci minha senha</a></div>
            <div class="col-xs-6 text-right"><button type="submit" class="btn btn-lg btn-primary">Entrar</button></div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script(array(
        'Pagseguro.jquery.min', 
        'Pagseguro.jquery.maskedinput.min',
    ));
?>
<script>
    $(".celular").mask("(99)99999-9999", { placeholder: "(__)_____-____" });
</script>