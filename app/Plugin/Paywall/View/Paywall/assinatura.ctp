<?php
    $siteLogo = Configure::read('Site.logo');
    if(!empty($siteLogo)){
        $themeLogo = Router::url('/', true).$siteLogo;
    }else{								
        //Caminho absoluto do arquivo de CSS customizado
        $themeLogo = WWW_ROOT . 'img' . DS . 'logo.png';
        //se o arquivo de CSS personalizado existe
        if(file_exists($themeLogo)){
            $themeLogo = '/img/logo.png';
        }else{
            $themeLogo = '/theme/' . Configure::read('Site.theme').'/img/logo.png';
        }
    }
?>

<!doctype html>
<html class="no-js" lang="pt-BR">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo $this->fetch('meta');?>

		<title>
			<?php echo Configure::read('Site.title'); ?> |
			Assinatura Digital
		</title>

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <?php
		//Se foi inofmrado o user do analytics
		if (!empty(Configure::read('Service.analytics'))) {
			$arrayAnalytics = explode(',', Configure::read('Service.analytics')); ?>
			<script>
				(function (i, s, o, g, r, a, m) {
					i['GoogleAnalyticsObject'] = r;
					i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
					a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a, m)
				})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

				<?php foreach ($arrayAnalytics as $key => $analytics) { ?>
				ga('create', '<?= trim($analytics) ?>', 'auto', {
					'name': 'id<?= $key ?>'
				});
				ga('id<?= $key ?>.send', 'pageview');
				<?php } ?>

				/**
					* Function that tracks a click on an outbound link in Analytics.
					* This function takes a valid URL string as an argument, and uses that URL string
					* as the event label. Setting the transport method to 'beacon' lets the hit be sent
					* using 'navigator.sendBeacon' in browser that support it.
					*/
				var trackOutboundLink = function (url) {
					ga('send', 'event', 'outbound', 'click', url, {
						'transport': 'beacon',
						'hitCallback': function () {
							document.location = url;
						}
					});
				}
			</script>
			<?php } ?>

			<script type='text/javascript'>
				var googletag = googletag || {};
				googletag.cmd = googletag.cmd || [];
				(function () {
					var gads = document.createElement('script');
					gads.async = true;
					gads.type = 'text/javascript';
					var useSSL = 'https:' == document.location.protocol;
					gads.src = (useSSL ? 'https:' : 'http:') +
						'//www.googletagservices.com/tag/js/gpt.js';
					var node = document.getElementsByTagName('script')[0];
					node.parentNode.insertBefore(gads, node);
				})();
			</script>

		<?php
		//Se foi informado o tempo de refresh para a Home
		if ($this->here == '/' && !empty(Configure::read('Site.refresh_home')) && is_numeric(Configure::read('Site.refresh_home'))) { ?>
			<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_home') ?>" />
		<?php }

		//Se foi informado o tempo de refresh para as internas
		if (		
		$this->here != '/' && 
		(isset($node['Node']['type']) && $node['Node']['type'] == 'noticia') && 
		!empty(Configure::read('Site.refresh_internas')) && 
		is_numeric(Configure::read('Site.refresh_internas'))) { 
		?>
		<meta http-equiv="refresh" content="<?php echo Configure::read('Site.refresh_internas') ?>" />

		<?php }
			echo Configure::read('Service.header')
		?>

		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

		<?php
		//Se tem algum bloco ativo nesta região
		if($this->Regions->blocks('header_scripts')){
			echo $this->Regions->blocks('header_scripts');
		}
		?>

	</head>

    <body class="loadingInProgress" data-disable-copy>
        <style>
            * {
                margin: 0;
            }

            body {
                -webkit-font-smoothing: antialiased;
            }

            section {
                background: <?php echo Configure::read('Assinatura.cor1'); ?>;
                color: #fff;
                padding: 2em 0 2em;
                position: relative;
                -webkit-font-smoothing: antialiased;
            }

            .logo {
                margin: 0 auto;
                max-width: 100%;
                display: block;
                max-width: 300px;
            }

            .bg-dark {
                background: <?php echo Configure::read('Assinatura.cor2'); ?>;
            }

            .pricing {
                display: -webkit-flex;
                display: flex;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-justify-content: center;
                justify-content: center;
                width: 100%;
                margin: 0 auto 3em;
            }

            .pricing-item {
                position: relative;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: column;
                flex-direction: column;
                -webkit-align-items: stretch;
                align-items: stretch;
                text-align: center;
                -webkit-flex: 0 1 330px;
                flex: 0 1 330px;
            }

            .pricing-action {
                color: inherit;
                border: none;
                background: none;
                text-decoration: none;
            }

            .pricing-action:focus {
                outline: none;
            }

            .pricing-feature-list {
                text-align: left;
            }

            .pricing-palden .pricing-item {
                font-family: "Open Sans", sans-serif;
                cursor: default;
                color: <?php echo Configure::read('Assinatura.cor2'); ?>;
                background: #fff;
                box-shadow: 0 0 10px rgba(46, 59, 125, 0.23);
                border-radius: 20px 20px 10px 10px;
                margin: 1em;
            }

            @media screen and (min-width: 66.25em) {
                .pricing-palden .pricing-item {
                    margin: 1em -0.5em;
                }

                .pricing-palden .pricing__item--featured {
                    margin: 0;
                    z-index: 10;
                    box-shadow: 0 0 20px rgba(46, 59, 125, 0.23);
                }
            }

            .pricing-palden .pricing-deco {
                border-radius: 10px 10px 0 0;
                background: <?php echo Configure::read('Assinatura.cor2'); ?>;
                padding: 4em 0 6em;
                position: relative;
            }

            .pricing-palden .pricing-deco-img {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                /* height: 160px; */
            }

            .pricing-palden .pricing-title {
                font-size: 0.75em;
                margin: 0;
                text-transform: uppercase;
                letter-spacing: 5px;
                color: #fff;
            }

            .pricing-palden .deco-layer {
                -webkit-transition: -webkit-transform 0.5s;
                transition: transform 0.5s;
            }

            .pricing-palden .pricing-item:hover .deco-layer--1 {
                -webkit-transform: translate3d(15px, 0, 0);
                transform: translate3d(15px, 0, 0);
            }

            .pricing-palden .pricing-item:hover .deco-layer--2 {
                -webkit-transform: translate3d(-15px, 0, 0);
                transform: translate3d(-15px, 0, 0);
            }

            .pricing-palden .icon {
                font-size: 2.5em;
            }

            .pricing-palden .pricing-price {
                font-size: 5em;
                font-weight: bold;
                padding: 0;
                color: #fff;
                margin: 0 0 0.25em 0;
                line-height: 0.75;
            }

            .pricing-palden .pricing-currency {
                font-size: 0.15em;
                vertical-align: top;
            }

            .pricing-palden .pricing-period {
                font-size: 0.15em;
                padding: 0 0 0 0.5em;
                font-style: italic;
            }

            .pricing-palden .pricing__sentence {
                font-weight: bold;
                margin: 0 0 1em 0;
                padding: 0 0 0.5em;
            }

            .pricing-palden .pricing-feature-list {
                margin: 0;
                padding: 0.25em 0 2.5em;
                list-style: none;
                text-align: center;
            }

            .pricing-palden .pricing-feature-div {
                margin: 0;
                padding: 0.25em 1.5em 2em;
            }

            .pricing-palden .pricing-feature-div p {
                margin-bottom: 1em;
            }

            .pricing-palden .pricing-feature {
                padding: 1em 0;
            }

            .pricing-palden .pricing-action {
                font-weight: bold;
                margin: auto 3em 2em 3em;
                padding: 1em 2em;
                color: #fff;
                border-radius: 30px;
                background: <?php echo Configure::read('Assinatura.cor2'); ?>;
                -webkit-transition: background-color 0.3s;
                transition: background-color 0.3s;
            }

            .pricing-palden .pricing-action:hover,
            .pricing-palden .pricing-action:focus {
                background-color: <?php echo Configure::read('Assinatura.cor3'); ?>;
                cursor: pointer;
            }

            .pricing-palden .pricing-item--featured .pricing-deco {
                padding: 5em 0 8.885em 0;
            }

            .footer {
                display: flex;
                justify-content: center;
                align-items: center;
                align-content: center;
                font-family: 'Montserrat', sans-serif;
                font-size: 0.75em;
                width: 70%;
                margin: 0 auto;
            }

            .footer img {
                max-width: 250px;
            }

            .footer p {
                margin: 0 0 0 80px;
            }

            @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
                .footer {
                    flex-wrap: wrap;
                    width: 100%;
                }

                .footer p {
                    margin: 5px 40px;
                }
            }
        </style>

        <section>
            <a href="/">
                <img src="<?php echo $themeLogo; ?>" class="logo" alt="<?php echo Configure::read('Site.title'); ?>">
            </a>
        </section>

        <section>
            <div class='pricing pricing-palden'>
                <?php 
                    $featured = null;
                    if (!empty(Configure::read('Assinatura.bloco1_valor')) && !empty(Configure::read('Assinatura.bloco3_valor'))) {
                        $featured = ' pricing__item--featured';
                    } 
                ?>
                <?php if (!empty(Configure::read('Assinatura.bloco1_valor'))): ?>
                    <div class='pricing-item'>
                        <div class='pricing-deco'>
                            <svg class='pricing-deco-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1'
                                preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve'
                                xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                                <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729&#x000A;	c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729&#x000A;	c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716&#x000A;	H42.401L43.415,98.342z'
                                    fill='#FFFFFF' opacity='0.7'></path>
                                <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428&#x000A;	c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z'
                                    fill='#FFFFFF'></path>
                            </svg>
                            <div class='pricing-price'>
                                <span class='pricing-currency'>R$</span><?php echo Configure::read('Assinatura.bloco1_valor'); ?>
                            </div>
                            <h3 class='pricing-title'><?php echo Configure::read('Assinatura.bloco1_nome'); ?></h3>
                        </div>
                        <div class="pricing-feature-div">
                            <?php echo Configure::read('Assinatura.bloco1_descricao'); ?>
                        </div>
                        <a href="<?php echo Configure::read('Assinatura.bloco1_link'); ?>" class='pricing-action'><?php echo Configure::read('Assinatura.bloco1_btn'); ?></a>
                    </div>
                <?php endif; ?>
                <?php if (!empty(Configure::read('Assinatura.bloco2_valor'))): ?>
                    <div class='pricing-item<?php echo $featured; ?>'>
                        <div class='pricing-deco'>
                            <svg class='pricing-deco-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1'
                                preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve'
                                xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                                <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729&#x000A;	c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729&#x000A;	c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716&#x000A;	H42.401L43.415,98.342z'
                                    fill='#FFFFFF' opacity='0.7'></path>
                                <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428&#x000A;	c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z'
                                    fill='#FFFFFF'></path>
                            </svg>
                            <div class='pricing-price'>
                                <span class='pricing-currency'>R$</span><?php echo Configure::read('Assinatura.bloco2_valor'); ?>
                            </div>
                            <h3 class='pricing-title'><?php echo Configure::read('Assinatura.bloco2_nome'); ?></h3>
                        </div>
                        <div class="pricing-feature-div">
                            <?php echo Configure::read('Assinatura.bloco2_descricao'); ?>
                        </div>
                        <a href="<?php echo Configure::read('Assinatura.bloco2_link'); ?>" class='pricing-action'><?php echo Configure::read('Assinatura.bloco2_btn'); ?></a>
                    </div>
                <?php endif; ?>
                <?php if (!empty(Configure::read('Assinatura.bloco3_valor'))): ?>
                    <div class='pricing-item'>
                        <div class='pricing-deco'>
                            <svg class='pricing-deco-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1'
                                preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve'
                                xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                                <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729&#x000A;	c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729&#x000A;	c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z'
                                    fill='#FFFFFF' opacity='0.6'></path>
                                <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716&#x000A;	H42.401L43.415,98.342z'
                                    fill='#FFFFFF' opacity='0.7'></path>
                                <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428&#x000A;	c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z'
                                    fill='#FFFFFF'></path>
                            </svg>
                            <div class='pricing-price'>
                                <span class='pricing-currency'>R$</span><?php echo Configure::read('Assinatura.bloco3_valor'); ?>
                            </div>
                            <h3 class='pricing-title'><?php echo Configure::read('Assinatura.bloco3_nome'); ?></h3>
                        </div>
                        <div class="pricing-feature-div">
                            <?php echo Configure::read('Assinatura.bloco3_descricao'); ?>
                        </div>
                        <a href="<?php echo Configure::read('Assinatura.bloco3_link'); ?>" class='pricing-action'><?php echo Configure::read('Assinatura.bloco3_btn'); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <section class="bg-dark">
            <div class="footer">
                <a href="/">
                    <img src="<?php echo $themeLogo; ?>" alt="<?php echo Configure::read('Site.title'); ?>">
                </a>
                <p>
                    <?php echo Configure::read('Assinatura.rodape'); ?>
                </p>
            </div>
        </section>
    </body>
</html>