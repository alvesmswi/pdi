<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Paywall', array(
    'admin' => true,
    'plugin' => 'paywall',
    'controller' => 'paywall',
    'action' => 'config',
  ));

$this->append('form-start', $this->Form->create('Paywall', array(
    'class' => 'protected-form',
    'enctype' => 'multipart/form-data'
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
       echo $this->Form->input('Paywall.PaywallAnonimo', array(
            'label' => 'Quantidade notícias que o usuário ANÔNIMO pode ver (0 = ilimitado)',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Paywall.PaywallCadastrado', array(
            'label' => 'Quantidade notícias que o usuário CADASTRADO pode ver (0 = ilimitado)',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Paywall.PaywallAssinante', array(
            'label' => 'Quantidade notícias que o usuário ASSINANTE pode ver (0 = ilimitado)',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Paywall.PaywallTempo', array(
            'label' => 'Depois de quantas horas o usuário vai poder ver novas notícias?',
            'type'  => 'number',
            'required'	=> true
        )) .
        $this->Form->input('Paywall.PaywallMensagem', array(
            'label' => 'Mensagem que será exibida quando exceder a quantidade permitidas de noticias lidas',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Paywall.paginaBloqueio', array(
            'label' => 'Página de bloqueio de materias',
            'type'  => 'text',
            'required'	=> true
        )) .                     
        $this->Form->input('Paywall.banner', array(
            'label' => 'Banner',
            'type' => 'file',
            'required' => false
        ));

        if(isset($this->data['Paywall']['banner']) && !empty($this->data['Paywall']['banner'])){
                echo $this->Form->input('Paywall.check', array(
                    'label' => 'Apagar Banner?',
                    'type'  => 'checkbox'
                ));
                echo '<img src="'.Router::url('/', true).$this->data['Paywall']['banner'].'" class="thumbnail" />';                
            }

        echo $this->Form->input('Paywall.PaywallPagamento', array(
            'label' => 'Para ser assinante é preciso pagar',
            'type'  => 'checkbox'
        ));

        echo $this->Form->input('Paywall.PaywallBloqueioParcial', array(
            'label' => 'Bloquear a matéria parcialmente',
            'type'  => 'checkbox'
        ));
        echo $this->Form->input('Paywall.PaywallQtdeCaracteres', array(
            'label' => 'Quantidade de Caracteres exibir na Notícia nloqueada parcialmente',
            'type'  => 'number'
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());