<?php
App::uses('PaywallAppController', 'Paywall.Controller');

class PaywallController extends PaywallAppController {

    public $name = 'Paywall';
    public $uses = array('Paywall.Paywall', 'Settings.Setting');   
    public $components = array('Cookie','Auth','Session'); 

    public function beforeFilter() {
        parent::beforeFilter();
        //desabilita o CSRF
        $this->Security->enabled = false;
        $this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Auth->allow('cadastro', 'assinatura');
    }

    public function admin_config()
    {
        $this->set('title_for_layout','Paywall');     
        if (!empty($this->request->data)) {
        // pr($this->data['Paywall']);exit;
        if(isset($this->request->data['Paywall']['banner']) && !empty($this->request->data['Paywall']['banner'])) {
            if($this->request->data['Paywall']['banner']['size'] == 0){
                $file = WWW_ROOT . Configure::read('Paywall.banner');
                if(file_exists($file)){                    
                    $this->request->data['Paywall']['banner'] = Configure::read('Paywall.banner');
                    if(isset($this->request->data['Paywall']['check']) && $this->request->data['Paywall']['check'] == 1){
                        $this->request->data['Paywall']['banner'] = '';    
                    }
                }else{
                    $this->request->data['Paywall']['banner'] = '';
                }
            }else{            
                $this->request->data['Paywall']['banner'] = 'img/'.$this->Paywall->upload($this->request->data['Paywall']['banner']);
            }
        }
        if($this->Setting->deleteAll(
            array('Setting.key LIKE' => 'Paywall.%')
            )){
                foreach($this->request->data['Paywall'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Paywall.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        } 

        //limpa o this->data
        $this->request->data = array();
        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Paywall.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $this->request->data['Paywall'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
    }

    function cadastro(){

        //se está logado
        if($this->Auth->user('id')){
            return $this->redirect($this->Auth->redirect());
        }
        
        $this->set('sidebar', 0);
        if($this->request->data){
            //Importa os models necessários
            $this->loadModel('User');
            $this->loadModel('Role');

            //procura o ID do Grupo assinante
            $grupos = $this->Role->find(
                'first',
                array(
                    'conditions' => array(
                        'alias' => 'assinante'
                    )
                )
            );
            
            //trata os dados
            $this->request->data['Paywall']['username'] = Inflector::slug(strtolower($this->request->data['Paywall']['email']));
            $this->request->data['Paywall']['role_id'] = $grupos['Role']['id'];
            $this->request->data['Paywall']['status'] = 1;

            //Salva o usuário            
            if($this->User->save($this->request->data['Paywall'])){
                $this->Flash->success('Usuário cadastrado com sucesso');
                //Se está logado
                if($this->Session->read('Auth.User.id')){
                    $this->Auth->logout();
                }
                //Pega os dados de login informados no momento do cadastro
                $email = $this->request->data['Paywall']['email'];
                $senha = $this->request->data['Paywall']['password'];
                //prepara os dados para fazer login
                $this->request->data = array();
                $this->request->data['User']['email']       = $email;
                $this->request->data['User']['password']    = $senha;
                //Faz o login
                $this->Auth->login();
                
                //Se o plugin está conigurado para gera fatura
                if(Configure::read('Paywall.PaywallPagamento')){
                    $this->redirect('/pagseguro/pagseguro/assinatura_pagamento');
                }else{
                    $this->redirect('/');
                } 
            }else{
                $errors = '';
                foreach ($this->User->validationErrors as $key => $value) {                    
                    $errors .= $value[0] . '</br>';
                }                
                $this->Flash->error('Corrija os seguintes erros: </br>'. $errors);
            }           
        }   
    }

    public function assinatura()
    {
        $this->layout = false;
    }
}
?>