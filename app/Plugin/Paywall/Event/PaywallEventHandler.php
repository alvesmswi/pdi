<?php

App::uses('CakeEventListener', 'Event');
//App::uses('PaywallUtility', 'Paywall.Lib');

class PaywallEventHandler implements CakeEventListener {

	public function implementedEvents() {
		return array(
			'Controller.Users.loginSuccessful' => array(
				'callable' => 'afterLogin',
			),
			'Controller.Users.adminLoginSuccessful' => array(
				'callable' => 'afterLogin',
			),
		);
	}

	public function afterLogin($event) {
		//Se o plugin está conigurado para gerar fatura
		if(Configure::read('Paywall.PaywallPagamento')){
			$usuarioLogado = AuthComponent::user();
			$userId = $usuarioLogado['id'];
			//Se está logado admin, colunista ou editor
			if(
				$usuarioLogado['Role']['alias'] == 'admin' ||
				$usuarioLogado['Role']['alias'] == 'editor' ||
				$usuarioLogado['Role']['alias'] == 'colunista' ||
				$usuarioLogado['Role']['alias'] == 'gerente' ||
				$usuarioLogado['Role']['alias'] == 'cortesia' || 
				$usuarioLogado['Role']['alias'] == 'funcionario' || 
				$usuarioLogado['Role']['alias'] == 'assinante-direto' ||
				$usuarioLogado['Role']['alias'] == 'blogueiro'
			){
				return true;
			}else{
				//Verifica o pagamento
				ClassRegistry::init('Pagseguro.Pagseguro');
				$Pagseguro = new Pagseguro();			
				//procura o status do pagamento do usuario logado
				$pagamento = $Pagseguro->query("SELECT status FROM pagseguros WHERE user_id = $userId;");
				//Verifica se o usuário tem que efetuar o pagamento
				if(empty($pagamento) || $pagamento[0]['pagseguros']['status'] <> 3){
					$status = 0;
					if(
						isset($pagamento[0]['pagseguros']['status']) && 
						($pagamento[0]['pagseguros']['status'] == 3 || $pagamento[0]['pagseguros']['status'] == 4)
					){
						$status = $pagamento[0]['pagseguros']['status'];
					}
					//funcao que gera a fatura
					header("Location: /pagseguro/pagseguro/assinatura_pagamento?status=$status");exit();
				}
			}			
		}
	}

}
