<?php
CroogoNav::add(
	'sidebar', 
	'settings.children.paywall', 
	array(
		//'icon' => 'link',
		'title' => 'Paywall',
		'url' => array(
			'admin' => true,
			'plugin' => 'paywall',
			'controller' => 'paywall',
			'action' => 'config',
		)
	)
);

CroogoNav::add(
	'sidebar', 
	'settings.children.assinatura', 
	array(
		'title' => 'Assinatura',
		'url' => array(
			'admin' => true,
			'plugin' => 'settings',
			'controller' => 'settings',
			'action' => 'prefix',
			'Assinatura'
		)
	)
);
