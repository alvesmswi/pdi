<?php
Croogo::hookRoutes('paywall');

CroogoNav::add('settings.children.paywall',array(
	'title' => 'Paywall',
	'url' => array('plugin' => 'paywall', 'controller' => 'paywall', 'action' => 'admin_config'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Paywall');
    
?>