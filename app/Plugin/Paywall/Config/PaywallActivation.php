<?php
class PaywallActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        //da permissões para acessar plugin
        $controller->Croogo->addAco('Paywall');
		$controller->Croogo->addAco('Paywall/Paywall/admin_config');
		//adiciona uma configuração basica 
		$controller->Setting->write('Paywall.PaywallAnonimo','5', array('title'=>'PaywallAnonimo'));
		$controller->Setting->write('Paywall.PaywallCadastrado','10', array('title'=>'PaywallCadastrado'));
		$controller->Setting->write('Paywall.PaywallAssinante','0', array('title'=>'PaywallAssinante'));//ilimitado
		$controller->Setting->write('Paywall.PaywallTempo','24', array('title'=>'PaywallTempo'));
		$controller->Setting->write('Paywall.PaywallMensagem','Atenção! Você já atingiu o limite diário de visualização de notícias. Torne-se um assinante!', array('title'=>'PaywallMensagem'));
		$controller->Setting->write('Paywall.paginaBloqueio','/users/users/login', array('title'=>'Página de bloqueio de materias'));
		$controller->Setting->write('Paywall.PaywallPagamento','0', array('title'=>'PaywallPagamento'));
		$controller->Setting->write('Paywall.PaywallBloqueioParcial','0', array('title'=>'PaywallBloqueioParcial'));
		$controller->Setting->write('Paywall.PaywallQtdeCaracteres','500', array('title'=>'PaywallQtdeCaracteres'));
		//adiciona as permissões
		$controller->Croogo->addAco('Paywall/Paywall/cadastro', array('registrado', 'publico', 'assinante'));

		//adiciona configuração assinatura 
		$controller->Setting->write('Assinatura.rodape', 'A compra da assinatura digital dá direito a acessar todos conteúdos restritos do Jornal pelo tempo de vigência. É reservado o direito de cancelamento da compra em um prazo de 3 dias. A devolução do dinheiro ou estorno do pagamento é responsabilidade do Pagseguro e da operadora de cartão, ou depósito em caso de pagamento via boleto.', array('title' => 'Rodapé'));
		$controller->Setting->write('Assinatura.cor1', '#3a7fc1', array('title' => 'Cor (Seção)'));
		$controller->Setting->write('Assinatura.cor2', '#224b73', array('title' => 'Cor (Dark)'));
		$controller->Setting->write('Assinatura.cor3', 'rgb(25, 57, 90)', array('title' => 'Cor (Hover)'));

		$controller->Setting->write('Assinatura.bloco1_valor', '9,90', array('title' => 'Bloco 1 - Valor'));
		$controller->Setting->write('Assinatura.bloco1_nome', '1 Mês', array('title' => 'Bloco 1 - Nome'));
		$controller->Setting->write('Assinatura.bloco1_descricao', '<p>Com a assinatura digital você tem acesso ilimitado a todo conteúdo produzido e também a versão digital do jornal impresso, no computador, tablet e celular, quando quiser.</p>', array('title' => 'Bloco 1 - Descrição'));
		$controller->Setting->write('Assinatura.bloco1_link', '#', array('title' => 'Bloco 1 - Link'));
		$controller->Setting->write('Assinatura.bloco1_btn', 'Assinar', array('title' => 'Bloco 1 - Botão'));

		$controller->Setting->write('Assinatura.bloco2_valor', '49,5', array('title' => 'Bloco 2 - Valor'));
		$controller->Setting->write('Assinatura.bloco2_nome', '6 Meses', array('title' => 'Bloco 2 - Nome'));
		$controller->Setting->write('Assinatura.bloco2_descricao', '<p>Com a assinatura digital você tem acesso ilimitado a todo conteúdo produzido e também a versão digital do jornal impresso, no computador, tablet e celular, quando quiser.</p>', array('title' => 'Bloco 2 - Descrição'));
		$controller->Setting->write('Assinatura.bloco2_link', '#', array('title' => 'Bloco 2 - Link'));
		$controller->Setting->write('Assinatura.bloco2_btn', 'Assinar', array('title' => 'Bloco 2 - Botão'));

		$controller->Setting->write('Assinatura.bloco3_valor', '99', array('title' => 'Bloco 3 - Valor'));
		$controller->Setting->write('Assinatura.bloco3_nome', '1 Ano', array('title' => 'Bloco 3 - Nome'));
		$controller->Setting->write('Assinatura.bloco3_descricao', '<p>Com a assinatura digital você tem acesso ilimitado a todo conteúdo produzido e também a versão digital do jornal impresso, no computador, tablet e celular, quando quiser.</p>', array('title' => 'Bloco 3 - Descrição'));
		$controller->Setting->write('Assinatura.bloco3_link', '#', array('title' => 'Bloco 3 - Link'));
		$controller->Setting->write('Assinatura.bloco3_btn', 'Assinar', array('title' => 'Bloco 3 - Botão'));

		//Limpa o cache
		Cache::clear(false, '_cake_model_');
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('paywall');
	}
}