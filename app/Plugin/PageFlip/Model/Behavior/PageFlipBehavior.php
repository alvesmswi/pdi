<?php

App::uses('ModelBehavior', 'Model');
App::uses('Pdf', 'PageFlip.Model');

class PageFlipBehavior extends ModelBehavior 
{
    /**
     * Setup
     *
     * @param Model $model
     * @param array $config
     * @return void
     */
    public function setup(Model $model, $config = array()) 
    {
		if (is_string($config)) {
			$config = array($config);
		}

		$this->settings[$model->alias] = $config;
    }

    /**
     * afterSave callback
     *
     * @param Model $model
     * @param boolean $created
     * @param array $options
     * @return array
     */
    public function afterSave(Model $model, $created, $options = array())
    {
        if ($created) {
            $Pdf = new Pdf();
                
            if (isset($model->data[$model->alias]['node_id_pdf']) && !empty($model->data[$model->alias]['node_id_pdf'])) {
                $node_id_pdf    = $model->data[$model->alias]['node_id_pdf'];
                $node_id        = $model->data[$model->alias]['id'];

                $Pdf->updateAll(
                    array('Pdf.node_id' => $node_id),
                    array('Pdf.node_id' => $node_id_pdf)
                );
            }
        }

        if (isset($model->data[$model->alias]['destaques_pdf'])) {
            $Pdf = new Pdf();

            $node_id    = $model->data[$model->alias]['id'];
            $destaques  = $model->data[$model->alias]['destaques_pdf'];

            if (!empty($destaques)) {
                $destaques_pdf  = array();
                $destaquesArray = explode(';', $destaques);
                foreach ($destaquesArray as $key => $destaque) {
                    if (!empty($destaque)) {
                        $destaquesArray2 = explode('-', $destaque);
                        $destaques_pdf[trim($destaquesArray2[0])] = trim($destaquesArray2[1]);
                    }
                }

                $destaques_salvar = json_encode($destaques_pdf, JSON_UNESCAPED_UNICODE);
            } else {
                $destaques_salvar = null;
            }
            
            $Pdf->updateAll(
                array('Pdf.destaques' => "'".$destaques_salvar."'"),
                array('Pdf.node_id' => $node_id)
            );
        }
    }

    /**
     * afterFind callback
     *
     * @param Model $model
     * @param array $results
     * @param boolean $primary
     * @return array
     */
    public function afterFind(Model $model, $results = array(), $primary = false) 
    {
		if ($primary && isset($results[0][$model->alias])) {
			foreach ($results as $i => $result) {
				$model->bindModel(
                    array('hasMany' => array(
                            'PageFlip' => array(
                                'className' 	=> 'PageFlip.Pdf',
                                'foreignKey' 	=> 'node_id',
                                'dependent' 	=> true,
                                'conditions' 	=> array('PageFlip.controller' => 'nodes')
                            )
                        )
                    )
                );
                
				$model->PageFlip->recursive = -1;
				if (isset($results[$i][$model->alias]['id'])) {
                    $pageflip = $model->PageFlip->find('first', array(
                        'conditions' => array(
                            'PageFlip.node_id' => $results[$i][$model->alias]['id']
                        )
                    ));

                    if (!empty($pageflip)) {
                        $results[$i]['PageFlip'] = $pageflip['PageFlip'];
                    }
                }
			}
        }

		return $results;
    }
}
