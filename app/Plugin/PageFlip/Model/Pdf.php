<?php
App::uses('AppModel', 'Model');

/**
 * Pdf
 *
 * @category Model
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class Pdf extends AppModel {
	public $name 		= 'Pdf';
	public $useTable 	= 'pageflip_pdfs';
	public $validate 	= array(
		'node_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.'
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]['pdf'])) {
				$results[$key][$this->alias]['url'] = '/pageflip/pdfs/normal/'.$value[$this->alias]['id'].'/'.$value[$this->alias]['pdf'];
			}
			if (isset($value[$this->alias]['images'])) {
				$images = json_decode($value[$this->alias]['images'], true);
				$results[$key][$this->alias]['capa'] = '/pageflip/pdfs/normal/'.$value[$this->alias]['id'].'/'.urlencode($images[0]['image']);
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		return true;
	}
}
