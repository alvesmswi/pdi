<?php
App::import('Vendor', 'PageFlip.ImageResize', array('file' => 'ImageResize.php'));

class PresetComponent extends Component 
{
	public $pathImages = null;

    public function image($filename, $pasta, $preset) 
    {
		$this->pathImages = WWW_ROOT.'files'.DS.'pageflip'.DS.$pasta.DS;
		// Pasta do Preset
		$presetDir = $this->pathImages . $preset;

		// Pega a imagem enviada
		$image = new ImageResize($this->pathImages . $filename);

		$arrayPreset = explode('x', $preset);

		if (isset($arrayPreset[0]) && !isset($arrayPreset[1])) {
			$image->resizeToWidth($arrayPreset[0]);
		} elseif (isset($arrayPreset[1])) {
			$image->crop($arrayPreset[0], $arrayPreset[1]);
		}

		// Se o diretorio ainda não existe
		if (!is_dir($presetDir)) {
			// Cria o diretorio
			mkdir($presetDir);
		}

		// Salva a imagem
		$image->save($presetDir.DS.$filename);

		return $presetDir.DS.$filename;
	}
}
