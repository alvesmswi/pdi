<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Sanitize', 'Utility');
App::uses('Inflector', 'Utility');
App::uses('Croogo', 'Lib');

/**
 * Pdfs Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class PdfsController extends AppController 
{
	public $name 			= 'Pdfs';
	public $pluginPrefix 	= "Pdfs";
	public $uses 			= array('PageFlip.Pdf');
	public $components 		= array('PageFlip.Preset');
	public $defaults 		= array();
	public $baseDir 		= null;
	public $path 			= null;

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

        $this->Security->unlockedActions += array('admin_node');
        $this->Auth->allow(array('display_file'));

        $this->baseDir 	=  WWW_ROOT.'files'.DS.'pageflip';
        $this->path 	=  DS.'files'.DS.'pageflip';
	}

	/**
	 * admin_node function
	 *
	 * @return void
	 */
	public function admin_node() 
	{
		$this->autoRender = false;

		// Se for o PDF enviado
		if (isset($this->request->form['pdf']) && !empty($this->request->form['pdf'])) {
			$pdf = $this->request->form['pdf']; // Pega o PDF enviado

			// Se não tiver nenhum erro
			if ($pdf['error'] === 0) {
				// Cria o array para salvar o PDF
				$pdf_save = array(
					'id' 		=> null,
					'node_id' 	=> $this->data['id'], // Ver um jeito para pegar para salvar no adicionar já
					'pdf' 		=> $pdf['name'],
					'images' 	=> null,
					'status' 	=> 1,
					'controller'=> $this->data['controller']
				);

				// Se salvar o PDF
				if ($this->Pdf->save($pdf_save)) {
					// Move o PDF para a pasta do CMS /public/files/pageflip
					$pdf_path 	= $this->path.DS.$this->Pdf->id;
					$pdf_dir 	= $this->baseDir.DS.$this->Pdf->id;

					$dir = new Folder($pdf_dir, true, 0755);

					$file_name 		= $pdf['name'];
					$destination 	= $pdf_path.DS.$file_name;
					$file 			= new File($pdf['tmp_name']);

					$file->copy($pdf_dir.DS.$file_name);
					$file->close();

					return json_encode(array('pdf_id' => $this->Pdf->id));
				}
			}
		// Se for as Imagens enviadas do PDF
		} elseif (isset($this->request->form['page']) && !empty($this->request->form['page'])) {
			// Pega o ID do PDF salvo anteriormente
			$pdf_id = (isset($this->data['pdf_id'])) ? $this->data['pdf_id'] : null;

			// Se o ID do PDF não for vazio
			if (!empty($pdf_id)) {
				// Busca a linha do PDF, Ver se é realmente necessário fazer essa verificação
				$pdf = $this->Pdf->find('first', array(
					'conditions' 	=> array('id' => $pdf_id),
					'fields' 		=> array('id', 'images'),
					'recursive' 	=> -1
				));

				// Se existir o PDF informado
				if (!empty($pdf)) {
					// Como ele manda em 20 em 20, tem que deixar para ele ir atualizando e não perder as imagens anteriores
					$imagens = json_decode($pdf['Pdf']['images']);

					// Percorre as Imagens informadas do PDF
					for ($i=0; $i < count($this->request->form['page']['name']); $i++) { 
						// Guarda o código do Erro da Imagem que está percorrendo
						$current_image_error = $this->request->form['page']['error'][$i];

						// Se não tiver nenhum Erro
						if ($current_image_error === 0) {
							// Guarda e Trata o nome da Imagem
							$current_image_name = $this->clean_name($this->request->form['page']['name'][$i]);
							// Guarda o caminho Temp da Imagem
							$current_image_tmp_name = $this->request->form['page']['tmp_name'][$i];

							// Guarda o nome da Imagem no array para Salvar
							$imagens[] = array(
								'image' => $current_image_name
							);

							// Copia a Imagem para a pasta do CMS /public/files/pageflip/{ID do PDF}/
							$image_path = $this->path.DS.$pdf['Pdf']['id'];
							$image_dir 	= $this->baseDir.DS.$pdf['Pdf']['id'];

							$dir = new Folder($image_dir, true, 0755);

							$file_name 		= $current_image_name;
							$destination 	= $image_path.DS.$file_name;
							$file 			= new File($current_image_tmp_name);

							$file->copy($image_dir.DS.$file_name);
							$file->close();
						}
					}
					
					// Atualiza o PDF com as Imagens informadas
					if ($this->Pdf->updateAll(
						array(
							'images' => "'".json_encode($imagens)."'"
						),
						array(
							'id' => $pdf['Pdf']['id']
						)
					)) {
						return json_encode("Feito!");
					}
				}
			}
		}
	}

	/**
	 * display_file function
	 *
	 * @param int $id
	 * @param string $filename
	 * @return void
	 */
	public function display_file($id, $filename, $preset = 'normal')
	{
		$this->autoRender = false;

		$cacheConfig = 'long_';
		$cacheName = 'pdf_images_'.$id.'_'.$preset.'_'.$filename;

		$image = Cache::read($cacheName, $cacheConfig);

		if (!$image) {
			if ($preset != 'normal') {
				$pathImage = $this->Preset->image($filename, $id, $preset);
			} else {
				$pathImage = $this->baseDir.DS.$id.DS.$filename;
			}

			if (file_exists($pathImage)) {
				$image = $pathImage;
				Cache::write($cacheName, $image, $cacheConfig);
			} else {
				throw new NotFoundException();
			}
		}

		//$this->response->type(mime_content_type($image));
		$this->response->sharable(true, 172800);
		$this->response->expires('+5 days');
		$this->response->cache('-1 minute', '+5 days');
		$this->response->file($image, array('download' => false, 'name' => $filename));
		$this->response->body($image);

		return $this->response;
	}

	/**
	 * admin_remove function
	 *
	 * @param int $id
	 * @param string $filename
	 * @return void
	 */
	public function admin_remove($id, $filename) 
	{
		$this->autoRender = false;

		if (!empty($id) && !empty($filename)) {
			if ($this->Pdf->delete($id)) {
				$path = $this->baseDir.DS.$id;

			    $folder = new Folder($path);
				$folder->delete();
				
				return $this->redirect($this->referer().'#node-pdf');
			}
		}

		return false;
	}

	/**
	 * clean_name function
	 *
	 * @param string $name
	 * @return string
	 */
	private function clean_name($name)
	{
		return preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $name);
	}
}
