$(document).ready(function () {
    var interval;

    function refresh() {
        var current = $('#currentPage').text();
        var total = $('#numberOfPages').text();

        if (current == total) {
            clearInterval(interval);
            
            $('#canvas-submit').trigger('click');
        }
    }

    $('#canvas-pdf').change(function () {
        interval =  setInterval(refresh, 1000); 
    });
});