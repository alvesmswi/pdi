$(document).ready(function() {
    const maxZoom = 1.5;
    const minZoom = 0.7;

    $('.pdf-viewer').slick({
        infinite: false,
        dots: true,
        variableWidth: true,
        centerMode: true,
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,

        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>',

        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    variableWidth: true,
                    centerMode: true
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    variableWidth: true,
                    centerMode: true
                }
            }
        ]
    });

    $('.lb-image').draggable({
        scroll: false
    });

    $('.lightboxOverlay').click(function() {
        $('.lb-image').css('top', '0');
        $('.lb-image').css('left', '0');
    });

    $('.pdf-page img').click(function(ev) {
        $('.lb-image').css('zoom', 1);
        $('.lb-image').css('top', 0);
        $('.lb-image').css('left', 0);

        // Adicionar botões ZoomIn, ZoomOut
        var btn_fit = '<button class="btn-zoom btn-zoom-fit"><i class="fas fa-compress fa-sm"></i></button>';
        var btn_zoom_in = '<button class="btn-zoom btn-zoom-in"><i class="fas fa-search-plus fa-sm"></i></button>';
        var btn_zoom_out = '<button class="btn-zoom btn-zoom-out"><i class="fas fa-search-minus fa-sm"></i></button>';
        var btns_zoom = '<div class="btns-zoom">' + btn_fit + btn_zoom_in + btn_zoom_out +'</div>';

        $('.lb-outerContainer').append(btns_zoom).ready(function() {
            $('.btn-zoom').bind('click', function() {
                var zoom = parseFloat($('.lb-image').css('zoom'));

                if (zoom < maxZoom) {
                    $('.btn-zoom-in').attr('disabled', false);
                } else {
                    $('.btn-zoom-in').attr('disabled', true);
                }

                if (zoom > minZoom) {
                    $('.btn-zoom-out').attr('disabled', false);
                } else {
                    $('.btn-zoom-out').attr('disabled', true);
                }

                if (zoom == 1) {
                    $('.btn-zoom-fit').attr('disabled', true);
                } else {
                    $('.btn-zoom-fit').attr('disabled', false);
                }
            });

            $('.btn-zoom-in').bind('click', function() {
                var zoom = parseFloat($('.lb-image').css('zoom'));

                if (zoom < maxZoom) {
                    var zoomIn = zoom + 0.1;

                    $('.lb-image').css('zoom', zoomIn);
                }
            });

            $('.btn-zoom-out').bind('click', function() {
                var zoom = parseFloat($('.lb-image').css('zoom'));

                if (zoom > minZoom) {
                    var zoomOut = zoom - 0.1;

                    $('.lb-image').css('zoom', zoomOut);
                }
            });

            $('.btn-zoom-fit').bind('click', function () {
                $('.lb-image').css('zoom', 1);
                $('.lb-image').css('top', 0);
                $('.lb-image').css('left', 0);
            });
        });
    });
    
    var editorias = { 1: 'Capa' };
    if ($('#destaques').val() != '') {
        editorias = JSON.parse($('#destaques').val());
    }
    
    $('.slick-dots').hide();
    $('.slick-dots li').each(function(i, val) {
        var li      = $(this);
        var button  = li.children();
        var page    = i + 1;

        if (page in editorias) {
            $('.slick-dots').show();
            button.text(page + ' ' + editorias[page]);
            li.show();
        } else {
            li.hide();
        }
    });

    $(window).on('resize', function() {
        $('.slick-dots').hide();
        $('.slick-dots li').each(function (i, val) {
            var li = $(this);
            var button = li.children();
            var page = i + 1;

            if (page in editorias) {
                $('.slick-dots').show();
                button.text(page + ' ' + editorias[page]);
                li.show();
            } else {
                li.hide();
            }
        });
    });

    // Zoom on Mouse Scroll
    $('.lb-image').bind('mousewheel', function(e) {
        var zoom = parseFloat($(this).css('zoom'));

        if (zoom < maxZoom) {
            $('.btn-zoom-in').attr('disabled', false);
        } else {
            $('.btn-zoom-in').attr('disabled', true);
        }

        if (zoom > minZoom) {
            $('.btn-zoom-out').attr('disabled', false);
        } else {
            $('.btn-zoom-out').attr('disabled', true);
        }

        if (zoom == 1) {
            $('.btn-zoom-fit').attr('disabled', true);
        } else {
            $('.btn-zoom-fit').attr('disabled', false);
        }

        // Scroll UP
        if (e.originalEvent.wheelDelta / 120 > 0) {
            centerZoom(e, $(this));
            
            if (zoom < maxZoom) {
                var zoomIn = zoom + 0.1;

                $(this).css('zoom', zoomIn);
            }
        } else { // Scroll Down
            if (zoom > minZoom) {
                var zoomOut = zoom - 0.1;

                $(this).css('zoom', zoomOut);
            }
        }
    });

    function centerZoom(ev, element) {
        // console.log('aquiii');
        // var offset = element.offset();

        // var mouseX = ev.pageX;
        // var mouseY = ev.pageY;

        // console.log(mouseX);
        // console.log(mouseY);

        // var left = parseInt((offset.left));
        // var top = parseInt((offset.top));

        // console.log(left);
        // console.log(top);

        // element.css('left', left);
        // element.css('top', top);
    }
});
