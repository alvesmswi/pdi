! function() {
    "use strict";
    var t = "undefined" != typeof window && void 0 !== window.document ? window.document : {},
        e = "undefined" != typeof module && module.exports,
        i = "undefined" != typeof Element && "ALLOW_KEYBOARD_INPUT" in Element,
        a = function() {
            for (var e, i = [
                    ["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"],
                    ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"],
                    ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"],
                    ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"],
                    ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]
                ], a = 0, n = i.length, o = {}; a < n; a++)
                if ((e = i[a]) && e[1] in t) {
                    for (a = 0; a < e.length; a++) o[i[0][a]] = e[a];
                    return o
                }
            return !1
        }(),
        n = {
            change: a.fullscreenchange,
            error: a.fullscreenerror
        },
        o = {
            request: function(e) {
                var n = a.requestFullscreen;
                e = e || t.documentElement, /5\.1[.\d]* Safari/.test(navigator.userAgent) ? e[n]() : e[n](i && Element.ALLOW_KEYBOARD_INPUT)
            },
            exit: function() {
                t[a.exitFullscreen]()
            },
            toggle: function(t) {
                this.isFullscreen ? this.exit() : this.request(t)
            },
            onchange: function(t) {
                this.on("change", t)
            },
            onerror: function(t) {
                this.on("error", t)
            },
            on: function(e, i) {
                var a = n[e];
                a && t.addEventListener(a, i, !1)
            },
            off: function(e, i) {
                var a = n[e];
                a && t.removeEventListener(a, i, !1)
            },
            raw: a
        };
    if (!a) return void(e ? module.exports = !1 : window.screenfull = !1);
    Object.defineProperties(o, {
        isFullscreen: {
            get: function() {
                return Boolean(t[a.fullscreenElement])
            }
        },
        element: {
            enumerable: !0,
            get: function() {
                return t[a.fullscreenElement]
            }
        },
        enabled: {
            enumerable: !0,
            get: function() {
                return Boolean(t[a.fullscreenEnabled])
            }
        }
    }), e ? module.exports = o : window.screenfull = o
}(),
function() {
    "use strict";
    var t = {},
        e = 0,
        i = !1,
        a = null,
        n = null,
        o = function() {
            return window.location.href.split("#")
        },
        r = function() {
            a || (a = setInterval(function() {
                e > 0 && n != window.location.href && (n = window.location.href, window.Hash.check())
            }, 100))
        };
    window.Hash = function(t) {
        return Object.freeze ? Object.freeze(t) : t
    }({
        pushState: function(t) {
            return window.history && window.history.pushState && (i = t), this
        },
        fragment: function() {
            var t = o();
            return i ? window.location.pathname + (t[1] ? "#" + t[1] : "") : t[1] || ""
        },
        get: function(t, e) {
            var a, n = [];
            for (a in e) Object.prototype.hasOwnProperty(a) && n.push(encodeURIComponent(a) + "=" + encodeURIComponent(e[a]));
            return n.length > 0 && (n = "?" + n.join("&")), i ? t + n : o()[0] + "#" + t + n
        },
        go: function(t, e) {
            if (this.fragment() != t) {
                var a = this.get(t, e);
                i ? window.history.pushState(null, document.title, a) : window.location.href = a
            }
            return this
        },
        update: function() {
            return n = window.location.href, this
        },
        on: function(i, a, n) {
            return t[i] || (t[i] = {
                title: n,
                listeners: []
            }), t[i].listeners.push(a), e++, r(), this
        },
        check: function() {
            var e, i, a, n = this.fragment();
            for (i in t)
                if (Object.prototype.hasOwnProperty.call(t, i))
                    if (t[i].regexp = t[i].regexp || new RegExp(i), a = t[i].regexp.exec(n))
                        for (t[i].title && (document.title = t[i].title), e = 0; e < t[i].listeners.length; e++) t[i].listeners[e].yep && t[i].listeners[e].yep(n, a);
                    else
                        for (e = 0; e < t[i].listeners.length; e++) t[i].listeners[e].nop && t[i].listeners[e].nop(n);
            return this
        }
    })
}(),
function(t) {
    "use strict";

    function e(t, i) {
        return t[0] != i[0] && (!!t.attr("page") || !!t.parent()[0] && e(t.parent(), i))
    }

    function i(t) {
        function e(t) {
            this.name = "TurnJsError", this.message = t
        }
        return e.prototype = new Error, e.prototype.constructor = e, new e(t)
    }

    function a(t, e, i) {
        return s && i ? " translate3d(" + t + "px," + e + "px, 0px) " : " translate(" + t + "px, " + e + "px) "
    }

    function n(t, e) {
        return s && e ? " scale3d(" + t + ", " + t + ", 1) " : " scale(" + t + ") "
    }

    function o(t, e) {
        return {
            x: t,
            y: e
        }
    }

    function r(t, e) {
        return function() {
            return t.apply(e, arguments)
        }
    }
    var s, h = {
            max: 2,
            flipbook: null,
            easeFunction: "ease-in-out",
            duration: 500,
            when: {}
        },
        l = {
            init: function(e) {
                var a = this,
                    n = this.data(),
                    d = t.extend({}, h, e);
                if (!d.flipbook || !d.flipbook.turn("is")) throw i("options.flipbook is required");
                if (s = "WebKitCSSMatrix" in window || "MozPerspective" in document.body.style, "function" != typeof d.max) {
                    var p = d.max;
                    d.max = function() {
                        return p
                    }
                }
                n.zoom = {
                    opts: d,
                    axis: o(0, 0),
                    scrollPos: o(0, 0),
                    eventQueue: [],
                    mouseupEvent: function() {
                        return l._eMouseUp.apply(a, arguments)
                    },
                    eventTouchStart: r(l._eTouchStart, a),
                    eventTouchMove: r(l._eTouchMove, a),
                    eventTouchEnd: r(l._eTouchEnd, a),
                    flipbookEvents: {
                        zooming: r(l._eZoom, a),
                        pressed: r(l._ePressed, a),
                        released: r(l._eReleased, a),
                        start: r(l._eStart, a),
                        turning: r(l._eTurning, a),
                        turned: r(l._eTurned, a),
                        destroying: r(l._eDestroying, a)
                    }
                };
                for (var u in d.when) Object.prototype.hasOwnProperty.call(d.when, u) && this.bind("zoom." + u, d.when[u]);
                for (u in n.zoom.flipbookEvents) Object.prototype.hasOwnProperty.call(n.zoom.flipbookEvents, u) && d.flipbook.bind(u, n.zoom.flipbookEvents[u]);
                this.css({
                    position: "relative",
                    overflow: "hidden"
                }), t.isTouch ? (d.flipbook.bind("touchstart", n.zoom.eventTouchStart).bind("touchmove", n.zoom.eventTouchMove).bind("touchend", n.zoom.eventTouchEnd), this.bind("touchstart", l._tap)) : this.mousedown(l._mousedown).click(l._tap)
            },
            _tap: function(i) {
                var a = t(this),
                    n = a.data().zoom;
                n.opts.flipbook;
                if (!n.draggingCorner && !n.dragging && e(t(i.target), a)) {
                    l._addEvent.call(a, "tap", i);
                    var o = l._eventSeq.call(a);
                    o && a.trigger(o)
                }
            },
            _addEvent: function(t, e) {
                var i = this.data().zoom,
                    a = (new Date).getTime(),
                    n = {
                        name: t,
                        timestamp: a,
                        event: e
                    };
                i.eventQueue.push(n), i.eventQueue.length > 10 && i.eventQueue.splice(0, 1)
            },
            _eventSeq: function() {
                var e = this.data().zoom,
                    i = e.eventQueue,
                    a = i.length - 1;
                return a > 0 && "tap" == i[a].name && "tap" == i[a - 1].name && i[a].event.pageX == i[a - 1].event.pageX && i[a].event.pageY == i[a - 1].event.pageY && i[a].timestamp - i[a - 1].timestamp < 200 && i[a].timestamp - i[a - 1].timestamp > 50 ? t.extend(i[a].event, {
                    type: "zoom.doubleTap"
                }) : "tap" == i[a].name ? t.extend(i[a].event, {
                    type: "zoom.tap"
                }) : void 0
            },
            _prepareZoom: function() {
                var e, i = 0,
                    a = this.data().zoom,
                    n = 1 / this.zoom("value"),
                    r = a.opts.flipbook,
                    s = r.turn("direction"),
                    h = r.data(),
                    l = r.offset(),
                    d = this.offset(),
                    p = {
                        height: r.height()
                    },
                    u = r.turn("view");
                "double" == r.turn("display") && r.data().opts.autoCenter ? u[0] ? u[1] ? (p.width = r.width(), e = o(l.left - d.left, l.top - d.top)) : (p.width = r.width() / 2, i = "ltr" == s ? 0 : p.width, e = o("ltr" == s ? l.left - d.left : l.left - d.left + p.width, l.top - d.top)) : (p.width = r.width() / 2, i = "ltr" == s ? p.width : 0, e = o("ltr" == s ? l.left - d.left + p.width : l.left - d.left, l.top - d.top)) : (p.width = r.width(), e = o(l.left - d.left, l.top - d.top)), a.zoomer || (a.zoomer = t("<div />", {
                    class: "zoomer",
                    css: {
                        overflow: "hidden",
                        position: "absolute",
                        zIndex: "1000000"
                    }
                }).mousedown(function() {
                    return !1
                }).appendTo(this)), a.zoomer.css({
                    top: e.y,
                    left: e.x,
                    width: p.width,
                    height: p.height
                });
                var c = u.join(",");
                if (c != a.zoomerView) {
                    a.zoomerView = c, a.zoomer.find("*").remove();
                    for (var f = 0; f < u.length; f++)
                        if (u[f]) {
                            var g = h.pageObjs[u[f]].offset(),
                                v = t(h.pageObjs[u[f]]);
                            v.clone().transform("").css({
                                width: v.width() * n,
                                height: v.height() * n,
                                position: "absolute",
                                display: "",
                                top: (g.top - l.top) * n,
                                left: (g.left - l.left - i) * n
                            }).appendTo(a.zoomer)
                        }
                }
                return {
                    pos: e,
                    size: p
                }
            },
            value: function() {
                return this.data().zoom.opts.flipbook.turn("zoom")
            },
            zoomIn: function(e) {
                var i, r = this,
                    s = this.data().zoom,
                    h = s.opts.flipbook,
                    d = s.opts.max(),
                    p = (h.offset(), this.offset());
                if (s.zoomIn) return this;
                h.turn("stop");
                var u = t.Event("zoom.change");
                if (this.trigger(u, [d]), u.isDefaultPrevented()) return this;
                var c = l._prepareZoom.call(this),
                    f = c.pos,
                    g = o(c.size.width / 2, c.size.height / 2),
                    v = t.cssPrefix(),
                    m = t.cssTransitionEnd(),
                    w = h.data().opts.autoCenter;
                s.scale = d, h.data().noCenter = !0, i = void 0 !== e ? "x" in e && "y" in e ? o(e.x - f.x, e.y - f.y) : t.isTouch ? o(e.originalEvent.touches[0].pageX - f.x - p.left, e.originalEvent.touches[0].pageY - f.y - p.top) : o(e.pageX - f.x - p.left, e.pageY - f.y - p.top) : o(g.x, g.y), (i.x < 0 || i.y < 0 || i.x > c.width || i.y > c.height) && (i.x = g.x, i.y = g.y);
                var y = o((i.x - g.x) * d + g.x, (i.y - g.y) * d + g.y),
                    x = o(c.size.width * d > this.width() ? i.x - y.x : 0, c.size.height * d > this.height() ? i.y - y.y : 0),
                    b = o(Math.abs(c.size.width * d - this.width()), Math.abs(c.size.height * d - this.height())),
                    z = o(Math.min(0, c.size.width * d - this.width()), Math.min(0, c.size.height * d - this.height())),
                    _ = o(g.x * d - g.x - f.x - x.x, g.y * d - g.y - f.y - x.y);
                _.y > b.y ? x.y = _.y - b.y + x.y : _.y < z.y && (x.y = _.y - z.y + x.y), _.x > b.x ? x.x = _.x - b.x + x.x : _.x < z.x && (x.x = _.x - z.x + x.x), _ = o(g.x * d - g.x - f.x - x.x, g.y * d - g.y - f.y - x.y);
                var P = {};
                P[v + "transition"] = v + "transform " + s.opts.easeFunction + " " + s.opts.duration + "ms";
                var M = function() {
                    r.trigger("zoom.zoomIn"), s.zoomIn = !0, s.flipPosition = o(h.css("left"), h.css("top")), h.turn("zoom", d).css({
                        position: "absolute",
                        margin: "",
                        top: 0,
                        left: 0
                    });
                    var e = h.offset();
                    s.axis = o(e.left - p.left, e.top - p.top), w && "double" == h.turn("display") && ("ltr" == h.turn("direction") && !h.turn("view")[0] || "rtl" == h.turn("direction") && !h.turn("view")[1]) && (s.axis.x = s.axis.x + h.width() / 2), r.zoom("scroll", _), r.bind(t.mouseEvents.down, l._eMouseDown), r.bind(t.mouseEvents.move, l._eMouseMove), t(document).bind(t.mouseEvents.up, s.mouseupEvent), r.bind("mousewheel", l._eMouseWheel), setTimeout(function() {
                        s.zoomer.hide(), s.zoomer.remove(), s.zoomer = null, s.zoomerView = null
                    }, 50)
                };
                return s.zoomer.css(P).show(), m ? s.zoomer.bind(m, function() {
                    t(this).unbind(m), M()
                }) : setTimeout(M, s.opts.duration), s.zoomer.transform(a(x.x, x.y, !0) + n(d, !0)), this
            },
            zoomOut: function(e) {
                var i, r = this,
                    s = this.data().zoom,
                    h = s.opts.flipbook,
                    d = 1 / s.scale,
                    p = t.cssPrefix(),
                    u = t.cssTransitionEnd(),
                    c = this.offset();
                if (e = void 0 !== e ? e : s.opts.duration, s.zoomIn) {
                    var f = t.Event("zoom.change");
                    if (this.trigger(f, [1]), f.isDefaultPrevented()) return this;
                    s.zoomIn = !1, s.scale = 1, h.data().noCenter = !1, r.unbind(t.mouseEvents.down, l._eMouseDown), r.unbind(t.mouseEvents.move, l._eMouseMove), t(document).unbind(t.mouseEvents.up, s.mouseupEvent), r.unbind("mousewheel", l._eMouseWheel);
                    var g = {};
                    g[p + "transition"] = p + "transform " + s.opts.easeFunction + " " + e + "ms", h.css(g);
                    var v, m = t("<div />", {
                        css: {
                            position: "relative",
                            top: s.flipPosition.y,
                            left: s.flipPosition.x,
                            width: h.width() * d,
                            height: h.height() * d,
                            background: "blue"
                        }
                    }).appendTo(h.parent());
                    v = o(m.offset().left - c.left, m.offset().top - c.top), m.remove();
                    var w = h.data().opts.autoCenter;
                    w && "double" == h.turn("display") && (h.turn("view")[0] ? h.turn("view")[1] || (v.x = "ltr" == h.turn("direction") ? v.x + m.width() / 4 : v.x - m.width() / 4) : v.x = "ltr" == h.turn("direction") ? v.x - m.width() / 4 : v.x + m.width() / 4);
                    var y = t.findPos(h[0]);
                    i = o(-h.width() / 2 - y.left + m.width() / 2 + v.x + c.left, -h.height() / 2 - y.top + m.height() / 2 + v.y + c.top);
                    var x = function() {
                        h[0].style.removeProperty ? (h[0].style.removeProperty(p + "transition"), h.transform(h.turn("options").acceleration ? a(0, 0, !0) : "").turn("zoom", 1), h[0].style.removeProperty("margin"), h.css({
                            position: "relative",
                            top: s.flipPosition.y,
                            left: s.flipPosition.x
                        })) : h.transform("none").turn("zoom", 1).css({
                            margin: "",
                            top: s.flipPosition.y,
                            left: s.flipPosition.x,
                            position: "relative"
                        }), w && h.turn("center"), r.trigger("zoom.zoomOut")
                    };
                    return 0 === e ? x() : u ? (h.bind(u, function() {
                        t(this).unbind(u), x()
                    }), h.transform(a(i.x, i.y, !0) + n(d, !0))) : (setTimeout(x, e), h.transform(a(i.x, i.y, !0) + n(d, !0))), this
                }
            },
            flipbookWidth: function() {
                var t = this.data().zoom,
                    e = t.opts.flipbook,
                    i = e.turn("view");
                return "double" != e.turn("display") || i[0] && i[1] ? e.width() : e.width() / 2
            },
            scroll: function(e, i, n) {
                var r = this.data().zoom,
                    h = r.opts.flipbook,
                    l = this.zoom("flipbookWidth"),
                    d = t.cssPrefix();
                if (s) {
                    var p = {};
                    p[d + "transition"] = n ? d + "transform 200ms" : "none", h.css(p), h.transform(a(-r.axis.x - e.x, -r.axis.y - e.y, !0))
                } else h.css({
                    top: -r.axis.y - e.y,
                    left: -r.axis.x - e.x
                });
                if (!i) {
                    var u, c = o(Math.min(0, (l - this.width()) / 2), Math.min(0, (h.height() - this.height()) / 2)),
                        f = o(l > this.width() ? l - this.width() : (l - this.width()) / 2, h.height() > this.height() ? h.height() - this.height() : (h.height() - this.height()) / 2);
                    e.y < c.y ? (e.y = c.y, u = !0) : e.y > f.y && (e.y = f.y, u = !0), e.x < c.x ? (e.x = c.x, u = !0) : e.x > f.x && (e.x = f.x, u = !0), u && this.zoom("scroll", e, !0, !0)
                }
                r.scrollPos = o(e.x, e.y)
            },
            resize: function() {
                var t = this.data().zoom,
                    e = t.opts.flipbook;
                if (this.zoom("value") > 1) {
                    var i = e.offset(),
                        a = this.offset();
                    t.axis = o(i.left - a.left + (t.axis.x + t.scrollPos.x), i.top - a.top + (t.axis.y + t.scrollPos.y)), "double" != e.turn("display") || "ltr" != e.turn("direction") || e.turn("view")[0] || (t.axis.x = t.axis.x + e.width() / 2), this.zoom("scroll", t.scrollPos)
                }
            },
            _eZoom: function() {
                for (var t = this.data().zoom, e = t.opts.flipbook, i = e.turn("view"), a = 0; a < i.length; a++) i[a] && this.trigger("zoom.resize", [t.scale, i[a], e.data().pageObjs[i[a]]])
            },
            _eStart: function(t, e) {
                1 != this.zoom("value") && t.preventDefault()
            },
            _eTurning: function(t, e, i) {
                var a = this,
                    n = this.zoom("value"),
                    o = this.data().zoom,
                    r = o.opts.flipbook;
                if (o.page = r.turn("page"), 1 != n) {
                    for (var s = 0; s < i.length; s++) i[s] && this.trigger("zoom.resize", [n, i[s], r.data().pageObjs[i[s]]]);
                    setTimeout(function() {
                        a.zoom("resize")
                    }, 0)
                }
            },
            _eTurned: function(t, e) {
                if (1 != this.zoom("value")) {
                    var i = this.data().zoom,
                        a = i.opts.flipbook;
                    e > i.page ? this.zoom("scroll", o(0, i.scrollPos.y), !1, !0) : e < i.page && this.zoom("scroll", o(a.width(), i.scrollPos.y), !1, !0)
                }
            },
            _ePressed: function() {
                t(this).data().zoom.draggingCorner = !0
            },
            _eReleased: function() {
                var e = t(this).data().zoom;
                setTimeout(function() {
                    e.draggingCorner = !1
                }, 1)
            },
            _eMouseDown: function(e) {
                return t(this).data().zoom.draggingCur = t.isTouch ? o(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY) : o(e.pageX, e.pageY), !1
            },
            _eMouseMove: function(e) {
                var i = t(this).data().zoom;
                if (i.draggingCur) {
                    i.dragging = !0;
                    var a = t.isTouch ? o(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY) : o(e.pageX, e.pageY),
                        n = o(a.x - i.draggingCur.x, a.y - i.draggingCur.y);
                    return t(this).zoom("scroll", o(i.scrollPos.x - n.x, i.scrollPos.y - n.y), !0), i.draggingCur = a, !1
                }
            },
            _eMouseUp: function(e) {
                var i = t(this).data().zoom;
                i.dragging && t(this).zoom("scroll", i.scrollPos), i.draggingCur = null, setTimeout(function() {
                    i.dragging = !1
                }, 1)
            },
            _eMouseWheel: function(e, i, a, n) {
                var r = t(this).data().zoom,
                    s = o(r.scrollPos.x + 10 * a, r.scrollPos.y - 10 * n);
                t(this).zoom("scroll", s, !1, !0)
            },
            _eTouchStart: function(e, i) {
                var a = t(this).data().zoom,
                    n = (a.opts.flipbook, o(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY));
                a.touch = {}, a.touch.initial = n, a.touch.last = n, a.touch.timestamp = (new Date).getTime(), a.touch.speed = o(0, 0)
            },
            _eTouchMove: function(e) {
                var i = t(this).data().zoom,
                    a = t(this).zoom("value"),
                    n = i.opts.flipbook,
                    r = (new Date).getTime(),
                    s = o(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY);
                i.touch && 1 == a && !n.data().mouseAction && (i.touch.motion = o(s.x - i.touch.last.x, s.y - i.touch.last.y), i.touch.speed.x = 0 === i.touch.speed.x ? i.touch.motion.x / (r - i.touch.timestamp) : (i.touch.speed.x + i.touch.motion.x / (r - i.touch.timestamp)) / 2, i.touch.last = s, i.touch.timestamp = r)
            },
            _eTouchEnd: function(e) {
                var i = t(this).data().zoom;
                if (i.touch && 1 == t(this).zoom("value")) {
                    var a = Math.abs(i.touch.initial.y - i.touch.last.y);
                    a < 50 && (i.touch.speed.x < -1 || i.touch.last.x - i.touch.initial.x < -100) ? this.trigger("zoom.swipeLeft") : a < 50 && (i.touch.speed.x > 1 || i.touch.last.x - i.touch.initial.x > 100) && this.trigger("zoom.swipeRight")
                }
            },
            _eDestroying: function() {
                var e = this,
                    i = this.data().zoom,
                    a = i.opts.flipbook,
                    n = ["tap", "doubleTap", "resize", "zoomIn", "zoomOut", "swipeLeft", "swipeRight"];
                this.zoom("zoomOut", 0), t.each(n, function(t, i) {
                    e.unbind("zoom." + i)
                });
                for (var o in i.flipbookEvents) Object.prototype.hasOwnProperty.call(i.flipbookEvents, o) && a.unbind(o, i.flipbookEvents[o]);
                a.unbind("touchstart", i.eventTouchStart).unbind("touchmove", i.eventTouchMove).unbind("touchend", i.eventTouchEnd), this.unbind("touchstart", l._tap).unbind("click", l._tap), i = null, this.data().zoom = null
            }
        };
    t.extend(t.fn, {
        zoom: function() {
            var e = arguments;
            if (e[0] && "object" != typeof e[0]) {
                if (l[e[0]]) return l[e[0]].apply(t(this[0]), Array.prototype.slice.call(e, 1));
                throw i(e[0] + " is not a method")
            }
            return l.init.apply(t(this[0]), e)
        }
    })
}(jQuery),
function(t) {
    "use strict";

    function e(t, e, i) {
        if (i[0] && "object" != typeof i[0]) {
            if (e[i[0]]) return e[i[0]].apply(t, Array.prototype.slice.call(i, 1));
            throw f(i[0] + " is not a method or property")
        }
        return e.init.apply(t, i)
    }

    function i(t, e, i, a) {
        return {
            css: {
                position: "absolute",
                top: t,
                left: e,
                overflow: a || "hidden",
                zIndex: i || "auto"
            }
        }
    }

    function a(t, e, i, a, n) {
        var r = 1 - n,
            s = r * r * r,
            h = n * n * n;
        return o(Math.round(s * t.x + 3 * n * r * r * e.x + 3 * n * n * r * i.x + h * a.x), Math.round(s * t.y + 3 * n * r * r * e.y + 3 * n * n * r * i.y + h * a.y))
    }

    function n(t) {
        return t / x * 180
    }

    function o(t, e) {
        return {
            x: t,
            y: e
        }
    }

    function r() {
        var t;
        if (t = /AppleWebkit\/([0-9\.]+)/i.exec(navigator.userAgent)) {
            return parseFloat(t[1]) > 534.3
        }
        return !0
    }

    function s(t, e, i) {
        return m && i ? " translate3d(" + t + "px," + e + "px, 0px) " : " translate(" + t + "px, " + e + "px) "
    }

    function h(t) {
        return " rotate(" + t + "deg) "
    }

    function l(t, e) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }

    function d() {
        for (var t = ["Moz", "Webkit", "Khtml", "O", "ms"], e = t.length, i = ""; e--;) t[e] + "Transform" in document.body.style && (i = "-" + t[e].toLowerCase() + "-");
        return i
    }

    function p() {
        var t, e = document.createElement("fakeelement"),
            i = {
                transition: "transitionend",
                OTransition: "oTransitionEnd",
                MSTransition: "transitionend",
                MozTransition: "transitionend",
                WebkitTransition: "webkitTransitionEnd"
            };
        for (t in i)
            if (void 0 !== e.style[t]) return i[t]
    }

    function u(t, e, i, a, n) {
        var r, s = [];
        if ("-webkit-" == y) {
            for (r = 0; r < n; r++) s.push("color-stop(" + a[r][0] + ", " + a[r][1] + ")");
            t.css({
                "background-image": "-webkit-gradient(linear, " + e.x + "% " + e.y + "%," + i.x + "% " + i.y + "%, " + s.join(",") + " )"
            })
        } else {
            e = {
                x: e.x / 100 * t.width(),
                y: e.y / 100 * t.height()
            }, i = {
                x: i.x / 100 * t.width(),
                y: i.y / 100 * t.height()
            };
            var h = i.x - e.x,
                l = i.y - e.y,
                d = Math.atan2(l, h),
                p = d - Math.PI / 2,
                u = Math.abs(t.width() * Math.sin(p)) + Math.abs(t.height() * Math.cos(p)),
                c = Math.sqrt(l * l + h * h),
                f = o(i.x < e.x ? t.width() : 0, i.y < e.y ? t.height() : 0),
                g = Math.tan(d),
                v = -1 / g,
                m = (v * f.x - f.y - g * e.x + e.y) / (v - g),
                w = {
                    x: m,
                    y: v * m - v * f.x + f.y
                },
                x = Math.sqrt(Math.pow(w.x - e.x, 2) + Math.pow(w.y - e.y, 2));
            for (r = 0; r < n; r++) s.push(" " + a[r][1] + " " + 100 * (x + c * a[r][0]) / u + "%");
            t.css({
                "background-image": y + "linear-gradient(" + -d + "rad," + s.join(",") + ")"
            })
        }
    }

    function c(e, i, a) {
        var n = t.Event(e);
        return i.trigger(n, a), n.isDefaultPrevented() ? "prevented" : n.isPropagationStopped() ? "stopped" : ""
    }

    function f(t) {
        function e(t) {
            this.name = "TurnJsError", this.message = t
        }
        return e.prototype = new Error, e.prototype.constructor = e, new e(t)
    }

    function g(t) {
        var e = {
            top: 0,
            left: 0
        };
        do {
            e.left += t.offsetLeft, e.top += t.offsetTop
        } while (t = t.offsetParent);
        return e
    }

    function v() {
        return -1 == navigator.userAgent.indexOf("MSIE 9.0")
    }
    var m, w, y = "",
        x = Math.PI,
        b = x / 2,
        z = "ontouchstart" in window,
        _ = z ? {
            down: "touchstart",
            move: "touchmove",
            up: "touchend",
            over: "touchstart",
            out: "touchend"
        } : {
            down: "mousedown",
            move: "mousemove",
            up: "mouseup",
            over: "mouseover",
            out: "mouseout"
        },
        P = {
            backward: ["bl", "tl"],
            forward: ["br", "tr"],
            all: ["tl", "bl", "tr", "br", "l", "r"]
        },
        M = ["single", "double"],
        E = ["ltr", "rtl"],
        k = {
            acceleration: !0,
            display: "double",
            duration: 600,
            page: 1,
            gradients: !0,
            turnCorners: "bl,br",
            when: null
        },
        T = {
            cornerSize: 100
        },
        F = {
            init: function(e) {
                m = "WebKitCSSMatrix" in window || "MozPerspective" in document.body.style, w = r(), y = d();
                var i, a = 0,
                    n = this.data(),
                    o = this.children();
                if (e = t.extend({
                        width: this.width(),
                        height: this.height(),
                        direction: this.attr("dir") || this.css("direction") || "ltr"
                    }, k, e), n.opts = e, n.pageObjs = {}, n.pages = {}, n.pageWrap = {}, n.pageZoom = {}, n.pagePlace = {}, n.pageMv = [], n.zoom = 1, n.totalPages = e.pages || 0, n.eventHandlers = {
                        touchStart: t.proxy(F._touchStart, this),
                        touchMove: t.proxy(F._touchMove, this),
                        touchEnd: t.proxy(F._touchEnd, this),
                        start: t.proxy(F._eventStart, this)
                    }, e.when)
                    for (i in e.when) l(i, e.when) && this.bind(i, e.when[i]);
                for (this.css({
                        position: "relative",
                        width: e.width,
                        height: e.height
                    }), this.turn("display", e.display), "" !== e.direction && this.turn("direction", e.direction), m && !z && e.acceleration && this.transform(s(0, 0, !0)), i = 0; i < o.length; i++) "1" != t(o[i]).attr("ignore") && this.turn("addPage", o[i], ++a);
                return t(this).bind(_.down, n.eventHandlers.touchStart).bind("end", F._eventEnd).bind("pressed", F._eventPressed).bind("released", F._eventReleased).bind("flip", F._flip), t(this).parent().bind("start", n.eventHandlers.start), t(document).bind(_.move, n.eventHandlers.touchMove).bind(_.up, n.eventHandlers.touchEnd), this.turn("page", e.page), n.done = !0, this
            },
            addPage: function(e, i) {
                var a, n, o = !1,
                    r = this.data(),
                    s = r.totalPages + 1;
                if (r.destroying) return !1;
                if ((a = /\bp([0-9]+)\b/.exec(t(e).attr("class"))) && (i = parseInt(a[1], 10)), i) {
                    if (i == s) o = !0;
                    else if (i > s) throw f('Page "' + i + '" cannot be inserted')
                } else i = s, o = !0;
                return i >= 1 && i <= s && (n = "double" == r.display ? i % 2 ? " odd" : " even" : "", r.done && this.turn("stop"), i in r.pageObjs && F._movePages.call(this, i, 1), o && (r.totalPages = s), r.pageObjs[i] = t(e).css({
                    float: "left"
                }).addClass("page p" + i + n), !v() && r.pageObjs[i].hasClass("hard") && r.pageObjs[i].removeClass("hard"), F._addPage.call(this, i), F._removeFromDOM.call(this)), this
            },
            _addPage: function(e) {
                var i = this.data(),
                    a = i.pageObjs[e];
                if (a)
                    if (F._necessPage.call(this, e)) {
                        if (!i.pageWrap[e]) {
                            i.pageWrap[e] = t("<div/>", {
                                class: "page-wrapper",
                                page: e,
                                css: {
                                    position: "absolute",
                                    overflow: "hidden"
                                }
                            }), this.append(i.pageWrap[e]), i.pagePlace[e] || (i.pagePlace[e] = e, i.pageObjs[e].appendTo(i.pageWrap[e]));
                            var n = F._pageSize.call(this, e, !0);
                            a.css({
                                width: n.width,
                                height: n.height
                            }), i.pageWrap[e].css(n)
                        }
                        i.pagePlace[e] == e && F._makeFlip.call(this, e)
                    } else i.pagePlace[e] = 0, i.pageObjs[e] && i.pageObjs[e].remove()
            },
            hasPage: function(t) {
                return l(t, this.data().pageObjs)
            },
            center: function(e) {
                var i = this.data(),
                    a = t(this).turn("size"),
                    n = 0;
                if (!i.noCenter) {
                    if ("double" == i.display) {
                        var o = this.turn("view", e || i.tpage || i.page);
                        "ltr" == i.direction ? o[0] ? o[1] || (n += a.width / 4) : n -= a.width / 4 : o[0] ? o[1] || (n -= a.width / 4) : n += a.width / 4
                    }
                    t(this).css({
                        marginLeft: n
                    })
                }
                return this
            },
            destroy: function() {
                var e = this,
                    i = this.data(),
                    a = ["end", "first", "flip", "last", "pressed", "released", "start", "turning", "turned", "zooming", "missing"];
                if ("prevented" != c("destroying", this)) {
                    for (i.destroying = !0, t.each(a, function(t, i) {
                            e.unbind(i)
                        }), this.parent().unbind("start", i.eventHandlers.start), t(document).unbind(_.move, i.eventHandlers.touchMove).unbind(_.up, i.eventHandlers.touchEnd); 0 !== i.totalPages;) this.turn("removePage", i.totalPages);
                    return i.fparent && i.fparent.remove(), i.shadow && i.shadow.remove(), this.removeData(), i = null, this
                }
            },
            is: function() {
                return "object" == typeof this.data().pages
            },
            zoom: function(e) {
                var i = this.data();
                if ("number" == typeof e) {
                    if (e < .001 || e > 100) throw f(e + " is not a value for zoom");
                    if ("prevented" == c("zooming", this, [e, i.zoom])) return this;
                    var a = this.turn("size"),
                        n = this.turn("view"),
                        o = 1 / i.zoom,
                        r = Math.round(a.width * o * e),
                        s = Math.round(a.height * o * e);
                    i.zoom = e, t(this).turn("stop").turn("size", r, s), i.opts.autoCenter && this.turn("center"), F._updateShadow.call(this);
                    for (var h = 0; h < n.length; h++) n[h] && i.pageZoom[n[h]] != i.zoom && (this.trigger("zoomed", [n[h], n, i.pageZoom[n[h]], i.zoom]), i.pageZoom[n[h]] = i.zoom);
                    return this
                }
                return i.zoom
            },
            _pageSize: function(t, e) {
                var i = this.data(),
                    a = {};
                if ("single" == i.display) a.width = this.width(), a.height = this.height(), e && (a.top = 0, a.left = 0, a.right = "auto");
                else {
                    var n = this.width() / 2,
                        o = this.height();
                    if (i.pageObjs[t].hasClass("own-size") ? (a.width = i.pageObjs[t].width(), a.height = i.pageObjs[t].height()) : (a.width = n, a.height = o), e) {
                        var r = t % 2;
                        a.top = (o - a.height) / 2, "ltr" == i.direction ? (a[r ? "right" : "left"] = n - a.width, a[r ? "left" : "right"] = "auto") : (a[r ? "left" : "right"] = n - a.width, a[r ? "right" : "left"] = "auto")
                    }
                }
                return a
            },
            _makeFlip: function(t) {
                var e = this.data();
                if (!e.pages[t] && e.pagePlace[t] == t) {
                    var i = "single" == e.display,
                        a = t % 2;
                    e.pages[t] = e.pageObjs[t].css(F._pageSize.call(this, t)).flip({
                        page: t,
                        next: a || i ? t + 1 : t - 1,
                        turn: this
                    }).flip("disable", e.disabled), F._setPageLoc.call(this, t), e.pageZoom[t] = e.zoom
                }
                return e.pages[t]
            },
            _makeRange: function() {
                var t, e;
                if (!(this.data().totalPages < 1))
                    for (e = this.turn("range"), t = e[0]; t <= e[1]; t++) F._addPage.call(this, t)
            },
            range: function(t) {
                var e, i, a, n, o = this.data();
                if (t = t || o.tpage || o.page || 1, n = F._view.call(this, t), t < 1 || t > o.totalPages) throw f('"' + t + '" is not a valid page');
                return n[1] = n[1] || n[0], n[0] >= 1 && n[1] <= o.totalPages ? (e = Math.floor(2), o.totalPages - n[1] > n[0] ? (i = Math.min(n[0] - 1, e), a = 2 * e - i) : (a = Math.min(o.totalPages - n[1], e), i = 2 * e - a)) : (i = 5, a = 5), [Math.max(1, n[0] - i), Math.min(o.totalPages, n[1] + a)]
            },
            _necessPage: function(t) {
                if (0 === t) return !0;
                var e = this.turn("range");
                return this.data().pageObjs[t].hasClass("fixed") || t >= e[0] && t <= e[1]
            },
            _removeFromDOM: function() {
                var t, e = this.data();
                for (t in e.pageWrap) l(t, e.pageWrap) && !F._necessPage.call(this, t) && F._removePageFromDOM.call(this, t)
            },
            _removePageFromDOM: function(t) {
                var e = this.data();
                if (e.pages[t]) {
                    var i = e.pages[t].data();
                    O._moveFoldingPage.call(e.pages[t], !1), i.f && i.f.fwrapper && i.f.fwrapper.remove(), e.pages[t].removeData(), e.pages[t].remove(), delete e.pages[t]
                }
                e.pageObjs[t] && e.pageObjs[t].remove(), e.pageWrap[t] && (e.pageWrap[t].remove(), delete e.pageWrap[t]), F._removeMv.call(this, t), delete e.pagePlace[t], delete e.pageZoom[t]
            },
            removePage: function(t) {
                var e = this.data();
                if ("*" == t)
                    for (; 0 !== e.totalPages;) this.turn("removePage", e.totalPages);
                else {
                    if (t < 1 || t > e.totalPages) throw f("The page " + t + " doesn't exist");
                    e.pageObjs[t] && (this.turn("stop"), F._removePageFromDOM.call(this, t), delete e.pageObjs[t]), F._movePages.call(this, t, -1), e.totalPages = e.totalPages - 1, e.page > e.totalPages ? (e.page = null, F._fitPage.call(this, e.totalPages)) : (F._makeRange.call(this), this.turn("update"))
                }
                return this
            },
            _movePages: function(t, e) {
                var i, a = this,
                    n = this.data(),
                    o = "single" == n.display,
                    r = function(t) {
                        var i = t + e,
                            r = i % 2,
                            s = r ? " odd " : " even ";
                        n.pageObjs[t] && (n.pageObjs[i] = n.pageObjs[t].removeClass("p" + t + " odd even").addClass("p" + i + s)), n.pagePlace[t] && n.pageWrap[t] && (n.pagePlace[i] = i, n.pageObjs[i].hasClass("fixed") ? n.pageWrap[i] = n.pageWrap[t].attr("page", i) : n.pageWrap[i] = n.pageWrap[t].css(F._pageSize.call(a, i, !0)).attr("page", i), n.pages[t] && (n.pages[i] = n.pages[t].flip("options", {
                            page: i,
                            next: o || r ? i + 1 : i - 1
                        })), e && (delete n.pages[t], delete n.pagePlace[t], delete n.pageZoom[t], delete n.pageObjs[t], delete n.pageWrap[t]))
                    };
                if (e > 0)
                    for (i = n.totalPages; i >= t; i--) r(i);
                else
                    for (i = t; i <= n.totalPages; i++) r(i)
            },
            display: function(e) {
                var i = this.data(),
                    a = i.display;
                if (void 0 === e) return a;
                if (-1 == t.inArray(e, M)) throw f('"' + e + '" is not a value for display');
                switch (e) {
                    case "single":
                        i.pageObjs[0] || (this.turn("stop").css({
                            overflow: "hidden"
                        }), i.pageObjs[0] = t("<div />", {
                            class: "page p-temporal"
                        }).css({
                            width: this.width(),
                            height: this.height()
                        }).appendTo(this)), this.addClass("shadow");
                        break;
                    case "double":
                        i.pageObjs[0] && (this.turn("stop").css({
                            overflow: ""
                        }), i.pageObjs[0].remove(), delete i.pageObjs[0]), this.removeClass("shadow")
                }
                if (i.display = e, a) {
                    var n = this.turn("size");
                    F._movePages.call(this, 1, 0), this.turn("size", n.width, n.height).turn("update")
                }
                return this
            },
            direction: function(e) {
                var i = this.data();
                if (void 0 === e) return i.direction;
                if (e = e.toLowerCase(), -1 == t.inArray(e, E)) throw f('"' + e + '" is not a value for direction');
                return "rtl" == e && t(this).attr("dir", "ltr").css({
                    direction: "ltr"
                }), i.direction = e, i.done && this.turn("size", t(this).width(), t(this).height()), this
            },
            animating: function() {
                return this.data().pageMv.length > 0
            },
            corner: function() {
                var t, e, i = this.data();
                for (e in i.pages)
                    if (l(e, i.pages) && (t = i.pages[e].flip("corner"))) return t;
                return !1
            },
            data: function() {
                return this.data()
            },
            disable: function(e) {
                var i, a = this.data(),
                    n = this.turn("view");
                a.disabled = void 0 === e || !0 === e;
                for (i in a.pages) l(i, a.pages) && a.pages[i].flip("disable", !!a.disabled || -1 == t.inArray(parseInt(i, 10), n));
                return this
            },
            disabled: function(t) {
                return void 0 === t ? !0 === this.data().disabled : this.turn("disable", t)
            },
            size: function(t, e) {
                if (void 0 === t || void 0 === e) return {
                    width: this.width(),
                    height: this.height()
                };
                this.turn("stop");
                var i, a, n = this.data(),
                    o = "double" == n.display ? t / 2 : t;
                this.css({
                    width: t,
                    height: e
                }), n.pageObjs[0] && n.pageObjs[0].css({
                    width: o,
                    height: e
                });
                for (i in n.pageWrap) l(i, n.pageWrap) && (a = F._pageSize.call(this, i, !0), n.pageObjs[i].css({
                    width: a.width,
                    height: a.height
                }), n.pageWrap[i].css(a), n.pages[i] && n.pages[i].css({
                    width: a.width,
                    height: a.height
                }));
                return this.turn("resize"), this
            },
            resize: function() {
                var t, e = this.data();
                for (e.pages[0] && (e.pageWrap[0].css({
                        left: -this.width()
                    }), e.pages[0].flip("resize", !0)), t = 1; t <= e.totalPages; t++) e.pages[t] && e.pages[t].flip("resize", !0);
                F._updateShadow.call(this), e.opts.autoCenter && this.turn("center")
            },
            _removeMv: function(t) {
                var e, i = this.data();
                for (e = 0; e < i.pageMv.length; e++)
                    if (i.pageMv[e] == t) return i.pageMv.splice(e, 1), !0;
                return !1
            },
            _addMv: function(t) {
                var e = this.data();
                F._removeMv.call(this, t), e.pageMv.push(t)
            },
            _view: function(t) {
                var e = this.data();
                return t = t || e.page, "double" == e.display ? t % 2 ? [t - 1, t] : [t, t + 1] : [t]
            },
            view: function(t) {
                var e = this.data(),
                    i = F._view.call(this, t);
                return "double" == e.display ? [i[0] > 0 ? i[0] : 0, i[1] <= e.totalPages ? i[1] : 0] : [i[0] > 0 && i[0] <= e.totalPages ? i[0] : 0]
            },
            stop: function(t, e) {
                if (this.turn("animating")) {
                    var i, a, n, o = this.data();
                    for (o.tpage && (o.page = o.tpage, delete o.tpage), i = 0; i < o.pageMv.length; i++) o.pageMv[i] && o.pageMv[i] !== t && (n = o.pages[o.pageMv[i]], a = n.data().f.opts, n.flip("hideFoldedPage", e), e || O._moveFoldingPage.call(n, !1), a.force && (a.next = a.page % 2 == 0 ? a.page - 1 : a.page + 1, delete a.force))
                }
                return this.turn("update"), this
            },
            pages: function(t) {
                var e = this.data();
                if (t) {
                    if (t < e.totalPages)
                        for (var i = e.totalPages; i > t; i--) this.turn("removePage", i);
                    return e.totalPages = t, F._fitPage.call(this, e.page), this
                }
                return e.totalPages
            },
            _missing: function(t) {
                var e = this.data();
                if (!(e.totalPages < 1)) {
                    var i, a = this.turn("range", t),
                        n = [];
                    for (i = a[0]; i <= a[1]; i++) e.pageObjs[i] || n.push(i);
                    n.length > 0 && this.trigger("missing", [n])
                }
            },
            _fitPage: function(t) {
                var e = this.data(),
                    i = this.turn("view", t);
                if (F._missing.call(this, t), e.pageObjs[t]) {
                    e.page = t, this.turn("stop");
                    for (var a = 0; a < i.length; a++) i[a] && e.pageZoom[i[a]] != e.zoom && (this.trigger("zoomed", [i[a], i, e.pageZoom[i[a]], e.zoom]), e.pageZoom[i[a]] = e.zoom);
                    F._removeFromDOM.call(this), F._makeRange.call(this), F._updateShadow.call(this), this.trigger("turned", [t, i]), this.turn("update"), e.opts.autoCenter && this.turn("center")
                }
            },
            _turnPage: function(e) {
                var i, a, n = this.data(),
                    o = n.pagePlace[e],
                    r = this.turn("view"),
                    s = this.turn("view", e);
                if (n.page != e) {
                    var h = n.page;
                    if ("prevented" == c("turning", this, [e, s])) return void(h == n.page && -1 != t.inArray(o, n.pageMv) && n.pages[o].flip("hideFoldedPage", !0)); - 1 != t.inArray(1, s) && this.trigger("first"), -1 != t.inArray(n.totalPages, s) && this.trigger("last")
                }
                "single" == n.display ? (i = r[0], a = s[0]) : r[1] && e > r[1] ? (i = r[1], a = s[0]) : r[0] && e < r[0] && (i = r[0], a = s[1]);
                var l = n.opts.turnCorners.split(","),
                    d = n.pages[i].data().f,
                    p = d.opts,
                    u = d.point;
                F._missing.call(this, e), n.pageObjs[e] && (this.turn("stop"), n.page = e, F._makeRange.call(this), n.tpage = a, p.next != a && (p.next = a, p.force = !0), this.turn("update"), d.point = u, "hard" == d.effect ? "ltr" == n.direction ? n.pages[i].flip("turnPage", e > i ? "r" : "l") : n.pages[i].flip("turnPage", e > i ? "l" : "r") : "ltr" == n.direction ? n.pages[i].flip("turnPage", l[e > i ? 1 : 0]) : n.pages[i].flip("turnPage", l[e > i ? 0 : 1]))
            },
            page: function(e) {
                var i = this.data();
                if (void 0 === e) return i.page;
                if (!i.disabled && !i.destroying) {
                    if ((e = parseInt(e, 10)) > 0 && e <= i.totalPages) return e != i.page && (i.done && -1 == t.inArray(e, this.turn("view")) ? F._turnPage.call(this, e) : F._fitPage.call(this, e)), this;
                    throw f("The page " + e + " does not exist")
                }
            },
            next: function() {
                return this.turn("page", Math.min(this.data().totalPages, F._view.call(this, this.data().page).pop() + 1))
            },
            previous: function() {
                return this.turn("page", Math.max(1, F._view.call(this, this.data().page).shift() - 1))
            },
            peel: function(t, e) {
                var i = this.data(),
                    a = this.turn("view");
                if (e = void 0 === e || !0 === e, !1 === t) this.turn("stop", null, e);
                else if ("single" == i.display) i.pages[i.page].flip("peel", t, e);
                else {
                    var n;
                    n = "ltr" == i.direction ? -1 != t.indexOf("l") ? a[0] : a[1] : -1 != t.indexOf("l") ? a[1] : a[0], i.pages[n] && i.pages[n].flip("peel", t, e)
                }
                return this
            },
            _addMotionPage: function() {
                var e = t(this).data().f.opts,
                    i = e.turn;
                i.data();
                F._addMv.call(i, e.page)
            },
            _eventStart: function(t, e, i) {
                var a = e.turn.data(),
                    n = a.pageZoom[e.page];
                if (t.isDefaultPrevented()) return void F._updateShadow.call(e.turn);
                n && n != a.zoom && (e.turn.trigger("zoomed", [e.page, e.turn.turn("view", e.page), n, a.zoom]), a.pageZoom[e.page] = a.zoom), "single" == a.display && i && ("l" == i.charAt(1) && "ltr" == a.direction || "r" == i.charAt(1) && "rtl" == a.direction ? (e.next = e.next < e.page ? e.next : e.page - 1, e.force = !0) : e.next = e.next > e.page ? e.next : e.page + 1), F._addMotionPage.call(t.target), F._updateShadow.call(e.turn)
            },
            _eventEnd: function(e, i, a) {
                var n = t(e.target),
                    o = (n.data().f, i.turn),
                    r = o.data();
                if (a) {
                    var s = r.tpage || r.page;
                    s != i.next && s != i.page || (delete r.tpage, F._fitPage.call(o, s || i.next, !0))
                } else F._removeMv.call(o, i.page), F._updateShadow.call(o), o.turn("update")
            },
            _eventPressed: function(e) {
                var i = t(e.target).data().f,
                    a = i.opts.turn,
                    n = a.data();
                n.pages;
                return n.mouseAction = !0, a.turn("update"), i.time = (new Date).getTime()
            },
            _eventReleased: function(e, i) {
                var a, n = t(e.target),
                    o = n.data().f,
                    r = o.opts.turn,
                    s = r.data();
                a = "single" == s.display ? "br" == i.corner || "tr" == i.corner ? i.x < n.width() / 2 : i.x > n.width() / 2 : i.x < 0 || i.x > n.width(), ((new Date).getTime() - o.time < 200 || a) && (e.preventDefault(), F._turnPage.call(r, o.opts.next)), s.mouseAction = !1
            },
            _flip: function(e) {
                e.stopPropagation();
                var i = t(e.target).data().f.opts;
                i.turn.trigger("turn", [i.next]), i.turn.data().opts.autoCenter && i.turn.turn("center", i.next)
            },
            _touchStart: function() {
                var t = this.data();
                for (var e in t.pages)
                    if (l(e, t.pages) && !1 === O._eventStart.apply(t.pages[e], arguments)) return !1
            },
            _touchMove: function() {
                var t = this.data();
                for (var e in t.pages) l(e, t.pages) && O._eventMove.apply(t.pages[e], arguments)
            },
            _touchEnd: function() {
                var t = this.data();
                for (var e in t.pages) l(e, t.pages) && O._eventEnd.apply(t.pages[e], arguments)
            },
            calculateZ: function(t) {
                var e, i, a, n, o, r = this,
                    s = this.data(),
                    h = this.turn("view"),
                    l = h[0] || h[1],
                    d = t.length - 1,
                    p = {
                        pageZ: {},
                        partZ: {},
                        pageV: {}
                    },
                    u = function(t) {
                        var e = r.turn("view", t);
                        e[0] && (p.pageV[e[0]] = !0), e[1] && (p.pageV[e[1]] = !0)
                    };
                for (e = 0; e <= d; e++) i = t[e], a = s.pages[i].data().f.opts.next, n = s.pagePlace[i], u(i), u(a), o = s.pagePlace[a] == a ? a : i, p.pageZ[o] = s.totalPages - Math.abs(l - o), p.partZ[n] = 2 * s.totalPages - d + e;
                return p
            },
            update: function() {
                var e, i = this.data();
                if (this.turn("animating") && 0 !== i.pageMv[0]) {
                    var a, n, o = this.turn("calculateZ", i.pageMv),
                        r = this.turn("corner"),
                        s = this.turn("view"),
                        h = this.turn("view", i.tpage);
                    for (e in i.pageWrap) l(e, i.pageWrap) && (n = i.pageObjs[e].hasClass("fixed"), i.pageWrap[e].css({
                        display: o.pageV[e] || n ? "" : "none",
                        zIndex: (i.pageObjs[e].hasClass("hard") ? o.partZ[e] : o.pageZ[e]) || (n ? -1 : 0)
                    }), (a = i.pages[e]) && (a.flip("z", o.partZ[e] || null), o.pageV[e] && a.flip("resize"), i.tpage ? a.flip("hover", !1).flip("disable", -1 == t.inArray(parseInt(e, 10), i.pageMv) && e != h[0] && e != h[1]) : a.flip("hover", !1 === r).flip("disable", e != s[0] && e != s[1])))
                } else
                    for (e in i.pageWrap)
                        if (l(e, i.pageWrap)) {
                            var d = F._setPageLoc.call(this, e);
                            i.pages[e] && i.pages[e].flip("disable", i.disabled || 1 != d).flip("hover", !0).flip("z", null)
                        } return this
            },
            _updateShadow: function() {
                var e, a, n = this.data(),
                    o = this.width(),
                    r = this.height(),
                    s = "single" == n.display ? o : o / 2;
                e = this.turn("view"), n.shadow || (n.shadow = t("<div />", {
                    class: "shadow",
                    css: i(0, 0, 0).css
                }).appendTo(this));
                for (var h = 0; h < n.pageMv.length && (e[0] && e[1]); h++) e = this.turn("view", n.pages[n.pageMv[h]].data().f.opts.next), a = this.turn("view", n.pageMv[h]), e[0] = e[0] && a[0], e[1] = e[1] && a[1];
                switch (e[0] ? e[1] ? 3 : "ltr" == n.direction ? 2 : 1 : "ltr" == n.direction ? 1 : 2) {
                    case 1:
                        n.shadow.css({
                            width: s,
                            height: r,
                            top: 0,
                            left: s
                        });
                        break;
                    case 2:
                        n.shadow.css({
                            width: s,
                            height: r,
                            top: 0,
                            left: 0
                        });
                        break;
                    case 3:
                        n.shadow.css({
                            width: o,
                            height: r,
                            top: 0,
                            left: 0
                        })
                }
            },
            _setPageLoc: function(t) {
                var e = this.data(),
                    i = this.turn("view"),
                    a = 0;
                if (t == i[0] || t == i[1] ? a = 1 : ("single" == e.display && t == i[0] + 1 || "double" == e.display && t == i[0] - 2 || t == i[1] + 2) && (a = 2), !this.turn("animating")) switch (a) {
                    case 1:
                        e.pageWrap[t].css({
                            zIndex: e.totalPages,
                            display: ""
                        });
                        break;
                    case 2:
                        e.pageWrap[t].css({
                            zIndex: e.totalPages - 1,
                            display: ""
                        });
                        break;
                    case 0:
                        e.pageWrap[t].css({
                            zIndex: 0,
                            display: e.pageObjs[t].hasClass("fixed") ? "" : "none"
                        })
                }
                return a
            },
            options: function(e) {
                if (void 0 === e) return this.data().opts;
                var i = this.data();
                if (t.extend(i.opts, e), e.pages && this.turn("pages", e.pages), e.page && this.turn("page", e.page), e.display && this.turn("display", e.display), e.direction && this.turn("direction", e.direction), e.width && e.height && this.turn("size", e.width, e.height), e.when)
                    for (var a in e.when) l(a, e.when) && this.unbind(a).bind(a, e.when[a]);
                return this
            },
            version: function() {
                return "4.1.0"
            }
        },
        O = {
            init: function(t) {
                return this.data({
                    f: {
                        disabled: !1,
                        hover: !1,
                        effect: this.hasClass("hard") ? "hard" : "sheet"
                    }
                }), this.flip("options", t), O._addPageWrapper.call(this), this
            },
            setData: function(e) {
                var i = this.data();
                return i.f = t.extend(i.f, e), this
            },
            options: function(e) {
                var i = this.data().f;
                return e ? (O.setData.call(this, {
                    opts: t.extend({}, i.opts || T, e)
                }), this) : i.opts
            },
            z: function(t) {
                var e = this.data().f;
                return e.opts["z-index"] = t, e.fwrapper && e.fwrapper.css({
                    zIndex: t || parseInt(e.parent.css("z-index"), 10) || 0
                }), this
            },
            _cAllowed: function() {
                var t = this.data().f,
                    e = t.opts.page,
                    i = t.opts.turn.data(),
                    a = e % 2;
                return "hard" == t.effect ? "ltr" == i.direction ? [a ? "r" : "l"] : [a ? "l" : "r"] : "single" == i.display ? 1 == e ? "ltr" == i.direction ? P.forward : P.backward : e == i.totalPages ? "ltr" == i.direction ? P.backward : P.forward : P.all : "ltr" == i.direction ? P[a ? "forward" : "backward"] : P[a ? "backward" : "forward"]
            },
            _cornerActivated: function(e) {
                var i = this.data().f,
                    a = this.width(),
                    n = this.height(),
                    o = {
                        x: e.x,
                        y: e.y,
                        corner: ""
                    },
                    r = i.opts.cornerSize;
                if (o.x <= 0 || o.y <= 0 || o.x >= a || o.y >= n) return !1;
                var s = O._cAllowed.call(this);
                switch (i.effect) {
                    case "hard":
                        if (o.x > a - r) o.corner = "r";
                        else {
                            if (!(o.x < r)) return !1;
                            o.corner = "l"
                        }
                        break;
                    case "sheet":
                        if (o.y < r) o.corner += "t";
                        else {
                            if (!(o.y >= n - r)) return !1;
                            o.corner += "b"
                        }
                        if (o.x <= r) o.corner += "l";
                        else {
                            if (!(o.x >= a - r)) return !1;
                            o.corner += "r"
                        }
                }
                return !(!o.corner || -1 == t.inArray(o.corner, s)) && o
            },
            _isIArea: function(t) {
                var e = this.data().f.parent.offset();
                return t = z && t.originalEvent ? t.originalEvent.touches[0] : t, O._cornerActivated.call(this, {
                    x: t.pageX - e.left,
                    y: t.pageY - e.top
                })
            },
            _c: function(t, e) {
                switch (e = e || 0, t) {
                    case "tl":
                        return o(e, e);
                    case "tr":
                        return o(this.width() - e, e);
                    case "bl":
                        return o(e, this.height() - e);
                    case "br":
                        return o(this.width() - e, this.height() - e);
                    case "l":
                        return o(e, 0);
                    case "r":
                        return o(this.width() - e, 0)
                }
            },
            _c2: function(t) {
                switch (t) {
                    case "tl":
                        return o(2 * this.width(), 0);
                    case "tr":
                        return o(-this.width(), 0);
                    case "bl":
                        return o(2 * this.width(), this.height());
                    case "br":
                        return o(-this.width(), this.height());
                    case "l":
                        return o(2 * this.width(), 0);
                    case "r":
                        return o(-this.width(), 0)
                }
            },
            _foldingPage: function() {
                var t = this.data().f;
                if (t) {
                    var e = t.opts;
                    return e.turn ? (t = e.turn.data(), "single" == t.display ? e.next > 1 || e.page > 1 ? t.pageObjs[0] : null : t.pageObjs[e.next]) : void 0
                }
            },
            _backGradient: function() {
                var e = this.data().f,
                    a = e.opts.turn.data(),
                    n = a.opts.gradients && ("single" == a.display || 2 != e.opts.page && e.opts.page != a.totalPages - 1);
                return n && !e.bshadow && (e.bshadow = t("<div/>", i(0, 0, 1)).css({
                    position: "",
                    width: this.width(),
                    height: this.height()
                }).appendTo(e.parent)), n
            },
            type: function() {
                return this.data().f.effect
            },
            resize: function(t) {
                var e = this.data().f,
                    i = e.opts.turn.data(),
                    a = this.width(),
                    n = this.height();
                switch (e.effect) {
                    case "hard":
                        t && (e.wrapper.css({
                            width: a,
                            height: n
                        }), e.fpage.css({
                            width: a,
                            height: n
                        }), i.opts.gradients && (e.ashadow.css({
                            width: a,
                            height: n
                        }), e.bshadow.css({
                            width: a,
                            height: n
                        })));
                        break;
                    case "sheet":
                        if (t) {
                            var o = Math.round(Math.sqrt(Math.pow(a, 2) + Math.pow(n, 2)));
                            e.wrapper.css({
                                width: o,
                                height: o
                            }), e.fwrapper.css({
                                width: o,
                                height: o
                            }).children(":first-child").css({
                                width: a,
                                height: n
                            }), e.fpage.css({
                                width: a,
                                height: n
                            }), i.opts.gradients && e.ashadow.css({
                                width: a,
                                height: n
                            }), O._backGradient.call(this) && e.bshadow.css({
                                width: a,
                                height: n
                            })
                        }
                        if (e.parent.is(":visible")) {
                            var r = g(e.parent[0]);
                            e.fwrapper.css({
                                top: r.top,
                                left: r.left
                            }), r = g(e.opts.turn[0]), e.fparent.css({
                                top: -r.top,
                                left: -r.left
                            })
                        }
                        this.flip("z", e.opts["z-index"])
                }
            },
            _addPageWrapper: function() {
                var e = this.data().f,
                    a = e.opts.turn.data(),
                    n = this.parent();
                if (e.parent = n, !e.wrapper) switch (e.effect) {
                    case "hard":
                        var o = {};
                        o[y + "transform-style"] = "preserve-3d", o[y + "backface-visibility"] = "hidden", e.wrapper = t("<div/>", i(0, 0, 2)).css(o).appendTo(n).prepend(this), e.fpage = t("<div/>", i(0, 0, 1)).css(o).appendTo(n), a.opts.gradients && (e.ashadow = t("<div/>", i(0, 0, 0)).hide().appendTo(n), e.bshadow = t("<div/>", i(0, 0, 0)));
                        break;
                    case "sheet":
                        var r = this.width(),
                            s = this.height();
                        Math.round(Math.sqrt(Math.pow(r, 2) + Math.pow(s, 2)));
                        if (e.fparent = e.opts.turn.data().fparent, !e.fparent) {
                            var h = t('<div class="turnCorners" />', {
                                css: {
                                    "pointer-events": "none"
                                }
                            }).hide();
                            h.data().flips = 0, h.css(i(0, 0, "auto", "visible").css).appendTo(e.opts.turn), e.opts.turn.data().fparent = h, e.fparent = h
                        }
                        this.css({
                            position: "absolute",
                            top: 0,
                            left: 0,
                            bottom: "auto",
                            right: "auto"
                        }), e.wrapper = t("<div/>", i(0, 0, this.css("z-index"))).appendTo(n).prepend(this), e.fwrapper = t("<div/>", i(n.offset().top, n.offset().left)).hide().appendTo(e.fparent), e.fpage = t("<div/>", i(0, 0, 0, "visible")).css({
                            cursor: "default"
                        }).appendTo(e.fwrapper), a.opts.gradients && (e.ashadow = t("<div/>", i(0, 0, 1)).appendTo(e.fpage)), O.setData.call(this, e)
                }
                O.resize.call(this, !0)
            },
            _fold: function(t) {
                var e = this.data().f,
                    i = e.opts.turn.data(),
                    a = O._c.call(this, t.corner),
                    r = this.width(),
                    l = this.height();
                switch (e.effect) {
                    case "hard":
                        "l" == t.corner ? t.x = Math.min(Math.max(t.x, 0), 2 * r) : t.x = Math.max(Math.min(t.x, r), -r);
                        var d, p, c, f, g, v = i.totalPages,
                            m = e.opts["z-index"] || v,
                            z = {
                                overflow: "visible"
                            },
                            _ = a.x ? (a.x - t.x) / r : t.x / r,
                            P = 90 * _,
                            M = P < 90;
                        switch (t.corner) {
                            case "l":
                                f = "0% 50%", g = "100% 50%", M ? (d = 0, p = e.opts.next - 1 > 0, c = 1) : (d = "100%", p = e.opts.page + 1 < v, c = 0);
                                break;
                            case "r":
                                f = "100% 50%", g = "0% 50%", P = -P, r = -r, M ? (d = 0, p = e.opts.next + 1 < v, c = 0) : (d = "-100%", p = 1 != e.opts.page, c = 1)
                        }
                        z[y + "perspective-origin"] = g, e.wrapper.transform("rotateY(" + P + "deg)translate3d(0px, 0px, " + (this.attr("depth") || 0) + "px)", g), e.fpage.transform("translateX(" + r + "px) rotateY(" + (180 + P) + "deg)", f), e.parent.css(z), M ? (_ = 1 - _, e.wrapper.css({
                            zIndex: m + 1
                        }), e.fpage.css({
                            zIndex: m
                        })) : (_ -= 1, e.wrapper.css({
                            zIndex: m
                        }), e.fpage.css({
                            zIndex: m + 1
                        })), i.opts.gradients && (p ? e.ashadow.css({
                            display: "",
                            left: d,
                            backgroundColor: "rgba(0,0,0," + .5 * _ + ")"
                        }).transform("rotateY(0deg)") : e.ashadow.hide(), e.bshadow.css({
                            opacity: 1 - _
                        }), M ? e.bshadow.parent()[0] != e.wrapper[0] && e.bshadow.appendTo(e.wrapper) : e.bshadow.parent()[0] != e.fpage[0] && e.bshadow.appendTo(e.fpage), u(e.bshadow, o(100 * c, 0), o(100 * (1 - c), 0), [
                            [0, "rgba(0,0,0,0.3)"],
                            [1, "rgba(0,0,0,0)"]
                        ], 2));
                        break;
                    case "sheet":
                        var E, k, T, F, S, I, j, C = this,
                            W = 0,
                            L = o(0, 0),
                            A = o(0, 0),
                            D = o(0, 0),
                            Z = O._foldingPage.call(this),
                            q = (Math.tan(0), i.opts.acceleration),
                            B = e.wrapper.height(),
                            R = "t" == t.corner.substr(0, 1),
                            H = "l" == t.corner.substr(1, 1),
                            Y = function() {
                                var e = o(0, 0),
                                    s = o(0, 0);
                                e.x = a.x ? a.x - t.x : t.x, e.y = w ? a.y ? a.y - t.y : t.y : 0, s.x = H ? r - e.x / 2 : t.x + e.x / 2, s.y = e.y / 2;
                                var h = b - Math.atan2(e.y, e.x),
                                    d = h - Math.atan2(s.y, s.x),
                                    p = Math.max(0, Math.sin(d) * Math.sqrt(Math.pow(s.x, 2) + Math.pow(s.y, 2)));
                                if (W = n(h), D = o(p * Math.sin(h), p * Math.cos(h)), h > b && (D.x = D.x + Math.abs(D.y * e.y / e.x), D.y = 0, Math.round(D.x * Math.tan(x - h)) < l)) return t.y = Math.sqrt(Math.pow(l, 2) + 2 * s.x * e.x), R && (t.y = l - t.y), Y();
                                if (h > b) {
                                    var u = x - h,
                                        c = B - l / Math.sin(u);
                                    L = o(Math.round(c * Math.cos(u)), Math.round(c * Math.sin(u))), H && (L.x = -L.x), R && (L.y = -L.y)
                                }
                                E = Math.round(D.y / Math.tan(h) + D.x);
                                var f = r - E,
                                    g = f * Math.cos(2 * h),
                                    v = f * Math.sin(2 * h);
                                if (A = o(Math.round(H ? f - g : E + g), Math.round(R ? v : l - v)), i.opts.gradients) {
                                    S = f * Math.sin(h);
                                    var m = O._c2.call(C, t.corner),
                                        y = Math.sqrt(Math.pow(m.x - t.x, 2) + Math.pow(m.y - t.y, 2)) / r;
                                    j = Math.sin(b * (y > 1 ? 2 - y : y)), I = Math.min(y, 1), F = S > 100 ? (S - 100) / S : 0, k = o(S * Math.sin(h) / r * 100, S * Math.cos(h) / l * 100), O._backGradient.call(C) && (T = o(1.2 * S * Math.sin(h) / r * 100, 1.2 * S * Math.cos(h) / l * 100), H || (T.x = 100 - T.x), R || (T.y = 100 - T.y))
                                }
                                return D.x = Math.round(D.x), D.y = Math.round(D.y), !0
                            },
                            X = function(t, a, n, d) {
                                var p = ["0", "auto"],
                                    c = (r - B) * n[0] / 100,
                                    f = (l - B) * n[1] / 100,
                                    g = {
                                        left: p[a[0]],
                                        top: p[a[1]],
                                        right: p[a[2]],
                                        bottom: p[a[3]]
                                    },
                                    v = {},
                                    m = 90 != d && -90 != d ? H ? -1 : 1 : 0,
                                    w = n[0] + "% " + n[1] + "%";
                                C.css(g).transform(h(d) + s(t.x + m, t.y, q), w), e.fpage.css(g).transform(h(d) + s(t.x + A.x - L.x - r * n[0] / 100, t.y + A.y - L.y - l * n[1] / 100, q) + h((180 / d - 2) * d), w), e.wrapper.transform(s(-t.x + c - m, -t.y + f, q) + h(-d), w), e.fwrapper.transform(s(-t.x + L.x + c, -t.y + L.y + f, q) + h(-d), w), i.opts.gradients && (n[0] && (k.x = 100 - k.x), n[1] && (k.y = 100 - k.y), v["box-shadow"] = "0 0 20px rgba(0,0,0," + .5 * j + ")", Z.css(v), u(e.ashadow, o(H ? 100 : 0, R ? 0 : 100), o(k.x, k.y), [
                                    [F, "rgba(0,0,0,0)"],
                                    [.8 * (1 - F) + F, "rgba(0,0,0," + .2 * I + ")"],
                                    [1, "rgba(255,255,255," + .2 * I + ")"]
                                ], 3), O._backGradient.call(C) && u(e.bshadow, o(H ? 0 : 100, R ? 0 : 100), o(T.x, T.y), [
                                    [.6, "rgba(0,0,0,0)"],
                                    [.8, "rgba(0,0,0," + .3 * I + ")"],
                                    [1, "rgba(0,0,0,0)"]
                                ], 3))
                            };
                        switch (t.corner) {
                            case "l":
                            case "r":
                                break;
                            case "tl":
                                t.x = Math.max(t.x, 1), Y(), X(D, [1, 0, 0, 1], [100, 0], W);
                                break;
                            case "tr":
                                t.x = Math.min(t.x, r - 1), Y(), X(o(-D.x, D.y), [0, 0, 0, 1], [0, 0], -W);
                                break;
                            case "bl":
                                t.x = Math.max(t.x, 1), Y(), X(o(D.x, -D.y), [1, 1, 0, 0], [100, 100], -W);
                                break;
                            case "br":
                                t.x = Math.min(t.x, r - 1), Y(), X(o(-D.x, -D.y), [0, 1, 1, 0], [0, 100], W)
                        }
                }
                e.point = t
            },
            _moveFoldingPage: function(t) {
                var e = this.data().f;
                if (e) {
                    var i = e.opts.turn,
                        a = i.data(),
                        n = a.pagePlace;
                    if (t) {
                        var o = e.opts.next;
                        if (n[o] != e.opts.page) {
                            e.folding && O._moveFoldingPage.call(this, !1);
                            O._foldingPage.call(this).appendTo(e.fpage), n[o] = e.opts.page, e.folding = o
                        }
                        i.turn("update")
                    } else if (e.folding) {
                        if (a.pages[e.folding]) {
                            var r = a.pages[e.folding].data().f;
                            a.pageObjs[e.folding].appendTo(r.wrapper)
                        } else a.pageWrap[e.folding] && a.pageObjs[e.folding].appendTo(a.pageWrap[e.folding]);
                        e.folding in n && (n[e.folding] = e.folding), delete e.folding
                    }
                }
            },
            _showFoldedPage: function(t, e) {
                var i = O._foldingPage.call(this),
                    a = this.data(),
                    n = a.f,
                    o = n.visible;
                if (i) {
                    if (!o || !n.point || n.point.corner != t.corner) {
                        var r = "hover" == n.status || "peel" == n.status || n.opts.turn.data().mouseAction ? t.corner : null;
                        if (o = !1, "prevented" == c("start", this, [n.opts, r])) return !1
                    }
                    if (e) {
                        var s = this,
                            h = n.point && n.point.corner == t.corner ? n.point : O._c.call(this, t.corner, 1);
                        this.animatef({
                            from: [h.x, h.y],
                            to: [t.x, t.y],
                            duration: 500,
                            frame: function(e) {
                                t.x = Math.round(e[0]), t.y = Math.round(e[1]), O._fold.call(s, t)
                            }
                        })
                    } else O._fold.call(this, t), a.effect && !a.effect.turning && this.animatef(!1);
                    if (!o) switch (n.effect) {
                        case "hard":
                            n.visible = !0, O._moveFoldingPage.call(this, !0), n.fpage.show(), n.opts.shadows && n.bshadow.show();
                            break;
                        case "sheet":
                            n.visible = !0, n.fparent.show().data().flips++, O._moveFoldingPage.call(this, !0), n.fwrapper.show(), n.bshadow && n.bshadow.show()
                    }
                    return !0
                }
                return !1
            },
            hide: function() {
                var t = this.data().f,
                    e = t.opts.turn.data(),
                    i = O._foldingPage.call(this);
                switch (t.effect) {
                    case "hard":
                        e.opts.gradients && (t.bshadowLoc = 0, t.bshadow.remove(), t.ashadow.hide()), t.wrapper.transform(""), t.fpage.hide();
                        break;
                    case "sheet":
                        0 == --t.fparent.data().flips && t.fparent.hide(), this.css({
                            left: 0,
                            top: 0,
                            right: "auto",
                            bottom: "auto"
                        }).transform(""), t.wrapper.transform(""), t.fwrapper.hide(), t.bshadow && t.bshadow.hide(), i.transform("")
                }
                return t.visible = !1, this
            },
            hideFoldedPage: function(t) {
                var e = this.data().f;
                if (e.point) {
                    var i = this,
                        n = e.point,
                        r = function() {
                            e.point = null, e.status = "", i.flip("hide"), i.trigger("end", [e.opts, !1])
                        };
                    if (t) {
                        var s = O._c.call(this, n.corner),
                            h = "t" == n.corner.substr(0, 1),
                            l = h ? Math.min(0, n.y - s.y) / 2 : Math.max(0, n.y - s.y) / 2,
                            d = o(n.x, n.y + l),
                            p = o(s.x, s.y - l);
                        this.animatef({
                            from: 0,
                            to: 1,
                            frame: function(t) {
                                var e = a(n, d, p, s, t);
                                n.x = e.x, n.y = e.y, O._fold.call(i, n)
                            },
                            complete: r,
                            duration: 800,
                            hiding: !0
                        })
                    } else this.animatef(!1), r()
                }
            },
            turnPage: function(t) {
                var e = this,
                    i = this.data().f,
                    n = i.opts.turn.data();
                t = {
                    corner: i.corner ? i.corner.corner : t || O._cAllowed.call(this)[0]
                };
                var o = i.point || O._c.call(this, t.corner, i.opts.turn ? n.opts.elevation : 0),
                    r = O._c2.call(this, t.corner);
                this.trigger("flip").animatef({
                    from: 0,
                    to: 1,
                    frame: function(i) {
                        var n = a(o, o, r, r, i);
                        t.x = n.x, t.y = n.y, O._showFoldedPage.call(e, t)
                    },
                    complete: function() {
                        e.trigger("end", [i.opts, !0])
                    },
                    duration: n.opts.duration,
                    turning: !0
                }), i.corner = null
            },
            moving: function() {
                return "effect" in this.data()
            },
            isTurning: function() {
                return this.flip("moving") && this.data().effect.turning
            },
            corner: function() {
                return this.data().f.corner
            },
            _eventStart: function(t) {
                var e = this.data().f,
                    i = e.opts.turn;
                if (!e.corner && !e.disabled && !this.flip("isTurning") && e.opts.page == i.data().pagePlace[e.opts.page]) {
                    if (e.corner = O._isIArea.call(this, t), e.corner && O._foldingPage.call(this)) return this.trigger("pressed", [e.point]), O._showFoldedPage.call(this, e.corner), !1;
                    e.corner = null
                }
            },
            _eventMove: function(t) {
                var e = this.data().f;
                if (!e.disabled)
                    if (t = z ? t.originalEvent.touches : [t], e.corner) {
                        var i = e.parent.offset();
                        e.corner.x = t[0].pageX - i.left, e.corner.y = t[0].pageY - i.top, O._showFoldedPage.call(this, e.corner)
                    } else if (e.hover && !this.data().effect && this.is(":visible")) {
                    var a = O._isIArea.call(this, t[0]);
                    if (a) {
                        if ("sheet" == e.effect && 2 == a.corner.length || "hard" == e.effect) {
                            e.status = "hover";
                            var n = O._c.call(this, a.corner, e.opts.cornerSize / 2);
                            a.x = n.x, a.y = n.y, O._showFoldedPage.call(this, a, !0)
                        }
                    } else "hover" == e.status && (e.status = "", O.hideFoldedPage.call(this, !0))
                }
            },
            _eventEnd: function() {
                var t = this.data().f,
                    e = t.corner;
                !t.disabled && e && "prevented" != c("released", this, [t.point || e]) && O.hideFoldedPage.call(this, !0), t.corner = null
            },
            disable: function(t) {
                return O.setData.call(this, {
                    disabled: t
                }), this
            },
            hover: function(t) {
                return O.setData.call(this, {
                    hover: t
                }), this
            },
            peel: function(e, i) {
                var a = this.data().f;
                if (e) {
                    if (-1 == t.inArray(e, P.all)) throw f("Corner " + e + " is not permitted");
                    if (-1 != t.inArray(e, O._cAllowed.call(this))) {
                        var n = O._c.call(this, e, a.opts.cornerSize / 2);
                        a.status = "peel", O._showFoldedPage.call(this, {
                            corner: e,
                            x: n.x,
                            y: n.y
                        }, i)
                    }
                } else a.status = "", O.hideFoldedPage.call(this, i);
                return this
            }
        };
    window.requestAnim = function() {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(t) {
            window.setTimeout(t, 1e3 / 60)
        }
    }(), t.extend(t.fn, {
        flip: function() {
            return e(t(this[0]), O, arguments)
        },
        turn: function() {
            return e(t(this[0]), F, arguments)
        },
        transform: function(t, e) {
            var i = {};
            return e && (i[y + "transform-origin"] = e), i[y + "transform"] = t, this.css(i)
        },
        animatef: function(e) {
            var i = this.data();
            if (i.effect && i.effect.stop(), e) {
                e.to.length || (e.to = [e.to]), e.from.length || (e.from = [e.from]);
                for (var a = [], n = e.to.length, o = !0, r = this, s = (new Date).getTime(), h = function() {
                        if (i.effect && o) {
                            for (var t = [], l = Math.min(e.duration, (new Date).getTime() - s), d = 0; d < n; d++) t.push(i.effect.easing(1, l, e.from[d], a[d], e.duration));
                            e.frame(1 == n ? t[0] : t), l == e.duration ? (delete i.effect, r.data(i), e.complete && e.complete()) : window.requestAnim(h)
                        }
                    }, l = 0; l < n; l++) a.push(e.to[l] - e.from[l]);
                i.effect = t.extend({
                    stop: function() {
                        o = !1
                    },
                    easing: function(t, e, i, a, n) {
                        return a * Math.sqrt(1 - (e = e / n - 1) * e) + i
                    }
                }, e), this.data(i), h()
            } else delete i.effect
        }
    }), t.isTouch = z, t.mouseEvents = _, t.cssPrefix = d, t.cssTransitionEnd = p, t.findPos = g
}(jQuery),
function t(e, i, a) {
    function n(r, s) {
        if (!i[r]) {
            if (!e[r]) {
                var h = "function" == typeof require && require;
                if (!s && h) return h(r, !0);
                if (o) return o(r, !0);
                var l = new Error("Cannot find module '" + r + "'");
                throw l.code = "MODULE_NOT_FOUND", l
            }
            var d = i[r] = {
                exports: {}
            };
            e[r][0].call(d.exports, function(t) {
                var i = e[r][1][t];
                return n(i || t)
            }, d, d.exports, t, e, i, a)
        }
        return i[r].exports
    }
    for (var o = "function" == typeof require && require, r = 0; r < a.length; r++) n(a[r]);
    return n
}({
    1: [function(t, e, i) {
        "use strict";
        ! function() {
            function t(t) {
                if (t % 2 == 0) var e = t,
                    i = t + 1;
                else var i = t,
                    e = t - 1;
                return e === M && (i = null), 1 === i && (e = null), {
                    left: e,
                    right: i
                }
            }

            function e(t) {
                var e = window.getComputedStyle(t, null),
                    i = parseInt(e.getPropertyValue("height")),
                    a = 0,
                    n = 0;
                return "border-box" === e.getPropertyValue("box-sizing") && (a = parseInt(e.getPropertyValue("padding-top")), n = parseInt(e.getPropertyValue("padding-bottom")), i = i - a - n), i || (i = window.innerHeight - a - n - (m || 0)), i
            }

            function i(t) {
                var e = window.getComputedStyle(t, null),
                    i = parseInt(e.getPropertyValue("width")),
                    a = 0,
                    n = 0;
                return "border-box" === e.getPropertyValue("box-sizing") && (a = parseInt(e.getPropertyValue("padding-left")), n = parseInt(e.getPropertyValue("padding-right")), i = i - a - n), i || (i = window.innerWidth - a - n), i
            }

            function a(t, a, n) {
                var o, r, s, h, l, d = n.getBoundingClientRect();
                return d.width && d.height || (d = {
                    width: n.offsetWidth,
                    height: n.offsetHeight
                }), o = d.width / z, r = t / a, s = o / r, h = o / t, i(P), l = e(P), s > l && (o *= l / s, s = l, h = o / t), {
                    width: o,
                    height: s,
                    ratio: r,
                    reduction: h
                }
            }

            function n(t) {
                return y[+t - 1] || !1
            }

            function o(t) {
                _ || (_ = {}), t.forEach(function(t) {
                    _[t] || (u(t), _[t] = !0)
                })
            }

            function r(e) {
                var i, a = [],
                    n = [],
                    r = $(this).turn("range", e),
                    s = [];
                for (i = r[0]; i <= r[1]; i++) s.push(i);
                var h = t(e).left,
                    l = t(e).right;
                if (null != l) var d = s.indexOf(l),
                    a = s.slice(d);
                if (null != h) var p = s.indexOf(h) + 1,
                    n = s.slice(0, p).reverse();
                var u = [],
                    c = n.shift();
                void 0 != c && a.unshift(c), u = a.concat(n), o(u)
            }

            function s(t) {
                void 0 === t && (t = I);
                var e = "scale(" + t + ")";
                [].forEach.call(j.querySelectorAll(".PFPage"), function(t) {
                    t.style.webkitTransform = e, t.style.MozTransform = e, t.style.msTransform = e, t.style.OTransform = e, t.style.transform = e
                })
            }

            function h() {
                return "single" === c.turn("display")
            }

            function l() {
                var t, e = parseFloat((W.currentZoom - 1).toFixed(2));
                return e !== parseFloat(0) ? (t = e / W.zoomStep, parseInt(t)) : parseInt(e)
            }

            function d(t) {
                c = $(t).turn({
                    display: b,
                    duration: 600,
                    acceleration: !0,
                    elevation: 50,
                    page: 1,
                    autoCenter: !0,
                    when: {
                        first: function(t) {
                            document.getElementById("PrevPage").disabled = !0
                        },
                        last: function(t) {
                            document.getElementById("NextPage").disabled = !0
                        },
                        start: function(t, e) {
                            s.call(this), w.classList.add("peeling")
                        },
                        turning: function(t, e, i) {
                            document.getElementById("PrevPage").disabled = 1 === e, document.getElementById("NextPage").disabled = e === $(this).turn("pages"), r.call(this, e), w.classList.remove("peeling"), s.call(this)
                        },
                        zooming: function(t, e, i) {
                            W.currentZoom = e;
                            var a = parseInt(w.style.top) || 0,
                                n = parseInt(w.style.left) || 0,
                                o = (screenfull.isFullscreen ? W.FSzoomHeight : W.zoomHeight) / 2,
                                r = screenfull.isFullscreen ? W.FSzoomWidth / 2 : W.zoomWidth / (h() ? 2 : 1);
                            e > i ? (w.style.top = a - o + "px", w.style.left = n - r + "px") : e !== i && (w.style.top = a + o + "px", w.style.left = n + r + "px"), I = S * e, s.call(this)
                        }
                    }
                }), r.call(c, 1), document.getElementById("ZoomPlus").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("zoom", W.currentZoom + W.zoomStep)
                }), document.getElementById("ZoomMinus").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("zoom", Math.max(W.currentZoom - W.zoomStep, .1))
                }), document.getElementById("ZoomRestore").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("zoom", 1), w.style.left = "0px", w.style.top = "0px"
                }), document.getElementById("NextPage").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("next")
                }), document.getElementById("PrevPage").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("previous")
                }), document.getElementById("FirstPage").addEventListener("click", function(t) {
                    t.preventDefault(), c.turn("page", 1)
                });
                var e = document.getElementById("FullScreen");
                screenfull.enabled ? (e.addEventListener("click", function() {
                    screenfull.toggle(document.getElementById("PageFlip"))
                }), document.addEventListener(screenfull.raw.fullscreenchange, function() {
                    var t, e, i = document.getElementById("PageFlip"),
                        a = l();
                    screenfull.isFullscreen ? (i.style.width = window.outerWidth + "px", i.style.height = window.outerHeight + "px", i.style.backgroundColor = "#ccc", C.style.height = Z + "px", C.style.width = q + "px", t = q + W.FSzoomWidth * a, e = Z + W.FSzoomHeight * a, S = A) : (i.style.width = "", i.style.height = "", i.style.backgroundColor = "", C.style.height = v + "px", C.style.width = g + "px", t = g + W.zoomWidth * a * 2, e = v + W.zoomHeight * a, S = L), j.style.height = e + "px", j.style.width = t + "px", c.turn("size", t, e), I = S * c.turn("zoom"), s()
                })) : e.parentNode.removeChild(e), x.init()
            }

            function p(t, e, i) {
                var a = {
                    width: e.width,
                    height: e.height
                };
                e.setAttribute("width", a.width), e.setAttribute("height", a.height);
                var n = document.createElement("div"),
                    o = document.createElement("div"),
                    r = document.createElement("div");
                return o.classList.add("PFWrapper"), r.classList.add("PFPage"), r.style.width = a.width + "px", r.style.height = a.height + "px", r.style.webkitTransform = O, r.style.MozTransform = O, r.style.msTransform = O, r.style.OTransform = O, r.style.transform = O, r.style.transformOrigin = "0px 0px", r.dataset.loaded = !0, n.classList.add("PFSheet"), r.appendChild(e), n.appendChild(r), o.appendChild(n), t.appendChild(o), document.getElementById("PFLoading").style.display = "none", i ? i.call(this, t, e) : t
            }

            function u(t) {
                var e = n(t);
                if (!e) return !1;
                window.setTimeout(function() {
                    var t = e.dataset.load;
                    if (t) {
                        var i = document.createElement("img");
                        i.src = t, i.onload = function() {
                            return p(e, i)
                        }
                    }
                }, 25)
            }
            var c, f, g, v, m, w, y, x, b, z, _, P, M, E, k, T, F, O, S, I, j, C, W, L, A, D, Z, q;
            if (W = {
                    zoomStep: .25,
                    currentZoom: 1
                }, x = {
                    init: function() {
                        w.addEventListener("touchstart", this.onTouchStart), w.addEventListener("touchmove", this.onTouchMove), w.addEventListener("touchend", this.onTouchEnd), w.addEventListener("mousedown", this.onMouseDown), w.addEventListener("mouseup", this.onMouseUp), w.addEventListener("mousemove", this.onMouseMove)
                    },
                    onTouchStart: function(t) {
                        this.classList.add("draggable"), this.classList.add("draggable"), this.style.cursor = "grabbing", x.d_h = e(this), x.d_w = i(this);
                        var a = this.getBoundingClientRect();
                        x.offset_h = a.top + document.body.scrollTop, x.offset_w = a.left + document.body.scrollLeft, x.pos_y = (parseInt(this.style.top) || 0) + x.offset_h + x.d_h - t.touches[0].pageY, x.pos_x = (parseInt(this.style.left) || 0) + x.offset_w + x.d_w - t.touches[0].pageX
                    },
                    onTouchMove: function(t) {
                        this.classList.contains("draggable") && (this.style.top = t.touches[0].pageY + x.pos_y - x.d_h - x.offset_h + "px", this.style.left = t.touches[0].pageX + x.pos_x - x.d_w - x.offset_w + "px"), t.preventDefault()
                    },
                    onTouchEnd: function(t) {
                        this.classList.remove("draggable")
                    },
                    onMouseDown: function(t) {
                        this.classList.add("draggable"), this.style.cursor = "grabbing", x.d_h = e(this), x.d_w = i(this);
                        var a = this.getBoundingClientRect();
                        x.offset_h = a.top + document.body.scrollTop, x.offset_w = a.left + document.body.scrollLeft, x.pos_y = (parseInt(this.style.top) || 0) + x.offset_h + x.d_h - t.pageY, x.pos_x = (parseInt(this.style.left) || 0) + x.offset_w + x.d_w - t.pageX, this.addEventListener("mousemove", x.onMouseMove, !1), t.preventDefault()
                    },
                    onMouseUp: function(t) {
                        this.classList.remove("draggable"), this.removeEventListener("mousemove", x.onMouseMove, !1), this.style.cursor = "grab"
                    },
                    onMouseMove: function(t) {
                        this.classList.contains("draggable") && (this.style.top = t.pageY + x.pos_y - x.d_h - x.offset_h + "px", this.style.left = t.pageX + x.pos_x - x.d_w - x.offset_w + "px")
                    },
                    d_h: null,
                    d_w: null,
                    pos_y: null,
                    pos_x: null
                }, !(P = document.getElementById("PFFlip"))) return !1;
            f = document.getElementById("PFButtons"), m = f.offsetHeight, b = function() {
                    return window.innerWidth < 768 ? "single" : "double"
                }(), z = "single" === b ? 1 : 2, "single" === b && P.classList.add("PFSingle"), j = document.getElementById("PFBook"), C = document.getElementById("PFContainer"), w = document.getElementById("PFWrapper"), y = j.querySelectorAll(".flipbook__page"), M = y.length,
                function(t, e) {
                    var i = t.dataset.load;
                    if (!i) return !1;
                    var n = document.createElement("img");
                    n.src = i, n.onload = function() {
                        var i = {
                            width: this.width,
                            height: this.height
                        };
                        return F = F || a(i.width, i.height, j), W.zoomWidth = F.width * W.zoomStep, W.zoomHeight = F.height * W.zoomStep, S = S || F.reduction, I = L = S, O = O || "scale(" + S + ")", p(t, n, e)
                    }
                }(y[0], function(t, e) {
                    k = e.width, T = e.height, E = {
                        width: k,
                        height: T
                    }, v = F.height, g = F.width * z, j.style.height = v + "px", j.style.width = g + "px", C.style.height = v + "px", C.style.width = g + "px", Z = window.outerHeight - m - 32, D = T / k, q = Z / D * z, A = Z / T, W.FSzoomWidth = q * W.zoomStep, W.FSzoomHeight = Z * W.zoomStep, d(j), document.getElementById("PFLoading").classList.add("pageLoaded")
                })
        }()
    }, {}]
}, {}, [1]);