<?php
App::uses('CakeSchema', 'Model');
App::uses('ConnectionManager', 'Model');

/**
 * PageFlip Activation
 *
 * Activation class for PageFlip plugin.
 *
 * @package  PageFlip
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class PageFlipActivation 
{
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}

	public function onActivation(Controller $controller) 
	{
		$controller->Croogo->addAco('PageFlip');
		$controller->Croogo->addAco('PageFlip/Pdfs/admin_node', array('editor', 'gerente'));
		$controller->Croogo->addAco('PageFlip/Pdfs/admin_remove', array('editor', 'gerente'));

		$tableName = 'pageflip_pdfs';
		$pluginName = 'PageFlip';
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();

		if (!in_array($tableName, $tables)) {
			$schema = & new CakeSchema(array(
				'name' => $pluginName,
				'path' => App::pluginPath($pluginName) . 'Config' . DS . 'Schema',
			));
			$schema = $schema->load();

			foreach ($schema->tables as $table => $fields) {
				if (!in_array($table, $tables)) {
					$create = $db->createSchema($schema, $table);
					
					try {
						$db->execute($create);
					} catch (PDOException $e) {
						die(__('Could not create table: %s', $e->getMessage()));
					}
				}
			}
		}

		Cache::clear(false, '_cake_model_');
	}

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('PageFlip');
	}
}
