<?php
class PageFlipSchema extends CakeSchema 
{
	public $pageflip_pdfs = array(
		'id' 		=> array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'node_id' 	=> array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'pdf' 		=> array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'images' 	=> array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'destaques' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'status' 	=> array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'controller'=> array('type' => 'string', 'null' => false, 'default' => 'nodes', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' 	=> array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'node_id' => array('column' => 'node_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public function before($event = array()) 
	{
		return true;
	}

	public function after($event = array()) 
	{
	}
}
