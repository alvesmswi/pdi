<?php

CroogoRouter::connect('/pageflip/pdfs/:preset/:id/:filename', 
	array(
		'plugin' => 'PageFlip', 'controller' => 'Pdfs', 'action' => 'display_file', 
		'admin' => false
	),
	array(
		'pass' => array('id', 'filename', 'preset')
	)
);

CroogoRouter::connect('/admin/pageflip/upload', array(
	'plugin' => 'PageFlip', 'controller' => 'Pdfs', 'action' => 'node', 
	'admin' => true
));
CroogoRouter::connect('/admin/pageflip/remove/:id/:filename', 
	array(
		'plugin' => 'PageFlip', 'controller' => 'Pdfs', 'action' => 'remove', 
		'admin' => true
	),
	array(
		'pass' => array('id', 'filename')
	)
);
