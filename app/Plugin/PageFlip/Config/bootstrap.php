<?php

// Pega os Tipos de Conteúdo do CMSWI, e verifica se a aba PDF está ativo para aquele Tipo de Conteúdo
$Type = ClassRegistry::init('Taxonomy.Type');
$typeDefs = $Type->find('all', array(
	'recursive' => -1,
	'fields' => array(
		'Type.alias',
		'Type.params'
	)
));

$types = array();
foreach($typeDefs as $typeDef) {
	$params = $typeDef['Params'];

	// If para tratar aparecer só no Tipos que tiverem ativo, por padrão não aparece em nenhum.
	if ((isset($params['pdf']) && $params['pdf'])) {
		$types[] = $typeDef['Type']['alias'];
	}
}

Croogo::hookRoutes('PageFlip');
Croogo::hookBehavior('Node', 'PageFlip.PageFlip', array());

Croogo::hookAdminTab('Nodes/admin_add', 'PDF', 'PageFlip.admin_node', array('type' => $types));
Croogo::hookAdminTab('Nodes/admin_edit', 'PDF', 'PageFlip.admin_node', array('type' => $types));

Croogo::hookModelProperty('Node', 'hasMany', array(
	'PageFlip' => array(
		'className' 	=> 'PageFlip.Pdf',
		'foreignKey' 	=> 'node_id',
		'dependent' 	=> true,
		'conditions' 	=> array('PageFlip.controller' => 'nodes')
	)
));
