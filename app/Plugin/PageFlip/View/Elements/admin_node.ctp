<?php echo $this->Html->css(array('PageFlip.admin')); ?>

<?php if (isset($this->data['PageFlip']) && !empty($this->data['PageFlip'])) { ?>
    <br />
    <table class="table">
    <tr>
        <td><a href="<?php echo $this->data['PageFlip']['url'] ?>" target="_blank"><?php echo $this->data['PageFlip']['pdf'] ?></a></td>
        <td><a href="/admin/pageflip/remove/<?php echo $this->data['PageFlip']['id'] ?>/<?php echo $this->data['PageFlip']['pdf'] ?>">Excluir</a></td>
    </tr>
    </table>

    <?php 
        $destaques_string = '1-Capa;';
        if (isset($this->data['PageFlip']['destaques']) && !empty($this->data['PageFlip']['destaques'])) {
            $destaques = json_decode($this->data['PageFlip']['destaques'], true);
            $destaques_string = null;
            $i = 1;
            $total_desataques = count($destaques);
            foreach ($destaques as $pagina => $destaque) {
                if ($i < $total_desataques) {
                    $destaques_string .= "{$pagina}-{$destaque};";
                } else {
                    $destaques_string .= "{$pagina}-{$destaque}";
                }

                $i++;
            }
        }

        echo $this->Form->input('destaques_pdf', array('label' => __d('croogo', 'Destaques PDF'), 'value' => $destaques_string)); 
    ?>
<?php } else { ?>
    <?php
        $node_id 	= ((isset($this->params['pass'][0])) ? $this->params['pass'][0] : rand());
        $controller = $this->params['controller'];

        echo $this->Form->hidden('node_id_pdf', array('value'=> $node_id));    
    ?>
    <div id="canvas-messages" data-step="1">
        <div id="canvas-waiting" class="canvas-message">Selecione um PDF no campo abaixo para iniciar o processamento</div>
        <div id="canvas-processing" class="canvas-message" style="display:none">Processando página
            <span id="currentPage"></span> de
            <span id="numberOfPages"></span>.</div>
        <div id="canvas-submitting" class="canvas-message" style="display:none">Enviando PDF...</div>
        <div id="canvas-done" class="canvas-message" style="display:none">PDF enviado com sucesso.</div>
    </div>
    <div id="canvas-container"></div>
    <div>
        <div data-pageflip-method="POST" data-pageflip-action="/admin/pageflip/upload" data-pageflip-enctype="multipart/form-data" id="canvas-form">
            <input id="canvas-node-id" type="hidden" value="<?php echo $node_id ?>">
            <input id="canvas-controller" type="hidden" value="<?php echo $controller ?>">
            <input id="canvas-pdf" type="file">
            <button type="button" id="canvas-submit" disabled="disabled">Enviar</button>
        </div>
    </div>
    <div id="canvas-preview">
        <div class="canvas-overlay"></div>
        <div class="canvas-content">
            <button type="button" class="canvas-btn canvas-btn--prev" data-canvas="prev">Previous</button>
            <figure class="canvas-figure"></figure>
            <button type="button" class="canvas-btn canvas-btn--next" data-canvas="next">Next</button>
        </div>
    </div>
    
    <?php $this->Croogo->adminScript('PageFlip.admin'); ?>
    <?php echo $this->Html->script(array('PageFlip.mswi')); ?>
<?php } ?>

