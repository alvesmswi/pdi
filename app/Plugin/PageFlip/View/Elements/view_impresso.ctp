<?php

    $this->layout = 'ajax';
    
    $jornalName         = Configure::read('Site.title');

    $pageflipId         = $pageflip['id'];
    $pageflipName       = $pageflip['pdf'];
    $pageflipDownload   = $pageflip['url'];
    $pageflipPages      = json_decode($pageflip['images']);
    $pageflipDestaques  = $pageflip['destaques'];

    echo $this->Form->hidden('destaques', array('value' => $pageflipDestaques));

    echo $this->Html->css('PageFlip./lib/slick/slick');
    echo $this->Html->css('PageFlip./lib/lightbox2/css/lightbox.min');
    echo $this->Html->css('PageFlip.main');

    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
    echo $this->fetch('meta');

    echo "<title>{$jornalName} | {$title_for_layout}</title>";

    echo "<div class='pageflip'>";
        echo "<nav>";
            echo "<span class='navigation'>";
                echo "<a href='/'><i class='fas fa-home'></i></a>";

                if ($download) {
                    echo "<a href='{$pageflipDownload}' target='_blank' download><i class='fas fa-download'></i></a>";
                }
            echo "</span>";

            echo "<span class='title'>{$jornalName}</span> | {$title_for_layout}";
        echo "</nav>";

        echo "<div class='container'>";
            echo "<div class='pdf-viewer'>";
            
                foreach ($pageflipPages as $key => $pageflipPage) { 
                    $datapage = $key + 1;
                    $dataload_normal = '/pageflip/pdfs/normal/' . $pageflipId . '/' . $pageflipPage->image;
                    $dataload = '/pageflip/pdfs/622x799/' . $pageflipId . '/' . $pageflipPage->image;

                    echo "<a class='pdf-page' href='{$dataload_normal}' data-lightbox='{$datapage}'>";
                        echo "<img data-lazy='{$dataload}' />";
                    echo "</a>";
                }
                
            echo "</div>";
        echo "</div>";

        
    echo "</div>";
    
    echo $this->Html->script('PageFlip./lib/jquery-1.12.4', array('data-cfasync' => 'false'));
    echo $this->Html->script('PageFlip./lib/jquery-ui', array('data-cfasync' => 'false'));
    echo $this->Html->script('PageFlip./lib/slick/slick.min', array('data-cfasync' => 'false'));
    echo $this->Html->script('PageFlip./lib/fontawesome/js/fontawesome-all.min', array('data-cfasync' => 'false'));
    echo $this->Html->script('PageFlip./lib/lightbox2/js/lightbox.min', array('data-cfasync' => 'false'));
    echo $this->Html->script('PageFlip.main', array('data-cfasync' => 'false'));
