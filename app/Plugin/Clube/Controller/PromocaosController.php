<?php
App::uses('ClubeAppController', 'Clube.Controller');

class PromocaosController extends ClubeAppController {
    public $name = 'Promocaos';
    public $components = array('Mswi', 'Clube.Clube');
    public $uses = array(
        'Clube.ClubePromocao',
        'Clube.ClubeParceiro'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Auth->allow(
            array(
                'index',    
                'view',
                'portal',
                'search'
            )
        );
    }
    
    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}

    public function admin_index(){
        // condicionar os filtros
        $conditions = array();

        if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])) {

            $Filtro = $this->request->data['Filtro'];

            if (isset($Filtro['title']) && !empty($Filtro['title'])) {
                $conditions['ClubePromocao.title LIKE'] = '%'.$Filtro['title'].'%';
            }
            if (isset($Filtro['validade']) && !empty($Filtro['validade'])) {
                $conditions['ClubePromocao.validade >='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 00:00');
                $conditions['ClubePromocao.validade <='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 23:59');
            }
            if (isset($Filtro['parceiro_id']) && !empty($Filtro['parceiro_id'])) {
                $conditions['ClubePromocao.clube_parceiro_id ='] = $Filtro['parceiro_id'];
            }

            if (isset($Filtro['status']) && $Filtro['status'] != '') {
                $conditions['ClubePromocao.status'] = $Filtro['status'];
            }
        }

		$registros=$this->paginate = array(
		    'conditions' => $conditions,
            'contain' => array(
                'ClubeParceiro'
            ),
            'fields' => array(
                'ClubePromocao.id',
                'ClubePromocao.title',
                'ClubePromocao.validade',
                'ClubePromocao.status',
                'ClubePromocao.slug',
                'ClubeParceiro.nome_fantasia'
            ),
		    'order' => array('Promocao.id'=>'DESC')
		);
		$this->set('registros', $this->paginate('ClubePromocao'));


        $parceiros = $this->ClubeParceiro->find(
            'list',
            array(
                'fields' => array('id','nome_fantasia'),
                'order' => 'nome_fantasia ASC'
            )
        );
        $this->set('parceiros',$parceiros);
    }

    public function admin_add(){
        if(!empty($this->request->data)){
            if(isset($this->request->data['ClubePromocao']['banner']['name']) && !empty($this->request->data['ClubePromocao']['banner']['name'])) {                 
                $this->request->data['ClubePromocao']['banner'] = '/descontos-banners/'.$this->Clube->upload($this->request->data['ClubePromocao']['banner'], 'descontos-banners');
            }else{
                unset($this->request->data['ClubePromocao']['banner']);
            }
            //gera o slug
            $this->request->data['ClubePromocao']['slug'] = strtolower(Inflector::slug($this->data['ClubePromocao']['title'], '-'));
            //Trata a data
            if(!empty($this->request->data['ClubePromocao']['validade'])){
                $this->request->data['ClubePromocao']['validade'] = $this->Mswi->tratarData($this->request->data['ClubePromocao']['validade']);
            }
            //salva
            if($this->ClubePromocao->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/promocaos');
            }else{
                $invalidateFields = $this->ClubePromocao->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }

        $parceiros = $this->ClubeParceiro->find(
            'list',
            array(
                'conditions' => array(
                    'status' => 1
                ),
                'fields' => array('id','nome_fantasia'),
                'order' => 'nome_fantasia ASC'
            )
        );
        $this->set('parceiros',$parceiros);
    }

    public function admin_edit($id){
        if(!empty($this->request->data)){
            if(isset($this->request->data['ClubePromocao']['banner']['name']) && !empty($this->request->data['ClubePromocao']['banner']['name'])) {                 
                $this->request->data['ClubePromocao']['banner'] = '/descontos-banners/'.$this->Clube->upload($this->request->data['ClubePromocao']['banner'], 'descontos-banners');
            }else{
                unset($this->request->data['ClubePromocao']['banner']);
            }
            //Trata a data
            if(!empty($this->request->data['ClubePromocao']['validade'])){
                $this->request->data['ClubePromocao']['validade'] = $this->Mswi->tratarData($this->request->data['ClubePromocao']['validade']);
            }
            
            if($this->ClubePromocao->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/promocaos');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 

        $parceiros = $this->ClubeParceiro->find(
            'list',
            array(
                'fields' => array('id','nome_fantasia'),
                'order' => 'nome_fantasia ASC'
            )
        );
        $this->set('parceiros',$parceiros);
        
        $this->request->data = $this->ClubePromocao->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        //Trata a data
        if(!empty($this->request->data['ClubePromocao']['validade'])){
            $this->request->data['ClubePromocao']['validade'] = $this->Mswi->tratarData($this->request->data['ClubePromocao']['validade'], 'br');
        }
        $this->render('admin_add');
    }

    public function admin_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->ClubePromocao->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/clube/promocaos');
    }

    public function index(){
        // condicionar os filtros
        $conditions = array(
            'ClubePromocao.status' => 1
        );

        if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])) {

            $Filtro = $this->request->data['Filtro'];

            if (isset($Filtro['title']) && !empty($Filtro['title'])) {
                $conditions['ClubePromocao.title LIKE'] = '%'.$Filtro['title'].'%';
            }
            if (isset($Filtro['validade']) && !empty($Filtro['validade'])) {
                $conditions['ClubePromocao.validade >='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 00:00');
                $conditions['ClubePromocao.validade <='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 23:59');
            }
            if (isset($Filtro['parceiro_id']) && !empty($Filtro['parceiro_id'])) {
                $conditions['ClubePromocao.clube_parceiro_id ='] = $Filtro['parceiro_id'];
            }

            if (isset($Filtro['status']) && $Filtro['status'] != '') {
                $conditions['ClubePromocao.status'] = $Filtro['status'];
            }
        }

		$registros=$this->paginate = array(
		    'conditions' => $conditions,
            'contain' => array(
                'ClubeParceiro'
            ),
		    'order' => array('Promocao.id'=>'DESC')
		);
		$this->set('registros', $this->paginate('ClubePromocao'));

    }

    public function view(){
        $registro = $this->ClubePromocao->find(
            'first',
            array(
                'conditions' => array(
                    'ClubePromocao.slug' => $this->params->slug,
                    'ClubePromocao.status' => 1
                ),
                'contain' => array(
                    'ClubeParceiro'
                )
            )
        );
        $this->set('registro',$registro);
    }

    public function portal()
    {
        $this->loadModel('Clube.ClubeCategoria');

        // Trazer as Categorias existentes no Clube
        $categorias = $this->ClubeCategoria->find('all', array(
            'conditions' => array(
                'status' => true
            ),
            'fields' => array(
                'id',
                'slug',
                'title'
            ),
            'order' => array(
                'title' => 'ASC'
            ),
            'recursive' => -1
        ));

        // Não é a melhor solução, usando, porque o find das promoções não rolou o Contain com as Categorias
        $categorias_lista = $this->ClubeCategoria->find('list', array(
            'conditions' => array(
                'status' => true
            ),
            'fields' => array(
                'id',
                'title'
            ),
        ));
        
        // Trazer as Promoções
        $promocoes_perfeitas = array();
        $promocoes = $this->ClubePromocao->find('all', array(
            'contain' => array(
                'ClubeParceiro'
            ),
            'conditions' => array(
                'ClubePromocao.status' => true
            ),
            'fields' => array(
                'ClubePromocao.id',
                'ClubePromocao.slug',
                'ClubePromocao.title',
                'ClubePromocao.banner',
                'ClubePromocao.desconto',
                'ClubeParceiro.id',
                'ClubeParceiro.slug',
                'ClubeParceiro.nome_fantasia',
                'ClubeParceiro.clube_categoria_id'
            )
        ));

        if (!empty($promocoes)) {
            foreach ($promocoes as $key => $promocao) {
                $promocoes_perfeitas[$promocao['ClubeParceiro']['clube_categoria_id']]['descontos'][] = $promocao;
            }
        }

        $this->set('categorias', $categorias);
        $this->set('categorias_lista', $categorias_lista);
        $this->set('promocoes', $promocoes_perfeitas);
    }

    public function search()
    {
        $search = (isset($this->request->query['q']) && !empty($this->request->query['q'])) ? $this->request->query['q'] : null;

		$conditions['ClubePromocao.status'] = true;
		$conditions['ClubePromocao.title LIKE'] = "%{$search}%";
		
		if (isset($this->request->query['categoria']) && !empty($this->request->query['categoria'])) {
            $this->loadModel('Clube.ClubeCategoria');

            $categoria = $this->ClubeCategoria->findBySlug($this->request->query['categoria'], ['id']);
            $conditions['ClubeParceiro.clube_categoria_id'] = (!empty($categoria)) ? $categoria['ClubeCategoria']['id'] : null;
		}

        $total = $this->ClubePromocao->find('count', array('conditions' => $conditions));

		$this->paginate = array(
            'conditions' 	=> $conditions,
			'recursive' 	=> 2,
			'limit' 		=> 12
		);

        $promocoes = $this->paginate('ClubePromocao');
        
        $this->set('search', $search);
        $this->set('total', $total);
        $this->set('promocoes', $promocoes);
    }
}