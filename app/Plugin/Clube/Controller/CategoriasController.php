<?php
App::uses('ClubeAppController', 'Clube.Controller');

class CategoriasController extends ClubeAppController {
	public $name = 'Categorias';
    public $uses = array(
        'Clube.ClubeCategoria'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
       // $this->Security->unlockedActions += array('admin_Categorias_get');
    }
    
    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}

    public function admin_index(){

	    // condicionar os filtros
        $conditions = array();
	    if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])) {

	        $Filtro = $this->request->data['Filtro'];

            if (isset($Filtro['categoria_id']) && !empty($Filtro['categoria_id'])) {
                $conditions['ClubeCategoria.id'] = $Filtro['categoria_id'];
            }

            if (isset($Filtro['status']) && $Filtro['status'] != '') {
                $conditions['ClubeCategoria.status'] = $Filtro['status'];
            }

        }
        $this->ClubeCategoria->recursive = -1;
		$this->paginate = array(
		    'conditions' => $conditions,
		    'order' => array('ClubeCategoria.id'=>'ASC')
		);
		$registros = $this->paginate('ClubeCategoria');

		// buscar e enviar cagegorias para view
		$categorias = $this->ClubeCategoria->find(
            'list',
            array(
                'fields' => array(
                    'id', 'title'
                )
            )
        );
        $this->set('categorias',$categorias);

		$this->set('registros', $this->paginate('ClubeCategoria'));
    }

    public function admin_add(){
        if(!empty($this->request->data)){
            //gera o slug
            $this->request->data['ClubeCategoria']['slug'] = strtolower(Inflector::slug($this->data['ClubeCategoria']['title'], '-'));
            //salva
            if($this->ClubeCategoria->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/categorias');
            }else{
                $invalidateFields = $this->ClubeCategoria->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }
    }

    public function admin_edit($id){
        if(!empty($this->request->data)){
            if($this->ClubeCategoria->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/categorias');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 
        
        $this->request->data = $this->ClubeCategoria->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        $this->render('admin_add');
    }

    public function admin_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->ClubeCategoria->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/clube/categorias');
    }

}