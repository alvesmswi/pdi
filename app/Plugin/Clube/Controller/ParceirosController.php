<?php
App::uses('ClubeAppController', 'Clube.Controller');

class ParceirosController extends ClubeAppController {
    public $name = 'Parceiros';
    public $components = array(
        'Clube.Clube'
    );
    public $uses = array(
        'Clube.ClubeParceiro',
        'Clube.ClubeCategoria',
        'Clube.ClubePromocao'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
       // $this->Security->unlockedActions += array('admin_Parceiros_get');
       $this->Auth->allow(
            array(
                'index',    
                'view' 
            )
        );
    }
    
    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}

    public function admin_index(){

        // condicionar os filtros
        $conditions=array();
        if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])){

            $Filtro=$this->request->data['Filtro'];

            if (isset($Filtro['nome']) && !empty($Filtro['nome'])){
                $conditions['ClubeParceiro.nome_fantasia LIKE']='%'.$Filtro['nome'].'%';
            }
            if (isset($Filtro['categoria_id']) && !empty($Filtro['categoria_id'])){
                $conditions['ClubeCategoria.id']=$Filtro['categoria_id'];
            }
            if (isset($Filtro['cidade']) && !empty($Filtro['cidade'])){
                $conditions['ClubeParceiro.cidade']=$Filtro['cidade'];
            }
            if (isset($Filtro['bairro']) && !empty($Filtro['bairro'])){
                $conditions['ClubeParceiro.bairro']=$Filtro['bairro'];
            }
            if (isset($Filtro['UF']) && !empty($Filtro['UF'])){
                $conditions['ClubeParceiro.uf']=$Filtro['UF'];
            }
            if (isset($Filtro['status']) && $Filtro['status'] != ''){
                $conditions['ClubeParceiro.status']=$Filtro['status'];
            }
        }

        $this->paginate = array(
            'conditions' => $conditions,
            'fields' => array(
                'ClubeParceiro.id',
                'ClubeParceiro.nome_fantasia',
                'ClubeParceiro.uf',
                'ClubeParceiro.cidade',
                'ClubeParceiro.bairro',
                'ClubeParceiro.slug',
                'ClubeParceiro.status',
                'ClubeCategoria.title'
            ),
            'contain' => array(
                'ClubeCategoria'
            ),
		    'order' => array('ClubeParceiro.id'=>'ASC')
		);
		$this->set('registros', $this->paginate('ClubeParceiro'));
        $registros = $this->paginate('ClubeParceiro');

        // enviar para view
        $this->set('registros', $registros );
        // chamar e enviar categorias para view
        $this->set('categorias', $this->categorias_return());
        // chamar e enviar estados para view
        $this->set('ufs', $this->ClubeParceiro->ufs());

    }

    /*
     * Retonrnar Categorias
     */
    public  function categorias_return(){
        $categorias = $this->ClubeCategoria->find(
            'list',
            array(
                'conditions' => array(
                    'status' => 1
                ),
                'fields' => array(
                    'id', 'title'
                )
            )
        );
        return $categorias;
    }

    public function admin_add(){
        if(!empty($this->request->data)){
            if(isset($this->request->data['ClubeParceiro']['logo']['name']) && !empty($this->request->data['ClubeParceiro']['logo']['name'])) {                 
                $this->request->data['ClubeParceiro']['logo'] = '/parceiros-logo/'.$this->Clube->upload($this->request->data['ClubeParceiro']['logo'], 'parceiros-logo');  
            }else{
                unset($this->request->data['ClubeParceiro']['logo']);
            }
            //gera o slug
            $this->request->data['ClubeParceiro']['slug'] = strtolower(Inflector::slug($this->data['ClubeParceiro']['nome_fantasia'], '-'));
            //salva
            if($this->ClubeParceiro->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/parceiros');
            }else{
                $invalidateFields = $this->ClubeParceiro->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }

        $categorias = $this->ClubeCategoria->find(
            'list',
            array(
                'conditions' => array(
                    'status' => 1
                ),
                'fields' => array(
                    'id', 'title'
                )
            )
        );
        $this->set('categorias', $categorias);
        $this->set('ufs', $this->ClubeParceiro->ufs());
    }

    public function admin_edit($id){
        if(!empty($this->request->data)){
            //pr($this->request->data);exit();
            if(isset($this->request->data['ClubeParceiro']['logo']['name']) && !empty($this->request->data['ClubeParceiro']['logo']['name'])) {                 
                $this->request->data['ClubeParceiro']['logo'] = '/parceiros-logo/'.$this->Clube->upload($this->request->data['ClubeParceiro']['logo'], 'parceiros-logo');  
            }else{
                unset($this->request->data['ClubeParceiro']['logo']);
            }
            if($this->ClubeParceiro->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/parceiros');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 

        $categorias = $this->ClubeCategoria->find(
            'list',
            array(
                'fields' => array(
                    'id', 'title'
                )
            )
        );
        $this->set('categorias', $categorias);
        $this->set('ufs', $this->ClubeParceiro->ufs());
        
        $this->request->data = $this->ClubeParceiro->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        $this->render('admin_add');
    }

    public function admin_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->ClubeParceiro->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/clube/parceiros');
    }

    public function index(){
        // condicionar os filtros
        $conditions=array(
            'ClubeParceiro.status' => 1
        );
        if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])){

            $Filtro=$this->request->data['Filtro'];

            if (isset($Filtro['nome']) && !empty($Filtro['nome'])){
                $conditions['ClubeParceiro.nome_fantasia LIKE']='%'.$Filtro['nome'].'%';
            }
            if (isset($Filtro['categoria_id']) && !empty($Filtro['categoria_id'])){
                $conditions['ClubeCategoria.id']=$Filtro['categoria_id'];
            }
            if (isset($Filtro['cidade']) && !empty($Filtro['cidade'])){
                $conditions['ClubeParceiro.cidade']=$Filtro['cidade'];
            }
            if (isset($Filtro['bairro']) && !empty($Filtro['bairro'])){
                $conditions['ClubeParceiro.bairro']=$Filtro['bairro'];
            }
            if (isset($Filtro['UF']) && !empty($Filtro['UF'])){
                $conditions['ClubeParceiro.uf']=$Filtro['UF'];
            }
            if (isset($Filtro['status']) && $Filtro['status'] != ''){
                $conditions['ClubeParceiro.status']=$Filtro['status'];
            }
        }

        $this->paginate = array(
            'conditions' => $conditions,
            'fields' => array(
                'ClubeParceiro.id',
                'ClubeParceiro.nome_fantasia',
                'ClubeParceiro.uf',
                'ClubeParceiro.cidade',
                'ClubeParceiro.bairro',
                'ClubeParceiro.status',
                'ClubeParceiro.logo',
                'ClubeParceiro.slug',
                'ClubeParceiro.fone',
                'ClubeCategoria.title',
                'ClubeCategoria.slug'
            ),
            'contain' => array(
                'ClubeCategoria'
            ),
		    'order' => array('ClubeParceiro.id'=>'ASC')
		);
		$this->set('registros', $this->paginate('ClubeParceiro'));
        $registros = $this->paginate('ClubeParceiro');

        // enviar para view
        $this->set('registros', $registros );
        // chamar e enviar categorias para view
        $this->set('categorias', $this->categorias_return());
        // chamar e enviar estados para view
        $this->set('ufs', $this->ClubeParceiro->ufs());
    }

    public function view(){
  
        $registro = $this->ClubeParceiro->find(
            'first',
            array(
                'conditions' => array(
                    'ClubeParceiro.slug' => $this->params->slug,
                    'ClubeParceiro.status' => 1
                ),
                'contain' => array(
                    'ClubeCategoria'
                )
            )
        );
        
        // enviar para view
        $this->set('registro', $registro );

        if(!empty($registro)){
            $descontos = $this->ClubePromocao->find(
                'all',
                array(
                    'conditions' => array(
                        'ClubePromocao.clube_parceiro_id' => $registro['ClubeParceiro']['id'],
                        'ClubePromocao.status' => 1
                    ),
                    'recursive' => -1
                )
            );
            $this->set('descontos', $descontos );
        }   
        
    }

}