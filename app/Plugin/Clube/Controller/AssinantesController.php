<?php
App::uses('ClubeAppController', 'Clube.Controller');

class AssinantesController extends ClubeAppController {
    public $name = 'Assinantes';
    public $components = array('Mswi');
    public $uses = array(
        'Clube.ClubeAssinante',
        'Users.User',
        'Roles.Role'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
    }
    
    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->User, $id, $status);
	}

    public function admin_index(){

        // condicionar os filtros
        $conditions = array();

        if (isset($this->request->data['Filtro']) && !empty($this->request->data['Filtro'])) {

            $Filtro = $this->request->data['Filtro'];

            if (isset($Filtro['nome']) && !empty($Filtro['nome'])) {
                $conditions['User.name LIKE'] ='%'. $Filtro['nome'] .'%';
            }
            if (isset($Filtro['email']) && !empty($Filtro['email'])) {
                $conditions['User.email'] = $Filtro['email'];
            }
            if (isset($Filtro['validade']) && !empty($Filtro['validade'])) {
                $conditions['User.data_expiracao >='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 00:00');
                $conditions['User.data_expiracao <='] = $this->Mswi->tratarDataHora($Filtro['validade'].' 23:59');
            }

            if (isset($Filtro['role_id']) && $Filtro['role_id'] != '') {
                $conditions['Role.id'] = $Filtro['role_id'];
            }
            if (isset($Filtro['status']) && $Filtro['status'] != '') {
                $conditions['User.status'] = $Filtro['status'];
            }

        }


		$this->paginate = array(
            'conditions' => array(
                $conditions,
                'Role.alias' => array(
                    'assinante',
                    'cortesia',
                    'assinante-direto'
                )
            ),
            'contain' => array(
                'Role'
            ),           
            'fields' => array(
                'User.id',
                'User.name',
                'User.email',
                'User.telefone',
                'User.data_expiracao',
                'User.status',
                'Role.title'
            ),
		    'order' => array('User.id'=>'DESC')
		);
		$this->set('registros', $this->paginate('User'));
        $grupos = $this->Role->find(
            'list',
            array(
                'conditions' => array(
                    'alias' => array(
                        'assinante',
                        'cortesia',
                        'assinante-direto'
                    )
                ),
                'fields' => array('id','title'),
                'order' => 'title ASC'
            )
        );
        $this->set('grupos',$grupos);
    }

    public function admin_add(){
        if(!empty($this->request->data)){
            //salva
            if($this->User->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/Assinantes');
            }else{
                $invalidateFields = $this->User->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }

        $grupos = $this->Role->find(
            'list',
            array(
                'conditions' => array(
                    'alias' => array(
                        'assinante',
                        'cortesia',
                        'assinante-direto'
                    )
                ),
                'fields' => array('id','title'),
                'order' => 'title ASC'
            )
        );
        $this->set('grupos',$grupos);

        $this->set('ufs', $this->ClubeAssinante->ufs());
    }

    public function admin_edit($id){
        if(!empty($this->request->data)){
            $arrayUpdate = array();            
            //Trata a data
            if(!empty($this->request->data['User']['data_expiracao'])){
                $arrayUpdate['User.data_expiracao'] = "'".$this->Mswi->tratarData($this->request->data['User']['data_expiracao'])."'";
            }
            if(!empty($this->request->data['User']['password'])){
                $arrayUpdate['User.password'] = $this->Mswi->tratarData($this->request->data['User']['password']);
            }
            $arrayUpdate['User.role_id'] = $this->request->data['User']['role_id'];
            $arrayUpdate['User.name'] = "'".$this->request->data['User']['name']."'";
            $arrayUpdate['User.email'] = "'".$this->request->data['User']['email']."'";
            $arrayUpdate['User.cpf'] = "'".$this->request->data['User']['cpf']."'";
            $arrayUpdate['User.telefone'] = "'".$this->request->data['User']['telefone']."'";
            $arrayUpdate['User.uf'] = "'".$this->request->data['User']['uf']."'";
            $arrayUpdate['User.cidade'] = "'".$this->request->data['User']['cidade']."'";
            $arrayUpdate['User.bairro'] = "'".$this->request->data['User']['bairro']."'";
            $arrayUpdate['User.rua'] = "'".$this->request->data['User']['rua']."'";
            $arrayUpdate['User.numero'] = "'".$this->request->data['User']['numero']."'";
            $arrayUpdate['User.cep'] = "'".$this->request->data['User']['cep']."'";
            $arrayUpdate['User.status'] = $this->request->data['User']['status'];
            
            $update = $this->User->updateAll(
                $arrayUpdate,
                array(
                    'User.id' => $id
                )
            );

            if($update){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/clube/Assinantes');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 
        
        $grupos = $this->Role->find(
            'list',
            array(
                'conditions' => array(
                    'alias' => array(
                        'assinante',
                        'cortesia',
                        'assinante-direto'
                    )
                ),
                'fields' => array('id','title'),
                'order' => 'title ASC'
            )
        );
        $this->set('grupos',$grupos);

        $this->set('ufs', $this->ClubeAssinante->ufs());

        $this->request->data = $this->User->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        //Trata a data
        if(!empty($this->request->data['User']['validade'])){
            $this->request->data['User']['validade'] = $this->Mswi->tratarData($this->request->data['User']['validade'], 'br');
        }
        unset($this->request->data['User']['password']);
        $this->render('admin_add');
    }

    public function admin_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->User->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/clube/Assinantes');
    }

}