<style>
.o-classificados_price:before {
  content: "";
}
.o-classificados_price:after {
  content: "\00a0%";
}
</style>

<?php
$this->start('classificados_header');
    echo '<div class="o-wrapper">';
        echo $this->element('Clube.big_banner'); 
    echo '</div>';
$this->end();
?>

<?php foreach ($promocoes as $key => $promocao): ?>
    <section class="s-list u-spacer_bottom">
        <header class="o-header">
            <span class="o-header_title"><?php echo $categorias_lista[$key] ?></span> 
            <!-- <a href="<?php echo $key ?>" class="o-header_read-more">
            Veja todos <i class="i-icon i-icon_caret-right"></i>
            </a> -->
        </header>
        <div class="row">
            <?php foreach ($promocao['descontos'] as $key => $desconto) { ?>
                <div class="col-sm-3 col-xs-6">
                    <div class="o-classificados">
                        <a href="/clube/parceiro/<?php echo $desconto['ClubeParceiro']['slug'] ?>" class="o-classificados_category">
                            <?php echo $desconto['ClubeParceiro']['nome_fantasia'] ?>
                        </a>
                        <a href="/clube/desconto/<?php echo $desconto['ClubePromocao']['slug'] ?>" class="o-classificados_content">
                            <?php if(!empty($desconto['ClubePromocao']['banner'])) { ?>
                            <img src="<?php echo $desconto['ClubePromocao']['banner']; ?>" class="o-classificados_image"> 
                            <?php } ?>
                            <span class="o-classificados_title"><?php echo $desconto['ClubePromocao']['title'] ?></span>
                            <span class="o-classificados_price"><?php echo $desconto['ClubePromocao']['desconto'] ?></span>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php endforeach; ?>