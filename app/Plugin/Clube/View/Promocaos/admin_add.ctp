<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Taxonomy.terms');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Promoções', array('plugin' => 'clube', 'controller' => 'promocaos', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html
	->addCrumb('Editar', '/' . $this->request->url);
}
if ($this->request->params['action'] == 'admin_add') {
	$this->Html
		->addCrumb('Adicionar', '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('ClubePromocao', array(
    'class' => 'protected-form',
    'enctype' => 'multipart/form-data'
)));

$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab('Promoção / Benefício', '#term-basic');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	
	echo $this->Html->tabStart('term-basic') .
		$this->Form->hidden('id') .
		$this->Form->input('clube_parceiro_id', array(
			'label' => 'Parceiro',			
			'options' => $parceiros,
			'empty' => true,
			'required' => true
		)) . 
		$this->Form->input('title', array(
			'label' => 'Titulo',
			'required' => true
		)) . 
		$this->Form->input('descricao', array(
			'label' => 'Descrição',
			'type' => 'texarea',
			'rows' => 2
		)) . 
		$this->Form->input('desconto', array(
			'label' => 'Desconto (Percentual)',
			'type' => 'number',
			'max' => 100
		)) . 
		$this->Form->input('validade', array(
			'label' => 'Validade',
			'type' => 'text',
			'class' => 'datetime2',
			'style' => 'text-align:left;',
			'before' => '<i class="fa fa-calendar"></i>'
		)) .
		$this->Form->input('banner', array(
            'label' => 'Banner',
            'type' => 'file',
            'required' => false
        ));

        if(isset($this->data['ClubePromocao']['banner']) && !empty($this->data['ClubePromocao']['banner'])){                
			echo '<img src="'.Router::url('/', true).$this->data['ClubePromocao']['banner'].'" class="thumbnail" />';                
		}		
	echo $this->Html->tabEnd();
	
	?>
		<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.datetime2').datetimepicker({
					dateFormat: 'dd/mm/yy',
					timeFormat: 'hh:mm'
				})
			;});
		</script>
	<?php
	echo $this->Croogo->adminTabs();

$this->end();

$this->start('panels');
	echo $this->Html->beginBox('Opções') .
		$this->Form->button('Salvar', array('button' => 'success')) .
		$this->Html->link(
			'Cancelar',
			array('action' => 'index'),
			array('button' => 'danger')
		) .
		$this->Form->input('status', array(
			'label' => 'Ativo',
			'type' => 'checkbox',
			'default' => 1
		)) . 
		$this->Html->endBox();

		echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
?>
