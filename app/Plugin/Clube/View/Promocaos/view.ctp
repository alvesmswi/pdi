<?php //pr($registro); ?>
<div class="desconto" id="desconto-<?php echo $registro['ClubeParceiro']['id']; ?>">        
    <div class="row list">
        <div class="list-item">
            <?php if(isset($registro['ClubePromocao']['banner']) && !empty($registro['ClubeParceiro']['banner'])) {?>
                <div class="col-md-3">     
                    <img src="<?php echo $registro['ClubePromocao']['banner']; ?>" class="thumb" />   
                </div>
                <div class="col-md-9">
            <?php }else{ ?>
                <div class="col-md-12">
            <?php } ?>
                <h2 class="title"><?php echo $registro['ClubePromocao']['title']; ?></h2>
                <p class="categoria"><?php echo $registro['ClubePromocao']['title']; ?></p>                        
                <p class="descricao"><?php echo $registro['ClubePromocao']['descricao']; ?></p>  
                <a href="/clube/parceiro/<?php echo $registro['ClubeParceiro']['slug']; ?>">
                    <p class="title"><?php echo $registro['ClubeParceiro']['nome_fantasia']; ?></p>    
                </a>                   
                <p class="endereco"><?php echo $registro['ClubeParceiro']['cidade']; ?> - <?php echo $registro['ClubeParceiro']['uf']; ?></p>
                <p class="fone"><?php echo $registro['ClubeParceiro']['fone']; ?></p>  
            </div>
        </div>
    </div>    
</div>