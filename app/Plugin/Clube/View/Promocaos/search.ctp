<style>
.o-classificados_price:before {
  content: "";
}
.o-classificados_price:after {
  content: "\00a0%";
}
</style>

<?php 
$this->start('classificados_header');
    echo '<div class="o-wrapper">'; ?>
    <div class="o-classificados_header">
        <h2 class="o-classificados_header-title">
            <i class="i-icon i-icon_search"></i>Buscar "<?php echo $search ?>"
        </h2>
        <form method="GET" action="/clube/search" class="o-classificados_header-form">
            <label class="sr-only" for="classificadosHeaderSearch">Pesquisar</label>
            <input type="text" class="o-classificados_header-input" name="q" id="classificadosHeaderSearch" placeholder="Pesquisar no Clube">
            <button type="submit" class="o-classificados_header-btn"><i class="i-icon i-icon_search"></i>Pesquisar</button>
            <div class="o-classificados_header-backdrop" aria-hidden="true"></div>
        </form>
    </div>
    <?php
    echo '</div>';
$this->end();

$this->Helpers->load('Medias.MediasImage');
?>

<div class="o-classificados_list">
  <header class="o-classificados_list-header">
    <!-- <div class="dropdown"><a href="/" class="dropdown-toggle" data-toggle="dropdown">Ordenar por <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><?php #echo $this->Paginator->sort('publish_start', 'Mais recentes', array('direction' => 'desc')); ?></li>
        <li><?php #echo $this->Paginator->sort('preco', 'Menor valor', array('direction' => 'asc')); ?></li>
        <li><?php #echo $this->Paginator->sort('preco', 'Maior valor', array('direction' => 'desc')); ?></li>
      </ul>
    </div> -->
    <span class="o-classificados_n-anuncios"><?php echo $total ?> Desconto(s)</span>
  </header>
  <div class="row">
    <?php foreach ($promocoes as $key => $promocao) { ?>
      <div class="col-sm-4 col-xs-6">
        <div class="o-classificados">
          <a href="/clube/desconto/<?php echo $promocao['ClubePromocao']['slug'] ?>" class="o-classificados_content">
            <img src="<?php echo $promocao['ClubePromocao']['banner'] ?>" class="o-classificados_image"> 
            <span class="o-classificados_title"><?php echo $promocao['ClubePromocao']['title'] ?></span>
            <span class="o-classificados_price"><?php echo $promocao['ClubePromocao']['desconto'] ?></span>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php echo $this->element('paginator'); ?>
</div>
