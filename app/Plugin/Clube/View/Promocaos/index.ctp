<div class="section-title">
    <h2 class="h6 header-color inline-block uppercase">Clube do Assinante :: Descontos</h2>
</div>

<br />

<?php
/*
echo $this->Form->create('Filtro',
	array(
		'class' => 'form-inline clearfix filter'
	)
);

echo $this->Form->input('title',
	array(
		'label' => false,
		'placeholder' => 'Procurar pelo título',
	)
);

echo $this->Form->submit('Procurar',
    array(
        'class' => 'btn btn-default submit-button',
        'div' => 'input text',
        'onclick' => "$('#divLoading').show();"
    )
);

echo '<a href='.$this->here.' class="btn ">Limpar</a>';*/

echo $this->Form->end();


if(!empty($registros)){ ?>
    <div class="parceiros">        
        <?php
        foreach($registros as $registro){ ?>
            <div class="row list">
                <div class="list-item">
                    <a href="/clube/desconto/<?php echo $registro['ClubePromocao']['slug']; ?>">
                        <?php 
                        if(isset($registro['ClubePromocao']['banner']) && !empty($registro['ClubePromocao']['banner'])) {
                            ?>
                            <div class="col-md-3">     
                                <img src="<?php echo $registro['ClubePromocao']['banner']; ?>" class="thumb" />   
                            </div>
                            <div class="col-md-9">
                        <?php }else{ ?>
                            <div class="col-md-12">
                        <?php } ?>
                            <h2 class="title"><?php echo $registro['ClubePromocao']['title']; ?></h2>
                            <p class="categoria"><?php echo $registro['ClubePromocao']['title']; ?></p>                        
                            <p class="descricao"><?php echo $registro['ClubePromocao']['descricao']; ?></p>  
                            <a href="/clube/parceiro/<?php echo $registro['ClubeParceiro']['slug']; ?>">
                                <p class="title"><?php echo $registro['ClubeParceiro']['nome_fantasia']; ?></p>    
                            </a>                   
                            <p class="endereco"><?php echo $registro['ClubeParceiro']['cidade']; ?> - <?php echo $registro['ClubeParceiro']['uf']; ?></p>
                            <p class="fone"><?php echo $registro['ClubeParceiro']['fone']; ?></p>  
                        </div>
                    </a>
                </div>
            </div>
            <br />
        <?php } ?>       
    </div>
<?php } ?>