<div class="c-classificados_banner" style="background-image: url(../images/clube-banner-bg.jpg);">
    <h1 class="c-classificados_banner-title">
        <?php if (Configure::read('Clube.faleconosco')): ?>
            <a href="<?php echo Configure::read('Clube.faleconosco'); ?>">
                <?php echo (Configure::read('Clube.titulo')) ? Configure::read('Clube.titulo') : 'Clube de Assinantes'; ?>
            </a>
        <?php else: ?>
            <?php echo (Configure::read('Clube.titulo')) ? Configure::read('Clube.titulo') : 'Clube de Assinantes'; ?>
        <?php endif; ?>
    </h1>

    <div class="c-classificados_search">
        <div class="tab-content c-classificados_panes">
            <div class="c-classificados_pane" id="busca-promocao">
                <form action="/clube/search/" method="get" class="c-classificados_form">
                    <?php if (!empty($categorias)): ?>
                        <div class="c-classificados_group c-classificados_select-group">
                            <select class="c-classificados_select selectpicker" name="categoria">
                                <?php foreach ($categorias as $key => $categoria): ?>
                                    <option value="<?php echo $categoria['ClubeCategoria']['slug'] ?>">
                                        <?php echo $categoria['ClubeCategoria']['title'] ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif; ?>
                    <div class="c-classificados_group c-classificados_input-group">
                        <label for="input-search" class="sr-only"></label>
                        <input type="search" class="c-classificados_input" name="q" placeholder="Digite a promoção para buscar" id="input-search">
                    </div>
                    <div class="c-classificados_group c-classificados_submit-group">
                        <button type="submit" class="c-classificados_btn">
                            <i class="i-icon i-icon_search"></i><span class="hidden-xs">Pesquisar</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>