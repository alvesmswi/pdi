<?php
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Categorias', array('plugin' => 'clube', 'controller' => 'categorias', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html
	->addCrumb('Editar', '/' . $this->request->url);
}
if ($this->request->params['action'] == 'admin_add') {
	$this->Html
		->addCrumb('Adicionar', '/' . $this->request->url);
}

$this->Form->create('ClubeCategoria', array('url' => '/' . $this->request->url));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab('ClubeCategoria', '#term-basic');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	
	echo $this->Html->tabStart('term-basic') .
		$this->Form->hidden('id') .
		$this->Form->input('title', array(
			'label' => 'Titulo da Categoria',
		));		
	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->start('panels');
	echo $this->Html->beginBox('Opções') .
		$this->Form->button('Salvar', array('button' => 'success')) .
		$this->Html->link(
			'Cancelar',
			array('action' => 'index'),
			array('button' => 'danger')
		) .
		$this->Form->input('status', array(
			'label' => 'Ativo',
			'type' => 'checkbox',
			'default' => 1
		)) . 
		$this->Html->endBox();

		echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
