<?php

$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Categorias', array('plugin' => 'Clube', 'controller' => 'categorias', 'action' => 'index'));

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Novo Registro',
		array(
			'action' => 'add'
		)
	);
$this->end();

$this->append('form-start');
echo $this->Form->create('Filtro',
	array(
		'class' => 'form-inline clearfix filter'
	)
);
echo $this->Form->input('categoria_id',
	array(
		'options' => $categorias,
		'empty' => 'Categorias',
		'default' => 'empty',
		'label' => false,
		'placeholder' => 'Categorias',
	)
);
echo $this->Form->input('status',
	array(
		'label' => false,
		'empty' => 'Todos',
		'placeholder' => 'Situação',
		'options' => array('0' => 'inativo', '1' => 'ativo'),
	)
);


echo $this->Form->submit('Filtrar',
	array(
		'class' => 'btn btn-default btn btn-warning ',
		'div' => 'input text',
		'onclick' => "$('#divLoading').show();"
	)
);

echo '<a href='.$this->here.' class="btn btn-default">Limpar</a>';

echo $this->Form->end();
$this->end();

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		'ID',
		'Titulo',
		'Slug',
		'Ativo',
		'Ações',
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();

	foreach ($registros as $registro):
		$actions = array();
		$actions[] = $this->Croogo->adminRowActions($registro['ClubeCategoria']['id']);
		$actions[] = $this->Croogo->adminRowAction('',
			array('action' => 'edit', $registro['ClubeCategoria']['id']),
			array('icon' => $this->Theme->getIcon('update'), 'tooltip' => 'Editar')
		);
		$actions = $this->Html->div('item-actions', implode(' ', $actions));

		$rows[] = array(
			$registro['ClubeCategoria']['id'],
			$registro['ClubeCategoria']['title'],
			$registro['ClubeCategoria']['slug'],
			$this->element('admin/toggle', array(
				'id' => $registro['ClubeCategoria']['id'],
				'status' => (int)$registro['ClubeCategoria']['status'],
				'model' => ''
			)),
			$actions,
		);
	endforeach;
	echo $this->Html->tableCells($rows);
$this->end();

?>
