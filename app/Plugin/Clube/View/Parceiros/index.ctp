<div class="section-title">
    <h2 class="h6 header-color inline-block uppercase">Clube do Assinante :: Parceiros</h2>
</div>

<br />
<?php

  /*  echo $this->Form->create('Filtro',
        array(
            'class' => 'form-inline clearfix filter'
        )
    );
    echo $this->Form->input('nome',
        array(
            'label' => false,
            'placeholder' => 'Nome',
            'div' => false,

		)
    );
    echo $this->Form->input('categoria_id',
        array(
            'options' => $categorias,
            'empty' => 'Categoria',
			'default' => 'empty',
            'placeholder' => 'Categorias',
            'div' => 'input text',
            'label' => false
		)
    );
    echo $this->Form->input('cidade',
        array(
            'label' => false,
            'placeholder' => 'Cidade',
        )
    );
    echo $this->Form->input('bairro',
        array(
            'label' => false,
            'div' => false,
            'placeholder' => 'Bairro',
		)
    );
    echo $this->Form->submit('Filtrar',
        array(
            'class' => 'btn btn-default submit-button',
            'div' => 'input text',
            'onclick' => "$('#divLoading').show();"
        )
    );

    echo '<a href='.$this->here.' class="btn ">Limpar</a>';

echo $this->Form->end();*/

if(!empty($registros)){ ?>
    <div class="parceiros">        
        <?php
        foreach($registros as $registro){ ?>
            <div class="row list" style="margin-bottom: 20px;">
                <div class="list-item">
                    <a href="/clube/parceiro/<?php echo $registro['ClubeParceiro']['slug']; ?>">
                        <?php if(isset($registro['ClubeParceiro']['logo']) && !empty($registro['ClubeParceiro']['logo'])) {?>
                            <div class="col-md-3">     
                                <img src="<?php echo $registro['ClubeParceiro']['logo']; ?>" class="thumb" />   
                            </div>
                            <div class="col-md-9">
                        <?php }else{ ?>
                            <div class="col-md-12">
                        <?php } ?>
                            <h2 class="title"><?php echo $registro['ClubeParceiro']['nome_fantasia']; ?></h2>
                            <p class="categoria"><?php echo $registro['ClubeCategoria']['title']; ?></p>                        
                            <p class="endereco"><?php echo $registro['ClubeParceiro']['cidade']; ?> - <?php echo $registro['ClubeParceiro']['uf']; ?></p>
                            <p class="fone"><?php echo $registro['ClubeParceiro']['fone']; ?></p>  
                        </div>
                    </a>
                </div>
            </div>
            <br />
        <?php } ?>       
    </div>
<?php } ?>