<?php //pr($registro); ?>
<div class="section-title">
    <h2 class="h6 header-color inline-block uppercase">Clube do Assinante :: Parceiro</h2>
</div>
<br />
<div class="parceiro" id="parceiro-<?php echo $registro['ClubeParceiro']['id']; ?>">        
    <div class="row list">
        <div class="list-item">
            <?php if(isset($registro['ClubeParceiro']['logo']) && !empty($registro['ClubeParceiro']['logo'])) {?>
                <div class="col-md-3">     
                    <img src="<?php echo $registro['ClubeParceiro']['logo']; ?>" class="thumb" />   
                </div>
                <div class="col-md-9">
            <?php }else{ ?>
                <div class="col-md-12">
            <?php } ?>
                <h2 class="title"><?php echo $registro['ClubeParceiro']['nome_fantasia']; ?></h2>
                <p class="categoria"><?php echo $registro['ClubeCategoria']['title']; ?></p>     
                <p class="endereco"><?php echo $registro['ClubeParceiro']['rua']; ?></p>                     
                <p class="endereco"><?php echo $registro['ClubeParceiro']['cidade']; ?> - <?php echo $registro['ClubeParceiro']['uf']; ?></p>
                <p class="fone"><?php echo $registro['ClubeParceiro']['fone']; ?></p>  
            </div>
        </div>
    </div>    

    <?php if($descontos) { ?>
        <br />
        <div class="section-title">
            <h2 class="h6 header-color inline-block uppercase">Clube do Assinante :: Descontos</h2>
        </div>
        <br />

        <?php foreach($descontos as $desconto) { ?>
            <div class="row list">
                <div class="list-item">
                    <a href="/clube/desconto/<?php echo $desconto['ClubePromocao']['slug']; ?>">
                        <?php if(isset($desconto['ClubePromocao']['banner']) && !empty($desconto['ClubePromocao']['banner'])) {?>
                            <div class="col-md-3">     
                                <img src="<?php echo $desconto['ClubePromocao']['banner']; ?>" class="thumb" />   
                            </div>
                            <div class="col-md-9">
                        <?php }else{ ?>
                            <div class="col-md-12">
                        <?php } ?>
                            <h2 class="title"><?php echo $desconto['ClubePromocao']['title']; ?></h2>
                            <p class="categoria"><?php echo $desconto['ClubePromocao']['descricao']; ?></p>     
                            <p class="fone"><?php echo date('h/m/Y', strtotime($desconto['ClubePromocao']['validade'])); ?></p>  
                        </div>
                    </a>
                </div>
            </div> 
        <?php } ?>
    <?php } ?>
        
    <a href="/clube/descontos" class="btn btn-default" style="margin: 20px 0; width: 100%;">Ver todas as promoções</a>

</div>