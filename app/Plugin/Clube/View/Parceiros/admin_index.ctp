<?php

$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Parceiros', array('plugin' => 'clube', 'controller' => 'parceiros', 'action' => 'index'));

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Novo Registro',
		array(
			'action' => 'add'
		)
	);
$this->end();

$this->append('form-start');
    echo $this->Form->create('Filtro',
        array(
            'class' => 'form-inline clearfix filter'
        )
    );
    echo $this->Form->input('nome',
        array(
            'label' => false,
            'placeholder' => 'Nome',
            'style' => 'width: 160px;',

		)
    );
    echo $this->Form->input('categoria_id',
        array(
            'options' => $categorias,
            'empty' => 'Categorias',
			'default' => 'empty',
			'label' => false,
            'placeholder' => 'Categorias',
		)
    );
    echo $this->Form->input('cidade',
        array(
            'label' => false,
            'placeholder' => 'Cidade',
			'style' => 'width: 160px;',
        )
    );
    echo $this->Form->input('bairro',
        array(
            'label' => false,
            'placeholder' => 'Bairro',
			'style' => 'width: 160px;',
		)
    );
    echo $this->Form->input('UF',
        array(
            'options' => $ufs,
            'label' => false,
            'default' => 'PR',
            'placeholder' => 'Estado',
            'style' => 'width: 160px;',

        )
    );

    echo $this->Form->input('status',
        array(
            'label' => false,
            'empty' => 'Todos',
            'placeholder' => 'Situação',
            'options' => array('0' => 'inativo', '1' => 'ativo'),
        )
    );


    echo $this->Form->submit('Filtrar',
        array(
            'class' => 'btn btn-default btn btn-warning ',
            'div' => 'input text',
            'onclick' => "$('#divLoading').show();"
        )
    );

    echo '<a href='.$this->here.' class="btn btn-default">Limpar</a>';

echo $this->Form->end();
$this->end();

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		'ID',
		'Nome',
		'Categoria',
		'Bairro',
		'Cidade',
		'UF',
		'Ativo',
		'Ações'
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();

	foreach ($registros as $registro):
		$actions = array();
		$actions[] = $this->Croogo->adminRowActions($registro['ClubeParceiro']['id']);
		$actions[] = $this->Croogo->adminRowAction('',
			array('action' => 'edit', $registro['ClubeParceiro']['id']),
			array('icon' => $this->Theme->getIcon('update'), 'tooltip' => 'Editar')
		);
		$actions = $this->Html->div('item-actions', implode(' ', $actions));

		$rows[] = array(
            $registro['ClubeParceiro']['id'],
            '<a href="/clube/parceiro/'.$registro['ClubeParceiro']['slug'].'" target="_blank">'.$registro['ClubeParceiro']['nome_fantasia'].'</a>',
			$registro['ClubeCategoria']['title'],
			$registro['ClubeParceiro']['bairro'],
			$registro['ClubeParceiro']['cidade'],
			$registro['ClubeParceiro']['uf'],
			$this->element('admin/toggle', array(
				'id' => $registro['ClubeParceiro']['id'],
				'status' => (int)$registro['ClubeParceiro']['status'],
				'model' => ''
			)),
			$actions,
		);
	endforeach;
	echo $this->Html->tableCells($rows);
$this->end();

?>
