<?php
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Parceiro', array('plugin' => 'clube', 'controller' => 'parceiros', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html
	->addCrumb('Editar', '/' . $this->request->url);
}
if ($this->request->params['action'] == 'admin_add') {
	$this->Html
		->addCrumb('Adicionar', '/' . $this->request->url);
}

$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('form-start', $this->Form->create('ClubeParceiro', array(
    'class' => 'protected-form',
    'enctype' => 'multipart/form-data'
)));

$this->append('tab-heading');
	echo $this->Croogo->adminTab('Parceiro', '#term-basic');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	
	echo $this->Html->tabStart('term-basic') .
		$this->Form->hidden('id') .
		$this->Form->input('clube_categoria_id', array(
			'label' => 'Categoria',
			'options' => $categorias
		)) . 
		$this->Form->input('razao_social', array(
			'label' => 'Razão Social',
		)) . 
		$this->Form->input('nome_fantasia', array(
			'label' => 'Nome Fantasia',
		)) . 
		$this->Form->input('cnpj', array(
			'label' => 'CNPJ',
			'class' => 'cnpj'
		)) . 
		$this->Form->input('fone', array(
			'label' => 'Telefone',
			'class' => 'fone'
		)) . 
		$this->Form->input('uf', array(
			'label' => 'UF (Estado)',
			'options' => $ufs,
			'default' => 'PR'
		)) . 
		$this->Form->input('cidade', array(
			'label' => 'Cidade',
		)) . 
		$this->Form->input('bairro', array(
			'label' => 'Bairro',
		)) . 
		$this->Form->input('rua', array(
			'label' => 'Rua',
		)) . 
		$this->Form->input('numero', array(
			'label' => 'Número',
			'type' => 'number'
		)) . 
		$this->Form->input('cep', array(
			'label' => 'CEP',
			'class' => 'cep'
		)) .
		$this->Form->input('latitude', array(
			'label' => 'Latitude (Preencher para poder exibir o mapa no aplicativo)',
		)) . 
		$this->Form->input('longitude', array(
			'label' => 'Longitude (Preencher para poder exibir o mapa no aplicativo)',
		)) . 
		$this->Form->input('logo', array(
            'label' => 'Logo',
            'type' => 'file',
            'required' => false
        ));

        if(isset($this->data['ClubeParceiro']['logo']) && !empty($this->data['ClubeParceiro']['logo'])){                
			echo '<img src="'.Router::url('/', true).$this->data['ClubeParceiro']['logo'].'" class="thumbnail" />';                
		}
	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->start('panels');
	echo $this->Html->beginBox('Opções') .
		$this->Form->button('Salvar', array('button' => 'success')) .
		$this->Html->link(
			'Cancelar',
			array('action' => 'index'),
			array('button' => 'danger')
		) .
		$this->Form->input('status', array(
			'label' => 'Ativo',
			'type' => 'checkbox',
			'default' => 1
		)) . 
		$this->Html->endBox();

		echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
