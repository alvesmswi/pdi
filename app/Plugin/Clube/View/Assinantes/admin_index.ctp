<?php

$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Assinantes', array('plugin' => 'clube', 'controller' => 'assinantes', 'action' => 'index'));

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Novo Registro',
		array(
			'action' => 'add'
		)
	);
$this->end();

$this->append('form-start');
echo $this->Form->create('Filtro',
	array(
		'class' => 'form-inline clearfix filter'
	)
);

echo $this->Form->input('nome',
	array(
		'label' => false,
		'placeholder' => 'Nome',
	)
);

echo $this->Form->input('email',
	array(
		'label' => false,
		'placeholder' => 'email',
	)
);

echo $this->Form->input('role_id',
	array(
		'options' => $grupos,
		'empty' => 'Grupo',
		'label' => false,
		'placeholder' => 'Grupo',
	)
);
echo $this->Form->input('status',
	array(
		'label' => false,
		'empty' => 'Todos',
		'placeholder' => 'Situação',
		'options' => array('0' => 'inativo', '1' => 'ativo'),
	)
);


echo $this->Form->input('validade', array(
	'label' => false,
	'class' => 'datepicker',
	'placeholder' => 'Validade',
	'style' => 'text-align:left; width: 90px',
	'before' => '<i class="fa fa-calendar"></i>'
)) ;
?>
<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
<script>

	$( function() {

		$( ".datepicker" ).datepicker({
			showButtonPanel: true,
			showHour:false,
			showMinute:false,
			selectOtherMonths: true,
			dateFormat: 'dd/mm/yy',
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			currentText:'Hoje',
			closeText:'Pronto'
		});

	} );
</script>
<?php
echo $this->Form->submit('Filtrar',
	array(
		'class' => 'btn btn-default btn btn-warning ',
		'div' => 'input text',
		'onclick' => "$('#divLoading').show();"
	)
);

echo '<a href='.$this->here.' class="btn btn-default">Limpar</a>';

echo $this->Form->end();
$this->end();

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		'ID',
		'Nome',
		'E-Mail',
		'Telefone',
		'Grupo',
		'Validade',
		'Ativo',
		'Ações'
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();

	foreach ($registros as $registro):
		$actions = array();
		$actions[] = $this->Croogo->adminRowActions($registro['User']['id']);
		$actions[] = $this->Croogo->adminRowAction('',
			array('action' => 'edit', $registro['User']['id']),
			array('icon' => $this->Theme->getIcon('update'), 'tooltip' => 'Editar')
		);
		$actions = $this->Html->div('item-actions', implode(' ', $actions));

		$rows[] = array(
			$registro['User']['id'],
			$registro['User']['name'],
			$registro['User']['email'],
			$registro['User']['telefone'],
			$registro['Role']['title'],
			$registro['User']['data_expiracao'] ? date('d/m/Y', strtotime($registro['User']['data_expiracao'])) : '-',
			$this->element('admin/toggle', array(
				'id' => $registro['User']['id'],
				'status' => (int)$registro['User']['status'],
				'model' => ''
			)),
			$actions,
		);
	endforeach;
	echo $this->Html->tableCells($rows);
$this->end();

?>
