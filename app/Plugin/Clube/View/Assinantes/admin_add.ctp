<?php
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Assinante', array('plugin' => 'clube', 'controller' => 'assinantes', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html
	->addCrumb('Editar', '/' . $this->request->url);
}
if ($this->request->params['action'] == 'admin_add') {
	$this->Html
		->addCrumb('Adicionar', '/' . $this->request->url);
}

$this->Form->create('User', array('url' => '/' . $this->request->url));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab('Assinante', '#basic');
	echo $this->Croogo->adminTab('Endereço', '#end');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	
	echo $this->Html->tabStart('basic') .
		$this->Form->hidden('id') .
		$this->Form->input('role_id', array(
			'label' => 'Grupo',
			'options' => $grupos
		)) . 
		$this->Form->input('name', array(
			'label' => 'Nome Completo',
		)) . 
		$this->Form->input('email', array(
			'label' => 'E-mail',
			'type' => 'email'
		)) . 
		$this->Form->input('cpf', array(
			'label' => 'CPF',
			'class' => 'cpf'
		)) . 
		$this->Form->input('telefone', array(
			'label' => 'Telefone',
			'class' => 'fone'
		));
	echo $this->Html->tabEnd();

	echo $this->Html->tabStart('end') .

		$this->Form->input('uf', array(
			'label' => 'UF (Estado)',
			'options' => $ufs,
			'default' => 'PR'
		)) . 
		$this->Form->input('cidade', array(
			'label' => 'Cidade',
		)) . 
		$this->Form->input('bairro', array(
			'label' => 'Bairro',
		)) . 
		$this->Form->input('rua', array(
			'label' => 'Rua',
		)) . 
		$this->Form->input('numero', array(
			'label' => 'Número',
			'type' => 'number'
		)) . 
		$this->Form->input('cep', array(
			'label' => 'CEP',
			'class' => 'cep'
		));		
	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->start('panels');
	echo $this->Html->beginBox('Opções') .
		$this->Form->button('Salvar', array('button' => 'success')) .
		$this->Html->link(
			'Cancelar',
			array('action' => 'index'),
			array('button' => 'danger')
		) .
		$this->Form->input('status', array(
			'label' => 'Ativo',
			'type' => 'checkbox',
			'default' => 1
		)) .
		$this->Form->input('password', array(
			'label' => 'Senha',
			'type' => 'password',
			'default' => '',
			'autocomplete' => 'off',
			'value' => '',
			'required' => false
		)) .  
		$this->Form->input('data_expiracao', array(
			'label' => 'Data de Expiração',
			'type' => 'text',
			'class' => 'datetime2',
			'style' => 'text-align:left;',
			'required' => false
		)) .
		
		$this->Html->endBox();
		?>
		<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.datetime2').datetimepicker({
					dateFormat: 'dd/mm/yy',
					timeFormat: 'hh:mm'
				})
			});
		</script>
		<?php

		echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
?>
