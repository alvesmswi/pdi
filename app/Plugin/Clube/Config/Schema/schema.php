<?php 
class ClubeSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $clube_parceiros = array(
		'id' 					=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'clube_categoria_id' 	=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'nome_fantasia' 		=> array('type' => 'string', 'length' => 255, 'null' => true),
		'razao_social' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'cnpj' 					=> array('type' => 'string', 'length' => 255, 'null' => true),
		'fone'		 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'uf' 					=> array('type' => 'string', 'length' => 255, 'null' => true),
		'cidade' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'bairro' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'rua' 					=> array('type' => 'string', 'length' => 255, 'null' => true),
		'numero' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'cep' 					=> array('type' => 'string', 'length' => 255, 'null' => true),
		'site' 					=> array('type' => 'string', 'length' => 255, 'null' => true),	
		'slug' 					=> array('type' => 'string', 'length' => 255, 'null' => true),
		'latitude' 				=> array('type' => 'string', 'length' => 64, 'null' => true),
		'longitude' 			=> array('type' => 'string', 'length' => 64, 'null' => true),
		'status' 				=> array('type' => 'integer', 'length' => 11, 'null' => true),		
		'logo' 					=> array('type' => 'string', 'length' => 255, 'null' => true),	
		'created' 				=> array('type' => 'timestamp', 'null' => true),
		'modified' 				=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 				=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'clube_categoria_id' => array(
				'column' => array('clube_categoria_id'),
			),
			'slug' => array(
				'column' => array('slug'), 
				'unique' => true
			)
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $clube_categorias = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'title' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'slug' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),		
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'slug' => array(
				'column' => array('slug')
			)
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $clube_promocaos = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'clube_parceiro_id'	=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'title' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'descricao' 		=> array('type' => 'string', 'length' => 255, 'null' => true),
		'desconto'			=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'validade' 			=> array('type' => 'datetime', 'null' => true),
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),		
		'banner'			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'slug' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'clube_parceiro_id' => array(
				'column' => array('clube_parceiro_id')
			),
			'slug' => array(
				'column' => array('slug')
			)
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $clube_parceiros_promocaos = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'clube_parceiro_id' 		=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'clube_promocao_id' 		=> array('type' => 'integer', 'length' => 11, 'null' => true),			
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'clube_parceiro_id' => array(
				'column' => array('clube_parceiro_id')
			),
			'clube_promocao_id' => array(
				'column' => array('clube_promocao_id')
			),
			'parceiro_promocao' => array(
				'column' => array('clube_parceiro_id', 'clube_promocao_id')
			),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	

}
