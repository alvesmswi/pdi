<?php
CroogoNav::add('sidebar', 'clube', array(
	'title' => 'Clube do Assinante',
	'icon' => array('group', 'large'),
	'url' => array(
		'admin' => true,
		'plugin' => 'clube',
		'controller' => 'parceiros',
		'action' => 'index',
	),
	'weight' => 40,
	'children' => array(
		'clube' => array(
			'title' => 'Parceiros',
			'url' => array(
				'admin' => true,
				'plugin' => 'clube',
				'controller' => 'parceiros',
				'action' => 'index',
			)
		),
		'assinantes' => array(
			'title' => 'Assinantes',
			'url' => array(
				'admin' => true,
				'plugin' => 'clube',
				'controller' => 'assinantes',
				'action' => 'index',
			)
		),
		'categorias' => array(
			'title' => 'Categorias',
			'url' => array(
				'admin' => true,
				'plugin' => 'clube',
				'controller' => 'categorias',
				'action' => 'index',
			)
		),
		'promocao' => array(
			'title' => 'Promoções',
			'url' => array(
				'admin' => true,
				'plugin' => 'clube',
				'controller' => 'promocaos',
				'action' => 'index',
			)
		)
	)
));

