<?php 
CroogoRouter::connect('/clube/parceiro/:slug', array(
	'plugin' => 'Clube', 
	'controller' => 'Parceiros', 
	'action' => 'view'
));
CroogoRouter::connect('/clube/desconto/:slug', array(
	'plugin' => 'Clube', 
	'controller' => 'Promocaos', 
	'action' => 'view'
));
CroogoRouter::connect('/clube/descontos', array(
	'plugin' => 'Clube', 
	'controller' => 'Promocaos', 
	'action' => 'index'
));
CroogoRouter::connect('/clube', array(
	'plugin' => 'Clube', 
	'controller' => 'Promocaos', 
	'action' => 'portal'
));
CroogoRouter::connect('/clube/search', array(
	'plugin' => 'Clube', 
	'controller' => 'Promocaos', 
	'action' => 'search'
));
?>