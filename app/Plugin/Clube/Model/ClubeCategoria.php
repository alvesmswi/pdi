<?php
App::uses('AppModel', 'Model');

class ClubeCategoria extends AppModel {

    public $validate = array(
        'slug' => array(
            'rule' => 'slugUnico',
            'on' => 'create',
            'message' => 'Este nome já existe.'
        )
    );

    public function slugUnico() {
        $slug = strtolower(Inflector::slug($this->data[$this->alias]['title'], '-'));
        $registros = $this->find(
            'count',
            array(
                'conditions' => array(
                    'slug' => $slug
                )
            )
        );
        if(!empty($registros)){
            return false;
        }
        return true;
    }
}