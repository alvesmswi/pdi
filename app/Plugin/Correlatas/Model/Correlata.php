<?php

App::uses('AppModel', 'Model');

/**
 * Correlata
 *
 * @category Model
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class Correlata extends AppModel 
{
	public $name 		= 'Correlata';
	public $useTable 	= 'correlatas';
	public $validate 	= array(
		'node_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
        ),
        'correlata_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		return true;
	}
}
