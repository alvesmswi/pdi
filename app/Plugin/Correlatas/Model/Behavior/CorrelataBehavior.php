<?php

App::uses('ModelBehavior', 'Model');
App::uses('Correlata', 'Correlatas.Model');

class CorrelataBehavior extends ModelBehavior 
{
    public function afterSave(Model $model, $created, $options = array())
    {
        if (isset($model->data['Correlata']['correlatas'])) {
			$node_id = $model->id;
			$correlatas = $model->data['Correlata']['correlatas'];

			$Correlata = new Correlata();
			
			$Correlata->deleteAll(array(
				'Correlata.node_id' => $node_id
			));

			if (!empty($correlatas)) {
				$arrayCorrelatas = explode(',', $correlatas);

				foreach ($arrayCorrelatas as $key => $correlata) {
					if (!empty($correlata)) {
						$saveCorrelata['Correlata']['id'] = null;
						$saveCorrelata['Correlata']['node_id'] = $node_id;
						$saveCorrelata['Correlata']['correlata_id'] = trim($correlata);

						$Correlata->save($saveCorrelata);
					}
				}
			}
		}
	}
	
	public function afterFind(Model $model, $results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value['Correlatas']) && !empty($value['Correlatas'])) {
				$arrayCorrelatas = array();
				$arrayCorrelatasOpcoes = array();
				foreach ($value['Correlatas'] as $keyCorrelata => $correlata) {
					$arrayCorrelatas[$keyCorrelata] = $correlata['correlata_id'];
					$arrayCorrelatasOpcoes[$keyCorrelata] = array(
						'id' => $correlata['correlata_id'],
						'title' => $this->getTitle($correlata['correlata_id'], $correlata['type'])
					);
				}

				$results[$key]['Correlata']['opcoes'] = json_encode($arrayCorrelatasOpcoes);
				$results[$key]['Correlata']['correlatas'] = implode(',', $arrayCorrelatas);
			}
		}
		
		return $results;
	}

	private function getTitle($id, $type)
	{
		$title = null;

		switch ($type) {
			case 'nodes':
				App::uses('Node', 'Model');
				$Node = new Node();

				$node = $Node->find('first', array(
					'conditions' => array(
						'id' => $id
					),
					'fields' => array(
						'id',
						'title'
					),
					'recursive' => -1
				));

				if (!empty($node)) {
					$title = $node['Node']['title'];
				}
			break;
		}
		
		return $title;
	}
}
