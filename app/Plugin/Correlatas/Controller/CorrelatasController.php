<?php

App::uses('AppController', 'Controller');

/**
 * Correlatas Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class CorrelatasController extends AppController 
{
	public $name 			= 'Correlatas';
	public $pluginPrefix 	= 'Correlatas';
	public $uses 			= array('Correlatas.Correlata', 'Nodes.Node');
	public $components 		= array();
    public $defaults 		= array();
    
    /**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

        $this->Security->unlockedActions += array('admin_node', 'admin_search_node');
    }
    
    /**
	 * admin_node
	 * Correlatas node add
	 */
	public function admin_node() 
	{
    }
    
    /**
     * admin_search_node
	 * Search node
     */
    public function admin_search_node($search = '')
    {
        $this->autoRender = false;

        $keyword = (isset($this->data['keyword']) && !empty($this->data['keyword'])) ? trim($this->data['keyword']) : $search;
        $nodeId = (isset($this->data['nodeId']) && !empty($this->data['nodeId'])) ? trim($this->data['nodeId']) : null;

        $sql = "
        SELECT 
            Node.id, 
            Node.title, 
            Node.type 
        FROM nodes AS Node
        WHERE Node.id <> '$nodeId' AND (Node.id = '$keyword' OR Node.title LIKE '%$keyword%') 
        ORDER BY Node.publish_start DESC
        LIMIT 20;
        ";

        $nodes = $this->Correlata->query($sql);

        $response['nodes'] = array();
        if (!empty($nodes)) {
            foreach ($nodes as $key => $node) {
                $response['nodes'][$key]['id'] = $node['Node']['id'];
                $response['nodes'][$key]['title'] = $node['Node']['title'];
                $response['nodes'][$key]['type'] = $node['Node']['type'];
            }
        }

        return json_encode($response);
    }
}
