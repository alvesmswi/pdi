<?php

echo $this->Html->css(array('Correlatas.selectize.default'));

if (!empty($this->data['Correlata']['opcoes'])) {
    echo $this->Form->hidden('Correlata.opcoes');
} else {
    echo $this->Form->hidden('Correlata.opcoes', array('value' => '[]'));
}

echo $this->Form->input('Correlata.correlatas', array(
    'multiple' => true,
    'placeholder' => 'Busque as correlatas pelo Título ou ID',
    'label' => 'Busque as correlatas pelo Título ou ID',
    'div' => array(
        'style' => 'padding-top: 10px;',
    ),
    'id' => 'nodes-correlatas',
    'required' => false
));

echo $this->Html->script(array('Correlatas.selectize.min', 'Correlatas.main'));
