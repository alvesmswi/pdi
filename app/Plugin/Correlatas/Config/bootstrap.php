<?php

// Pega os Tipos de Conteúdo do CMSWI, e verifica se a aba PDF está ativo para aquele Tipo de Conteúdo
$Type = ClassRegistry::init('Taxonomy.Type');
$typeDefs = $Type->find('all', array(
	'recursive' => -1,
	'fields' => array(
		'Type.alias',
		'Type.params'
	)
));

$types = array();
foreach($typeDefs as $typeDef) {
	$params = $typeDef['Params'];

	// If para tratar aparecer só no Tipos que tiverem ativo, por padrão não aparece em nenhum.
	if ((isset($params['correlatas']) && $params['correlatas'])) {
		$types[] = $typeDef['Type']['alias'];
	}
}

Croogo::hookRoutes('Correlatas');

Croogo::hookAdminTab('Nodes/admin_add', 'Correlatas', 'Correlatas.admin_node', array('type' => $types));
Croogo::hookAdminTab('Nodes/admin_edit', 'Correlatas', 'Correlatas.admin_node', array('type' => $types));

Croogo::hookModelProperty('Node', 'hasMany', array(
	'Correlatas' => array(
		'className' 	=> 'Correlatas.Correlata',
		'foreignKey' 	=> 'node_id',
		'dependent' 	=> true,
		'conditions' 	=> array('Correlatas.type' => 'nodes'),
		'order' 		=> 'Correlatas.id ASC'
	)
));
Croogo::hookBehavior('Node', 'Correlatas.Correlata');
