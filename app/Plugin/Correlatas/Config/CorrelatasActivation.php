<?php

App::uses('CakeSchema', 'Model');
App::uses('ConnectionManager', 'Model');

/**
 * Correlatas Activation
 *
 * Activation class for Correlatas plugin.
 *
 * @package  Correlatas
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class CorrelatasActivation 
{
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}

	public function onActivation(Controller $controller) 
	{
		//da permissões para acessar plugin
        $controller->Croogo->addAco('Correlatas');
		$controller->Croogo->addAco('Correlatas/Correlatas/admin_node');
		$controller->Croogo->addAco('Correlatas/Correlatas/admin_search_node');

		$tableName = array('correlatas');
		$pluginName = 'Correlatas';
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();

		if (!in_array($tableName, $tables)) {
			$schema = & new CakeSchema(array(
				'name' => $pluginName,
				'path' => App::pluginPath($pluginName) . 'Config' . DS . 'Schema',
			));
			$schema = $schema->load();

			foreach ($schema->tables as $table => $fields) {
				if (!in_array($table, $tables)) {
					$create = $db->createSchema($schema, $table);
					
					try {
						$db->execute($create);
					} catch (PDOException $e) {
						die(__('Could not create table: %s', $e->getMessage()));
					}
				}
			}
		}

		Cache::clear(false, '_cake_model_');
	}

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
	}
}
