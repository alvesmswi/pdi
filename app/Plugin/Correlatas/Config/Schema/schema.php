<?php

class CorrelatasSchema extends CakeSchema 
{
	public $correlatas = array(
		'id' 		    => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'node_id' 	    => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'correlata_id'  => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'type'          => array('type' => 'string', 'null' => false, 'default' => 'nodes', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' 	    => array(
			'PRIMARY' => array('column' => 'id', 'unique' => true),
			'node_id' => array('column' => 'node_id', 'unique' => false)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public function before($event = array()) 
	{
		return true;
	}

	public function after($event = array()) 
	{
	}
}
