<?php
$this->Html->css('Marketplace.redirect.css', null, array('inline' => false));
$this->Html->script('Marketplace.redirect.js', false);
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Marketplace', array(
    'admin' => true,
    'plugin' => 'Marketplace',
    'controller' => 'Marketplace',
    'action' => 'settings',
  ));

$this->append('form-start', $this->Form->create('Marketplace', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('FTP', '#setting-ftp');
$this->end();

$this->start('tab-content');

    echo $this->Html->tabStart('setting-ftp');
       echo $this->Form->input('Marketplace.endereco', array(
            'label' => 'Host do endereço',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Marketplace.porta', array(
            'label' => 'Porta de acesso',
            'type'  => 'number',
            'required'	=> true
        )) .
        $this->Form->input('Marketplace.usuario', array(
            'label' => 'Usuário / Login',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Marketplace.senha', array(
            'label' => 'Senha / Password',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Marketplace.diretorio_remoto', array(
            'label' => 'Diretório remoto que contém os XMLs',
            'type'  => 'text',
            'required'	=> false
        )).
        $this->Form->input('Marketplace.diretorio', array(
            'label' => 'Diretório que será baixado os XMLs',
            'type'  => 'text',
            'help'	=> 'O endereço informado é relativo à pasta "public", por exemplo: "Marketplace/xmls"',
            'required'	=> true
        )).
        $this->Form->input('Marketplace.coluna_adi', array(
            'label' => 'ID da Coluna da ADI',
            'type'  => 'text',
            'required'	=> false
        )).
        $this->Form->input('Marketplace.exclusiva', array(
            'label' => 'Cada matéria será exclusiva para assinantes!',
            'type'  => 'checkbox',
            'required'	=> false
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->Form->unlockField('MarketplaceTermos.*');
$this->append('form-end', $this->Form->end());