<?php
App::uses('AppModel', 'Model');
App::uses('String', 'Utility');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

  class Marketplace extends AppModel
  {
    public $useTable = false;

	public $arrayMarketplace = array(
		'NOEC' => array(40 => 'economia'),// economia
		'NOES' => array(34 => 'esporte'),// esporte
		'NOGE' => array(36 => 'variedades'),// geral
		'NOIN' => array(39 => 'exterior'),// internacional / mundo
		'NOPO' => array(31 => 'politica'),// politica
		'NOVA' => array(36 => 'variedades'),// Bem-estar / variedades
	);
	
	function localizaTermo($codigo, $nomeTermo = false){
		//dados padroes
		$editorias = $this->arrayMarketplace;

		App::import('Model','Setting');
		$setting = new Setting();
		$config = $setting->find(
			'first',
			array(
				'conditions' => array(
					'Setting.title' => $codigo
				),
				'recursive' => -1
			)
		);

		//es encontrou alguma coisa
		if(!empty($config)){
			$editorias[$config['Setting']['title']] = array(
				$config['Setting']['value'] => $config['Setting']['description']
			);
		}

		//Se encontrou na lista
		if(isset($editorias[$codigo])){
			//se é para retornar só o termo
			if($nomeTermo){
				foreach($editorias[$codigo] as $termo){
					return $termo;
				}
			}else{
				return json_encode($editorias[$codigo]);
			}
		}else{
			return 0;
		}
	}
	
	function slug($str, $espacos = '-'){
		$str = strtolower(Inflector::slug($str));
		$str = str_replace('_', $espacos, $str);
		return $str;
	}
  
  }
?>