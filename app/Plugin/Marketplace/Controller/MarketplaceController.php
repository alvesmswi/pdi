<?php
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class MarketplaceController extends AppController {
      
	public $uses = array('Marketplace.Marketplace', 'Nodes.Node', 'Taxonomy.Term', 'Taxonomy.Taxonomy', 'NoticiumDetail', 'ModelTaxonomies', 'Multiattach.Multiattach', 'Settings.Setting');

    public function beforeFilter() {    
      	parent::beforeFilter();
		$this->Auth->allow('importar');
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
		$this->Security->authCheck = false;
    }
	
	public function admin_settings()
    {

      	$this->set('title_for_layout', 'Configuração de Agência Estado');

        if (!empty($this->request->data)) {
        	//pr($this->data);exit();
            if($this->Setting->deleteAll(
                array('Setting.key LIKE' => 'Marketplace.%')
            )){
                foreach($this->request->data['Marketplace'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Marketplace.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
                foreach($this->request->data['MarketplaceTermos'] as $key => $value){
                	//se foi informado alguma coisa
                	if(!empty($value['value'])){
                		$arraySave = array();
	                    $arraySave['id']            = NULL;
	                    $arraySave['editable']      = 0;
	                    $arraySave['input_type']    = 'text';
	                    $arraySave['key']           = 'Marketplace.termo_'.$value['description'].'_'.$value['title'].'_'.$value['value'];
	                    $arraySave['value']         = $value['value'];
	                    $arraySave['title']         = $value['title'];
	                    $arraySave['description']   = $value['description'];

	                    $this->Setting->create();
	                    if ($this->Setting->saveAll($arraySave)) {
	                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
	                    } else {
	                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
	                    }
                	}
                    
                }
            }
        }

        //limpa o this->data
        $this->request->data = array();

       	$arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Marketplace.%'),'recursive' => -1)
        );

        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                 $this->request->data['Marketplace'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }

        $arrayTermos = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Marketplace.termo_%'),'recursive' => -1)
        );
        if(!empty($arrayTermos)){
            foreach($arrayTermos as $dados){
                 $this->request->data['MarketplaceTermos']['termo_'.$dados['Setting']['title'].'_'.$dados['Setting']['id']] = $dados['Setting'];
            }
        }
    }

    public function admin_add_field()
    {
        
    }
	
	private function _FtpConnect(){

		//Variaveis
		$host 		= Configure::read('Marketplace.endereco')?Configure::read('Marketplace.endereco'):'35.184.135.208';
		$port 		= Configure::read('Marketplace.porta')?Configure::read('Marketplace.porta'):'21';
		$usuario 	= Configure::read('Marketplace.usuario')?Configure::read('Marketplace.usuario'):'jornal';
		$senha 		= Configure::read('Marketplace.senha')?Configure::read('Marketplace.senha'):'jornal';
		$diretorio	= Configure::read('Marketplace.diretorio')?Configure::read('Marketplace.diretorio'):'Marketplace';
		$limit		= Configure::read('Marketplace.limit')?Configure::read('Marketplace.limit'):10;

		//monta o endereço
		$diretorio 	= Configure::read('Marketplace.diretorio')?Configure::read('Marketplace.diretorio'):'Marketplace';
		$dirRemoto 	= Configure::read('Marketplace.diretorio_remoto')?Configure::read('Marketplace.diretorio_remoto'):'teste';
		$dirLocal 	= WWW_ROOT.$diretorio.DS;		
		$ext 		= '*.xml';

		//verifica se o caminho existe
		if(!is_dir($dirLocal)){
			//cria o diretorio
			mkdir($dirLocal, 0777, true);
		}
		//testa o server
		$conn_id = ftp_connect($host) or die("Não foi possível conectar-se a $host"); 
		
		//tenta se conectar
		if(ftp_login($conn_id, $usuario, $senha)){
			$mode = ftp_pasv($conn_id, TRUE);
			$this->_downloadFiles($conn_id, $dirLocal, $dirRemoto, $ext, $limit);
		}else{
			return false;
		}
		
		// close the connection
		ftp_close($conn_id);
	}
	
	private function _downloadFiles($conn_id, $dirLocal, $dirRemoto, $ext, $limit = 10){
		//Se o diretório existe
		if (ftp_chdir($conn_id, $dirRemoto)) {
			$arquivos = ftp_nlist($conn_id, $ext);
			//se tem arquivos
			if(!empty($arquivos)){
				$contador = 0;
				//percorre os arquivos
				foreach($arquivos as $arquivo){
					//Define os caminhos dos arquivos
					$pathLocal = $dirLocal.$arquivo;
					$pathRemoto = $arquivo;
					//faz download
					if(ftp_get($conn_id, $pathLocal, $pathRemoto, FTP_BINARY)){
						//deleta o arquivo
						ftp_delete($conn_id, $pathRemoto);
					}
					$contador++;
					//se chegou no limite
					if($contador == $limit){
						break;
					}
				}
			}
		}
	}
	
	public function admin_index()
    {
    	//Baixa os arquivos por FTp
    	$this->_FtpConnect();
		//monta o diretorio para buscar os XMLs
    	$diretorio = Configure::read('Marketplace.diretorio')?Configure::read('Marketplace.diretorio'):'Marketplace';
		$xmlsDIr = WWW_ROOT.$diretorio.DS;

		$dir = new Folder($xmlsDIr);
		//procura apenas os XMLs
		$files = $dir->find('.*\.xml');
		
		$this->set('xmlsDIr',$xmlsDIr);
		$this->set('files',$files);
    }
	
    public function importar($quantidade = 1, $cron = true)
    {
    	$this->autoRender = false;
		$totalImportado = 0;
		
		//monta o diretorio para buscar os XMLs
		$diretorio = Configure::read('Marketplace.diretorio')?Configure::read('Marketplace.diretorio'):'Marketplace';
		$xmlsDIr = WWW_ROOT.$diretorio.DS;
		$dir = new Folder($xmlsDIr);
		//procura apenas os XMLs
		$files = $dir->find('.*\.xml');
		//se encontrou algum arquivo
		if(!empty($files)){
			//percore os arquivos
			foreach($files as $file){
				//le o arquivo XMLs
				try {
					$xml = Xml::build($xmlsDIr.DS.$file);
					$totalImportado = $this->_saveNoticia($xml);
				} catch (XmlException $e) {
					//throw new InternalErrorException();
				}
				
				//move o arquivos
				$arq1 = new File($xmlsDIr.DS.$file);
				if ($arq1->exists()) {
					//Copia o arquivo
				    $arq2 = new Folder($xmlsDIr.DS.'importados', true);
				    $arq1->copy($arq2->path . DS . $arq1->name);
					//deleta o arquivo imoprtado
					$arq1->delete($xmlsDIr.DS.$file);
				}
				
				$totalImportado++;
				if($totalImportado == $quantidade){
					break;
				}
			}
		}
		
		//Baixa os arquivos por FTp
    	$this->_FtpConnect();
		
		if($cron){
			return $totalImportado.' noticias importadas.';
		}else{
			$this->Session->setFlash($totalImportado.' noticias importadas.', 'flash', array('class' => 'success'));
			$this->redirect('/admin/marketplace');
		}
    }	
	
	private function _saveNoticia($data, $typeAlias= 'noticia') {
		
		$Node 					= $this->Node;
		$this->request->data 	= $this->_preparaArray($data);
		$type 					= $Node->Taxonomy->Vocabulary->Type->findByAlias($typeAlias);

		//se é um post de blog
		if(isset($this->request->data['Node']['blog_id'])){
			$this->loadModel('Blog.BlogNode');
			$this->BlogNode->save($this->request->data['Node']);

		}else if ($Node->saveNode($this->request->data, $typeAlias)) {
			Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $this->request->data));
			$id_salvo = $Node->getLastInsertID();
		} else {
			return false;
		}
	}

	private function _preparaCorpo($integra)
	{

		return join('',array_reduce(explode("\n", $integra), function($carry, $item) 
			{
				if (strlen($item))
				{
					$item = '<p>' . $item . '</p>';
					$carry[] = $item;
				}
				
				return $carry;
			}, []));
	}
	
	function _preparaArray($dados){
		//se não está vazio
		if(!empty($dados)){
			$id = (string)$dados->ID;
			$exclusiva = Configure::read('Marketplace.exclusiva');
			
			//trata a data e hora
			$data = str_replace('/', '-', $dados->DATA);
			$data = date('Y-m-d', strtotime($data));
			$dataHora = $data.' '.$dados->HORA.':00';

			$corpo = $this->_preparaCorpo((string)$dados->INTEGRA);
			
			//Prepara o array para salvar
			$arraySave['Node'] = array();
			$arraySave['Node']['id'] 				= null;
			$arraySave['Node']['user_id'] 			= '1';
			$arraySave['Node']['title'] 			= (string)$dados->TITULO;
			$arraySave['Node']['slug'] 				= $this->Marketplace->slug((string)$dados->TITULO);
			$arraySave['Node']['body'] 				= $corpo;
			$arraySave['Node']['status'] 			= '1';
			$arraySave['Node']['publish_start'] 	= $dataHora;
			$arraySave['Node']['created'] 			= $dataHora;
			$arraySave['Node']['modified'] 			= $dataHora;
			$arraySave['Node']['exclusivo'] 		= $exclusiva;
			//$arraySave['Node']['terms'] 			= $this->Marketplace->localizaTermo((string) $dados->EDITORIA);
			
			/* -- noticium_details -- */
			$arraySave['NoticiumDetail']['jornalista'] 	= (string) $dados->FORNECEDOR;
			$arraySave['NoticiumDetail']['chapeu'] 		= null;
			$arraySave['NoticiumDetail']['titulo_capa'] = null;
			$arraySave['NoticiumDetail']['keywords'] 	= null;
			
			/* -- campos nulos -- */
			$arraySave['Node']['parent_id'] 				= null;
			$arraySave['Node']['autocomplete_parent_id'] 	= null;
			$arraySave['Node']['id'] 						= null;
			$arraySave['Role']['Role'] 						= null;
			
			//Taxonomia
			$termo = $this->Marketplace->localizaTermo((string) $dados->EDITORIA, true);
			
			$arraySave['TaxonomyData'][1] = $this->_getTermId($termo);

			//se o plugin de Editorias está ativo
			if(CakePlugin::loaded('Editorias')){
				$_getEditoriaId = $this->_getEditoriaId($termo);
				if($_getEditoriaId){
					$arraySave['Editoria']['Editorias'][] = $_getEditoriaId;
				}
			}

			//Se veio do marketplace
			if((string)$dados->EDITORIA == 'adi' && (string)$dados->SERVICO == 'pdi'){
				if(Configure::read('Marketplace.coluna_adi')){
					$arraySave['Node']['blog_id'] 		= Configure::read('Marketplace.coluna_adi');
					$arraySave['Node']['jornalista'] 	= (string)$dados->FORNECEDOR;
					$arraySave['Node']['keywords'] 		= (string)$dados->TAGS;
				}
			}
	
		}
		return $arraySave;
	}
	
	private function _getTermId($term, $vocabularyId = '1') 
	{
		$slugTerm 	= strtolower(Inflector::slug($term));
		$term_find 	= $this->Term->findBySlug($slugTerm, array('id', 'slug'));
		if (!empty($term_find)) {
			$taxonomy_id = null;
			foreach ($term_find['Vocabulary'] as $key => $taxonomy) {
				$taxonomy_id = $taxonomy['Taxonomy']['id'];
			}
			return $taxonomy_id;
		}
	}

	private function _getEditoriaId($slug) 
	{
		$editoria 	= $this->Term->query("SELECT id, slug FROM editorias WHERE slug = '$slug' LIMIT 1;");
		if (!empty($editoria)) {
			return $editoria[0]['editorias']['id'];
		}else{
			return false;
		}
	}

}
?>