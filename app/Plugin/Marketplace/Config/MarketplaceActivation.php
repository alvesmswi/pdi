<?php
class MarketplaceActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        $controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.endereco',
			'value' =>       '35.184.135.208',
			'title' =>       'Endereço FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));

		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.porta',
			'value' =>       '21',
			'title' =>       'Porta FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.usuario',
			'value' =>       'jornal',
			'title' =>       'Usuário para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.senha',
			'value' =>       '!@#jornal321',
			'title' =>       'Senha para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.diretorio',
			'value' =>       '',
			'title' =>       'Diretorio para buscar os XMLS',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      256,
			'params' =>      '',
		));

		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.diretorio_remoto',
			'value' =>       'teste',
			'title' =>       'Pasta remota',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      256,
			'params' =>      '',
		));

		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Marketplace.coluna_adi',
			'value' =>       '1',
			'title' =>       'ID da Coluna da ADI',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      256,
			'params' =>      '',
		));
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Marketplace');
	}
}