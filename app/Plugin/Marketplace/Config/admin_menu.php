<?php
CroogoNav::add('sidebar', 'settings.children.marketplace', array(
	'icon' => 'list',
	'title' => 'Marketplace',
	'url' => array(
		'admin' => true,
		'plugin' => 'marketplace',
		'controller' => 'marketplace',
		'action' => 'settings',
	),
	'weight' => 40,
	'children' => array(
		'settings' => array(
			'title' => 'Settings',
			'url' => array(
				'admin' => true,
				'plugin' => 'marketplace',
				'controller' => 'marketplace',
				'action' => 'settings'
			)
		),
		'importar' => array(
			'title' => 'Importar',
			'url' => array(
				'admin' => true,
				'plugin' => 'marketplace',
				'controller' => 'marketplace',
				'action' => 'index'
			)
		)
	)
));