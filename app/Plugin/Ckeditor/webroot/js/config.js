/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'pt-br';
	// config.uiColor = '#AADC6E';
	config.disallowedContent = 'img{width,height}';
	// config.extraPlugins = 'uploadimage';
	config.uploadUrl = '/admin/file_manager/attachments/add';
	config.filebrowserBrowseUrl = '/admin/file_manager/attachments/browse';
	//config.filebrowserUploadUrl = '/admin/file_manager/attachments/add';
	config.forcePasteAsPlainText = true;
	// config.embed_provider = '//iframe.ly/api/oembed?url={url}&callback={callback}&api_key=28c69e1eb17de9e867af66';
	config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';
};
