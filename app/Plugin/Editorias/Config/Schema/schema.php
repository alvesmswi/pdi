<?php 
class EditoriasSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $editorias_grupos = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'title' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'slug' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),		
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'slug' => array(
				'column' => array('slug'), 
				'unique' => true
			),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $editorias = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'grupo_id' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'title' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'slug' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),		
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'slug' => array(
				'column' => array('slug')
			),
			'slug_grupo' => array(
				'column' => array('slug', 'grupo_id')
			),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $editorias_nodes = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'editoria_id' 		=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'node_id' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),			
		'indexes' 			=> array(
			'id' => array(
				'column' => array('id'), 
				'unique' => true
			),
			'editoria_id' => array(
				'column' => array('editoria_id')
			),
			'node_id' => array(
				'column' => array('node_id')
			),
			'node_editoria' => array(
				'column' => array('node_id', 'editoria_id')
			),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
