<?php
class EditoriasActivation {

public function beforeActivation(&$controller) {
	return true;
}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {
		Configure::write('debug',1);

		//Cria as tabelas
		CakePlugin::load('Editorias');

		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		include_once(App::pluginPath('Editorias').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		//se a tabela NÃO existe
		if (!in_array(strtolower('editorias'), $tables)) {
			$CakeSchema = new CakeSchema();
			$EditoriasSchema = new EditoriasSchema();
			foreach ($EditoriasSchema->tables as $table => $config) {
				if (!in_array($table, $tables)) {
					$db->execute($db->createSchema($EditoriasSchema, $table));
				}
			}
		}		

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('Editorias/Editorias/admin_index', array('registered', 'assinante', 'cortesia', 'editor', 'colunista', 'gerente'));

		Cache::clear(false, '_cake_model_');
	}

/**
 * onDeactivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

/**
 * Called after deactivating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onDeactivation(&$controller) {
		// ACL: remove ACOs with permissions
		$controller->Croogo->removeAco('Editorias'); // EditoriasController ACO and it's actions will be removed
	}
}
