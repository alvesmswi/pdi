<?php
CroogoNav::add('sidebar', 'content.children.editorias', array(
	'title' => __d('croogo', 'Editorias'),
	'url' => array(
		'admin' => true,
		'plugin' => 'editorias',
		'controller' => 'editorias',
		'action' => 'index',
	),
	'weight' => 40,
	'children' => array(
		'grupos_index' => array(
			'title' => 'Grupos',
			'url' => array(
				'admin' => true,
				'plugin' => 'editorias',
				'controller' => 'editorias',
				'action' => 'grupos',
			)
		),
		'index' => array(
			'title' => 'Editorias',
			'url' => array(
				'admin' => true,
				'plugin' => 'editorias',
				'controller' => 'editorias',
				'action' => 'index',
			)
		),
		'importar' => array(
			'title' => 'Importar Termos',
			'url' => array(
				'admin' => true,
				'plugin' => 'editorias',
				'controller' => 'editorias',
				'action' => 'importar_terms',
			)
		),
		'migrar' => array(
			'title' => 'Migrar Nodes',
			'url' => array(
				'admin' => true,
				'plugin' => 'editorias',
				'controller' => 'editorias',
				'action' => 'migrar_nodes',
			)
		),
	)
));

