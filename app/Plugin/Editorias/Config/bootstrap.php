<?php
// Pega os Tipos de Conteúdo do CMSWI
$Type = ClassRegistry::init('Taxonomy.Type');
$typeDefs = $Type->find('all', array(
	'recursive' => -1,
	'fields' => array(
		'Type.alias',
		'Type.params'
	)
));

$types = array();
foreach($typeDefs as $typeDef) {
	$params = $typeDef['Params'];
	// If para tratar aparecer só no Tipos que tiverem ativo, por padrão não aparece em nenhum.
	if ((isset($params['editorias']) && $params['editorias'])) {
		$types[] = $typeDef['Type']['alias'];
	}
}

Croogo::hookAdminTab('Nodes/admin_add', 'Editorias', 'Editorias.admin_node', array('type' => $types));
Croogo::hookAdminTab('Nodes/admin_edit', 'Editorias', 'Editorias.admin_node', array('type' => $types));
Croogo::hookBehavior('Node', 'Editorias.EditoriasNode');