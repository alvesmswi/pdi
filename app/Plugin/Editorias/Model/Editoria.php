<?php
App::uses('AppModel', 'Model');

class Editoria extends AppModel {
    
    public $belongsTo = array(
		'EditoriasGrupo' => array(
            'foreignKey' => 'grupo_id'
        )
	);

}