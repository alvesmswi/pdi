<?php

App::uses('ModelBehavior', 'Model');
App::uses('EditoriasNode', 'Editorias.Model');

class EditoriasNodeBehavior extends ModelBehavior 
{
    public function afterSave(Model $model, $created, $options = array())
    {
        if (isset($model->data['Editoria']['Editorias'])) {
			$node_id = $model->id;
			$editorias = $model->data['Editoria']['Editorias'];

			$EditoriasNode = new EditoriasNode();
			
			$EditoriasNode->deleteAll(array(
				'EditoriasNode.node_id' => $node_id
			));

			if (!empty($editorias)) {
				foreach($editorias as $editoria){
					$saveEditoria['EditoriasNode']['id'] = null;
					$saveEditoria['EditoriasNode']['node_id'] = $model->data['Node']['id'];
					$saveEditoria['EditoriasNode']['editoria_id'] = $editoria;
					$EditoriasNode->saveAll($saveEditoria);
				}				
			}
		}
	}
	
	public function afterFind(Model $model, $results, $primary = false) 
	{
		
		if ($primary && isset($results[0][$model->alias])) {
			$arrayEditorias = array();
			foreach($results as $key => $result){
				if(isset($result['Node']['id'])){
					$nodeId = $result['Node']['id'];
					$EditoriasNode = new EditoriasNode();
					$sql = "
						SELECT 
							Editoria.id, Editoria.title, Editoria.slug
						FROM editorias_nodes AS EditoriasNode 
						INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
						WHERE 
							EditoriasNode.node_id = $nodeId
						ORDER BY Editoria.id ASC;
					";
					$editorias = $EditoriasNode->query($sql);
					//se encontrou
					if(!empty($editorias)){
						foreach($editorias as $editoria){
							$results[$key]['Editorias'][] = $editoria['Editoria'];
						}
					}
				}
				
			}
		}
		
		return $results;
	}

}
