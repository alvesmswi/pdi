<?php
if(isset($type['Params']['editorias'])){
    //Busca as editorias
    $Editoria = ClassRegistry::init('Editorias.Editoria');
    
    $conditions = array();
    $conditions['EditoriasGrupo.id'] = $type['Params']['editorias'];
    //se está adicionando
    if($this->action == 'admin_add'){
        $conditions['Editoria.status'] = 1;
    }

	$editorias = $Editoria->find('list', array(
        'recursive' => 0,
        'conditions' => $conditions,
        'fields' => array('id','title')

    ));

    echo $this->Html->css(array('Editorias.selectize.default'));

    echo $this->Form->input('Editoria.Editorias', array(
        'multiple' => true,
        'label' => 'Selecione uma ou mais editorias <span style="color:red;">*</span>',
        'options' => $editorias,
        'div' => array(
            'style' => 'padding-top: 10px;',
        ),
        'id' => 'editorias-select',
        'required' => true
    ));

    echo $this->Html->script(array('Editorias.selectize.min', 'Editorias.main'));
}