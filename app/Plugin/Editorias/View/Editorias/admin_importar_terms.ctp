<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Editorias', array(
    'admin' => true,
    'plugin' => 'editorias',
    'controller' => 'editorias',
    'action' => 'importar_terms',
  ));

$this->append('form-start', $this->Form->create('Editorias', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Importação', '#tab-config');
$this->end();
$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
    echo $this->Form->input('Editorias.vocabulario_id', array(
            'label' => 'Selecione o Vocabulário que será importado para Editorias',
            'options'  => $vocabularios
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Ações') .
        $this->Form->button('Importar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());