<?php

$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Editorias', array('plugin' => 'editorias', 'controller' => 'editorias', 'action' => 'index'));

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Nova Editoria',
		array(
			'action' => 'add'
		)
	);
$this->end();

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		'ID',
		'Titulo',
		'Slug',
		'Ativo',
		'Ações',
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();

	foreach ($registros as $registro):
		$actions = array();
		$actions[] = $this->Croogo->adminRowActions($registro['Editoria']['id']);
		$actions[] = $this->Croogo->adminRowAction('',
			array('action' => 'edit', $registro['Editoria']['id']),
			array('icon' => $this->Theme->getIcon('update'), 'tooltip' => 'Editar')
		);
		$actions = $this->Html->div('item-actions', implode(' ', $actions));

		$rows[] = array(
			$registro['Editoria']['id'],
			$registro['Editoria']['title'],
			$registro['Editoria']['slug'],
			$this->element('admin/toggle', array(
				'id' => $registro['Editoria']['id'],
				'status' => (int)$registro['Editoria']['status'],
				'model' => ''
			)),
			$actions,
		);
	endforeach;
	echo $this->Html->tableCells($rows);
$this->end();

?>
