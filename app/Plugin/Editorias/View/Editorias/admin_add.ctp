<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Taxonomy.terms');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Editoria', array('plugin' => 'editorias', 'controller' => 'editorias', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_edit') {
	$this->Html
	->addCrumb('Editar', '/' . $this->request->url);
}
if ($this->request->params['action'] == 'admin_add') {
	$this->Html
		->addCrumb('Adicionar', '/' . $this->request->url);
}

$this->Form->create('Editoria', array('url' => '/' . $this->request->url));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab('Editoria', '#term-basic');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	
	echo $this->Html->tabStart('term-basic') .
		$this->Form->hidden('id') .
		$this->Form->input('title', array(
			'label' => 'Titulo',
		)) . 
		$this->Form->input('grupo_id', array(
			'label' => 'Grupo',
			'empty' => false,
			'options' => $grupos
		));		
	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->start('panels');
	echo $this->Html->beginBox('Opções') .
		$this->Form->button('Aplicar', array('name' => 'apply')) .
		$this->Form->button('Salvar', array('button' => 'success')) .
		$this->Html->link(
			'Cancelar',
			array('action' => 'index'),
			array('button' => 'danger')
		) .
		$this->Form->input('status', array(
			'label' => 'Ativo',
			'type' => 'checkbox',
			'default' => 1
		)) . 
		$this->Html->endBox();

		echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
