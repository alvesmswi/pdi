<?php

$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Grupos de Editorias', array('plugin' => 'editorias', 'controller' => 'editorias', 'action' => 'index'));

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Novo Grupo de Editoria',
		array(
			'action' => 'grupos_add'
		)
	);
$this->end();

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		'ID',
		'Titulo',
		'Slug',
		'Ações',
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();

	foreach ($registros as $registro):
		$actions = array();
		$actions[] = $this->Croogo->adminRowActions($registro['EditoriasGrupo']['id']);
		$actions[] = $this->Croogo->adminRowAction('',
			array('action' => 'grupos_edit', $registro['EditoriasGrupo']['id']),
			array('icon' => $this->Theme->getIcon('update'), 'tooltip' => 'Editar')
		);
		$actions = $this->Html->div('item-actions', implode(' ', $actions));

		$rows[] = array(
			$registro['EditoriasGrupo']['id'],
			$registro['EditoriasGrupo']['title'],
			$registro['EditoriasGrupo']['slug'],
			$actions,
		);
	endforeach;
	echo $this->Html->tableCells($rows);
$this->end();

?>
