<?php
App::uses('EditoriasAppController', 'Editorias.Controller');

class EditoriasController extends EditoriasAppController {
	public $name = 'Editorias';
    public $uses = array(
        'Editorias.Editoria',
        'Editorias.EditoriasGrupo',
        'Editorias.EditoriasNode'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions += array('admin_editorias_get');
        $this->Auth->allow(
            'admin_migrar_nodes',
            'admin_migrar_nodes_ajax'
		);
    }
    
    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}

    public function admin_index($grupoId = null){ 
        $this->Editoria->recursive = -1;
		$this->paginate = array(
		    'order' => array('Editoria.id'=>'ASC')
		);
		$this->set('registros', $this->paginate('Editoria'));
    }

    public function admin_add(){
        if(!empty($this->request->data)){
            //gera o slug
            $this->request->data['Editoria']['slug'] = strtolower(Inflector::slug($this->data['Editoria']['title'], '-'));
            //salva
            if($this->Editoria->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/editorias/editorias');
            }else{
                $invalidateFields = $this->Editoria->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }
        $this->set(
            'grupos',
            $this->EditoriasGrupo->find(
                'list',
                array(
                    'conditions' => array(
                        'status' => 1
                    ),
                    'fields' => array(
                        'id', 'title'
                    ),
                    'order' => 'title ASC'
                )
            )
        );
    }

    public function admin_edit($id){
        if(!empty($this->request->data)){
            if($this->Editoria->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/editorias/editorias');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 
        $this->set(
            'grupos',
            $this->EditoriasGrupo->find(
                'list',
                array(
                    'fields' => array(
                        'id', 'title'
                    ),
                    'order' => 'title ASC'
                )
            )
        );
        $this->request->data = $this->Editoria->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        $this->render('admin_add');
    }

    public function admin_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->Editoria->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/editorias/editorias');
    }

    public function admin_grupos(){ 
		$this->EditoriasGrupo->recursive = -1;
		$this->paginate = array(
		    'order' => array('EditoriasGrupo.id'=>'ASC')
		);
		$this->set('registros', $this->paginate('EditoriasGrupo'));
    }

    public function admin_grupos_add(){ 
        if(!empty($this->request->data)){
            //gera o slug
            $this->request->data['EditoriasGrupo']['slug'] = strtolower(Inflector::slug($this->data['EditoriasGrupo']['title'], '-'));
            //salva
            if($this->EditoriasGrupo->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/editorias/editorias/grupos');
            }else{
                $invalidateFields = $this->EditoriasGrupo->invalidFields();
                if(!empty($invalidateFields)){
                    foreach($invalidateFields as $invalid){
                        $this->Flash->error(" " .$invalid[0]);
                    }
                }
               
            }            
        }
    }

    public function admin_grupos_edit($id){ 
        if(!empty($this->request->data)){
            if($this->EditoriasGrupo->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso!');
                $this->redirect('/admin/editorias/editorias/grupos');
            }else{
                $this->Flash->error('Erro ao tentar salvar o registro.');
            }            
        } 
        $this->request->data = $this->EditoriasGrupo->find(
            'first',
            array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            )
        );
        $this->render('admin_grupos_add');
    }

    public function admin_grupos_delete($id){ 
        $this->autoRender = false;
        if(!empty($id)){
            if($this->EditoriasGrupo->delete($id)){
                $this->Flash->success('Registro EXCLUÍDO com sucesso!');
            }else{
                $this->Flash->error('Erro ao tentar EXCLUIR o registro.');
            }            
        }
        $this->redirect('/admin/editorias/editorias/grupos');
    }

    public function admin_importar_terms(){
        $this->loadModel('Taxonomy.Vocabulary');
        $vocabularios = $this->Vocabulary->find(
            'list',
            array(
                'fields' => array(
                    'id','title'
                ),
                'recursive' => -1
            )
        );

        if(!empty($this->data)){
            //pega o ID do vocabulário selecionado
            $vocabularioId = $this->data['Editorias']['vocabulario_id'];
            //Monta a query
            $query = "SELECT Term.id, Term.title, Term.slug FROM taxonomies AS Tax
            INNER JOIN terms AS Term ON(Term.id = Tax.term_id)
            WHERE Tax.vocabulary_id = $vocabularioId 
            ORDER BY Term.id ASC";
            $termos = $this->Vocabulary->query($query);

            //se encontrou algum registro
            if(!empty($termos)){
                $vocTitle = $vocabularios[$vocabularioId];
                $vocSlug = strtolower(Inflector::slug($vocTitle, '-'));

                //verifica se este grupo de editoria já existe
                $grupo = $this->EditoriasGrupo->find(
                    'first',
                    array(
                        'conditions' => array(
                            'slug' => $vocSlug
                        ),
                        'recursive' => -1
                    )
                );
                //se não existe, cria
                if(empty($grupo)){
                    
                    $arrayGrupo = array(
                        'id' => null,
                        'title' => $vocTitle,
                        'slug' => $vocSlug,
                        'status' => 1
                    );
                    if($this->EditoriasGrupo->save($arrayGrupo)){
                        $grupoId = $this->EditoriasGrupo->getLastInsertId();                        
                    }else{
                        return $this->Flash->error('Erro ao tentar salvar o Grupo.');
                    }
                    $contador = 0;
                    //percorre os termos gravando cada um
                    foreach($termos as $key => $term){
                        $arraySave = array(
                            'id' => null,
                            'title' => $term['Term']['title'],
                            'slug' => $term['Term']['slug'],
                            'grupo_id' => $grupoId,
                            'status' => 1
                        );
                        $this->Editoria->save($arraySave);
                        $contador++;
                    }
                    $this->Flash->success($contador.' editorias importadas.');
                }
            }else{
                $this->Flash->error('Nenhum termo encontrado.');
            }
        }        
        
        $this->set('vocabularios',$vocabularios);
    }

    public function admin_migrar_nodes(){
        $this->loadModel('Taxonomy.Vocabulary');

        if(!empty($this->data)){
            //pega o ID do vocabulário selecionado
            $vocabularioId = $this->data['Editorias']['vocabulario_id'];
            $this->redirect('/admin/editorias/editorias/migrar_nodes_ajax/'.$vocabularioId);
            //$this->admin_migrar_nodes_ajax($vocabularioId);
        }   

        $vocabularios = $this->Vocabulary->find(
            'list',
            array(
                'fields' => array(
                    'id','title'
                ),
                'recursive' => -1
            )
        );
        
        $this->set('vocabularios',$vocabularios);
    }

    public function admin_migrar_nodes_ajax($vocabularioId, $limit = 500){
        $this->loadModel('Taxonomy.Terms');
        $termos = $this->Terms->find(
            'list',
            array(
                'fields' => array(
                    'id','title'
                ),
                'recursive' => -1
            )
        );
        $editorias = $this->Editoria->find(
            'list',
            array(
                'fields' => array(
                    'slug','id'
                ),
                'recursive' => -1
            )
        );

        //Conta quantos faltam
        $query = "SELECT COUNT(*) AS total FROM taxonomies AS Tax
        INNER JOIN model_taxonomies AS Mox ON(Mox.taxonomy_id = Tax.id)
        INNER JOIN nodes AS Node ON(Node.id = Mox.foreign_key)
        WHERE Tax.vocabulary_id = $vocabularioId  AND Node.migrado = 0;";
        $nodesRestantes = $this->Terms->query($query);
        $this->set('nodesRestantes', $nodesRestantes[0][0]['total']);

        //Monta a query
        $query = "SELECT Node.id, Term.slug FROM taxonomies AS Tax
        INNER JOIN terms AS Term ON(Term.id = Tax.term_id) 
        INNER JOIN model_taxonomies AS Mox ON(Mox.taxonomy_id = Tax.id)
        INNER JOIN nodes AS Node ON(Node.id = Mox.foreign_key)
        WHERE Tax.vocabulary_id = $vocabularioId AND Node.migrado = 0 
        ORDER BY Term.id ASC
        LIMIT $limit;";
        $nodes = $this->Terms->query($query);

        //se encontrou algum registro
        if(!empty($nodes)){
            $contador = 0;
            //percorre os termos gravando cada um
            foreach($nodes as $key => $node){
                //se este termo existe
                if(isset($editorias[$node['Term']['slug']])){
                    $editoriaId = $editorias[$node['Term']['slug']];
                    $nodeId = $node['Node']['id'];
                    $arraySave = array(
                        'id' => null,
                        'editoria_id' => $editoriaId,
                        'node_id' => $node['Node']['id']
                    );
                    $this->EditoriasNode->save($arraySave);
                    //atualiza os nodes
                    $this->EditoriasNode->query("UPDATE nodes SET migrado = 1 WHERE id = $nodeId;");
                    $contador++;
                }else{
                    $this->Flash->error('Termo '.$node['Term']['slug'].' não encontrado.');
                }
            }
            $this->Flash->success($contador.' editorias importadas.');            
        }else{
            $this->Flash->error('Nenhum node encontrado.');
        }
    }

}