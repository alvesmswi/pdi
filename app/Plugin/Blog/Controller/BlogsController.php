<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Xml', 'Utility');
App::uses('Sanitize', 'Utility');
App::uses('Croogo', 'Lib');

/**
 * Blog Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class BlogsController extends AppController {
	public $name 			= 'Blogs';
	public $pluginPrefix 	= "Blog";
	public $uses 			= array('Blog.Blog', 'Blog.Post', 'Blog.BlogBanner', 'Nodes.Node');
	public $components 		= array();
	public $defaults 		= array();

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();

		$this->Security->unlockedActions += array('admin_create', 'admin_edit', 'admin_create_post', 'admin_edit_post');
		$this->Auth->allow(array('blogs', 'view', 'view_post', 'ultimos_posts'));
	}

	/**
	 * Toggle Node status
	 *
	 * @param string $id Node id
	 * @param integer $status Current Node status
	 * @return void
	 */
	public function admin_toggle($id = null, $status = null, $model = null) 
	{
		Cache::clear(false, 'ultimos_posts');
		$Model = (!empty($model)) ? $this->{$model} : $this->{$this->modelClass};
		$this->Croogo->fieldToggle($Model, $id, $status);
		
	}

	/**
	 * admin_index
	 * Blog index
	 */
	public function admin_index() 
	{
		$this->set('title_for_layout', __d('croogo', 'Blogs'));

		$alias 				= 'Blog';
		$arrayConditions 	= array();

		if (!empty($this->data)) {
			if (!empty($this->data[$alias]['filter'])) {
				$arrayConditions['AND']['Blog.title LIKE'] = '%'.$this->data[$alias]['filter'].'%';
			}
			if ($this->data[$alias]['status'] != "" && ($this->data[$alias]['status'] == 0 || $this->data[$alias]['status'] == 1)) {
				$arrayConditions['AND']['Blog.status'] = $this->data[$alias]['status'];
			}
		}

		$this->paginate[$alias]['recursive'] = -1;

		//Se for colunista
		if (AuthComponent::user('role_id') == 6) {
			$this->paginate[$alias]['joins'] = array(
				array(
					'table' => 'blog_users',
					'alias' => 'Blogueiros',
					'type' => 'LEFT',
					'conditions' => array(
						'Blogueiros.blog_id = Blog.id'
					)
				)
			);

			$arrayConditions['AND']['Blogueiros.user_id'] = AuthComponent::user('id');

			$this->set('showActions', false);
		}

		$this->paginate[$alias]['order'] = array('Blog.status' => 'desc', 'Blog.title' => 'asc');
		$this->paginate[$alias]['conditions'] = $arrayConditions;

		$blogs = $this->paginate($alias);

		$this->set(compact('blogs'));
	}

	/**
	 * admin_create
	 * Blog create
	 */
	public function admin_create() 
	{
		if (!empty($this->request->data)) {
			if ($this->Blog->save($this->request->data)) {
				Cache::clear(false, 'ultimos_posts');
				$this->Session->setFlash(__d('croogo', 'Blog: %s foi salvo', $this->request->data['Blog']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Blog->id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s não pode ser salvo.',$this->request->data['Blog']['title']), 'flash', array('class' => 'error'));
			}
		}
		
		$this->set('title_for_layout', __d('croogo', 'Criar blog'));
		$this->set('blogueiros', $this->Blog->blogueiros());
		
		$this->render('admin_form');
	}

	/**
	 * admin_edit
	 * Blog edit
	 */
	public function admin_edit($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->Blog->save($this->request->data)) {
				Cache::clear(false, 'ultimos_posts');
				$this->Session->setFlash(__d('croogo', 'Blog: %s foi salvo', $this->request->data['Blog']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__d('croogo', '%s não pode ser salvo.',$this->request->data['Blog']['title']), 'flash', array('class' => 'error'));
			}
		}

		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$this->Blog->contain('Blogueiros');
			$data = $this->Blog->read(null, $id);
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}
			$this->request->data = $data;
		}

		$this->set('title_for_layout', __d('croogo', 'Edit blog: %s', $this->request->data['Blog']['title']));
		$this->set('blogueiros', $this->Blog->blogueiros());

		$this->render('admin_form');
	}

	/**
	 * admin_delete
	 * Blog delete
	 */
	public function admin_delete($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->Blog->delete($id)) {
			Cache::clear(false, 'ultimos_posts');
			$this->Session->setFlash('Blog id: '.$id.' foi deletado', 'flash', array('class' => 'success'));
			$this->Croogo->redirect(array('action' => 'index'));
		}

		$this->autoRender = false;
	}

	/**
	 * admin_index_post
	 * Posts blog index
	 */
	public function admin_index_post() 
	{
		$this->set('title_for_layout', __d('croogo', 'Posts'));

		$alias 				= 'Post';
		$arrayConditions 	= array();

		if (!empty($this->data)) {
			if (!empty($this->data[$alias]['filter'])) {
				$arrayConditions['AND']['Post.title LIKE'] = '%'.$this->data[$alias]['filter'].'%';
			}
			if (!empty($this->data[$alias]['type'])) {
				$arrayConditions['AND']['Post.blog_id'] = $this->data[$alias]['type'];
			}
			if ($this->data[$alias]['status'] != "" && ($this->data[$alias]['status'] == 0 || $this->data[$alias]['status'] == 1)) {
				$arrayConditions['AND']['Post.status'] = $this->data[$alias]['status'];
			}
		}

		if (AuthComponent::user('role_id') == 6) {
			$this->paginate[$alias]['joins'] = array(
				array(
					'table' => 'blog_users',
					'alias' => 'Blogueiros',
					'type' => 'LEFT',
					'conditions' => array(
						'Blogueiros.blog_id = Blog.id'
					)
				)
			);

			$arrayConditions['AND']['Blogueiros.user_id'] = AuthComponent::user('id');
		}

		$this->paginate[$alias]['order'] = array('Post.id' => 'desc');
		$this->paginate[$alias]['conditions'] = $arrayConditions;

		$posts = $this->paginate($alias);

		$blogs = $this->Blog->find('list');

		$this->set(compact('posts', 'blogs'));
	}

	/**
	 * admin_create_post
	 * Post blog create
	 */
	public function admin_create_post() 
	{
		$blogs = $this->Blog->lista();

		if (!empty($this->request->data)) {
			###TRATA AS DATAS
			//se foi informada a publish_start
			if(!empty($this->request->data['Post']['publish_start'])){
				$publish_start = str_replace('/','-', $this->request->data['Post']['publish_start']);
				$this->request->data['Post']['publish_start'] = date('Y-m-d H:i:s', strtotime($publish_start));
			}else{
				$this->request->data['Post']['creapublish_startted'] = date('Y-m-d H:i:s');
			}
			//se foi informada a publish_end
			if(!empty($this->request->data['Post']['publish_end'])){
				$publish_end = str_replace('/','-', $this->request->data['Post']['publish_end']);
				$this->request->data['Post']['publish_end'] = date('Y-m-d H:i:s', strtotime($publish_end));
			}
			//se foi informada a created
			if(!empty($this->request->data['Post']['created'])){
				$created = str_replace('/','-', $this->request->data['Post']['created']);
				$this->request->data['Post']['created'] = date('Y-m-d H:i:s', strtotime($created));
			}else{
				$this->request->data['Post']['created'] = date('Y-m-d H:i:s');
			}
			if ($this->Post->save($this->request->data)) {
				Cache::clear(false, 'ultimos_posts');
				$this->Session->setFlash('Post: '.$this->request->data['Post']['title'].' foi salvo com sucesso!', 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit_post', $this->Post->id));
			} else {
				$this->Session->setFlash($this->request->data['Post']['title'].' não pode ser salvo.', 'flash', array('class' => 'error'));
			}
		}

		$this->set(compact('blogs'));
		$this->set('title_for_layout', __d('croogo', 'Criar post'));
		$this->render('admin_form_post');
	}

	/**
	 * admin_edit_post
	 * Post blog edit
	 */
	public function admin_edit_post($id = null) 
	{
		$blogs = $this->Blog->lista();

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			###TRATA AS DATAS
			//se foi informada a publish_start
			if(!empty($this->request->data['Post']['publish_start'])){
				$publish_start = str_replace('/','-', $this->request->data['Post']['publish_start']);
				$this->request->data['Post']['publish_start'] = date('Y-m-d H:i:s', strtotime($publish_start));
			}else{
				$this->request->data['Post']['creapublish_startted'] = date('Y-m-d H:i:s');
			}
			//se foi informada a publish_end
			if(!empty($this->request->data['Post']['publish_end'])){
				$publish_end = str_replace('/','-', $this->request->data['Post']['publish_end']);
				$this->request->data['Post']['publish_end'] = date('Y-m-d H:i:s', strtotime($publish_end));
			}
			//se foi informada a created
			if(!empty($this->request->data['Post']['created'])){
				$created = str_replace('/','-', $this->request->data['Post']['created']);
				$this->request->data['Post']['created'] = date('Y-m-d H:i:s', strtotime($created));
			}else{
				$this->request->data['Post']['created'] = date('Y-m-d H:i:s');
			}
			if ($this->Post->save($this->request->data)) {
				Cache::clear(false, 'ultimos_posts');
				$this->Session->setFlash(__d('croogo', 'Post: %s foi salvo', $this->request->data['Post']['title']), 'flash', array('class' => 'success'));
				$this->Croogo->redirect($this->referer());
			} else {
				$this->Session->setFlash(__d('croogo', 'Post: %s não pode ser salvo.',$this->request->data['Post']['title']), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->Croogo->setReferer();
			$data = $this->Post->read(null, $id);
			if (empty($data)) {
				throw new NotFoundException('Invalid id: ' . $id);
			}
			$this->request->data = $data;
		}

		$this->set(compact('blogs'));
		$this->set('title_for_layout', __d('croogo', 'Editar post: %s', $this->request->data['Post']['title']));
		$this->render('admin_form_post');
	}

	/**
	 * admin_delete_post
	 * Post blog delete
	 */
	public function admin_delete_post($id = null) 
	{
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid content'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index_post'));
		}

		if ($this->Post->delete($id)) {
			Cache::clear(false, 'ultimos_posts');
			$this->Session->setFlash(__d('croogo', 'Post id: %s foi deletado', $id), 'flash', array('class' => 'success'));
			$this->Croogo->redirect(array('action' => 'index_post'));
		}

		$this->autoRender = false;
	}

	/**
	 * blogs
	 * public blogs
	 */
	public function blogs() 
	{
		$this->set('title_for_layout', 'Blogs');

		$blogs = Cache::read('index_blogs', 'nodes_index');
		if (!$blogs) {
			//procura todos os blogs
			$blogs = $this->Blog->find(
				'all',
				array(
					'conditions' 	=> array(
						'Blog.status' => 1
					),
					'contain' 		=> array(
						'Post' => array(
							'conditions' => array(
								'Post.publish_start <=' => date('Y-m-d H:i:s'),
								'OR' => array(
									'Post.publish_end >=' => date('Y-m-d H:i:s'),
									'Post.publish_end'	=> null
								),
							),
							'fields' => array(
								'id',
								'title',
								'slug',
								'excerpt'
							),
							'limit' => 1,
							'Images'
						)
					),
					'fields' => array(
						'Blog.id',
						'Blog.title',
						'Blog.slug',
						'Blog.image'
					),
					'order' => 'Blog.title'
				)
			);
			foreach ($blogs as $key => $blog) {
				if (isset($blog['Post'][0])) {
					$blogs[$key]['Post'] = $blog['Post'][0];
				}
			}
			Cache::write('lista_blogs', $blogs, 'nodes_index');
		}		
		
		$this->set(compact('blogs'));
	}

	/**
	 * view blog
	 * public view blog
	 */
	public function view() 
	{
		if (!isset($this->request->params['slug'])) {
			return $this->redirect(array('action' => 'blogs'));
		}

		$blog = $this->request->params['slug'];

		$limit = 10;

		$this->paginate = array(
			'conditions' 	=> array(
				'Post.status' => 1, 
				'Blog.slug' => $blog,
				'Post.publish_start <=' => date('Y-m-d H:i:s'),
				'OR' => array(
					'Post.publish_end >=' => date('Y-m-d H:i:s'),
					'Post.publish_end'	=> null
				),
			),
			'contain' => array(
				'Blog' => array(
					'Blogueiros.User' => array(
						'fields' => array(
							'name',
							'bio',
							'email',
							'facebook',
							'twitter',
							'instagram'
						)
					)
				),
				'Images'
			),
			'limit' 		=> $limit,
			'order' 		=> array(
				'Post.publish_start' => 'desc',
			)
		);

		$cacheDisabled = Configure::read('Cache.disable');

		if (!$cacheDisabled) {
			$cacheNamePrefix = 'posts_index_'.$blog;
			$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
			$cacheName = $cacheNamePrefix . '_' . $this->paginate['page'] . '_' . $limit;
			$cacheNamePaging = $cacheNamePrefix . $this->paginate['page'] . '_' . $limit . '_paging';
			$cacheConfig = 'nodes_index';
			$posts = Cache::read($cacheName, $cacheConfig);
			if (!$posts) {
				$posts = $this->paginate('Post');				
				Cache::write($cacheName, $posts, $cacheConfig);
				Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
			} else {
				$paging = Cache::read($cacheNamePaging, $cacheConfig);
				$this->request->params['paging'] = $paging;
			}
		} else {
			$posts = $this->paginate('Post');
		}
		
		if(empty($posts)){
			$this->Flash->error('Blog não localizado :(');
			return $this->redirect(array('action' => 'blogs'));
		}

		$this->set(compact('posts'));
		$this->set('title_for_layout', $posts[0]['Blog']['title']); // arumamr aqui para poder tirar o return de cima
		$this->render('index');
		$this->Croogo->viewFallback(array(
			'index_' . $blog,
		));
	}
	
	public function ultimos_posts() 
	{
		$limit = Configure::read('Service.ultimos_posts') ? Configure::read('Service.ultimos_posts') : 3;
		$agora = date('Y-m-d H:i:s');
		$diasAtras = '1';
		if(Configure::read('Site.blocos_blog_dias_atras')){
			$diasAtras = Configure::read('Site.blocos_blog_dias_atras');
		}
		$posts = Cache::read('ultimos_posts', 'ultimos_posts');
		if(!$posts){
			$sql = "
			SELECT
			Blog.id, 
			Blog.slug, 
			Blog.title, 
			Blog.image, 
			Post.id, 
			Post.title, 
			Post.slug, 
			Post.excerpt, 
			Post.publish_start, 
			images.filename, 
			images.alt
			FROM blogs AS Blog 
			INNER JOIN blog_nodes AS Post ON(Blog.id = Post.blog_id)
			LEFT JOIN media_images AS images ON(images.node_id = Post.id) 
			WHERE 
			Blog.status = 1 AND 
			Post.blog_id IS NOT NULL AND 
			Post.publish_start = (
				SELECT MAX(post2.publish_start) FROM blog_nodes post2
				WHERE 
					post2.blog_id = Blog.id AND 
					post2.status = 1 AND 
					post2.publish_start >= '".date('Y-m-d H:i:s', strtotime('-'.$diasAtras.' days'))."' AND 
					post2.publish_start <= '$agora' AND 
					(post2.publish_end >= '$agora' OR post2.publish_end IS NULL)
				ORDER BY post2.publish_start DESC 
				LIMIT 1
			)
			ORDER BY Post.publish_start
			DESC LIMIT $limit;
			";

			$posts = $this->Post->query($sql);			

			Cache::write('ultimos_posts', $posts, 'ultimos_posts');
		}

		$postsAgenda = "";
		$theme = Configure::read('Site.theme');
		if($theme == 'H2foz'){
			$postsAgenda = Cache::read('ultimos_posts_agenda');
			if(!$postsAgenda){
				//select para tema H2Foz, bloco "Agenda"
				$sqlPostAgenda = "SELECT
						Post.id,
						Post.title,
						Post.slug,
						Post.excerpt,
						Post.publish_start,
						Post.promote,
						Blog.id,
						Blog.slug,
						Blog.title,
						Images.filename,
						Images.alt
						FROM blog_nodes AS Post 
						INNER JOIN blogs AS Blog ON(Blog.id = Post.blog_id)
						LEFT JOIN media_images AS Images ON(Images.node_id = Post.id) 
						WHERE 
							Post.status = 1 AND 
							Blog.status = 1 AND
							Post.blog_id IS NOT NULL AND
							Post.publish_start <= '$agora' AND 
							(Post.publish_end >= '$agora' OR Post.publish_end IS NULL)
						ORDER BY Post.publish_start DESC;";

				$postsAgenda = $this->Post->query($sqlPostAgenda);
				Cache::write('ultimos_posts_agenda', $postsAgenda);
			}
		}

		$this->set('posts',$posts);
		$this->set('postAgenda', $postsAgenda);
		$element = 'ultimos_posts';
		//se foi informdo o elemento
		if(isset($this->params['element'])){
			$element = $this->params['element'];
		}
		$this->render('/Elements/'.$element, 'ajax');
	}

	/**
	 * view post blog
	 * public view post blog
	 */
	public function view_post() 
	{
		if (!isset($this->request->params['slug'])) {
			$this->Flash->error('Blog não localizado :(');
			return $this->redirect(array('action' => 'blogs'));
		}

		//$post = $this->Post->findBySlug($slug);
		$post = $this->Post->find(
			'first',
			array(
				'conditions' => array(
					'Post.status' => 1,
					'Post.slug' => $this->request->params['slug']
				),
				'contain' => array(
					'Blog' => array(
						'Blogueiros.User' => array(
							'fields' => array(
								'name',
								'bio',
								'email',
								'facebook',
								'twitter',
								'instagram'
							)
						),
						'BlogBanner'
					),
					'User',
					'Images'					
				),
			)
		);
		if (empty($post)) {
			$this->Flash->error('Post não localizado :(');
			return $this->redirect(array('action' => 'blogs'));
		}

		//prepara uma variavel
		$infoForBlock = array();
		$infoForBlock[0] = $post;
		$this->set('posts', $infoForBlock);
		$this->set(compact('post'));
		$this->set('title_for_layout', $post['Blog']['title']. ' :: ' .$post['Post']['title']);
		$this->render('view');
		$this->Croogo->viewFallback(array(
			'view_' . $post['Blog']['slug'],
		));
	}

	public function keywords($keyword) {
		//trata a palavra
		urldecode($keyword);

		$this->paginate['Post']['order'] = 'Post.publish_start DESC';
		$this->paginate['Post']['conditions']['Post.status'] 		= 1;
		$this->paginate['Post']['conditions']['Post.publish_start <='] = date('Y-m-d H:i:s');
		$this->paginate['Post']['conditions']['Post.keywords LIKE'] 		= "%".$keyword."%";
		//Condições padrão para não trazer nodes encerrados
		$this->paginate['Post']['conditions']['OR']['Post.publish_end >='] 	= date('Y-m-d H:i:s');
		$this->paginate['Post']['conditions']['OR']['Post.publish_end'] 		= null;

		//se foi informado o limit
		if (isset($this->request->params['named']['limit'])) {
			$limit = $this->request->params['named']['limit'];
		} else {
			$limit = Configure::read('Reading.nodes_per_page');
		}
		$this->paginate['Post']['limit'] = $limit;

		$cacheDisabled = Configure::read('Cache.disable');

		if (!$cacheDisabled) {
			$cacheNamePrefix = 'posts_keywords_' . $keyword;
			$this->paginate['page'] = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1;
			$cacheName = $cacheNamePrefix . '_' . $keyword . '_' . $this->paginate['page'] . '_' . $limit;
			$cacheNamePaging = $cacheNamePrefix . '_' . $keyword . '_' . $this->paginate['page'] . '_' . $limit . '_paging';
			$cacheConfig = 'nodes_index';
			$posts = Cache::read($cacheName, $cacheConfig);
			if (!$posts) {
				$posts = $this->paginate('Post');
				Cache::write($cacheName, $posts, $cacheConfig);
				Cache::write($cacheNamePaging, $this->request->params['paging'], $cacheConfig);
			} else {
				$paging = Cache::read($cacheNamePaging, $cacheConfig);
				$this->request->params['paging'] = $paging;
			}
		} else {
			$posts = $this->paginate('Post');
		}

		$this->set('title_for_layout', $keyword);
		$this->set(compact('posts'));
	}

	public function admin_banner_index(){
		$this->paginate['BlogBanner']['recursive'] = -1;
		$this->paginate['BlogBanner']['order'] = 'BlogBanner.id DESC';
        $this->set('registros', $this->paginate('BlogBanner'));
	}

	public function admin_banner_add(){
		if (!empty($this->request->data)) {
            if($this->BlogBanner->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/blog/blogs/banner_index');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
		}
		$this->set('blogs',$this->Blog->lista());
	}

	public function admin_banner_edit($id){
		if (!empty($this->request->data)) {
            if($this->BlogBanner->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/blog/blogs/banner_index');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
		}
		
		$this->request->data = $this->BlogBanner->find(
            'first',
            array(
                'conditions' => array(
                    'BlogBanner.id' => $id
                )
            )
		);
		$this->set('blogs',$this->Blog->lista());
		$this->render('admin_banner_add');
	}

	public function admin_banner_delete($id){
		$this->autoRender = false;
		$dirLocal 	= WWW_ROOT.'blog_banners'.DS;		
		$this->BlogBanner->id = $id;
		$filename = $this->BlogBanner->field('banner_src');
		if($this->BlogBanner->delete($id) && unlink($dirLocal.$filename)){
            $this->Flash->success('Registro excluído com sucesso');
            $this->redirect('/admin/blog/blogs/banner_index');
        }else{
            $this->Flash->success('Ocorreu um erro ao excluir o registro');
        }
	}

	public function admin_banner_img_delete($id){
		$this->autoRender = false;
		$dirLocal 	= WWW_ROOT.'blog_banners'.DS;		
		$this->BlogBanner->id = $id;
		$filename = $this->BlogBanner->field('banner_src');
		if(unlink($dirLocal.$filename)){
			$this->BlogBanner->saveField('banner_src', '');
            $this->Flash->success('Imagem excluído com sucesso');
        }else{
            $this->Flash->success('Ocorreu um erro ao excluir a imagem');
		}
		$this->redirect('/admin/blog/blogs/banner_edit/'.$id);
	}

	
}