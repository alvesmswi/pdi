<?php

App::uses('HttpSocket', 'Network/Http');

/**
 * Migração Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class MigracaoController extends AppController 
{
    public $uses = array('Blog.Post', 'Nodes.Node', 'Multiattach.Multiattach');

    /**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        
        $this->Auth->allow();
        $this->autoRender = false;

        Configure::write('debug', 1);
    }
    
    /**
     * migrar
     * blog_nodes to nodes (type: posts)
     */
    public function migrar()
    {
        $node = array();

        // Buscar o Post na blog_nodes
        $post = $this->Post->find('first', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'contain' => array(
                'Images'
            ),
            'order' => array(
                'Post.id' => 'DESC'
            )
        ));

        $posts_restantes = $this->Post->find('count', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'recursive' => '-1',
            'order' => array(
                'Post.id' => 'DESC'
            )
        ));
        pr('Posts restantes: '.$posts_restantes);

        if (!empty($post)) {
            $Node = $this->Node;

            $node['Node'] = $post['Post'];
            $node['Node']['id'] = null;
            $node['Node']['id_antigo'] = $post['Post']['id'];
            $node['Node']['type'] = 'posts';
            $node['Node']['path'] = '/posts/'.$post['Post']['slug'];

            if (isset($post['Images']) && !empty($post['Images'])) {
                foreach ($post['Images'] as $key => $image) {
                    $node['Multiattachments'][$key]['filename'] = $image['filename'];
                    $node['Multiattachments'][$key]['filepath'] = $image['url'];
                }
            }

            if ($Node->saveNode($node, 'posts')) {
                Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $node));

                // Pega o ID que foi salvo
                $id_salvo = $Node->getLastInsertID();

                // Se salvar corretamente os anexos
                if (isset($node['Multiattachments']) && !empty($node['Multiattachments'])) {
                    foreach ($node['Multiattachments'] as $key => $multiattachments) {
                        $this->saveImageNode($multiattachments, $id_salvo);
                    }
                }

                // Aqui atualiza o post como migrado
                $this->Post->updateAll(
                    array('Post.migrado' => 1),
                    array('Post.id' => $post['Post']['id'])
                );

                pr('blog_nodes ID: '.$post['Post']['id'].', migrado para nodes ID: '.$id_salvo);
            } else {
                pr('blog_nodes ID: '.$post['Post']['id'].', NÃO SALVOU EM nodes!');
            }

            // Chama novamente a função
            pr('<script>setTimeout(function() { location.reload(); }, 1500);</script>');
        } else {
            pr('Migração terminou!');
        }
    }

    /**
	 * saveImageNode
	 * save image for node
	 */
	private function saveImageNode($dataImage, $node) 
	{
        $url_image 	= Router::url('/', true).str_replace(' ', '%20', $dataImage['filepath']);
        
		if ($this->url_exists($url_image)) {
			$size = getimagesize($url_image);

			$name 			= strtolower(str_replace(' ', '_', basename($dataImage['filename'])));
			$nameArray 		= explode('.', $name);
			$nameArray[0] 	= Inflector::slug($nameArray[0]);
			$nameArray 		= implode('.', $nameArray);
			$name 			= $nameArray;

			$save_url 	= APP.'files'.DS.$node;
			$file_path 	= '/files/'.$node.'/'.$name;
			$mime_type 	= (!empty($size['mime'])) ? $size['mime'] : '';
			$comment 	= (isset($dataImage['comment'])) ? $dataImage['comment'] : null;

			$folder = new Folder();
            if ($folder->create($save_url)) {
                copy($url_image,$save_url.DS.$name);
            }	

			$this->Multiattach->create();
			$this->Multiattach->set(array('node_id' => $node, 'real_filename' => $file_path, 'filename' => $name, 'comment' => $comment, 'mime' => $mime_type));
			$this->Multiattach->save();
		}
    }
    
    /**
	 * url_exists
	 * verify url
	 */
    private function url_exists($url) 
    {
        $http = new HttpSocket();
        $response = $http->get($url);

        $code = $response->code;

		return ($code == 200);
	}
}
