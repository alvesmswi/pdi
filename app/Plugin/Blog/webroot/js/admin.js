$(document).ready(function() {
    var Latinise = {};
    Latinise.latin_map={
        'á' : 'a',
        'à' : 'a',
        'â' : 'a',
        'ä' : 'a',
        'ã' : 'a',
        'ç' : 'c',
        'é' : 'e',
        'è' : 'e',
        'ê' : 'e',
        'ë' : 'e',
        'í' : 'i',
        'ì' : 'i',
        'î' : 'i',
        'ï' : 'i',
        'ó' : 'o',
        'ò' : 'o',
        'ô' : 'o',
        'ö' : 'o',
        'õ' : 'o',
        'ú' : 'u',
        'ù' : 'u',
        'ü' : 'u',
        'û' : 'u'
    };
    String.prototype.latinise=function(){return this.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})};
    String.prototype.latinize=String.prototype.latinise;
    String.prototype.isLatin=function(){return this==this.latinise()}

    var slug = function(str) {
        var $slug = '';
        var trimmed = $.trim(str);

        $slug = trimmed.latinize().
        replace(/[^a-z0-9-]/gi, '-').
        replace(/-+/g, '-').
        replace(/^-|-$/g, '');

        return $slug.toLowerCase();
    }

    $("#BlogCreateForm #BlogTitle").keyup(function(e) {
        var value = $(this).val();
        $("#BlogSlug").val(slug(value));
    });

    $("#PostCreatePostForm #PostTitle").keyup(function(e) {
        var value = $(this).val();
        $("#PostSlug").val(slug(value));
    });
});