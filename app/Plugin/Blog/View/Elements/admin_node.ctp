<?php

$Blog = ClassRegistry::init('Blog.Blog');
$blogs = $Blog->lista();

echo $this->Form->input('blog_id', array(
    'label' => __d('croogo', 'Blog'),
    'options' => $blogs
));
