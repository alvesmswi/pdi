<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Blog.admin');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Post'), array('controller' => 'blogs', 'action' => 'index_post'));

	
if ($this->request->params['action'] == 'admin_create_post') {
	$formUrl = array('controller' => 'blogs', 'action' => 'create_post');
	$this->Html->addCrumb('Novo Post', array('controller' => 'blogs', 'action' => 'create_post'));
	$created 		= date('d/m/Y H:i');
	$publish_start 	= date('d/m/Y H:i');
	$publish_end 	= '';
}

if ($this->request->params['action'] == 'admin_edit_post') {
	$formUrl = array('controller' => 'blogs', 'action' => 'edit_post');
	$this->Html->addCrumb($this->request->data['Post']['title'], '/' . $this->request->url);
	//$publish_start = $this->request->data['Post']['publish_start'];

	$created 		= date('d/m/Y H:i', strtotime($this->request->data['Post']['created']));
	$publish_start 	= date('d/m/Y H:i', strtotime($this->request->data['Post']['publish_start']));
	$publish_end 	= '';
	//se tem data de fim
	if(!empty($this->request->data['Post']['publish_end'])){
		$publish_end = date('d/m/Y H:i', strtotime($this->request->data['Post']['publish_end']));
	}
}

$lookupUrl = $this->Html->apiUrl(array(
	'plugin' => 'users',
	'controller' => 'users',
	'action' => 'lookup',
));

$this->append('form-start', $this->Form->create('Post', array(
	'url' => $formUrl,
	'type'=>'file',
	'class' => 'protected-form',
)));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Post'), '#node-main');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	echo $this->Html->tabStart('node-main') .
		$this->Form->input('id') .
		$this->Form->input('blog_id', array(
			'label' => __d('croogo', 'Blog'),
		)) .
		$this->Form->input('title', array(
			'label' => __d('croogo', 'Title'),
		)) .
		$this->Form->input('slug', array(
			'label' => 'Slug (Endereço do Post)',
		)) . 
		$this->Form->input('jornalista', array(
			'label' => 'Jornalista/Autor (Se houver)',
			'value' => !empty($this->data['Post']['jornalista']) ? ($this->data['Post']['jornalista']) : $_SESSION['Auth']['User']['name']
		)) .
		$this->Form->input('keywords', array(
			'label' => 'Tags (Separado por vírgula)',
			'type' => 'text'
		)) . 
		$this->Form->input('excerpt', array(
			'label' => __d('croogo', 'Excerpt'),
			'class' => $inputClass,
			'rows' => '2'
		)) .
		$this->Form->input('body', array(
			'label' => __d('croogo', 'Body'),
			'class' => $inputClass,
		)) .
	$this->Html->tabEnd();
	echo $this->Croogo->adminTabs();
$this->end();

$username 	= isset($this->request->data['User']['name']) ? 	$this->request->data['User']['name'] : $this->Session->read('Auth.User.name');
$userId 	= isset($this->request->data['User']['id']) ? 	$this->request->data['User']['id'] : $this->Session->read('Auth.User.id');

$this->start('panels');
	echo $this->Html->beginBox('Publicar') .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
		$this->Form->input('status', array(
			'type' => 'checkbox',
			'label' => __d('croogo', 'Active'),
		));

		//se é um admin ou gerente
		if(AuthComponent::user('role_id') == 1 || AuthComponent::user('role_id') == 7){
			/*echo $this->Form->input('promote', array(
				'type' => 'checkbox',
				'label' => __d('croogo', 'Destacado na página inicial')
			));*/
			if(Configure::read('Site.posicoes_capa')){
				$options['Destaques'] = array(
					1 => '1 - Super Destaque',
					2 => '2 - Destaque médio',
					3 => '3 - Destaque pequeno'
				);
				$a = 4;
				$posicoes = Configure::read('Site.posicoes_capa');
				$grupo = 'Outros Destaques';		
				while ($a <= $posicoes) {				
					$options[$grupo][$a] = $a;
					$a++;
					if($a >= 13){
						$grupo = 'Bloco central de Destaques';
					}
					if($a >= 19){
						$grupo = 'Lista de notícias inferiores';
					}
				}
				//pr($options);exit();
				echo $this->Form->input('ordem', array(
					'label' => 'Posição na Home (Ordenação)',
					'empty' => true,
					'options' => $options
				));

			}else{
				echo $this->Form->input('ordem', array(
				'type' => 'number',
				'label' => 'Posição da Home (Ordenação)',
				'value' => isset($this->data['Post']['ordem']) ? $this->data['Post']['ordem'] : 0
				));
			}

			echo $this->Form->autocomplete('user_id', array(
				'type' => 'text',
				'label' => 'Autor',
				'default' => $userId,//Aqui é o valor padrão do campo Hidden
				'autocomplete' => array(
					'default' => $username,//Aqui é o valor padrão do campo Autocomplete
					'data-displayField' => 'name',
					'data-primaryKey' => 'id',
					'data-queryField' => 'name',
					'data-relatedElement' => '#PostUserId',
					'data-url' => $lookupUrl
				),
			));
		//se é qualquer outro usuario	
		}else{
			echo $this->Form->input('user_id', array(
				'type' => 'hidden',
				'value' => $userId
			)); 
			echo $this->Form->input('username', array(
				'type' => 'text',
				'label' => 'Autor',
				'value' => $username
			));
		}

		echo $this->Html->div('input-daterange',
			$this->Form->input('publish_start', array(
				'label' => 'Início da publicação',
				'type' => 'text',
				'value' => $publish_start,
				'class' => 'input-datetime2',
				'style' => 'text-align:left;'
			)) .
			$this->Form->input('publish_end', array(
				'label' => 'Fim da publicação',
				'type' => 'text',
				'value' => $publish_end,
				'class' => 'input-datetime2',
				'style' => 'text-align:left;',
				'before' => '<i class="fa fa-calendar"></i>'
			))
		);

		/*echo $this->Html->div('input-daterange',
			$this->Form->input('publish_start', array(
				'label' => __d('croogo', 'Publish Start'),
				'type' => 'text',
				'value' => $publish_start
			)) .
			$this->Form->input('publish_end', array(
				'label' => __d('croogo', 'Publish End'),
				'type' => 'text',
			))
		);*/
		?>
		<script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.input-datetime2').datetimepicker({
					dateFormat: 'dd/mm/yy',
					timeFormat: 'hh:mm'
				})
			;});
		</script>
		<?php
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());