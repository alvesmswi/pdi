<?php
//$this->extend('/Common/admin_index');
$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Banners', '/' . $this->request->url);
?>
<div class="row-fluid">
	<div class="actions span12">
		<div class="btn-group">
            <a href="/admin/blog/blogs/banner_add" method="get" class="btn btn-success">Novo Banner</a>
        </div>	
    </div>
</div>
<table class="table">
	<thead>
        <th>Banner</th>        
		<th><?php echo $this->Paginator->sort('banner_link', 'Link')?></th>
        <th><?php echo $this->Paginator->sort('title', 'Título')?></th>
		<th><?php echo $this->Paginator->sort('created', 'Criado Em')?></th>
		<th><?php echo $this->Paginator->sort('status', 'Status')?></th>
		<th>Ações</th>
	</thead>
	<tbody>
	<?php foreach ($registros as $registro): ?>
		<tr>			
            <td><?php echo '<img src="/blog_banners/'.$registro['BlogBanner']['banner_src'].'" width="100px" />'; ?></td>
            <td><?php echo $registro['BlogBanner']['banner_link']; ?></td>
            <td><?php echo $registro['BlogBanner']['title']; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($registro['BlogBanner']['created'])); ?></td>
			<td><?php echo $registro['BlogBanner']['status'] ? 'Ativo' : 'Inativo';?></td>
			<td>
                <?php 
                echo ' ' . $this->Croogo->adminRowAction('',
                    array('action' => 'banner_edit', $registro['BlogBanner']['id']),
                    array(
                        'icon' => $this->Theme->getIcon('update'), 
                        'tooltip' => 'Editar'
                    )
                );
                
                echo ' ' . $this->Croogo->adminRowAction('',
                    array('action' => 'banner_delete', $registro['BlogBanner']['id']),
                    array(
                        'icon' => $this->Theme->getIcon('delete'), 
                        'tooltip' => 'Excluir', 
                        'confirm' => 'Tem certeza que deseja EXCLUIR este registro?'
                    )
                );	
                ?>
            </td>
		</tr>
	<?php endforeach ?>
	</tbody>
<table>