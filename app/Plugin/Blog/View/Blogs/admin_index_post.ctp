<?php
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Posts'), '/' . $this->request->url);

$this->append('actions');
	echo $this->Croogo->adminAction(
		'Novo Post',
		array('action' => 'create_post'),
		array('button' => 'success')
	);
$this->end();

$this->append('search', $this->element('admin/posts_search'));

$this->append('form-start', $this->Form->create(
	'Blog',
	array(
		'url' => array('controller' => 'blogs', 'action' => 'process'),
		'class' => 'form-inline'
	)
));

$this->start('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Form->checkbox('checkAll'),
		$this->Paginator->sort('id', __d('croogo', 'Id')),
		$this->Paginator->sort('title', __d('croogo', 'Title')),
		$this->Paginator->sort('slug', __d('croogo', 'Slug')),
		$this->Paginator->sort('slug', __d('croogo', 'Blog')),
		$this->Paginator->sort('status', __d('croogo', 'Status')),
		''
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
?>
<tbody>
<?php foreach ($posts as $post): ?>
	<tr>
		<td><?php echo $this->Form->checkbox('Post.' . $post['Post']['id'] . '.id', array('class' => 'row-select')); ?></td>
		<td><?php echo $post['Post']['id']; ?></td>
		<td>
			<?php
				echo $this->Html->link($post['Post']['title'], array(
					'admin' => false,
					'controller' => 'blogs',
					'action' => 'view_post',
					'blog' => $post['Blog']['slug'],
					'slug' => $post['Post']['slug']
				));
			?>

			<?php if ($post['Post']['promote']): ?>
				<span class="label label-info"><?php echo __d('croogo', 'promoted'); ?></span>
				<span class="label label-default"><?php echo $post['Post']['ordem'] ?></span>
			<?php endif ?>
		</td>
		<td><?php echo $post['Post']['slug']; ?></td>
		<td><?php echo $post['Blog']['slug']; ?></td>
		<td>
			<?php
				echo $this->element('admin/toggle', array(
					'id' => $post['Post']['id'],
					'status' => (int)$post['Post']['status'],
					'model' => 'Post',
				));
			?>
		</td>
		<td>
			<div class="item-actions">
			<?php
				echo $this->Croogo->adminRowActions($post['Post']['id']);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'edit_post', $post['Post']['id']),
					array('icon' => $this->Theme->getIcon('update'), 'tooltip' => __d('croogo', 'Edit this item'))
				);
				echo ' ' . $this->Croogo->adminRowAction('',
					array('action' => 'delete_post', $post['Post']['id']),
					array('icon' => $this->Theme->getIcon('delete'), 'tooltip' => __d('croogo', 'Delete this item'))
				);
				//se já foi configurado o OneSignal
				if(Configure::read('Onesignal.oneSignalId') != '' && Configure::read('Onesignal.oneSignalKey') != ''){
					//se já foi notificado
					if($post['Post']['onesignal_push']){ ?>
						<a style="color:#ccc;" onclick="alert('Post já notificado.')"><i class="icon-comment-alt icon-large"></i></a>
					<?php 
					}else{
						echo $this->Html->link(
							'<i class="icon-comment-alt icon-large"></i>',
							'/admin/onesignal/onesignal/create/'.$post['Post']['id'].'/post',
							array('confirm' => 'Tem certeza que deseja notificar os inscritos desta notícia?')
						);
					}							
				}
			?>
			</div>
		</td>
	</tr>
<?php endforeach ?>
</tbody>
<?php
$this->end();

$this->start('bulk-action');
	echo $this->Form->input('Post.action', array(
		'label' => __d('croogo', 'Applying to selected'),
		'div' => 'input inline',
		'options' => array(
			'publish' => __d('croogo', 'Publish'),
			'unpublish' => __d('croogo', 'Unpublish'),
			'delete' => __d('croogo', 'Delete'),
			'copy' => array(
				'value' => 'copy',
				'name' => __d('croogo', 'Copy'),
				'hidden' => true,
			),
		),
		'empty' => true,
	));

	$jsVarName = uniqid('confirmMessage_');
	$button = $this->Form->button(__d('croogo', 'Submit'), array(
		'type' => 'button',
		'class' => 'bulk-process',
		'data-relatedElement' => '#' . $this->Form->domId('Post.action'),
		'data-confirmMessage' => $jsVarName,
	));
	echo $this->Html->div('controls', $button);
	$this->Js->set($jsVarName, __d('croogo', '%s selected items?'));

$this->end();

$this->append('form-end', $this->Form->end());