<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Blogs Banners', array(
    'admin' => true,
    'plugin' => 'blog',
    'controller' => 'blogs',
    'action' => 'banner_index',
  ));

//ADICIONANDO
if($this->action == 'admin_banner_add'){
    $this->Html->addCrumb('Novo', $this->here);
//EDITANDO 
}else{
    $this->Html->addCrumb('Editar', $this->here);
}

$this->append('form-start', $this->Form->create('BlogBanner', array(
    'class' => 'protected-form',
    'type'=>'file',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Novo Banner', '#tab-descricao');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-descricao');

        echo $this->Form->hidden('id');

        echo $this->Form->input('blog_id', array(
			'label' => 'Blog',
        ));
        
        echo $this->Form->input('title', array(
			'label' => 'Títitulo do Banner',
			'type' => 'text'
        ));
        
        echo $this->Form->input('banner', array(
			'label' => 'Imagem do banner',
			'type' => 'file'
		));
		//Se tem banner
		if(!empty($this->data['BlogBanner']['banner_src'])){
			echo '<img src="/blog_banners/'.$this->data['BlogBanner']['banner_src'].'" />';
			echo '<br /><br />';
			echo $this->Html->link(
				'Remover Banner',
				array('controller' => 'blog', 'controller' => 'blogs', 'action' => 'banner_img_delete', $this->data['BlogBanner']['id']),
				array(
					'confirm' => 'Tem certeza que deseja remover o banner?',
					'class' => 'btn btn-danger'
				)
			);
		}
		echo $this->Form->input('banner_link', array(
			'label' => 'Link do banner (Utilize http://)',
			'type' => 'text'
		));
		echo '<br />';
		echo $this->Form->input('banner_tag', array(
			'label' => 'TAG do banner (Caso exista um script)',
			'rows' => '4'
		));

    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação');
        echo $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        );

        echo $this->Form->input('status', array(
            'label' => 'Ativo',
            'type' => 'checkbox',
            'default' => 1
        ));    

    echo $this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());

