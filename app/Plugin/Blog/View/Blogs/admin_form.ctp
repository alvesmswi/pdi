<?php
$this->extend('/Common/admin_edit');

$this->Croogo->adminScript('Blog.admin');

$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb(__d('croogo', 'Blog'), array('controller' => 'blogs', 'action' => 'index'));

if ($this->request->params['action'] == 'admin_create') {
	$formUrl = array('action' => 'create');
	$this->Html->addCrumb(__d('croogo', 'Create'), array('controller' => 'blogs', 'action' => 'create'));
}

if ($this->request->params['action'] == 'admin_edit') {
	$formUrl = array('action' => 'edit');
	$this->Html->addCrumb($this->request->data['Blog']['title'], '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Blog', array(
	'url' => $formUrl,
	'class' => 'protected-form',
	'type' => 'file'
)));
$inputDefaults = $this->Form->inputDefaults();
$inputClass = isset($inputDefaults['class']) ? $inputDefaults['class'] : null;

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('croogo', 'Blog'), '#node-main');
	echo $this->Croogo->adminTab(__d('croogo', 'Imagens'), '#node-image');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');
	echo $this->Html->tabStart('node-main') .
		$this->Form->input('id') .
		$this->Form->input('title', array(
			'label' => __d('croogo', 'Title'),
		)) .
		$this->Form->input('slug', array(
			'label' => 'Slug (URL do Blog)',
		)) .
		$this->Form->input('link', array(
			'label' => 'Link para um blog externo (Se existir)',
		)) . 
		$this->Form->input('keywords', array(
			'label' => 'Tags (Separado por vírgula)',
			'type' => 'text'
		)) . 
		$this->Form->input('descricao', array(
			'label' => 'Descrição'
		)) . 
	$this->Html->tabEnd();

	echo $this->Html->tabStart('node-image');
		if(!empty($this->data['Blog']['image'])){ ?>
			<img src="<?php echo $this->data['Blog']['image'];?>" width="100" />
		<?php }
		echo $this->Form->input('avatar', array(
			'label' => 'Foto do Colunista ou Bloqueiro',
			'type' => 'file'
		));
		if(!empty($this->data['Blog']['capa'])){ ?>
			<img src="<?php echo $this->data['Blog']['capa'];?>" width="450" />
		<?php }
		echo $this->Form->input('capa_imagem', array(
			'label' => 'Banner do Blog',
			'type' => 'file'
		)) .
	$this->Html->tabEnd();

	echo $this->Croogo->adminTabs();
$this->end();

$username = isset($this->request->data['User']['username']) ?
	$this->request->data['User']['username'] :
	$this->Session->read('Auth.User.username');

$this->start('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'success')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('class' => 'cancel btn btn-danger')) .
		$this->Form->input('status', array(
			'type' => 'checkbox',
			'label' => __d('croogo', 'Active'),
		));
	echo $this->Html->endBox();

	echo $this->Html->beginBox(__d('croogo', 'Blogueiros')) . 
		$this->Form->select('blogueiros', $blogueiros, array('multiple' => 'checkbox'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());