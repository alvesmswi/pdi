<?php
CroogoNav::add('sidebar', 'blog', array(
	'icon' => 'list',
	'title' => 'Blog',
	'url' => array(
		'admin' => true,
		'plugin' => 'blog',
		'controller' => 'blogs',
		'action' => 'index',
	),
	'weight' => 40,
	'children' => array(
		'blogs' => array(
			'title' => 'Blogs',
			'url' => array(
				'admin' => true,
				'plugin' => 'blog',
				'controller' => 'blogs',
				'action' => 'index',
			),
			'children' => array(
				'list' => array(
					'title' => 'Lista',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'index'
					)
				),
				'create' => array(
					'title' => 'Criar',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'create'
					)
				)
			)
		),		
		'posts' => array(
			'title' => 'Posts',
			'url' => array(
				'admin' => true,
				'plugin' => 'blog',
				'controller' => 'blogs',
				'action' => 'index_post'
			),
			'children' => array(
				'list' => array(
					'title' => 'Lista',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'index_post'
					)
					
				),
				'create' => array(
					'title' => 'Criar',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'create_post'
					)
				)
			)
		),
		'blog_banners' => array(
			'title' => 'Banners',
			'url' => array(
				'admin' => true,
				'plugin' => 'blog',
				'controller' => 'blogs',
				'action' => 'banner_index'
			),
			'children' => array(
				'list' => array(
					'title' => 'Lista',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'banner_index'
					)
					
				),
				'create' => array(
					'title' => 'Criar',
					'url' => array(
						'admin' => true,
						'plugin' => 'blog',
						'controller' => 'blogs',
						'action' => 'banner_add'
					)
				)
			)
		)
	)
));
