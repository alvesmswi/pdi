<?php

Croogo::hookRoutes('Blog');

Croogo::hookAdminTab('Nodes/admin_add', 'Blog', 'Blog.admin_node', array('type' => array('posts')));
Croogo::hookAdminTab('Nodes/admin_edit', 'Blog', 'Blog.admin_node', array('type' => array('posts')));

Croogo::hookModelProperty('Node', 'belongsTo', array(
	'Blog' => array(
		'className' 	=> 'Blog.Blog',
		'foreignKey' 	=> 'blog_id'
	)
));

// Configure Wysiwyg
Croogo::mergeConfig('Wysiwyg.actions', array(
	'Blogs/admin_create_post' => array(
		array(
			'elements' => 'PostBody',
		),
	),
	'Blogs/admin_edit_post' => array(
		array(
			'elements' => 'PostBody',
		),
	)
));
//Croogo::hookBehavior('Node', 'Blog.Blog');