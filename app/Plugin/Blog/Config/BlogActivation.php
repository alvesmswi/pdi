<?php
App::uses('CakeSchema', 'Model');
App::uses('ConnectionManager', 'Model');

/**
 * Blog Activation
 *
 * Activation class for Blog plugin.
 *
 * @package  Blog
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class BlogActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}

	public function onActivation(Controller $controller) 
	{
		// Carrega os Models
		$controller->loadModel('Blog.Blog');

		// Gera as permissões para acessar plugin
        $controller->Croogo->addAco('Blog');
		$controller->Croogo->addAco('Blog/Blogs/admin_index');
		$controller->Croogo->addAco('Blog/Blogs/admin_create');

		// Tem que gerar na tabela nodes, o blog_id
		$controller->Blog->query("ALTER TABLE nodes ADD blog_id INT(20) DEFAULT NULL;");

		// Gerar o Tipo de Conteúdo Posts
		$type = $controller->Blog->query("SELECT * FROM types WHERE alias = 'posts';");
		if (empty($type)) {
			$controller->Blog->query("INSERT INTO types (title, alias, format_show_author, format_show_date, comment_status, comment_approve, comment_spam_protection, comment_captcha, params) VALUES ('Posts', 'posts', 0, 0, 0, 0, 0, 0, 'routes=true');");
		}

		// Gerar tabela de Blogs e Usuários
		$tableName = array('blogs', 'blog_users');
		$pluginName = 'Blog';
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();

		if (!in_array($tableName, $tables)) {
			$schema = & new CakeSchema(array(
				'name' => $pluginName,
				'path' => App::pluginPath($pluginName) . 'Config' . DS . 'Schema',
			));
			$schema = $schema->load();

			foreach ($schema->tables as $table => $fields) {
				if (!in_array($table, $tables)) {
					$create = $db->createSchema($schema, $table);
					
					try {
						$db->execute($create);
					} catch (PDOException $e) {
						die(__('Could not create table: %s', $e->getMessage()));
					}
				}
			}
		}
	}

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		// Remove as permissões
		$controller->Croogo->removeAco('Blog');
	}
}