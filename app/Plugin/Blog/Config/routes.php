<?php
CroogoRouter::connect('/admin/blog', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'index', 
	'admin' => true
));

CroogoRouter::connect('/admin/blog/posts', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'index_post', 
	'admin' => true
));

CroogoRouter::connect('/blogs', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'blogs'
));
CroogoRouter::connect('/colunas', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'blogs'
));
CroogoRouter::connect('/blogs/archives/*', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'blogs'
));

CroogoRouter::connect('/blog/migrar', array(
	'plugin' => 'blog', 'controller' => 'migracao', 'action' => 'migrar'
));

CroogoRouter::connect('/blog/:blog/post/:slug', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view_post'
));
CroogoRouter::connect('/coluna/:coluna/post/:slug', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view_post'
));
CroogoRouter::connect('/blog/:slug/*', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view'
));
CroogoRouter::connect('/coluna/:slug/*', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view'
));

CroogoRouter::connect(
	'/blogs/ultimos_posts', 
	array(
		'plugin' => 'blog', 
		'controller' => 'blogs', 
		'action' => 'ultimos_posts'
	)
);

CroogoRouter::connect('/posts/tag/*', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'keywords'
));

CroogoRouter::connect('/blogs/:slug/*', array(
	'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view'
));


