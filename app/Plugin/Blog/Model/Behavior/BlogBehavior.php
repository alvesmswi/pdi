<?php

App::uses('ModelBehavior', 'Model');
App::uses('Blog', 'Blog.Model');

class BlogBehavior extends ModelBehavior {

/**
 * Setup
 *
 * @param Model $model
 * @param array $config
 * @return void
 */
	public function setup(Model $model, $config = array()) {
		if (is_string($config)) {
			$config = array($config);
		}

		$this->settings[$model->alias] = $config;
	}

/**
 * afterFind callback
 *
 * @param Model $model
 * @param array $results
 * @param boolean $primary
 * @return array
 */
	public function afterFind(Model $model, $results = array(), $primary = false) {

		if ($primary && isset($results[0][$model->alias]['type']) && $results[0][$model->alias]['type'] == 'post') {
			$model->bindModel(
				array('hasOne' => array(
						'Blog' => array(
							'className' => 'Blog'
						)
					)
				)
			);
			$model->Blog->recursive = -1;
			
			foreach ($results as $i => $result) {				
				if (isset($results[$i][$model->alias]['id'])) {
					//Busca o blog
					$arrayBlog = $model->Blog->find(
						'all', 
						array(
							'conditions' => array(
								'Blog.id' => $results[$i][$model->alias]['blog_id']
							),
							/*'contain' => array(
								'Blogueiros' => array('User')
							)*/
							'recursive' => 2
						)
					);
					//se encontrou o blogo
					if(!empty($arrayBlog) && isset($arrayBlog[0])){
						//array_push($results[$i], $arrayBlog[0], false);
						$results[$i] += $arrayBlog[0];
					}					
				}
			}
		}
		return $results;
	}
}
