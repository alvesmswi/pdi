<?php

class BlogUser extends AppModel 
{
	public $name 		= 'BlogUser';
    public $useTable 	= 'blog_users';
    public $validate 	= array(
		'blog_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.'
		),
		'user_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.'
		)
    );
    
    public $belongsTo   = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		),
		'Blog' => array(
			'className' => 'Blog.Blog',
			'foreignKey' => 'blog_id'
		)
	);
}
