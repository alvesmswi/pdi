<?php

class BlogBanner extends AppModel 
{
	public $name 		= 'BlogBanner';
    public $useTable 	= 'blog_banners';
    
    public $belongsTo   = array(
		'Blog' => array(
			'className' => 'Blog.Blog',
			'foreignKey' => 'blog_id'
		)
	);
	public function beforeSave($options = array()) 
	{
		//se foi informado o banner
		if(!empty($this->data['BlogBanner']['banner']['tmp_name'])){
			$dirLocal 	= WWW_ROOT.'blog_banners'.DS;
			//verifica se o caminho existe
			if(!is_dir($dirLocal)){
				//cria o diretorio
				mkdir($dirLocal, 0777, true);				
			}
			//upload do banner
			$file 	= new File($this->data['BlogBanner']['banner']['tmp_name']);
			$fileName = uniqid().'_'.$this->data['BlogBanner']['banner']['name'];
			$file->copy($dirLocal.DS.$fileName);
			$file->close();
			//sava no banco
			$this->data['BlogBanner']['banner_src'] = $fileName;
		}
	}
}
