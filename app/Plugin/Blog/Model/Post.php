<?php
App::uses('AppModel', 'Model');
App::uses('BlogTag', 'Blog.Model');

/**
 * Post
 *
 * @category Post.Model
 * @package  Post.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Post extends AppModel {
	public $name 		= 'Post';
	public $useTable 	= 'blog_nodes';
	public $validate 	= array(
		'title' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'slug' => array(
			/*'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This slug has already been taken.',
			),*/
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Slug cannot be empty.',
			),
		),
		'blog_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'user_id' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
	);
	public $belongsTo = array(
		'User' => array(
			'className' => 'Users.User',
			'foreignKey' => 'user_id'
		),
		'Blog' => array(
			'className' => 'Blog.Blog',
			'foreignKey' => 'blog_id'
		)
	);

	/**
	 * beforeFind callback
	 *
	 * @param array $queryData
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]) && isset($value['Blog']['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view_post',
					'blog' => $value['Blog']['slug'], 'slug' => $value[$this->alias]['slug']
				);
			}
		}

		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		//verifica a ordenação
		if(
			isset($this->data[$this->alias]['ordem']) && 
			$this->data[$this->alias]['ordem'] > 0 && 
			$this->data[$this->alias]['publish_start'] <= date('Y-m-d H:i:s')
		){
			//procura POSTS com a mesma order, e remove
			$this->updateAll(
				array(
					'Post.ordem'		=> '0', //desativa a ordenação do Node
					'Post.promote' 		=> '0' //Remove a promoção
				),
				array(
					'Post.publish_start <='	=> date('Y-m-d H:i:s'), //que seja menor que agora
					'Post.id <>' 			=> $this->data[$this->alias]['id'], //que não seja ele mesmo,
					'Post.ordem' 			=> $this->data[$this->alias]['ordem'] //que tenha a mesms ordem
				)
			);
			$this->data['Node']['promote'] = 1; // força como promovido

			//procura NOTICIAS com a mesma order, e remove
			App::uses('Node', 'Model');
			$Node = new Node();
			$Node->updateAll(
				array(
					'Node.ordem'		=> '0', //desativa a ordenação do Node
					'Node.promote' 		=> '0', //Remove a promoção
				),
				array(
					'Node.publish_start <='	=> date('Y-m-d H:i:s'), //que seja menor que agora
					'Node.ordem' 			=> $this->data[$this->alias]['ordem'],//que tenha a mesam ordem
				)
			);

			$this->data[$this->alias]['promote'] = 1; // força como promovido
		}

		//Se a posição for 0
		if(isset($this->data[$this->alias]['ordem']) && $this->data[$this->alias]['ordem'] < 1){
			$this->data[$this->alias]['promote'] = 0; // desativa a promoção
		}

		return true;
	}

	public function afterSave($created, $options = array()) {				
		//quando salvar, limpa o cache
		Cache::clear(false, 'ultimos_posts');
	}
}