<?php
App::uses('AppModel', 'Model');
App::uses('User', 'Model');
App::uses('BlogTag', 'Blog.Model');
App::uses('BlogUser', 'Blog.Model');

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Blog
 *
 * @category Blog.Model
 * @package  Croogo.Blog.Model
 * @version  1.0
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Blog extends AppModel {
	public $name = 'Blog';
	public $urlImport = null;
	public $validate = array(
		'title' => array(
			'rule' => 'notBlank',
			'message' => 'This field cannot be left blank.',
		),
		'slug' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This slug has already been taken.',
			),
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Slug cannot be empty.',
			),
		),
	);
	public $hasMany = array(
		'Post' => array(
			'className' => 'Blog.Post',
			'order' => 'publish_start DESC'
		),
		'Blogueiros' => array(
			'className' => 'Blog.BlogUser'
		),
		'BlogBanner' => array(
			'className' => 'Blog.BlogBanner'
		)
	);

	/*public $hasAndBelongsToMany = array(
		'Blogueiros' => array(
			'className' => 'User',
			'joinTable' => 'blog_users',
			'foreignKey' => 'blog_id',
			'associationForeignKey' => 'user_id'
		)
	);*/

	/**
	 * beforeFind callback
	 *
	 * @param array $q
	 * @return array
	 */
	public function beforeFind($queryData) 
	{
		return $queryData;
	}

	
	/**
	 * afterFind callback
	 *
	 * @param array $results
	 * @param array $primary
	 * @return array
	 */
	public function afterFind($results, $primary = false) 
	{
		foreach ($results as $key => $value) {
			if (isset($value[$this->alias]) && isset($value['Blog']['slug'])) {
				$results[$key][$this->alias]['url'] = array(
					'plugin' => 'blog', 'controller' => 'blogs', 'action' => 'view', 'slug' => $value[$this->alias]['slug']
				);
			}

			if (isset($value['Blogueiros']) && !empty($value['Blogueiros'])) {
				$arrayBlogueiros = array();
				foreach ($value['Blogueiros'] as $keyBlogueiro => $blogueiro) {
					$arrayBlogueiros[$keyBlogueiro] = $blogueiro['user_id'];
				}

				$results[$key][$this->alias]['blogueiros'] = $arrayBlogueiros;
			}
		}
		
		return $results;
	}

	/**
	 * beforeSave callback
	 *
	 * @return boolean
	 */
	public function beforeSave($options = array()) 
	{
		if (isset($this->data[$this->alias]['avatar']['name'])) {
			$image = $this->upload($this->data[$this->alias]['avatar']);
			if ($image) {
				$this->data[$this->alias]['image'] = $image;
			}
		} elseif (isset($this->data[$this->alias]['avatar']['importacao'])) {
			$image = $this->uploadImporter($this->data[$this->alias]['avatar']);
			if ($image) {
				$this->data[$this->alias]['image'] = $image;
			}
		}

		if (isset($this->data[$this->alias]['capa_imagem']['name'])) {
			$image = $this->upload($this->data[$this->alias]['capa_imagem'], 'capa');
			if ($image) {
				$this->data[$this->alias]['capa'] = $image;
			}
		} elseif (isset($this->data[$this->alias]['capa_imagem']['importacao'])) {
			$image = $this->uploadImporter($this->data[$this->alias]['capa_imagem'], 'capa');
			if ($image) {
				$this->data[$this->alias]['capa'] = $image;
			}
		}

		return true;
	}

	private function upload($file, $type = 'avatar') 
	{
		$base_dir 	= WWW_ROOT.'files'.DS.'blogs'.DS.$type;
		$path 		= DS.'files'.DS.'blogs'.DS.$type;
		$dir 		= new Folder($base_dir, true, 0755);

		if ($file['error'] === 0) {
            $file_name 		= $file['name'];
            $destination 	= $path.DS.$file_name;
            $file 			= new File($file['tmp_name']);

            $file->copy($base_dir.DS.$file_name);
            $file->close();

            return $destination;
		}

		return false;
	}

	private function uploadImporter($data, $type = 'avatar') 
	{
		$url_image 	= $this->urlImport.str_replace(' ', '%20', $data['importacao']);
		$size 		= getimagesize($this->urlImport.str_replace(' ', '%20', $data['importacao']));

		$name 			= strtolower(str_replace(' ', '_', basename($data['importacao'])));
		$nameArray 		= explode('.', $name);
		$nameArray[0] 	= Inflector::slug($nameArray[0]);
		$nameArray 		= implode('.', $nameArray);
		$name 			= $nameArray;

		$save_url 	= APP.'files'.DS.'blogs'.DS.$type;
		$file_path 	= DS.'files'.DS.'blogs'.DS.$type.DS.$name;
		$folder 	= new Folder();

		if ($folder->create($save_url)) {
			if (copy($url_image, $save_url.DS.$name)) {
				return $file_path;
			}
			/*try {
				copy($url_image, $save_url.DS.$name);
			} catch (Exception $e) {
			 	$e->getMessage();
			}*/
		}
	}

	public function lista() 
	{
		$opcoes['conditions']['AND']['Blog.status'] = 1;

		if (AuthComponent::user('role_id') == 6) {
			$opcoes['joins'] = array(
				array(
					'table' => 'blog_users',
					'alias' => 'Blogueiros',
					'type' => 'LEFT',
					'conditions' => array(
						'Blogueiros.blog_id = Blog.id'
					)
				)
			);

			$opcoes['conditions']['AND']['Blogueiros.user_id'] = AuthComponent::user('id');
		}

		$registros = $this->find('list', $opcoes);

		return $registros;
	}

	public function afterSave($created, $options = array()) {		
		//quando salvar, limpa o cache
		Cache::clear(false, 'ultimos_posts');

		if (isset($this->data[$this->alias]['blogueiros'])) {
			$blog_id = $this->id;
			$blogueiros = $this->data[$this->alias]['blogueiros'];

			$BlogUser = new BlogUser();
			
			$BlogUser->deleteAll(array(
				'BlogUser.blog_id' => $blog_id
			));

			if (!empty($blogueiros)) {
				foreach ($blogueiros as $key => $blogueiro) {
					$saveBlogueiro['BlogUser']['id'] = null;
					$saveBlogueiro['BlogUser']['blog_id'] = $blog_id;
					$saveBlogueiro['BlogUser']['user_id'] = $blogueiro;

					$BlogUser->save($saveBlogueiro);
				}
			}
		}
	}

	public function blogueiros()
	{
		$User = new User();

		$blogueiros = $User->find('list', array(
			'conditions' => array(
				'role_id' => 6
			),
			'order' => array(
				'name' => 'asc'
			)
		));

		return $blogueiros;
	}
}