<?php
class AgenciaActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        $controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Agencia.endereco',
			'value' =>       '',
			'title' =>       'Endereço FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Agencia.usuario',
			'value' =>       '',
			'title' =>       'Usuário para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Agencia.senha',
			'value' =>       '',
			'title' =>       'Senha para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Agencia.diretorio',
			'value' =>       '',
			'title' =>       'Diretorio para buscar os XMLS',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      256,
			'params' =>      '',
		));
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Redirect');
	}
}