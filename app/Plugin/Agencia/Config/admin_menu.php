<?php
CroogoNav::add('sidebar', 'settings.children.agencia', array(
	'icon' => 'list',
	'title' => 'Agência',
	'url' => array(
		'admin' => true,
		'plugin' => 'agencia',
		'controller' => 'agencia',
		'action' => 'settings',
	),
	'weight' => 40,
	'children' => array(
		'settings' => array(
			'title' => 'Settings',
			'url' => array(
				'admin' => true,
				'plugin' => 'agencia',
				'controller' => 'agencia',
				'action' => 'settings'
			)
		),
		'importar' => array(
			'title' => 'Importar',
			'url' => array(
				'admin' => true,
				'plugin' => 'agencia',
				'controller' => 'agencia',
				'action' => 'index'
			)
		)
	)
));