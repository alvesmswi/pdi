<?php
Croogo::hookRoutes('agencia');

CroogoNav::add('settings.children.agencia',array(
	'title' => __('Agencia'),
	'url' => array('plugin' => 'agencia', 'controller' => 'agencia', 'action' => 'admin_index'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Agencia');
    
?>