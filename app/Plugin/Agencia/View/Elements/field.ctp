<?php
//pr($data);
$uuid = String::uuid();
$id 			= isset($data['id']) ? $data['id'] : null;
$title 			= isset($data['title']) ? $data['title'] : null;
$description 	= isset($data['description']) ? $data['description'] : null;
$value 			= isset($data['value']) ? $data['value'] : null;

if(!isset($data['title'])){
	$indice = $uuid;
}else{
	$indice =' termo_'.$title.'_'.$id;
}
?>
<tr class="route">
	<td>
		<?php 
		echo $this->Form->input(
			'AgenciaTermos.'.$indice.'.title',
			array(
				'type'=>'text',
				'label'=>false, 
				'placeholder'=>'Slug do Termo',
				'value'=>$title,
				'style'=>'width:100px;'
			)
		); ?>		
	</td>
	<td>
		<?php 
		echo $this->Form->input(
			'AgenciaTermos.'.$indice.'.description',
			array(
				'type'=>'text',
				'label'=>false, 
				'placeholder'=>'Cód. Agência',
				'value'=>$description,
				'style'=>'width:100px;'
			)
		); ?>
	</td>	
	<td>
		<?php 

		echo $this->Form->input(
			'AgenciaTermos.'.$indice.'.value',
			array(
				'type'=>'number',
				'label'=>false, 
				'placeholder'=>'Id do Termo',
				'value'=>$value,
				'style'=>'width:100px;'
			)
		); ?>
	</td>
	<td>
	    <?php echo $this->Html->link('X','#',array('class'=>'remove-route')); ?>
	</td>
</tr>
<?php 
$this->Form->unlockField('AgenciaTermos.'.$indice.'.title');
$this->Form->unlockField('AgenciaTermos.'.$indice.'.value');
$this->Form->unlockField('AgenciaTermos.'.$indice.'.description');
?>
