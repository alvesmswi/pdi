<?php
$this->Html->css('Agencia.redirect.css', null, array('inline' => false));
$this->Html->script('Agencia.redirect.js', false);
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Agência Estado', array(
    'admin' => true,
    'plugin' => 'agencia',
    'controller' => 'agencia',
    'action' => 'settings',
  ));

$this->append('form-start', $this->Form->create('Agencia', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Termos', '#setting-termos');
    echo $this->Croogo->adminTab('FTP', '#setting-ftp');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('setting-termos'); ?>
    	<br />

		<table class="table table-striped">
		    <thead>
		    	<tr>
		    		<th><b>Cód. Agência</b></th>
		    		<th><b>Slug do Termo</b></th>
		    		<th><b>ID do Termo</b></th>
		    		<th><b>Remover</b></th>
		    	</tr>
		    </thead>
		    <tbody>
		    	<tr>
		    		<?php echo $this->element('field',array('plugin'=>'Agencia')); ?>
		    	</tr>
	    		<?php
	    		//se tem configuracoes
	    		if(isset($this->request->data['AgenciaTermos']) && !empty($this->request->data['AgenciaTermos'])){
	    			foreach($this->request->data['AgenciaTermos'] as $termo){
		          		echo $this->element('field',array('data'=>$termo,'plugin'=>'Agencia'));
		        	}
	    		}		        	
	        	?> 
		    </tbody>
		</table>
    <?php
    echo $this->Html->tabEnd();

    echo $this->Html->tabStart('setting-ftp');
       echo $this->Form->input('Agencia.endereco', array(
            'label' => 'Host do endereço',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Agencia.porta', array(
            'label' => 'Porta de acesso',
            'type'  => 'number',
            'required'	=> true
        )) .
        $this->Form->input('Agencia.usuario', array(
            'label' => 'Usuário / Login',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Agencia.senha', array(
            'label' => 'Senha / Password',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Agencia.diretorio', array(
            'label' => 'Diretório que será baixado os XMLs',
            'type'  => 'text',
            'help'	=> 'O endereço informado é relativo à pasta "public", por exemplo: "agencia/xmls"',
            'required'	=> true
        )).
        $this->Form->input('Agencia.exclusiva', array(
            'label' => 'Cada matéria será exclusiva para assinantes!',
            'type'  => 'checkbox',
            'required'	=> false
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->Form->unlockField('AgenciaTermos.*');
$this->append('form-end', $this->Form->end());