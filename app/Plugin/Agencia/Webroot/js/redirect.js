/**
 * Redirect
 *
 */
var Redirect = {};

/**
 * functions to execute when document is ready
 *
 * @return void
 */
Redirect.documentReady = function() {
  Redirect.addRedirect();
  Redirect.removeRedirect();
}



/**
 * add redirect field
 *
 * @return void
 */
Redirect.addRedirect = function() {
    $('a.add-redirect').click(function() {
        $.get(Croogo.basePath+'admin/agencia/agencia/add_field', function(data) {
            $('#agencia-fields').append(data);
        });
        return false;
    });
}

/**
 * remove redirect field
 *
 * @return void
 */
Redirect.removeRedirect = function() {
    $('.route a.remove-route').live('click',function() {
      $(this).parents('.route').remove();
      return false;
    });
}


/**
 * document ready
 *
 * @return void
 */
$(document).ready(function() {
  Redirect.documentReady();
});