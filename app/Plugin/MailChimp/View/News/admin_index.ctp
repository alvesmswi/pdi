<h3>Newsletters</h3>

<div class="row-fluid">
	<div class="span12 actions">
		<ul class="nav-buttons">
			<li><a href="/admin/mail_chimp/news/add" method="get" class="btn btn-success">Nova Newsletter</a></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<table cellpadding="0" cellspacing="0"  class="table table-striped table-bordered table-condensed">
			<tr>
				<th><?php echo $this->Paginator->sort('id');?></th>
				<th><?php echo $this->Paginator->sort('user', 'Usuário');?></th>
				<th><?php echo $this->Paginator->sort('transaction_id', 'NewsID');?></th>
				<th><?php echo $this->Paginator->sort('created', 'Criada Em');?></th>
				<th>Ações</th>
			</tr>
			<?php
			foreach ($news as $new): ?>
			<tr>
				<td><?php echo $new['Newsletter']['id']; ?></td>
				<td><?php echo $new['Newsletter']['user']; ?></td>
				<td><?php echo $new['Newsletter']['transaction_id']; ?></td>
				<td><?php echo $new['Newsletter']['created']; ?></td>	
				<td><a href="/mail_chimp/news/view/<?php echo $new['Newsletter']['id'];?>" target="_blank">Ver</a></td>			
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<?php if ($pagingBlock = $this->fetch('paging')): ?>
			<?php echo $pagingBlock; ?>
		<?php else: ?>
			<?php if (isset($this->Paginator) && isset($this->request['paging'])): ?>
				<div class="pagination">
					<ul>
						<?php echo $this->Paginator->first('< ' . __d('croogo', 'Primeiro')); ?>
						<?php echo $this->Paginator->prev('< ' . __d('croogo', 'Anterior')); ?>
						<?php echo $this->Paginator->numbers(); ?>
						<?php echo $this->Paginator->next(__d('croogo', 'Próximo') . ' >'); ?>
						<?php echo $this->Paginator->last(__d('croogo', 'Último') . ' >'); ?>
					</ul>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
