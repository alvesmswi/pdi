<?php
class MailChimpActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeActivation(&$controller) {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {
		//Cria as tabelas
		CakePlugin::load('MailChimp');
		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		include_once(App::pluginPath('MailChimp').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$MailChimpSchema = new MailChimpSchema();
		foreach ($MailChimpSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($MailChimpSchema, $table));
			}
		}

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('MailChimp/News/admin_config');
		$controller->Croogo->addAco('MailChimp/News/add', array('gerente'));

		//Configurações basicas
		$controller->Setting->write('MailChimp.apikey','6b03264458a08be93a9b78096382bbca-us17',array('description' => 'ApiKey do MailChimp','editable' => 1));

		Cache::clear(false, '_cake_model_');
	}

	/**
	 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 *
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

	public function onDeactivation(&$controller) {
		
	}
}