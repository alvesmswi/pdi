<?php
CroogoCache::config('mailchimp_lists', array_merge(
	Configure::read('Cache.defaultConfig'),
	array('groups' => array('lists'))
));

CroogoNav::add('mailchimp', array(
	'title' => 'Newsletter',
	'icon' => 'envelope',
	'url' => array(
		'admin' => true,
		'plugin' => 'mail_chimp',
		'controller' => 'news',
		'action' => 'index',
	),
	'weight' => 60,
	'children' => array(		
		'emails' => array(
			'title' => 'Emails',
			'url' => array(
				'admin' => true,
				'plugin' => 'mail_chimp',
				'controller' => 'news',
				'action' => 'index',
			),
			'children' => array(
				'add' => array(
					'title' => 'Criar',
					'url' => array(
						'admin' => true,
						'plugin' => 'mail_chimp',
						'controller' => 'news',
						'action' => 'add',
					)
				),
				'list' => array(
					'title' => 'Listar Todas',
					'url' => array(
						'admin' => true,
						'plugin' => 'mail_chimp',
						'controller' => 'news',
						'action' => 'index',
					)
				)
			)
		),
		'listas' => array(
			'title' => 'Listas de Envio',
			'url' => array(
				'admin' => true,
				'plugin' => 'mail_chimp',
				'controller' => 'mail_lists',
				'action' => 'index',
			)
		),
		'config' => array(
			'title' => 'Configurar',
			'url' => array(
				'admin' => true,
				'plugin' => 'mail_chimp',
				'controller' => 'news',
				'action' => 'config',
			)
		),
	)
));
