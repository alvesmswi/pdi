<?php 
class MailChimpSchema extends CakeSchema {

	public $name = 'MailChimp';

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {

	}

	public $newsletters = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user' 				=> array('type' => 'string', 'length' => 255, 'null' => false),
		'transaction_id' 	=> array('type' => 'string', 'length' => 255, 'null' => true),
		'html' 				=> array('type' => 'text','null' => true),		
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $mail_lists = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'title' 			=> array('type' => 'string', 'length' => 255, 'null' => false),
		'alias' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'body' 				=> array('type' => 'text','null' => true),		
		'list_id' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'status' 			=> array('type' => 'integer','null' => true),
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),
		'thank_you_message' 			=> array('type' => 'string', 'length' => 255, 'null' => true),
		'error_message' 			=> array('type' => 'string', 'length' => 255, 'null' => true),		
		'indexes' 			=> array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
