<?php

App::uses('MailChimpAppController', 'MailChimp.Controller');

class NewsController extends MailChimpAppController {

	public $uses = array('MailList');

	public function beforeFilter() {
		parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
		$this->Security->unlockedActions += array('subscribe', 'admin_add','admin_get_nodes');
		$this->Auth->allow('subscribe');
		include(App::pluginPath('MailChimp').DS.'vendor'.DS.'autoload.php');
	}

	public function admin_config() 
	{
		$this->set('title_for_layout','MailChimp');     
        if (!empty($this->request->data)) {
        if($this->Setting->deleteAll(
            array('Setting.key LIKE' => 'MailChimp.%')
            )){
                foreach($this->request->data['MailChimp'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'MailChimp.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        } 

        //limpa o this->data
        $this->request->data = array();
        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'MailChimp.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $this->request->data['MailChimp'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
	}
	
	public function admin_index() 
	{
		$this->loadModel('Newsletter');
		$this->Newsletter->recursive = -1;
		$this->paginate = array(
		    'order' => array('Newsletter.created'=>'DESC')
		);
		$this->set('news', $this->paginate('Newsletter'));
	}

	/*
	FUNÇÃO DE INCRIÇÃO PARA A NEWSLETTER
	*/
	public function subscribe($alias) {

		
		$this->autoRender = false;

		$list = $this->MailList->findByAlias($alias);

		if (!isset($list['MailList']['id'])) {
			throw new NotFoundException();
		}
		
		if ($this->request->is('post') && !empty($this->request->data['email'])) {
			
			$mailChimp = new Mailchimp(Configure::read('MailChimp.apikey'));

			//verifica se já é membro
			$membroCheck = $mailChimp->lists->memberInfo(
					$list['MailList']['list_id'], 
					['emails' => ['email' => trim($this->request->data['email'])]]
			);
			
			//se este email não está cadastrado ainda
			if(isset($membroCheck['data']) && !empty($membroCheck['data'])){
				$this->Flash->warning('Este e-mail já está cadastrado em nossa newsletter!');
			}else{				
				//envia o cadastro
				$result = $mailChimp->lists->subscribe(
					$list['MailList']['list_id'], 
					array(
						'email' => trim($this->request->data['email'])
					)
				);
				//se deu erro
				if (isset($result['status'])) {
					$this->Flash->error($list['MailList']['error_message'] . ' ' . $result['error']);
				} else {
					$this->Flash->success($list['MailList']['thank_you_message']);
				}
			}
			
			$this->redirect($this->referer());
		}
	}

	/*
	FUNÇÃO PARA A CRIAÇÃO DA NEWS - HTML DAS ÚLTIMA NOTÍCIAS
	*/
	public function createnews($html, $titulo = null) {
		$this->autoRender = false;

		$list = $this->MailList->find('first');
		
		if (!isset($list['MailList']['id'])) {
			throw new NotFoundException();
		}

		//cria uma campanha (novas noticias)
		$mailChimp = new Mailchimp(Configure::read('MailChimp.apikey'));
		$email_news = Configure::read('Site.email_newsletter');
		if(empty($email_news)){
			$email_news = Configure::read('Site.email');
		}
		$news = $mailChimp->campaigns->create(
			'regular',
			array(
				'list_id' => $list['MailList']['list_id'],
				'subject' => $titulo ? $titulo : $list['MailList']['title'],
				'from_email' => $email_news,
				'from_name' => Configure::read('Site.title')
			),
			array(
				'html' => $html
			)
		);

		//se retornou o ID da campanha
		if(isset($news['id']) && !empty($news['id'])){			
			//envia o email
			if($this->sendnews($news['id'])){
				//grava no banco
				$this->loadModel('Newsletter');
				$arrayNews = array();
				$arrayNews['Newsletter']['user'] = $this->Auth->user('name');
				$arrayNews['Newsletter']['transaction_id'] = $news['id'];
				$arrayNews['Newsletter']['html'] = preg_replace('/\s+/', ' ', $html);
				$this->Newsletter->saveAll($arrayNews);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/*
	FUNÇÃO PARA O ENVIO DOS EMAILS
	*/
	public function sendnews($cid) {
		$this->autoRender = false;
		$mailChimp = new Mailchimp(Configure::read('MailChimp.apikey'));
		//envia o comando par ao mailchimp para disparar os emails
		if($mailChimp->campaigns->send($cid)){
			return true;
		}else{
			return false;
		}
	}

	public function admin_add() {
		$this->layout = 'ajax';

	}

	public function view($id) {
		$this->autoRender = false;
		$this->loadModel('Newsletter');
		$news = $this->Newsletter->find(
			'first',
			array(
				'conditions' => array(
					'Newsletter.id' => $id
				)
			)
		);
		return $news['Newsletter']['html'];
	}

	public function admin_get_nodes($search = '')
    {
		//$this->log($this->params->data['pesquisa']);
        Configure::write('debug',0);
        $this->autoRender = false;

		$response = array();

		$pesquisa = trim($this->params->data['pesquisa']);

		//verifica se tem algum tipo
		$arrayPesquisa = explode(':', $pesquisa);

		//se foi informado post
		if(isset($arrayPesquisa[1]) && ($arrayPesquisa[0] == 'post' || $arrayPesquisa[0] == 'p')){
			$pesquisa = trim($arrayPesquisa[1]);
			
			//Procura posts
			$sql = "
			SELECT 
				Node.id, 
				Node.title, 
				Node.slug, 
				Node.publish_start,
				Multiattach.filename,
				Blog.title,
				Blog.slug,
				Blog.image   
			FROM blog_nodes AS Node
			LEFT JOIN media_images AS Multiattach ON(Multiattach.node_id = Node.id AND Multiattach.controller = 'blogs')
			LEFT JOIN blogs AS Blog ON(Blog.id = Node.blog_id) 
			WHERE (Node.id = '$pesquisa' OR Node.title LIKE '%$pesquisa%') 
			GROUP BY Node.id 
			ORDER BY Node.publish_start DESC
			LIMIT 20;
			";

			$nodes = $this->MailList->query($sql);

			if (!empty($nodes)) {
				foreach ($nodes as $key => $node) {
					//se não tem imagem
					if(empty($node['Blog']['image'])){
						$imgFull = 'img/sem-foto.jpg';	
						$img = Router::url('/', true).'img/sem-foto.jpg';							
					}else{
						$imgFull = str_replace('\\','/',$node['Blog']['image']);
						$img = Router::url('/', true).$node['Blog']['image'];
					}
					//prepara o json
					$response[$key]['id'] = $node['Node']['id'];
					$response[$key]['date'] = date('d/m/Y H:i', strtotime($node['Node']['publish_start']));
					$response[$key]['category'] = $node['Blog']['title'];
					$response[$key]['type'] = 'post';
					$response[$key]['title'] = $node['Node']['title'];
					$response[$key]['url'] = Router::url('/', true).'blog/'.$node['Blog']['slug'].'/post/'.$node['Node']['slug'];
					$response[$key]['image'] = $img;
					$response[$key]['imgFull'] = $imgFull;
					$response[$key]['summary'] = '';
				}
			}
			
		}else{ 

			//se pesquisa está vazio
			if(empty($pesquisa)){
				//´traz só noticias da capa
				$sql = "
				SELECT 
					Node.id, 
					Node.title, 
					Node.path,
					Node.type,
					Node.publish_start,              
					NoticiumDetail.chapeu,
					NoticiumDetail.titulo_capa, 
					Multiattach.filename,
					Term.title  
				FROM nodes AS Node
				LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
				LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
				LEFT JOIN model_taxonomies AS Mtax ON(Mtax.foreign_key = Node.id) 
				LEFT JOIN taxonomies AS Tax ON(Tax.id = Mtax.taxonomy_id) 
				LEFT JOIN terms AS Term ON(Term.id = Tax.term_id) 
				WHERE Node.promote = 1 
				GROUP BY Node.id 
				ORDER BY Node.ordem ASC
				LIMIT 40;
				";
			}else{
				//Senão, procura nodes normais
				$sql = "
				SELECT 
					Node.id, 
					Node.title, 
					Node.path,
					Node.type,
					Node.publish_start,              
					NoticiumDetail.chapeu,
					NoticiumDetail.titulo_capa, 
					Multiattach.filename,
					Term.title  
				FROM nodes AS Node
				LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
				LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
				LEFT JOIN model_taxonomies AS Mtax ON(Mtax.foreign_key = Node.id) 
				LEFT JOIN taxonomies AS Tax ON(Tax.id = Mtax.taxonomy_id) 
				LEFT JOIN terms AS Term ON(Term.id = Tax.term_id) 
				WHERE (Node.id = '$pesquisa' OR Node.title LIKE '%$pesquisa%')
				GROUP BY Node.id 
				ORDER BY Node.publish_start DESC
				LIMIT 20;
				";
			}
			
			$nodes = $this->MailList->query($sql);
			
			if (!empty($nodes)) {
				foreach ($nodes as $key => $node) {
					//se não tem imagem
					if(empty($node['Multiattach']['filename'])){
						$imgFull = 'sem-foto.jpg';
						$img = Router::url('/', true).'img/sem-foto.jpg';
					}else{
						$imgFull = $node['Multiattach']['filename'];
						$img = Router::url('/', true).'fl/600largura/'.$node['Multiattach']['filename'];
					}
					//prepara o json
					$response[$key]['id'] = $node['Node']['id'];
					$response[$key]['date'] = date('d/m/Y H:i', strtotime($node['Node']['publish_start']));
					$response[$key]['category'] = (!empty($node['NoticiumDetail']['chapeu'])) ? $node['NoticiumDetail']['chapeu'] : $node['Term']['title'];
					$response[$key]['type'] = $node['Node']['type'];
					$response[$key]['title'] = $node['Node']['title'];
					$response[$key]['url'] = Router::url('/', true).$node['Node']['path'];
					$response[$key]['image'] = $img;
					$response[$key]['imgFull'] = $imgFull;
					$response[$key]['summary'] = '';
				}
			}
		}

        echo json_encode($response);
	}
	
	public function admin_get_html(){
		Configure::write('debug',0);
		$this->autoRender = false;

		$page_data = file_get_contents('php://input');

		if (!empty($page_data)) {
			$page_data = json_decode($page_data);
		}

		//Variaveis importantes
		$siteUrl = Router::url('/', true);
		$siteNome = Configure::read('Site.title'); 
		$urlTemplate = 'newsletter-template.html';
		//se não existe um template customizado, pega o  padrã
		if(!file_exists($urlTemplate)){
			$urlTemplate = App::pluginPath('MailChimp').'webroot/template/newsletter-template.html';
		}

		//Carrega o HTML de exemplo
		$templateHTML = new DomDocument();
		$templateHTML->preserveWhiteSpace = FALSE;
		$templateHTML->loadHTMLFile($urlTemplate);
		$finder = new DOMXPath($templateHTML);

		//Link da versão online
		$nodeItem = $finder->query("//*[@id='linkOnline']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', $siteUrl.'ultima-newsletter.html');
		}

		//troca o link da logo
		$nodeItem = $finder->query("//*[@id='logoLink']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', $siteUrl);
			$item->setAttribute('title', $siteNome);
		}

		//troca a logo
		$nodeItem = $finder->query("//*[@id='logoImg']");
		foreach($nodeItem as $item){
			$item->setAttribute('src', $siteUrl.'img/logo.png');
			$item->setAttribute('alt', $siteNome);
		}

		//link facebook
		$nodeItem = $finder->query("//*[@id='facebookLink']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', Configure::read('Social.facebook'));
		}

		//link instagram
		$nodeItem = $finder->query("//*[@id='instagramLink']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', Configure::read('Social.instagram'));
		}

		//link youtube
		$nodeItem = $finder->query("//*[@id='youtubeLink']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', Configure::read('Social.youtube'));
		}

		//link RSS
		$nodeItem = $finder->query("//*[@id='rssLink']");
		foreach($nodeItem as $item){
			$item->setAttribute('href', $siteUrl.'noticia.rss');
		}

		//Endereço
		$nodeItem = $finder->query("//*[@id='endereco']");
		foreach($nodeItem as $item){
			$item->nodeValue = Configure::read('Site.endereco');
		}

		//Cidade
		$nodeItem = $finder->query("//*[@id='cidade']");
		foreach($nodeItem as $item){
			$item->nodeValue = Configure::read('Site.cidade');
		}

		//Estado
		$nodeItem = $finder->query("//*[@id='estado']");
		foreach($nodeItem as $item){
			$item->nodeValue = Configure::read('Site.estado');
		}

		//CEP
		$nodeItem = $finder->query("//*[@id='cep']");
		foreach($nodeItem as $item){
			$item->nodeValue = Configure::read('Site.cep');
		}

		//Nome do site no rodape
		$nodeItem = $finder->query("//*[@id='rodapeSite']");
		foreach($nodeItem as $item){
			$item->nodeValue = $siteNome;
		}

		//pega o titulo da newsletter
		$newsTitle = $page_data->titulo;

		//se não tem nada no BG Escuro
		/*if(
			empty($page_data->destaque2) && 
			empty($page_data->news7) && 
			empty($page_data->news8)
		){
			//Sessão Escura
			$nodeItem = $finder->query("//*[@id='sessaoEscura']");
			foreach($nodeItem as $item){
				$item->parentNode->removeChild($item);
			}
		}
		*/


		//Percorre os itens 
		foreach ($page_data as $key => $value) {

			// Faz a pesquisa na estrutura
			$rootPattern = "//*[contains(@class, '". $key ."')]";
		
			// Verifica se existe o titulo
			if(isset($value->title)) {
		
				// Insere os dados na estrutura do template
				if (!empty($value->category)) {
					$nodeCategory= $finder->query($rootPattern."//*[contains(@class, 'category')]");
					$nodeCategory->item(0)->nodeValue = $value->category;
				}
				$nodeImg = $finder->query($rootPattern."//*[contains(@class, 'imgNews')]");

				if (!empty($value->image) && $value->imgFull != 'sem-foto.jpg') {					
					if($key == 'destaque1' || $key == 'destaque2'){
						$nodeImg->item(0)->setAttribute('src', $siteUrl.'fl/600largura/'.$value->imgFull);	
					}else if($value->type == 'post'){
						$nodeImg->item(0)->setAttribute('src', $siteUrl.$value->imgFull);	
					}else{
						$nodeImg->item(0)->setAttribute('src', $siteUrl.'fl/288largura/'.$value->imgFull);	
					}

					//coloca o link na imagem
					$nodeLink = $finder->query($rootPattern."//*[@class='linkImg']");
					$nodeLink->item(0)->setAttribute('href', $value->url);
					
				}else{
					//Se não tem imagem, deleta TD inteira
					$imgtag = $nodeImg->item(0);
					$aTag = $imgtag->parentNode;
					$tdTag = $aTag->parentNode;
					$trTag = $tdTag->parentNode;
					$trTag->removeChild($tdTag);
				}
		
				if (!empty($value->title)) {
					$nodeTitle = $finder->query($rootPattern."//*[@class='title']");
					$nodeTitle->item(0)->nodeValue = $value->title;
				}
				
				//Se foi informado o link
				if (!empty($value->url)) {					
					$nodeLink = $finder->query($rootPattern."//*[@class='linkTitle']");
					$nodeLink->item(0)->setAttribute('href', $value->url);
				}
		
				if (!empty($value->date)) {
					$nodePublished = $finder->query($rootPattern."//*[contains(@class, 'published')]");
					$nodePublished->item(0)->nodeValue = $value->date;
				}
		
			} else {   
				// Deleta o nó ativo, removendo codigo html da saida
				$nodeRemove = $finder->query($rootPattern);
				$nodeRemove->item(0)->nodeValue = "";
			
			}
		
		}
		//salva o arquivo HTML
		file_put_contents('ultima-newsletter.html',$templateHTML->saveHTML());
		$this->createnews($templateHTML->saveHTML(), $newsTitle);
	}

	// REMOVE OS ESPACOS EM BRANCO
	function _sanitize_output($html) {

		$search = array(
			'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
			'/[^\S ]+\</s',     // strip whitespaces before tags, except space
			'/(\s)+/s',         // shorten multiple whitespace sequences
			'/<!--(.|\s)*?-->/' // Remove HTML comments
		);

		$replace = array(
			'>',
			'<',
			'\\1',
			''
		);

		$html = preg_replace($search, $replace, $buffer);
		return $html;
	}

	function newsletter(){
		$this->render('newsletter');
	}
} 