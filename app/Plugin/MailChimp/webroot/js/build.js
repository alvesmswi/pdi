var app = angular.module('newsApp',['ngDragDrop']);

url = '/admin/mail_chimp/news/';

app.controller('newsletterBuilder', function ($scope, $http) {

    $scope.pesquisar = function (pesquisa) {

        $http.post(url + 'get_nodes.json', { "pesquisa": pesquisa })
        .then(function(res) {
            $scope.completing = true;
            $scope.news = res.data;
        });

    };

    //quando iniciar
    angular.element(document).ready(function () {
        $scope.pesquisar();
    });

    $scope.titulo = '',
    $scope.destaque = {};
    $scope.destaque2 = {};
    $scope.news1 = {};
    $scope.news2 = {};
    $scope.news3 = {};
    $scope.news4 = {};    
    $scope.news5 = {};
    $scope.news6 = {};
    $scope.news7 = {};
    $scope.news8 = {};
    $scope.news9 = {};
    $scope.news10 = {};
    $scope.news11 = {};
    $scope.news12 = {};

    $scope.sendDatas = function() {
        var allDatas = {
            titulo: $scope.titulo,
            destaque1: $scope.destaque,
            destaque2: $scope.destaque2,
            news1: $scope.news1,
            news2: $scope.news2,
            news3: $scope.news3,
            news4: $scope.news4,
            news5: $scope.news5,
            news6: $scope.news6,
            news7: $scope.news7,
            news8: $scope.news8,
            news9: $scope.news9,
            news10: $scope.news10,
            news11: $scope.news11,
            news12: $scope.news12
        };
        
        if (confirm("Deseja ENVIAR a Newsletter agora?")) {
            $('#btnEnviar').attr('disabled', true);
            $('#btnCancelar').attr('disabled', true);
            //Envia para processamento
            $http.post(url + 'get_html', allDatas)
            .then(function (success) {
                alert('Newsletter envida com sucesso!');
                window.location.href = '/admin/mail_chimp/news';
            });
        }
    };

    $scope.cancelar = function() {
        if (confirm("Deseja CANCELAR a construção da Newsletter?")) {
            window.location.href = '/admin/mail_chimp/news';
         }
    }
});

// jQuery
$(function () {
    $("[data-drop='true']").droppable({
        hoverClass: "dashed"
        
    });
});

