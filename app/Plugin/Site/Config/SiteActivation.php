<?php
class SiteActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        //da permissões para acessar plugin
        $controller->Croogo->addAco('Site');
		$controller->Croogo->addAco('Site/Site/admin_index');

		Cache::clear(false, '_cake_model_');
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Site');
	}
}