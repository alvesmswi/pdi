<?php
Croogo::hookRoutes('site');

CroogoNav::add('sidebar', 'site', array(
	'icon' => 'cog',
	'title' => 'Config. Site',
	'url' => array(
		'admin' => true,
		'plugin' => 'site',
		'controller' => 'site',
		'action' => 'index',
	)
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Onesignal');
    
?>