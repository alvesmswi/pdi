<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Site', array(
    'admin' => true,
    'plugin' => 'Site',
    'controller' => 'Site',
    'action' => 'index',
  ));

$this->append('form-start', $this->Form->create('Site', array(
    'class' => 'protected-form',
    'enctype' => 'multipart/form-data'
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Informações', '#tab-info');
    echo $this->Croogo->adminTab('Mídias Sociais', '#tab-midias');    
    echo $this->Croogo->adminTab('Endereço', '#tab-endereco');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-info');

        echo $this->Form->input('Site.title', array(
            'label' => 'Título do Site',
            'type'  => 'text',
            'required'	=> true
        ));
        echo $this->Form->input('Site.tagline', array(
            'label' => 'Descrição do Site',
            'type'  => 'textarea',
            'rows' => 2,
            'required'	=> true
        ));
        echo $this->Form->input('Site.keywords', array(
            'label' => 'Palavras-chaves que definem o Site',
            'type'  => 'textarea',
            'rows' => 2,
            'required'	=> true
        ));
        echo $this->Form->input('Site.email', array(
            'label' => 'Email do Site',
            'type'  => 'email',
            'required'	=> true
        ));   
        $value = $this->data['Site']['layout_destaque'];
        echo $this->Form->input('Site.layout_destaque', array(
           // 'div' => 'radio-group',
            'type'  => 'radio',
            'value' => $value,
            'options'	=> array(
               /* 1 => '<img src="/theme/Mswi/img/Capa1.png"/>',
                2 => '<img src="/theme/Mswi/img/Capa2.png"/>',
                3 => '<img src="/theme/Mswi/img/Capa3.png"/>'*/
                1 => 'Capa 1',
                2 => 'Capa 2 (Imagem grande)',
                3 => 'Capa 3',
                4 => 'Capa 4 (Espaço para Vídeo)'
            )
        ));
        
        echo $this->Form->input('Site.logo', array(
            'label' => 'Logo',
            'type' => 'file',
            'required' => false
        ));

        if(isset($this->data['Site']['logo']) && !empty($this->data['Site']['logo'])){                
                echo '<img src="'.Router::url('/', true).$this->data['Site']['logo'].'" class="thumbnail" />';                
            }

        echo $this->Form->input('Site.logo_footer', array(
            'label' => 'Logo Rodapé',
            'type' => 'file',
            'required' => false
        ));
        if(isset($this->data['Site']['logo_footer']) && !empty($this->data['Site']['logo_footer'])){
                echo '<img src="'.Router::url('/', true).$this->data['Site']['logo_footer'].'" class="thumbnail" />';
            }
    
    echo $this->Html->tabEnd();
    
    echo $this->Html->tabStart('tab-midias');    
    
        echo $this->Form->input('Social.twitter', array(
            'label' => 'Twitter',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Social.facebook', array(
            'label' => 'Facebook',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Social.instagram', array(
            'label' => 'Instagram',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Social.youtube', array(
            'label' => 'YouTube',
            'type'  => 'text',
            'required'	=> false
        ));
    
    echo $this->Html->tabEnd();

    echo $this->Html->tabStart('tab-endereco');

        echo $this->Form->input('Site.endereco', array(
            'label' => 'Endereco',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Site.cidade', array(
            'label' => 'Cidade',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Site.estado', array(
            'label' => 'Estado',
            'type'  => 'text',
            'required'	=> false
        ));
        echo $this->Form->input('Site.cep', array(
            'label' => 'CEP',
            'type'  => 'text',
            'required'	=> false
        ));
    echo $this->Html->tabEnd();

    echo $this->Html->tabStart('tab-config');    
    
            echo $this->Form->input('Meta.keywords', array(
                'label' => 'Palavras chave',
                'type'  => 'textarea',
                'rows' => 5,
                'required'	=> false
            ));
            echo $this->Form->input('Site.refresh_home', array(
                'label' => 'Refresh Home',
                'type'  => 'text',
                'required'	=> false
            ));
            echo $this->Form->input('Site.refresh_internas', array(
                'label' => 'Refresh Internas',
                'type'  => 'text',
                'required'	=> false
            ));
            echo $this->Form->input('Site.titulo_imagem_do_dia', array(
                'label' => 'Título do bloco imagem do dia',
                'type'  => 'text',
                'required'	=> false
            ));
    
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());