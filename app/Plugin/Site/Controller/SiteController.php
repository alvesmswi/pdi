<?php
App::uses('SiteAppController', 'Site.Controller');

  class SiteController extends SiteAppController {

    public $name = 'Site';
    public $uses = array('Site.Site', 'Settings.Setting');    
    
    public function beforeFilter() {
        parent::beforeFilter();
        //desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Auth->allow('cron','test');
    }

    public function admin_index()
    {
        $this->set('title_for_layout','Site');     

        if (!empty($this->request->data)) {
            // pr($this->request->data);exit;
             if(isset($this->request->data['Site']['logo']) && !empty($this->request->data['Site']['logo'])) {                 
                if($this->request->data['Site']['logo']['size'] == 0){
                    $file = WWW_ROOT . Configure::read('Site.logo');
                    if(file_exists($file)){                    
                        $this->request->data['Site']['logo'] = Configure::read('Site.logo');                        
                    }else{
                        $this->request->data['Site']['logo'] = '';
                    }
                }else{                                
                    $this->request->data['Site']['logo'] = 'img/'.$this->Site->upload($this->request->data['Site']['logo']);
                }
            }
             if(isset($this->request->data['Site']['logo_footer']) && !empty($this->request->data['Site']['logo_footer'])) {
                if($this->request->data['Site']['logo_footer']['size'] == 0){
                    $file = WWW_ROOT . Configure::read('Site.logo_footer');
                    if(file_exists($file)){
                        $this->request->data['Site']['logo_footer'] = Configure::read('Site.logo_footer');
                    }else{
                        $this->request->data['Site']['logo_footer'] = '';
                    }
                }else{
                    $this->request->data['Site']['logo_footer'] = '/img/'.$this->Site->upload($this->request->data['Site']['logo_footer']);
                }
            }
            foreach($this->request->data as $model => $value){
                foreach($value as $key => $v){
                    if(empty($v)){
                        continue;
                    }
                    //procura pra ver se existe
                    $setting = $this->Setting->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Setting.key' => $model.'.'.$key
                            ),
                            'recursive' => -1
                        )
                    );
                    //se já existe
                    if(!empty($setting)){
                        //atualiza
                        $this->Setting->updateAll(
                            array(
                                'Setting.value' => "'".$v."'"
                            ),
                            array(
                                'Setting.id' => $setting['Setting']['id']
                            )
                        );
                    }else{
                        //cria
                        $arraySave = array(
                            'id' => null,
                            'key' => $model.'.'.$key,
                            'value' => $v,
                            'description' => '',
                            'params' => '',
                            'title' => $model.'.'.$key,
                            'editable' => 1
                        );
                        $this->Setting->saveAll($arraySave);
                    }                    
                }               
            }
        }

        //limpa o this->data
        $this->request->data = array();

        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Site.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $arrayKey = explode('.',$dados['Setting']['key']);
                $key = $arrayKey[1];
                $this->request->data['Site'][$key] = $dados['Setting']['value'];
            }
        }

        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Meta.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $arrayKey = explode('.',$dados['Setting']['key']);
                $key = $arrayKey[1];
                $this->request->data['Meta'][$key] = $dados['Setting']['value'];
            }
        }

        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Social.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $arrayKey = explode('.',$dados['Setting']['key']);
                $key = $arrayKey[1];
                $this->request->data['Social'][$key] = $dados['Setting']['value'];
            }
        }
        
    }

    #http://jornal.localhost/site/site/cron
    public function cron(){
        //App::import('Controller', 'Folhapress.Folhapress');

        App::uses('FolhapressController','Folhapress.Controller');

        $this->autoRender = false;

        //Folhapress
        $Folhapress = new FolhapressController();
        $Folhapress->importar(1,1);

        return true;
    }

    public function test(){
        $this->autoRender = false;

        //testa a conexão com o Banco
        $db = ConnectionManager::getDataSource('default');
        $dbname = $db->config['database'];
        $dbuser = $db->config['login'];
        $dbpass = $db->config['password'];
        $dbhost = $db->config['host'];
        $connect = mysql_connect($dbhost, $dbuser, $dbpass);
        if($connect){
            return true;
        }else{
            return false;
        }
    }
}
?>