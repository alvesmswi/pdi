<?php
Croogo::hookRoutes('Horoscopo');

CroogoNav::add('sidebar', 'Horoscopo', array(
	'icon' => 'heart',
	'title' => 'Horoscopo',
	'url' => array(
		'admin' => true,
		'plugin' => 'horoscopo',
		'controller' => 'horoscopo',
		'action' => 'index',
	)
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Horoscopo');
		
	// Configure Wysiwyg
	Croogo::mergeConfig('Wysiwyg.actions', array(
		'Horoscopo/admin_add' => array(
			array(
				'elements' => 'HoroscopoResumo',
			),
		),
		'Horoscopo/admin_edit' => array(
			array(
				'elements' => 'HoroscopoResumo',
			),
		)
	));
?>