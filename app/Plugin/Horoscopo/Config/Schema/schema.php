<?php 
class HoroscopoSchema extends CakeSchema {

	public $name = 'Horoscopo';

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $horoscopos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'resumo' => array('type' => 'text', 'null' => true),
		'aries' => array('type' => 'text', 'null' => true),
		'touro' => array('type' => 'text', 'null' => true),
		'gemeos' => array('type' => 'text', 'null' => true),
		'cancer' => array('type' => 'text', 'null' => true),
		'leao' => array('type' => 'text', 'null' => true),
		'virgem' => array('type' => 'text', 'null' => true),
		'libra' => array('type' => 'text', 'null' => true),
		'escorpiao' => array('type' => 'text', 'null' => true),
		'sagitario' => array('type' => 'text', 'null' => true),
		'capricornio' => array('type' => 'text', 'null' => true),
		'aquario' => array('type' => 'text', 'null' => true),
		'peixes' => array('type' => 'text', 'null' => true),
		'publish_start' => array('type' => 'datetime', 'null' => true),
		'publish_end' => array('type' => 'datetime', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'status' => array('type' => 'tinyint', 'null' => true, 'default' => 1),
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
