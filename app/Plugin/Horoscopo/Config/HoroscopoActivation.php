<?php
class HoroscopoActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		//Cria a tabela
		include_once(App::pluginPath('Horoscopo').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$HoroscopoSchema = new HoroscopoSchema();
		foreach ($HoroscopoSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($HoroscopoSchema, $table));
			}
		}
		
        //da permissões para acessar plugin
        $controller->Croogo->addAco('Horoscopo');
		$controller->Croogo->addAco('Horoscopo/Horoscopo/admin_index', array('gerente', 'editor'));
		$controller->Croogo->addAco('Horoscopo/Horoscopo/admin_add', array('gerente', 'editor'));
		$controller->Croogo->addAco('Horoscopo/Horoscopo/admin_edit', array('gerente', 'editor'));
		$controller->Croogo->addAco('Horoscopo/Horoscopo/admin_delete', array('gerente', 'editor'));
		$controller->Croogo->addAco('Horoscopo/Horoscopo/admin_toggle', array('gerente', 'editor'));
		$controller->Croogo->addAco('Horoscopo/Horoscopo/todos', array('publico'));

		//Cria o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'horoscopo')));
		//se o Bloco não existe
		if(empty($block)){
			$controller->Block->create();
			$controller->Block->set(array(
				'visibility_roles' => '',
				'visibility_paths' => '',
				'region_id'        => '',
				'title'            => 'Horoscopo',
				'alias'            => 'horoscopo',
				'body'             => '[element:horoscopo plugin="horoscopo"]',
				'show_title'       => 0,
				'status'           => 1
			));
			$controller->Block->save();
		}
		
		Cache::clear(false, '_cake_model_');
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Horoscopo');
		//Apaga o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'horoscopo')));
        if($block){
            $controller->Block->delete($block['Block']['id']);
		} 
	}
}