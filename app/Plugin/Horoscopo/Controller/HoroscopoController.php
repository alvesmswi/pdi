<?php
App::uses('HoroscopoAppController', 'Horoscopo.Controller');

  class HoroscopoController extends HoroscopoAppController {

    public $name = 'Horoscopo';
    public $uses = array('Horoscopo.Horoscopo');    
    public $components = array('Mswi');    
    
    public function beforeFilter() {
        parent::beforeFilter();
        //desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions += array('admin_index');
        $this->Auth->allow('todos');
    }

    public function todos()
    {
        $arrayHoroscopos = array();
        $corpo = '';

        //Pega os dados
        $arrayDados = $this->Horoscopo->find(
            'first',
            array( 
                'conditions' => array(
                    'Horoscopo.publish_start <=' => date('Y-m-d H:i:s'),
                    'OR' => array(
                        'Horoscopo.publish_end >=' => date('Y-m-d H:i:s'),
                        'Horoscopo.publish_end' => null
                    )
                ),
                'recursive' => -1,
                'order' => 'publish_start DESC'
            )            
        );
        
        if(!empty($arrayDados)){
            $corpo = $arrayDados['Horoscopo']['resumo'];
            unset($arrayDados['Horoscopo']['id']);
            unset($arrayDados['Horoscopo']['resumo']);
            unset($arrayDados['Horoscopo']['publish_start']);
            unset($arrayDados['Horoscopo']['publish_end']);
            unset($arrayDados['Horoscopo']['created']);
            unset($arrayDados['Horoscopo']['modified']);
            unset($arrayDados['Horoscopo']['status']);
            
            foreach($arrayDados['Horoscopo'] as $key => $horoscopo){
                if(!empty($horoscopo)){
                    $arrayHoroscopos[$this->Horoscopo->getName($key)] = $horoscopo;
                }                
            }
        }
        
        $this->set('corpo',$corpo);
        $this->set('horoscopos',$arrayHoroscopos);
    }

    public function admin_index()
    {
        $this->Horoscopo->recursive = -1;
		$this->paginate['Horoscopo']['order'] = 'Horoscopo.publish_start DESC';
		$this->set('registros', $this->paginate());
    }

    public function admin_add()
    {        
        if (!empty($this->request->data)) {
            //se foi informada a data de inicio
			if(isset($this->request->data['Horoscopo']['publish_start']) && !empty($this->request->data['Horoscopo']['publish_start'])){
				//trata a data
				$this->request->data['Horoscopo']['publish_start'] = $this->Mswi->tratarDataHora($this->request->data['Horoscopo']['publish_start']);
            }
            //se foi informada a data de inicio
			if(isset($this->request->data['Horoscopo']['publish_end']) && !empty($this->request->data['Horoscopo']['publish_end'])){
				//trata a data
				$this->request->data['Horoscopo']['publish_end'] = $this->Mswi->tratarDataHora($this->request->data['Horoscopo']['publish_end']);
            }
            
            if($this->Horoscopo->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/horoscopo');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
        }
    }

    public function admin_edit($id)
    {        
        $this->render('admin_add');

        if (!empty($this->request->data)) {
            //se foi informada a data de inicio
			if(isset($this->request->data['Horoscopo']['publish_start']) && !empty($this->request->data['Horoscopo']['publish_start'])){
				//trata a data
				$this->request->data['Horoscopo']['publish_start'] = $this->Mswi->tratarDataHora($this->request->data['Horoscopo']['publish_start']);
            }
            //se foi informada a data de inicio
			if(isset($this->request->data['Horoscopo']['publish_end']) && !empty($this->request->data['Horoscopo']['publish_end'])){
				//trata a data
				$this->request->data['Horoscopo']['publish_end'] = $this->Mswi->tratarDataHora($this->request->data['Horoscopo']['publish_end']);
            }
            
            if($this->Horoscopo->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/horoscopo');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
        }

        $this->request->data = $this->Horoscopo->find(
            'first',
            array(
                'conditions' => array(
                    'Horoscopo.id' => $id
                )
            )
        );

        $this->render('admin_add');
    }

    public function admin_delete($id)
    {        
        if($this->Horoscopo->delete($id)){
            $this->Flash->success('Registro excluído com sucesso');
            $this->redirect('/admin/horoscopo');
        }else{
            $this->Flash->success('Ocorreu um erro ao excluir o registro');
        }
    }

    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
	}
}
?>