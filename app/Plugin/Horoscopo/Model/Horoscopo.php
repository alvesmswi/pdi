<?php
App::uses('AppModel', 'Model');

    class Horoscopo extends AppModel
    {
        function getName($key){
            $arrayKeys = array(
                'aries'         => 'Áries',
                'touro'         => 'Touro',
                'gemeos'        => 'Gêmeos',
                'cancer'        => 'Câncer',
                'leao'          => 'Leão',
                'virgem'        => 'Virgem',
                'libra'         => 'Libra',
                'escorpiao'     => 'Escorpião',
                'sagitario'     => 'Sagitário',
                'capricornio'   => 'Capricórnio',
                'aquario'       => 'Aquário',
                'peixes'        => 'Peixes'
            );
            return $arrayKeys[$key];
        }
    }
?>