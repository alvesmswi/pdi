<?php
$this->Html->meta(
	array('property' => 'og:title', 'content' => 'Horóscopo'),
	null,
	array('inline'=>false)
);
$this->Html->meta(
  array('property' => 'og:description', 'content' => 'Horóscopo, Áries, Touro, Gêmeos, Câncer, Leão, Virgem, Libra, Escorpião, Sagitário, Capricórnio, Aquário, Peixes'),
  null,
  array('inline'=>false)
);
$this->Html->meta(
	array('property' => 'og:url', 'content' => Router::url(null, true)),
	null,
	array('inline'=>false)
);
$this->Html->meta(
	array('name' => 'twitter:card', 'content' => 'summary'),
	null,
	array('inline'=>false)
);
//gera o description
$this->Html->meta(
	array(
		'name' => 'description ', 
		'content' => 'Horóscopo, Áries, Touro, Gêmeos, Câncer, Leão, Virgem, Libra, Escorpião, Sagitário, Capricórnio, Aquário, Peixes'
	),
	null,
	array('inline'=>false)
);

echo $this->Html->css('Horoscopo.horoscopo');
?>
<section id="main-content" class="main-content col-lg-8 col-md-12 col-sm-12 horoscopo">
	<article id="article-content" class="article__content" itemprop="content">
		<header class="container" itemprop="headline">
			<div class="heading" role="heading">
				<h1 itemprop="name">
					Horóscopo
				</h1>
			</div>
			
		</header>
		<div class="container">
			<div class="row">
				<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="content" itemtype="http://schema.org/articleBody">

					<?php echo $this->element('share');?>

					<div id="corpo">
					<?php 
					echo $corpo;
					if(isset($horoscopos) && !empty($horoscopos)) { ?>
                        <ul>
							<?php 
                            foreach($horoscopos as $key => $horoscopo):
                            ?>
								<li class="<?php echo strtolower(Inflector::slug($key)); ?>">
									<?php 
									echo $this->Html->image(
										'Horoscopo.'.strtolower(Inflector::slug($key)).'.png',
										array(
											'align' => 'left',
											'title' => $key,
											'alt' => $key
										)
									);
									?>
									<strong><?php echo $key; ?></strong>
									<div><?php echo trim($horoscopo); ?></div>
                                </li>
                            <?php endforeach; ?>
                        	</ul>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer do artigo -->
		<footer>
			<?php echo $this->element('keywords_links');?>		
			<?php echo $this->element('facebook_comentarios');?>
		</footer>
	</article>
</section>