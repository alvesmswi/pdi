<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Horoscopo', array(
    'admin' => true,
    'plugin' => 'horoscopo',
    'controller' => 'horoscopo',
    'action' => 'index',
  ));

//ADICIONANDO
if($this->action == 'admin_add'){
    $this->Html->addCrumb('Novo', $this->here);
    $inicio = date('d/m/Y H:i:s');    
    $fim = '';
//EDITANDO 
}else{
    $this->Html->addCrumb('Editar', $this->here);
    $inicio = $this->data['Horoscopo']['publish_start'];
    $fim = isset($this->data['Horoscopo']['publish_end']) ? $this->data['Horoscopo']['publish_end'] : '';
}

$this->append('form-start', $this->Form->create('Horoscopo', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Previsão Geral', '#tab-descricao');
    echo $this->Croogo->adminTab('Previsão por Signo', '#tab-horoscopo');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-descricao');

        echo $this->Form->hidden('id');

        echo $this->Form->input(
            'resumo',
            array(
                'label' => 'Descrição / Texto',
                'type'  => 'textarea',
                'rows' => 10
            )
        );

    echo $this->Html->tabEnd();
    echo $this->Html->tabStart('tab-horoscopo');

        echo $this->Form->input(
            'aries',
            array(
                'label' => 'Áries',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );

        echo $this->Form->input(
            'touro',
            array(
                'label' => 'Touro',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'gemeos',
            array(
                'label' => 'Gêmeos',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'cancer',
            array(
                'label' => 'Câncer',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'leao',
            array(
                'label' => 'Leão',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'virgem',
            array(
                'label' => 'Virgem',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'libra',
            array(
                'label' => 'Libra',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'escorpiao',
            array(
                'label' => 'Escorpião',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );
        
        echo $this->Form->input(
            'sagitario',
            array(
                'label' => 'Sagitário',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );

        echo $this->Form->input(
            'capricornio',
            array(
                'label' => 'Capricórnio',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );

        echo $this->Form->input(
            'aquario',
            array(
                'label' => 'Aquário',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );

        echo $this->Form->input(
            'peixes',
            array(
                'label' => 'Peixes',
                'type'  => 'textarea',
                'rows' => 3                
            )
        );

    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação');
        echo $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        );

        echo $this->Form->input('status', array(
            'label' => 'Ativo',
            'type' => 'checkbox'
        ));
    
        echo $this->Form->input('publish_start', array(
            'label' => 'Início',        
            'class' => 'input-datetime',
            'value' => $inicio,
            'type' => 'text'
        ));
        echo $this->Form->input('publish_end', array(
            'label' => 'Fim',
            'class' => 'input-datetime',
            'value' => $fim,
            'type' => 'text'
        )); 
        ?>
        <script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
        <script type="text/javascript">

            //Quando iniciar o formulario:
            $(document).ready(function(){            
               //Configura o calendario
                $('.input-datetime').datetimepicker({
                    dateFormat: 'dd/mm/yy', 
                    timeFormat: 'hh:mm:ss'
                });
            });
        </script>
        <?php

    echo $this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());

