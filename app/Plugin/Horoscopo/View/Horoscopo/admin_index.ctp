<?php
$this->extend('/Common/admin_index');
$this->Html
	->addCrumb('', '/admin', array('icon' => $this->Theme->getIcon('home')))
	->addCrumb('Horóscopo', '/' . $this->request->url);
?>
<table class="table">
	<thead>
		<th><?php echo $this->Paginator->sort('publish_start', 'Início')?></th>
        <th><?php echo $this->Paginator->sort('publish_end', 'Fim')?></th>
		<th><?php echo $this->Paginator->sort('created', 'Criado Em')?></th>
		<th><?php echo $this->Paginator->sort('status', 'Status')?></th>
		<th>Ações</th>
	</thead>
	<tbody>
	<?php foreach ($registros as $registro): ?>
		<tr>
			<td><?php echo date('d/m/Y H:i', strtotime($registro['Horoscopo']['publish_start'])); ?></td>
			<td><?php echo !empty($registro['Horoscopo']['publish_end']) ?  date('d/m/Y H:i', strtotime($registro['Horoscopo']['publish_end'])) : 'Não informado'; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($registro['Horoscopo']['created'])); ?></td>
			<td>
                <?php
                    echo $this->element('admin/toggle', array(
                        'id' => $registro['Horoscopo']['id'],
                        'status' => (int)$registro['Horoscopo']['status'],
                    ));
                ?>
            </td>
			<td>
                <?php 
                echo ' ' . $this->Croogo->adminRowAction('',
                    array('action' => 'edit', $registro['Horoscopo']['id']),
                    array(
                        'icon' => $this->Theme->getIcon('update'), 
                        'tooltip' => 'Editar',
                        //'onclick' => '$("#divLoading").show();'
                    )
                );
                
                echo ' ' . $this->Croogo->adminRowAction('',
                    array('action' => 'delete', $registro['Horoscopo']['id']),
                    array(
                        'icon' => $this->Theme->getIcon('delete'), 
                        'tooltip' => 'Excluir', 
                        'confirm' => 'Tem certeza que deseja EXCLUIR este registro?',
                        'onclick' => '$("#divLoading").show();'
                    )
                );	
                ?>
            </td>
		</tr>
	<?php endforeach ?>
	</tbody>
<table>