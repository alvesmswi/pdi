<?php
App::uses('AppModel', 'Model');
App::uses('String', 'Utility');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

  class Importar extends AppModel
  {
    public $useTable = false;

	function localizaEditoria($codigo, $nomeTermo = false){
		$arrayEditoria = array(
			'NOEC' => array(40 => 'economia'),// economia
			'NOES' => array(34 => 'esporte'),// esporte
			'NOGE' => array(36 => 'variedades'),// geral
			'NOIN' => array(39 => 'exterior'),// internacional / mundo
			'NOPO' => array(31 => 'politica'),// politica
			'NOVA' => array(36 => 'variedades'),// Bem-estar / variedades
		);
		
		//Se encontrou na lista
		if(isset($arrayEditoria[$codigo])){
			//se é para retornar só o termo
			if($nomeTermo){
				foreach($arrayEditoria[$codigo] as $termo){
					return $termo;
				}
			}else{
				return json_encode($arrayEditoria[$codigo]);
			}
		}else{
			return 0;
		}
	}
	
	function slug($str, $espacos = '-'){
		$str = strtolower(Inflector::slug($str));
		$str = str_replace('_', $espacos, $str);
		return $str;
	}
  
  }
?>