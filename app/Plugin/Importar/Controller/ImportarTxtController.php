<?php
App::uses('ImportarAppController', 'Importar.Controller');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

  class ImportarTxtController extends ImportarAppController {

    public $name = 'ImportarTxt';
    public $urlImport 	= '';

	public $uses = array('Importar.Importar', 'Nodes.Node', 'Taxonomy.Term', 'Taxonomy.Taxonomy', 'NoticiumDetail', 'ModelTaxonomies', 'Multiattach.Multiattach', 'Blog.Blog', 'Blog.Post', 'Users.User');
	
    public function beforeFilter() {
    	
    	//Configure::write('Cache.disable', true);
		//clearCache(null, 'models', null);
      	parent::beforeFilter();
		$this->Auth->allow('admin_index', 'admin_importar');
    }
	
	public function admin_settings()
    {
      	
    }
	
	public function admin_index()
    {
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		$xmls = $this->localizarArquivos();
		$txt = $this->localizarArquivos('importar.txt');
		$this->set('xmlsDIr',$xmlsDir);
		$this->set('xmls',$xmls);
		$this->set('txt',$txt);
    }

    public function admin_upload(){
		if($this->request->data){
			$diretorio = WWW_ROOT . 'files';
			//se o diretorio NÃO existe
			if(!is_dir($diretorio)){
				if(mkdir($diretorio, '0777')){
					$diretorio .= DS . 'importar';
					if(!is_dir($diretorio)){
						mkdir($diretorio, '0777');
					}
				}
			}
     		if(move_uploaded_file(
     			$this->request->data['Importar']['arquivo']['tmp_name'],     
     			 $diretorio . DS . $this->request->data['Importar']['arquivo']['name']
     		)){
     			$this->Flash->success('Arquivo importado com sucesso');
     			$this->redirect(array('plugin'=>'importar', 'controller'=>'importar', 'action'=>'importar'));
     		}else{
     			$this->Flash->error('Ocorreu um erro ao importar o arquivo');
     		}
     	}
	}
	
    public function admin_importar($quantidade = 15, $type='csv')
    {
    	//$this->autoRender = false;
		$arqImportacao = 'importar.txt';  
		//monta o diretorio para buscar os XMLs
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		//Tenta localizar arquivos
		$files = $this->localizarArquivos();
			
		//Se tem algo para importar
		if(!empty($this->localizarArquivos($arqImportacao))){
			$processados = 0;
			//le o arquivo txt
			$txt = new File($xmlsDir . DS . $arqImportacao);
			$txtConteudo = unserialize($txt->read());
			//conta quantos faltam
			//percorre a primeiro chave
			foreach($txtConteudo as $tipo => $nodes){
				$restante = count($nodes);	
				//se ainda tem nodes
				if(!empty($nodes)){
					foreach($nodes as $chave1 => $node){
						//Prepara as infomrações para salvar
						$this->preparaArray($node);
						//remove o indice que foi salvo
						unset($txtConteudo[$tipo][$chave1]);
						$processados++;
						//Controla a quantidade a ser processada
						if($processados == $quantidade){
							break;
						}
					}
				//Se já acabou tudo
				}else{
					$concluido = true;
					break;
				}
			}
			
			//fecha o arquivo
			$txt->close();
			$this->set('nodesRestantes',$restante);
			
			//se já terminou tudo
			if(isset($concluido)){
				//Deleta o arquivo
				$txt->delete();
			}else{
				//atualiza o arquivo
				$txt->write(serialize($txtConteudo), 'w', true);
			}
			
		}else{
			
			//se encontrou algum arquivo
			if(!empty($files)){
				//percore os arquivos
				foreach($files as $file){

					$arrayPrincipal = array();

					if($type == 'csv'){
						if($type == 'csv'){
							$csv = fopen($xmlsDir.DS.$file, 'r');
							while (($line = fgetcsv($csv, 0, "|")) !== FALSE) {
							  	$arrayPrincipal['Nodes'][] = $line;							  	
							}
							fclose($csv);
						}
					}

					if($type == 'xml'){						
						//le o arquivo XMLs
						$xml = Xml::build($xmlsDir.DS.$file); 
						//transofrma em array
						$arrayPrincipal = Xml::toArray($xml);						
					}
				}
				$this->arraySerialize($arrayPrincipal, $xmlsDir, $file);
			}
		}
		$this->set('xmlsRestantes',count($files));
		$this->set('xmlsDir',$xmlsDir);
    }

    private function arraySerialize($xmlArray, $xmlsDir, $file){
    	//pr($xmlArray);exit();
		$xmlArray = serialize($xmlArray);
		$arqImp = new File($xmlsDir.DS.'importar.txt');
		//Se criar o arquivo temporario
		if($arqImp->create($arqImp->path . DS . 'importar.txt', true, 0755)){
			//se gravar o array corretamente
			if($arqImp->write((string)$xmlArray, 'w', true)){
				//move o arquivos
				$this->moveXml($xmlsDir.DS.$file, $xmlsDir.DS.'importados');
			}
		}
    }
	
	private function moveXml($arquivoDe, $arquivoPara){
		$arq1 = new File($arquivoDe);
		if ($arq1->exists()) {
			//Cpia o arquivo
		    $arq2 = new Folder($arquivoPara, true);
		    $arq1->copy($arq2->path . DS . $arq1->name);
			//deleta o arquivo imoprtado
			$arq1->delete($arquivoDe);
		}
	}
	
	private function localizarArquivos($ext = '.*\.csv'){
		//monta o diretorio para buscar os XMLs
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		$dir = new Folder($xmlsDir);
		//procura apenas os XMLs
		$files = $dir->find($ext);		
		return $files;
	}
	
	private function corrigeTermo($idAntigo, $termo){
		//procura o node pelo ID eantigo
		$node = $this->Node->find(
			'first',
			array(
				'conditions' => array(
					'Node.id_antigo' => $idAntigo
				),
				'fields' => array(
					'Node.id',
					'Node.id_antigo'
				),
				'recursive' => -1
			) 
		);

		//se encontrou o node
		if(!empty($node)){
			//pega o id do node
			$nodeId = $node['Node']['id'];
			//trata o slug
			$slug 	= strtolower(Inflector::slug($termo));
			//procura o ermo
			$termoSQL = $this->Node->query("SELECT tm.id, tx.id FROM terms as tm INNER JOIN taxonomies as tx ON(tx.term_id=tm.id) WHERE slug = '$slug';");
			//se encontrou o termo
			if(!empty($termoSQL)){
				$termId = $termoSQL[0]['tm']['id'];
				$taxId = $termoSQL[0]['tx']['id'];			
				//atualiza o node
				$terms = '{"'.$termId.'":"'.$slug.'"}';
				$this->Node->query("UPDATE nodes SET terms = '$terms' WHERE id = $nodeId;");
				//atualiza o vinculo
				$this->Node->query("INSERT INTO model_taxonomies (model, foreign_key, taxonomy_id) VALUES ('Node', '$nodeId', '$taxId');");
			}
		}
	}

	private function preparaArray($dados, $idPai = null, $corrigeTermo=false){
		//se tem apenas um indice, é porque não coube tudo
		if(count($dados) < 5){
			return true;
		}

		//se não está vazio
		if(!empty($dados)){

			$nodeType 	= false;

			//se for Site
			if($dados[0] == 'site'){

				#Trata os dados
				$siteId 		= NULL;

				$this->loadModel('Site');

				//se é FEDERAÇÃO
				if($dados[21] == 'FED'){
					//variaveis 
					$parentId 			= 2;
					$tema 				= 'Tema1';
					$subDominioLocal 	= strtolower($dados[9]).'.apae.localhost';
					$title 				= 'FEAPAES - '.strtoupper($dados[9]);

					//verifica se já existe
					$site = $this->Site->find(
						'first',
						array(
							'conditions' => array(
								'Site.uf' => $dados[9],
								'Site.tipo' => 'FED',
							)
						)
					);
					//se encontrou
					if(!empty($site)){
						$siteId = $site['Site']['id'];
						$title 	= $site['Site']['title'];
					}

				//Se é MUNICÍPIO	
				}else{
					//variaveis 
					$tema 				= 'Tema2';
					$subDominioLocal 	= $this->_removeAcentos($dados[10]).'.'.strtolower($dados[9]).'.apae.localhost';
					$title 				= 'Apae - '.$dados[10];

					$siteMun = $this->Site->find(
						'first',
						array(
							'conditions' => array(
								'Site.uf' => $dados[9],
								'Site.dominio' => $dados[20],
								'Site.tipo' => 'MUN',
							)
						)
					);
					//se já existe
					if(!empty($siteMun)){
						$siteId = $siteMun['Site']['id'];
						$title 	= $siteMun['Site']['title'];
					}

					//procura o site parent
					$siteFed = $this->Site->find(
						'first',
						array(
							'conditions' => array(
								'Site.uf' => $dados[9],
								'Site.tipo' => 'FED',
							)
						)
					);
					//se encontrou
					if(!empty($siteFed)){
						$parentId = $siteFed['Site']['id'];
					}else{
						$this->log('UF NÃO ENCONTRADA: '.print_r($dados));
						return false;
					}
				}

				$model = 'Site';
				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 			= $siteId;				
				$arraySave[$model]['parent_id'] 	= $parentId;
				$arraySave[$model]['title'] 		= $title;
				$arraySave[$model]['description'] 	= $dados[2];
				$arraySave[$model]['tagline'] 		= $dados[3];
				$arraySave[$model]['locale'] 		= $dados[4];
				$arraySave[$model]['status'] 		= $dados[5];
				$arraySave[$model]['email'] 		= $dados[6];				
				$arraySave[$model]['uf'] 			= $dados[9];
				$arraySave[$model]['municipio'] 	= $dados[10];
				$arraySave[$model]['cnpj'] 			= $dados[11];
				$arraySave[$model]['endereco'] 		= $dados[12];
				$arraySave[$model]['bairro'] 		= $dados[13];
				$arraySave[$model]['cep'] 			= $dados[14];
				$arraySave[$model]['ddd'] 			= $dados[15];
				$arraySave[$model]['fone'] 			= $dados[16];
				$arraySave[$model]['link_facebook'] = $dados[17];
				$arraySave[$model]['link_youtube'] 	= $dados[18];
				$arraySave[$model]['link_twitter'] 	= $dados[19];
				$arraySave[$model]['dominio'] 		= $dados[20];
				$arraySave[$model]['tipo'] 			= $dados[21];
				$arraySave[$model]['id_antigo'] 	= $dados[22];
				$arraySave[$model]['theme'] 		= $tema;
				//se o site do municipio já existia
				/*if(!isset($siteMun) || (isset($siteMun) && empty($siteMun))){
					$arraySave['SiteDomain'][]['domain']= $dados[20];
					$arraySave['SiteDomain'][]['domain']= $subDominioLocal;
				}*/
				
			}

			//se for Notícia
			if($dados[0] == 'noticia'){

				//Variáveis
				$model 		= 'Node';
				$nodeType 	= 'noticia';
				$excerpt = ($dados[8] != 'NULL')?($dados[8]):('');

				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 				= NULL;
				$arraySave[$model]['title'] 			= $dados[4];
				$arraySave[$model]['excerpt'] 			= $excerpt;
				$arraySave[$model]['slug'] 				= $this->_gerarSlug($dados[4]);
				$arraySave[$model]['body'] 				= $dados[14];
				$arraySave[$model]['status'] 			= 1;
				$arraySave[$model]['promote'] 			= 1;
				$arraySave[$model]['ordem'] 			= 1;
				$arraySave[$model]['sticky'] 			= 1;
				$arraySave[$model]['user_id'] 			= 1;
				$arraySave[$model]['type'] 				= $nodeType;
				$arraySave[$model]['publish_start'] 	= ($dados[5] != 'NULL')?($dados[5]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['created'] 			= ($dados[6] != 'NULL')?($dados[6]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['updated'] 			= ($dados[7] != 'NULL')?($dados[7]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['id_antigo'] 		= $dados[1];

				/* -- noticium_details -- */
				$arraySave['NoticiumDetail']['jornalista'] 	= $dados[3];
				$arraySave['NoticiumDetail']['chapeu'] 		= $dados[2];
				
				/* -- multiattachments -- */
				if($dados[11] != 'NULL' && !is_null($dados[11]) && !empty($dados[11])){
					$arrayFile = explode('/', $dados[11]);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados[11];
				}

				//Taxonomia
				$arraySave['TaxonomyData'][0] = $this->getTermId($dados[2]);

				$arraySave['SitesNode']['site_id'] 	= $this->_pegaIdSite($dados[13]);
				
			}

			//se for Página comum
			if($dados[0] == 'page' || $dados[0] == 'secao'){

				$pagina = $this->Node->find(
					'first',
					array(
						'conditions' => array(
							'Node.id_antigo' => $dados[1],
							'Node.type' => 'page'
						),
						'recursive' => -1,
						'fields' => array('Node.id')
					)
				);
				//se já existe
				if(!empty($pagina)){
					return true;
				}

				//Variáveis
				$model 		= 'Node';
				$nodeType 	= 'page';
				$data 		= ($dados[5] != 'NULL')?($dados[5]):(date('Y-m-d H:i:s'));

				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 				= NULL;
				$arraySave[$model]['title'] 			= $dados[3];
				$arraySave[$model]['slug'] 				= $this->_gerarSlug($dados[3]).'-'.$dados[1];
				$arraySave[$model]['body'] 				= $dados[4];
				$arraySave[$model]['status'] 			= 1;
				$arraySave[$model]['promote'] 			= 1;
				$arraySave[$model]['ordem'] 			= 1;
				$arraySave[$model]['sticky'] 			= 1;
				$arraySave[$model]['user_id'] 			= 1;
				$arraySave[$model]['type'] 				= $nodeType;
				$arraySave[$model]['publish_start'] 	= $data;
				$arraySave[$model]['created'] 			= $data;
				$arraySave[$model]['updated'] 			= $data;
				$arraySave[$model]['id_antigo'] 		= $dados[1];

				$arraySave['SitesNode']['site_id'] 	= $this->_pegaIdSite($dados[2]);
				
			}

			//se for Agenda ou Evento
			if($dados[0] == 'evento' || $dados[0] == 'agenda'){

				//Variáveis
				$model 		= 'Node';
				$nodeType 	= 'evento';
				$data 		= ($dados[5] != 'NULL')?($dados[5]):(date('Y-m-d H:i:s'));

				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 				= NULL;
				$arraySave[$model]['title'] 			= $dados[3];
				$arraySave[$model]['slug'] 				= $this->_gerarSlug($dados[3]);
				$arraySave[$model]['body'] 				= $dados[4];
				$arraySave[$model]['status'] 			= 1;
				$arraySave[$model]['promote'] 			= 1;
				$arraySave[$model]['ordem'] 			= 1;
				$arraySave[$model]['sticky'] 			= 1;
				$arraySave[$model]['user_id'] 			= 1;
				$arraySave[$model]['type'] 				= $nodeType;
				$arraySave[$model]['publish_start'] 	= ($dados[6] != 'NULL' && $dados[6] != '0000-00-00')?($dados[6]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['publish_end'] 		= ($dados[7] != 'NULL' && $dados[7] != '0000-00-00')?($dados[7]):('');
				$arraySave[$model]['created'] 			= ($dados[6] != 'NULL' && $dados[6] != '0000-00-00')?($dados[6]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['updated'] 			= ($dados[6] != 'NULL' && $dados[6] != '0000-00-00')?($dados[6]):(date('Y-m-d H:i:s'));
				$arraySave[$model]['id_antigo'] 		= $dados[1];

				$arraySave['EventoDetail']['data'] 		= ($dados[6] != 'NULL' && $dados[6] != '0000-00-00')?($dados[6]):(date('Y-m-d H:i:s'));

				$arraySave['SitesNode']['site_id'] 	= $this->_pegaIdSite($dados[2]);
				
			}

			//se for Página comum
			if($dados[0] == 'arquivo_top'){
				$idArquivo = $dados[1];
				//Taxonomia
				$termId = $this->getTermId(trim($dados[2]), 9, true);
				//se conseguiu salvar o termo
				if($termId){
					$sql = "UPDATE arquivo_top SET termo_id = $termId WHERE art_id = $idArquivo;";
					$this->Importar->query($sql);
				}
				return true;
				
			}

			//se for Arquivo
			if($dados[0] == 'arquivo'){

				//Variáveis
				$model 		= 'Node';
				$nodeType 	= 'arquivo';
				$data 		= ($dados[4] != 'NULL' && $dados[4] != '0000-00-00')?($dados[4]):(date('Y-m-d H:i:s'));

				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 				= NULL;
				$arraySave[$model]['title'] 			= $dados[2];
				$arraySave[$model]['slug'] 				= $this->_gerarSlug($dados[2]).'-'.$dados[1];
				$arraySave[$model]['body'] 				= $dados[3];
				$arraySave[$model]['status'] 			= 1;
				$arraySave[$model]['promote'] 			= 1;
				$arraySave[$model]['ordem'] 			= 1;
				$arraySave[$model]['sticky'] 			= 1;
				$arraySave[$model]['user_id'] 			= 1;
				$arraySave[$model]['type'] 				= $nodeType;
				$arraySave[$model]['publish_start'] 	= $data;
				$arraySave[$model]['created'] 			= $data;
				$arraySave[$model]['updated'] 			= $data;
				$arraySave[$model]['id_antigo'] 		= $dados[1];
				$arraySave[$model]['arquivo_id'] 		= $dados[1];

				$arraySave['SitesNode']['site_id'] 	= $this->_pegaIdSite($dados[5]);
				
			}

			//se for Arquivo
			if($dados[0] == 'menu1' || $dados[0] == 'menu2' || $dados[0] == 'menu3'){

				//Variáveis
				$model 		= 'Link';
				$menuId 	= 5;
				$siteId 	= $this->_pegaIdSite($dados[4]);
				$parentId = NULL;
				if(!empty($dados[5])){
					$parentId 	= $this->_getParentId($dados[5], $dados[4]);
				}

				//Prepara o array para salvar
				$arraySave[$model] = array();
				$arraySave[$model]['id'] 				= NULL;
				$arraySave[$model]['menu_id'] 			= $menuId;
				$arraySave[$model]['parent_id'] 		= $parentId;
				$arraySave[$model]['title'] 			= $dados[2];
				$arraySave[$model]['link'] 				= $this->_gerarLink($dados[3], $siteId);
				$arraySave[$model]['class'] 			= $this->_gerarLink($dados[3], $siteId);
				$arraySave[$model]['status'] 			= 1;
				$arraySave[$model]['id_antigo'] 		= $dados[1];
				$arraySave[$model]['site_id'] 			= $dados[4];

				$arraySave['Role']['Role']				= '';
				$arraySave['Site']['Site'][] 			= $siteId;

			}
		}
		//Configure::write('debug',1);pr($dados);pr($arraySave);exit();
		//se existe id antigo
		if(isset($arraySave[$model]['id_antigo']) && !empty($arraySave[$model]['id_antigo'])){
			//verifica se o node já foi importado
			if($this->_verificaExisteNode($arraySave[$model]['id_antigo'])){
				return true;
			}
		}
	
		//Se salvar corretamente
		if ($this->salvar($arraySave, $model, $nodeType)) {
			return true;
		} else {
			return false;
		}
	}
		
	private function salvar($data, $model, $nodeType=false) {

		//se for node
		if($model == 'Node'){
			//Se salvar corretamente o node
			if ($this->Node->saveNode($data, $nodeType)) {
				Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $data));
				//Pega o ID que foi salvo
				$id_salvo = $this->Node->getLastInsertID();
				//Se tem sites para salvar
				if (isset($data['SitesNode']) && !empty($data['SitesNode'])) {
					$this->LoadModel('SitesNode');
					$arraySite = array();
					$arraySite['id'] = null;
					$arraySite['node_id'] = $id_salvo;
					$arraySite['site_id'] = $data['SitesNode']['site_id'];
					$this->SitesNode->save($arraySite);
				}
				//Se salvar corretamente os anexos
				if (isset($data['Multiattachments']) && !empty($data['Multiattachments'])) {
					foreach ($data['Multiattachments'] as $key => $multiattachments) {
						$copyImagem = true;
						if($key==='pdf'){
							$copyImagem = false;
						}
						$this->saveImageNode($multiattachments, $id_salvo, $copyImagem);
					}
				}
			}else{
				$this->log($data, 'importacao-erro');
			}
		}else{
			$this->loadModel($model);
			//se for link
			if($model == 'Link'){
				$data['Link']['visibility_roles'] = $this->Link->encodeData($data['Role']['Role']);
				$this->Link->create();
				$this->Link->setTreeScope($data['Link']['menu_id']);
				if (!$this->Link->save($data)) {
					return false;
				}
			}else{
				if ($this->$model->saveAll($data)) {
					//Pega o ID que foi salvo
					$id_salvo = $this->$model->getLastInsertID();
					return true;
				} else {
					return false;
				}
			}			
		}
	}
	
	//testa a se a imagem existe
	function url_exists($url) {

	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    return ($code == 200); // verifica se recebe "status OK"
	}
	
	/**
	 * saveImageNode
	 * save image for node
	 */
	private function saveImageNode($dataImage, $node, $copyImagem = true) 
	{
		$url_image 	= $this->urlImport.str_replace(' ', '%20', $dataImage['filepath']);
		///Se a imagem exite
		if($this->url_exists($url_image)){
			$size 		= getimagesize($url_image);
			//se encontrou os dados da imagem
			if(!empty($size)){
				$name 			= $node.strtotime('Y-m-d H:i:s');
				$mime_type 		= $size['mime'];

				$nameExp = explode('.', $name);
				if(count($nameExp) == 1){
					$extArray = explode('/', $mime_type);
					$name = $name.'.'.$extArray[1];
				}
				$save_url 	= APP.'files'.DS.$node;
				$file_path 	= '/files/'.$node.'/'.$name;
		
				$comment 	= (isset($dataImage['comment'])) ? $dataImage['comment'] : null;

				$folder 	= new Folder();

			//pr($size);exit();
				if($copyImagem){
					if ($folder->create($save_url)) {
						copy($url_image,$save_url.DS.$name);
					}
				}else{
					$mime_type = 'application/pdf';
				}			
				
				if(empty($mime_type)){
					return false;
				}
				$this->Multiattach->create();
				$this->Multiattach->set(array('node_id' => $node, 'real_filename' => $file_path, 'filename' => $name, 'comment' => $comment, 'mime' => $mime_type));
				$this->Multiattach->save();
			}
			
		}
		
	}

	private function getTermId($term, $vocabularyId = '1', $returnTermo = false) 
	{
		$slugTerm 	= strtolower(Inflector::slug($term));
		$term_find 	= $this->Term->findBySlug($slugTerm, array('id', 'slug'));

		Configure::write('debug',1);
		
		//pr($term);pr($slugTerm);pr($term_find);exit();

		//se encontrou o termo
		if (!empty($term_find)) {
			//se não tem vocabulario
			if(empty($term_find['Vocabulary'])){
				//apaga para refazer
				$this->Term->query('DELETE FROM terms WHERE id = '.$term_find['Term']['id'].';');
				return $this->getTermId($term, $vocabularyId, $returnTermo);
			}
			$taxonomy_id = null;
			foreach ($term_find['Vocabulary'] as $key => $taxonomy) {
				//pr($taxonomy);exit();
				if(!$returnTermo){
					$taxonomy_id = $taxonomy['Taxonomy']['id'];
				}else{
					$taxonomy_id = $taxonomy['Taxonomy']['term_id'];
				}
			}

			return $taxonomy_id;
		} else {
			$arrayTerm['Taxonomy'] = array(
				'id' 			=> null,
				'parent_id' 	=> null
			);
			$arrayTerm['Term'] = array(
				'id' 			=> null,
				'title' 		=> $term,
				'slug' 			=> $slugTerm,
				'description' 	=> null
			);
			if ($this->Term->add($arrayTerm, $vocabularyId)) {
				return $this->getTermId($term, $vocabularyId, $returnTermo);
			} else {
				$this->log('não salvou o novo termo: '.$term, 'erroSalvarTermo');
				return false;
			}
		}
	}

	function _gerarSlug($string){
		return str_replace('_', '-', strtolower(Inflector::slug(trim($string))));
	}

	function _mask($mask,$str){
	    $str = str_replace(" ","",$str);
	    for($i=0;$i<strlen($str);$i++){
	        $mask[strpos($mask,"#")] = $str[$i];
	    }
	    return $mask;
	}

	function _onlyNumbers($str) {
	    return preg_replace("/[^0-9]/", "", $str);
	}

	function _removeAcentos($string){
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   		return strtolower($string);
	}

	function _pegaIdSite($idAntigo){
		$this->LoadModel('Site');
		$site = $this->Site->find(
			'first',
			array(
				'conditions' => array(
					'Site.id_antigo' => $idAntigo
				),
				'recursive' => -1,
				'fields' => array('Site.id')
			)
		);
		if(empty($site)){
			return 1;
		}
		return $site['Site']['id'];
	}
	
	function _gerarLink($string='', $siteId){
		if($string == '' || $string == 'NULL'){
			return '#';
		}
		if($string == '/apaes.phtml'){
			return 'plugin:sites/controller:sites/action:index';
		}
		if(strstr($string, 'http')){
			return $string;
		}

		//Trata o link
		$arrayLink = explode('=', $string);
		$type = 'page';
		if(strstr($arrayLink[0], 'arquivo.')){
			$type = 'arquivo';
		}

		$idAntigo = $this->_onlyNumbers($arrayLink[1]);

		if(empty($idAntigo)){
			return '#';
		}
		$query = "SELECT nodes.id, nodes.slug FROM nodes INNER JOIN sites_nodes ON(sites_nodes.node_id=node_id) WHERE sites_nodes.site_id=$siteId AND id_antigo=$idAntigo GROUP BY nodes.id ORDER BY nodes.id DESC;";
		$result = $this->Importar->query($query);
		//se encontoru o node
		if(!empty($result)){
			$slug = $result[0]['nodes']['slug'];
			$link = "plugin:nodes/controller:nodes/action:view/type:$type/slug:$slug";
			return $link;
		}
		return '#';
	}

	function _getParentId($idAntigo, $siteId){
		$query = "SELECT id FROM links WHERE site_id=$siteId AND id_antigo=$idAntigo;";
		$result = $this->Importar->query($query);
		//se encontoru o node
		if(!empty($result)){
			return $result[0]['links']['id'];
		}
		return NULL;
	}

	function _verificaExisteNode($idAntigo){
		$this->loadModel('Node');
		$node = $this->Node->query("SELECT id FROM nodes WHERE id_antigo = '$idAntigo';");
		if(!empty($node)){
			return true;
		}else{
			return false;
		}
	}
  }
?>