<?php

App::uses('HttpSocket', 'Network/Http');

/**
 * Migração Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://mswi.com.br
 */
class MigracaoController extends AppController 
{
    public $uses = array(
        'Blog.Blog',
        'Blog.Post',

        'Taxonomy.Type',
        'Taxonomy.Vocabulary',
        'Taxonomy.Taxonomy',
        'Taxonomy.Term',

        'Nodes.Node', 
        'Multiattach.Multiattach'
    );

    public $components = array('Mswi');

    /**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        
        $this->Auth->allow();
        $this->autoRender = false;

        Configure::write('debug', 1);
    }

    /**
     * migrar
     * blog_nodes to nodes (type: post)
     */
    public function migrar($limit = 1)
    {
        // Primeiro tem que criar o tipo de conteúdo : Post
        // Verificar se já existe o tipo de Conteúdo Post, se não cria o novo Tipo de Conteúdo
        $tipo = $this->Type->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'alias' => 'post'
            )
        ));

        // Se não existe o Tipo de Conteúdo: post
        if (empty($tipo)) {
            $arrayTipo = array();
            $arrayTipo['id']                    = null;
            $arrayTipo['title']                 = 'Post / Artigo';
            $arrayTipo['alias']                 = 'post';
            $arrayTipo['format_show_author']    = false;
            $arrayTipo['format_show_date']      = false;
            $arrayTipo['format_use_wysiwyg']    = true;
            $arrayTipo['comment_status']        = false;
            $arrayTipo['comment_approve']       = false;
            $arrayTipo['params']                = "routes=true\ndetail=true\nblog=true";

            // Criar os Details do Tipo de Conteúdo

            // Salva o novo Tipo de Conteúdo
            if ($this->Type->saveAssociated($arrayTipo)) {
                $tipo_id = $this->Type->id;
            }
        } else {
            $tipo_id = $tipo['Type']['id'];
        }

        pr('Tipo de Conteúdo ID: '.$tipo_id.' (post)');

        // Migrar os Anuncios para os Nodes
        $post = $this->Post->find('first', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'contain' => array(
                'Blog',
                'Images'
            ),
            'order' => array(
                'Post.id' => 'ASC'
            ),
            'limit' => $limit
        ));
//pr($post);exit();
        // Posts restantes
        $posts_restantes = $this->Post->find('count', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'recursive' => -1
        ));
        pr('Posts restantes: '.$posts_restantes);

        if (!empty($post)) {

            $node = array();

            $node['Node']['id']                     = null;
            $node['Node']['type']                   = 'post';
            $node['Node']['title']                  = $post['Post']['title'];
            $node['Node']['slug']                   = $post['Post']['slug'];
            $node['Node']['excerpt']                = $post['Post']['excerpt'];
            $node['Node']['body']                   = $post['Post']['body'];
            $node['Node']['status']                 = $post['Post']['status'];
            $node['Node']['publish_start']          = $post['Post']['publish_start'];
            $node['Node']['publish_end']            = $post['Post']['publish_end'];
            $node['Node']['user_id']                = $post['Post']['user_id'];
            $node['Node']['autocomplete_user_id']   = $post['Post']['jornalista'];
            $node['Node']['created']                = $post['Post']['created'];
            $node['Node']['exclusivo']              = false;
            $node['Node']['blog_id']                = $post['Post']['blog_id'];
            $node['Node']['id_antigo']              = $post['Post']['id'];
            
            $node['PostDetail']['keywords']     = $post['Post']['keywords'];
            $node['PostDetail']['jornalista']   = $post['Post']['jornalista'];

            if (isset($post['Images']) && !empty($post['Images'])) {
                foreach ($post['Images'] as $key => $image) {
                    $node['Multiattachments'][$key]['filename'] = $image['filename'];
                    $node['Multiattachments'][$key]['filepath'] = Router::url($image['url'], true);
                    $node['Multiattachments'][$key]['comment']  = $image['legenda'];
                    $node['Multiattachments'][$key]['meta']     = $image['credito'];
                }
            }


            ##VERIFICAR SE DEVE SER SALVO NOS NODES ARQUIVADOS OU NÃO
           // pr($node);exit();
            $node_salvo = $this->salvar_node($node, 'post');

            pr('Node ID: '.$node_salvo.' ('.$node['Node']['slug'].')');

            // Chama novamente a função
            //pr('<script>setTimeout(function() { location.reload(); }, 2000);</script>');
        } else {
            pr('A migração terminou!');
        }
    }

    /**
     * migrar
     * blog_nodes to nodes (type: coluna)
     */
    public function migrar_colunas()
    {
        // Primeiro tem que criar o tipo de conteúdo : Post
        // Verificar se já existe o tipo de Conteúdo Post, se não cria o novo Tipo de Conteúdo
        $tipo = $this->Type->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'alias' => 'coluna'
            )
        ));

        // Se não existe o Tipo de Conteúdo: coluna
        if (empty($tipo)) {
            $arrayTipo = array();
            $arrayTipo['id']                    = null;
            $arrayTipo['title']                 = 'Coluna';
            $arrayTipo['alias']                 = 'coluna';
            $arrayTipo['format_show_author']    = false;
            $arrayTipo['format_show_date']      = false;
            $arrayTipo['format_use_wysiwyg']    = true;
            $arrayTipo['comment_status']        = false;
            $arrayTipo['comment_approve']       = false;
            $arrayTipo['params']                = "routes=true\ndetail=true";

            // Criar os Details do Tipo de Conteúdo

            // Salva o novo Tipo de Conteúdo
            if ($this->Type->saveAssociated($arrayTipo)) {
                $tipo_id = $this->Type->id;
            }
        } else {
            $tipo_id = $tipo['Type']['id'];
        }

        pr('Tipo de Conteúdo ID: '.$tipo_id.' (coluna)');

        // Criar o Vocabulario e Vincular para o Tipo de Conteúdo : coluna
        $vocabulario = $this->Vocabulary->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'alias' => 'colunas'
            )
        ));

        // Se não existe o Vocabulario: colunas
        if (empty($vocabulario)) {
            $arrayVocabulario = array();
            $arrayVocabulario['id']                 = null;
            $arrayVocabulario['title']              = 'Colunas';
            $arrayVocabulario['alias']              = 'colunas';
            $arrayVocabulario['Type']['Type'][0]    = $tipo_id;

            // Salva o novo Vocabulario
            if ($this->Vocabulary->save($arrayVocabulario)) {
                $vocabulario_id = $this->Vocabulary->id;
            }
        } else {
            $vocabulario_id = $vocabulario['Vocabulary']['id'];
        }

        pr('Vocabulario ID: '.$vocabulario_id.' (colunas)');

        // Depois Criar os Termos e Vincular para o Vocabulario : colunas
        $blogs = $this->Blog->find('all', array(
            'recursive' => -1
        ));

        if (!empty($blogs)) {
            foreach ($blogs as $key => $blog) {
                // Verificar se já existe o Blog como Termo
                $taxonomia = $this->Taxonomy->find('first', array(
                    'contain' => array('Term'),
                    'conditions' => array(
                        'Term.slug' => $blog['Blog']['slug'],
                        'Taxonomy.vocabulary_id' => $vocabulario_id,
                    )
                ));

                $taxonomia_outro_vocabulario = $this->Taxonomy->find('first', array(
                    'contain' => array('Term'),
                    'conditions' => array(
                        'Term.slug' => $blog['Blog']['slug']
                    )
                ));

                // Se não existe o Termo para o Vocabulário
                if (empty($taxonomia) && empty($taxonomia_outro_vocabulario)) {
                    $arrayTerm = array();
                    $arrayTerm['id']            = null;
                    $arrayTerm['title']         = $blog['Blog']['title'];
                    $arrayTerm['slug']          = $blog['Blog']['slug'];
                    $arrayTerm['description']   = $blog['Blog']['descricao'];

                    // Salva o novo Termo
                    if ($this->Term->save($arrayTerm)) {
                        $termo_id = $this->Term->id;

                        $query_taxonomy = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termo_id,$vocabulario_id,0,0);";
                        $this->Taxonomy->query($query_taxonomy);
				        $this->Taxonomy->recover();
                    }
                } else if (empty($taxonomia) && !empty($taxonomia_outro_vocabulario)) {
                    $termo_id = $taxonomia_outro_vocabulario['Term']['id'];

                    $query_taxonomy = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termo_id,$vocabulario_id,0,0);";
                    $this->Taxonomy->query($query_taxonomy);
                    $this->Taxonomy->recover();
                } else {
                    $termo_id = $taxonomia['Term']['id'];
                }

                pr('Termo ID: '.$termo_id.' ('.$blog['Blog']['slug'].')');
            }
        }

        // Migrar os Anuncios para os Nodes
        $post = $this->Post->find('first', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'contain' => array(
                'Blog',
                'Images'
            ),
            'order' => array(
                'Post.id' => 'ASC'
            ),
            'limit' => 1
        ));

        // Posts restantes
        $posts_restantes = $this->Post->find('count', array(
            'conditions' => array(
                'Post.migrado' => 0
            ),
            'recursive' => '-1'
        ));
        pr('Posts restantes: '.$posts_restantes);

        if (!empty($post)) {
            // Buscar o Taxonomy ID do Termo do Blog
            $taxonomia_node = $this->Taxonomy->find('first', array(
                'contain' => array('Term'),
                'conditions' => array(
                    'Term.slug' => $post['Blog']['slug'],
                    'Taxonomy.vocabulary_id' => $vocabulario_id
                ),
                'fields' => array(
                    'Taxonomy.id'
                )
            ));

            $node = array();

            $node['Node']['id']                     = null;
            $node['Node']['type']                   = 'coluna';
            $node['Node']['title']                  = $post['Post']['title'];
            $node['Node']['slug']                   = $post['Post']['slug'];
            $node['Node']['excerpt']                = $post['Post']['excerpt'];
            $node['Node']['body']                   = $post['Post']['body'];
            $node['Node']['status']                 = $post['Post']['status'];
            $node['Node']['publish_start']          = $post['Post']['publish_start'];
            $node['Node']['publish_end']            = $post['Post']['publish_end'];
            $node['Node']['user_id']                = $post['Post']['user_id'];
            $node['Node']['autocomplete_user_id']   = $post['Post']['jornalista'];
            $node['Node']['created']                = $post['Post']['created'];
            $node['Node']['exclusivo']              = false;
            $node['Node']['id_antigo']              = $post['Post']['id'];
            
            $node['PostDetail']['keywords']     = $post['Post']['keywords'];
            $node['PostDetail']['jornalista']   = $post['Post']['jornalista'];

            if (isset($post['Images']) && !empty($post['Images'])) {
                foreach ($post['Images'] as $key => $image) {
                    $node['Multiattachments'][$key]['filename'] = $image['filename'];
                    $node['Multiattachments'][$key]['filepath'] = Router::url($image['url'], true);
                    $node['Multiattachments'][$key]['comment']  = $image['legenda'];
                    $node['Multiattachments'][$key]['meta']     = $image['credito'];
                }
            }

            $node['TaxonomyData'][1] = $taxonomia_node['Taxonomy']['id'];

            $node_salvo = $this->salvar_node($node, 'coluna');

            pr('Node ID: '.$node_salvo.' ('.$node['Node']['slug'].')');

            // Chama novamente a função
            pr('<script>setTimeout(function() { location.reload(); }, 2000);</script>');
        } else {
            pr('A migração terminou!');
        }
    }

    /**
     * salvar_node
     * salva o post como node
     */
    private function salvar_node($node, $tipo = 'post')
    {
        //pr($node);exit();
        if ($this->Node->saveNode($node, $tipo)) {
            Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $node));

            // Pega o ID que foi salvo
            $id_salvo = $this->Node->getLastInsertID();

            // Se salvar corretamente os anexos
            if (isset($node['Multiattachments']) && !empty($node['Multiattachments'])) {
                //pr($node['Multiattachments']);exit();
                foreach ($node['Multiattachments'] as $key => $multiattachments) {
                    $this->saveImageNode($multiattachments, $id_salvo);
                }
            }

            // Atualiza o Post para não migrar novamente
            /*$this->Post->updateAll(
                array('Post.migrado' => 1),
                array('Post.id' => $node['Node']['id_antigo'])
            );*/

            return $id_salvo;
        }
    }

    /**
	 * saveImageNode
	 * save image for node
	 */
	private function saveImageNode($dataImage, $node, $copyImagem = true) 
	{
		$url_image 	= $this->urlImport.str_replace(' ', '%20', $dataImage['filepath']);
		///Se a imagem exite
		if($this->url_exists($url_image)){
			$size 		= getimagesize($this->urlImport.str_replace(' ', '%20', $dataImage['filepath']));

			$name 			= strtolower(str_replace(' ', '_', basename($dataImage['filename'])));
			$nameArray 		= explode('.', $name);
			$nameArray[0] 	= Inflector::slug($nameArray[0]);
			$nameArray 		= implode('.', $nameArray);
			$name 			= $nameArray;

			$save_url 	= APP.'files'.DS.$node;
			$file_path 	= '/files/'.$node.'/'.$name;
			$mime_type 	= (!empty($size['mime'])) ? $size['mime'] : '';

			$comment 	= (isset($dataImage['comment'])) ? $dataImage['comment'] : null;

			$folder 	= new Folder();
		
			if($copyImagem){
				if ($folder->create($save_url)) {
					copy($url_image,$save_url.DS.$name);
				}
			}else{
				$mime_type = 'application/pdf';
			}			

			$this->Multiattach->create();
			$this->Multiattach->set(array('node_id' => $node, 'real_filename' => $file_path, 'filename' => $name, 'comment' => $comment, 'mime' => $mime_type));
			$this->Multiattach->save();
		}
		
	}

    //testa a se a imagem existe
	function url_exists($url) {
return true;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return ($code == 200); // verifica se recebe "status OK"
	}
}