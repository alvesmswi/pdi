<?php
App::uses('ImportarAppController', 'Importar.Controller');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class ExportarController extends ImportarAppController 
{
	public $name = 'Exportar';
	public $uses = array('Importar.Importar', 'Nodes.Node', 'Taxonomy.Term', 'Taxonomy.Taxonomy', 'NoticiumDetail', 'ModelTaxonomies', 'Multiattach.Multiattach', 'Blog.Blog', 'Blog.Post', 'Users.User');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
			'admin_gerar_noticias_xml',
			'admin_gerar_posts_xml', 
			'admin_gerar_impressos_xml',
			'admin_gerar_empresas_xml',
			'admin_gerar_sql_ajuste',
			'admin_gerar_usuarios_xml'
		);
	}

	public function admin_update_views($id=0){

		$this->Importar->useDbConfig = 'boca';

		$registros = $this->Importar->query("
			SELECT * FROM wp_postmeta
			WHERE meta_key = 'magazin_post_views_count'
			AND post_id > $id
			ORDER BY post_id
			LIMIT 500;
		");		

		$this->Importar->useDbConfig = 'default';
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			foreach($registros as $registro){

				$post_id = $registro['wp_postmeta']['post_id'];
				$lida = $registro['wp_postmeta']['meta_value'];

				if(!empty($post_id)){
					$this->Importar->query("
						UPDATE nodes SET lida = $lida WHERE id_antigo = $post_id;			
					");
				}
			}
		}
		$this->set('ultimo_id',$registro['wp_postmeta']['post_id']);
		$this->set('arquivo_atualizado', 'ID: ' . $registro['wp_postmeta']['post_id']);

	}

	public function admin_gerar_noticias_xml($id=0){

		App::uses('Xml', 'Utility');

		//Alterar o banco de dados
		$this->Importar->useDbConfig = 'bp';

		//Variáveis
		$xmlsDir 			= WWW_ROOT . 'files' . DS . 'importar';
		$xmlNome 			= 'bp_noticias_';		

		//busca as noticias
		$registros = $this->Importar->query("
			SELECT 
				Noticia.noticia_id,
				Noticia.editoria_id,
				Noticia.tit,
				Noticia.gravata,
				Noticia.chapeu,
				Noticia.txt,
				Noticia.autor,
				Noticia.link,
				Noticia.datainicio,
				Noticia.dataalt,
				Noticia.datacad,
				Noticia.views,
				Noticia.img1,
				Noticia.slug,
				Noticia.metakeys,
				Noticia.titcapa,
				Editoria.tit 
			FROM noticia AS Noticia 
			LEFT JOIN editoria AS Editoria ON(Editoria.editoria_id = Noticia.editoria_id)
			WHERE 
				Noticia.excluido = 'N' AND noticia_id > $id 
			ORDER BY Noticia.noticia_id ASC 
			LIMIT 500
		");

		//se NÃO encontrou noticia
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			$xmlConteudo 	= '<?xml version="1.0" encoding="UTF-8"?><Noticias>';
	
			foreach($registros as $registro){
	
				//variaveis da noticia				
				$image 			= $registro['Noticia']['img1'] ? 'http://bemparana.com.br/upload/image/noticia/'.trim($registro['Noticia']['img1']) : '';
				$secao 			= $registro['Editoria']['tit'] ? trim($registro['Editoria']['tit']) : 'Bem Paraná';
				$jornalista		= $registro['Noticia']['autor'] ? trim($registro['Noticia']['autor']) : 'Bem Paraná';
				$titulo			= $registro['Noticia']['tit'] ? trim($registro['Noticia']['tit']) : 'Não Informado - '.$registro['Noticia']['noticia_id'];
				$slug			= $registro['Noticia']['slug'] ? trim($registro['Noticia']['slug']) : str_replace('_', '-', strtolower(Inflector::slug($titulo.'-'.$registro['Noticia']['noticia_id'])));
				$datainicio		= $registro['Noticia']['datainicio'] ? trim($registro['Noticia']['datainicio']) : '2005-01-01 00:00:00';
				$datacad		= $registro['Noticia']['datacad'] ? trim($registro['Noticia']['datacad']) : '2005-01-01 00:00:00';
				$dataalt		= $registro['Noticia']['dataalt'] ? trim($registro['Noticia']['dataalt']) : '2005-01-01 00:00:00';
				$tituloCapa 	= $registro['Noticia']['titcapa'] ? trim($registro['Noticia']['titcapa']) : '';

				//Monta o XML
				$xmlConteudo .= '<Noticia Código="'.$registro['Noticia']['noticia_id'].'">';
				$xmlConteudo .= '<Secao><![CDATA['.$secao.']]></Secao>';
				$xmlConteudo .= '<Jornalista><![CDATA['.$jornalista.']]></Jornalista>';
				$xmlConteudo .= '<Titulo><![CDATA['.$titulo.']]></Titulo>';				
				$xmlConteudo .= '<Texto><![CDATA['.$this->trataTexto($registro['Noticia']['txt']).']]></Texto>';
				$xmlConteudo .= '<Chapeu><![CDATA['.$this->trataTexto($registro['Noticia']['chapeu']).']]></Chapeu>';
				$xmlConteudo .= '<Gravata><![CDATA['.$this->trataTexto($registro['Noticia']['gravata']).']]></Gravata>';
				$xmlConteudo .= '<Keywords><![CDATA['.$this->trataTexto($registro['Noticia']['metakeys']).']]></Keywords>';
				$xmlConteudo .= '<Slug><![CDATA['.$slug.']]></Slug>';
				$xmlConteudo .= '<Lidas>'.$registro['Noticia']['views'].'</Lidas>';
				$xmlConteudo .= '<Imagem><![CDATA['.$image.']]></Imagem>';		
				$xmlConteudo .= '<Publicacao>'.$datainicio.'</Publicacao>';
				$xmlConteudo .= '<Data>'.$datacad.'</Data>';
				$xmlConteudo .= '<Criado>'.$datacad.'</Criado>';
				$xmlConteudo .= '<Atualizado>'.$dataalt.'</Atualizado>';
				$xmlConteudo .= '<TituloCapa><![CDATA['.$tituloCapa.']]></TituloCapa>';								
				$xmlConteudo .= '<Ativo>true</Ativo>';
				$xmlConteudo .= '<Relacionadas>true</Relacionadas>';
				$xmlConteudo .= '<PermitirComentarios>true</PermitirComentarios>';								
				$xmlConteudo .= '</Noticia>';
			}
			$xmlConteudo .= '</Noticias>';
	
			//criar o arquivo
			$file = new File($xmlsDir.DS.$xmlNome.$registro['Noticia']['noticia_id'].'.xml', true, 0775);
			$file->write($xmlConteudo);
			$file->close();
	
			//envia as variaveis para a view
			$this->set('ultimo_id',$registro['Noticia']['noticia_id']);
			$this->set('arquivo_gerado',$xmlsDir.DS.$xmlNome.$registro['Noticia']['noticia_id'].'.xml');
		}
	}

	public function admin_gerar_empresas_xml($id=0){

		App::uses('Xml', 'Utility');

		//Alterar o banco de dados
		$this->Importar->useDbConfig = 'bp';

		//Variáveis
		$xmlsDir 			= WWW_ROOT . 'files' . DS . 'importar';
		$xmlNome 			= 'bp_empresas_';		

		//busca as noticias
		$registros = $this->Importar->query("
			SELECT 
				Empresa.empresa_id,
				Empresa.empresacat_id,
				Empresa.tit,
				Empresa.end,
				Empresa.num,
				Empresa.compl,
				Empresa.bairro,
				Empresa.cidade,
				Empresa.cep,
				Empresa.fone1,
				Empresa.fone2,
				Empresa.email,
				Empresa.site,
				Empresa.txt,
				Empresa.keywords,
				Empresa.cnpj,
				Empresa.24horas,
				Empresa.estacionamento,
				Empresa.horario,
				Empresa.fumantes,
				Empresa.reservas,
				Empresa.criancas,
				Empresa.necessidadesespeciais,
				Empresa.cheque,
				Empresa.slug,
				Empresa.datacad,
				Empresa.dataalt,
				Editoria.tit 
			FROM empresa AS Empresa 
			LEFT JOIN empresacat AS Editoria ON(Editoria.empresacat_id = Empresa.empresacat_id)
			WHERE 
				Empresa.status = 3 AND Empresa.empresa_id > $id 
			ORDER BY Empresa.empresa_id ASC 
			LIMIT 500
		");

		//se NÃO encontrou noticia
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			$xmlConteudo 	= '<?xml version="1.0" encoding="UTF-8"?><Empresas>';
	
			foreach($registros as $registro){
	
				//variaveis da noticia				
				$image 			= '';
				$secao 			= $registro['Editoria']['tit'] ? trim($registro['Editoria']['tit']) : 'Outros';
				$titulo			= $registro['Empresa']['tit'] ? trim($registro['Empresa']['tit']) : 'Não Informado - '.$registro['Empresa']['empresa_id'];
				$slug			= $registro['Empresa']['slug'] ? trim($registro['Empresa']['slug']) : str_replace('_', '-', strtolower(Inflector::slug($titulo.'-'.$registro['Empresa']['empresa_id'])));
				$datacad		= $registro['Empresa']['datacad'] ? trim($registro['Empresa']['datacad']) : '2005-01-01 00:00:00';
				$dataalt		= $registro['Empresa']['dataalt'] ? trim($registro['Empresa']['dataalt']) : '2005-01-01 00:00:00';
				$endereco 		= $registro['Empresa']['end'] ? trim($registro['Empresa']['end']) : '';
				$num 		= $registro['Empresa']['num'] ? trim($registro['Empresa']['num']) : '';
				$compl 		= $registro['Empresa']['compl'] ? trim($registro['Empresa']['compl']) : '';
				$bairro 		= $registro['Empresa']['bairro'] ? trim($registro['Empresa']['bairro']) : '';
				$cidade 		= $registro['Empresa']['cidade'] ? trim($registro['Empresa']['cidade']) : '';
				$cep 		= $registro['Empresa']['cep'] ? trim($registro['Empresa']['cep']) : '';
				$fone1 		= $registro['Empresa']['fone1'] ? trim($registro['Empresa']['fone1']) : '';
				$fone2 		= $registro['Empresa']['fone2'] ? trim($registro['Empresa']['fone2']) : '';
				$email 		= $registro['Empresa']['email'] ? trim($registro['Empresa']['email']) : '';
				$site 		= $registro['Empresa']['site'] ? trim($registro['Empresa']['site']) : '';
				$txt 		= trim($registro['Empresa']['txt'].'<br/>'.$registro['Empresa']['horario']);
				$keywords 		= $registro['Empresa']['keywords'] ? trim($registro['Empresa']['keywords']) : '';
				$status 		= '1';
				$datacad 		= $registro['Empresa']['datacad'] ? trim($registro['Empresa']['datacad']) : '';
				$dataalt 		= $registro['Empresa']['dataalt'] ? trim($registro['Empresa']['dataalt']) : '';
				$cnpj 		= $registro['Empresa']['cnpj'] ? trim($registro['Empresa']['cnpj']) : '';
				$horas24 		= $registro['Empresa']['24horas'] ? trim($registro['Empresa']['24horas']) : '';
				$estacionamento 		= $registro['Empresa']['estacionamento'] ? trim($registro['Empresa']['estacionamento']) : '';
				$fumantes 		= $registro['Empresa']['fumantes'] ? trim($registro['Empresa']['fumantes']) : '';
				$reservas 		= $registro['Empresa']['reservas'] ? trim($registro['Empresa']['reservas']) : '';
				$criancas 		= $registro['Empresa']['criancas'] ? trim($registro['Empresa']['criancas']) : '';
				$necessidadesespeciais 		= $registro['Empresa']['necessidadesespeciais'] ? trim($registro['Empresa']['necessidadesespeciais']) : '';
				$cheque 		= $registro['Empresa']['cheque'] ? trim($registro['Empresa']['cheque']) : '';
				$credito 		= 1;
				$debito 		= 1;

				//Monta o XML
				$xmlConteudo .= '<Empresa Codigo="'.$registro['Empresa']['empresa_id'].'">';
				$xmlConteudo .= '<Secao><![CDATA['.$secao.']]></Secao>';
				$xmlConteudo .= '<Titulo><![CDATA['.$titulo.']]></Titulo>';				
				$xmlConteudo .= '<Texto><![CDATA['.$this->trataTexto($txt).']]></Texto>';	
				$xmlConteudo .= '<Publicacao>'.$datacad.'</Publicacao>';
				$xmlConteudo .= '<Data>'.$datacad.'</Data>';
				$xmlConteudo .= '<Criado>'.$datacad.'</Criado>';
				$xmlConteudo .= '<Atualizado>'.$dataalt.'</Atualizado>';
				$xmlConteudo .= '<Slug><![CDATA['.$slug.']]></Slug>';		
				$xmlConteudo .= '<endereco><![CDATA['.$endereco.']]></endereco>';	
				$xmlConteudo .= '<num><![CDATA['.$num.']]></num>';	
				$xmlConteudo .= '<compl><![CDATA['.$compl.']]></compl>';													
				$xmlConteudo .= '<bairro><![CDATA['.$bairro.']]></bairro>';	
				$xmlConteudo .= '<cidade><![CDATA['.$cidade.']]></cidade>';	
				$xmlConteudo .= '<cep><![CDATA['.$cep.']]></cep>';	
				$xmlConteudo .= '<fone1><![CDATA['.$fone1.']]></fone1>';	
				$xmlConteudo .= '<fone2><![CDATA['.$fone2.']]></fone2>';	
				$xmlConteudo .= '<email><![CDATA['.$email.']]></email>';	
				$xmlConteudo .= '<site><![CDATA['.$site.']]></site>';	
				$xmlConteudo .= '<keywords><![CDATA['.$keywords.']]></keywords>';	
				$xmlConteudo .= '<Ativo><![CDATA['.$status.']]></Ativo>';	
				$xmlConteudo .= '<cnpj><![CDATA['.$cnpj.']]></cnpj>';	
				$xmlConteudo .= '<horas24><![CDATA['.$horas24.']]></horas24>';	
				$xmlConteudo .= '<estacionamento><![CDATA['.$estacionamento.']]></estacionamento>';	
				$xmlConteudo .= '<fumantes><![CDATA['.$fumantes.']]></fumantes>';	
				$xmlConteudo .= '<reservas><![CDATA['.$reservas.']]></reservas>';	
				$xmlConteudo .= '<criancas><![CDATA['.$criancas.']]></criancas>';	
				$xmlConteudo .= '<necessidadesespeciais><![CDATA['.$necessidadesespeciais.']]></necessidadesespeciais>';	
				$xmlConteudo .= '<cheque><![CDATA['.$cheque.']]></cheque>';	
				$xmlConteudo .= '<credito><![CDATA['.$credito.']]></credito>';	
				$xmlConteudo .= '<debito><![CDATA['.$debito.']]></debito>';	

				$xmlConteudo .= '</Empresa>';
			}
			$xmlConteudo .= '</Empresas>';
	
			//criar o arquivo
			$file = new File($xmlsDir.DS.$xmlNome.$registro['Empresa']['empresa_id'].'.xml', true, 0775);
			$file->write($xmlConteudo);
			$file->close();
	
			//envia as variaveis para a view
			$this->set('ultimo_id',$registro['Empresa']['empresa_id']);
			$this->set('arquivo_gerado',$xmlsDir.DS.$xmlNome.$registro['Empresa']['empresa_id'].'.xml');
		}
	}

	public function admin_gerar_impressos_xml($id=0){
		
		App::uses('Xml', 'Utility');

		//Alterar o banco de dados
		$this->Importar->useDbConfig = 'bp';

		//Variáveis
		$xmlsDir 			= WWW_ROOT . 'files' . DS . 'importar';
		$xmlNome 			= 'bp_impressos_';		

		//busca as noticias
		$registros = $this->Importar->query("
			SELECT 
				Jornal.jornal_id,
				Jornal.tit,
				Jornal.datainicio,
				Jornal.datacad,
				Jornal.dataalt,
				Jornal.img1 
			FROM jornal AS Jornal 
			WHERE 
				Jornal.excluido = 'N' AND Jornal.jornal_id > $id 
			ORDER BY Jornal.jornal_id ASC 
			LIMIT 500
		");

		//se NÃO encontrou noticia
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			$xmlConteudo 	= '<?xml version="1.0" encoding="UTF-8"?><Capas>';
	
			foreach($registros as $registro){	


				//variaveis da noticia				
				$image 			= $registro['Jornal']['img1'] ? 'http://upload.portalparanaense.com.br/image/jornal/'.trim($registro['Jornal']['img1']) : '';
				$titulo			= $registro['Jornal']['tit'] ? trim($registro['Jornal']['tit']) : 'Não Informado - '.$registro['Jornal']['jornal_id'];		
				$slug 			= date('Y', strtotime($registro['Jornal']['datainicio'])).'/'.date('m', strtotime($registro['Jornal']['datainicio'])).'/'.date('d', strtotime($registro['Jornal']['datainicio'])).'/';
				$datainicio		= $registro['Jornal']['datainicio'] ? trim($registro['Jornal']['datainicio']) : '2005-01-01 00:00:00';
				$datacad		= $registro['Jornal']['datacad'] ? trim($registro['Jornal']['datacad']) : '2005-01-01 00:00:00';
				$dataalt		= $registro['Jornal']['dataalt'] ? trim($registro['Jornal']['dataalt']) : '2005-01-01 00:00:00';

				//Monta o XML
				$xmlConteudo .= '<Capa Codigo="'.$registro['Jornal']['jornal_id'].'">';
				$xmlConteudo .= '<Titulo><![CDATA['.$titulo.']]></Titulo>';				
				$xmlConteudo .= '<NumeroEdicao><![CDATA[]]></NumeroEdicao>';
				$xmlConteudo .= '<Link><![CDATA[]]></Link>';
				$xmlConteudo .= '<CodIncorporacao><![CDATA[]]></CodIncorporacao>';
				$xmlConteudo .= '<Slug><![CDATA['.$slug.']]></Slug>';
				$xmlConteudo .= '<Imagem><![CDATA['.$image.']]></Imagem>';		
				$xmlConteudo .= '<Publicacao>'.$datainicio.'</Publicacao>';
				$xmlConteudo .= '<Data>'.$datacad.'</Data>';
				$xmlConteudo .= '<Criado>'.$datacad.'</Criado>';
				$xmlConteudo .= '<Atualizado>'.$dataalt.'</Atualizado>';							
				$xmlConteudo .= '<Ativo>true</Ativo>';							
				$xmlConteudo .= '</Capa>';
			}
			$xmlConteudo .= '</Capas>';
	
			//criar o arquivo
			$file = new File($xmlsDir.DS.$xmlNome.$registro['Jornal']['jornal_id'].'.xml', true, 0775);
			$file->write($xmlConteudo);
			$file->close();
	
			//envia as variaveis para a view
			$this->set('ultimo_id',$registro['Jornal']['jornal_id']);
			$this->set('arquivo_gerado',$xmlsDir.DS.$xmlNome.$registro['Jornal']['jornal_id'].'.xml');
		}
	}

	public function admin_gerar_usuarios_xml($id=0){
		
		App::uses('Xml', 'Utility');

		//Alterar o banco de dados
		$this->Importar->useDbConfig = 'bp';

		//Variáveis
		$xmlsDir 			= WWW_ROOT . 'files' . DS . 'importar';
		$xmlNome 			= 'bp_usuarios_';		

		//busca as noticias
		$registros = $this->Importar->query("
			SELECT 
				Assinante.assinatura_id,
				Assinante.tit,
				Assinante.datacad,
				Assinante.dataalt,
				Assinante.email,
				Assinante.fone,
				Assinante.celular,
				Assinante.end,
				Assinante.bairro,
				Assinante.cidade,
				Assinante.uf,
				Assinante.cep,
				Assinante.senha,
				Assinante.datainicio,
				Assinante.datafim,
				Assinante.datanasc
			FROM assinatura AS Assinante 
			WHERE 
			Assinante.status = '3' AND Assinante.assinatura_id > $id 
			ORDER BY Assinante.assinatura_id ASC 
			LIMIT 1000
		");

		//se NÃO encontrou noticia
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			$xmlConteudo 	= '<?xml version="1.0" encoding="UTF-8"?><Usuarios>';
	
			foreach($registros as $registro){	


				//variaveis da noticia				
				$titulo			= $registro['Assinante']['tit'] ? trim($registro['Assinante']['tit']) : 'Não Informado - '.$registro['Assinante']['assinatura_id'];		
				$datainicio		= $registro['Assinante']['datainicio'] ? trim($registro['Assinante']['datainicio']) : '2005-01-01 00:00:00';
				$datafim		= $registro['Assinante']['datafim'] ? trim($registro['Assinante']['datafim']) : '2005-01-01 00:00:00';
				$datacad		= $registro['Assinante']['datacad'] ? trim($registro['Assinante']['datacad']) : '2005-01-01 00:00:00';
				$dataalt		= $registro['Assinante']['dataalt'] ? trim($registro['Assinante']['dataalt']) : '2005-01-01 00:00:00';

				//Monta o XML
				$xmlConteudo .= '<Usuario Codigo="'.$registro['Assinante']['assinatura_id'].'">';
				$xmlConteudo .= '<name><![CDATA['.$titulo.']]></name>';	
				$xmlConteudo .= '<username><![CDATA['.$titulo.']]></username>';		
				$xmlConteudo .= '<password><![CDATA['.$registro['Assinante']['senha'].']]></password>';		
				$xmlConteudo .= '<email><![CDATA['.$registro['Assinante']['email'].']]></email>';		
				$xmlConteudo .= '<nascimento><![CDATA['.$registro['Assinante']['datanasc'].']]></nascimento>';		
				$xmlConteudo .= '<endereco><![CDATA['.$registro['Assinante']['end'].']]></endereco>';	
				$xmlConteudo .= '<updated><![CDATA['.$dataalt.']]></updated>';	
				$xmlConteudo .= '<created><![CDATA['.$datacad.']]></created>';
				$xmlConteudo .= '<expirado_em><![CDATA['.$registro['Assinante']['datafim'].']]></expirado_em>';		
				$xmlConteudo .= '<telefone><![CDATA['.$registro['Assinante']['fone'].']]></telefone>';	
				$xmlConteudo .= '<celular><![CDATA['.$registro['Assinante']['celular'].']]></celular>';	
				$xmlConteudo .= '<cep><![CDATA['.$registro['Assinante']['cep'].']]></cep>';	
				$xmlConteudo .= '<status>1</status>';	
				$xmlConteudo .= '<role_id>4</role_id>';
					
				$xmlConteudo .= '</Usuario>';
			}
			$xmlConteudo .= '</Usuarios>';
	
			//criar o arquivo
			$file = new File($xmlsDir.DS.$xmlNome.$registro['Assinante']['assinatura_id'].'.xml', true, 0775);
			$file->write($xmlConteudo);
			$file->close();
	
			//envia as variaveis para a view
			$this->set('ultimo_id',$registro['Assinante']['assinatura_id']);
			$this->set('arquivo_gerado',$xmlsDir.DS.$xmlNome.$registro['Assinante']['assinatura_id'].'.xml');
		}
	}

	public function admin_gerar_posts_xml($id=0){
		//Configure::write('debug',1);
		App::uses('Xml', 'Utility');		

		//Alterar o banco de dados
		$this->Importar->useDbConfig = 'bp_blogs';

		///////////////////////////////////////
		$prefixo 			= 'papopet_';			
		$autor 				= 'Fabiana Ferreira';		
		$Nome 				= 'Crônicas sobre cães e outros bichos';	
		$Url 				= 'papopet';	
		$Foto 				= 'papopet.jpg';
		$idBlog 			= 84;
		

		//busca as noticias
		$registros = $this->Importar->query("
			SELECT 
				Post.ID,
				Post.post_date,				
				Post.post_title,
				Post.post_excerpt,
				Post.post_content,
				Post.post_name 
			FROM wp_posts AS Post 
			WHERE 
				Post.post_type = 'post' AND 
				Post.post_status <> 'draft' AND 
				Post.ID > $id 
			ORDER BY Post.ID ASC 
			LIMIT 200
		");		

		//se NÃO encontrou noticia
		if(empty($registros)){
			$this->set('ultimo_id',0);
		}else{
			$xmlConteudo 	= '<?xml version="1.0" encoding="UTF-8"?>
			<Blogs>
				<Blog Codigo="'.$idBlog.'">
					<Nome>'.$Nome.'</Nome>
					<Url>'.$Url.'</Url>
					<Foto>'.$Foto.'</Foto>
					<CriadoEm>2005-01-01T15:15:15</CriadoEm>
					<AtualizadoEm>2005-01-01T15:15:15</AtualizadoEm>
					<Chamada>'.$Nome.'</Chamada>
					<Ativo>true</Ativo>
					<Posts>
					';
	
			foreach($registros as $registro){
	
				//variaveis da noticia				
				$title			= $registro['Post']['post_title'] ? trim($registro['Post']['post_title']) : 'Não Informado - '.$registro['Post']['ID'];
				$slug			= $registro['Post']['post_name'] ? trim($registro['Post']['post_name']) : str_replace('_', '-', strtolower(Inflector::slug($title.'-'.$registro['Post']['ID'])));
				$publish_start	= $registro['Post']['post_date'] ? trim($registro['Post']['post_date']) : '2005-01-01 00:00:00';

				//Monta o XML
				$xmlConteudo .= '<Post Codigo="'.$registro['Post']['ID'].'">';
					$xmlConteudo .= '<Titulo><![CDATA['.$title.']]></Titulo>';	
					$xmlConteudo .= '<Publicacao><![CDATA['.$publish_start.']]></Publicacao>';			
					$xmlConteudo .= '<Texto><![CDATA['.$this->trataTexto($registro['Post']['post_content']).']]></Texto>';
					$xmlConteudo .= '<Resumo><![CDATA['.$this->trataTexto($registro['Post']['post_excerpt']).']]></Resumo>';
					$xmlConteudo .= '<Slug><![CDATA['.$slug.']]></Slug>';	
					$xmlConteudo .= '<CriadoEm>'.$publish_start.'</CriadoEm>';
					$xmlConteudo .= '<AtualizadoEm>'.$publish_start.'</AtualizadoEm>';							
					$xmlConteudo .= '<CriadoPor>admin</CriadoPor>';
					$xmlConteudo .= '<BlogAutor>'.$autor.'</BlogAutor>';							
				$xmlConteudo .= '</Post>';
			}
			$xmlConteudo .= '</Posts>
				</Blog>
			</Blogs>';
			
			//criar o arquivo
			$xmlsDir 			= WWW_ROOT . 'files' . DS . 'importar';
			$xmlNome 			= 'bp_blogs_'.$prefixo;	
			$file = new File($xmlsDir.DS.$xmlNome.$registro['Post']['ID'].'.xml', true, 0775);
			$file->write($xmlConteudo);
			$file->close();
	
			//envia as variaveis para a view
			$this->set('ultimo_id',$registro['Post']['ID']);
			$this->set('arquivo_gerado',$xmlsDir.DS.$xmlNome.$registro['Post']['ID'].'.xml');
		}
	}

	function trataTexto($string){
		$texto = trim(html_entity_decode(str_replace(array('<![CDATA[',']]>','','','','','','','','',''),'',$string)));
		return $texto;
	}

}
