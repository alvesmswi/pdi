<?php
App::uses('ImportarAppController', 'Importar.Controller');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class ImportarController extends ImportarAppController 
{
	public $name = 'Importar';
	public $urlImport 	= '';

	public $uses = array('Importar.Importar', 'Nodes.Node', 'Taxonomy.Term', 'Taxonomy.Taxonomy', 'NoticiumDetail', 'ModelTaxonomies', 'Multiattach.Multiattach', 'Blog.Blog', 'Blog.Post', 'Users.User');

	public function beforeFilter() {

		parent::beforeFilter();
		$this->Auth->allow('admin_index', 'admin_importar');
	}

	public function admin_settings() 
	{
		
	}

	public function admin_index()
	{
		//$xmlsDir = App::pluginPath('Importar') .'Webroot'.DS.'xmls';
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		$xmls = $this->localizarArquivos();
		$txt = $this->localizarArquivos('importar.txt');
		$this->set('xmlsDIr',$xmlsDir);
		$this->set('xmls',$xmls);
		$this->set('txt',$txt);
	}

	public function admin_importar($quantidade = 20)
	{
		//$this->autoRender = false;
		$arqImportacao = 'importar.txt';  
		//monta o diretorio para buscar os XMLs
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		//Tenta localizar arquivos
		$files = $this->localizarArquivos();
			
		//Se tem algo para importar
		if(!empty($this->localizarArquivos($arqImportacao))){
			$processados = 0;
			//le o arquivo txt
			$txt = new File($xmlsDir . DS . $arqImportacao);
			$txtConteudo = unserialize($txt->read());
			//percorre a primeiro chave
			foreach($txtConteudo as $chave1 => $conteudo){
				foreach($conteudo as $tipo => $nodes){
					//conta quantos faltam
					$restante = count($nodes);
					//se ainda tem nodes
					if(!empty($nodes)){
						//percorre os nodes
						foreach($nodes as $key => $node){
							//se for um blog
							if($tipo == 'Blog'){
								if(isset($nodes['Posts'])){
									$blogId = $nodes['@Codigo'];
									$posts = $nodes['Posts'];
									$txtConteudo['Posts']['Post'][$blogId] = $posts;
									unset($nodes['Posts']);
								}
								//Prepara as infomrações para salvar
								$this->preparaArray($tipo, $nodes);
								unset($txtConteudo['Blogs']);
							}else{
								//Prepara as infomrações para salvar
								$this->preparaArray($tipo, $node, $key, false);
								//remove o indice que foi salvo
								unset($txtConteudo[$chave1][$tipo][$key]);
							}
							$processados++;
							//Controla a quantidade a ser processada
							if($processados == $quantidade){
								break;
							}
						}
					//Se já acabou tudo
					}else{
						$concluido = true;
						break;
					}
				}
			}
			
			//fecha o arquivo
			$txt->close();
			$this->set('nodesRestantes',$restante);
			
			//se já terminou tudo
			if(isset($concluido)){
				//Deleta o arquivo
				$txt->delete();
			}else{
				//atualiza o arquivo
				$txt->write(serialize($txtConteudo), 'w', true);
			}
			
		}else{
			
			//se encontrou algum arquivo
			if(!empty($files)){
				//percore os arquivos
				foreach($files as $file){
					//le o arquivo XMLs
					$xml = Xml::build($xmlsDir.DS.$file); 
					//transofrma em array
					$xmlArray = Xml::toArray($xml);
					//pr($xmlArray);exit();
					$xmlArray = serialize($xmlArray);
					$arqImp = new File($xmlsDir.DS.'importar.txt');
					//Se criar o arquivo temporario
					if($arqImp->create($arqImp->path . DS . 'importar.txt', true, 0755)){
						//se gravar o array corretamente
						if($arqImp->write((string)$xmlArray, 'w', true)){
							//move o arquivos
							$this->moveXml($xmlsDir.DS.$file, $xmlsDir.DS.'importados');
						}
					}
					break;
				}
			}
		}
		$this->set('xmlsRestantes',count($files));
		$this->set('xmlsDir',$xmlsDir);
	}

	private function moveXml($arquivoDe, $arquivoPara){
		$arq1 = new File($arquivoDe);
		if ($arq1->exists()) {
			//Cpia o arquivo
			$arq2 = new Folder($arquivoPara, true);
			$arq1->copy($arq2->path . DS . $arq1->name);
			//deleta o arquivo imoprtado
			$arq1->delete($arquivoDe);
		}
	}

	private function localizarArquivos($ext = '.*\.xml'){
		//monta o diretorio para buscar os XMLs
		$xmlsDir = WWW_ROOT . 'files' . DS . 'importar';
		$dir = new Folder($xmlsDir);
		//procura apenas os XMLs
		$files = $dir->find($ext, true);
		natcasesort($files);		
		return $files;
	}

	private function corrigeTermo($idAntigo, $termo){
		//procura o node pelo ID eantigo
		$node = $this->Node->find(
			'first',
			array(
				'conditions' => array(
					'Node.id_antigo' => $idAntigo
				),
				'fields' => array(
					'Node.id',
					'Node.id_antigo'
				),
				'recursive' => -1
			) 
		);

		//se encontrou o node
		if(!empty($node)){
			//pega o id do node
			$nodeId = $node['Node']['id'];
			//trata o slug
			$slug 	= strtolower(Inflector::slug($termo, '-'));
			//procura o ermo
			$termoSQL = $this->Node->query("SELECT tm.id, tx.id FROM terms as tm INNER JOIN taxonomies as tx ON(tx.term_id=tm.id) WHERE slug = '$slug';");
			//se encontrou o termo
			if(!empty($termoSQL)){
				$termId = $termoSQL[0]['tm']['id'];
				$taxId = $termoSQL[0]['tx']['id'];			
				//atualiza o node
				$terms = '{"'.$termId.'":"'.$slug.'"}';
				$this->Node->query("UPDATE nodes SET terms = '$terms' WHERE id = $nodeId;");
				//atualiza o vinculo
				$this->Node->query("INSERT INTO model_taxonomies (model, foreign_key, taxonomy_id) VALUES ('Node', '$nodeId', '$taxId');");
			}
		}
	}

	private function preparaArray($tipo = 'Noticia', $dados, $idPai = null, $corrigeTermo=false) 
	{
		//se for noticiaa
		if($tipo == 'Noticia'){
			//se não está vazio
			if (!empty($dados)) {
				if($corrigeTermo){
					$this->corrigeTermo($dados['@Código'], trim($dados['Secao']));
					return true;
				}

				//trata os dados
				$type 			= 'noticia';
				$id_antigo 		= $dados['@Código'];
				$titulo 		= trim($dados['Titulo']);
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$slug			= $dados['Slug'];

				//se não tem slug
				if(empty($dados['Slug'])){
					$slug = str_replace('_', '-', strtolower(Inflector::slug($titulo)));
				}

				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= trim($slug);
				//$arraySave['Node']['body'] 				= html_entity_decode($dados['Texto']);
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['excerpt'] 			= $dados['Gravata'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['lida'] 				= $dados['Lidas'];
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['modified'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				$arraySave['Node']['relacionadas'] 		= trim($dados['Relacionadas']);
				
				/* -- noticium_details -- */
				$arraySave['NoticiumDetail']['jornalista'] 	= $dados['Jornalista'];
				$arraySave['NoticiumDetail']['chapeu'] 		= $dados['Chapeu'];
				$arraySave['NoticiumDetail']['titulo_capa'] = $dados['TituloCapa'];
				$arraySave['NoticiumDetail']['keywords'] 	= $dados['Keywords'];
				
				/* -- multiattachments -- */
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$images = explode(',', $dados['Imagem']);
					foreach($images as $key => $image){					
						$arrayFile = explode('/', $image);
						$arraySave['Multiattachments'][$key]['filename'] = end($arrayFile);
						$arraySave['Multiattachments'][$key]['filepath'] = $image;						
					}
				}

				//Taxonomia
				$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']));
			}
		}

		//se for noticiaa
		if($tipo == 'Empresa'){
			//se não está vazio
			if (!empty($dados)) {
				//pr($dados);exit();
				if($corrigeTermo){
					$this->corrigeTermo($dados['@Codigo'], trim($dados['Secao']));
					return true;
				}

				//trata os dados
				$type 			= 'empresa';
				$id_antigo 		= $dados['@Codigo'];
				$titulo 		= trim($dados['Titulo']);
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= '1';
				$slug			= $dados['Slug'];

				//se não tem slug
				if(empty($dados['Slug'])){
					$slug = str_replace('_', '-', strtolower(Inflector::slug($titulo)));
				}

				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= trim($slug);
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['modified'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				/* -- details -- */
				$arraySave['EmpresaDetail']['endereco'] 	= $dados['endereco'];			
				$arraySave['EmpresaDetail']['numero'] 	= $dados['num'];		
				$arraySave['EmpresaDetail']['complemento'] 	= $dados['compl'];		
				$arraySave['EmpresaDetail']['bairro'] 	= $dados['bairro'];		
				$arraySave['EmpresaDetail']['cidade'] 	= $dados['cidade'];		
				$arraySave['EmpresaDetail']['cep'] 	= $dados['cep'];		
				$arraySave['EmpresaDetail']['fone1'] 	= $dados['fone1'];		
				$arraySave['EmpresaDetail']['fone2'] 	= $dados['fone2'];
				$arraySave['EmpresaDetail']['email'] 	= $dados['email'];			
				$arraySave['EmpresaDetail']['site'] 	= $dados['site'];		
				$arraySave['EmpresaDetail']['cnpj'] 	= $dados['cnpj'];		
				$arraySave['EmpresaDetail']['24horas'] 	= $dados['horas24'];		
				$arraySave['EmpresaDetail']['estacionamento'] 	= $dados['estacionamento'];		
				$arraySave['EmpresaDetail']['fumantes'] 	= $dados['fumantes'];		
				$arraySave['EmpresaDetail']['reservas'] 	= $dados['reservas'];		
				$arraySave['EmpresaDetail']['criancas'] 	= $dados['criancas'];
				$arraySave['EmpresaDetail']['necessidades_especiais'] 	= $dados['necessidadesespeciais'];			
				$arraySave['EmpresaDetail']['cheque'] 	= $dados['cheque'];		
				$arraySave['EmpresaDetail']['credito'] 	= $dados['credito'];		
				$arraySave['EmpresaDetail']['debito'] 	= $dados['debito'];	


				//Taxonomia
				$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']),11);
			}
		}

		//se for página plana
		if ($tipo == 'PaginaPlana') {
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'page';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$slug			= $dados['Slug'];

				//se não tem slug
				if(empty($dados['Slug'])){
					$slug = str_replace('_', '-', strtolower(Inflector::slug(trim($dados['Titulo']))));
				}
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= trim($dados['Titulo']);
				$arraySave['Node']['slug'] 				= $slug;
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
			}
		}

		//se for página (diário dos campos)
		if ($tipo == 'Pagina') {
			//se não está vazio
			if (!empty($dados)) {
				//trata os dados
				$type 			= 'page';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s');
				$created 		= date('Y-m-d H:i:s');
				$modified 		= date('Y-m-d H:i:s');
				$status 		= $dados['Ativo'] ? '1' : '0';
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= trim($dados['Publicacao']);
				$arraySave['Node']['slug'] 				= trim($dados['Slug']);
				$arraySave['Node']['body'] 				= $dados['NumeroEdicao'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
			}
		}

		//se for Impresso
		if($tipo == 'Capa'){ // || $tipo = 'Impressos'
			//se não está vazio
			if(!empty($dados)){

				$this->urlImport 	= '';

				//trata os dados
				$type 			= 'impresso';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $dados['Titulo'];
				$arraySave['Node']['slug'] 				= $dados['Slug'];
				$arraySave['Node']['body'] 				= $dados['CodIncorporacao'];
				$arraySave['Node']['excerpt'] 			= $dados['Link'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				//details 
				/*$arraySave['ImpressoDetail']['edicao'] 			= trim($dados['NumeroEdicao']);
				$arraySave['ImpressoDetail']['link'] 			= trim($dados['Link']);
				$arraySave['ImpressoDetail']['incorporacao'] 	= trim($dados['CodIncorporacao']);*/
				
				/* -- multiattachments -- */
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					//$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
					$arraySave['Multiattachments'][0]['filepath'] = str_replace('/imagem/', '/capaimpressa/', $dados['Imagem']);
				}
				/* -- multiattachments -- */
				if(isset($dados['PDF']) && !empty($dados['PDF'])){
					$arrayFile = explode('/', $dados['PDF']);
					$arraySave['Multiattachments']['pdf']['filename'] = end($arrayFile);
					$arraySave['Multiattachments']['pdf']['filepath'] = str_replace('/imagem/', '/capaimpressa/', $dados['Imagem']);
				}
			}
		}

		//se for noticiaa
		if($tipo == 'Galeria'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'galeria';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= trim($dados['Descricao']);
				$arraySave['Node']['slug'] 				= str_replace('_', '-', strtolower(Inflector::slug($dados['Descricao'])));
				$arraySave['Node']['body'] 				= $dados['Content'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				//Details
				$arraySave['GaleriumDetail']['creditos'] 		= trim($dados['Chapeu']);
				$arraySave['GaleriumDetail']['creditos'] 		= trim($dados['Creditos']);
				$arraySave['GaleriumDetail']['ficha_tecnica'] 	= trim($dados['FichaTecnica']);
				
				//multiattachments 
				if(isset($dados['Medias']) && !empty($dados['Medias'])){
					foreach($dados['Medias'] as $fotos){
						//Percorre as fotos
						foreach($fotos as $key => $foto){
							$arrayFile = explode('/', $foto['Caminho']);
							$arraySave['Multiattachments'][$key]['filename'] 	= end($arrayFile);
							$arraySave['Multiattachments'][$key]['filepath'] 	= $foto['Caminho'];
							$arraySave['Multiattachments'][$key]['comment'] 	= $foto['Legenda'];
						}
					}
				}
			}
		}

		if($tipo == 'GaleriaImpressos'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'impresso';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= trim($dados['Descricao']);
				$arraySave['Node']['slug'] 				= str_replace('_', '-', strtolower(Inflector::slug($dados['Descricao'])));
				$arraySave['Node']['body'] 				= $dados['Descricao'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;							
				// pr($arraySave['Node']);exit();
				//multiattachments 
				if(isset($dados['Medias']) && !empty($dados['Medias'])){					
					foreach($dados['Medias'] as $fotos){
						// pr(count($dados['Medias']['Media']));exit();
						$count = count($dados['Medias']['Media']);						
						//Percorre as fotos
						if($count > 3){
							foreach($fotos as $key => $foto){							
								$arrayFile = explode('/', $foto['Caminho']);
								$arraySave['Multiattachments'][$key]['filename'] 	= end($arrayFile);
								$arraySave['Multiattachments'][$key]['filepath'] 	= $foto['Caminho'];
								$arraySave['Multiattachments'][$key]['comment'] 	= $foto['Legenda'];
							}
						}else{
								$arrayFile = explode('/', $fotos['Caminho']);
								$arraySave['Multiattachments'][0]['filename'] 	= end($arrayFile);
								$arraySave['Multiattachments'][0]['filepath'] 	= $fotos['Caminho'];
								$arraySave['Multiattachments'][0]['comment'] 	= $fotos['Legenda'];
						}
					}
				}
			}
		}

		//se for Imagem do Dia
		if($tipo == 'ImagemDia'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'imagem';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$titulo 		= $dados['Titulo'] != '' ? $dados['Titulo'] : $id_antigo;
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= str_replace('_', '-', strtolower(Inflector::slug($titulo)));
				$arraySave['Node']['body'] 				= $dados['Descricao'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				//details 
				$arraySave['ImagemDetail']['credito'] 			= trim($dados['Credito']);
				$arraySave['ImagemDetail']['tipo'] 				= trim($dados['Tipo']);
				
				//multiattachments 
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
				}
			}
		}

		//se for Imagem do Dia
		if($tipo == 'Media'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'imagem';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$titulo 		= $dados['Chapeu'] != '' ? $dados['Chapeu'] : $id_antigo;
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['chapeu'] 			= $titulo;
				$arraySave['Node']['slug'] 				= str_replace('_', '-', strtolower(Inflector::slug($titulo)));
				$arraySave['Node']['body'] 				= $dados['Descricao'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				//details 
				$arraySave['ImagemDetail']['creditos'] 			= trim($dados['Creditos']);
				$arraySave['ImagemDetail']['tipo'] 				= trim($dados['Tipo']);
				
				//multiattachments 
				if(isset($dados['Caminho']) && !empty($dados['Caminho'])){
					$arrayFile = explode('/', $dados['Caminho']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Caminho'];
				}
			}
		}

		if($tipo == 'Video'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'videos';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$titulo 		= $dados['Titulo'] != '' ? $dados['Titulo'] : $id_antigo;
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= strtolower(Inflector::slug($titulo, '-'));
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				//details 
				$arraySave['VideoDetail']['video'] 	= trim($dados['LinkExterno']);

				//multiattachments 
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
				}

				//Taxonomia
				if (!empty(trim($dados['Secao']))) {
					$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']), 7);
				}
			}
		}

		if($tipo == 'Foto'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type 			= 'fotos';
				$id_antigo 		= $dados['@Codigo'];
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$titulo 		= $dados['Titulo'] != '' ? $dados['Titulo'] : $id_antigo;
				
				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= strtolower(Inflector::slug($titulo, '-'));
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['updated'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;

				//multiattachments 
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
				}

				//Taxonomia
				if (!empty(trim($dados['Secao']))) {
					$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']), 8);
				}
			}
		}

		if($tipo == 'Blog'){
			//se não está vazio
			if(!empty($dados)){
				//trata os dados
				$type			= 'blog';
				$id_antigo 		= $dados['@Codigo'];
				$status 		= $dados['Ativo'] ? '1' : '0';		
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['CriadoEm']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['AtualizadoEm']));		

				$query = "SELECT id FROM blogs WHERE id = $id_antigo;";
				$result = $this->Importar->query($query);
				if(!empty($result)){
					return true;
				}

				//Prepara o array para salvar
				$arraySave['Blog'] = array();
				$arraySave['Blog']['id'] 				= $id_antigo;
				$arraySave['Blog']['title'] 			= $dados['Nome'];
				$arraySave['Blog']['slug'] 				= $dados['Url'];
				$arraySave['Blog']['status'] 			= $status;
				$arraySave['Blog']['image'] 			= Router::url('/content/media/blog/', true).$dados['Foto'];
				$arraySave['Blog']['id_antigo'] 		= $id_antigo;
				$arraySave['Blog']['created'] 			= $created;
				$arraySave['Blog']['updated'] 			= $modified;
				$arraySave['Blog']['publish_start'] 	= $publish_start;
			}
		}
		
		//se for Post
		if($tipo == 'Post'){
			//se não está vazio
			if(!empty($dados)){
				$dado = (array)$dados;
				//Prepara o array para salvar
				$arraySave = array();
				foreach($dados as $dado){
					if (!isset($dado[0]) && !empty($dado)) { // Se tiver Posts, mas 1 só, não possui index.
						

						$key = 0;
						$n = $dado;
						//trata os dados
						$type			= 'post';
						$id_antigo 		= $n['@Codigo'];
						$publish_start 	= date('Y-m-d H:i:s', strtotime($n['CriadoEm']));
						$created 		= date('Y-m-d H:i:s', strtotime($n['CriadoEm']));
						$modified 		= date('Y-m-d H:i:s', strtotime($n['AtualizadoEm']));
						$status 		= '1';		

						$query = "SELECT id FROM blog_nodes WHERE blog_id = $idPai AND id_antigo = $id_antigo;";
						$result = $this->Importar->query($query);
						if(!empty($result)){
							return true;
						}
						
						$arraySave['Post'][$key]['id'] 				= null;
						$arraySave['Post'][$key]['user_id'] 		= '1';
						$arraySave['Post'][$key]['blog_id'] 		= $idPai;
						$arraySave['Post'][$key]['status'] 			= '1';
						$arraySave['Post'][$key]['title'] 			= trim($n['Titulo']);
						$arraySave['Post'][$key]['body'] 			= $n['Texto'];
						$arraySave['Post'][$key]['slug'] 			= $n['Slug'];
						$arraySave['Post'][$key]['status'] 			= $status;
						$arraySave['Post'][$key]['created'] 		= $created;
						$arraySave['Post'][$key]['updated'] 		= $modified;
						$arraySave['Post'][$key]['publish_start'] 	= $publish_start;

						$arraySave['Post'][$key]['image'] = '';
						if (isset($n['Foto']) && !empty($n['Foto'])) {
							$arraySave['Post'][$key]['image'] = Router::url('/content/media/blog/', true).$n['Foto'];
						}
						
						$arraySave['Post'][$key]['id_antigo'] 		= $id_antigo;
					} else { // Se não, continua normal
						foreach ($dado as $key => $n) {
							//trata os dados
							$type			= 'post';
							$id_antigo 		= $n['@Codigo'];
							$publish_start 	= date('Y-m-d H:i:s', strtotime($n['CriadoEm']));
							$created 		= date('Y-m-d H:i:s', strtotime($n['CriadoEm']));
							$modified 		= date('Y-m-d H:i:s', strtotime($n['AtualizadoEm']));
							$status 		= '1';		
							
							$query = "SELECT id FROM blog_nodes WHERE blog_id = $idPai AND id_antigo = $id_antigo;";
							$result = $this->Importar->query($query);
							if(!empty($result)){
								continue;;
							}
							
							$arraySave['Post'][$key]['id'] 				= null;
							$arraySave['Post'][$key]['user_id'] 		= '1';
							$arraySave['Post'][$key]['blog_id'] 		= $idPai;
							$arraySave['Post'][$key]['status'] 			= '1';
							$arraySave['Post'][$key]['title'] 			= trim($n['Titulo']);
							$arraySave['Post'][$key]['body'] 			= $n['Texto'];
							$arraySave['Post'][$key]['slug'] 			= $n['Slug'];
							$arraySave['Post'][$key]['status'] 			= $status;
							$arraySave['Post'][$key]['created'] 		= $created;
							$arraySave['Post'][$key]['updated'] 		= $modified;
							$arraySave['Post'][$key]['publish_start'] 	= $publish_start;

							$arraySave['Post'][$key]['image'] = '';
							if (isset($n['Foto']) && !empty($n['Foto'])) {
								$arraySave['Post'][$key]['image'] = Router::url('/content/media/blog/', true).$n['Foto'];
							}
							
							$arraySave['Post'][$key]['id_antigo'] 		= $id_antigo;
						}	
					}
				}
			}
		}
		
		//se for Assinantes
		if($tipo == 'ROW'){
			$type			= 'user';
			//se não está vazio
			if(!empty($dados)){
				//se não está excluido
				if(!$dados['COLUMN'][9]['@']){
					//trata os dados
					$id_antigo 		= $dados['COLUMN'][11]['@'];
					$username = explode('@', $dados['COLUMN'][1]['@']);
					$username = $username[0];
					$nome = $username;
					if(!empty($dados['COLUMN'][0]['@'])){
						$nome = $dados['COLUMN'][0]['@'];
					}
					$created		= str_replace('/', '-', $dados['COLUMN'][4]['@']);
					$created 		= date('Y-m-d H:i:s', strtotime($created));
					$updated		= str_replace('/', '-', $dados['COLUMN'][5]['@']);
					$updated		= date('Y-m-d H:i:s', strtotime($updated));
					$expirado		= str_replace('/', '-', $dados['COLUMN'][3]['@']);
					$expirado		= date('Y-m-d H:i:s', strtotime($expirado));
					$nascimento 	= '';
					if(!empty($dados['COLUMN'][13]['@'])){
						$nascimento		= str_replace('/', '-', $dados['COLUMN'][13]['@']);
						$nascimento		= date('Y-m-d', strtotime($nascimento));
					}
					$senha 			= 'adc860adc5bf57c6635512c0aff383cb5764c7b4';
					
					
					//Prepara o array para salvar
					$arraySave['User'] = array();
					$arraySave['User']['id'] 				= null;
					$arraySave['User']['role_id'] 			= 4;
					$arraySave['User']['username'] 			= $username;
					$arraySave['User']['password'] 			= $senha;
					$arraySave['User']['name'] 				= $nome;
					$arraySave['User']['email'] 			= $dados['COLUMN'][1]['@'];
					$arraySave['User']['activation_key'] 	= md5(uniqid());
					$arraySave['User']['status'] 			= $dados['COLUMN'][8]['@'];
					$arraySave['User']['created'] 			= $created;
					$arraySave['User']['updated'] 			= $updated;
					$arraySave['User']['site_id']			= $id_antigo;
					$arraySave['User']['timezone'] 			= '0';
					$arraySave['User']['empresa_id'] 		= isset($dados['COLUMN'][10]['@'])?($dados['COLUMN'][10]['@']):('');
					$arraySave['User']['referencia'] 		= isset($dados['COLUMN'][11]['@'])?($dados['COLUMN'][11]['@']):('');
					$arraySave['User']['telefone'] 			= isset($dados['COLUMN'][12]['@'])?($dados['COLUMN'][12]['@']):('');
					$arraySave['User']['nascimento'] 		= $nascimento;
					$arraySave['User']['sexo'] 				= isset($dados['COLUMN'][14]['@'])?($dados['COLUMN'][14]['@']):('');
					$arraySave['User']['endereco'] 			= isset($dados['COLUMN'][15]['@'])?($dados['COLUMN'][15]['@']):('');
					$arraySave['User']['numero'] 			= isset($dados['COLUMN'][16]['@'])?($dados['COLUMN'][16]['@']):('');
					$arraySave['User']['complemento'] 		= isset($dados['COLUMN'][17]['@'])?($dados['COLUMN'][17]['@']):('');
					$arraySave['User']['cep'] 				= isset($dados['COLUMN'][18]['@'])?($dados['COLUMN'][18]['@']):('');
					$arraySave['User']['cpf'] 				= isset($dados['COLUMN'][19]['@'])?($dados['COLUMN'][19]['@']):('');
					$arraySave['User']['rg'] 				= isset($dados['COLUMN'][20]['@'])?($dados['COLUMN'][20]['@']):('');
					$arraySave['User']['celular'] 			= isset($dados['COLUMN'][21]['@'])?($dados['COLUMN'][21]['@']):('');
				}
			}
		}

		//se for Assinantes
		if($tipo == 'Usuario'){
			$type			= 'user';			
			//se não está vazio
			if(!empty($dados)){					
				//pr($dados);exit();
				//verifica se este email já existe
				$query = 'SELECT id FROM users WHERE email = "'.$dados['email'].'";';
				$result = $this->Importar->query($query);
				if(!empty($result)){
					//pr($dados);
					//pula este registro
					return true;
				}
			
				//Prepara o array para salvar
				$arraySave['User'] = array();
				$arraySave['User']['id'] 				= null;
				$arraySave['User']['role_id'] 			= 4;
				$arraySave['User']['username'] 			= substr(strtolower(Inflector::slug($dados['name'])).$dados['@Codigo'],0,64);
				$arraySave['User']['password'] 			= AuthComponent::password($dados['password']);
				$arraySave['User']['name'] 				= substr($dados['name'],0,128);
				$arraySave['User']['email'] 			= $dados['email'];
				$arraySave['User']['activation_key'] 	= md5(uniqid());
				$arraySave['User']['status'] 			= 1;
				$arraySave['User']['created'] 			= $dados['created'];
				$arraySave['User']['updated'] 			= $dados['updated'];
				$arraySave['User']['telefone'] 			= $dados['telefone'];
				$arraySave['User']['nascimento'] 		= $dados['nascimento'];
				$arraySave['User']['endereco'] 			= $dados['endereco'];
				$arraySave['User']['cep'] 				= $dados['cep'];
				$arraySave['User']['celular'] 			= $dados['celular'];
			}
		}

		// Se for Termo
		if ($tipo == 'Term') {
			// Se não está vazio
			if (!empty($dados)) {
				// Trata os dados
				$type 		= 'termo';
				$id_antigo 	= $dados['@Codigo'];
				$titulo 	= trim($dados['Name']);
				$slug 		= trim($dados['Slug']);

				// Prepara o array para salvar
				$arraySave['Taxonomy'] = array(
					'id' 			=> null,
					'parent_id' 	=> null
				);
				$arraySave['Term'] = array(
					'id' 			=> null,
					'title' 		=> $titulo,
					'slug' 			=> $slug,
					'description' 	=> null
				);
			}
		}

		// Se for Atrativos
		if($tipo == 'Atrativo'){
			//se não está vazio
			if (!empty($dados)) {
				if($corrigeTermo){
					$this->corrigeTermo($dados['@Codigo'], trim($dados['Secao']));
					return true;
				}

				//trata os dados
				$type 			= 'atrativos';
				$id_antigo 		= $dados['@Codigo'];
				$titulo 		= trim($dados['Titulo']);
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$slug			= $dados['Slug'];

				//se não tem slug
				if(empty($dados['Slug'])){
					$slug = strtolower(Inflector::slug($titulo, '-'));
				}

				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= trim($slug);
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['excerpt'] 			= $dados['Gravata'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['lida'] 				= $dados['Lidas'];
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['modified'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				/* -- multiattachments -- */
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
				}

				//Taxonomia
				$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']), 6);
			}
		}

		// Se for PlanetaFoz
		if($tipo == 'PlanetaFoz'){
			//se não está vazio
			if (!empty($dados)) {
				if($corrigeTermo){
					$this->corrigeTermo($dados['@Codigo'], trim($dados['Secao']));
					return true;
				}

				//trata os dados
				$type 			= 'planeta-foz';
				$id_antigo 		= $dados['@Codigo'];
				$titulo 		= trim($dados['Titulo']);
				$publish_start 	= date('Y-m-d H:i:s', strtotime($dados['Publicacao']));
				$created 		= date('Y-m-d H:i:s', strtotime($dados['Criado']));
				$modified 		= date('Y-m-d H:i:s', strtotime($dados['Atualizado']));
				$status 		= $dados['Ativo'] ? '1' : '0';
				$slug			= $dados['Slug'];

				//se não tem slug
				if(empty($dados['Slug'])){
					$slug = strtolower(Inflector::slug($titulo, '-'));
				}

				//Prepara o array para salvar
				$arraySave['Node'] = array();
				$arraySave['Node']['id'] 				= null;
				$arraySave['Node']['user_id'] 			= '1';
				$arraySave['Node']['title'] 			= $titulo;
				$arraySave['Node']['slug'] 				= trim($slug);
				$arraySave['Node']['body'] 				= $dados['Texto'];
				$arraySave['Node']['excerpt'] 			= $dados['Gravata'];
				$arraySave['Node']['status'] 			= $status;
				$arraySave['Node']['lida'] 				= $dados['Lidas'];
				$arraySave['Node']['publish_start'] 	= $publish_start;
				$arraySave['Node']['created'] 			= $created;
				$arraySave['Node']['modified'] 			= $modified;
				$arraySave['Node']['id_antigo'] 		= $id_antigo;
				$arraySave['Node']['type'] 				= $type;
				
				/* -- multiattachments -- */
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['Multiattachments'][0]['filename'] = end($arrayFile);
					$arraySave['Multiattachments'][0]['filepath'] = $dados['Imagem'];
				}

				//Taxonomia
				$arraySave['TaxonomyData'][0] = $this->getTermId(trim($dados['Secao']), 9);
			}
		}

		// Se for Multiattachment
		if($tipo == 'Multiattachment'){
			//se não está vazio
			if (!empty($dados)) {
				//trata os dados
				$type 			= 'multiattachments';
				$id_antigo 		= $dados['@Codigo'];
				$node_type 		= $dados['NodeType'];
				$node_id 		= $dados['NodeId'];
				$filename 		= trim($dados['Filename']);
				$comment 		= trim($dados['Comment']);
				$meta 			= trim($dados['Meta']);
				
				if(isset($dados['Imagem']) && !empty($dados['Imagem'])){
					$arrayFile = explode('/', $dados['Imagem']);
					$arraySave['filename'] 			= end($arrayFile);
					$arraySave['filepath'] 			= $dados['Imagem'];
					//$arraySave['comment'] 			= $filename;
					$arraySave['node_antigo_id'] 	= $node_id;
					$arraySave['node_antigo_type'] 	= $node_type;
				}
			}
		}
		
		if (isset($arraySave) && !empty($arraySave)) {
			//Se salvar corretamente
			if ($this->salvar($arraySave, $type)) {
				return true;
			} else {
				return false;
			}
		}
	}
		
	private function salvar($data, $typeAlias = 'Noticia') {

		//pega os dados
		$Node 					= $this->Node;
		$this->request->data 	= $data;
		
		//se for blog
		if($typeAlias == 'blog'){
			//se slvar corretamente o blgo
			if ($this->Blog->save($this->request->data)) {
				return true;
			} else {
				$this->log($this->request->data, 'erro_importar');
				return false;
			}
		}
		
		//se for post
		if ($typeAlias == 'post') {
			//se slvar corretamente o blgo
			if ($this->Post->saveAll($this->request->data['Post'])) {
				return true;
			} else {
				$this->log($this->Post->invalidFields(), 'erro_importar');
				return false;
			}
		}
		
		//se for post
		if($typeAlias == 'user'){
			//se slvar corretamente o blgo
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				return true;
			} else {
				//pr($this->User->validationErrors);exit();
				//pr($this->User->getDataSource()->getLog(false, false));exit();
				$this->log('não salvou o novo usuario: '.$data['User']['username'], 'importer');
				return false;
			}
		}

		// Se for termo
		if ($typeAlias == 'termo') {
			// Se salvar o novo Termo
			if ($this->Term->add($data, 1)) {
				return true;
			} else {
				$this->log('não salvou o novo termo: '.$data['Term']['title'], 'erroSalvarTermo');
				return false;
			}
		}

		// Se for multiattachments
		if ($typeAlias == 'multiattachments') {
			$node_id = array();

			$arrayType = array(
				'pagina' 	=> 'page',
				'noticia' 	=> 'noticia',
				'foto' 		=> 'fotos',
				'post' 		=> array('atrativos', 'planeta-foz')
			);

			$node_id = $Node->find('first', array(
				'conditions' => array(
					'Node.id_antigo' => $this->request->data['node_antigo_id'],
					'Node.type' => $arrayType[$this->request->data['node_antigo_type']]		
				),
				'fields' => array(
					'Node.id'
				),
				'recursive' => -1
			));

			if (isset($node_id['Node']['id']) && !empty($node_id['Node']['id'])) {
				$this->saveImageNode($this->request->data, $node_id['Node']['id'], true);

				return true;
			} else {
				return false;
			}
		}

		//verifica se o node já existe
		$node = $Node->find(
			'first',
			array(
				'conditions' => array(
					'Node.id_antigo' => $this->request->data['Node']['id_antigo'],
					'Node.type' => $this->request->data['Node']['type']					
				),
				'fields' => array(
					'Node.id'
				),
				'recursive' => -1
			)
		);
		//se encontrou
		if(!empty($node)){
			//faz com que atualize os dados
			$this->request->data['Node']['id'] = $node['Node']['id'];
		}
		//pr($this->request->data['Node']);
		//se a data está incorreta
		if($this->request->data['Node']['created']=='-0001-11-30 00:00:00'){
			//pega a data do created
			$this->request->data['Node']['created'] = '2010-10-10 10:10:10';
		}
		//se a data está incorreta
		if($this->request->data['Node']['publish_start']=='-0001-11-30 00:00:00'){
			//pega a data do created
			$this->request->data['Node']['publish_start'] = $this->request->data['Node']['created'];
		}		
		//se a data está incorreta
		/*if($this->request->data['Node']['updated']=='-0001-11-30 00:00:00'){
			//pega a data do created
			$this->request->data['Node']['updated'] = $this->request->data['Node']['created'];
		}*/
		//pr($this->request->data['Node']);exit();
		$this->request->data['Node']['body'] = str_replace(array('“', '”'), '', $this->request->data['Node']['body']);

		$this->request->data['Node']['slug'] = substr($this->request->data['Node']['slug'],0,128);
		
		//Se salvar corretamente o node
		if ($Node->saveNode($this->request->data, $typeAlias)) {
			Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $this->request->data));
			//Pega o ID que foi salvo
			$id_salvo = $Node->getLastInsertID();
			//Se salvar corretamente os anexos			
			if (isset($this->request->data['Multiattachments']) && !empty($this->request->data['Multiattachments'])) {
				foreach ($this->request->data['Multiattachments'] as $key => $multiattachments) {
					$copyImagem = true;
					if($key==='pdf'){
						$copyImagem = false;
					}
					$this->saveImageNode($multiattachments, $id_salvo, $copyImagem);
				}
			}
			return true;
		} else {
			return false;
		}
	}

	//testa a se a imagem existe
	function url_exists($url) {

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return ($code == 200); // verifica se recebe "status OK"
	}

	/**
	 * saveImageNode
	 * save image for node
	 */
	private function saveImageNode($dataImage, $node, $copyImagem = true) 
	{
		$url_image 	= $this->urlImport.str_replace(' ', '%20', $dataImage['filepath']);
		// pr($url_image);exit;
		///Se a imagem exite
		if($this->url_exists($url_image)){
			$size 		= getimagesize($this->urlImport.str_replace(' ', '%20', $dataImage['filepath']));

			$name 			= strtolower(str_replace(' ', '_', basename($dataImage['filename'])));
			$nameArray 		= explode('.', $name);
			$nameArray[0] 	= Inflector::slug($nameArray[0]);
			$nameArray 		= implode('.', $nameArray);
			$name 			= $nameArray;

			$save_url 	= APP.'files'.DS.$node;
			$file_path 	= '/files/'.$node.'/'.$name;
			$mime_type 	= (!empty($size['mime'])) ? $size['mime'] : '';

			$comment 	= (isset($dataImage['comment'])) ? $dataImage['comment'] : null;

			$folder 	= new Folder();
		
			if($copyImagem){
				if ($folder->create($save_url)) {
					copy($url_image,$save_url.DS.$name);
				}
			}else{
				$mime_type = 'application/pdf';
			}			

			$this->Multiattach->create();
			$this->Multiattach->set(array('node_id' => $node, 'real_filename' => $file_path, 'filename' => $name, 'comment' => $comment, 'mime' => $mime_type));
			$this->Multiattach->save();
		}
		
	}

	private function getTermId($term, $vocabularyId = '1') 
	{
		$slugTerm 	= strtolower(Inflector::slug($term, '-'));
		$term_find 	= $this->Term->findBySlug($slugTerm, array('id', 'slug'));

		if (!empty($term_find)) {
			if (!empty($term_find['Vocabulary'])) {
				$taxonomy_id = null;
				foreach ($term_find['Vocabulary'] as $key => $taxonomy) {
					$taxonomy_id = $taxonomy['Taxonomy']['id'];
				}
				return $taxonomy_id;
			}else{
				//cria um taxononomies TAXONOMIES
				$termId = $term_find['Term']['id'];
				$query = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termId,$vocabularyId,0,0);";
				$this->Term->query($query);
				$this->Taxonomy->recover();
			}	
		} else {
			$arrayTerm['Taxonomy'] = array(
				'id' 			=> null,
				'parent_id' 	=> null,
				'vocabulary_id' => $vocabularyId
			);
			$arrayTerm['Term'] = array(
				'id' 			=> null,
				'title' 		=> $term,
				'slug' 			=> $slugTerm,
				'description' 	=> null
			);
			if ($this->Term->add($arrayTerm, $vocabularyId)) {
				return $this->getTermId($term, $vocabularyId);
			} else {
				$this->log('não salvou o novo termo: '.$term, 'erroSalvarTermo');
				return false;
			}
		}
	}

	public function recover_taxonomies($mode = 'parent')
	{
		$this->autoRender = false;
		
		$this->Taxonomy->recover($mode);
	}
}
