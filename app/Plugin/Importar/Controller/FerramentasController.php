<?php
App::uses('ImportarAppController', 'Importar.Controller');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class FerramentasController extends ImportarAppController 
{
	public $name = 'Ferramentas';
	public $uses = array('Importar.Importar', 'Nodes.Node', 'Taxonomy.Term', 'Taxonomy.Taxonomy', 'NoticiumDetail', 'ModelTaxonomies','Users.User');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
			'admin_ajustar_editorias',
			'admin_gerar_termos'
		);
	}

	function trataTexto($string){
		$texto = trim(html_entity_decode(str_replace(array('<![CDATA[',']]>','','','','','','','','',''),'',$string)));
		return $texto;
	}
	

	#admin/importar/ferramentas/ajustar_editorias
	function admin_ajustar_editorias($termoId = 0, $noticiaId = 0){

		//configurações basicas
		$vocabularyId 	= 1;
		$limit 			= 100;
		$date 			= date('Y-m-d H:i:s');

		//procura os termo
		$query = "SELECT id, title, slug, id_antigo FROM terms WHERE id = $termoId AND id_antigo > 0;";
		$arrayTermos = $this->Importar->query($query);

		//se encontrou 
		if(!empty($arrayTermos)){

			//variaveis
			$termId		= $arrayTermos[0]['terms']['id'];
			$termSlug 	= $arrayTermos[0]['terms']['slug'];
			$termTitle 	= $arrayTermos[0]['terms']['title'];
			$termsTxt 	= '{"'.$termId.'":"'.$termSlug.'"}';
			$idAntigo	= $arrayTermos[0]['terms']['id_antigo'];			

			//procura o id da EDITORIA
			$this->Importar->useDbConfig = 'bp';
			$query = "SELECT editoria_id FROM editoria WHERE tit = '$termTitle';";
			$arrayEditoria = $this->Importar->query($query);
			if(!empty($arrayEditoria)){
				$editoriaId = $arrayEditoria[0]['editoria']['editoria_id'];
				//trecho da query
				$editoriaAntiga = 'editoria_id = '.$editoriaId; //Referente ao site antigo	
			}else{
				//procura o id da SUBEDITORIA
				$query = "SELECT subeditoria_id FROM subeditoria WHERE tit = '$termTitle';";
				$arrayEditoria = $this->Importar->query($query);
				//se encntrou a editoria
				if(!empty($arrayEditoria)){
					$editoriaId = $arrayEditoria[0]['subeditoria']['subeditoria_id'];
					//trecho da query
					$editoriaAntiga = 'subeditoria_id = '.$editoriaId; //Referente ao site antigo	
				}
			}	
			
			//VOlta para o banco novo
			$this->Importar->useDbConfig = 'default';

			//procura o TAXONOMIES		
			$query = "SELECT id FROM taxonomies WHERE term_id = $termId;";
			$taxonomies = $this->Importar->query($query);
			//se não encontrou
			if(empty($taxonomies)){
				//cria um taxononomies TAXONOMIES
				$query = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termId,$vocabularyId,0,0);";
				$this->Importar->query($query);
				//pega o ID inserido
				$ultimoId = $this->Importar->query('SELECT MAX(id) AS ultimo_id FROM taxonomies');
				$taxonomiesId = $ultimoId[0][0]['ultimo_id'];
				$this->log('MSWI: TermID: '.$termId.' - ID Inserido: '.$ultimoId.' - TacID: '.$taxonomiesId);
			}else{
				//pega o ID do termo
				$taxonomiesId = $taxonomies[0]['taxonomies']['id'];
			}
			
			//PRONTO, JÁ TENHO O termId e taxonomiesId
			//AGORA BUSCA AS NOTÍCIAS...

			//Alterar o banco de dados
			$this->Importar->useDbConfig = 'bp';

			$query = "
				SELECT noticia_id FROM noticia 
				WHERE $editoriaAntiga AND noticia_id > $noticiaId 
				ORDER BY noticia_id ASC LIMIT $limit;
			";
			$noticiasAntigas = $this->Importar->query($query);
			
			//Alterar o banco de dados
			$this->Importar->useDbConfig = 'default';

			//se encontrou alguma coisa
			if(!empty($noticiasAntigas)){			
				//percorre cada uma
				foreach($noticiasAntigas as $noticia){
					//pega o ultimo ID processado
					$idAntigo = $noticia['noticia']['noticia_id'];
					//procura pelo novo ID da materia
					$query = "SELECT id, id_antigo FROM nodes WHERE id_antigo = $idAntigo AND type = 'noticia';";
					$arrayNodeNovo = $this->Importar->query($query);
					//se não encontrou
					if(empty($arrayNodeNovo)){
						continue;//pula este laço
					}
					//pega o ID do node novo
					$idNovo = $arrayNodeNovo[0]['nodes']['id'];

					//UPDATE
					$updateQuery = "UPDATE nodes SET terms = '$termsTxt' WHERE id_antigo = $idAntigo AND type = 'noticia';";
					//pr($idNovo);exit();
					$this->Importar->query($updateQuery);
					//DELETE
					$deleteQuery = "DELETE FROM model_taxonomies WHERE foreign_key = $idNovo;";
					$this->Importar->query($deleteQuery);
					//INSERT
					$deleteQuery = "INSERT INTO model_taxonomies (model,foreign_key,taxonomy_id) VALUES ('Node',$idNovo,$taxonomiesId);";
					$this->Importar->query($deleteQuery);
					
				}
			}else{
				//zerar para procurar pelo proximo termo
				$idAntigo = 0;
			}

		//se não achou o ID do termo informado
		}else{
			//pr('aqui');exit();
			if(isset($termId) && $termId == 500){
				$idAntigo = 0;
				$termoId = 0;
				$termTitle = 'Nenhum';
			}else{
				$idAntigo = $noticiaId;
				$termTitle = 'Nenhum';
			}
			
		}

		$this->set('ultimo_id',$idAntigo);
		$this->set('termoId',$termoId);	
		$this->set('termTitle',$termTitle);		
		
	}


	function admin_gerar_termos(){
		//$this->autoRender = false;
		$vocabularyId = 1;
		$date = date('Y-m-d H:i:s');

		$this->Importar->useDbConfig = 'bp';
		//procura as editorias antigoas
		$query = "SELECT subeditoria_id, tit, editoria_id FROM subeditoria GROUP BY tit ORDER BY tit ASC;";
		$editorias = $this->Importar->query($query);

		$this->Importar->useDbConfig = 'default';

		//se enconturo alguma coisa
		if(!empty($editorias)){
			//percorre elas
			foreach($editorias as $editoria){
				$editoria_id = $editoria['subeditoria']['editoria_id'];
				$subeditoria_id = $editoria['subeditoria']['subeditoria_id'];
				$titulo = $editoria['subeditoria']['tit'];
				$slug = str_replace('_', '-', strtolower(Inflector::slug($titulo)));

				//procura o TERMO		
				$query = "SELECT id FROM terms WHERE slug = '$slug';";
				$term = $this->Importar->query($query);
				//se não encontrou o termo
				if(empty($term)){
					
					//cria um novo TERMO
					$query = "INSERT INTO terms (title, slug, created, updated, id_antigo, id_pai_antigo) VALUES ('$titulo','$slug', '$date', '$date', $subeditoria_id, $editoria_id);";
					//salva
					$this->Importar->query($query);
					//pega o ID inserido
					$ultimoId = $this->Importar->query('SELECT MAX(id) AS ultimo_id FROM terms');
					$termId = $ultimoId[0][0]['ultimo_id'];
				}else{
					//pega o ID do termo
					$termId = $term[0]['terms']['id'];
					//atualiza o termo
					$query = "UPDATE terms SET id_antigo = $editoria_id WHERE id = $termId;";
					//salva
					$this->Importar->query($query);					
				}

				//procura o TAXONOMIES		
				/*$query = "SELECT id FROM taxonomies WHERE term_id = $termId;";
				$taxonomies = $this->Importar->query($query);
				//se não encontrou
				if(empty($taxonomies)){
					//cria um taxononomies TAXONOMIES
					$query = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termId,$vocabularyId,0,0);";
					$this->Importar->query($query);
					//pega o ID inserido
					$ultimoId = $this->Importar->query('SELECT MAX(id) AS ultimo_id FROM taxonomies');
					$taxonomiesId = $ultimoId[0][0]['ultimo_id'];
				}else{
					//pega o ID do termo
					$taxonomiesId = $taxonomies[0]['taxonomies']['id'];
				}*/
			}

		}
		$idAntigo = 0;
		$this->set('ultimo_id',$idAntigo);
	}

}
