<?php
class ImportarActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        $controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Importar.endereco',
			'value' =>       '',
			'title' =>       'Endereço FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Importar.usuario',
			'value' =>       '',
			'title' =>       'Usuário para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Importar.senha',
			'value' =>       '',
			'title' =>       'Senha para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Importar.diretorio',
			'value' =>       '',
			'title' =>       'Diretorio para buscar os XMLS',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      256,
			'params' =>      '',
		));
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Redirect');
	}
}