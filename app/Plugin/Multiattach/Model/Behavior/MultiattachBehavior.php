<?php

App::uses('ModelBehavior', 'Model');

/**
 * Multiattach Behavior
 *
 * PHP version 5
 *
 * @category Behavior
 * @author   Elias Coronado <coso.del.cosito@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://github.com/ecoreng
 */
class MultiattachBehavior extends ModelBehavior {

/**
 * Setup
 *
 * @param Model $model
 * @param array $config
 * @return void
 */
	public function setup(Model $model, $config = array()) {
		if (is_string($config)) {
			$config = array($config);
		}

		$this->settings[$model->alias] = $config;
	}

/**
 * afterFind callback
 *
 * @param Model $model
 * @param array $results
 * @param boolean $primary
 * @return array
 */
	public function afterFind(Model $model, $results = array(), $primary = false) {

		//se foi setado
		if(isset($model->vincularMultiattach)){
			//se foi setado para não trazer nada
			if($model->vincularMultiattach == 0){
				return $results;
			}else{
				$limit = $model->vincularMultiattach;
			}
		}

		if ($primary && isset($results[0][$model->alias])) {
			$model->bindModel(
				array('hasMany' => array(
						'Multiattach' => array(
							'className' => 'Multiattach.Multiattach',
							'foreignKey' => 'node_id'
						)
					)
				)
			);
			$model->Multiattach->recursive = -1;
			
			foreach ($results as $i => $result) {				
				if (isset($results[$i][$model->alias]['id'])) {
					//Prepara o find
					$arrayFind = array();
					$arrayFind['conditions'] = array(
						'Multiattach.node_id' => $results[$i][$model->alias]['id']
					);
					$arrayFind['order'] = array(
						'Multiattach.order ASC, Multiattach.id ASC'
					);
					//se tem limite
					if(isset($limit) && !empty($limit)){
						$arrayFind['limit'] = $limit; 
					}
					$results[$i]['Multiattach'] = $model->Multiattach->find(
						'all', 
						$arrayFind
					);
				}
			}
		}
		return $results;
	}
}
