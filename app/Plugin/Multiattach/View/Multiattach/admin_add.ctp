<?php
//se é imagem
if(isset($this->params->query['imagem'])){
    echo $this->Html->css('Multiattach.slim.min.css', null, array('inline' => false));
    echo $this->Form->create(
        'Multiattach', 
        array(
            'type' => 'file'
        )
    );
    ?>
        <label for="uploads[]">Selecione uma imagem.</label>
        <div class="slim"
            data-label="Arraste a imagem aqui, ou clique aqui para selecionar"
            data-ratio="free">
            <input name='uploads[]' type="file" required multiple>
        </div>
    <?php 
    echo $this->Form->input('step',array('value'=>'1','type'=>'hidden'));
    echo $this->Form->button('Ok, enviar foto!', array('type' => 'submit','class'=>'btn-success', 'style'=>'margin-top:5px;'));
    echo $this->Form->end();
    $this->Html->script('/multiattach/js/slim.kickstart.min.js', false);

//Se é Arquivo    
}else{
    echo $this->Form->create('Multiattach', array('type' => 'file'));
?>
    <label for="uploads[]">Selecione os arquivos que serão anexados.</label>
        <input name='uploads[]' type="file" required multiple>
        <div></div>
    <?php 
    echo $this->Form->input('step',array('value'=>'1','type'=>'hidden'));
    echo $this->Form->button('Enviar', array('type' => 'submit','class'=>'btn btn-success'));
    echo $this->Form->end();
}
?>