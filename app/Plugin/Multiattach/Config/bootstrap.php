<?php

// Pega os Tipos de Conteúdo do CMSWI, e verifica se a aba Anexos está ativo para aquele Tipo de Conteúdo
$Type = ClassRegistry::init('Taxonomy.Type');
$typeDefs = $Type->find('all', array(
	'recursive' => -1,
	'fields' => array(
		'Type.alias',
		'Type.params'
	)
));

$types = array();
foreach($typeDefs as $typeDef) {
	$params = $typeDef['Params'];

	// If para tratar só a exceção que não é para aparecer, por padrão aparece sem precisar informar
	if ((isset($params['attach']) && $params['attach']) || !isset($params['attach'])) {
		$types[] = $typeDef['Type']['alias'];
	}
}

Croogo::hookRoutes('Multiattach');
Croogo::hookComponent('Nodes', 'Multiattach.Multiattaches');
Croogo::hookBehavior('Node', 'Multiattach.Multiattach', array());

Croogo::hookAdminTab('Nodes/admin_add', 'Attachments', 'Multiattach.admin_tab_node', array('type' => $types));
Croogo::hookAdminTab('Nodes/admin_edit', 'Attachments', 'Multiattach.admin_tab_node', array('type' => $types));

CroogoNav::add('settings.children.multiattach',array(
	'title' => __('Multiattach'),
	'url' => array('plugin' => 'Multiattach', 'controller' => 'Multiattach', 'action' => 'settings'),
	'access' => array('admin')
));