<header class="o-header">
      <span class="o-header_title o-header_title--small">Vídeos do Canal</span>
</header>
<?php 

$results = Cache::read('videos_youtube_home');
if(empty($results)){
	App::uses('HttpSocket', 'Network/Http');

	$API_key    = Configure::read('Youtube.api_key');
	$maxResults = Configure::read('Youtube.max_lista');
	$channelID = Configure::read('Youtube.id_canal');

	$HttpSocket = new HttpSocket();

	$results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
		'order' => 'date',
		'part' => 'snippet',
		'channelId' => $channelID,
		'maxResults' => $maxResults,
		'key' => $API_key
	));
	$results = json_decode($results->body);
	Cache::write('videos_youtube_home', $results); 
}

	if(!empty($results)){
		foreach($results->items as $item){
			if(isset($item->id->videoId)){
				echo '<div class="youtube-video">
						<iframe width="280" height="150" src="https://www.youtube.com/embed/'.$item->id->videoId.'" frameborder="0" allowfullscreen></iframe>
						<h5>'. $item->snippet->title .'</h5>
					</div>';
			}
		}
	}
?>