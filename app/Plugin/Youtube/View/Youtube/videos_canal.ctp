
<header class="o-header">
      <span class="o-header_title o-header_title--large">Vídeos do Canal</span>
</header>

<?php 
	if(!empty($results)){
        echo "<div class='row'>";
		foreach($results->items as $item){
			if(isset($item->id->videoId)){
				echo '<div class="col-lg-4">
						<iframe width="280" height="150" src="https://www.youtube.com/embed/'.$item->id->videoId.'" frameborder="0" allowfullscreen></iframe>
					</div>';
			}
        }
        echo "</div>";
	}
?>