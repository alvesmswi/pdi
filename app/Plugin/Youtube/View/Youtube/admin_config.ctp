<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Youtube', array(
    'admin' => true,
    'plugin' => 'Youtube',
    'controller' => 'Youtube',
    'action' => 'config',
  ));

$this->append('form-start', $this->Form->create('Youtube', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
       echo $this->Form->input('Youtube.id_canal', array(
            'label' => 'ID do Canal',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Youtube.api_key', array(
            'label' => 'API key',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Youtube.max_lista', array(
            'label' => 'Maximo de vídeos',
            'type'  => 'number',
            'required'	=> true
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());
