<?php
App::uses('YoutubeAppController', 'Youtube.Controller');

class YoutubeController extends YoutubeAppController {
    public $name = 'Youtube';
    public $uses = array('Youtube.Youtube', 'Settings.Setting');
    public $components = array('Cookie');

	public function beforeFilter() 
	{
        parent::beforeFilter();
        
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions[] = 'admin_config';
        
        $this->Auth->allow('videos_canal', 'live');
	}

	public function admin_config() 
	{
        $this->set('title_for_layout','Youtube');     
        if (!empty($this->request->data)) {
        if($this->Setting->deleteAll(
            array('Setting.key LIKE' => 'Youtube.%')
            )){
                foreach($this->request->data['Youtube'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Youtube.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        } 

        //limpa o this->data
        $this->request->data = array();
        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Youtube.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $this->request->data['Youtube'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
    }
    
    public function videos_canal() 
	{
        $results = Cache::read('videos_youtube');
        if(empty($results)){
            App::uses('HttpSocket', 'Network/Http');

            $API_key   = Configure::read('Youtube.api_key');
            $channelID = Configure::read('Youtube.id_canal');

            $HttpSocket = new HttpSocket();    

            if(!empty($channelID)){
                $results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
                    'order' => 'date',
                    'part' => 'snippet',
                    'channelId' => $channelID,
                    'maxResults' => 21,
                    'key' => $API_key
                ));
            }
            $results = json_decode($results->body);
            Cache::write('videos_youtube', $results);    
        }
       
        $this->set('results', $results);

    }  

    public function live()
    {
        $this->autoRender = false;
        
        App::uses('HttpSocket', 'Network/Http');

        $API_key   = Configure::read('Youtube.api_key');
        $channelID = Configure::read('Youtube.id_canal');

        if (!empty($API_key) && !empty($channelID)) {
            $HttpSocket = new HttpSocket();

            $results = $HttpSocket->get('https://www.googleapis.com/youtube/v3/search', array(
                'part' => 'id',
                'channelId' => $channelID,
                'eventType' => 'live',
                'type' => 'video',
                'maxResults' => 1,
                'order' => 'date',
                'key' => $API_key
            ));

            $body = json_decode($results->body);

            return json_encode($body->items);
        } else {
            return "Necessário informar API Key e/ou Channel ID";
        }
    }
}