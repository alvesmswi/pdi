<?php
/**
 * Admin menu (navigation)
 */
CroogoNav::add('settings.children.youtube', array(
	'icon' => array('play', 'large'),
	'title' => 'Youtube',
	'children' => array(
		'create' => array(
			'title' => 'Configurações',
			'url' => array(
				'admin' => true,
				'plugin' => 'youtube',
				'controller' => 'youtube',
				'action' => 'config'
			)
		)
	)
));
