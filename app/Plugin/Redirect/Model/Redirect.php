<?php
App::uses('AppModel', 'Model');
App::uses('String', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

  class Redirect extends AppModel
  {
    public $useTable 	= false;
    private $_routeCode = "CroogoRouter::connect(':url', array('plugin'=>'Redirect', 'controller'=>'Redirect', 'action'=>'go', 'forward'=>':forward', 'code'=>':code'));";
    private $_routeFile = null;

    public function __construct()
    {
      	$this->_routeFile = APP . 'Config' . DS . 'custom_routes.php';
      	parent::__construct();
    }
	
	public function localizarSlug($url, $buscar = false){
		$this->autoRender = false;
		//pagina 404
		$pagina404 = '/redirect/redirect/pagina404';
		//trata a URL
		$arrayUrl = explode('/', $url);
		//se já foi configurado o Redirect
		if(Configure::read('Redirect')){
			$redConf = Configure::read('Redirect');
		}else{
			$redConf = array(
				'key_type' 	=> 2,
				'key_slug' 	=> 3,
				'key_id'	=> 4
			);
		}
		//define as variaveis
		$type 		= $redConf['key_type'];
		$slug 		= $redConf['key_slug'];
		$idAntigo 	= $redConf['key_id'];

		//Se existe o slug e o id antigo
		if((isset($arrayUrl[$slug]) && !empty($arrayUrl[$slug]))){
			//Monta a query
			if(isset($arrayUrl[$idAntigo]) && !empty($arrayUrl[$idAntigo])){
				$query = "SELECT type, slug  FROM nodes WHERE slug = '".$arrayUrl[$slug]."' OR id_antigo = '".$arrayUrl[$idAntigo]."' LIMIT 1;";
			}else{
				$query = "SELECT type, slug  FROM nodes WHERE slug = '".$arrayUrl[$slug]."' LIMIT 1;";
			}	
			
			//se é uma editoria
			if($arrayUrl[0] == 'subeditoria'){
				$query = "SELECT slug  FROM terms WHERE slug = '".$arrayUrl[1] ."' LIMIT 1;";	
				$editoria = true;
			}
			//procura o node
			$result = $this->query($query);
			//Se encontrou o node
			if(!empty($result)){				

				//se éuma editoria
				if(isset($editoria)){
					$slug = '/noticia/term/'.$result[0]['terms']['slug'];
				}else{
					$slug = '/'.$result[0]['nodes']['type'].'/'.$result[0]['nodes']['slug'];
				}
			}else{
				$pagina404 .= '?q=';
				$total = count($arrayUrl);
				$i = 0;
				//trata a busca
				foreach($arrayUrl as $q){
					$i++;
					if($i < $total){
						$pagina404 .= $q.'+';
					}else{
						$pagina404 .= $q;
					}
				}
				$slug = $pagina404;
			}
		//Se não está no padrão de URL
		}else{
			$pagina404 .= '?q=';
			$total = count($arrayUrl);
			$i = 0;
			//trata a busca
			foreach($arrayUrl as $q){
				$i++;
				if($i < $total){
					$pagina404 .= $q.'+';
				}else{
					$pagina404 .= $q;
				}
			}
			$slug = $pagina404;
		}

		return $slug;
	}

    public function procurar(){
      	$results = array();
      	$router = Router::$routes;
	  
      	foreach($router as $route){
	        if(isset($route->defaults['plugin']) && $route->defaults['plugin'] == 'Redirect' && $route->defaults['controller'] == 'Redirect' && $route->defaults['action'] == 'go'){
	         	 $results[] = array(
	            	'url' => $route->template,
	            	'forward' => $route->defaults['forward'],
	            	'code' => $route->defaults['code']
	          	);
	        }
  		}
      
      	return $results;
    }

    public function gravar()
    {
      	$output = '';
  		foreach($this->data['Route'] as $route){
			$output .= String::insert($this->_routeCode, $route)."\n";
      	}
      	$output = "<?php\n\n".$output."\n?>";
      	$file = new File($this->_routeFile);
      	return $file->write($output,'w',true);
    }  
}
?>