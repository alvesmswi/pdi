<?php
  $uuid = String::uuid();
  $url = isset($data['url']) ? $data['url'] : null;
  $forward = isset($data['forward']) ? $data['forward'] : null;
  $status = isset($data['code']) ? $data['code'] : '301';
?>
<tr class="route">
	<td>
		<?php 
		echo $this->Form->input(
			'Route.'.$uuid.'.url',
			array(
				'type'=>'text',
				'label'=>false, 
				'placeholder'=>'URL alteranativa',
				'div'=>'input text url',
				'value'=>$url,
				'style' => 'width: 450px;'
			)
		); ?>
	</td>
	<td>
		<?php 
		echo $this->Form->input(
			'Route.'.$uuid.'.forward',
			array(
				'type'=>'text',
				'label'=>false, 
				'placeholder'=>'Direcionar para',
				'div'=>'input text forward',
				'value'=>$forward,
				'style' => 'width: 450px;'
			)
		); ?>
	</td>
	<td>
		<?php 
		echo $this->Form->input(
			'Route.'.$uuid.'.code',
			array(
				'type'=>'text',
				'label'=>false, 
				'placeholder'=>'Código',
				'div'=>'input text code',
				'value'=>$status,
				'style' => 'width: 40px;',
				'maxlength' => '3'
			)
		); ?>
	</td>
	<td  style="text-align:center">
	    <?php echo $this->Html->link('X','#',array('class'=>'btn btn-danger remove-route', 'title'=>'Remover')); ?>
	</td>
</tr>
