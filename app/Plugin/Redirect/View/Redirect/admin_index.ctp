<?php
$this->Html->css('Redirect.redirect', null, array('inline' => false));
$this->Html->script('Redirect.redirect', false);
?>

<h2>URLs Alternativas</h2>

<?php
    echo $this->Form->create('Redirect',array('url'=>$this->here));
?>

<div id="route-fields">
    
</div>
<div class="buttons">
    <a class="add-redirect btn btn-info" href="#">Adicionar outro endereço</a>
    <?php echo $this->Form->submit('Salvar', array('class'=>'btn btn-primary', 'div'=>false, 'style'=>'margin-right:10px;')); ?>
</div>
<br />

<table class="table table-striped">
    <thead>
    	<tr>
    		<th><b>URL alternativa</b></th>
    		<th><b>Redirecionar Para</b></th>
    		<th><b>Código</b></th>
    		<th><b>Remover</b></th>
    	</tr>
    </thead>
    <tbody>
    	<tr>
    		<?php
        	foreach($routes as $route){
          		echo $this->element('field',array('data'=>$route,'plugin'=>'Redirect'));
        	}?> 
    	</tr>
    </tbody>
</table>
	
<?php
  echo $this->Form->input('token_key', array(
      'type' => 'hidden',
      'value' => $this->params['_Token']['key'],
  ));
  echo $this->Form->end();
?>