<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb(__d('croogo', 'Redirect'), array(
    'admin' => true,
    'plugin' => 'redirect',
    'controller' => 'redirect',
    'action' => 'index',
  ));

if ($this->request->params['action'] == 'admin_edit') {
    $this->Html->addCrumb($this->request->data['Setting']['key'], '/' . $this->request->url);
}

if ($this->request->params['action'] == 'admin_add') {
    $this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Setting', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configuração', '#setting-basic');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('setting-basic') .
        $this->Form->input('Redirect.key_type', array(
            'label' => 'Indice do "type"',
            //'help'  => 'Por exemplo: exemplo/tipo/titulo/id',
            'type'  => 'number'
        )) .
        $this->Form->input('Redirect.key_slug', array(
            'label' => 'Indice do "slug"',
            'type'  => 'number'
        )) .
        $this->Form->input('Redirect.key_id', array(
            'label' => 'Indice do "ID"',
            'type'  => 'number'
        )) .
    $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());