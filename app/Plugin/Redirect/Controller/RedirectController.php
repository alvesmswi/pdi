<?php
App::uses('RedirectAppController', 'Redirect.Controller');

  class RedirectController extends RedirectAppController {

    public $name = 'Redirect';
    public $uses = array('Redirect.Redirect', 'Settings.Setting');

    public function beforeFilter() {
      	parent::beforeFilter();
		$this->Auth->allow('pagina404');
        // CSRF Protection
        if (in_array($this->params['action'], array('admin_index'))) {
            $this->Security->validatePost = false;
        }
    }

    public function admin_index()
    {
        $this->set('title_for_layout','URLs Alternativas');      
        if(!empty($this->data)) {        
            $this->Redirect->set($this->data);        
            if($this->Redirect->gravar()) {
                $this->Session->setFlash('As rotas foram salvas com sucesso!', 'default', array('class' => 'success'));
                $this->redirect($this->here);
            }else{
                $this->Session->setFlash('Ocorreu um problema ao salvar as rotas.', 'default', array('class' => 'error'));
            }
        }      
        $routes = $this->Redirect->procurar();
        $this->set(compact('routes'));
    }

    public function admin_add_field()
    {
        
    }

    public function admin_404()
    {
        $this->set('title_for_layout', 'Configuração de URL antiga');

        if (!empty($this->request->data)) {
            if($this->Setting->deleteAll(
                array('Setting.key LIKE' => 'Redirect.%')
            )){
                foreach($this->request->data['Redirect'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Redirect.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        }else{
           $arrayDados = $this->Setting->find(
                'all',
                array( 'conditions' => array('key LIKE' => 'Redirect.%'),'recursive' => -1)
            );

            if(!empty($arrayDados)){
                foreach($arrayDados as $dados){
                     $this->request->data['Redirect'][$dados['Setting']['title']] = $dados['Setting']['value'];
                }
            }
        }
    }

    public function go()
    {
        $this->redirect($this->params['forward'], $this->params['code']);
    }
	
	public function pagina404(){
        $this->response->statusCode(404);
	}
	
}
?>