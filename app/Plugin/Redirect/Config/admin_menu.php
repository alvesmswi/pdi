<?php
CroogoNav::add('sidebar', 'settings.children.redirect', array(
	'icon' => 'link',
	'title' => 'Redirect',
	'url' => array(
		'admin' => true,
		'plugin' => 'redirect',
		'controller' => 'redirect',
		'action' => 'index',
	),
	'weight' => 40,
	'children' => array(
		'index' => array(
			'title' => 'URLs Alternativas',
			'url' => array(
				'admin' => true,
				'plugin' => 'redirect',
				'controller' => 'redirect',
				'action' => 'index'
			)
		),
		'404' => array(
			'title' =>'404',
			'url' => array(
				'admin' => true,
				'plugin' => 'redirect',
				'controller' => 'redirect',
				'action' => '404'
			)
		)
	)
));