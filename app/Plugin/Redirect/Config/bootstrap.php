<?php
Croogo::hookRoutes('redirect');

CroogoNav::add('settings.children.redirect',array(
	'title' => __('Redirect'),
	'url' => array('plugin' => 'redirect', 'controller' => 'redirect', 'action' => 'admin_index'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Redirect');
    
?>