<?php
class RedirectActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        // ACL: set ACOs with permissions
        $controller->Croogo->addAco('redirect');
        $controller->Croogo->addAco('redirect/redirect/admin_index');
        $controller->Croogo->addAco('redirect/redirect/admin_add_field');
        $controller->Croogo->addAco('redirect/redirect/go', array('registered', 'public'));
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Redirect');
	}
}