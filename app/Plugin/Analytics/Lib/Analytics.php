<?php
/**
 * 1. create project at https://console.developers.google.com/project
 * 2. enable 'Analytics API' under 'APIs & auth' / APIs
 * 3. create 'NEW CLIENT ID' (OAuth client) under 'APIs & auth' / Credentials
 *    i.  select 'Service account'
 *    ii. save generated key file to 'key.p12'
 *    iii. remember CLIENT ID
 * 4. under GA account add 'Read & Analyze' access to newly generated email (access to GA Account not Property nor View)
 * 5. get View ID. go to GA Admin section, select proper Account, than proper Property, than proper View.
 *     Under View click on 'View settings' and copy the number below 'View ID' (that is your GA_VIEW_ID number)
 * 5. download google php library https://github.com/google/google-api-php-client
 * 6. use the code below, use info from google API console (1.)
 *    doc here: https://developers.google.com/analytics/devguides/reporting/realtime/v3/reference/data/realtime/get 
 *    real time metrics doc: https://developers.google.com/analytics/devguides/reporting/realtime/dimsmets/
 */

//header("Refresh:60");//60 segundos

include 'Google/autoload.php';

class Analytics {
	
    static public function initialize(){
		$certificado = App::pluginPath('Analytics') .'Lib' . DS . '35cddaf286e5.p12';
        $client = new Google_Client();
        $client->setClientId('904936451047-ut730nia3irvv148u9bb844ulu2g5geb.apps.googleusercontent.com');
        $client->setAssertionCredentials(
            new Google_Auth_AssertionCredentials(
                'api-jornais@ga-jornais.iam.gserviceaccount.com',
                array('https://www.googleapis.com/auth/analytics.readonly'),
                file_get_contents($certificado)
            )
        );
        $analytics = new Google_Service_Analytics($client);
       	return $analytics;
    }

    public static function visitantes($analytics, $GA_VIEW_ID=null){  
		//se ainda não foi configurado o ViewID
		if(!Configure::read('Analytic.viewId')){
			return false;
		}
		//se NAO foi informado o $GA_VIEW_ID 
		if(is_null($GA_VIEW_ID)){
			//pega da configuração
			$GA_VIEW_ID = Configure::read('Analytic.viewId');  
		}		 
        $result = $analytics->data_realtime->get(
            'ga:'.$GA_VIEW_ID,
            'rt:activeVisitors'
        );
        return $result->totalsForAllResults['rt:activeVisitors'];
	}
	
    static public function pageviews($analytics){
		//se ainda não foi configurado o ViewID
		if(!Configure::read('Analytic.viewId')){
			return false;
		}
        $GA_VIEW_ID = Configure::read('Analytic.viewId');  
        $result = array();
        for ($j = 5; $j >= 0; $j--){
            $start_date = date('Y-m', strtotime("-$j month")) . '-01';
            $d = new DateTime($start_date);
            $end_date = $d->format('Y-m-t');
			//ORGANICO
			$optParams = array(
				'dimensions'=>'ga:source',
				'filters'=>'ga:medium==organic',
				'metrics'=>'ga:sessions'
			);
			$result['organic'][$j] = $analytics->data_ga->get(
				'ga:' . $GA_VIEW_ID,
				$start_date,
				$end_date,
				'ga:sessions',
				$optParams
			);
			//NÃO ORGANICO
			$optParams = array(
				'dimensions'=>'ga:source',
				'filters'=>'ga:medium!=organic',
				'metrics'=>'ga:sessions'
			);
			$result['nonorganic'][$j] = $analytics->data_ga->get(
				'ga:' . $GA_VIEW_ID,
				$start_date,
				$end_date,
				'ga:sessions',
				$optParams
			);
        }
        return $result;
    }

	static public function mostvisitedsTable($analytics){	
		//se ainda não foi configurado o ViewID
		if(!Configure::read('Analytic.viewId')){
			return false;
		}
		$GA_VIEW_ID = Configure::read('Analytic.viewId');  
		$optParams = array(
			'dimensions'=>'ga:pagePath',
			'filters'=>'ga:pagePathLevel1!=/',
			'metrics'=>'ga:pageviews,ga:uniquePageviews,ga:timeOnPage,ga:bounces,ga:entrances,ga:exits',
			'sort'=>'-ga:pageviews',
			'max-results' => 10
		);
		return $analytics->data_ga->get(
			'ga:' . $GA_VIEW_ID,
			'7daysAgo',
			'yesterday',
			'ga:sessions',
			$optParams
		);
	}
 }
?>