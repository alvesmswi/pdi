<?php 
//se tem registros
if(!empty($mostvisitedsTable)){
	?>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<th>Endereço</th>
			<th>Views</th>
		</thead>
		<tbody>
		<?php
		//percorre os registros
		foreach($mostvisitedsTable->rows as $registro){ 
		?>
			<tr>
				<td><?php echo $registro[0]; ?></td>
				<td><?php echo $registro[1]; ?></td>
			</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?php
}
?>