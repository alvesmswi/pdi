<?php header("Refresh:60");//60 segundos?>
<script type="text/javascript" src="/analytics/js/highcharts.js"></script>
<script type="text/javascript" src="/analytics/js/highcharts-more.js"></script>
<script type="text/javascript" src="/analytics/js/solid-gauge.js"></script>	

<div id="visitantes"></div>

<script>
var gaugeOptions = {
	chart: {
		type: 'solidgauge'
	},
	title: null,
	pane: {
		center: ['50%', '60%'],
		size: '100%',
		startAngle: -90,
		endAngle: 90,
		background: {
			innerRadius: '60%',
			outerRadius: '100%',
			shape: 'arc'
		}
	},
	tooltip: {
		enabled: false
	},
	yAxis: {
		stops: [
			[0.1, '#55BF3B'], // green
			[0.5, '#DDDF0D'], // yellow
			[0.9, '#DF5353'] // red
		],
		lineWidth: 0,
		minorTickInterval: null,
		tickAmount: 2,
		title: {
			y: -70
		},
		labels: {
			y: 16
		}
	},
	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: 5,
				borderWidth: 0,
				useHTML: true
			}
		}
	}
};

//VISITANTES
Highcharts.chart('visitantes', Highcharts.merge(gaugeOptions, {
	yAxis: {
		min: 0,
		max: <?php echo $visitantes + 20; ?>
	},
	credits: {
		enabled: false
	},
	series: [{
		name: 'Speed',
		data: [<?php echo $visitantes; ?>],
		dataLabels: {
			format: '<div style="text-align:center"><span style="font-size:35px;color:' +
				((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
				'<span style="font-size:18px;color:silver">online</span></div>'
		},
		tooltip: {
			valueSuffix: ' online'
		}
	}]

}));
</script>