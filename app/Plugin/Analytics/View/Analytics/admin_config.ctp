<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Analytics', array(
    'admin' => true,
    'plugin' => 'analytics',
    'controller' => 'analytics',
    'action' => 'index',
  ));

$this->append('form-start', $this->Form->create('Analytic', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
    echo $this->Form->input('Analytic.viewId', array(
        'label' => 'GA View ID (Vista da propriedade)',
        'type'  => 'text',
        'required'	=> true
    ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());