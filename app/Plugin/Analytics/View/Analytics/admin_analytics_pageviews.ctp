

<script type="text/javascript" src="/analytics/js/highcharts.js"></script>
<div id="pageviews"></div>
<script>
//PAGEVIEWS
Highcharts.chart('pageviews', {
	colors: ['#4572a7', '#aa4643'],
	chart: {
		type: 'column'
	},
	title: {
		text: "<?php echo html_entity_decode('Total de visitas nos últimos 6 meses');?>"
	},
	xAxis: {
		categories: [
			<?php
			$first  = strtotime('first day this month');
			for ($j = 5; $j >=0; $j--){
				echo '"'.date("m/Y", strtotime(" -$j month", $first)).'",';
			}
			?>
		]
	},
	yAxis: {
		min: 0,
		title: {
			text: 'Total de visitas por mês'
		},
		stackLabels: {
			enabled: true,
			style: {
				fontWeight: 'bold',
				color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			}
		}
	},
	legend: {
	align: 'right',
	x: -30,
	verticalAlign: 'top',
	y: 25,
	floating: true,
	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	borderColor: '#CCC',
	borderWidth: 1,
	shadow: false
	},
	tooltip: {
	headerFormat: '<b>{point.x}</b><br/>',
	pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	},
	plotOptions: {
	column: {
		stacking: 'normal',
		dataLabels: {
		enabled: true,
		color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
		}
	}
	},
	series: [{
	name: 'Organicas',
	data: [
	<?php
	for ($j = 5; $j >=0; $j--){
		echo $pageviews['organic'][$j]['totalsForAllResults']['ga:sessions'].', ';
	}
	?>
	]
	}, {
	name: 'Nao-Organicas',
	data: [
	<?php
	for ($j = 5; $j >=0; $j--){
		echo $pageviews['nonorganic'][$j]['totalsForAllResults']['ga:sessions'].', ';
	}
	?>
	]
	}]
});
</script>