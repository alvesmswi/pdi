
<?php
$this->Html->script('Analytics.highcharts', false);
$this->Html->script('Analytics.highcharts-more', false);
$this->Html->script('Analytics.solid-gauge', false);
?>
<div class="inner-content">
	<div class="span6">
		<div id="visitantes"></div>
        <?php echo $visitantes; ?>
	</div>
	<div class="span6">
		<?php 
		//se tem registros
		if(!empty($mostvisitedsTable)){
			?>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<th>Páginas mais visitadas nos últimos 7 dias</th>
					<th>Views</th>
				</thead>
				<tbody>
				<?php
				//percorre os registros
				foreach($mostvisitedsTable->rows as $registro){ 
				?>
					<tr>
						<td><?php echo $registro[0]; ?></td>
						<td><?php echo $registro[1]; ?></td>
					</tr>
				<?php
				}
				?>
				</tbody>
			</table>
			<?php
		}
		?>	
	</div>
</div>
<div class="inner-content">	
	<div class="span12">
		<div id="pageviews"></div>
	</div>
</div>
<script>
var gaugeOptions = {
	chart: {
		type: 'solidgauge'
	},
	title: null,
	pane: {
		center: ['50%', '60%'],
		size: '100%',
		startAngle: -90,
		endAngle: 90,
		background: {
			innerRadius: '60%',
			outerRadius: '100%',
			shape: 'arc'
		}
	},
	tooltip: {
		enabled: false
	},
	yAxis: {
		stops: [
			[0.1, '#55BF3B'], // green
			[0.5, '#DDDF0D'], // yellow
			[0.9, '#DF5353'] // red
		],
		lineWidth: 0,
		minorTickInterval: null,
		tickAmount: 2,
		title: {
			y: -70
		},
		labels: {
			y: 16
		}
	},
	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: 5,
				borderWidth: 0,
				useHTML: true
			}
		}
	}
};

//VISITANTES
Highcharts.chart('visitantes', Highcharts.merge(gaugeOptions, {
	yAxis: {
		min: 0,
		max: <?php echo $visitantes + 20; ?>
	},
	credits: {
		enabled: false
	},
	series: [{
		name: 'Speed',
		data: [<?php echo $visitantes; ?>],
		dataLabels: {
			format: '<div style="text-align:center"><span style="font-size:35px;color:' +
				((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
				'<span style="font-size:18px;color:silver">online</span></div>'
		},
		tooltip: {
			valueSuffix: ' online'
		}
	}]

}));

//PAGEVIEWS
Highcharts.chart('pageviews', {
	colors: ['#4572a7', '#aa4643'],
	chart: {
		type: 'column'
	},
	title: {
		text: "<?php echo html_entity_decode('Total de visitas nos últimos 6 meses');?>"
	},
	xAxis: {
		categories: [
			<?php
			$first  = strtotime('first day this month');
			for ($j = 5; $j >=0; $j--){
				echo '"'.date("m/Y", strtotime(" -$j month", $first)).'",';
			}
			?>
		]
	},
	yAxis: {
		min: 0,
		title: {
			text: 'Total de visitas por mês'
		},
		stackLabels: {
			enabled: true,
			style: {
				fontWeight: 'bold',
				color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			}
		}
	},
	legend: {
	align: 'right',
	x: -30,
	verticalAlign: 'top',
	y: 25,
	floating: true,
	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	borderColor: '#CCC',
	borderWidth: 1,
	shadow: false
	},
	tooltip: {
	headerFormat: '<b>{point.x}</b><br/>',
	pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	},
	plotOptions: {
	column: {
		stacking: 'normal',
		dataLabels: {
		enabled: true,
		color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
		}
	}
	},
	series: [{
	name: 'Organicas',
	data: [
	<?php
	for ($j = 5; $j >=0; $j--){
		echo $pageviews['organic'][$j]['totalsForAllResults']['ga:sessions'].', ';
	}
	?>
	]
	}, {
	name: 'Nao-Organicas',
	data: [
	<?php
	for ($j = 5; $j >=0; $j--){
		echo $pageviews['nonorganic'][$j]['totalsForAllResults']['ga:sessions'].', ';
	}
	?>
	]
	}]
});
</script>