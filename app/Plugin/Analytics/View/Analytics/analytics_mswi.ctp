<?php 
echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex'));

//Se ainda não está logado
if(!isset($arrayVisitantes)){ 
//Form de Login
?>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <div class="container">
    
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <?php 
                        //se tem mensagem pra ser exibida
                        if(isset($mensagem) && !empty($mensagem)){
                            echo '<div class="alert alert-danger" role="alert">'.$mensagem.'</div>';
                        }
                        ?>
                        <h5 class="card-title text-center">Identificação</h5>
                        <form class="form-signin" action="/jornais-mswi" method="POST">
                            <div class="form-label-group">
                                <label for="email">E-mail</label>
                                <input type="email" id="email" name="email" class="form-control" required autofocus>
                            </div>

                            <div class="form-label-group">
                                <label for="password">Senha</label>
                                <input type="password" id="password" name="password" class="form-control" required>
                            </div>

                            <div class="custom-control custom-checkbox mb-3">
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

//Se já está logado
}else{
    header("Refresh:60");//60 segundos
    ?>

    <script type="text/javascript" src="/analytics/js/highcharts.js"></script>

    <div id="container" style="min-width: 310px; max-width: 800px; height: 500px; margin: 0 auto"></div>

    <?php 
    $arrayJornais = array();
    $arrayOnline = array();
    //ordena o array
    arsort($arrayVisitantes);
    //percorre os jornais
    foreach($arrayVisitantes as $ga => $jornal){
        array_push($arrayJornais, $jornal['nome']);
        array_push($arrayOnline, $jornal['visitantes']);
    } 
    ?>

    <script>

    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Visitantes Online'
        },
        subtitle: {
            text: 'MSWI Soluções Web Inteligentes'
        },
        xAxis: {
            categories: <?php echo json_encode($arrayJornais); ?>,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Visitantes Online',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Online',
            data: <?php echo json_encode($arrayOnline, JSON_NUMERIC_CHECK); ?>
        }]
    });
    </script>

    <style>
        .sair{
            display:block;
            font-weight: bold;
            color: red;
            text-align: center;
        }
    </style>

    <a href="/jornais-mswi?sair=true" class="sair">SAIR</a>
<?php 
}
?>
