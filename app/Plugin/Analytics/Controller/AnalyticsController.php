<?php
App::uses('AnalyticsAppController', 'Analytics.Controller');
App::uses('Analytics', 'Analytics.Lib');

class AnalyticsController extends AnalyticsAppController {

	public $name = 'Analytics';
	public $uses = array('Analytics.Analytics', 'Settings.Setting');    
    //public $helpers = array('Session');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->Allow('analytics_mswi');
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
		$this->Security->authCheck = false;
		$this->Security->unlockedActions = array('analytics_mswi');
	}

	public function admin_index()
	{
		$analytics = Analytics::initialize();
		$this->set('visitantes', Analytics::visitantes($analytics));
		$this->set('mostvisitedsTable', Analytics::mostvisitedsTable($analytics));
		$this->set('pageviews', Analytics::pageviews($analytics));
	}

	public function admin_config()
	{
		$this->set('title_for_layout','Analytics');     
		
		if (!empty($this->request->data)) {
				if($this->Setting->deleteAll(
						array('Setting.key' => 'Analytic.viewId')
				)){
						$arraySave = array();
						$arraySave['id']            = NULL;
						$arraySave['editable']      = 0;
						$arraySave['input_type']    = 'text';
						$arraySave['key']           = 'Analytic.viewId';
						$arraySave['value']         = $this->data['Analytic']['viewId'];

						$this->Setting->create();
						if ($this->Setting->saveAll($arraySave)) {
								$this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
						} else {
								$this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
						}
				}
		} 

		//limpa o this->data
		$this->request->data = array();
		//Pega os dados
		$arrayDados = $this->Setting->find(
				'first',
				array( 'conditions' => array('key' => 'Analytic.viewId'),'recursive' => -1)
		);
		//se encontrou
		if(!empty($arrayDados)){
				$this->request->data['Analytic']['viewId'] = $arrayDados['Setting']['value'];
		}    
	}

	public function admin_analytics_online(){
		$this->layout = 'ajax';
		$analytics = Analytics::initialize();
		$this->set('visitantes', Analytics::visitantes($analytics));
	}

	public function admin_analytics_tops(){
		$this->layout = 'ajax';
		$analytics = Analytics::initialize();
		$this->set('mostvisitedsTable', Analytics::mostvisitedsTable($analytics));
	}

	public function admin_analytics_pageviews(){
		$this->layout = 'ajax';
		$analytics = Analytics::initialize();
		$this->set('pageviews', Analytics::pageviews($analytics));
	}

	public function analytics_mswi(){
        $this->layout = 'ajax';
        //pr($this->params->query['sair']);exit();
        //se pediu pra sair
        if(isset($this->params->query['sair'])){
            $this->Session->delete('Analytics.login');
            $this->redirect('/jornais-mswi');
        }

		//se enviou o login
		if(!empty($this->data)){
			if(
                $this->data['email'] == 'mswi@mswi.com.br' && 
                $this->data['password'] == 'Ms0705wi!'
			){
                $this->Session->write('Analytics.login', $this->data);
                //OK
			}else{
                //ERRO
                $this->set('mensagem', 'E-mail ou Senha incorreta.');
                return false;
            }
        }
        
        //Se já está logado
        if($this->Session->check('Analytics.login')){
            $analytics = Analytics::initialize();
            //lista de todos os jornais
            $jornais = array(
                '33537265'    	=> 'Diário dos Campos',
                '37824114'    	=> 'Tribuna do Interior',
                '146643567'   	=> 'Diário do Sudoeste',
                '164191809'   	=> 'Folha Extra',
                '45600677'    	=> 'Jornal do Oeste',
                '136391849'   	=> 'Tribuna de Cianorte',
                '146601181'   	=> 'O Paraná',   
                '155090692'   	=> 'Correio do Cidadão',
                '136463925'   	=> 'Agora Paraná',
                '136447786'   	=> 'Gazeta Regional',
                '20540131'    	=> 'H2Foz',
                '172815238'   	=> 'Manchete do Povo',                
                '43804551'    	=> 'JCorreio do Povo',
                '138340318'   	=> 'Tribuna Hoje',
                '177505240'   	=> 'Folha de Capanema',				
				'97706170'    	=> 'O Estafeta',
				'60147293'    	=> 'O Celeiro',
				'68811227'    	=> 'Portal Agora',
				'106172083'    	=> 'Folha de Irati'
				//'97706170'    => 'O Caminho',				
				//'97706170'    => 'O Farroupilha',
				//'183262515'   => 'Barulho Curitiba',
				//'3621751'     => 'Bem Paraná',
				//'164580173'   => 'JB Litoral',
            );
            $arrayVisitantes = array();
            foreach($jornais as $ga => $jornal){
                $arrayVisitantes[$ga]['visitantes'] = Analytics::visitantes($analytics, $ga); 
                $arrayVisitantes[$ga]['nome'] = $jornal;      
            }    
            $this->set('arrayVisitantes', $arrayVisitantes);
        }		
	}
}
?>