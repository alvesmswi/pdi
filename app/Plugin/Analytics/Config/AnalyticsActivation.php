<?php
class AnalyticsActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        //da permissões para acessar plugin
        $controller->Croogo->addAco('Analytics');
		$controller->Croogo->addAco('Analytics/Analytics/admin_index');
		$controller->Croogo->addAco('Analytics/Analytics/admin_config');
		$controller->Croogo->addAco('Analytics/Analytics/admin_analytics_online', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Analytics/Analytics/admin_analytics_tops', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Analytics/Analytics/admin_analytics_pageviews', array('editor', 'colunista', 'gerente'));
		//adiciona uma configuração basica 
		$controller->Setting->write('Analytic.viewId','158132668',array('description' => 'Vistas da propriedade do GA','editable' => 0));

		//Limpa o cache
		Cache::clear(false, '_cake_model_');

    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Analytics');
	}
}