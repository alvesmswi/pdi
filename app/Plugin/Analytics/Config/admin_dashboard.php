<?php
$config = array(
    'dashboard.analytics_online' => array(
        'title' => 'Visitantes Online',
        'element' => 'Analytics.analytics_online',
        'column' => CroogoDashboard::LEFT
    ),
    'dashboard.analytics_tops' => array(
        'title' => 'Páginas mais acessadas nos últimos 7 dias',
        'element' => 'Analytics.analytics_tops',
        'column' => CroogoDashboard::RIGHT
    ),
    /*'dashboard.analytics_pageviews' => array(
        'title' => 'Pageviews nos últimos 6 meses',
        'element' => 'Analytics.analytics_pageviews',
        'column' => CroogoDashboard::FULL
    )*/
);