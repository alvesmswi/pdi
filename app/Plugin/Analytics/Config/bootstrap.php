<?php
Croogo::hookRoutes('analytics');

CroogoNav::add('settings.children.analytics',array(
	'title' => 'Analytics',
	'url' => array('plugin' => 'analytics', 'controller' => 'analytics', 'action' => 'admin_index'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Analytics');
    
?>