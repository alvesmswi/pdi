<?php
CroogoNav::add(
	'sidebar', 
	'settings.children.analytics', 
	array(
		//'icon' => 'link',
		'title' => 'Analytics',
		'url' => array(
			'admin' => true,
			'plugin' => 'analytics',
			'controller' => 'analytics',
			'action' => 'index',
		),
		'children' => array(
			'list' => array(
				'title' => 'Dashboard',
				'url' => array(
					'admin' => true,
					'plugin' => 'analytics',
					'controller' => 'analytics',
					'action' => 'index'
				)
			),
			'create' => array(
				'title' => 'Configurações',
				'url' => array(
					'admin' => true,
					'plugin' => 'analytics',
					'controller' => 'analytics',
					'action' => 'config'
				)
			)
		)
	)
);