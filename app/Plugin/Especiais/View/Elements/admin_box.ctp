<?php 
App::uses('Especiais','Especiais.Model');
$especiais = new Especiais();
$arrayRegioes = $especiais->getGetRegions();

$especiais = $this->Mswi->query("SELECT id, title, regiao FROM especiais WHERE status = 1 ORDER BY title ASC;", 'especiais');
$arrayEspeciais = array();
foreach($especiais as $especial){
    $arrayEspeciais[$especial['especiais']['id']] = $especial['especiais']['title'].' - '.$arrayRegioes[$especial['especiais']['regiao']];
}   
echo $this->Form->hidden('EspeciaisNode.node_id');
echo $this->Form->input('EspeciaisNode.especiais_id', array(
    'label' => 'Selecione a seção especial',
    'empty' => true,
    'options' => $arrayEspeciais
));
echo $this->Form->input('EspeciaisNode.ordem', array(
    'label' => 'Posição na seção',
    'empty' => true,
    'options' => array(
        '1' => '1 (Exibe foto grande)',
        '2' => '2 (Exibe foto média)',
        '3' => '3 (Não exibe foto)',
        '4' => '4 (Exibe foto média)',
        '5' => '5 (Exibe foto média)',
        '6' => '6 (Exibe foto média)'
     )
));
?>