<?php 
$regiao = $block['Block']['alias'];
$sql = "
SELECT
    Especiais.title,
    Node.id, Node.title, Node.slug, Node.path, Node.terms, 
    NoticiumDetail.chapeu, NoticiumDetail.titulo_capa,
    Multiattach.filename, Multiattach.meta,
    EspeciaisNode.ordem 
FROM especiais AS Especiais
INNER JOIN especiais_nodes AS EspeciaisNode ON(EspeciaisNode.especiais_id = Especiais.id)
INNER JOIN nodes AS Node ON(Node.id = EspeciaisNode.node_id)
LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
WHERE Especiais.regiao = '$regiao' AND Especiais.status = 1 AND Node.status = 1 AND EspeciaisNode.ordem > 0 
GROUP BY Node.id 
ORDER BY EspeciaisNode.ordem ASC 
LIMIT 6
";
$resultsQuery = $this->Mswi->query($sql,$regiao);

//se tem resultado
if(!empty($resultsQuery)){
    $result = array();
    //percorre o resultado colocando em ordem
    foreach($resultsQuery as $key => $n){
        $result[$n['EspeciaisNode']['ordem']] = $n;
    }
    $imagemUrl = '';
	$imagemBgUrl = '';
	//se tem imagem
	if(isset($result[1]['Multiattach']['filename']) && !empty($result[1]['Multiattach']['filename'])){
        $imagemUrlArray = array(
            'plugin'		=> 0,
            'controller'	=> 0,
            'action'		=> 'displayFile', 
            'admin'			=> false,
            'filename'		=> $result[1]['Multiattach']['filename'],
            'dimension'		=> '600largura'
        );
        $imagemUrl = $this->Html->url($imagemUrlArray);	
        $imagemUrlArray = array(
            'plugin'		=> 0,
            'controller'	=> 0,
            'action'		=> 'displayFile', 
            'admin'			=> false,
            'filename'		=> $result[1]['Multiattach']['filename'],
            'dimension'		=> '1024largura'
        );
        $imagemBgUrl = $this->Html->url($imagemUrlArray);		
	}
?>
<div class="row">
        <section class="section section--bg-img coverage-news" style="background-image:url(<?php echo $imagemBgUrl;?>)">
            <div class="section--black-opacity">
                <div class="container">
                    <h1 class="section__title">
                        <?php echo $result[1]['Especiais']['title']; ?>
                    </h1>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 submanchete">
                            <div class="bg-article">
                                <a href="<?php echo $result[1]['Node']['path']; ?>">
                                    <article>
                                        <header>
                                            <div class="heading" role="heading">
                                                <span class="headover">
                                                    <?php 
                                                    //se tem chapey
                                                    if(isset($result[1]['NoticiumDetail']['chapeu']) && !empty($result[1]['NoticiumDetail']['chapeu'])){
                                                        echo $result[1]['NoticiumDetail']['chapeu'];
                                                    }else{
                                                        echo $this->Mswi->categoryName($result[1]['Node']['terms']);
                                                    }
                                                    ?>
                                                </span>
                                                <figure class="bp-media">
                                                    <div class="img-responsive">
                                                        <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $result[1]['Node']['title']; ?>">
                                                    </div>
                                                </figure>
                                                <h1>
                                                    <?php echo $result[1]['Node']['title']; ?>
                                                </h1>
                                            </div>
                                        </header>
                                        <div class="clearfix"></div>
                                    </article>
                                </a>
                            </div>
                        </div>
                        <?php if(isset($result[2])) { ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="bg-article">
                                    <a href="<?php echo $result[2]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[2]['NoticiumDetail']['chapeu']) && !empty($result[2]['NoticiumDetail']['chapeu'])){
                                                            echo $result[2]['NoticiumDetail']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[2]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php
                                                    $imagemUrl = '';
                                                    //se tem imagem
                                                    if(isset($result[2]['Multiattach']['filename']) && !empty($result[2]['Multiattach']['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $result[2]['Multiattach']['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                        ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $result[2]['Node']['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1>
                                                        <?php echo $result[2]['Node']['title']; ?>
                                                    </h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if(isset($result[3])) { ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="bg-article">
                                    <a href="<?php echo $result[3]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[3]['NoticiumDetail']['chapeu']) && !empty($result[3]['NoticiumDetail']['chapeu'])){
                                                            echo $result[3]['NoticiumDetail']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[3]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>                                                
                                                    <h1>
                                                        <?php echo $result[3]['Node']['title']; ?>
                                                    </h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if(isset($result[4])) { ?>
                            <div class="col-md-6 visible-md">
                                <div class="bg-article">
                                    <a href="<?php echo $result[4]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[4]['NoticiumDetail']['chapeu']) && !empty($result[4]['NoticiumDetail']['chapeu'])){
                                                            echo $result[4]['NoticiumDetail']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[4]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>                                                
                                                    <h1>
                                                        <?php echo $result[4]['Node']['title']; ?>
                                                    </h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <?php if(isset($result[4])){ ?>
                            <div class="col-lg-4 col-sm-12 hidden-md">
                                <div class="bg-article">                            
                                    <a href="<?php echo $result[4]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[4]['Node']['chapeu']) && !empty($result[4]['Node']['chapeu'])){
                                                            echo $result[4]['Node']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[4]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php
                                                    $imagemUrl = '';
                                                    //se tem imagem
                                                    if(isset($result[4]['Multiattach']['filename']) && !empty($result[4]['Multiattach']['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $result[4]['Multiattach']['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                        ?>
                                                        <figure class="bp-media hidden-md">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $result[4]['Node']['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $result[4]['Node']['title']; ?></h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>                                
                                </div>
                            </div>
                        <?php } ?>
                        <?php if(isset($result[5])){ ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="bg-article">                            
                                    <a href="<?php echo $result[5]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[5]['Node']['chapeu']) && !empty($result[5]['Node']['chapeu'])){
                                                            echo $result[5]['Node']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[5]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php
                                                    $imagemUrl = '';
                                                    //se tem imagem
                                                    if(isset($result[5]['Multiattach']['filename']) && !empty($result[5]['Multiattach']['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $result[5]['Multiattach']['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                        ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $result[5]['Node']['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $result[5]['Node']['title']; ?></h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>                                
                                </div>
                            </div>
                        <?php } ?>
                        <?php if(isset($result[6])){ ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="bg-article">                            
                                    <a href="<?php echo $result[6]['Node']['path']; ?>">
                                        <article>
                                            <header class="container">
                                                <div class="heading" role="heading">
                                                    <span class="headover">
                                                        <?php 
                                                        //se tem chapey
                                                        if(isset($result[6]['Node']['chapeu']) && !empty($result[6]['Node']['chapeu'])){
                                                            echo $result[6]['Node']['chapeu'];
                                                        }else{
                                                            echo $this->Mswi->categoryName($result[6]['Node']['terms']);
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php
                                                    $imagemUrl = '';
                                                    //se tem imagem
                                                    if(isset($result[6]['Multiattach']['filename']) && !empty($result[6]['Multiattach']['filename'])){
                                                        $imagemUrlArray = array(
                                                            'plugin'		=> 0,
                                                            'controller'	=> 0,
                                                            'action'		=> 'displayFile', 
                                                            'admin'			=> false,
                                                            'filename'		=> $result[6]['Multiattach']['filename'],
                                                            'dimension'		=> '300largura'
                                                        );
                                                        $imagemUrl = $this->Html->url($imagemUrlArray);	
                                                    }
                                                    //se tem imagem
                                                    if(!empty($imagemUrl)){
                                                        ?>
                                                        <figure class="bp-media">
                                                            <div class="img-responsive">
                                                                <img src="<?php echo $imagemUrl; ?>" alt="<?php echo $result[6]['Node']['title']; ?>" />
                                                            </div>
                                                        </figure>
                                                    <?php } ?>
                                                    <h1><?php echo $result[6]['Node']['title']; ?></h1>
                                                </div>
                                            </header>
                                        </article>
                                    </a>                                
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php } ?>