<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Especiais', array(
    'admin' => true,
    'plugin' => 'especiais',
    'controller' => 'especiais',
    'action' => 'index',
  ));

//ADICIONANDO
if($this->action == 'admin_add'){
    $this->Html->addCrumb('Novo', $this->here);
//EDITANDO 
}else{
    $this->Html->addCrumb('Editar', $this->here);
}

$this->append('form-start', $this->Form->create('Especiais', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Nova Seção', '#tab-descricao');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-descricao');

        echo $this->Form->hidden('id');

        echo $this->Form->input(
            'title',
            array(
                'label' => 'Título da Seção'
            )
        );

        echo $this->Form->input(
            'regiao',
            array(
                'label' => 'Região da Seção',
                'options' => $arrayRegioes
            )
        );

    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação');
        echo $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        );

        echo $this->Form->input('status', array(
            'label' => 'Ativo',
            'type' => 'checkbox'
        ));    

    echo $this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());

