<?php
App::uses('AppModel', 'Model');

    class Especiais extends AppModel
    {
        function getGetRegions(){
            $arrayRegioes = array(
                'secao-especial1' => 'Seção 1 (Abaixo da Super Manchete)',
                'secao-especial2' => 'Seção 2 (Acima de Diversão e Arte)',
            );
            return $arrayRegioes;
        }
    }
?>