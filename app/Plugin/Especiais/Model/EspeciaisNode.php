<?php
App::uses('AppModel', 'Model');

    class EspeciaisNode extends AppModel
    {

        /**
         * beforeFind callback
         *
         * @param array $queryData
         * @return array
         */
        public function beforeFind($queryData) 
        {
            return $queryData;
        }

        /**
         * afterFind callback
         *
         * @param array $results
         * @param array $primary
         * @return array
         */
        public function afterFind($results, $primary = false) 
        {
            return $results;
        }

        /**
         * beforeSave callback
         *
         * @return boolean
         */
        public function beforeSave($options = array()) 
        {
            return true;
        }

        public function afterSave($created, $options = array())
        {
            $node_id 		= $this->data['EspeciaisNode']['node_id'];
            $ordem 			= $this->data['EspeciaisNode']['ordem'];
            $especiais_id 	= $this->data['EspeciaisNode']['especiais_id'];

            //se foi informado o $especiais_id
            if(!empty($especiais_id)){
                //Deleta se for do mesmo node e mesma posição
                $this->query("DELETE FROM especiais_nodes WHERE node_id <> $node_id AND especiais_id = $especiais_id AND ordem = $ordem;");
            }else{
                //Deleta se for do mesmo node e mesma posição
                $this->query("DELETE FROM especiais_nodes WHERE node_id = $node_id;");
            }
        }
    }
    
?>