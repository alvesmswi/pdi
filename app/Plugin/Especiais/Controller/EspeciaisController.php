<?php
App::uses('EspeciaisAppController', 'Especiais.Controller');

  class EspeciaisController extends EspeciaisAppController {

    public $name = 'Especiais';
    public $uses = array('Especiais.Especiais');    
    public $components = array('Mswi');    
    
    public function beforeFilter() {
        parent::beforeFilter();
        //desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions += array('admin_index');
        //$this->Auth->allow('todos');
    }

    public function admin_index()
    {
        $this->Especiais->recursive = -1;
		$this->paginate['Especiais']['order'] = 'Especiais.id DESC';
        $this->set('registros', $this->paginate());
        $this->set('arrayRegioes', $this->Especiais->getGetRegions());
        
    }

    public function admin_add()
    {        
        if (!empty($this->request->data)) {
            if($this->Especiais->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/especiais');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
        }
        $this->set('arrayRegioes', $this->Especiais->getGetRegions());
    }

    public function admin_edit($id)
    {        
        $this->render('admin_add');

        if (!empty($this->request->data)) {
            //se foi informada a data de inicio
			if(isset($this->request->data['Especiais']['publish_start']) && !empty($this->request->data['Especiais']['publish_start'])){
				//trata a data
				$this->request->data['Especiais']['publish_start'] = $this->Mswi->tratarDataHora($this->request->data['Especiais']['publish_start']);
            }
            //se foi informada a data de inicio
			if(isset($this->request->data['Especiais']['publish_end']) && !empty($this->request->data['Especiais']['publish_end'])){
				//trata a data
				$this->request->data['Especiais']['publish_end'] = $this->Mswi->tratarDataHora($this->request->data['Especiais']['publish_end']);
            }
            
            if($this->Especiais->save($this->request->data)){
                $this->Flash->success('Registro salvo com sucesso');
                $this->redirect('/admin/especiais');
            }else{
                $this->Flash->success('Ocorreu um erro ao salvar o registro');
            }
        }

        $this->request->data = $this->Especiais->find(
            'first',
            array(
                'conditions' => array(
                    'Especiais.id' => $id
                )
            )
        );

        $this->set('arrayRegioes', $this->Especiais->getGetRegions());

        $this->render('admin_add');
    }

    public function admin_delete($id)
    {        
        if($this->Especiais->delete($id)){
            $this->Flash->success('Registro excluído com sucesso');
            $this->redirect('/admin/especiais');
        }else{
            $this->Flash->success('Ocorreu um erro ao excluir o registro');
        }
    }

    public function admin_toggle($id = null, $status = null) {
		$this->Croogo->fieldToggle($this->{$this->modelClass}, $id, $status);
    }

}
?>