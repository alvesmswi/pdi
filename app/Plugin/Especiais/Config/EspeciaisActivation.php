<?php
class EspeciaisActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {

		//da permissões para acessar plugin
		$controller->Croogo->addAco('Especiais');
		$controller->Croogo->addAco('Especiais/Especiais/admin_index', array('gerente', 'editor'));
		$controller->Croogo->addAco('Especiais/Especiais/admin_add', array('gerente', 'editor'));
		$controller->Croogo->addAco('Especiais/Especiais/admin_edit', array('gerente', 'editor'));
		$controller->Croogo->addAco('Especiais/Especiais/admin_delete', array('gerente', 'editor'));
		$controller->Croogo->addAco('Especiais/Especiais/admin_toggle', array('gerente', 'editor'));
		
		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		//Cria a tabela
		include_once(App::pluginPath('Especiais').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$EspeciaisSchema = new EspeciaisSchema();
		foreach ($EspeciaisSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($EspeciaisSchema, $table));
			}
		}       

		$i = 1;
		while($i <= 2){
			//cria a região
			$controller->loadModel('Region');
			$region = $controller->Region->find('first', array('conditions'=>array('Region.alias'=>'secao_especial'.$i)));
			//se a região não existe
			if(empty($region)){
				$controller->Region->create();
				$controller->Region->set(
					array(
						'title'            => 'Seção Especial '.$i,
						'alias'            => 'secao_especial'.$i,
						'description'      => 'Seção Especial '.$i
					)
				);
				$controller->Region->save();
				$regionId = $controller->Region->id;	
			}else{
				$regionId = $region['Region']['id'];
			} 

			//Cria o bloco
			$controller->loadModel('Block');
			$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'secao-especial'.$i)));
			//se o Bloco não existe
			if(empty($block)){
				$controller->Block->create();
				$controller->Block->set(array(
					'visibility_roles' => '',
					'visibility_paths' => '/',
					'region_id'        => $regionId,
					'title'            => 'Seção Especial '.$i,
					'alias'            => 'secao-especial'.$i,
					'body'             => '[element:secao plugin="Especiais"]',
					'show_title'       => 0,
					'status'           => 1
				));
				$controller->Block->save();
			}
			$i++;
		}

		Cache::clear(false, '_cake_model_');
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Especiais');
	}
}