<?php 
class EspeciaisSchema extends CakeSchema {

	public $name = 'Especiais';

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $especiais = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'title' => array('type' => 'string', 'length' => 255, 'null' => true),
		'regiao' => array('type' => 'string', 'length' => 255, 'null' => true),
		'status' => array('type' => 'integer', 'null' => true, 'default' => 1),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),		
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $especiais_nodes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'especiais_id' => array('type' => 'integer','null' => true),
		'node_id' => array('type' => 'integer', 'null' => true),
		'ordem' => array('type' => 'integer', 'null' => true),		
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
