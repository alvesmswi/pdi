<?php
Croogo::hookRoutes('Especiais');

CroogoNav::add('sidebar', 'Especiais', array(
	'icon' => 'star',
	'title' => 'Seções Especiais',
	'url' => array(
		'admin' => true,
		'plugin' => 'especiais',
		'controller' => 'especiais',
		'action' => 'index',
	)
));
  /**
   * Admin menu
   */
	Croogo::hookAdminMenu('Especiais');
	
	Croogo::hookAdminBox('Nodes/admin_add', 'Seção Especial', 'Especiais.admin_box', array('type'=>array('noticia')));
	Croogo::hookAdminBox('Nodes/admin_edit', 'Seção Especial', 'Especiais.admin_box', array('type'=>array('noticia')));
	
	Croogo::hookModelProperty('Node', 'hasOne', array(
		'EspeciaisNode' => array(
			'className' 	=> 'Especiais.EspeciaisNode',
			'foreignKey' 	=> 'node_id',
			'dependent' 	=> true,
			//'conditions' 	=> array('Especiais.type' => 'nodes'),
			//'order' 		=> 'Especiais.id ASC'
		)
	));
?>