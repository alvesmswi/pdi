<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Parceiros Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class ParceirosV2Controller extends ApiAppController 
{
    public $name = 'ParceirosV2';
    public $components = array('Api.Util');
    public $uses = array('Api.Api');
    public $cacheName = 'AppParceiros_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'index',    
                'view',
                'search',
                'categoria',
                'lista' 
            )
        );
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */

    public function index(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_parceiros AS Parceiro 
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Parceiro.status = 1 
            $where 
        ORDER BY Parceiro.nome_fantasia ASC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function lista(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Monta o SQL
        $sql = "SELECT 
            id, nome_fantasia AS value
        FROM clube_parceiros AS Parceiro 
        WHERE 
            status = 1 
        ORDER BY nome_fantasia ASC";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'lista';
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function search(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se foi informado a palavra de busca
        if (isset($this->params->string) && !empty($this->params->string)) {
            $string = $this->params->string;
            $where .= " AND Parceiro.nome_fantasia LIKE '%$string%' ";
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_parceiros AS Parceiro 
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Parceiro.status = 1 
            $where 
        ORDER BY Parceiro.nome_fantasia ASC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'search_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function categoria(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se NÃO foi informado a palavra de busca
        if (!isset($this->params->categoria) || empty($this->params->categoria)) {
            return false;
        }

        $categoria = $this->params->categoria;
        $where .= " AND Categoria.slug = '$categoria' ";
        
        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_parceiros AS Parceiro 
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Parceiro.status = 1 
            $where 
        ORDER BY Parceiro.nome_fantasia ASC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'categoria_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function view(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Se NÃO foi informado o ID da conteúdo
        if (!isset($this->params->id) || empty($this->params->id)) {
            return false;
        }

        $id = $this->params->id;
        $where .= " AND Parceiro.id = $id ";

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_parceiros AS Parceiro 
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Parceiro.status = 1 
            $where";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'view_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

}
