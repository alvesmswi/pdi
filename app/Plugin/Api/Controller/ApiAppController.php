<?php

App::uses('AppController', 'Controller');

class ApiAppController extends AppController 
{
    public $components = array('Paginator', 'RequestHandler');
    public $uses    = array('Nodes.Node');
    public $limit   = 8;

    public function beforeFilter() 
    {
        parent::beforeFilter();

        $this->Auth->allow(array('index'));

        //Configure::write('Cache.disable', true);
		//clearCache(null, 'models', null);
        
        $this->autoRender = false;

        $this->response->charset('UTF-8');
        $this->response->header(array(
            'Cache-Control: no-cache, must-revalidate',
            'Vary: Accept-Encoding',
            'X-Content-Type-Options: nosniff',
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, Access-Control-Allow-Origin, X-Requested-With',
            'Content-type: application/json; charset=UTF-8'
        ));
    }
}
