<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Noticias Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class NoticiasController extends ApiAppController 
{
    public $name = 'Noticias';
    public $components = array('Api.Util');

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(array('index', 'view', 'search'));
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */
    public function index()
    {
        $conditions = array();
        $conditions['AND']['Node.type'] = 'noticia';
        $conditions['AND']['Node.status'] = 1;
        $conditions['AND']['Node.publish_start <='] = date('Y-m-d H:i:s');

        $page = 1;

        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        if (isset($this->request->query['editoria']) && !empty($this->request->query['editoria'])) {
            $editorias = $this->request->query['editoria'];
            $arrayEditorias = explode(',', $editorias);
            foreach ($arrayEditorias as $key => $editoria) {
                $conditions['OR'][$key]['Node.terms LIKE'] = "%{$editoria}%";
            }
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array(
                'Node.id',
                'Node.title',
                'Node.excerpt',
                'Node.publish_start',
                'Node.path'
            ),
            'contain' => array(
                'Taxonomy' => array(
                    'Term' => array(
                        'fields' => array(
                            'id',
                            'title'
                        )
                    )
                )
            ),
            'order' => array(
                'Node.publish_start' => 'DESC'
            ),
            'page'  => $page,
            'limit' => $this->limit
        );

        //prepara o nome do cache
        $cacheName = 'app_noticias_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda não tem cache
        if(!$result){            
            //faz a busca nos nodes
            $nodes = $this->Paginator->paginate('Node');   
            //se tem encontrou nodes
            if(!empty($nodes)){
                $result = array();
                //percorre os nodes
                foreach ($nodes as $key => $node) { 
                    $image_url = '';
                    if (isset($node['Multiattach'][0]) && !empty($node['Multiattach'][0])) {
                        $image = $node['Multiattach'][0]['Multiattach']['filename'];
                        $image_url = Router::url("/fl/474largura/{$image}", true);
                    }
        
                    $term = '';
                    if(isset($node['Taxonomy']) && !empty($node['Taxonomy'])){
                        $term = $node['Taxonomy'][0]['Term']['title'];
                    }
        
                    $interval = $this->Util->haTempos($node['Node']['publish_start']);
        
                    $result['result'][] = array(
                        'id'        => $node['Node']['id'],
                        'title'     => $node['Node']['title'],
                        'excerpt'   => $node['Node']['excerpt'],
                        'url'       => Router::url($node['Node']['path'], true),
                        'date'      => $interval,
                        'category'  => $term,
                        'image'     => $image_url           
                    );
                }

                //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
                $result = json_encode($result);
                //guarda no cache
                Cache::write($cacheName, $result);   
            }       
        }      

        $this->response->body($result);
    }

    public function view($id) 
    {
        //prepara o nome do cache
        $cacheName = 'app_noticia_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda não tem cache
        if(!$result){ 
            $node = $this->Node->find(
                'first',
                array(
                    'conditions' => array(
                        'Node.id' => $id,
                        'Node.status' => 1
                    ),
                    'fields' => array(
                        'Node.id',
                        'Node.title',
                        'Node.excerpt',
                        'Node.publish_start',
                        'Node.body',
                        'Node.path'
                    ),
                    'contain' => array(
                        'Taxonomy' => array(
                            'Term' => array(
                                'fields' => array(
                                    'id',
                                    'title'
                                )
                            )
                        ),
                        'NoticiumDetail'
                    )
                )
            );
            
            $jornalista = null;
            if (isset($node['NoticiumDetail']) && !empty($node['NoticiumDetail']['jornalista'])) {
                $jornalista = $node['NoticiumDetail']['jornalista'];
            }

            $image_url = '';
            $image_description = null;
            $image_src = null;
            if (isset($node['Multiattach'][0]) && !empty($node['Multiattach'][0])) {
                $image = $node['Multiattach'][0]['Multiattach']['filename'];
                $image_url = Router::url("/fl/474largura/{$image}", true);

                if (!empty($node['Multiattach'][0]['Multiattach']['meta'])) {
                    $meta = json_decode($node['Multiattach'][0]['Multiattach']['meta']);
                    $image_description = (isset($meta[0])) ? $meta[0] : null;
                }
                $image_src = $node['Multiattach'][0]['Multiattach']['comment'];
            }

            $result = array();
            $result['result'] = array(
                'id'            => $node['Node']['id'],
                'title'         => $node['Node']['title'],
                'url'           => Router::url($node['Node']['path'], true),
                'body'          => $node['Node']['body'],
                'date'          => date('d/m/Y H\hi', strtotime($node['Node']['publish_start'])),
                'jornalista'    => $jornalista,
                'category'      => $node['Taxonomy'][0]['Term']['title'],
                'image'         => $image_url,
                'image_description' => $image_description,
                'image_src'     => $image_src
            );

            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);

            //guarda no cache
            Cache::write($result, $cacheName);
        }

        $this->response->body($result);
    }

    /**
     * index function
     *
     * @param string string
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */
    public function search($string)
    {
        $conditions = array();
        $conditions['AND']['Node.type'] = 'noticia';
        $conditions['AND']['Node.status'] = 1;
        $conditions['AND']['Node.publish_start <='] = date('Y-m-d H:i:s');

        $conditions['OR']['Node.title LIKE'] = "%{$string}%";
        $conditions['OR']['Node.excerpt LIKE'] = "%{$string}%";

        $page = 1;

        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        if (isset($this->request->query['editoria']) && !empty($this->request->query['editoria'])) {
            $editorias = $this->request->query['editoria'];
            $arrayEditorias = explode(',', $editorias);
            foreach ($arrayEditorias as $key => $editoria) {
                $conditions['OR'][$key]['Node.terms LIKE'] = "%{$editoria}%";
            }
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array(
                'Node.id',
                'Node.title',
                'Node.excerpt',
                'Node.publish_start',
                'Node.path'
            ),
            'contain' => array(
                'Taxonomy' => array(
                    'Term' => array(
                        'fields' => array(
                            'id',
                            'title'
                        )
                    )
                )
            ),
            'order' => array(
                'Node.publish_start' => 'DESC'
            ),
            'page'  => $page,
            'limit' => $this->limit
        );

        $nodes = $this->Paginator->paginate('Node');

        $result = array();
        foreach ($nodes as $key => $node) { 
            $image_url = '';
            if (isset($node['Multiattach'][0]) && !empty($node['Multiattach'][0])) {
                $image = $node['Multiattach'][0]['Multiattach']['filename'];
                $image_url = Router::url("/fl/474largura/{$image}", true);
            }

            $term = '';
            if(isset($node['Taxonomy']) && !empty($node['Taxonomy'])){
                $term = $node['Taxonomy'][0]['Term']['title'];
            }

            $interval = $this->Util->haTempos($node['Node']['publish_start']);

            $result['result'][] = array(
                'id'        => $node['Node']['id'],
                'title'     => $node['Node']['title'],
                'excerpt'   => $node['Node']['excerpt'],
                'url'       => Router::url($node['Node']['path'], true),
                'date'      => $interval,
                'category'  => $term,
                'image'     => $image_url           
            );
        }

        $this->response->body(json_encode($result));
    }
}
