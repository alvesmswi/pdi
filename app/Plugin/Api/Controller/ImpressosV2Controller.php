<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * ImpressosV2 Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class ImpressosV2Controller extends ApiAppController 
{
    public $name = 'ImpressosV2';
    public $components = array('Api.Util');
    public $uses = array('Api.Api');
    public $cacheName = 'AppImpressosV2_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'index',   
                'view',
                'search' 
            )
        );
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */

    public function index(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.publish_start AS date, Node.exclusivo, 
            Multiattach.filename AS capa, 
            PageFlip.pdf AS pdf
        FROM nodes AS Node 
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
        LEFT JOIN pageflip_pdfs AS PageFlip ON(PageFlip.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.type = 'impresso' AND
            Node.status = 1 
            $where 
        GROUP BY Node.id  
        ORDER BY Node.publish_start DESC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function view(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Se NÃO foi informado o ID da conteúdo
        if (!isset($this->params->id) || empty($this->params->id)) {
            return false;
        }

        $id = $this->params->id;
        $where .= " AND Node.id = $id ";

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.publish_start AS date, Node.exclusivo, 
            Multiattach.filename AS capa,
            PageFlip.id, PageFlip.pdf, PageFlip.images
        FROM nodes AS Node 
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN pageflip_pdfs AS PageFlip ON(PageFlip.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.type = 'impresso' AND
            Node.status = 1 
            $where 
        GROUP BY Node.id";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'view_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        
        //se ainda NÃO tem cache
        if(!$result){             
            //Executa a query
            $result = $this->Api->query($sql);
            //Pega as imagens
            $imagensPdf = array();
            if (isset($result[0]['PageFlip']['images']) && !empty($result[0]['PageFlip']['images'])) {
                //se for o PDF
                $pdf = '/pageflip/pdfs/normal/'.$result[0]['PageFlip']['id'].'/'.$result[0]['PageFlip']['pdf'];
                $arrayImagens = json_decode($result[0]['PageFlip']['images'], true);

                if (!empty($arrayImagens)) {
                    foreach ($arrayImagens as $key => $imagem) {
                        $imagensPdf[$key] = Router::url('/pageflip/pdfs/normal/'.$result[0]['PageFlip']['id'].'/'.$imagem['image'], true);
                    }
                }
            }   
            //se encontrou imagens dentro do Json
            if(!empty($imagensPdf)){
                $result[0]['PageFlip']['imagens'] = $imagensPdf;
            }
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function search(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se foi informado a palavra de busca
        if (isset($this->params->string) && !empty($this->params->string)) {
            $string = $this->params->string;
            $where .= " AND Node.title LIKE '%$string%' ";
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.publish_start AS date, Node.exclusivo, 
            Multiattach.filename AS capa, 
            PageFlip.pdf AS pdf
        FROM nodes AS Node 
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id) 
        LEFT JOIN pageflip_pdfs AS PageFlip ON(PageFlip.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.type = 'impresso' AND
            Node.status = 1 
            $where 
        GROUP BY Node.id  
        ORDER BY Node.publish_start DESC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    }

}
