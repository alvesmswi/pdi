<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Categoria Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class CategoriasV2Controller extends ApiAppController 
{
    public $name = 'CategoriasV2';
    public $components = array('Api.Util');
    public $uses = array('Api.Api');
    public $cacheName = 'AppCategorias_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'lista' 
            )
        );
    }
    
    
    public function lista(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Monta o SQL
        $sql = "SELECT 
            slug AS id, title AS value
        FROM clube_categorias AS Categoria 
        WHERE 
            status = 1 
        ORDER BY title ASC";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'lista';
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

}
