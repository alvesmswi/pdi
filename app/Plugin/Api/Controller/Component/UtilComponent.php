<?php

App::uses('Component', 'Controller');

class UtilComponent extends Component 
{
    public function haTempos($date_start) 
    {
        $publish_start = new DateTime($date_start);
        $since_start = $publish_start->diff(new DateTime(date('Y-m-d H:i:s')));
        
        if ($since_start->y > 0) {
            $texto = ($since_start->y == 1) ? 'ano' : 'anos';
            $interval = "Há {$since_start->y} {$texto}";
        } elseif ($since_start->m > 0) {
            $texto = ($since_start->m == 1) ? 'mês' : 'meses';
            $interval = "Há {$since_start->m} {$texto}";
        } elseif ($since_start->d > 0) {
            if ($since_start->d >= 7) {
                $week = round($since_start->d / 7);
                $texto = ($week == 1) ? 'semana' : 'semanas';
                $interval = "Há {$week} {$texto}";
            } else {
                $texto = ($since_start->d == 1) ? 'ontem' : "Há {$since_start->d} dias";
                $interval = "{$texto}";
            }
        } elseif ($since_start->h > 0) {
            $texto = ($since_start->h == 1) ? 'hora' : 'horas';
            $interval = "Há {$since_start->h} {$texto}";
        } elseif ($since_start->i > 0) {
            $texto = ($since_start->i == 1) ? 'minuto' : 'minutos';
            $interval = "Há {$since_start->i} {$texto}";
        } elseif ($since_start->s > 0) {
            $texto = ($since_start->s == 1) ? 'segundo' : 'segundos';
            $interval = "Há {$since_start->s} {$texto}";
        } else {
            $interval = "Há {$since_start->days} dias";
        }

        return $interval;
    }

    function sendEmail($to, $subject, $message) {
        App::uses('CakeEmail', 'Network/Email');
		try {
			$email = new CakeEmail();
			$email->from(array(Configure::read('Site.email') => Configure::read('Site.title')));
            $email->to($to);
            $email->subject($subject);
            $result = $email->send($message);
		} catch (SocketException $e) {
			$this->log(sprintf('Erro ao enviar email : %s', $e->getMessage()));
		}
		return $result;
	}
}