<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Descontos Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class DescontosV2Controller extends ApiAppController 
{
    public $name = 'DescontosV2';
    public $components = array('Api.Util');
    public $uses = array('Api.Api');
    public $cacheName = 'AppDescontos_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'index',    
                'view',
                'categoria',
                'parceiro' 
            )
        );
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */

    public function index(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_promocaos AS Desconto 
        INNER JOIN clube_parceiros AS Parceiro ON(Parceiro.id = Desconto.clube_parceiro_id)
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Desconto.status = 1 
            $where 
        ORDER BY Desconto.title ASC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function categoria(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se NÃO foi informado a palavra de busca
        if (!isset($this->params->categoria) || empty($this->params->categoria)) {
            return false;
        }

        $categoria = $this->params->categoria;
        $where .= " AND Categoria.slug = '$categoria' ";
        
        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_promocaos AS Desconto 
        INNER JOIN clube_parceiros AS Parceiro ON(Parceiro.id = Desconto.clube_parceiro_id)
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Desconto.status = 1 
            $where 
        ORDER BY Desconto.title ASC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'categoria_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function view(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Se NÃO foi informado o ID da conteúdo
        if (!isset($this->params->id) || empty($this->params->id)) {
            return false;
        }

        $id = $this->params->id;
        $where .= " AND Desconto.id = $id ";

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_promocaos AS Desconto 
        INNER JOIN clube_parceiros AS Parceiro ON(Parceiro.id = Desconto.clube_parceiro_id)
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Desconto.status = 1 
            $where";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'view_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function parceiro(){
        $this->autoRender = false;

        //Variaveis padrões
        $where = '';

        //Se NÃO foi informado o ID da conteúdo
        if (!isset($this->params->id) || empty($this->params->id)) {
            return false;
        }

        $id = $this->params->id;
        $where .= " AND Parceiro.id = $id ";

        //Monta o SQL
        $sql = "SELECT 
            *
        FROM clube_promocaos AS Desconto 
        INNER JOIN clube_parceiros AS Parceiro ON(Parceiro.id = Desconto.clube_parceiro_id)
        INNER JOIN clube_categorias AS Categoria ON(Categoria.id = Parceiro.clube_categoria_id)
        WHERE 
            Desconto.status = 1 
            $where";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'parceiro_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

}
