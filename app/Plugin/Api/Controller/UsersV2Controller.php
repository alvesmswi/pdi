<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Categoria Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class UsersV2Controller extends ApiAppController 
{
    public $name = 'UsersV2';
    public $uses = array('Api.Api');
    public $components = array('Api.Util');
    public $cacheName = 'AppUsers_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'login',
                'profile',
                'recuperar' 
            )
        );
    }
    
    public function login(){
        $this->autoRender = false;

        $data       = json_decode(file_get_contents('php://input'));
        $password   = AuthComponent::password($data->senha);
        $email      = $data->email;
        //$this->log($data);

        //Monta o SQL
        $sql = "SELECT 
           *
        FROM users AS User 
        INNER JOIN roles AS Role ON(Role.id = User.role_id)
        WHERE 
            User.email = '$email' AND 
            User.password = '$password' AND 
            User.status = 1";
        //Executa a query
        $result = $this->Api->query($sql);
        //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
        $result = json_encode($result);

        $this->response->body($result);

    }

    public function profile($id = null){
        $this->autoRender = false;
        
        $data      = json_decode(file_get_contents('php://input'));
        //se foi enviado uma foto
        if(isset($data->foto) && !empty($data->foto)){
            $id         = $data->id;
            $foto       = $data->foto;

            $filename   = 'assinante-'.$id.'.jpeg';
            $filepath   = 'img/'.$filename;
            //pega a foto
            $image_parts    = explode(";base64,", $foto);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type     = $image_type_aux[1];
            $foto           = base64_decode($image_parts[1]);
            file_put_contents($filepath, $foto);
            //Monta o SQL
            $sql = "UPDATE users SET foto = '$filepath' WHERE id = $id";
            //Executa a query
            $this->Api->query($sql);

            //Monta o SQL
            $sql = "SELECT *  FROM users AS User 
            INNER JOIN roles AS Role ON(Role.id = User.role_id)
            WHERE 
                User.id = '$id'";
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            $this->response->body($result);
        }

        //se foi informado o ID por get
        if($id){
            //Monta o SQL
            $sql = "SELECT *  FROM users AS User 
            INNER JOIN roles AS Role ON(Role.id = User.role_id)
            WHERE 
                User.id = '$id'";
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            $this->response->body($result);
        }
        return false;
    }

    public function recuperar(){
        $data       = json_decode(file_get_contents('php://input'));
        $email      = $data->email;

        //Monta o SQL
        $sql = "SELECT * FROM users AS User 
        WHERE 
            User.email = '$email' AND 
            User.status = 1";
        //Executa a query
        $result = $this->Api->query($sql);

        //se encontrou o usuario com o email informado
        if(!empty($result)){
            $userId = $result[0]['User']['id'];
            //Gera uma nova senha
            $senha = rand(111111,999999);
            $senhaHash = AuthComponent::password($senha );
            //atualiza no BD
			$this->Api->query("UPDATE users SET password = '$senhaHash' WHERE id = $userId;");            

            //envia por email
            $this->Util->sendEmail(
                $email,
                'Recuperação de senha - '.Configure::read('Site.title'),
                'Sua nova senha é: '.$senha
            );
        }
        //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
        $result = json_encode($result);
        $this->response->body($result);
    }

}
