<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * NoticiasV2 Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class NoticiasV2Controller extends ApiAppController 
{
    public $name = 'NoticiasV2';
    public $components = array('Api.Util');
    public $uses = array('Api.Api');
    public $cacheName = 'AppNoticiasV2_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'index', 
                'destaques',                
                'search',
                'editoria',
                'view' 
            )
        );
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @param string editoria (?editoria=esportes,politica)
     * @return void
     */

    public function index(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        $editoria = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se foi informado a editoria
        if (isset($this->request->query['editoria']) && !empty($this->request->query['editoria'])) {
            $editoria = $this->request->query['editoria'];
            $where .= " AND Editoria.slug = '$editoria' ";
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.excerpt, Node.slug, Node.path, Node.publish_start, Node.updated, Node.exclusivo, 
            NoticiumDetail.chapeu AS chapeu, NoticiumDetail.titulo_capa AS titulo_capa, 
            Multiattach.filename AS img_src, Multiattach.meta AS img_legenda, Multiattach.comment AS img_autor, 
            Editoria.title AS editoria_title, Editoria.slug AS editoria_slug      
        FROM nodes AS Node 
        INNER JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id)
        INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.status = 1 AND
            Node.promote = 0 
            $where 
        GROUP BY Node.id  
        ORDER BY Node.publish_start DESC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index_page_'.$editoria.'_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function destaques(){
        $this->autoRender = false;

        $limit = 6;
        //Se foi informado o limit
        if (isset($this->request->query['limit']) && !empty($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.excerpt, Node.slug, Node.path, Node.publish_start, Node.updated, Node.exclusivo, 
            NoticiumDetail.chapeu AS chapeu, NoticiumDetail.titulo_capa AS titulo_capa, 
            Multiattach.filename AS img_src, Multiattach.meta AS img_legenda, Multiattach.comment AS img_autor, 
            Editoria.title AS editoria_title, Editoria.slug AS editoria_slug      
        FROM nodes AS Node 
        INNER JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id)
        INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.status = 1 AND 
            Node.promote = 1             
        GROUP BY Node.id  
        ORDER BY Node.ordem ASC, Node.publish_start DESC 
        LIMIT $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'destaques_'.$limit;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function search(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se foi informado a palavra de busca
        if (isset($this->params->string) && !empty($this->params->string)) {
            $string = $this->params->string;
            $where .= " AND Node.title LIKE '%$string%' ";
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.excerpt, Node.slug, Node.path, Node.publish_start, Node.updated, Node.exclusivo, 
            NoticiumDetail.chapeu AS chapeu, NoticiumDetail.titulo_capa AS titulo_capa, 
            Multiattach.filename AS img_src, Multiattach.meta AS img_legenda, Multiattach.comment AS img_autor, 
            Editoria.title AS editoria_title, Editoria.slug AS editoria_slug      
        FROM nodes AS Node 
        INNER JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id)
        INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.status = 1 
            $where 
        GROUP BY Node.id  
        ORDER BY Node.publish_start DESC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'search_page_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function editoria(){
        $this->autoRender = false;

        //Variaveis padrões
        $limit = $this->limit;
        $page = 1;
        $where = '';
        $editoria = '';
        
        //Se foi informado a página
        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        //Se foi informado a palavra de busca
        if (isset($this->params->editoria) && !empty($this->params->editoria)) {
            $editoria = $this->params->editoria;
            $where .= " AND Editoria.slug = '$editoria' ";
        }

        $offset = ($page - 1) * $limit;

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.excerpt, Node.slug, Node.path, Node.publish_start, Node.updated, Node.exclusivo,  
            NoticiumDetail.chapeu AS chapeu, NoticiumDetail.titulo_capa AS titulo_capa, 
            Multiattach.filename AS img_src, Multiattach.meta AS img_legenda, Multiattach.comment AS img_autor, 
            Editoria.title AS editoria_title, Editoria.slug AS editoria_slug      
        FROM nodes AS Node 
        INNER JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id)
        INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.status = 1 
            $where 
        GROUP BY Node.id  
        ORDER BY Node.publish_start DESC 
        LIMIT $offset, $limit;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'editoria_page_'.$editoria.'_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    public function view(){
        $this->autoRender = false;
        $caminhoAbs = Router::url('/', true);

        //Variaveis padrões
        $where = '';

        //Se NÃO foi informado o ID da conteúdo
        if (!isset($this->params->id) || empty($this->params->id)) {
            return false;
        }

        $id = $this->params->id;
        $where .= " AND Node.id = $id ";

        //Monta o SQL
        $sql = "SELECT 
            Node.id, Node.title, Node.excerpt, Node.slug, Node.path, Node.publish_start, Node.updated, Node.body, Node.exclusivo,  
            NoticiumDetail.chapeu AS chapeu, NoticiumDetail.jornalista, NoticiumDetail.keywords, 
            Multiattach.filename AS img_src, Multiattach.meta AS img_legenda, Multiattach.comment AS img_autor, 
            Editoria.title AS editoria_title, Editoria.slug AS editoria_slug,
            User.name      
        FROM nodes AS Node 
        INNER JOIN editorias_nodes AS EditoriasNode ON(EditoriasNode.node_id = Node.id)
        INNER JOIN editorias AS Editoria ON(Editoria.id = EditoriasNode.editoria_id)
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id) 
        LEFT JOIN users AS User ON(User.id = Node.user_id) 
        WHERE 
            Node.publish_start <= NOW() AND 
            (Node.publish_end IS NULL OR Node.publish_end >= NOW()) AND 
            Node.status = 1 
            $where 
        GROUP BY Node.id";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'view_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            //trata as imagens do corpo
            $result[0]['Node']['body'] = $this->_imgBody($result[0]['Node']['body']);
            //trata a legenda
            $result[0]['Multiattach']['img_legenda'] = str_replace(                
                array(
                    '{',
                    '"',
                    ':',
                    '}',
                    '[',
                    ']',
                    '\\'
                ),
                '',
                $result[0]['Multiattach']['img_legenda']
            );
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        $this->response->body($result);
    } 

    function _imgBody($body){
        $procurar = 'src="/uploads/';        
        $caminhoAbs = Router::url('/', true);
        $substituir = 'src="'.$caminhoAbs.'uploads/';
        $body = str_replace($procurar, $substituir, $body);
        return $body;
    }

}
