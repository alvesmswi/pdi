<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Impressos Controller
 *
 * @category Controller
 * @author   Marcelo Carvalho <marcelo@mswi.com.br>
 * @link     http://mswi.com.br
 */
class ImpressosController extends ApiAppController 
{
    public $name = 'Impressos';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(array('index','view'));
    }
    
    /**
     * index function
     *
     * @param string page (?page=2)
     * @return void
     */
    /*public function index()
    {
        $page = 1;

        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        $this->Paginator->settings = array(
            'conditions' => array(
                'Node.type' => 'impresso'
            ),
            'fields' => array(
                'Node.id',
                'Node.title',
                'Node.slug',
                'Node.type',
                'Node.publish_start',
                'ImpressoDetail.id',
                'ImpressoDetail.node_id',
                'ImpressoDetail.edicao'
            ),
            'recursive' => 0,
            'order' => array(
                'Node.publish_start' => 'DESC'
            ),
            'page'  => $page,
            'limit' => $this->limit
        );

        $nodes = $this->Paginator->paginate('Node');

        $this->response->body(json_encode($nodes));
    }*/

    public function index()
    {
        $page = 1;

        if (isset($this->request->query['page']) && !empty($this->request->query['page'])) {
            $page = $this->request->query['page'];
        }

        $this->Paginator->settings = array(
            'conditions' => array(
                'Node.type' => 'impresso',
                'Node.status' => 1
            ),
            'fields' => array(
                'Node.id',
                'Node.title',
                'Node.publish_start'
            ),
            'recursive' => -1,
            'order' => array(
                'Node.publish_start' => 'DESC'
            ),
            'page'  => $page,
            'limit' => $this->limit
        );

        //prepara o nome do cache
        $cacheName = 'app_impressos_'.$page;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda não tem cache
        if(!$result){  

            $nodes = $this->Paginator->paginate('Node');
            
            $result = array();
            foreach ($nodes as $key => $node) {
                $image = '';
                if (isset($node['Multiattach']) && !empty($node['Multiattach'])) {
                    //percorre os anexos
                    foreach($node['Multiattach'] as $anexo){
                        //se não for o PDF
                        if($anexo['Multiattach']['mime']!='application/pdf'){
                            $image = $anexo['Multiattach']['filename'];
                            $image = Router::url("/fl/474largura/{$image}", true);
                        }
                    }
                }

                if (isset($node['PageFlip']) && !empty($node['PageFlip'])) {
                    $image = Router::url($node['PageFlip']['capa'], true);
                }

                $result['result'][$key] = array(
                    'id'        => $node['Node']['id'],
                    'title'     => $node['Node']['title'],
                    'date'      => date('d/m/Y', strtotime($node['Node']['publish_start'])),
                    'image'     => $image             
                );
            }

            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);
        }

        $this->response->body($result);
    }

    public function view($id){

        //prepara o nome do cache
        $cacheName = 'app_impresso_'.$id;
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda não tem cache
        if(!$result){  
            
            $node = $this->Node->find(
                'first',
                array(
                    'conditions' => array(
                        'Node.id' => $id,
                        'Node.status' => 1
                    ),
                    'fields' => array(
                        'Node.id',
                        'Node.title',
                        'Node.publish_start'
                    ),
                    'contain' => array(
                        'Taxonomy' => array(
                            'Term' => array(
                                'fields' => array(
                                    'id',
                                    'title'
                                )
                            )
                        )
                    )
                )
            );

            $pdf = '';
            if (isset($node['Multiattach']) && !empty($node['Multiattach'])) {
                //percorre os anexos
                foreach($node['Multiattach'] as $anexo){
                    //se for o PDF
                    if($anexo['Multiattach']['mime']=='application/pdf'){
                        $pdf = $anexo['Multiattach']['filename'];
                    }
                }
            }

            $imagensPdf = array();
            if (isset($node['PageFlip']) && !empty($node['PageFlip'])) {
                //se for o PDF
                $pdf = '/pageflip/pdfs/normal/'.$node['PageFlip']['id'].'/'.$node['PageFlip']['pdf'];
                $arrayImagens = json_decode($node['PageFlip']['images'], true);

                if (!empty($arrayImagens)) {
                    foreach ($arrayImagens as $key => $imagem) {
                        $imagensPdf[$key] = Router::url('/pageflip/pdfs/normal/'.$node['PageFlip']['id'].'/'.$imagem['image'], true);
                    }
                }
            }

            $result = array();
            $result['result'] = array(
                'id'        => $node['Node']['id'],
                'title'     => $node['Node']['title'],
                'date'      => date('d/m/Y', strtotime($node['Node']['publish_start'])),
                'pdf'       => Router::url("/fl/normal/{$pdf}", true),
                'imagens'   => $imagensPdf              
            );

            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($result);
            //guarda no cache
            Cache::write($cacheName, $result);
        }

        $this->response->body($result);
    }
}
