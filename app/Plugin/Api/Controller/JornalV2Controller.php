<?php

App::uses('ApiAppController', 'Api.Controller');

/**
 * Categoria Controller
 *
 * @category Controller
 * @author   Rodrigo Alves <alves@mswi.com.br>
 * @link     http://mswi.com.br
 */
class JornalV2Controller extends ApiAppController 
{
    public $name = 'JornalV2';
    public $components = array('Api.Util', 'Clube.Clube');
    public $uses = array('Api.Api');
    public $cacheName = 'AppJornal_';

	/**
	 * beforeFilter
	 * before everything
	 */
	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow(
            array(
                'index' 
            )
        );
    }
    
    public function index(){
        $this->autoRender = false;        

        //Monta o SQL
        $sql = "SELECT 
           Jornal.key,
           Jornal.value
        FROM settings AS Jornal 
        WHERE 
            Jornal.key LIKE 'Site.%' OR
            Jornal.key LIKE 'Youtube.%'
        ;";

        //prepara o nome do cache
        $cacheName = $this->cacheName.'index';
        //tenta pegar do cache
        $result = Cache::read($cacheName);
        //se ainda NÃO tem cache
        if(!$result){     
            //Executa a query
            $result = $this->Api->query($sql);
            $arrayResult = array();
            foreach($result as $res){
                $arrayTitle = explode('.', $res['Jornal']['key']);
                $arrayResult['Jornal'][$arrayTitle[1]] = $res['Jornal']['value'];
            }
            //informa o app sobre clube do assinante
            $arrayResult['Jornal']['clube'] = '';            
            //se está ativo o plugin de Clube
            if(CakePlugin::loaded('Clube')){
                $arrayResult['Jornal']['clube'] = 1;
            }
            //informa o app sobre youtube
            $arrayResult['Jornal']['youtube'] = '';            
            //se está ativo o youtube
            if($arrayResult['Jornal']['clube']){
                $arrayResult['Jornal']['youtube'] = $arrayResult['Jornal']['id_canal'];
            }            
            //O json_encode exige processamento do PHP, então vamos fazer 1 vez só e guardar no cache!
            $result = json_encode($arrayResult);
            //guarda no cache
            Cache::write($cacheName, $result);  
        }
        
        $this->response->body($result);

    }

}
