<?php

/**
 * V1
 */
CroogoRouter::connect('/api/noticias', array(
	'plugin' => 'Api', 'controller' => 'Noticias', 'action' => 'index'
));
CroogoRouter::connect('/api/impressos', array(
	'plugin' => 'Api', 'controller' => 'Impressos', 'action' => 'index'
));

/**
 * V2
 */

//Jornal
CroogoRouter::connect('/api/v2/jornal', array(
	'plugin' => 'Api', 
	'controller' => 'JornalV2', 
	'action' => 'index'
));
 //Users
CroogoRouter::connect('/api/v2/login', array(
	'plugin' => 'Api', 
	'controller' => 'UsersV2', 
	'action' => 'login'
));
CroogoRouter::connect('/api/v2/profile', array(
	'plugin' => 'Api', 
	'controller' => 'UsersV2', 
	'action' => 'profile'
));
CroogoRouter::connect('/api/v2/recuperar', array(
	'plugin' => 'Api', 
	'controller' => 'UsersV2', 
	'action' => 'recuperar'
));
//Notícias
CroogoRouter::connect('/api/v2/noticias', array(
	'plugin' => 'Api', 
	'controller' => 'NoticiasV2', 
	'action' => 'index'
));
CroogoRouter::connect('/api/v2/noticias/destaques', array(
	'plugin' => 'Api', 
	'controller' => 'NoticiasV2', 
	'action' => 'destaques'
));
CroogoRouter::connect('/api/v2/noticias/:id', array(
	'plugin' => 'Api', 
	'controller' => 'NoticiasV2', 
	'action' => 'view'
));
CroogoRouter::connect('/api/v2/noticias/search/:string', array(
	'plugin' => 'Api', 
	'controller' => 'NoticiasV2', 
	'action' => 'search'
));
CroogoRouter::connect('/api/v2/noticias/editoria/:editoria', array(
	'plugin' => 'Api', 
	'controller' => 'NoticiasV2', 
	'action' => 'editoria'
));


//Impressos
CroogoRouter::connect('/api/v2/impressos', array(
	'plugin' => 'Api', 
	'controller' => 'ImpressosV2', 
	'action' => 'index'
));
CroogoRouter::connect('/api/v2/impressos/:id', array(
	'plugin' => 'Api', 
	'controller' => 'ImpressosV2', 
	'action' => 'view'
));
CroogoRouter::connect('/api/v2/impressos/search/:string', array(
	'plugin' => 'Api', 
	'controller' => 'ImpressosV2', 
	'action' => 'search'
));

//Parceiros
CroogoRouter::connect('/api/v2/parceiros', array(
	'plugin' => 'Api', 
	'controller' => 'ParceirosV2', 
	'action' => 'index'
));
CroogoRouter::connect('/api/v2/parceiros/:id', array(
	'plugin' => 'Api', 
	'controller' => 'ParceirosV2', 
	'action' => 'view'
));
CroogoRouter::connect('/api/v2/parceiros/search/:string', array(
	'plugin' => 'Api', 
	'controller' => 'ParceirosV2', 
	'action' => 'search'
));
CroogoRouter::connect('/api/v2/parceiros/categoria/:categoria', array(
	'plugin' => 'Api', 
	'controller' => 'ParceirosV2', 
	'action' => 'categoria'
));
CroogoRouter::connect('/api/v2/parceiros-lista', array(
	'plugin' => 'Api', 
	'controller' => 'ParceirosV2', 
	'action' => 'lista'
));

//Descontos
CroogoRouter::connect('/api/v2/descontos', array(
	'plugin' => 'Api', 
	'controller' => 'DescontosV2', 
	'action' => 'index'
));
CroogoRouter::connect('/api/v2/descontos/:id', array(
	'plugin' => 'Api', 
	'controller' => 'DescontosV2', 
	'action' => 'view'
));
CroogoRouter::connect('/api/v2/descontos/categoria/:categoria', array(
	'plugin' => 'Api', 
	'controller' => 'DescontosV2', 
	'action' => 'categoria'
));
CroogoRouter::connect('/api/v2/descontos/parceiro/:id', array(
	'plugin' => 'Api', 
	'controller' => 'DescontosV2', 
	'action' => 'parceiro'
));

//Categoria
CroogoRouter::connect('/api/v2/categorias-lista', array(
	'plugin' => 'Api', 
	'controller' => 'CategoriasV2', 
	'action' => 'lista'
));

//Editorias
CroogoRouter::connect('/api/v2/editorias-lista', array(
	'plugin' => 'Api', 
	'controller' => 'EditoriasV2', 
	'action' => 'lista'
));