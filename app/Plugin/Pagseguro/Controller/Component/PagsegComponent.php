<?php
$pluginPath = App::pluginPath('Pagseguro');
require_once $pluginPath.DS.'vendor'.DS.'autoload.php';

\PagSeguro\Library::initialize();
\PagSeguro\Library::cmsVersion()->setName(Configure::read('Site.name'))->setRelease("1.0.0");
\PagSeguro\Library::moduleVersion()->setName(Configure::read('Site.name'))->setRelease("1.0.0");

class PagsegComponent extends Component {

    /**
     * 
     * @param \Controller $controller
     * @throws RuntimeException
     * @since 2.1
     */
    public function startup(\Controller $controller) {
        $config = Configure::read('Pagseguro');
        if(!empty($configs)) {
            throw new RuntimeException('Você precisa definir as configurações básicas do plugin "Pagseguro", leia o manual.');
        }
        //Se está para Sandbox
        if(Configure::read('Pagseguro.isSandbox')){
            \PagSeguro\Configuration\Configure::setEnvironment('sandbox');
        }else{
            \PagSeguro\Configuration\Configure::setEnvironment('production');
        }
        \PagSeguro\Configuration\Configure::setAccountCredentials(
            Configure::read('Pagseguro.email'),
            Configure::read('Pagseguro.token')
        );
        \PagSeguro\Configuration\Configure::setCharset('UTF-8');// UTF-8 or ISO-8859-1
        //\PagSeguro\Configuration\Configure::setLog(true, '/logpath/logFilename.log');
    }

    function curlExec($url, $post = NULL, array $header = array()){
        $ch = curl_init($url);        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);        
        if(count($header) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if($post !== null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post, '', '&'));
        }    
        //Ignore SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    
   
}
