<?php

class AssinaturasComponent extends PagsegComponent {

    public function assinar($dados){        
        //Se está usando cloudflare
        if(isset($_SERVER['HTTP_CF_CONNECTING_IP']) && !empty($_SERVER['HTTP_CF_CONNECTING_IP'])){
            //Pega o IP do usuario
            $userIp = $_SERVER['HTTP_CF_CONNECTING_IP'];
        }else{
            //Pega o IP do usuario
            $userIp = $_SERVER['REMOTE_ADDR'];
        }        
        //Seleciona o plano
        if(isset($dados['Pagseguro']['plano']) && $dados['Pagseguro']['plano'] == 'anual'){
            $plano = Configure::read('Pagseguro.codigo_plano_anual');
        }else if(isset($dados['Pagseguro']['plano']) && $dados['Pagseguro']['plano'] == 'semestral'){            
            $plano = Configure::read('Pagseguro.codigo_plano_semestral');
        }else{
            $plano = Configure::read('Pagseguro.codigo_plano_mensal');
        }    
        $preApproval = new \PagSeguro\Domains\Requests\DirectPreApproval\Accession();    
        $preApproval->setPlan($this->removeTracos($plano));
        $preApproval->setReference($dados['Pagseguro']['reference']);
        $preApproval->setSender()->setName($dados['Pagseguro']['nome']);//assinante
        $preApproval->setSender()->setEmail($dados['Pagseguro']['email']);//assinante
        $preApproval->setSender()->setIp($userIp);//assinante
        $preApproval->setSender()->setAddress()->withParameters(
            $dados['Pagseguro']['rua'], 
            $this->onlyNumbers($dados['Pagseguro']['numero']), 
            $dados['Pagseguro']['bairro'], 
            $this->onlyNumbers($dados['Pagseguro']['cep']), 
            $dados['Pagseguro']['cidade'], 
            $dados['Pagseguro']['uf'],
            'BRA'
        );

        $document = new \PagSeguro\Domains\DirectPreApproval\Document();
        $document->withParameters(
            'CPF', 
            $this->onlyNumbers($dados['Pagseguro']['documento'])
        );
        $preApproval->setSender()->setDocuments($document);
        $preApproval->setSender()->setPhone()->withParameters(
            $this->getAreaCode($dados['Pagseguro']['telefone']), 
            $this->getPhone($dados['Pagseguro']['telefone'])
        );
        $preApproval->setPaymentMethod()->setCreditCard()->setToken(htmlspecialchars($dados['Pagseguro']['token'])); //token do cartão de crédito gerado via javascript
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setName($dados['Pagseguro']['nome']); //nome do titular do cartão de crédito
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBirthDate($dados['Pagseguro']['nascimento']); //data de nascimento do titular do cartão de crédito
        $document = new \PagSeguro\Domains\DirectPreApproval\Document();
        $document->withParameters(
            'CPF', 
            $this->onlyNumbers($dados['Pagseguro']['documento'])
        ); 
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setDocuments($document);
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setPhone()->withParameters(
            $this->getAreaCode($dados['Pagseguro']['telefone']), 
            $this->getPhone($dados['Pagseguro']['telefone'])
        );
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBillingAddress()->withParameters(
            $dados['Pagseguro']['rua'], 
            $this->onlyNumbers($dados['Pagseguro']['numero']), 
            $dados['Pagseguro']['bairro'], 
            $this->onlyNumbers($dados['Pagseguro']['cep']), 
            $dados['Pagseguro']['cidade'], 
            $dados['Pagseguro']['uf'],
            'BRA'
        );

        try {
            $response = $preApproval->register(
                new \PagSeguro\Domains\AccountCredentials(Configure::read('Pagseguro.email'), Configure::read('Pagseguro.token')) // credencias do vendedor no pagseguro
            );
$this->log($response);
            return $response;
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    function removeTracos($string){
        return str_replace('-','', $string);
    }

    function onlyNumbers($string){
		return preg_replace("/[^0-9]/", '', $string);
	}

	function getAreaCode($phone){
		preg_match('#\((.*?)\)#', $phone, $match);
		return $match[1];
	}

	function getPhone($phone){
		$arrayPhone = explode(')',$phone);
		return $this->onlyNumbers($arrayPhone[1]);
	}

	function tratarData($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('/', '-', $valor);
			$resultValor = date('Y-m-d', strtotime($valorReplace));
		}else{
			$resultValor = date('d/m/Y', strtotime($valor));
		}
		return $resultValor;
	}

	function tratarValor($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('.', '', $valor);
			$valorReplace = str_replace(',', '.', $valorReplace);
			$resultValor = $valorReplace;
		}else{
			$resultValor = number_format($valor,2,',','.');
		}
		return $resultValor;
	}

}