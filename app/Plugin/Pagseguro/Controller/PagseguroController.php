<?php
App::uses('PagseguroAppController', 'Pagseguro.Controller');

class PagseguroController extends PagseguroAppController {
	public $name = 'Pagseguro';
    public $uses = array('Pagseguro.Pagseguro', 'Settings.Setting');
    public $components = array(
        'Pagseguro.Pagseg', 
        'Pagseguro.Planos',
        'Pagseguro.Assinaturas'
    );

	public function beforeFilter() 
	{
        parent::beforeFilter();
        $this->Auth->allow('retorno');
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions[] = 'assinatura_pagamento';
        $this->Security->unlockedActions[] = 'retorno';
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        header("access-control-allow-origin: https://pagseguro.uol.com.br");
	}

	public function admin_config() 
	{
		$this->set('title_for_layout','Pagseguro');     
        if (!empty($this->request->data)) {
        if($this->Setting->deleteAll(
            array('Setting.key LIKE' => 'Pagseguro.%')
            )){
                foreach($this->request->data['Pagseguro'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Pagseguro.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        } 

        //limpa o this->data
        $this->request->data = array();
        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Pagseguro.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                $this->request->data['Pagseguro'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
    }
    
    public function assinatura_pagamento(){ 

        //se está setado como expirado
        if(isset($_GET['status'])){
            //Se já houve tentativa de pagamento anterior
            if($_GET['status'] <> 0){
                $this->Flash->warning('Atenção! Ocorreu um erro na última tentativa de pagamento, tente novamente.');
            }else{
                $this->Flash->warning('Olá! Agora você precisa efetuar o pagamento para continuar lendo as notícias do '.Configure::read('Site.title'));
            }           
        }

        //Prepara a conexão com o Pagseguro     
        $PAGSEGURO_API_URL = Configure::read('Pagseguro.urlProducao');
        if(Configure::read('Pagseguro.isSandbox')){
            $PAGSEGURO_API_URL = Configure::read('Pagseguro.urlSandbox');
		}
        $params = array(
            'email' => Configure::read('Pagseguro.email'),
            'token' => Configure::read('Pagseguro.token')
        );
        $header = array();
        $response = $this->Pagseg->curlExec($PAGSEGURO_API_URL."/v2/sessions", $params, $header);
        $json = json_decode(json_encode(simplexml_load_string($response)));

        $this->set('sessionCode', $json->id);

        //se enviou o pagamento
        if(!empty($this->data)){
            //Gera uma referencia
            $this->request->data['Pagseguro']['reference'] = uniqid();
            $result = $this->Assinaturas->assinar($this->data);
            //se deu certo
            if(isset($result->code)){
                //mensagem de sucesso
                $this->Flash->success('Pronto! Agora é só aguardar a confirmação do pagamento!');

                //apaga algum possível registro já existente no banco de dados
                $this->Pagseguro->query("DELETE FROM pagseguros WHERE user_id = ".$this->Auth->user('id'));
                //Array para salvar
                $arraySave = array();
                $arraySave['id']         = null;
                $arraySave['user_id']    = $this->Auth->user('id');
                $arraySave['reference']  = $this->request->data['Pagseguro']['reference'];
                $arraySave['status']     = 0;
                $arraySave['code']       = $result->code;
                $this->Pagseguro->save($arraySave);
                //redireciona para a pagina de sucesso    
                $urlSucesso = '/';
                if(Configure::read('Pagseguro.redirect_sucesso')){
                    $urlSucesso = Configure::read('Pagseguro.redirect_sucesso');
                }
                $this->redirect($urlSucesso);
            }else{
                $result = json_decode($result);
                foreach($result->errors as $code => $erro){
                    //Se deu erro, exibe os erros
                    $this->Flash->error($code.' - '.$erro);
                } 
            }
        } else{
            $arrPlanos = array();
            $valor_mensal = Configure::read('Pagseguro.valor_mensal');
            $valor_sem = Configure::read('Pagseguro.valor_semestral');
            $valor_anual = Configure::read('Pagseguro.valor_anual');
            
            $frase_plano_mensal = 'Plano Mensal';
            $frase_plano_semestral = 'Plano Semestral';
            $frase_plano_anual = 'Plano Anual';

            if(Configure::read('Pagseguro.frase_plano_mensal')){
                $frase_plano_mensal = Configure::read('Pagseguro.frase_plano_mensal');
            }
            if(Configure::read('Pagseguro.frase_plano_semestral')){
                $frase_plano_semestral = Configure::read('Pagseguro.frase_plano_semestral');
            }
            if(Configure::read('Pagseguro.frase_plano_anual')){
                $frase_plano_anual = Configure::read('Pagseguro.frase_plano_anual');
            }

            if(!empty($valor_mensal)){
                $arrPlanos = array('mensal' => 'R$' . $valor_mensal . ' - ' . $frase_plano_mensal);
            }
            if(!empty($valor_sem)){
                $arrPlanos += array('semestral' => 'R$' . $valor_sem . ' - ' . $frase_plano_semestral);
            }
            if(!empty($valor_anual)){
                $arrPlanos += array('anual' => 'R$' . $valor_anual . ' - ' . $frase_plano_anual);
            }            
            //preenche o this data com os dados do usuario logado
            $this->request->data['Pagseguro']['nome']       = $this->Session->read('Auth.User.name');
            $this->request->data['Pagseguro']['telefone']   = $this->Session->read('Auth.User.telefone');
            $this->request->data['Pagseguro']['email']      = $this->Session->read('Auth.User.email');
            //preenche o this data com os valores dos planos
            $this->request->data['Pagseguro']['planos']     = $arrPlanos;
        }
    }

    public function retorno(){
        
        //se está retornando um resultado do transaction
        if(isset($_POST['notificationType']) && $_POST['notificationType'] != 'transaction'){
            return false;
        }
        $this->layout = 'ajax';        
        $this->autoRender = false;

        //Prepara a conexão com o Pagseguro     
        $PAGSEGURO_API_URL = Configure::read('Pagseguro.urlProducao');
        if(Configure::read('Pagseguro.isSandbox')){
            $PAGSEGURO_API_URL = Configure::read('Pagseguro.urlSandbox');
        }
        $notificationCode = $_POST['notificationCode'];
        $response = $this->Pagseg->curlExec($PAGSEGURO_API_URL.'/v2/transactions/notifications/'.$notificationCode.'?email='.Configure::read('Pagseguro.email').'&token='.Configure::read('Pagseguro.token'));
        
        $json = json_decode(json_encode(simplexml_load_string($response)));

        //atualiza o status do pagamento
        if(isset($json->reference) && !empty($json->reference)){
            //procura o registro desta referencia
            $pagamentos = $this->Pagseguro->find(
                'first',
                array(
                    'conditions' => array(
                        'Pagseguro.reference' => $json->reference
                    ),
                    'recursive' => -1
                )
            );

            //se encontrou
            if(!empty($pagamentos)){
                $this->Pagseguro->updateAll(
                    array(
                        'Pagseguro.status' => "'".$json->status."'",
                        'Pagseguro.modified' => "'".date('Y-m-d H:i:s')."'"              
                    ),
                    array(
                        'Pagseguro.id' => $pagamentos['Pagseguro']['id']
                    )
                );
            }            
        }
        return true;
    }

}