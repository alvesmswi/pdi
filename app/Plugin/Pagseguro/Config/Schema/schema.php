<?php 
class PagseguroSchema extends CakeSchema {

	public $name = 'Pagseguro';

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $pagseguros = array(
		'id' 				=> array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' 			=> array('type' => 'integer', 'null' => false),
		'reference' 		=> array('type' => 'string', 'length' => 255, 'null' => true),
		'status' 			=> array('type' => 'integer', 'length' => 11, 'null' => true),
		'code' 				=> array('type' => 'string', 'length' => 255, 'null' => true),
		'created' 			=> array('type' => 'timestamp', 'null' => true),
		'modified' 			=> array('type' => 'timestamp', 'null' => true),		
		'indexes' 			=> array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' 	=> array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
