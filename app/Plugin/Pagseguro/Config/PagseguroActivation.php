<?php
/**
 * Pagseguro Activation
 *
 * Activation class for Pagseguro plugin.
 * This is optional, and is required only if you want to perform tasks when your plugin is activated/deactivated.
 *
 * @package  Croogo
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class PagseguroActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeActivation(&$controller) {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {
		Configure::write('debug',1);

		//Cria as tabelas
		CakePlugin::load('Pagseguro');

		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		include_once(App::pluginPath('Pagseguro').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$PagseguroSchema = new PagseguroSchema();
		foreach ($PagseguroSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($PagseguroSchema, $table));
			}
		}

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('Pagseguro/Pagseguro/admin_config');
		$controller->Croogo->addAco('Pagseguro/Pagseguro/admin_transactions', array('gerente'));
		$controller->Croogo->addAco('Pagseguro/Pagseguro/assinatura_pagamento', array('registered', 'assinante', 'cortesia', 'editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Pagseguro/Pagseguro/retorno', array('public'));

		//Configurações basicas
		$controller->Setting->write('Pagseguro.email','suporte@mswi.com.br',array('description' => 'E-mail cadastrado no Pagseguro','editable' => 1));
		$controller->Setting->write('Pagseguro.token','52E71D76D8B74A03B082528D734934FA',array('description' => 'Token informado no Pagseguro','editable' => 1));
		$controller->Setting->write('Pagseguro.codigo_plano_mensal','5232CD95E5E59E68845E4FB2DE7D410E',array('description' => 'Código do plano MENSAL de assinaturas criado no Pagseguro','editable' => 1));
		$controller->Setting->write('Pagseguro.codigo_plano_semestral','',array('description' => 'Código do plano SEMESTRAL de assinaturas criado no Pagseguro','editable' => 1));
		$controller->Setting->write('Pagseguro.codigo_plano_anual','',array('description' => 'Código do plano ANUAL de assinaturas criado no Pagseguro','editable' => 1));
		$controller->Setting->write('Pagseguro.redirect_sucesso','/',array('description' => 'URL da página de retorno de sucesso','editable' => 1));
		$controller->Setting->write('Pagseguro.urlProducao','https://ws.pagseguro.uol.com.br',array('description' => 'URL da API do PagSeguro em Produção','editable' => 1));
		$controller->Setting->write('Pagseguro.urlSandbox','https://ws.sandbox.pagseguro.uol.com.br',array('description' => 'URL da API do PagSeguro em Sandbox (testes)','editable' => 1));
		$controller->Setting->write('Pagseguro.isSandbox','0',array('description' => 'Sandbox (testes)','editable' => 1));

		Cache::clear(false, '_cake_model_');
	}

/**
 * onDeactivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

/**
 * Called after deactivating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onDeactivation(&$controller) {
		// ACL: remove ACOs with permissions
		$controller->Croogo->removeAco('Pagseguro'); // PagseguroController ACO and it's actions will be removed
	}
}
