<?php
/**
 * Admin menu (navigation)
 */
CroogoNav::add(
	'sidebar', 
	'settings.children.pagseguro', 
	array(
		'icon' => array('money', 'large'),
		'title' => 'Pagseguro',
		'url' => array(
			'admin' => true,
			'plugin' => 'pagseguro',
			'controller' => 'pagseguro',
			'action' => 'config'
		)
	)
);