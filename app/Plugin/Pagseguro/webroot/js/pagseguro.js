//exibe e oculta os dados do cartão de crédito       
$('input[name="data[Pagseguro][tipo]"]').change(function () {
    if ($('input[name="data[Pagseguro][tipo]"]:checked').val() === "creditCard") {
        $('#divCC').show();
    } else {
        $('#divCC').hide();
    }
});
//mascaras
$(".date").mask("99/99/9999",{placeholder:"__/__/____"});
$(".cpf").mask("999.999.999.99",{placeholder:"___.___.___-__"});
$(".celular").mask("(99)99999-9999",{placeholder:"(__)_____-____"});
$(".validade").mask("99/9999",{placeholder:"__/____"});
$(".cep").mask("99.999-999",{placeholder:"__.___-___"});