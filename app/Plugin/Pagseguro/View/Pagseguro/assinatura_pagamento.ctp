
<?php 
echo $this->Html->css('Pagseguro.pagseguro');
?>
<div class="row margin-top margin-bottom">
    <div class="col-sm-12 border-right">
        <div class="o-login_content">
            <header class="o-header">
                <span class="o-header_title o-header_title--large">Efetuar o pagamento</span>
            </header>
            <?php 
            echo $this->Form->create('Pagseguro');

            echo $this->Form->hidden('user_id', array('value'=>AuthComponent::user('id')));
            echo $this->Form->input('nome', array('label'=>'Nome completo', 'class'=>'form-control', 'div'=>'form-group col-lg-12', 'required'=>true));             
            echo $this->Form->input('telefone', array('label'=>'Celular', 'class'=>'form-control celular', 'div'=>'form-group col-lg-6', 'required'=>true));            
            echo $this->Form->input('nascimento', array('label'=>'Data de Nascimento', 'class'=>'date form-control', 'div'=>'form-group col-lg-6', 'required'=>true));
            echo $this->Form->input('documento', array('label'=>'CPF', 'class'=>'form-control cpf', 'div'=>'form-group col-lg-6', 'required'=>true));
            echo $this->Form->input('email', array('label'=>'Seu e-mail', 'type'=>'email','class'=>'form-control', 'div'=>'form-group col-lg-6', 'required'=>true));  
            echo $this->Form->input('rua', array('label'=>'Nome da Rua', 'class'=>'form-control', 'div'=>'form-group col-lg-10', 'required'=>true));
            echo $this->Form->input('numero', array('label'=>'Número','type'=>'number', 'class'=>'form-control', 'div'=>'form-group col-lg-2', 'required'=>true));
            echo $this->Form->input('bairro', array('label'=>'Bairro', 'class'=>'form-control', 'div'=>'form-group col-lg-6', 'required'=>true));
            echo $this->Form->input('cidade', array('label'=>'Cidade', 'class'=>'form-control', 'div'=>'form-group col-lg-6', 'required'=>true));
            echo $this->Form->input('uf', 
                array(
                    'label'=>'Estado', 
                    'class'=>'form-control', 
                    'div'=>'form-group col-lg-6',
                    'required'=>true,
                    'options' => array(
                        'AC'=>'Acre',
                        'AL'=>'Alagoas',
                        'AM'=>'Amazonas',
                        'AP'=>'Amapá',
                        'BA'=>'Bahia',
                        'CE'=>'Ceará',
                        'DF'=>'Distrito Federal',
                        'ES'=>'Espírito Santo',
                        'GO'=>'Goiás',
                        'MA'=>'Maranhão',
                        'MT'=>'Mato Grosso',
                        'MS'=>'Mato Grosso do Sul',
                        'MG'=>'Minas Gerais',
                        'PA'=>'Pará',
                        'PB'=>'Paraíba',
                        'PR'=>'Paraná',
                        'PE'=>'Pernambuco',
                        'PI'=>'Piauí',
                        'RJ'=>'Rio de Janeiro',
                        'RN'=>'Rio Grande do Norte',
                        'RO'=>'Rondônia',
                        'RS'=>'Rio Grande do Sul',
                        'RR'=>'Roraima',
                        'SC'=>'Santa Catarina',
                        'SE'=>'Sergipe',
                        'SP'=>'São Paulo',
                        'TO'=>'Tocantins'
                    )
                )
            );
            echo $this->Form->input('cep', array('label'=>'CEP', 'class'=>'form-control cep', 'div'=>'form-group col-lg-6', 'required'=>true));
          
            //cartão de crédito
            echo $this->Form->hidden('brand');
            echo $this->Form->hidden('token');
            echo $this->Form->hidden('senderHash');     
            echo $this->Form->input(
                'ccNumero', 
                array(
                    'label'=>'Cartão de Crédito', 
                    'class'=>'form-control', 
                    'div'=>'form-group col-lg-6',
                    'type' => 'number',
                    'required' => true
                )
            );
            echo $this->Form->input(
                'ccValidade', 
                array(
                    'label'=>'Validade', 
                    'class'=>'form-control validade', 
                    'div'=>'form-group col-lg-4',
                    'required' => true
                )
            );
            echo $this->Form->input(
                'ccCvv', 
                array(
                    'label'=>'CVV', 
                    'class'=>'form-control', 
                    'div'=>'form-group col-lg-2',
                    'type' => 'number',
                    'required' => true
                )
            );
            ?>         
            <div class="text-right">
                <button id="pagseguroSubmit" type="submit" class="btn btn-lg btn-primary">Confirmar Pagamento</button>
            </div>
            <?php echo $this->Form->end();?>   
        </div>
    </div>
</div>

<?php 
echo $this->Html->script(array(
    'Pagseguro.jquery.min', 
    'Pagseguro.jquery.maskedinput.min',
    'Pagseguro.pagseguro', 
));
?>
<?php 
//Se está em sandbox
if(Configure::read('Paywall.isSandbox')) { ?>
    <script src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<?php }else{ ?>
    <script src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<?php } ?>
<script>
    $("#PagseguroCcNumero, #PagseguroCcValidade, #PagseguroCcCvv").keyup(function(){
        getBrand();
    });

    function getBrand(){        
        var cardNumber = $("#PagseguroCcNumero").val();        
        //enquanto não quando chegar no ultimo numero do cartão, não faz nada
        if(cardNumber.length < 16){
            return;
        } 
        PagSeguroDirectPayment.getBrand({
            cardBin: cardNumber.replace(/ /g,''),
            success: function(json){
                var brand = json.brand.name;
                $("#PagseguroBrand").val(brand);
            }, error: function(json){
                console.log(json);
            }, complete: function(json){
                //Complete
            }
        });
        var param = {
            cardNumber: $("#PagseguroCcNumero").val().replace(/ /g,''),
            brand: $("#PagseguroBrand").val(),
            cvv: $("#PagseguroCcCvv").val(),
            expirationMonth: $("#PagseguroCcValidade").val().split('/')[0],
            expirationYear: $("#PagseguroCcValidade").val().split('/')[1],
            success: function(json){
                var token = json.card.token;
                $("#PagseguroToken").val(token);
                var senderHash = PagSeguroDirectPayment.getSenderHash();
                $("#PagseguroSenderHash").val(senderHash);
            }, error: function(json){
                console.log(json);
            }, complete:function(json){
                //quando completar
            }
        }
        PagSeguroDirectPayment.createCardToken(param);
    }

    //Quando for enviar o form
    $("#PagseguroAssinaturaPagamentoForm").submit(function(event){
        efetuarPagamento();
    });
            
    function efetuarPagamento(){
        //desabilita o botão
        $("#pagseguroSubmit").prop( "disabled", true );
    };

    jQuery(function($) {
        PagSeguroDirectPayment.setSessionId('<?php echo $sessionCode;?>');
    });
</script>

