<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Pagseguro', array(
    'admin' => true,
    'plugin' => 'Pagseguro',
    'controller' => 'Pagseguro',
    'action' => 'config',
  ));

$this->append('form-start', $this->Form->create('Pagseguro', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
       echo $this->Form->input('Pagseguro.email', array(
            'label' => 'E-mail cadastrado no Pagseguro',
            'type'  => 'email',
            'required'	=> true
        )) .
        $this->Form->input('Pagseguro.token', array(
            'label' => 'Token informado no Pagseguro',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Pagseguro.codigo_plano_mensal', array(
            'label' => 'Código do plano MENSAL de assinaturas criado no Pagseguro',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.codigo_plano_semestral', array(
            'label' => 'Código do plano SEMESTRAL de assinaturas criado no Pagseguro',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.codigo_plano_anual', array(
            'label' => 'Código do plano ANUAL de assinaturas criado no Pagseguro',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.valor_mensal', array(
            'label' => 'Valor do Plano Mensal',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.valor_semestral', array(
            'label' => 'Valor do Plano Semestral',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.valor_anual', array(
            'label' => 'Valor do Palno Anual',
            'type'  => 'text'
        )) .
        $this->Form->input('Pagseguro.redirect_sucesso', array(
            'label' => 'URL da página de retorno de sucesso',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Pagseguro.urlProducao', array(
            'label' => 'URL da API do PagSeguro em Produção',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Pagseguro.urlSandbox', array(
            'label' => 'URL da API do PagSeguro em Sandbox (testes)',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Pagseguro.isSandbox', array(
            'label' => 'Sandbox (testes)',
            'type'  => 'checkbox'
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());