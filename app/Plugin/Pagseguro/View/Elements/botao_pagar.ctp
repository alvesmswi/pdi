<?php 
//pr($checkout);
//se tem items
if(!empty($checkout['items'])){
    //começa criando o formulário
    echo $this->Form->create(
        'Pagseguro', 
        array(
            'url' => array(
                'plugin' => 'pagseguro',
                'controller' => 'pagseguro',
                'action' => 'carrinho_add'
            )
        )
    );
    echo $this->Form->hidden('Pagseguro.user_id', array('value' => AuthComponent::user('id')));
    echo $this->Form->hidden('Pagseguro.tipo', array('value' => $checkout['tipo']));
    //percorre os items
    foreach($checkout['items'] as $key=>$item){
        echo $this->Form->hidden('Pagseguro.items.'.$key.'.id', array('value' => $item['id']));
        echo $this->Form->hidden('Pagseguro.items.'.$key.'.descricao', array('value' => $item['descricao']));
        echo $this->Form->hidden('Pagseguro.items.'.$key.'.valor', array('value' => $this->Mswi->tratarValor($item['valor'])));
        echo $this->Form->hidden('Pagseguro.items.'.$key.'.quantidade', array('value' => $item['quantidade']));
    }  
    ?>
        <button type="submit" class="btn btn-lg btn-primary" style="width:100%;">Pagar Agora</button>
    <?
    echo $this->Form->end();
}
?>