<ul class="breadcrumb">
    <li>
		<?php echo $this->Html->link('Enquetes', array('action'=>'index'));?>
	</li>
</ul>
<h3>Enquetes</h3>

<div class="row-fluid">
	<div class="span12 actions">
		<ul class="nav-buttons">
			<li><a href="/admin/polls/polls/add/#/edit_poll/0" method="get" class="btn btn-success">Nova Enquete</a></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<table cellpadding="0" cellspacing="0"  class="table table-striped table-bordered table-condensed">
			<tr>
				<th><?php echo $this->Paginator->sort('id');?></th>
				<th><?php echo $this->Paginator->sort('question', 'Questão');?></th>
				<th><?php echo $this->Paginator->sort('description', 'Descrição');?></th>
				<th><?php echo $this->Paginator->sort('status', 'Ativo');?></th>
				<th class="actions" style="text-align:center;width:250px;"></th>
			</tr>
			<?php
			foreach ($polls as $poll): ?>
			<tr>
				<td><?php echo h($poll['Poll']['id']); ?>&nbsp;</td>
				<td><?php echo h($poll['Poll']['question']); ?>&nbsp;</td>
				<td><?php echo h($poll['Poll']['description']); ?>&nbsp;</td>
				<td><?php echo ((int)$poll['Poll']['status'])?('Sim'):('Não');?></td>
				<td class="actions" style="text-align:center;">
					<?php
						$actions = array();
						$item = $poll;
						$modelClass = 'Poll';

						$actions[] = $this->Croogo->adminRowAction('',
							array('action' => 'view', $item[$modelClass]['id']),
							array('icon' => 'bar-chart', 'tooltip' => 'Estatísticas')
						);
						$actions[] = $this->Croogo->adminRowAction('',
							'/admin/polls/polls/edit/'.$item[$modelClass]['id'].'/#/edit_poll/'.$item[$modelClass]['id'],
							array('icon' => 'pencil', 'tooltip' =>  'Editar')
						);
						$actions[] = $this->Croogo->adminRowActions($item[$modelClass]['id']);
						$actions[] = $this->Croogo->adminRowAction('',
							array(
								'action' => 'delete',
								$item[$modelClass]['id'],
							),
							array(
								'icon' => 'trash',
								'tooltip' => 'Excluir'
							),
							'Tem certeza que deseja EXCLUIR este item?');

						print $this->Html->div('item-actions', implode(' ', $actions));
					?>


				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<?php if ($pagingBlock = $this->fetch('paging')): ?>
			<?php echo $pagingBlock; ?>
		<?php else: ?>
			<?php if (isset($this->Paginator) && isset($this->request['paging'])): ?>
				<div class="pagination">
					<ul>
						<?php echo $this->Paginator->first('< ' . __d('croogo', 'Primeiro')); ?>
						<?php echo $this->Paginator->prev('< ' . __d('croogo', 'Anterior')); ?>
						<?php echo $this->Paginator->numbers(); ?>
						<?php echo $this->Paginator->next(__d('croogo', 'Próximo') . ' >'); ?>
						<?php echo $this->Paginator->last(__d('croogo', 'Último') . ' >'); ?>
					</ul>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
