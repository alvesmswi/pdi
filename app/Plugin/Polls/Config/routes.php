<?php

CroogoRouter::connect('/enquete/:slug', 
    array(
        'plugin' => 'Polls', 
        'controller' => 'Polls', 
        'action' => 'enquete'
    ),
	array(
		'pass' => array(
			'slug'
		)
	)
);
