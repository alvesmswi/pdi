<?php 
class PollsSchema extends CakeSchema {

	public $name = 'Polls';

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $polls = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'question' => array('type' => 'string', 'length' => 255, 'null' => true),
		'description' => array('type' => 'string', 'length' => 255, 'null' => true),
		'published' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'timestamp', 'null' => true),
		'modified' => array('type' => 'timestamp', 'null' => true),
		'status' => array('type' => 'integer', 'null' => true, 'default' => 1),
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $poll_options = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'poll_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'option' => array('type' => 'string', 'length' => 100, 'null' => true),
		'vote_count' => array('type' => 'integer', 'null' => true, 'default' => null),
		'ordered' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'timestamp', 'null' => true),
		'modified' => array('type' => 'timestamp', 'null' => true),
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
			'poll_id' => array('column' => array('poll_id'))
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	public $poll_votes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'poll_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'poll_option_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'timestamp', 'null' => true),
		'modified' => array('type' => 'timestamp', 'null' => true),
		'indexes' => array(
			'id' => array('column' => array('id'), 'unique' => true),
			'poll_id' => array('column' => array('poll_id')),
			'poll_option_id' => array('column' => array('poll_option_id'))
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

}
