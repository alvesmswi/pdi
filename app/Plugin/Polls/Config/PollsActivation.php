<?php
/**
 * Polls Activation
 *
 * Activation class for Polls plugin.
 * This is optional, and is required only if you want to perform tasks when your plugin is activated/deactivated.
 *
 * @package  Croogo
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class PollsActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeActivation(&$controller) {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {
		Configure::write('debug',1);

		//Cria as tabelas
		CakePlugin::load('Polls');
		App::import('Model', 'CakeSchema');
		App::import('Model', 'ConnectionManager');
		
		include_once(App::pluginPath('Polls').DS.'Config'.DS.'Schema'.DS.'schema.php');
		$db = ConnectionManager::getDataSource('default');
		$tables = $db->listSources();
		$CakeSchema = new CakeSchema();
		$PollsSchema = new PollsSchema();
		foreach ($PollsSchema->tables as $table => $config) {
			if (!in_array($table, $tables)) {
				$db->execute($db->createSchema($PollsSchema, $table));
			}
		}

		//verifica a região
		$controller->loadModel('Region');
		$region = $controller->Region->find('first', array('conditions'=>array('Region.alias'=>'right')));
		//se a região existe
		if(!empty($region)){
			//Cria o bloco
			$controller->loadModel('Block');
			$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'enquete')));
			//se o Bloco não existe
			if(empty($block)){
				$controller->Block->create();
				$controller->Block->set(array(
					'visibility_roles' => '',
					'visibility_paths' => '',
					'region_id'        => $region['Region']['id'],
					'title'            => 'Enquete',
					'alias'            => 'enquete',
					'body'             => '[element:enquete plugin="Polls"]',
					'show_title'       => 0,
					'status'           => 1
				));
				$controller->Block->save();
			}
		} 

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('Polls/Polls/admin_index', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Polls/Polls/admin_view', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Polls/Polls/admin_add', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Polls/Polls/admin_edit', array('editor', 'colunista', 'gerente'));
		$controller->Croogo->addAco('Polls/Polls/admin_delete', array('gerente'));
		$controller->Croogo->addAco('Polls/Polls/index', array('registered', 'public'));
		$controller->Croogo->addAco('Polls/Polls/get_poll', array('registered', 'public'));
		$controller->Croogo->addAco('Polls/Polls/get_answers', array('registered', 'public'));
		$controller->Croogo->addAco('Polls/Polls/submit_poll', array('registered', 'public'));

		Cache::clear(false, '_cake_model_');
	}

/**
 * onDeactivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

/**
 * Called after deactivating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onDeactivation(&$controller) {
		// ACL: remove ACOs with permissions
		$controller->Croogo->removeAco('Polls'); // PollsController ACO and it's actions will be removed
	}
}
