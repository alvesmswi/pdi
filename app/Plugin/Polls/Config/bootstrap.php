<?php

Croogo::hookRoutes('Polls');

/**
 * Admin menu (navigation)
 */
CroogoNav::add('polls', array(
	'icon' => array('bar-chart', 'large'),
	'title' => 'Enquetes',
	'weight' => 11,
	'url' => array(
		'admin' => true,
		'plugin' => 'polls',
		'controller' => 'polls',
		'action' => 'index',
	),
));
