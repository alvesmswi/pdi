<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Agência FolhaPress', array(
    'admin' => true,
    'plugin' => 'folhapress',
    'controller' => 'folhapress',
    'action' => 'settings',
  ));

$this->append('form-start', $this->Form->create('folhapress', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#setting-ftp');
$this->end();

$this->start('tab-content');

    echo $this->Html->tabStart('setting-ftp');
       echo $this->Form->input('Folhapress.token', array(
            'label' => 'Token',
            'type'  => 'text',
            'required'	=> true
        )) .
        /*$this->Form->input('Folhapress.host', array(
            'label' => 'Host do endereço',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Folhapress.porta', array(
            'label' => 'Porta de acesso',
            'type'  => 'number',
            'required'	=> true
        )) .
        $this->Form->input('Folhapress.usuario', array(
            'label' => 'Usuário / Login',
            'type'  => 'text',
            'required'	=> true
        )).*/
        $this->Form->input('Folhapress.jornalista', array(
            'label' => 'Nome do Jornalista',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Folhapress.user_id', array(
            'label' => 'User ID',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Folhapress.taxonomy_id', array(
            'label' => 'Taxonomy ID',
            'type'  => 'text',
            'required'	=> true
        )).
        $this->Form->input('Folhapress.diretorio', array(
            'label' => 'Diretório que será baixado os XMLs',
            'type'  => 'text',
            'help'	=> 'O endereço informado é relativo à pasta "public", por exemplo: "folhapress/xmls"',
            'required'	=> true
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->Form->unlockField('folhapressTermos.*');
$this->append('form-end', $this->Form->end());