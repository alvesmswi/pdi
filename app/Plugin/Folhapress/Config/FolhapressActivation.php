<?php
class FolhapressActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        $controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Folhapress.token',
			'value' =>       '',
			'title' =>       'Token',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));
		
		/*$controller->Setting->create();
		$controller->Setting->save(array(
			'key' =>         'Folhapress.usuario',
			'value' =>       '',
			'title' =>       'Usuário para acessar o FTP',
			'description' => '',
			'input_type' =>  'text',
			'editable' =>    0,
			'weight' =>      100,
			'params' =>      '',
		));*/
		
    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Folhapress');
	}
}