<?php
Croogo::hookRoutes('folhapress');

CroogoNav::add('settings.children.Folhapress',array(
	'title' => __('Folhapress'),
	'url' => array('plugin' => 'folhapress', 'controller' => 'folhapress', 'action' => 'admin_index'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('folhapress');
    
?>