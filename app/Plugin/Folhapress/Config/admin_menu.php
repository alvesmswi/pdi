<?php
CroogoNav::add('sidebar', 'settings.children.Folhapress', array(
	'icon' => 'list',
	'title' => 'Folhapress',
	'url' => array(
		'admin' => true,
		'plugin' => 'folhapress',
		'controller' => 'folhapress',
		'action' => 'settings',
	),
	'weight' => 40,
	'children' => array(
		'settings' => array(
			'title' => 'Settings',
			'url' => array(
				'admin' => true,
				'plugin' => 'folhapress',
				'controller' => 'folhapress',
				'action' => 'settings'
			)
		),
		'importar' => array(
			'title' => 'Importar',
			'url' => array(
				'admin' => true,
				'plugin' => 'folhapress',
				'controller' => 'folhapress',
				'action' => 'index'
			)
		)
	)
));