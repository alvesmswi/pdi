<?php
App::uses('AppModel', 'Model');
App::uses('String', 'Utility');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class Folhapress extends AppModel
{
	public $useTable = false;
	public $name 		= 'Folhapress';

	function slug($str, $espacos = '-'){
		$str = strtolower(Inflector::slug($str));
		$str = str_replace('_', $espacos, $str);
		return $str;
	}

  	private function getTermId($term, $vocabularyId = '1') 
	{
		$slugTerm 	= strtolower(Inflector::slug($term, '-'));
		if($slugTerm == 'esporte'){
			$slugTerm = 'esportes';
		}
		$Taxonomy = new Taxonomy;
		$term_find 	= $Taxonomy->Term->findBySlug($slugTerm, array('id', 'slug'));

		if (!empty($term_find)) {
			if (!empty($term_find['Vocabulary'])) {
				$taxonomy_id = null;
				foreach ($term_find['Vocabulary'] as $key => $taxonomy) {
					$taxonomy_id = $taxonomy['Taxonomy']['id'];
				}
				return $taxonomy_id;
			}else{
				//cria um taxononomies TAXONOMIES
				$termId = $term_find['Term']['id'];
				$query = "INSERT INTO taxonomies (term_id, vocabulary_id, lft, rght) VALUES ($termId,$vocabularyId,0,0);";
				$this->query($query);
			}			
		} else {
			$arrayTerm['Taxonomy'] = array(
				'id' 			=> null,
				'parent_id' 	=> null,
				'vocabulary_id' => $vocabularyId
			);
			$arrayTerm['Term'] = array(
				'id' 			=> null,
				'title' 		=> $term,
				'slug' 			=> $slugTerm,
				'description' 	=> null
			);

			if ($Taxonomy->Term->add($arrayTerm, $vocabularyId)) {
				return $this->getTermId($term, $vocabularyId);
			} else {
				return false;
			}
		}
	}

	function preparaArray($dados){
		$arraySave = array();
		//se não está vazio
		if(!empty($dados)){
			//tansforma em array
			$arrayNews = (array)$dados->channel;

			foreach($arrayNews['item'] as $key => $noticia){
				$noticia = (array)$noticia;

				//trata a data e hora
				$dataHora = date('Y-m-d H:i:s', strtotime($noticia['pubDate']));
				$slug = str_replace('_', '-', strtolower(Inflector::slug(trim((string)$noticia['title']))));
				$corpo = $this->preparaCorpo((string)$noticia['description']);

				//Prepara o array para salvar
				$arraySave[$key]['Node'] = array();
				$arraySave[$key]['Node']['id'] 				= null;
				$arraySave[$key]['Node']['user_id'] 		= Configure::read('Folhapress.user_id');
				$arraySave[$key]['Node']['title'] 			= (string)$noticia['title'];
				$arraySave[$key]['Node']['folhapress_id'] 	= (string)$noticia['guid'];
				$arraySave[$key]['Node']['slug'] 			= $slug;
				$arraySave[$key]['Node']['body'] 			= $corpo;				
				$arraySave[$key]['Node']['publish_start'] 	= $dataHora;
				$arraySave[$key]['Node']['created'] 		= $dataHora;
				$arraySave[$key]['Node']['modified'] 		= $dataHora;
				$arraySave[$key]['Node']['status'] 			= '1';
				$arraySave[$key]['Node']['type'] 			= 'noticia';
				
				/* -- noticium_details -- */
				$arraySave[$key]['NoticiumDetail']['jornalista'] 	= Configure::read('Folhapress.jornalista');
				$arraySave[$key]['NoticiumDetail']['chapeu'] 		= (string)$noticia['editoria'];
				
				//Taxonomia
				$arraySave[$key]['TaxonomyData'][1] = $this->getTermId((string)$noticia['editoria'], Configure::read('Folhapress.taxonomy_id'));

			}
			
		}
		return $arraySave;
	}

	private function preparaCorpo($integra)
	{
		return join('',array_reduce(explode("\n", $integra), function($carry, $item) 
		{
			if (strlen($item))
			{
				$item = '<p>' . $item . '</p>';
				$carry[] = $item;
			}
			
			return $carry;
		}, []));
	}


	public function get_news(){
		//configurações de serviço
		$token = Configure::read('Folhapress.token') ; //token individual de acesso
		$mnemonic = "" ; //(opcional) código de editoria a ser retornada, ex: "COTX", se vazio irá retornar
		//url de serviço
		$url = "https://folhapress.folha.com.br/sng" ;
		//objeto de acesso
		$curl = curl_init() ;
		//configurações de objeto
		curl_setopt( $curl , CURLOPT_URL , $url ) ;
		curl_setopt( $curl , CURLOPT_POST , true ) ;
		curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;
		curl_setopt( $curl , CURLOPT_FAILONERROR, true ) ;
		curl_setopt( $curl , CURLOPT_POSTFIELDS, array( "token" => $token));
		//requisição
		$result = curl_exec( $curl ) ;

		//libera recursos
		curl_close( $curl ) ;

		//criar o arquivo
		$xmlsDir = WWW_ROOT.Configure::read('Folhapress.diretorio');
		$date = new DateTime();
		$xmlNome = $date->getTimestamp();
		$file = new File($xmlsDir.DS.$xmlNome.'.xml', true, 0775);
		$file->write($result,'w',true);
		$file->close();
		return true;
	}
	
}
?>