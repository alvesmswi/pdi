<?php
App::uses('FolhapressAppController', 'Folhapress.Controller');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

  class FolhapressController extends FolhapressAppController {

    public $name = 'Folhapress';
      
	public $uses = array('Folhapress.Folhapress','Settings.Setting', 'Nodes.Node');

    public function beforeFilter() {   
      	parent::beforeFilter();
		$this->Auth->allow('importar','get_news');
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
		$this->Security->authCheck = false;
    }
	
	public function admin_settings()
    {

      	$this->set('title_for_layout', 'Configuração de Agência Estado');

        if (!empty($this->request->data)) {
            if($this->Setting->deleteAll(
                array('Setting.key LIKE' => 'Folhapress.%')
            )){
                foreach($this->request->data['Folhapress'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Folhapress.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        }

        //limpa o this->data
        $this->request->data = array();

       	$arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Folhapress.%'),'recursive' => -1)
        );

        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                 $this->request->data['Folhapress'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
	}


	public function admin_index()
    {
		$this->loadModel('Taxonomy');

        //procura mais noticias
        $this->Folhapress->get_news();
            
		//monta o diretorio para buscar os XMLs
		$diretorio = Configure::read('Folhapress.diretorio')?Configure::read('Folhapress.diretorio'):'folhapress';
		$xmlsDIr = WWW_ROOT.$diretorio.DS;
		$dir = new Folder($xmlsDIr);
		//procura apenas os XMLs
		$files = $dir->find('.*\.xml');
		
		$this->set('xmlsDIr',$xmlsDIr);
		$this->set('files',$files);
    }
	
    public function importar($quantidade = 1, $cron = true)
    {
        $this->autoRender = false;

        //monta o diretorio para buscar os XMLs
		$diretorio = Configure::read('Folhapress.diretorio')?Configure::read('Folhapress.diretorio'):'Folhapress';
		$xmlsDIr = WWW_ROOT.$diretorio.DS;
		$dir = new Folder($xmlsDIr);
		//procura apenas os XMLs
        $files = $dir->find('.*\.xml');
        $totalImportado = 0;
		//se encontrou algum arquivo
		if(!empty($files)){
			
			//percore os arquivos
			foreach($files as $file){
				//le o arquivo XMLs
				try {
					$xml = Xml::build($xmlsDIr.DS.$file);
					$totalImportado = $this->saveNoticia($xml);
				} catch (XmlException $e) {
					//throw new InternalErrorException();
				}

				//move o arquivos
				$arq1 = new File($xmlsDIr.DS.$file);
				if ($arq1->exists()) {
					//Copia o arquivo
					$arq2 = new Folder($xmlsDIr.DS.'importados', true);
					$arq1->copy($arq2->path . DS . $arq1->name);
					//deleta o arquivo imoprtado
					$arq1->delete($xmlsDIr.DS.$file);
				}
			}
		//Se não	
		}else{
			//procura mais noticias
			$this->Folhapress->get_news();
		}
        
        if($cron){
			return $totalImportado.' noticias importadas.';
		}else{
			$this->Session->setFlash($totalImportado.' noticias importadas.', 'flash', array('class' => 'success'));
			$this->redirect('/admin/folhapress');
		}
    }	

    private function saveNoticia($data, $typeAlias = 'noticia') {

        $Node 					= $this->Node;

		$arrayNodes 	= $this->Folhapress->preparaArray($data);
		$importadas = 0;
		if(!empty($arrayNodes)){
			foreach($arrayNodes as $materia){
				//Se salvar corretamente o node
				if ($Node->saveNode($materia, $typeAlias)) {
					Croogo::dispatchEvent('Controller.Nodes.afterAdd', $this, array('data' => $materia));
					$importadas++;
				}
			}
		}
		return $importadas;
	}
	
  }
?>