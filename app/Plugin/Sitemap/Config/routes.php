<?php

//Indices
CroogoRouter::connect('/sitemap/sitemap', array('plugin' => 'sitemap', 'controller' => 'sitemap', 'action' => 'indice'));
//Sitemaps
CroogoRouter::connect('/sitemap/sitemap_editorias', array('plugin' => 'sitemap', 'controller' => 'sitemap', 'action' => 'editorias'));

Router::parseExtensions('xml');
?>