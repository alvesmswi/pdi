<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo Router::url('/', true); ?> </loc>
        <lastmod><?php echo trim($this->Time->toAtom(time())); ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>
    <?php
    foreach ($nodes as $node){
        ?>
        <url>
            <loc> <?php echo Router::url('/', true) . ltrim($node['Node']['path'], '/'); ?> </loc>
            <lastmod> <?php echo trim($this->Time->toAtom(time())); ?> </lastmod>
            <priority>0.5</priority>
            <changefreq>hourly</changefreq>
        </url>
    <?php } ?>
</urlset>