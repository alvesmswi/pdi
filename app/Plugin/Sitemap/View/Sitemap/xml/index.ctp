<?php echo '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    if(!empty($terms)){ 
        foreach($terms as $term){ 
            echo '<sitemap><loc>'.Router::url('/', true) . 'sitemap/sitemap_editorias.xml?editoria='.$term['ter']['slug'].'</loc></sitemap>';
        }
    }
echo '</sitemapindex>';