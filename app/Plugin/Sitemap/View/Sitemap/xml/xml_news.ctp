<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    <?php
    foreach ($nodes as $node){
        ?>
        <url>
            <loc><?php echo Router::url('/', true) . ltrim($node['Node']['path'], '/'); ?></loc>
            <news:news>
                <news:publication>
                    <news:name><?php echo Configure::read('Site.title'); ?></news:name>
                    <news:language>pt</news:language>
                </news:publication>
                <news:publication_date><?php echo date('c', strtotime($node['Node']['publish_start'])); ?></news:publication_date>
                <news:title><?php echo $node['Node']['title']; ?></news:title>
                <news:keywords><?php echo $node['det']['keywords']; ?></news:keywords>
                <news:stock_tickers><?php echo $node['ter']['title']; ?></news:stock_tickers>
            </news:news>
        </url>
    <?php } ?>
</urlset>