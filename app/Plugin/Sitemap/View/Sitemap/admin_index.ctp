<h2> <?php echo __('Sitemap - Options', true); ?> </h2>

<?php
echo $this->Form->create('SitemapIndices', array('url' => array('plugin' => 'sitemap', 'controller' => 'sitemap', 'action' => 'index', 'admin' => true)));
echo $this->Form->input('SitemapIndices.gerar', array('type' => 'hidden', 'default' => 1));
echo $this->Form->end(__('Gerar Indices', true));
?>