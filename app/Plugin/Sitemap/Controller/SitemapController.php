<?php

/**
 * Sitemap Controller
 *
 * PHP version 5
 * @package sitemapplugin
 * @version  1.0
 * @author   Nicolas Traeder <traedamatic@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class SitemapController extends SitemapAppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Sitemap';

    /**
     * Models used by the Controller
     *
     * @var array
     * @access public
     */
    public $uses = array('Nodes.Node');
    public $components = array('RequestHandler');
    public $helpers = array('Time');
    public $defaults = array();

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('indice','editorias');
        Configure::write('debug', 0);
              
    }

    public function admin_index() {
        Configure::write('debug', 1);
        if (!empty($this->request->data['SitemapIndices'])) {
            if(isset($this->request->data['SitemapIndices'])){
                $this->_recriar();
                $this->Session->setFlash('Indices recriados com sucesso', 'flash', array('class' => 'success'));
            }           
        }
    }

    public function indice() {   
        //Desativado em 12/09/18 - Agora gera arquivo físico para não carrega o banco de dados
        return false;

        //constroi a query
        /*$query = "SELECT ter.id, ter.slug FROM taxonomies AS tax 
        INNER JOIN terms AS ter ON(ter.id = tax.term_id)
        WHERE vocabulary_id = 1";

        //se é para renderizar news
        if(isset($_GET['news'])){
            $template = 'index_news';
        }else{
            $template = 'index';
        }

        $cacheDisable = Configure::read('Cache.disable');
        //Se o cache está ativo
        if (!$cacheDisable) {
            $cacheName = 'sitemap_'.$template;            
            $cacheConfig = 'sitemaps';
            $terms = Cache::read($cacheName, $cacheConfig);            
            if (!$terms) {
                $terms = $this->Node->query($query);
                Cache::write($cacheName, $terms, $cacheConfig);
            }
        }else{
            $terms = $this->Node->query($query);
        }        
        $this->set(compact('terms'));        
        $this->render($template);
        $this->RequestHandler->respondAs('xml');*/
    }

    public function editorias() {
        //Desativado em 12/09/18 - Agora gera arquivo físico para não carrega o banco de dados
        return false;
        
        //$this->layout = 'xml/default';

        //se é para renderizar comum
        /*if(isset($_GET['editoria'])){
            $editoria = $_GET['editoria'];
            $template = 'xml';
        }
        //se é para renderizar news
        if(isset($_GET['news'])){
            $editoria = $_GET['news'];
            $template = 'xml_news';
        }        

        //constroi a query
        $query = "SELECT Node.title, Node.path, Node.publish_start, ter.title, det.keywords  FROM taxonomies AS tax 
        INNER JOIN terms AS ter ON(ter.id = tax.term_id)
        INNER JOIN model_taxonomies AS mot ON(mot.taxonomy_id = tax.id)
        INNER JOIN nodes AS Node ON(Node.id = mot.foreign_key)
        LEFT JOIN noticium_details AS det ON(det.node_id = Node.id)
        WHERE Node.type = 'noticia' AND Node.status = 1 AND ter.slug = '$editoria'
        ORDER BY Node.publish_start DESC
        LIMIT 10;";

        $cacheDisable = Configure::read('Cache.disable');
        //Se o cache está ativo
        if (!$cacheDisable) {
            $cacheName = 'sitemap_'.$template;            
            $cacheConfig = 'sitemaps';
            $nodes = Cache::read($cacheName, $cacheConfig);            
            if (!$nodes) {
                $nodes = $this->Node->query($query);
                Cache::write($cacheName, $nodes, $cacheConfig);
            }
        }else{
            $nodes = $this->Node->query($query);
        } 
        $this->set(compact('nodes'));
        $this->render($template);
        $this->RequestHandler->respondAs('xml');*/
    }

    function _recriar(){
        //constroi a query
        $query = "SELECT ter.id, ter.slug FROM taxonomies AS tax 
        INNER JOIN terms AS ter ON(ter.id = tax.term_id)
        WHERE vocabulary_id = 1";
        $terms = $this->Node->query($query);
        $xmlContent = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if(!empty($terms)){
            foreach($terms as $term){ 
                $xmlContent .= '<sitemap><loc>'.Router::url('/', true) . 'sitemap/'.$term['ter']['slug'].'.xml'.'</loc></sitemap>';
                $this->_verificaSitemap($term['ter']['slug']);
            }
            $xmlContent .= '</sitemapindex>';
        }
        $xml = Xml::build($xmlContent, array('return' => 'domdocument'));
        $xml->save('sitemap.xml');

        //Recriar as News
        $this->_recriarNews($terms);
    }

    function _verificaSitemap($termo = null){
        $xmlContent = '<?xml version="1.0" encoding="UTF-8"?>';
        $path 	= WWW_ROOT.'sitemap';
        $folder = new Folder($path);
        //Se a pasta não existe
        if(is_null($folder->path)){
            $folder->create($path);
        }
        //se foi informado o termo
        if($termo){
            $path .= DS.$termo.'.xml';
        }
        
        $sitemapNews 	= new File($path);
		//se o arquivo ainda não existe
		if(!$sitemapNews->exists()){
			//Monta o básico do XML
			$xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>';
			//Cria o arquivo
			$sitemapNews->write($xml);
			$sitemapNews->close();
		}
        return true;
    }

    function _recriarNews($terms){
        $xmlContent = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if(!empty($terms)){
            foreach($terms as $term){ 
                $xmlContent .= '<sitemap><loc>'.Router::url('/', true) . 'sitemap/news-'.$term['ter']['slug'].'.xml'.'</loc></sitemap>';
                $this->_verificaSitemapNews($term['ter']['slug']);
            }
            $xmlContent .= '</sitemapindex>';
        }
        $xml = Xml::build($xmlContent, array('return' => 'domdocument'));
        $xml->save('sitemap_news.xml');
    }

    function _verificaSitemapNews($termo = null){
        $xmlContent = '<?xml version="1.0" encoding="UTF-8"?>';
        $path 	= WWW_ROOT.'sitemap';
        $folder = new Folder($path);
        //Se a pasta não existe
        if(is_null($folder->path)){
            $folder->create($path);
        }
        //se foi informado o termo
        if($termo){
            $path .= DS.'news-'.$termo.'.xml';
        }
        
        $sitemapNews 	= new File($path);
		//se o arquivo ainda não existe
		if(!$sitemapNews->exists()){
			//Monta o básico do XML
			$xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"></urlset>';
			//Cria o arquivo
			$sitemapNews->write($xml);
			$sitemapNews->close();
		}
        return true;
    }

}
?>