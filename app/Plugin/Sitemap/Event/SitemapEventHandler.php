<?php
App::uses('CakeEventListener', 'Event');
App::uses('Xml', 'Utility');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class SitemapEventHandler implements CakeEventListener {

	public function implementedEvents() {
		return array(
			/*'Controller.Nodes.afterDelete' => array(
				'callable' => 'afterDelete',
			),*/
			'Controller.Nodes.afterAdd' => array(
				'callable' => 'afterAdd',
			),
			/*'Controller.Nodes.afterEdit' => array(
				'callable' => 'afterEdit',
			)*/
		);
	}

	public function afterAdd($event) {
		//Adcicionar o node
		$this->addSitemapNews($event->data['data']);
	}
	public function afterEdit($event) {
		//Adcicionar o node
		$this->editSitemapNews($event->data['data']);
	}
	public function afterDelete($event) {
		//Adcicionar o node
		$this->deleteSitemapNews($event->data['data']);
	}

	public function addSitemapNews($node){		
		//Adcicionar o node
		$this->_addNode($node);
		//se for notícia
		if(isset($node['Node']['type']) && $node['Node']['type'] == 'noticia'){
			//cria os sitemaps de noticias
			$this->_addNodeNews($node);
		}		
		return true;
	}
	public function editSitemapNews($node){	
			
		//Adcicionar o node
		$this->_addNode($node);
		//se for notícia
		if(isset($node['Node']['type']) && $node['Node']['type'] == 'noticia'){
			//cria os sitemaps de noticias
			//$this->_addNodeNews($node);
		}		
		return true;
	}	
	public function deleteSitemapNews($node){	
		//Cria um novo
		$baseUrl 		= Router::url('/', true);
		//tira o prefixo
		$baseUrl 	= str_replace('://mswi.','://', $baseUrl);
		
		$termo 			= $this->_getTerm(key($node['TaxonomyData']));		
		$editoria 		= $termo['slug'];
		$nomeArquivo 	= 'sitemap-'.$editoria.'.xml';
		$urlnode 		= $baseUrl . $node['Node']['type'] . '/' .$node['Node']['slug'];

		$sitemapNews 	= new File($nomeArquivo);
		//se o arquivo ainda não existe
		if($sitemapNews->exists()){
			//remove o node se existir
			$this->_removeNode($nomeArquivo, $urlnode);
		}
		return true;
	}	

	/*SITEMAP - PADRÃO*/

	public function _addNode($node){	
		//se foi informado um termo
		if(isset($node['TaxonomyData']) && !empty($node['TaxonomyData'])){
			$key = key($node['TaxonomyData']);
			$termo = $this->_getTerm($node['TaxonomyData'][$key]);
			//se não encontrou o termo
			if(!$termo){
				$termo = array(
					'slug' => $node['Node']['type'],
					'title' => $node['Node']['type']
				);
			}
		}else{
			$termo = array(
				'slug' => $node['Node']['type'],
				'title' => $node['Node']['type']
			);
		}		
		//se o plugin de Editorias está ativo
		if(CakePlugin::loaded('Editorias')){
			//se foi informado a Editoria			
			if(isset($node['Editoria']['Editorias'][0]) & !empty($node['Editoria']['Editorias'][0])){
				$termo = $this->_getEditoria($node['Editoria']['Editorias'][0]);
				//se não encontrou a editoria
				if(!$termo){
					$termo = array(
						'slug' => $node['Node']['type'],
						'title' => $node['Node']['type']
					);
				}
			}
		}

		//Cria um novo
		$baseUrl 	= Router::url('/', true);	
		//tira o prefixo
		$baseUrl 	= str_replace('://mswi.','://', $baseUrl);

		$editoria 		= $termo['slug'];
		$urlnode = '';
		if(isset($node['Node']['type'])){		
			$urlnode 		= $baseUrl . $node['Node']['type'] . '/' .$node['Node']['slug'];
		}
		$path 			= WWW_ROOT.'sitemap';
		$nomeArquivo 	= $editoria.'.xml';

		$folder = new Folder($path);		
        //Se a pasta não existe
        if(is_null($folder->path)){
            $folder->create($path);
        }
		$sitemapNews 	= new File($path.DS.$nomeArquivo);
		//se o arquivo ainda não existe
		if(!$sitemapNews->exists()){
			//Monta o básico do XML
			$xml = '<?xml version="1.0" encoding="UTF-8"?>
				<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>';
			//Cria o arquivo
			$sitemapNews->write($xml);
			$sitemapNews->close();
		}else{
			//remove o node se existir
			$this->_removeNode($path.DS.$nomeArquivo, $urlnode);
		}		

		$xml = Xml::build($path.DS.$nomeArquivo, array('return' => 'domdocument'));
		$url = $xml->firstChild->appendChild($xml->createElement('url'));
			$url->appendChild($xml->createElement('loc', $urlnode));
			$url->appendChild($xml->createElement('lastmod', date('Y-m-d', strtotime($node['Node']['publish_start']))));
			$url->appendChild($xml->createElement('changefreq', 'hourly'));
			$url->appendChild($xml->createElement('priority', '0.5'));

		$xml->save($path.DS.$nomeArquivo);
	}

	public function _removeNode($nomeArquivo, $urlnode){
		$xml = Xml::build($nomeArquivo, array('return' => 'domdocument'));
		$urls = $xml->getElementsByTagName('url');
		foreach($urls as $urlkey => $url){
			$locs = $url->getElementsByTagName('loc');
			foreach($locs as $lockey => $loc){
				//se encontrar com o mesmo valor
				if($loc->nodeValue == $urlnode){
					//apaga
					$loc->parentNode->parentNode->removeChild($url);
				}			
			}			
		}
		$xml->save($nomeArquivo);
	}

	/*SITEMAP - NEWS*/

	public function _addNodeNews($node){
		//se foi informado um termo
		if(isset($node['TaxonomyData']) && !empty($node['TaxonomyData'])){
			$key = key($node['TaxonomyData']);
			$termo = $this->_getTerm($node['TaxonomyData'][$key]);
		}else{
			$termo = array(
				'slug' => $node['Node']['type'],
				'title' => $node['Node']['type']
			);
		}	
		//se o plugin de Editorias está ativo
		if(CakePlugin::loaded('Editorias')){
			//se foi informado a Editoria			
			if(isset($node['Editoria']['Editorias'][0]) & !empty($node['Editoria']['Editorias'][0])){
				$termo = $this->_getEditoria($node['Editoria']['Editorias'][0]);
				//se não encontrou a editoria
				if(!$termo){
					$termo = array(
						'slug' => $node['Node']['type'],
						'title' => $node['Node']['type']
					);
				}
			}
		}				
		//Cria um novo
		$baseUrl 		= Router::url('/', true);
		//tira o prefixo
		$baseUrl 	= str_replace('://mswi.','://', $baseUrl);
		
		$editoria 		= $termo['slug'];		
		$urlnode 		= $baseUrl . $node['Node']['type'] . '/' .$node['Node']['slug'];
		$path 			= WWW_ROOT.'sitemap';
		$nomeArquivo 	= 'news-'.$editoria.'.xml';

		$folder = new Folder($path);		
        //Se a pasta não existe
        if(is_null($folder->path)){
            $folder->create($path);
        }
		$sitemapNews 	= new File($path.DS.$nomeArquivo);
		//se o arquivo ainda não existe
		if(!$sitemapNews->exists()){
			//Monta o básico do XML
			$xml = '<?xml version="1.0" encoding="UTF-8"?>
				<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"></urlset>';
			//Cria o arquivo
			$sitemapNews->write($xml);
			$sitemapNews->close();
		}else{
			//remove o node se existir
			$this->_removeNodeNews($path.DS.$nomeArquivo, $urlnode);
		}		

		$xml = Xml::build($path.DS.$nomeArquivo, array('return' => 'domdocument'));
		$url = $xml->firstChild->appendChild($xml->createElement('url'));
		$url->appendChild($xml->createElement('loc', $urlnode));
			$news = $url->appendChild($xml->createElement('news:news'));
			$publication = $news->appendChild($xml->createElement('news:publication'));
				$publication->appendChild($xml->createElement('news:name', Configure::read('Site.title')));
				$publication->appendChild($xml->createElement('news:language', 'pt'));
			$url->appendChild($xml->createElement('news:publication_date', date('c', strtotime($node['Node']['publish_start']))));
			$url->appendChild($xml->createElement('news:title', $node['Node']['title']));
			$url->appendChild($xml->createElement('news:keywords', isset($node['NoticiumDetail']['keywords']) ? $node['NoticiumDetail']['keywords'] : ''));
			$url->appendChild($xml->createElement('news:stock_tickers', $termo['title']));

		$xml->save($path.DS.$nomeArquivo);
	}

	public function _removeNodeNews($nomeArquivo, $urlnode){
		$xml = Xml::build($nomeArquivo, array('return' => 'domdocument'));
		$urls = $xml->getElementsByTagName('url');
		foreach($urls as $urlkey => $url){
			$locs = $url->getElementsByTagName('loc');
			foreach($locs as $lockey => $loc){
				//se encontrar com o mesmo valor
				if($loc->nodeValue == $urlnode){
					//apaga
					$loc->parentNode->parentNode->removeChild($url);
				}			
			}			
		}
		$xml->save($nomeArquivo);
	}

	public function _getTerm($termId){
		//se NÃO informou o termo
		if(empty($termId)){
			return false;
		}
		ClassRegistry::init('Sitemap.Sitemap');
		$Sitemap = new Sitemap();			
		$termo = $Sitemap->query("
		SELECT Term.id, Term.title, Term.slug 
		FROM terms as Term 
		INNER JOIN taxonomies as Tax ON(Tax.term_id = Term.id) 
		WHERE Tax.id = $termId;
		");
		return $termo[0]['Term'];
	}

	public function _getEditoria($id){
		//se NÃO informou o termo
		if(empty($id)){
			return false;
		}
		ClassRegistry::init('Sitemap.Sitemap');
		$Sitemap = new Sitemap();			
		$termo = $Sitemap->query("
			SELECT id, title, slug 
			FROM editorias
			WHERE id = $id;
		");
		return $termo[0]['editorias'];
	}
}