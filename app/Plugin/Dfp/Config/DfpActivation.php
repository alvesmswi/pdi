<?php
class DfpActivation {

/**
 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 * @return boolean
 */
	public function beforeActivation(&$controller) {
		return true;
	}

/**
 * Called after activating the plugin in ExtensionsPluginsController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
	public function onActivation(&$controller) {

		// ACL: set ACOs with permissions
		$controller->Croogo->addAco('Dfp/Dfp/admin_config');
		$controller->Croogo->addAco('Dfp/Dfp/admin_index', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_add', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_blocos', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_get_tag', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/gerar_header_tags', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/gerar_block_tags', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_edit', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_aprovar', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_pausar', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_retomar', array('gerente', 'editor'));
		$controller->Croogo->addAco('Dfp/Dfp/admin_arquivar', array('gerente', 'editor'));
		//Configurações basicas
		$controller->Setting->write('Dfp.TraffickerId','244867205', array('description' => 'ID do Publicador Autorizado','editable' => 1, 'title' => 'TraffickerId'));
		$controller->Setting->write('Dfp.SlotTopId','', array('description' => 'ID do Slot principal do Jornal','editable' => 1, 'title' => 'SlotTopId'));
		
		//cria a região
		$controller->loadModel('Region');
		$region = $controller->Region->find('first', array('conditions'=>array('Region.alias'=>'header_scripts')));
		//se a região não existe
		if(empty($region)){
			$controller->Region->create();
			$controller->Region->set(
				array(
					'title'            => 'Scripts no Header',
					'alias'            => 'header_scripts',
					'description'      => 'Scripts no Header'
				)
			);
			$controller->Region->save();
			$regionId = $controller->Region->id;	
		}else{
			$regionId = $region['Region']['id'];
		} 

		//Cria o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'dfp')));
		//se o Bloco não existe
		if(empty($block)){
			$controller->Block->create();
			$controller->Block->set(array(
				'visibility_roles' => '',
				'visibility_paths' => '',
				'region_id'        => $regionId,
				'title'            => 'Anúncios (Scripts DFP)',
				'alias'            => 'dfp',
				'body'             => '[element:dfp_tag plugin="dfp"]',
				'show_title'       => 0,
				'status'           => 1
			));
			$controller->Block->save();
		}

		$this->moveFiles();
		
		Cache::clear(false, '_cake_model_');
	}

	/**
	 * onActivate will be called if this returns true
 *
 * @param  object $controller Controller
 *
 * @return boolean
 */
	public function beforeDeactivation(&$controller) {
		return true;
	}

	public function onDeactivation(&$controller) {
		$controller->Croogo->removeAco('Dfp');
		//Apaga o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'dfp')));
        if($block){
            $controller->Block->delete($block['Block']['id']);
		} 
	}

	function moveFiles(){
		//monta o diretorio para buscar os arquivos
		$diretorio = App::pluginPath('Dfp') .'webroot';
		$dir = new Folder($diretorio);
		//procura todos os arquivos
		$files = $dir->find('.*.');
		//se encontrou algum arquivo
		if(!empty($files)){
			//percore os arquivos
			foreach($files as $file){
				//move o arquivos
				$arq1 = new File($diretorio.DS.$file);
				if ($arq1->exists()) {
					//Copia o arquivo para o root (public)
				    $arq2 = new Folder(WWW_ROOT, true);
					$arq1->copy($arq2->path . DS . $arq1->name);
				}
			}
		}
	}
}