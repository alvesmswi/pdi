<?php
CroogoNav::add('dfp', array(
	'title' => 'Anúncios',
	'icon' => 'money',
	'url' => array(
		'admin' => true,
		'plugin' => 'dfp',
		'controller' => 'dfp',
		'action' => 'index',
	),
	'weight' => 60,
	'children' => array(
		'listar' => array(
			'title' => 'Listar',
			'url' => array(
				'admin' => true,
				'plugin' => 'dfp',
				'controller' => 'dfp',
				'action' => 'index'
			)
		),
		'criar' => array(
			'title' => 'Novo',
			'url' => array(
				'admin' => true,
				'plugin' => 'dfp',
				'controller' => 'dfp',
				'action' => 'add'
			)
		),
		'blocos' => array(
			'title' => 'Blocos',
			'url' => array(
				'admin' => true,
				'plugin' => 'dfp',
				'controller' => 'dfp',
				'action' => 'blocos'
			)
		),
		'reports' => array(
			'title' => 'Relatório',
			'url' => array(
				'admin' => true,
				'plugin' => 'dfp',
				'controller' => 'dfp_reports',
				'action' => 'index'
			)
		),
		'config' => array(
			'title' => 'Configurar',
			'url' => array(
				'admin' => true,
				'plugin' => 'dfp',
				'controller' => 'dfp',
				'action' => 'config'
			)
		),
	)
));