<?php

App::uses('DfpAppController', 'Dfp.Controller');
App::uses('DfpApi', 'Dfp.Lib');

class DfpReportsController extends DfpAppController 
{
    public $name = 'DfpReports';
	public $uses = array('Dfp.Dfp', 'Settings.Setting');
	public $components = array('Dfp.DfpUtil');
	public $helper = array('Dfp.Dfp');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions[] = 'admin_index';
    }

    private function verificaConfig()
    {
        // Se foi configura do o Anunciante e foi configurado o Slot
		if (Configure::read('Dfp.TraffickerId') && Configure::read('Dfp.SlotTopId')) {
			return true;
		}

        $this->Session->setFlash('É preciso configurar o ID do Slot Principal do Jornal', 'flash', array('class' => 'error'));
		return $this->redirect('/admin/dfp/dfp/config');
    }

    public function admin_index($pedidoId = null)
    {
        $this->verificaConfig();

        if (isset($this->request->query['limpar']) && $this->request->query['limpar']) {
            $this->Session->delete('DfpReports');

            return $this->redirect(array('action' => 'admin_index'));
        }

        $arrayPerfeito = ($this->Session->check('DfpReports.registros')) ? $this->Session->read('DfpReports.registros') : array();

        if (!empty($this->request->data)) {
            $pedidoId = floatval($this->request->data['Dfp']['pedidoId']); // 2341265965 - Só funciona se for Int ou Float - String não.

            $dataInicial    = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['dataInicial']);
            $dataFinal      = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['dataFinal']);

            $report = DfpApi::relatoriosSalvos($pedidoId, $dataInicial, $dataFinal);
            
            if (!empty($report)) {
                $contador = 0;
                foreach ($report['dataset'] as $key => $value) {
                    if ($key == 'Row') {
                        foreach ($value as $key => $column) {
                            foreach ($column['Column'] as $line) {
                                $name_tag = $line['@attributes']['name'];
                                $value_tag = $line['Val'];
                                $arrayPerfeito[$contador][$name_tag] = $value_tag;
                            }

                            $contador++;
                        }
                        
                    }
                }

                // guardar na sesssão
                $this->Session->write('DfpReports.registros', $arrayPerfeito);
            } else {
                $this->Session->setFlash('Não foi encontrado nenhum relatório para o Pedido e/ou Data informado!', 'flash', array('class' => 'error'));
            }
        }

        if (!empty($pedidoId)) {
            $this->request->data['Dfp']['pedidoId'] = $pedidoId;
        }

        $this->set('registros', $arrayPerfeito);
        $this->set('diasDaSemanda', $this->Dfp->diasDaSemanda());

        if (isset($this->request->query['excel']) && $this->request->query['excel']) {
            $this->render('admin_index_exportar', 'excel');
        }
    }
}
