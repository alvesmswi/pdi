<?php
App::uses('DfpAppController', 'Dfp.Controller');
App::uses('DfpApi', 'Dfp.Lib');

class DfpController extends DfpAppController {

	public $name = 'Dfp';
	public $uses = array('Dfp.Dfp', 'Settings.Setting');
	public $components = array('Dfp.DfpUtil');
	public $helper = array('Dfp.Dfp');

	public function beforeFilter() {
		parent::beforeFilter();
		//desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
		$this->Security->authCheck = false;
		$this->Security->unlockedActions[] = 'admin_add';
		$this->Security->unlockedActions[] = 'admin_edit';
		//$this->Auth->allow('slots');
	}

	function _verificaConfig(){
		//Se foi configura do o Anunciante e foi configurado o Slot
		if(
			Configure::read('Dfp.TraffickerId') && 
			Configure::read('Dfp.SlotTopId')
		){
			return true;
		}

		$this->Session->setFlash('É preciso configurar o ID do Slot Principal do Jornal', 'flash', array('class' => 'error'));
		return $this->redirect('/admin/dfp/dfp/config');
	}

	public function admin_config(){

		$this->set('title_for_layout','Dfp');
		if (!empty($this->request->data)) {
			if($this->Setting->deleteAll(
				array('Setting.key LIKE' => 'Dfp.%')
			)){
				foreach($this->request->data['Dfp'] as $key => $value){
					$arraySave = array();
					$arraySave['id']            = NULL;
					$arraySave['editable']      = 0;
					$arraySave['input_type']    = 'text';
					$arraySave['key']           = 'Dfp.'.$key;
					$arraySave['value']         = $value;
					$arraySave['title']         = $key;

					$this->Setting->create();
					if ($this->Setting->saveAll($arraySave)) {
						$this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
					} else {
						$this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
					}
				}

				//Gera o script principal
				$arraySave = array();
				$arraySave['id']            = NULL;
				$arraySave['editable']      = 0;
				$arraySave['input_type']    = 'text';
				$arraySave['key']           = 'Dfp.tagHeader';
				$arraySave['value']         = $this->gerar_header_tags();
				$arraySave['title']         = 'DFP - Tag Header';
				$this->Setting->create();
				$this->Setting->saveAll($arraySave);

				//Gera os blocos de anuncios
				$this->gerar_block_tags(true);

				//Cria um anunciante com o nome do Jornal
				$anuncianteId = DfpApi::getAnunciante(Configure::read('Site.title'));

				//Gera e grava o anunciante
				$arraySave = array();
				$arraySave['id']            = NULL;
				$arraySave['editable']      = 0;
				$arraySave['input_type']    = 'text';
				$arraySave['key']           = 'Dfp.anuncianteId';
				$arraySave['value']         = $anuncianteId;
				$arraySave['title']         = 'DFP - Anunciante ID';
				$this->Setting->create();
				$this->Setting->saveAll($arraySave);
			}			
		}

		//limpa o this->data
		$this->request->data = array();
		//Pega os dados
		$arrayDados = $this->Setting->find(
			'all',
			array( 'conditions' => array('key LIKE' => 'Dfp.%'),'recursive' => -1)
		);
		if(!empty($arrayDados)){
			foreach($arrayDados as $dados){
				$this->request->data['Dfp'][$dados['Setting']['title']] = $dados['Setting']['value'];
			}
		}		
	}

	public function admin_blocos(){
		$this->_verificaConfig();
		$arraySlots = Cache::read('slots', 'dfp');
		//se não encontrou
		if(empty($arraySlots)){
			//busca os slots
			$arraySlots = DfpApi::getSlots();
			//se encontrou algum
			if(!empty($arraySlots)){
				//grava no cache
				Cache::write('slots', $arraySlots, 'dfp');
			}	
		}
		$this->set('registros',$arraySlots);
	}

	function admin_get_tag($id){
		$this->set('tag',$this->gerar_block_tags(false, $id));
	}
	
	function gerar_header_tags(){

		$arraySlots = DfpApi::getSlots(false);
		
		$slots = '';
		foreach($arraySlots as $slot){
			//pega as variaveis
			$adUnitSizesArray = $slot->getAdUnitSizes()[0];
			$arraySizes = $adUnitSizesArray->getSize();
			$width = $arraySizes->getWidth();
			$height = $arraySizes->getHeight();
			$parentPathArray = $slot->getParentPath()[1];
			$parentAdUnitCode = $parentPathArray->getAdUnitCode();			
			$adUnitCode = $slot->getAdUnitCode();
			$adUnitId = $slot->getId();

			//Monta o script
			$slots .= "googletag.defineSlot('/334726759/$parentAdUnitCode/$adUnitCode', [$width, $height], '$adUnitId').addService(googletag.pubads());\n";
		}
		$script = "
		<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
		<script>
		  var googletag = googletag || {};
		  googletag.cmd = googletag.cmd || [];
		  googletag.cmd.push(function() {
			$slots
			googletag.pubads().enableSingleRequest();
			googletag.pubads().collapseEmptyDivs();
			googletag.enableServices();
		  });
		</script>";
		return trim($script);
	}

	function gerar_block_tags($gerarBlocos = false, $idSlot = null){
		$this->loadModel('Block');
		$arraySlots = DfpApi::getSlots(false);
		
		$slots = '';
		foreach($arraySlots as $slot){
			//pega as variaveis
			$adUnitSizesArray = $slot->getAdUnitSizes()[0];
			$arraySizes = $adUnitSizesArray->getSize();
			$width = $arraySizes->getWidth();
			$height = $arraySizes->getHeight();
			$parentPathArray = $slot->getParentPath()[1];
			$parentAdUnitCode = $parentPathArray->getAdUnitCode();			
			$adUnitCode = $slot->getAdUnitCode();
			$adUnitId = $slot->getId();
			$adUnitName = $slot->getName();

			//Monta o script
			$script = "
			<!-- /334726759/$parentAdUnitCode/$adUnitCode -->
			<div id='$adUnitId' style='height:".$height."px; width:".$width."px;' class='bloco-dfp'>
				<script>
					googletag.cmd.push(function() { googletag.display('$adUnitId'); });
				</script>
			</div>";

			//se foi informado um ID
			if(!empty($idSlot)){
				if($idSlot == $adUnitId){
					return $script;
				}
			}
			//se for para gerar blocos
			if($gerarBlocos){
				//Cria o bloco			
				$block = $this->Block->find(
					'first', 
					array(
						'conditions'=>array(
							'Block.alias'=>$adUnitId
						)
					)
				);
				//se o Bloco não existe
				if(empty($block)){
					$this->Block->create();
					$this->Block->set(array(
						'visibility_roles' => '',
						'visibility_paths' => '',
						'region_id'        => '',
						'title'            => 'DFP - '.$adUnitName,
						'alias'            => $adUnitId,
						'body'             => $script,
						'show_title'       => 0,
						'status'           => 0
					));
					$this->Block->save();
				}
			}
			
		}
		
		return trim($script);
	}

	public function admin_index()
	{
		$this->_verificaConfig();
		$status = 1;
		$pedidoId = null;
		if(!empty($this->request->data)){
			$status = $this->request->data['Dfp']['status'];
			if(!empty($this->request->data['Dfp']['pedidoId'])){
				$pedidoId = $this->request->data['Dfp']['pedidoId'];
			}
		}		
		
		$arrayItems = DfpApi::listarPedidos($status, $pedidoId);
		$this->set('registros',$arrayItems);
	}

	public function admin_add()
	{
		$this->_verificaConfig();

		$arrayErros = array();

		//se foi postado alguam coisa
		if (!empty($this->request->data)) {
			//se foi informada a data de inicio
			if(isset($this->request->data['Dfp']['inicio']) && !empty($this->request->data['Dfp']['inicio'])){
				//trata a data
				$this->request->data['Dfp']['inicio'] = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['inicio']);
			}else{
				$this->request->data['Dfp']['inicio'] = '';
			}
			//se foi informada a data de fim
			if(isset($this->request->data['Dfp']['fim']) && !empty($this->request->data['Dfp']['fim'])){
				//trata a data
				$this->request->data['Dfp']['fim'] = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['fim']);
				//verifica se a data de fim é maior que a data atual
				if($this->request->data['Dfp']['fim'] <= date('Y-m-d H:i:s')){
					$arrayErros[] = 'Informe data e hora de fim maior que agora.';
				}
			}else{
				$this->request->data['Dfp']['fim'] = '';
			}

			//Se não tem nenhum erro no formulario
			if(empty($arrayErros)){
				//Cria o pedido no servido do DFP
				$criarPedido = DfpApi::criarPedido($this->request->data['Dfp']);
				//Se foi criado com sucesso
				if($criarPedido){
					$this->Session->setFlash('Anúncio criado com sucesso!', 'flash', array('class' => 'success'));
					return $this->redirect('/admin/dfp');
				}else{
					$this->Session->setFlash('Erro ao criar o anúncio', 'flash', array('class' => 'error'));
				}
			}else{
				$mensagemErros = '';
				//percorre os erros
				foreach($arrayErros as $erro){
					$mensagemErros = $erro;
				}
				$this->Session->setFlash($mensagemErros, 'flash', array('class' => 'error', 'escape'=>true));
			}
		}		

		$arraySlots = Cache::read('slots', 'dfp');

		//se não encontrou
		if(empty($arraySlots)){
			//busca os slots
			$arraySlots = DfpApi::getSlots();
			//se encontrou algum
			if(!empty($arraySlots)){
				//grava no cache
				Cache::write('slots', $arraySlots, 'dfp');
			}	
		}
		$this->set('arraySlots',$arraySlots);

	}

	public function admin_edit($pedidoId = null)
	{
		$this->_verificaConfig();

		$arrayErros = array();

		//se foi postado alguam coisa
		if (!empty($this->request->data)) {
			
			//se foi informada a data de inicio
			if(isset($this->request->data['Dfp']['inicio']) && !empty($this->request->data['Dfp']['inicio'])){
				//trata a data
				$this->request->data['Dfp']['inicio'] = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['inicio']);
			}else{
				$this->request->data['Dfp']['inicio'] = '';
			}
			//se foi informada a data de fim
			if(isset($this->request->data['Dfp']['fim']) && !empty($this->request->data['Dfp']['fim'])){
				//trata a data
				$this->request->data['Dfp']['fim'] = $this->DfpUtil->tratarDataHora($this->request->data['Dfp']['fim']);
				//verifica se a data de fim é maior que a data atual
				if($this->request->data['Dfp']['fim'] <= date('Y-m-d H:i:s')){
					$arrayErros[] = 'Informe data e hora de fim maior que agora.';
				}
			}else{
				$this->request->data['Dfp']['fim'] = '';
			}

			//Se não tem nenhum erro no formulario
			if(empty($arrayErros)){
				//Cria o pedido no servido do DFP
				$criarPedido = DfpApi::editarPedido($this->request->data['Dfp']);
				//Se foi criado com sucesso
				if($criarPedido){
					$this->Session->setFlash('Anúncio '.$pedidoId.' editado com sucesso!', 'flash', array('class' => 'success'));
					return $this->redirect('/admin/dfp');
				}else{
					$this->Session->setFlash('Erro ao editar o anúncio', 'flash', array('class' => 'error'));
				}
			}else{
				$mensagemErros = '';
				//percorre os erros
				foreach($arrayErros as $erro){
					$mensagemErros = $erro;
				}
				$this->Session->setFlash($mensagemErros, 'flash', array('class' => 'error', 'escape'=>true));
			}
			
		}else{
			//Pega os dados do pedido
			$this->request->data['Dfp'] = DfpApi::getPedido($pedidoId);
		}

		$arraySlots = Cache::read('slots', 'dfp');
		//se não encontrou
		if(empty($arraySlots)){
			//busca os slots
			$arraySlots = DfpApi::getSlots();
			//se encontrou algum
			if(!empty($arraySlots)){
				//grava no cache
				Cache::write('slots', $arraySlots, 'dfp');
			}
		}
		$this->set('arraySlots',$arraySlots);

		$this->render('admin_add');
	}

	public function admin_aprovar($pedidoId){
		DfpApi::aprovarPedido($pedidoId);
		$this->Session->setFlash('Anúncio '.$pedidoId.' ativado com sucesso!', 'flash', array('class' => 'success'));
		return $this->redirect('/admin/dfp');
	}

	public function admin_pausar($pedidoId){
		DfpApi::pausarPedido($pedidoId);
		$this->Session->setFlash('Anúncio '.$pedidoId.' pausado com sucesso!', 'flash', array('class' => 'success'));
		return $this->redirect('/admin/dfp');
	}

	public function admin_retomar($pedidoId){
		DfpApi::retomarPedido($pedidoId);
		$this->Session->setFlash('Anúncio '.$pedidoId.' retomado com sucesso!', 'flash', array('class' => 'success'));
		return $this->redirect('/admin/dfp');
	}

	public function admin_arquivar($pedidoId){
		DfpApi::arquivarPedido($pedidoId);
		$this->Session->setFlash('Anúncio '.$pedidoId.' arquivado com sucesso!', 'flash', array('class' => 'success'));
		return $this->redirect('/admin/dfp');
	}

	public function admin_clear_cache($pedidoId){
		Cache::clear(false, 'dfp');
		return $this->redirect($this->referer());
	}

}
?>