<?php
App::uses('Component', 'Controller');
class DfpUtilComponent extends Component {
    function tratarData($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('/', '-', $valor);
			$resultValor = date('Y-m-d', strtotime($valorReplace));
		}else{
			$resultValor = date('d/m/Y', strtotime($valor));
		}
		return $resultValor;
    }
    
    function tratarDataHora($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('/', '-', $valor);
			$resultValor = date('Y-m-d H:i:s', strtotime($valorReplace));
		}else{
			$resultValor = date('d/m/Y H:i:s', strtotime($valor));
		}
		return $resultValor;
	}

	function tratarValor($valor, $tipo='us'){
		if($tipo=='us'){
			$valorReplace = str_replace('.', '', $valor);
			$valorReplace = str_replace(',', '.', $valorReplace);
			$resultValor = $valorReplace;
		}else{
			$resultValor = number_format($valor,2,',','.');
		}
		return $resultValor;
	}
}