<?php

//namespace Dfp;

require 'Google/vendor/autoload.php';

//use DateTime;
//use DateTimeZone;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\Dfp\DfpServices;
use Google\AdsApi\Dfp\DfpSession;
use Google\AdsApi\Dfp\DfpSessionBuilder;
use Google\AdsApi\Dfp\v201802\Order;
use Google\AdsApi\Dfp\v201802\OrderService;
use Google\AdsApi\Dfp\v201802\LineItem;
use Google\AdsApi\Dfp\v201802\LineItemService;
use Google\AdsApi\Dfp\v201802\LineItemType;
use Google\AdsApi\Dfp\v201802\CreativePlaceholder;
use Google\AdsApi\Dfp\v201802\Size;
use Google\AdsApi\Dfp\v201802\StartDateTimeType;
use Google\AdsApi\Dfp\Util\v201802\DfpDateTimes;
use Google\AdsApi\Dfp\v201802\CostType;
use Google\AdsApi\Dfp\v201802\Money;
use Google\AdsApi\Dfp\v201802\UnitType;
use Google\AdsApi\Dfp\v201802\Goal;
use Google\AdsApi\Dfp\v201802\GoalType;
use Google\AdsApi\Dfp\v201802\Targeting;
use Google\AdsApi\Dfp\v201802\InventoryTargeting;
use Google\AdsApi\Dfp\v201802\CreativeAsset;
use Google\AdsApi\Dfp\v201802\CreativeService;
use Google\AdsApi\Dfp\v201802\ImageCreative;
use Google\AdsApi\Dfp\v201802\LineItemCreativeAssociation;
use Google\AdsApi\Dfp\v201802\LineItemCreativeAssociationService;
use Google\AdsApi\Dfp\v201802\AdUnit;
use Google\AdsApi\Dfp\v201802\InventoryService;
use Google\AdsApi\Dfp\v201802\NetworkService;
use Google\AdsApi\Dfp\Util\v201802\StatementBuilder;
use Google\AdsApi\Dfp\v201802\AdUnitTargeting;
use Google\AdsApi\Dfp\v201802\ApproveAndOverbookOrders as ApproveOrdersAction;
use Google\AdsApi\Dfp\v201802\ArchiveOrders;
use Google\AdsApi\Dfp\v201802\PauseOrders;
use Google\AdsApi\Dfp\v201802\ResumeOrders;
use Google\AdsApi\Dfp\v201802\Company;
use Google\AdsApi\Dfp\v201802\CompanyService;
use Google\AdsApi\Dfp\v201802\CompanyType;
use Google\AdsApi\Dfp\v201802\ApiException;
use Google\AdsApi\Dfp\v201802\StringFormatError;

use Google\AdsApi\Dfp\Util\v201802\ReportDownloader;
use Google\AdsApi\Dfp\v201802\ReportDownloadOptions;
use Google\AdsApi\Dfp\v201802\Column;
use Google\AdsApi\Dfp\v201802\DateRangeType;
use Google\AdsApi\Dfp\v201802\Dimension;
use Google\AdsApi\Dfp\v201802\DimensionAttribute;
use Google\AdsApi\Dfp\v201802\ExportFormat;
use Google\AdsApi\Dfp\v201802\ReportService;
use Google\AdsApi\Dfp\v201802\ReportQuery;
use Google\AdsApi\Dfp\v201802\ReportJob;


/*
DRAFT = 'DRAFT';
PENDING_APPROVAL = 'PENDING_APPROVAL';
APPROVED = 'APPROVED';
DISAPPROVED = 'DISAPPROVED';
PAUSED = 'PAUSED';
CANCELED = 'CANCELED';
DELETED = 'DELETED';
UNKNOWN = 'UNKNOWN';
*/
        
class DfpApi {

    static public function autenticar(){
        $configFile = App::pluginPath('Dfp') .'Lib' . DS .'adsapi_php.ini';

        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile($configFile)
            ->build();

        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        
        $session = (new DfpSessionBuilder())->fromFile($configFile)
            ->withOAuth2Credential($oAuth2Credential)
            ->build();

        return $session;
    }

    //Criar um novo Pedido
    static public function criarPedido($dados)
    {

        //trata os dados
        $session = self::autenticar();
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        //Dados padrões
        $jornalId               = Configure::read('Dfp.SlotTopId');
        $traffickerId           = Configure::read('Dfp.TraffickerId');
        $anuncianteId           = Configure::read('Dfp.anuncianteId') ? Configure::read('Dfp.anuncianteId') : '4544957716';
        $titulo                 = $dados['titulo'];

        //Prepara o novo pedido
        $order = new Order();
        $order->setName($titulo . ' ' . uniqid()); // Nome do Pedido
        $order->setAdvertiserId($anuncianteId); // ID do Anunciante -  <<< Aqui está como ADI
        $order->setTraffickerId($traffickerId); // ID do Publicador - <<< Aqui esta como dfp-integracao@ga-jornais.iam.gserviceaccount.com
        $order->setNotes($titulo);
        $order->setPoNumber($jornalId);

        //Cria o pedido lá no servidor do DFP
        $results = $orderService->createOrders([$order]);

        //Percorre o resultado
        foreach ($results as $i => $order) {
            $pedidoId = $order->getId();
        }
        
        //Cria o item de linha
        $lineItemService = $dfpServices->get($session, LineItemService::class);
        
        //Define aonde será exibido este anúncio
        $inventoryTargeting = new InventoryTargeting();
        $adUnitTargeting = new AdUnitTargeting();
        $adUnitTargeting->setAdUnitId($dados['slot']); //Slot selecionado
        $inventoryTargeting->setTargetedAdUnits([$adUnitTargeting]);
        $targeting = new Targeting();
        $targeting->setInventoryTargeting($inventoryTargeting);

        //Pega os dados da imagem
        $arrayImage = getimagesize($dados['imagem']['tmp_name']);
        //Informa o tamanho do criativo que pode ser associado a este item de linha
        $creativePlaceholder = new CreativePlaceholder();
        $width = $arrayImage[0];
        $height = $arrayImage[1];
        $creativePlaceholder->setSize(new Size($width, $height, false));

        //Metas de impressões
        $goal = new Goal();   
        $goal->setUnitType(UnitType::IMPRESSIONS);      
        //se foi informado quandidade STANDARD
        if(isset($dados['quantidade'])){
            $goal->setUnits($dados['quantidade']);                   
            $goal->setGoalType(GoalType::LIFETIME);
        }else{
            $goal->setUnits($dados['percentual']);  
            $goal->setGoalType(GoalType::DAILY);
        }

        //Cria um item de linha
        $lineItem = new LineItem();
        $lineItem->setName($titulo);
        $lineItem->setOrderId($pedidoId);
        $lineItem->setTargeting($targeting);
        $lineItem->setAllowOverbook(true);
        $lineItem->setSkipInventoryCheck(true);
        $lineItem->setDeliveryRateType('AS_FAST_AS_POSSIBLE');
        /*
        Tipo de item de linha:
        SPONSORSHIP = Patrocinado, Exibe com revezamento e percentual de prioridade e pode ter data indeterminada
        STANDARD = Padrão, é utilizando quando tem metas de entregas e data de fim
        */
        //Se o timpo é standard
        if($dados['tipo']=='STANDARD'){   
            $lineItem->setLineItemType(LineItemType::STANDARD);
        }else{
            $lineItem->setLineItemType(LineItemType::SPONSORSHIP);
        }          
        $lineItem->setCreativePlaceholders([$creativePlaceholder]);
        $lineItem->setCostType(CostType::CPM);
        $lineItem->setCostPerUnit(new Money('BRL', 0));
        $lineItem->setPrimaryGoal($goal);

        //Se foi informado para iniciar imediatamente
        if($dados['iniciaragora'] || $dados['inicio'] <= date('Y-m-d H:i:s')){
            $lineItem->setStartDateTimeType(StartDateTimeType::IMMEDIATELY);
        }else{
            //Imforma quanto tempo vai durar o anúncio
            $lineItem->setStartDateTime(
                DfpDateTimes::fromDateTime(            
                    new DateTime($dados['inicio'], new DateTimeZone('America/Sao_Paulo'))
                )
            ); 
        }       
        //se foi informado a data de encerramento
        if(isset($dados['fim']) && !empty($dados['fim'])){
            $lineItem->setEndDateTime(
                DfpDateTimes::fromDateTime(            
                    new DateTime($dados['fim'], new DateTimeZone('America/Sao_Paulo'))
                )
            ); 
        }else{
            $lineItem->setUnlimitedEndDateTime(true);
        }

        //Cria o Item de Linha no servidor do Google
        $results = $lineItemService->createLineItems([$lineItem]);

        foreach ($results as $i => $lineItem) {
            $lineItemId = $lineItem->getId();
        }    
        
        /*
        CRIATIVO: Envia a imagem do criativo para o servidor do google
        */
        $creativeService = $dfpServices->get($session, CreativeService::class);
        $imageCreative = new ImageCreative();
        $imageCreative->setName($pedidoId);
        $imageCreative->setAdvertiserId($anuncianteId);
        $imageCreative->setDestinationUrl($dados['link']); //Aqui é o link da imagem do anúncio

        //Informe o tamanho da imagem
        $size = new Size();
        $size->setWidth($width);
        $size->setHeight($height);
        $size->setIsAspectRatio(false);
        $imageCreative->setSize($size);

        // Set the creative's asset.
        $creativeAsset = new CreativeAsset();
        $creativeAsset->setFileName($titulo);

        //Caminho da imagem
        $creativeAsset->setAssetByteArray(
            file_get_contents($dados['imagem']['tmp_name'])
        );
        $imageCreative->setPrimaryImageAsset($creativeAsset);

        //Cria a imagem no servidor fo Google
        $results = $creativeService->createCreatives([$imageCreative]);

        foreach ($results as $i => $imageCreative) {
            $creativeId = $imageCreative->getId();
        }
    
        //Vincula o criativo com o item de linha
        $licaService = $dfpServices->get($session, LineItemCreativeAssociationService::class);
        $lica = new LineItemCreativeAssociation();
        $lica->setCreativeId($creativeId);
        $lica->setLineItemId($lineItemId);
        $licaService->createLineItemCreativeAssociations([$lica]);

        self::aprovarPedido($pedidoId, $session);

        return $pedidoId;
    }

    static public function editarPedido($dados){
        $session = self::autenticar();
        $anuncianteId           = Configure::read('Dfp.anuncianteId') ? Configure::read('Dfp.anuncianteId') : '4544957716';

        $dfpServices = new DfpServices();        

        /*
        PEDIDO
        */

        //PEDIDO: Montar o filtro
        $statementBuilder = new StatementBuilder();
        $statementBuilder->Where('id = :id');
        $statementBuilder->WithBindVariableValue('id', $dados['id']);

        //PEDIDO:Pega o pedido
        $orderService = $dfpServices->get($session, OrderService::class);
        $page = $orderService->getOrdersByStatement($statementBuilder->toStatement());
        $pedido = $page->getResults()[0];
        $pedidoId = $dados['id'];

        //PEDIDO:Atualiza
        $pedido->setName($dados['titulo'].uniqid());
        $pedido->setNotes($dados['titulo']);

        //PEDIDO:Envia as atualizações
        $orderService->updateOrders([$pedido]);

        /*
        LINEITEM
        */

        //LINEITEM: Montar o filtro
        $statementBuilder = new StatementBuilder();
        $statementBuilder->Where('orderId = :id');
        $statementBuilder->WithBindVariableValue('id', $dados['id']);

        //LINEITEM:Pega o pedido
        $lineItemService = $dfpServices->get($session, LineItemService::class);
        $page = $lineItemService->getLineItemsByStatement($statementBuilder->toStatement());
        $lineItem = $page->getResults()[0];
        $lineItemId = $lineItem->getId();

        //Define aonde será exibido este anúncio
        $inventoryTargeting = new InventoryTargeting();
        $adUnitTargeting = new AdUnitTargeting();
        $adUnitTargeting->setAdUnitId($dados['slot']); //Slot selecionado
        $inventoryTargeting->setTargetedAdUnits([$adUnitTargeting]);
        $targeting = new Targeting();
        $targeting->setInventoryTargeting($inventoryTargeting);

        //Metas de impressões
        $goal = new Goal();   
        $goal->setUnitType(UnitType::IMPRESSIONS);      
        //se foi informado quandidade STANDARD
        if(isset($dados['quantidade'])){
            $goal->setUnits($dados['quantidade']);                   
            $goal->setGoalType(GoalType::LIFETIME);
        }else{
            $goal->setUnits($dados['percentual']);  
            $goal->setGoalType(GoalType::DAILY);
        }

        //LINEITEM:Atualiza
        $lineItem->setName($dados['titulo']);
        $lineItem->setTargeting($targeting);      
        $lineItem->setPrimaryGoal($goal);
        $lineItem->setAllowOverbook(true);
        $lineItem->setSkipInventoryCheck(true);

        //Se foi informado para iniciar
        if(!empty($dados['inicio'])){
            //Imforma quanto tempo vai durar o anúncio
            $lineItem->setStartDateTime(
                DfpDateTimes::fromDateTime(            
                    new DateTime($dados['inicio'], new DateTimeZone('America/Sao_Paulo'))
                )
            ); 
        }       
        //se foi informado a data de encerramento
        if(!empty($dados['fim'])){
            $lineItem->setEndDateTime(
                DfpDateTimes::fromDateTime(            
                    new DateTime($dados['fim'], new DateTimeZone('America/Sao_Paulo'))
                )
            ); 
        }else{
            $lineItem->setUnlimitedEndDateTime(true);
        }

        //LINEITEM:Envia as atualizações
        $lineItemService->updatelineItems([$lineItem]);

        /*
        CRIATIVO:
        */ 

        //Se o tamanho da imagem que est´aatualizando é diferente, Cria uma nova
        if(isset($dados['imagem']['tmp_name']) && !empty($dados['imagem']['tmp_name'])){

            //Pega os dados da imagem atual
            $creativeService = $dfpServices->get($session, CreativeService::class);
            $statementBuilder->where('name = :name');
            $statementBuilder->orderBy('id DESC');
            $statementBuilder->withBindVariableValue('name', $dados['id']);

            //Busca criativo
            $page = $creativeService->getCreativesByStatement($statementBuilder->toStatement());
            $creative = $page->getResults()[0];

            //Imagem atual
            $creativeAsset = $creative->getPrimaryImageAsset();
            //pr($creativeAsset);exit();
            $arraySize = $creativeAsset->getSize();
            $widthAtual = $arraySize->getWidth();
            $heightAtual = $arraySize->getHeight();

            //Pega os dados da imagem nova
            $arrayImage = getimagesize($dados['imagem']['tmp_name']);
            $width = $arrayImage[0];
            $height = $arrayImage[1];

            //Se o tamanho da imagem é diferente
            if($width <> $widthAtual || $height <> $heightAtual){

                //Cria a nova imagem
                $imageCreative = new ImageCreative();
                $imageCreative->setName($pedidoId);
                $imageCreative->setAdvertiserId($anuncianteId);
                $imageCreative->setDestinationUrl($dados['link']); //Aqui é o link da imagem do anúncio

                //Informe o tamanho da imagem
                $size = new Size();
                $size->setWidth($width);
                $size->setHeight($height);
                $size->setIsAspectRatio(false);
                $imageCreative->setSize($size);

                // Set the creative's asset.
                $creativeAsset = new CreativeAsset();
                $creativeAsset->setFileName($dados['titulo']);

                //Caminho da imagem
                $creativeAsset->setAssetByteArray(
                    file_get_contents($dados['imagem']['tmp_name'])
                );
                $imageCreative->setPrimaryImageAsset($creativeAsset);

                //Cria a imagem no servidor fo Google
                $results = $creativeService->createCreatives([$imageCreative]);

                foreach ($results as $i => $imageCreative) {
                    $creativeId = $imageCreative->getId();
                }
            
                //Vincula o criativo com o item de linha
                $licaService = $dfpServices->get($session, LineItemCreativeAssociationService::class);
                $lica = new LineItemCreativeAssociation();
                $lica->setCreativeId($creativeId);
                $lica->setLineItemId($lineItemId);
                $licaService->createLineItemCreativeAssociations([$lica]);
            }else{                
                //Atualiza os dados
                $creativeAsset->setFileName($dados['titulo']);
                //Caminho da imagem
                $creativeAsset->setAssetByteArray(
                    file_get_contents($dados['imagem']['tmp_name'])
                );
                //Atualiza a imagem no servidor fo Google
                $creativeService->updateCreatives([$creative]);
            }
        }    

        return true;
    }

    static public function aprovarPedido($pedidoId, $session = null){
        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        //Procura o anuncio
        $statementBuilder = (new StatementBuilder())->where('id = '.$pedidoId);
        $page = $orderService->getOrdersByStatement($statementBuilder->toStatement());

        //Se achou
        if ($page->getResults() !== null) {
            //Aprova o anuncio
            $action = new ApproveOrdersAction();
            $orderService->performOrderAction(
                $action,
                $statementBuilder->toStatement()
            );
            return true;
        }else{
            return false;
        }
    }

    static public function arquivarPedido($pedidoId, $session = null){
        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        //Procura o anuncio
        $statementBuilder = (new StatementBuilder())->where('id = '.$pedidoId);
        $page = $orderService->getOrdersByStatement($statementBuilder->toStatement());

        //Se achou
        if ($page->getResults() !== null) {
            //Aprova o anuncio
            $action = new ArchiveOrders();
            $orderService->performOrderAction(
                $action,
                $statementBuilder->toStatement()
            );
            return true;
        }else{
            return false;
        }
    }

    static public function pausarPedido($pedidoId, $session = null){
        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        //Procura o anuncio
        $statementBuilder = (new StatementBuilder())->where('id = '.$pedidoId);
        $page = $orderService->getOrdersByStatement($statementBuilder->toStatement());

        //Se achou
        if ($page->getResults() !== null) {
            //Aprova o anuncio
            $action = new PauseOrders();
            $orderService->performOrderAction(
                $action,
                $statementBuilder->toStatement()
            );
            return true;
        }else{
            return false;
        }
    }

    static public function retomarPedido($pedidoId, $session = null){
        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        //Procura o anuncio
        $statementBuilder = (new StatementBuilder())->where('id = '.$pedidoId);
        $page = $orderService->getOrdersByStatement($statementBuilder->toStatement());

        //Se achou
        if ($page->getResults() !== null) {
            //Aprova o anuncio
            $action = new ResumeOrders();
            $orderService->performOrderAction(
                $action,
                $statementBuilder->toStatement()
            );
            return true;
        }else{
            return false;
        }
    }

    static public function getPedido($pedidoId){

        $session = self::autenticar();
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        $jornalId = Configure::read('Dfp.SlotTopId');
        $statementBuilder = new StatementBuilder();
        $statementBuilder->orderBy('id DESC');
        $statementBuilder->where('poNumber = :jornalId AND orderId = :orderId');
        $statementBuilder->withBindVariableValue(
            'jornalId',
            $jornalId
        );        
        $statementBuilder->withBindVariableValue(
            'orderId',
            $pedidoId
        );       

        $page = $orderService->getOrdersByStatement(
            $statementBuilder->toStatement()
        );

        $arrayResults = $page->getResults();
        
        $arrayRegistros = array();
        foreach($arrayResults as $i => $result){

            $arrayRegistros['id'] = $result->getId();
            $arrayRegistros['jornalId'] = $jornalId;
            $arrayRegistros['titulo'] = !empty($result->getNotes()) ? $result->getNotes() : $result->getName();    
            $arrayRegistros['notes'] = $result->getNotes();        
            $arrayRegistros['anuncianteId'] = $result->getAdvertiserId();
            $arrayRegistros['entregas'] = $result->getTotalImpressionsDelivered();
            $arrayRegistros['cliques'] = $result->getTotalClicksDelivered();
            $arrayRegistros['impressoes'] = $result->getTotalImpressionsDelivered();
            $arrayRegistros['status'] = $result->getStatus();            
            //item de linha
            $arrayRegistros += self::getLineItem($result->getId(), $session);
            //pega o criativo
            $arrayRegistros += self::getCriatives($result->getId(), $session);
        }
        return $arrayRegistros;
    }

    static public function getLineItem($pedidoId, $session = null){

        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }
        $dfpServices = new DfpServices();
        $lineItemService = $dfpServices->get($session, LineItemService::class);

        $statementBuilder = new StatementBuilder();
        $statementBuilder->orderBy('id ASC');
        $statementBuilder->limit(1);
        $statementBuilder->where('orderId = :orderId');       
        $statementBuilder->withBindVariableValue(
            'orderId',
            $pedidoId
        );       

        $page = $lineItemService->getLineItemsByStatement(
            $statementBuilder->toStatement()
        );

        $arrayResults = $page->getResults();

        $arrayRegistros = array();
        foreach($arrayResults as $i => $result){

            $arrayRegistros['id'] = $result->getId();
            $arrayRegistros['orderId'] = $result->getOrderId();
            $arrayRegistros['tipo'] = $result->getLineItemType();
            $arrayRegistros['prioridade'] = $result->getPriority();
            $arrayRegistros['status'] = $result->getStatus();
            $arrayRegistros['inicioTipo'] = $result->getStartDateTimeType();
            $arrayRegistros['inicio'] = date('d/m/Y H:i', strtotime(self::tratarData($result->getStartDateTime())));
            //se não é ilimitado
            if($result->getEndDateTime()){
                $arrayRegistros['fim'] = date('d/m/Y H:i', strtotime(self::tratarData($result->getEndDateTime())));
            }

            //Pega o inventario (slot)
            $inventory = $result->getTargeting();
            $inventoryTargeting = $inventory->getInventoryTargeting();
            $targetedAdUnits = $inventoryTargeting->getTargetedAdUnits();
            $arrayRegistros['slot'] = $targetedAdUnits[0]->getAdUnitId();
            //pega as impressões
            $primaryGoal = $result->getPrimaryGoal();
            //se é padrão
            if($arrayRegistros['tipo'] == 'SPONSORSHIP'){
                $arrayRegistros['percentual'] = $primaryGoal->getUnits();
            }else{
                $arrayRegistros['quantidade'] = $primaryGoal->getUnits();
            }
            
        }

        return $arrayRegistros;
    }

    static public function getSlots($tratado = true){

        $session = self::autenticar();

        $dfpServices = new DfpServices();
        $inventoryService = $dfpServices->get($session, InventoryService::class);

        // Get the NetworkService.
        $networkService = $dfpServices->get($session, NetworkService::class);

        // Get the effective root ad unit.
        $rootAdUnitId = $networkService->getCurrentNetwork()
            ->getEffectiveRootAdUnitId();

        // Create a statement to select only the root ad unit by ID.
        $statementBuilder = new StatementBuilder();
        /*
        Aqui vai o ID do Slot Pai
        É preciso criar e configurar antes
        */
        $jornalId = Configure::read('Dfp.SlotTopId');
        $statementBuilder->where('parentId = '.$jornalId); 
        $statementBuilder->orderBy('id DESC');
        //$statementBuilder->limit(1);
        $statementBuilder->withBindVariableValue('id', $rootAdUnitId);

        $page = $inventoryService->getAdUnitsByStatement(
            $statementBuilder->toStatement()
        );

        $slots = $page->getResults();

        if($tratado){
            $arraySlots = array();
            if(!empty($slots)){            
                foreach($slots as $slot){
                    $arraySlots[$slot->getId()] = $slot->getName();
                }
                asort($arraySlots);
            }            
            return $arraySlots;
        }else{
            return $slots;
        }
        
    }

    static public function getCriatives($pedidoId, $session = null){

        //Se nao foir informado a session
        if(empty($session)){
            $session = self::autenticar();
        }

        $dfpServices = new DfpServices();
        $creativeService = $dfpServices->get($session, CreativeService::class);

        $statementBuilder = new StatementBuilder();
        $statementBuilder->where('name = '.$pedidoId);
        $statementBuilder->orderBy('id DESC');
        $statementBuilder->limit(1);

        $page = $creativeService->getCreativesByStatement(
            $statementBuilder->toStatement()
        );
        $arrayResults = $page->getResults();
        $arrayRegistros = array();

        //se tem criativos
        if(!empty($arrayResults)){
            foreach($arrayResults as $i => $result){
                $arrayRegistros['link'] = $result->getDestinationUrl();    
                $image = $result->getPrimaryImageAsset();
                $arrayRegistros['src'] = $image->getAssetUrl();       
                $size =   $image->getSize();    
                $arrayRegistros['width'] = $size->getWidth();
                $arrayRegistros['height'] = $size->getHeight();      
            }
        }
        
        return $arrayRegistros;
    }

    static public function listarPedidos($status = 1, $pedidoId = null){

        $session = self::autenticar();
        $dfpServices = new DfpServices();
        $orderService = $dfpServices->get($session, OrderService::class);

        $jornalId = Configure::read('Dfp.SlotTopId');
        $statementBuilder = new StatementBuilder();
        $statementBuilder->orderBy('startDateTime DESC');
        $statementBuilder->where('poNumber = :jornalId AND isArchived <> 1 AND status = :status');
        $statementBuilder->withBindVariableValue(
            'jornalId',
            $jornalId
        );
        $statementBuilder->withBindVariableValue(
            'status',
            'APPROVED'
        );
        //Se apenas os aprovados
        if($status <> 1){
            $statementBuilder->where('poNumber = :jornalId AND status != :status');
        }
        //Se foi informado o pedido
        if(!empty($pedidoId)){
            $statementBuilder->where('poNumber = :jornalId AND orderId = :orderId');
            $statementBuilder->withBindVariableValue(
                'orderId',
                $pedidoId
            );
        }        

        $page = $orderService->getOrdersByStatement(
            $statementBuilder->toStatement()
        );
        $arrayResults = $page->getResults();

        $arrayRegistros = array();
        foreach($arrayResults as $i => $result){

            $arrayRegistros[$i]['id'] = $result->getId();
            $arrayRegistros[$i]['jornalId'] = $jornalId;
            $arrayRegistros[$i]['titulo'] = !empty($result->getNotes()) ? $result->getNotes() : $result->getName();
            $arrayRegistros[$i]['notes'] = $result->getNotes();  
            $arrayRegistros[$i]['inicio'] = date('d/m/Y H:i', strtotime(self::tratarData($result->getStartDateTime())));
            $dataFim = 'Não informado';
            //se não é ilimitado
            if($result->getEndDateTime()){
                $dataFim = date('d/m/Y H:i', strtotime(self::tratarData($result->getEndDateTime())));
            }
            $arrayRegistros[$i]['fim'] = $dataFim;
            $arrayRegistros[$i]['anuncianteId'] = $result->getAdvertiserId();
            $arrayRegistros[$i]['entregas'] = $result->getTotalImpressionsDelivered();
            $arrayRegistros[$i]['cliques'] = $result->getTotalClicksDelivered();
            $arrayRegistros[$i]['impressoes'] = $result->getTotalImpressionsDelivered();
            $arrayRegistros[$i]['status'] = $result->getIsArchived() ? 0 : 1;
            //pega o criativo
            $arrayRegistros[$i]['criativo'] = self::getCriatives($result->getId(), $session);
            $arrayRegistros[$i]['lineitem'] = self::getLineItem($result->getId(), $session);
            //$arrayRegistros[$i]['criativo'] = array();
            
        }
        return $arrayRegistros;
    }

    static public function getAnunciante($name = 'MSWI'){

        $session = self::autenticar();
        $dfpServices = new DfpServices();
        $companyService = $dfpServices->get($session, CompanyService::class);

        $statementBuilder = (new StatementBuilder())->where('name = :name')
            ->orderBy('id ASC')
            ->withBindVariableValue('name', $name);
        $results = $companyService->getCompaniesByStatement(
            $statementBuilder->toStatement()
        );
        $arrayResults = $results->getResults();

        //se NÃO encontrou
        if(empty($arrayResults)){
            //Cria uma nova
            $company = new Company();
            $company->setName($name);
            $company->setType(CompanyType::ADVERTISER);
            try{
                $results = $companyService->createCompanies([$company]);
                foreach ($results as $i => $company) {
                    return $company->getId();
                }
            } catch(ApiException $e){
                return $e->getMessage();
            }
        }else{
            foreach($arrayResults as $company){
                return $company->getId();
            }
        }
    }

    static public function tratarData($date){
        $arrayDate = $date->getDate();
        $year       = $arrayDate->getYear();
        $month      = str_pad($arrayDate->getMonth(),2,'0', STR_PAD_LEFT);
        $day        = str_pad($arrayDate->getDay(),2,'0', STR_PAD_LEFT);
        $hour       = str_pad($date->getHour(),2,'0', STR_PAD_LEFT);
        $minute     = str_pad($date->getMinute(),2,'0', STR_PAD_LEFT);
        $second     = str_pad($date->getSecond(),2,'0', STR_PAD_LEFT);
        return $year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.$second;
    }

    static public function relatoriosSalvos($pedidoId, $dataInicial = '-30 days', $dataFinal = 'now')
    {
        $session = self::autenticar();
        $dfpServices = new DfpServices();
        $reportService = $dfpServices->get($session, ReportService::class);

        // Create report query.
        $reportQuery = new ReportQuery();
        $reportQuery->setDimensions(
            [
                Dimension::LINE_ITEM_NAME,
                Dimension::ORDER_NAME,
                Dimension::LINE_ITEM_ID,
                Dimension::ORDER_ID,
                Dimension::DATE,
                Dimension::DAY
            ]
        );
        $reportQuery->setDimensionAttributes(
            [
                DimensionAttribute::ORDER_START_DATE_TIME,
                DimensionAttribute::ORDER_END_DATE_TIME
            ]
        );
        $reportQuery->setColumns(
            [
                Column::AD_SERVER_IMPRESSIONS,
                Column::AD_SERVER_CLICKS
            ]
        );

        // Create statement to filter for an order.
        $statementBuilder = (new StatementBuilder())->where('ORDER_ID = :orderId')
            ->withBindVariableValue('orderId', $pedidoId);

        // Set the filter statement.
        $reportQuery->setStatement($statementBuilder->toStatement());

        // Set the start and end dates or choose a dynamic date range type.
        $reportQuery->setDateRangeType(DateRangeType::CUSTOM_DATE);
        $reportQuery->setStartDate(
            DfpDateTimes::fromDateTime(
                new DateTime(
                    $dataInicial,
                    new DateTimeZone('America/Sao_Paulo')
                )
            )
            ->getDate()
        );
        $reportQuery->setEndDate(
            DfpDateTimes::fromDateTime(
                new DateTime(
                    $dataFinal,
                    new DateTimeZone('America/Sao_Paulo')
                )
            )
            ->getDate()
        );

        // Create report job and start it.
        $reportJob = new ReportJob();
        $reportJob->setReportQuery($reportQuery);
        $reportJob = $reportService->runReportJob($reportJob);

        // Create report downloader to poll report's status and download when ready.
        $reportDownloader = new ReportDownloader($reportService, $reportJob->getId());
        if ($reportDownloader->waitForReportToFinish()) {
            $options = new ReportDownloadOptions(ExportFormat::XML, false, true, false);
            $reportDownloadUrl = $reportService->getReportDownloadUrlWithOptions($reportJob->getId(), $options);

            $reportContents = file_get_contents($reportDownloadUrl);

            // XML to Array
            $xml = simplexml_load_string($reportContents);
            $json = json_encode($xml);
            $data = json_decode($json, true);
            
            if (isset($data['ReportData']['DataSet']['Row']) && !empty($data['ReportData']['DataSet']['Row'])) {
                return array(
                    'columnheaders' => $data['ReportData']['ColumnHeaders']['ColumnHeader'],
                    'dataset' => $data['ReportData']['DataSet']
                );
            }
        }

        return false;
    }
}
?>
