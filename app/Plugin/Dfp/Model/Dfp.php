<?php
App::uses('AppModel', 'Model');

class Dfp extends DfpAppModel {

    public $useTable = false;

    public function diasDaSemanda()
    {
        return array(
            'Monday' => 'Segunda feira',
            'Tuesday' => 'Terça feira',
            'Wednesday' => 'Quarta feira',
            'Thursday' => 'Quinta feira',
            'Friday' => 'Sexta feira',
            'Saturday' => 'Sábado',
            'Sunday' => 'Domingo'
        );
    }
} 