<?php
App::uses('AppHelper', 'View/Helper');

class DfpHelper extends AppHelper {
	
	public $helpers = array(
		'Html',
		'Form',
		'Menus'
	);
	
	public function getStatusLineItem($status) {
		$arrayStatus = array(
			'DELIVERING' 	=> 'Entregando',
			'PAUSED' 		=> 'Pausado',
			'READY' 		=> 'Pronto',
			'INACTIVE' 		=> 'Inativo',
			'COMPLETED' 	=> 'Completo',
			'CANCELED' 		=> 'Cancelado',
			'DRAFT' 		=> 'Excluído',
			'DELIVERY_EXTENDED' => 'DELIVERY_EXTENDED',
			'PAUSED_INVENTORY_RELEASED' => 'PAUSED_INVENTORY_RELEASED',
			'PENDING_APPROVAL' => 'PENDING_APPROVAL',
			'DISAPPROVED' => 'DISAPPROVED'
			
		);
		return $arrayStatus[$status];
	}
}