<style>
	table {
		width: 100%;
		margin-bottom: 15px;
		border-collapse: collapse;
	}
	td, th {
		border: 1px solid #cdcdcd;
		padding: 5px;
	}
	tr:nth-child(even) {
		background-color: #f2f2f2;
	}
	.bold {
		font-weight: bold;
	}
	.center {
		text-align: center;
	}
</style>

<table>
	<tr>
		<td class="center bold" colspan="8"><?php echo Configure::read('Site.title'); ?></td>
	</tr>
</table>

<table>
	<tr>
		<th>Item de linha</th>
		<th>Pedido</th>
		<th>ID do Item de linha</th>
		<th>ID do Pedido</th>
		<th>Início do Pedido</th>
		<th>Fim do Pedido</th>
		<th>Data</th>
		<th>Dia da Semana</th>
		<th>Impressões</th>
		<th>Cliques</th>
	</tr>
	<?php foreach ($registros as $key => $registro): ?>
		<tr>
			<td><?php echo $registro['lineItemName']; ?></td>
			<td><?php echo $registro['orderName']; ?></td>
			<td><?php echo $registro['lineItemId']; ?></td>
			<td><?php echo $registro['orderId']; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($registro['orderStartDate'])); ?></td>
			<td><?php echo ($registro['orderEndDate'] != 'Unlimited') ? date('d/m/Y H:i', strtotime($registro['orderEndDate'])) : 'Não informado'; ?></td>
			<td><?php echo date('d/m/Y', strtotime($registro['date'])); ?></td>
			<td><?php echo $diasDaSemanda[$registro['dayOfWeek']]; ?></td>
			<td><?php echo str_replace(',', '.', $registro['reservationImpressionsDelivered']); ?></td>
			<td><?php echo str_replace(',', '.', $registro['reservationClicksDelivered']); ?></td>
		</tr>
	<?php endforeach; ?>
</table>
