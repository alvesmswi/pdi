<?php 
    $this->Html->addCrumb($this->Html->icon('home'), '/admin');
    $this->Html->addCrumb('Anúncios', '/admin/dfp');
    $this->Html->addCrumb('Relatório', $this->here);

    echo $this->Form->create('Dfp',
        array(
            'class' => 'form-inline clearfix filter'
        )
    );
    echo $this->Form->input('pedidoId',
        array(
            'label' => false,
            'class' => 'input text',
            'div' => false,
            'placeholder' => 'ID do Pedido',
            'required' => true
        )
    );
    echo $this->Form->input('dataInicial',
        array(
            'label' => false,
            'class' => 'input text datetime2',
            'div' => false,
            'placeholder' => 'Data Inicial',
            'autocomplete' => 'off',
            'required' => true
        )
    );
    echo $this->Form->input('dataFinal',
        array(
            'label' => false,
            'class' => 'input text datetime2',
            'div' => false,
            'placeholder' => 'Data Final',
            'autocomplete' => 'off',
            'required' => true
        )
    );
    echo $this->Form->submit('Filtrar',
        array(
            'class' => 'btn btn-default input-block-level',
            'div' => false,
            'onclick' => "$('#divLoading').show();"
        )
    );

    echo '<a href="/admin/dfp/dfp_reports/index?excel=true" target="_blank" class="btn btn-success" style="margin:10px;">Excel</a>';
    echo '<a href="/admin/dfp/dfp_reports/index?limpar=true" class="btn btn-warning" style="margin:10px;">Limpar</a>';

    echo $this->Form->end();
?>

<?php if (!empty($registros)): ?>
    <div class="row-fluid">
        <div class="span12">
            <table cellpadding="0" cellspacing="0"  class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Item de linha</th>
                        <th>Pedido</th>
                        <th>ID do Item de linha</th>
                        <th>ID do Pedido</th>
                        <th>Início do Pedido</th>
                        <th>Fim do Pedido</th>
                        <th>Data</th>
                        <th>Dia da Semana</th>
                        <th>Impressões</th>
                        <th>Cliques</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($registros as $registro): ?>
                        <tr>
                            <td><?php echo $registro['lineItemName']; ?></td>
                            <td><?php echo $registro['orderName']; ?></td>
                            <td><?php echo $registro['lineItemId']; ?></td>
                            <td><?php echo $registro['orderId']; ?></td>
                            <td><?php echo date('d/m/Y H:i', strtotime($registro['orderStartDate'])); ?></td>
                            <td><?php echo ($registro['orderEndDate'] != 'Unlimited') ? date('d/m/Y H:i', strtotime($registro['orderEndDate'])) : 'Não informado'; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($registro['date'])); ?></td>
                            <td><?php echo $diasDaSemanda[$registro['dayOfWeek']]; ?></td>
                            <td><?php echo str_replace(',', '.', $registro['reservationImpressionsDelivered']); ?></td>
                            <td><?php echo str_replace(',', '.', $registro['reservationClicksDelivered']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<script src="/details/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="/details/css/theme.css"/>
<script>
    $(document).ready(() => {
        $('.datetime2').datetimepicker({
            dateFormat: 'dd/mm/yy',
            timeFormat: 'hh:mm'
        });

        $('#ui-datepicker-div').hide();
    });
</script>
