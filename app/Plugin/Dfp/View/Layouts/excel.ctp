<?php

header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/vnd.ms-excel; charset=utf-8");
header ("Content-Disposition: attachment; filename=\"".strtolower($this->params['controller'])."-".strtolower($this->params['action'])."-".date('Ymd')."-".date('H\hi').".xls\"" );
header ("Content-Description: Relatório Gerado" );

// Conteúdo
echo utf8_decode($this->fetch('content'));
