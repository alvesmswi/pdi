<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('DFP', array(
    'admin' => true,
    'plugin' => 'Dfp',
    'controller' => 'Dfp',
    'action' => 'config',
  ));

$this->append('form-start', $this->Form->create('Dfp', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
    echo $this->Form->input('Dfp.SlotTopId', array(
        'label' => 'ID do Slot principal do Jornal (Criar no DFP)',
        'required'	=> true
    ));
    echo $this->Form->input('Dfp.TraffickerId', array(
        'label' => 'ID do Publicador Autorizado (Não alterar)',
        'required'	=> true
    ));  
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());