<?php 
$this->Html->addCrumb($this->Html->icon('home'), '/admin');
$this->Html->addCrumb('Anúncios', '/admin/dfp');
$this->Html->addCrumb('Blocos', $this->here);
?>

<div class="row-fluid">
	<div class="span12">
		<table cellpadding="0" cellspacing="0"  class="table table-striped table-bordered table-condensed">
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Ações</th>
			</tr>
			<?php
			foreach ($registros as $id => $titulo): 
				$linkModal = $this->Html->url(array(
					'admin' => true,
					'plugin' => 'dfp',
					'controllers' => 'dfp',
					'action' => 'get_tag',
					$id
				));
			?>
				<tr>
					<td><?php echo $id; ?></td>
					<td><?php echo $titulo; ?></td>
					<td>
						<div class="item-actions">
							<?php	
							echo $this->element('admin/modal', array(
								'id' => $id,
								'title' => 'TAG do bloco de anúncio',
							));
							echo $this->Html->link(
								'Ver TAG', 
								'#'.$id, 
								array(
									'button' => 'default',
									//'icon' => $this->Theme->getIcon('link'),
									//'iconSize' => 'small',
									'data-title' => 'TAG do bloco de anúncio',
									'data-toggle' => 'modal',
									'data-remote' => $linkModal
								)
							);		
							?>
						</div>
					</td>
				</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>