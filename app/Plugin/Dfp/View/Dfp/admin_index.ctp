<?php 
$this->Html->addCrumb($this->Html->icon('home'), '/admin');
$this->Html->addCrumb('Anúncios', $this->here);
?>

<div class="row-fluid">
	<div class="span12 actions">
		<ul class="nav-buttons">
			<li><a href="/admin/dfp/dfp/add" method="get" class="btn btn-success">Novo Anúncio</a></li>
		</ul>
	</div>
</div>
<?php 
echo $this->Form->create(
	'Dfp',
	array(
		'class' => 'form-inline clearfix filter'
	)
);
/*echo $this->Form->input(
	'status',
	array(
		'label' => false,
		'class' => 'input text',
		'div' => false,
		'options' => array(
			1 => 'Publicado',
			0 => 'Não publicado'
		)
	)
);*/
echo $this->Form->input(
	'pedidoId',
	array(
		'label' => false,
		'class' => 'input text',
		'div' => false,
		'placeholder' => 'ID do Anúncio'
	)
);
echo $this->Form->submit(
	'Filtrar',
	array(
		'class' => 'btn btn-default input-block-level',
		'div' => false
	)
);
echo $this->Form->end();
?>
<div class="row-fluid">
	<div class="span12">
		<table cellpadding="0" cellspacing="0"  class="table table-striped table-bordered table-condensed">
			<tr>
				<th>Criativo</th>
				<th>Tamanho</th>
				<th>ID</th>
				<th>Nome</th>
				<th>Início</th>
				<th>Fim</th>
				<th>Meta</th>
				<th>Impressões</th>
				<th>Cliques</th>
				<th>Status</th>
				<th>Ações</th>
			</tr>
			<?php
			foreach ($registros as $registro): 
				$criativoSrc = '';
				$dimensoes = '';
				$criativoSrc = '';
				//se tem criativo
				if(isset($registro['criativo']['src']) && !empty($registro['criativo']['src'])){
					$criativoSrc = '<img src="'.$registro['criativo']['src'].'" width="100px" />';
					$dimensoes = $registro['criativo']['width'].'x'.$registro['criativo']['height'];
				}
			?>
				<tr>
					<td><?php echo $criativoSrc; ?></td>
					<td><?php echo $dimensoes; ?></td>
					<td><?php echo $registro['id']; ?></td>
					<td><?php echo $registro['titulo']; ?></td>
					<td><?php echo $registro['inicio']; ?></td>
					<td><?php echo $registro['fim']; ?></td>
					<td><?php echo isset($registro['lineitem']['quantidade']) ? ($registro['lineitem']['quantidade']) : '-'; ?></td>
					<td><?php echo $registro['impressoes']; ?></td>
					<td><?php echo $registro['cliques']; ?></td>
					<td><?php echo  $this->Dfp->getStatusLineItem($registro['lineitem']['status']); ?></td>
					<td>
						<div class="item-actions">
							<?php
							echo $this->Croogo->adminRowActions($registro['id']);							
							
							//Se está ativo
							if($registro['lineitem']['status'] != 'PAUSED'){	
								echo ' ' . $this->Croogo->adminRowAction('',
									array('action' => 'pausar', $registro['id']),
									array(
										'icon' => $this->Theme->getIcon('pause'), 
										'tooltip' => 'Pausar', 
										'confirm' => 'Tem certeza que deseja PAUSAR este anúncio?',
										'onclick' => '$("#divLoading").show();'
									)
								);
							}

							//Se está pausado
							if($registro['lineitem']['status'] == 'PAUSED'){
								echo ' ' . $this->Croogo->adminRowAction('',
									array('action' => 'retomar', $registro['id']),
									array(
										'icon' => $this->Theme->getIcon('play'), 
										'tooltip' => 'Retomar', 
										'confirm' => 'Tem certeza que deseja RETOMAR este anúncio?',
										'onclick' => '$("#divLoading").show();'
									)
								);
							}

							echo ' ' . $this->Croogo->adminRowAction('',
								array('action' => 'edit', $registro['id']),
								array(
									'icon' => $this->Theme->getIcon('update'), 
									'tooltip' => 'Editar',
									'onclick' => '$("#divLoading").show();'
								)
							);
							
							echo ' ' . $this->Croogo->adminRowAction('',
								array('action' => 'arquivar', $registro['id']),
								array(
									'icon' => $this->Theme->getIcon('delete'), 
									'tooltip' => 'Excluir', 
									'confirm' => 'Tem certeza que deseja EXCLUIR este anúncio?',
									'onclick' => '$("#divLoading").show();'
								)
							);	

							echo ' ' . $this->Croogo->adminRowAction('',
								array('controller' => 'dfp_reports', 'action' => 'index', $registro['id']),
								array(
									'icon' => $this->Theme->getIcon('th-list'), 
									'tooltip' => 'Relatório', 
									'target' => '_blank'
								)
							);
														
							?>
						</div>
					</td>
				</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>