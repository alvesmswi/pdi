<?php
$this->extend('/Common/admin_edit');

$this->Html->addCrumb($this->Html->icon('home'), '/admin');
$this->Html->addCrumb('Anúncios', '/admin/dfp');
//pr($this->request->data);
//ADICIONANDO
if($this->action == 'admin_add'){
    $this->Html->addCrumb('Novo Anúncio', $this->here);
    $inicio = date('d/m/Y H:i:s');    
    $fim = '';
    $status =  1;
    $disabledTipo = '';    
    $requiredImg = 'required';
    $disabledAgora = '';
//EDITANDO 
}else{
    $this->Html->addCrumb('Editar Anúncio', $this->here);
    $inicio = $this->data['Dfp']['inicio'];
    $fim = isset($this->data['Dfp']['fim']) ? $this->data['Dfp']['fim'] : '';
    $status =  $this->data['Dfp']['status'] == 'APPROVED' ? 1 : 0;
    $disabledTipo = 'disabled';
    $requiredImg = '';
    $disabledAgora = '';
    //se a data é menor que agora
    if($this->data['Dfp']['inicio'] <= date('Y-m-d H:i:s')){
        $disabledAgora = 'disabled';
    }
}

//se tem data de fim
if(isset($this->request->data['Dfp']['fim'])){
    $this->request->data['Dfp']['fim'] = date('d/m/Y H:i:s', strtotime($this->request->data['Dfp']['fim']));
}

$this->append('form-start', $this->Form->create('Dfp', array(
    'class' => 'protected-form',
    'type'=>'file',
    'class' => 'form-loading'
)));

$this->append('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#node-main');
$this->end();

$this->append('tab-content');
    echo $this->Html->tabStart('node-main');
    
        echo $this->Form->hidden('Dfp.id');

        echo $this->Form->input('Dfp.tipo', array(
            'label' => 'Tipo do anúncio',
            'options' => array(
                'STANDARD' => 'Campanha por impressões (Padrão)',
                'SPONSORSHIP' => 'Campanha por tempo (Patrocínio)'                
            ),
            'disabled' => $disabledTipo
        ));

		echo $this->Form->input('Dfp.titulo', array(
            'label' => 'Título do anúncio',
            'required' => 'required'
        ));
        
        echo $this->Form->input('Dfp.quantidade', array(
            'label' => 'Quantidade de impressões',
            'type' => 'number'
        ));
        echo $this->Form->input('Dfp.percentual', array(
            'label' => 'Prioridade no revezamento de anúncios (Máximo 100)',
            'type' => 'number',
            'max' => 100
        ));
        echo $this->Form->input('Dfp.slot', array(
            'label' => 'Posição onde o anúncio vai ser exibido',
            'options' => $arraySlots
        ));
        
        echo $this->Form->input('Dfp.link', array(
            'label' => 'Link do anúncio (ex: www.google.com.br)',
            'required' => 'required'
        ));

        //se tem imagem
        if(isset($this->data['Dfp']['src'])){
            echo '<img src="'.$this->data['Dfp']['src'].'" class="thumbnail" />';
        }     

        echo $this->Form->input('Dfp.imagem', array(
            'label' => 'Imagem que será exibida no anúncio',
            'type' => 'file',
            'required' => $requiredImg
        ));
        
    echo $this->Html->tabEnd();

echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação');
    
    echo $this->Form->button('Salvar', array('button' => 'default'));
    
    //se estão editando
    if($this->action == 'admin_edit'){
        echo $this->Html->link(
            'Excluir', 
            array('action' => 'arquivar', $this->request->data['Dfp']['id']), 
            array(
                'confirm' => 'Tem certeza que deseja EXCLUIR este anúncio?',
                'button' => 'danger'
            )
        );
    }

    
    echo $this->Form->input('Dfp.iniciaragora', array(
        'label' => 'Iniciar a publicação agora',
        'type' => 'checkbox',
        'disabled' => $disabledAgora
    ));

    echo $this->Form->input('Dfp.inicio', array(
        'label' => 'Início',        
        'class' => 'input-datetime',
        'value' => $inicio,
    ));
    echo $this->Form->input('Dfp.fim', array(
        'label' => 'Fim',
        'class' => 'input-datetime data-fim',
        'value' => $fim,
    )); 
?>
    <script type="text/javascript" src="/details/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="/details/css/theme.css"/>
	<script type="text/javascript">

        //Quando iniciar o formulario:
		$(document).ready(function(){            
            tipoAnuncio();  
            iniciarAgora(); 
        ;});

        //Configura o calendario
        $('.input-datetime').datetimepicker({
            dateFormat: 'dd/mm/yy', 
            timeFormat: 'hh:mm:ss'
        })
        
        //Quando alterar o tipo de anuncio
        $( "#DfpTipo" ).change(function() {                
            tipoAnuncio();               
        });

        //se for para iniciar agora            
        $( "#DfpIniciaragora" ).change(function() {
            iniciarAgora();                   
        });

        //se não tiver fim            
        $( "#DfpSemfim" ).change(function() {
            semFim();              
        });

        function tipoAnuncio(tipo){
            var tipo = $("#DfpTipo").val();
            //Se a campanha for impressões (Patrocínio)
            if(tipo == 'SPONSORSHIP'){                                       
                //DESATIVA os elementos desnecessários   
                $('#DfpQuantidade').attr('disabled', 'disabled');                      
                //ATIVA os elementos necessários 
                $('#DfpSemfim').removeAttr('disabled');   
                $('#DfpPercentual').removeAttr('disabled');  
                //COLOCA obrigatorio                      
                $('#DfpPercentual').attr('required', 'required'); 
                //REMOVE obrigatorio  
                $('#DfpFim').removeAttr('required'); 
                $('#DfpQuantidade').removeAttr('required');                                              
            //Se for padrão
            }else{
                //ATIVA os elementos necessários
                $('#DfpQuantidade').removeAttr('disabled');                                      
                //DESATIVA os elementos desnecessários  
                $('#DfpSemfim').attr('disabled', 'disabled');   
                $('#DfpPercentual').attr('disabled', 'disabled'); 
                //COLOCA obrigatorio
                $('#DfpFim').attr('required', 'required'); 
                $('#DfpQuantidade').attr('required', 'required');                       
                //REMOVE obrigatorio  
                $('#DfpPercentual').removeAttr('required');    
            } 
        }

        function iniciarAgora(){
            //se não tiver fim o anúncio
            if($("#DfpIniciaragora").is(':checked')){
                $('#DfpInicio').attr('disabled', 'disabled');                    
            //Se for padrão
            }else{
                $('#DfpInicio').removeAttr('disabled');
            }
        }
        
    </script>
<?php
echo $this->Html->endBox();
echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());
