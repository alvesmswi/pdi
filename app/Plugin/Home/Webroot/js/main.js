$('#HomeDestaquesTopo').selectize({//inpu
    plugins: ['drag_drop','remove_button'],
    valueField: 'id',    
    labelField: 'title',
    searchField: ['id', 'title'],
    maxItems: 16,
    options: JSON.parse($('#HomeHiddenDestaquesTopo').val()),//hidden
    create: false,
    render: {
        item: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        },
        option: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        }
    },
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/admin/home/home/search_node',
            type: 'POST',
            dataType: 'json',
            data: {
                keyword: query,
               // nodeId: $('#NodeId').val()
            },
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.nodes.slice(0, 10));
            }
        });
    }
});

$('#HomeCategoriaDestaque').selectize({//inpu
    plugins: ['drag_drop','remove_button'],
    valueField: 'id',
    labelField: 'title',
    searchField: ['id', 'title'],
    maxItems: 3,
    options: JSON.parse($('#HomeHiddenCategoriaDestaque').val()),//hidden
    create: false,
    render: {
        item: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        },
        option: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        }
    },
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/admin/home/home/search_node',
            type: 'POST',
            dataType: 'json',
            data: {
                keyword: query
            },
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.nodes.slice(0, 10));
            }
        });
    }
});

$('#HomeHomeLista').selectize({//inpu
    plugins: ['drag_drop','remove_button'],
    valueField: 'id',
    labelField: 'title',
    searchField: ['id', 'title'],
    maxItems: 6,
    options: JSON.parse($('#HomeHiddenHomeLista').val()),//hidden
    create: false,
    render: {
        item: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        },
        option: function (item, escape) {
            return '<div>' +
                '<span>' + escape(item.id) + ' - ' + escape(item.title) + '</span>' +
                '</div>';
        }
    },
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/admin/home/home/search_node',
            type: 'POST',
            dataType: 'json',
            data: {
                keyword: query
            },
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.nodes.slice(0, 10));
            }
        });
    }
});