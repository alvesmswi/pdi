var app = angular.module('newsApp',['ngDragDrop']);

app.controller('newsletterBuilder', function ($scope, $http) {
    // var newsList = this;
    $http.get("/admin/home/home/get_news.json")
        .then(function(res) {
           $scope.news = res.data;
        });

    // $scope.list1 = [];
    $scope.destaque = {};
    $scope.news1 = {};
    $scope.news2 = {};
    $scope.news3 = {};
    $scope.news4 = {};
    $scope.destaque2 = {};
    $scope.news5 = {};
    $scope.news6 = {};
    $scope.news7 = {};
    $scope.news8 = {};

    $scope.sendDatas = function() {
        var allDatas = {
            Destaque1: $scope.destaque,
            Destaque2: $scope.destaque2,
            News1: $scope.news1,
            News2: $scope.news2,
            News3: $scope.news3,
            News4: $scope.news4,
            News5: $scope.news5,
            News6: $scope.news6,
            News7: $scope.news7,
            News8: $scope.news8
        }
        // use $.param jQuery function to serialize data from JSON 
        var data = allDatas;
        console.log(data);

        var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
        }
            
        $http.post(url, data, config)
            .success(function (data, status, headers, config) {
                    $scope.PostDataResponse = data;
                })
                .error(function (data, status, header, config) {
                    $scope.ResponseDetails = "Data: " + data +
                        "<hr />status: " + status +
                        "<hr />headers: " + header +
                        "<hr />config: " + config;
                });
    };
});

// jQuery
$(function () {
    $("[data-drop='true']").droppable({
        hoverClass: "dashed"
        
    });
});

