<?php
Croogo::hookRoutes('Home');

CroogoNav::add('sidebar', 'Home', array(
	'icon' => 'cog',
	'title' => 'Config. Home',
	'url' => array(
		'admin' => true,
		'plugin' => 'home',
		'controller' => 'home',
		'action' => 'index',
	)
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Onesignal');
    
?>