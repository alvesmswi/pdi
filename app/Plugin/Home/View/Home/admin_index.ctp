<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Site', array(
    'admin' => true,
    'plugin' => 'Site',
    'controller' => 'Site',
    'action' => 'index',
  ));

$this->append('form-start', $this->Form->create('Site', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configuração da Home', '#tab-home');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-home');

        echo $this->Html->css(array('Home.selectize.default'));

        echo '<fieldset class="box">';
            echo '<legend style="border:0 none;padding-left:20px;"><b>Bloco de Destaque Principal</b></legend>';
            $hiddenValue = isset($this->data['Home']['hidden_destaques_topo']) ? $this->data['Home']['hidden_destaques_topo'] : '[]' ;
            echo $this->Form->hidden('Home.hidden_destaques_topo', array('value' => $hiddenValue,'disabled'=>'disabled'));
            echo $this->Form->input('Home.destaques_topo', array(
                'multiple' => true,
                'placeholder' => 'Busque a Notícia pelo Título ou ID',
                'label' => 'Materias exibidas neste bloco (Em ordem)'
            ));
        echo '</fieldset>';

        
        echo '<fieldset class="box">';
            echo '<legend style="border:0 none;padding-left:20px;"><b>Bloco de Destaque Central</b></legend>';
            $hiddenValue = isset($this->data['Home']['hidden_categoria_destaque']) ? $this->data['Home']['hidden_categoria_destaque'] : '[]' ;
            echo $this->Form->hidden('Home.hidden_categoria_destaque', array('value' => $hiddenValue,'disabled'=>'disabled'));
            echo $this->Form->input('Home.categoria_destaque_title', array(
                'label' => 'Título do Bloco',
            ));
            echo $this->Form->input('Home.categoria_destaque', array(
                'multiple' => true,
                'placeholder' => 'Busque a Notícia pelo Título ou ID',
                'label' => 'Materias exibidas neste bloco (Em ordem)'
            ));
        echo '</fieldset>';

        
        echo '<fieldset class="box">';
            echo '<legend style="border:0 none;padding-left:20px;"><b>Bloco de lista de notícias da home</b></legend>';
            $hiddenValue = isset($this->data['Home']['hidden_home_lista']) ? $this->data['Home']['hidden_home_lista'] : '[]' ;
            echo $this->Form->hidden('Home.hidden_home_lista', array('value' => $hiddenValue,'disabled'=>'disabled'));
            echo $this->Form->input('Home.home_lista_title', array(
                'label' => 'Título do Bloco',
            ));
            echo $this->Form->input('Home.home_lista', array(
                'multiple' => true,
                'placeholder' => 'Busque a Notícia pelo Título ou ID',
                'label' => '3 primeiras na lista de notícias na Home',
                'div' => array(
                    'style' => 'padding-top: 10px;',
                )
            ));
        echo '</fieldset>';

        echo $this->Html->script(array('Home.selectize.min', 'Home.main'));
       
    echo $this->Html->tabEnd();


    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());

