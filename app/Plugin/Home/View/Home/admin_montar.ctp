<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="newsApp">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <!--<![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Construa a Newsletter</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular-animate.min.js"></script>
    <?php 
    
    echo $this->Html->script(
        array(
            'Home.angular-dragdrop.min', 
            'Home.build'
        )
    );
    echo $this->Html->css(
        array(
            'Home.styles'
        )
    );
    ?>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Asap+Condensed:400,600,700');

        @-ms-viewport { width: device-width; } 

        body {
            font-family: 'Asap Condensed', sans-serif;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            margin: 0;
            padding: 0;
            background-color: #fff;

        }
    
        /* .ReadMsgBody {
            width: 100%;
        } */

        a,
        h1,
        p,
        span {
            padding: 0px;
            margin: 0px;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
            color: #444;
        }

        p {
            font-weight: 400;
        }

        .block-mark {
            width: 5px;
            height: 100%;
            background-color: #CCC;
        }

        .logo-jornal {
            max-width: 90% !important;
            height: auto;
        }


        .published {
            height: 24px;
            font-size: 12px;
            line-height: 24px;
            color: #888;
        }

        .bg--escuro {
            background-color: #4B4B4B;
            padding: 6px 0px;
        }

        .two-column {
            text-align: left;
            font-size: 0;
        }

        .two-column .column {
            width: 48%;
            display: inline-block;
            vertical-align: top;
            padding: 6px 10px 12px;
            min-width: 280px;
        }

        .two-column .col-1 {
            padding-left: 0px;
            /* float: left; */

        }

        .two-column .col-2 {
            padding-right: 0px !important;
            /* float: right; */
        }

        .destaque {
            padding-top: 12px!important;
            padding-bottom: 24px !important;
        }

        .btn-social a:hover {
            text-decoration: none;
            opacity: .7;
        } 

        @media screen and (max-width: 630px) {
            .content {
                padding: 0 10px;
            }
            .two-column .column {
                width: 100% !important;
                margin: 0 auto!important;
                padding: 12px 0px 12px;
                float: left!important;
            }
            .bg--escuro {
                padding: 0 15px;
                margin: 0 -10px !important;
            }
            .bg--footer {
                padding: 0 10px;
                background-color: #999 !important;
            }

        }
    </style>

</head>

<body bgcolor="fffff" style="text-align:center; padding: 0px!important; margin:0px!important" ng-controller="newsletterBuilder">

    <div class="container">
        <div class="main" >
            <!-- TABELA A SER ATUALIZADA -->
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                    <!-- MENSAGEM TOP -->
                    <tr>
                        <td align="center" bgcolor="#444" style="padding:7px; font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#FFFFFF;">Caso não veja as imagens, clique
                            <a href="#" target="_blank" style="color:#FFFFFF;">aqui</a> para ver a versão online.</td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#444" style="padding:7px; font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#FFFFFF;" ng-hide="!PostDataResponse">{{PostDataResponse}}</td>
                    </tr>
                     <tr>
                        <td align="center" bgcolor="#444" style="padding:7px; font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#FFFFFF;" ng-hide="!ResponseDetails">{{ResponseDetails}}</td>
                    </tr>
            
                    <!-- HEADER -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;" class="content">
                            <table align="center" cellpadding="0" cellspacing="0" class="header" style="max-width:600px;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:0px solid #A6A5A5;">
                                            <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="100%" height="168px" align="center">
                                                            <a href="http://www.oparana.com.br/" title="www.oparana.com.br" target="_blank">
                                                                <img class="logo-jornal" src="https://www.oparana.com.br/img/logo.png" width="300" height="79" alt="Jornal O Paraná" style="display:block;">
                                                            </a>
            
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            
                    <!-- DESTAQUE -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;" class="content">
                            <table  id="news{{destaque.id}}" cellpadding="0" cellspacing="0" width="100%" align="center" class="destaque" style="max-width:600px;"
                            data-drop="true" data-jqyoui-options ng-model="destaque" jqyoui-droppable>
                                <tbody>
                                    <!-- Titulo destaque -->
                                    <tr height="48">
                                        <td class="title-section" width="100%" height="36" style="vertical-align:middle">
                                            <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px">
                                            </span>
                                            <span style="font-size:34px;line-height:36px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:900; letter-spacing: -1px;">EM DESTAQUE</span>
                                        </td>
                                    </tr>
                                    <!-- Imagem do Destaque -->
                                    <tr>
                                        <td class="img-destaque" height="100%" style="overflow:hidden!important; display:inline-block;" align="center">
                                            <img data-drag="false" src="{{destaque.image}}" width="100%" height="400px" alt="Imagem de Destaque" style="margin:0 auto;" ng-hide="!destaque.image">
                                        </td>
                                    </tr>
                                    <!-- Categoria -->
                                    <tr height="36px">
                                        <td height="36px">
                                            <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                            </span>
                                            <span class="category" style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle; position:relative"> <span ng-hide="destaque.category">Categoria</span> {{destaque.category}}
                                            <!-- <div class="placeholder"> </div>-->
                                            </span> 
                                            
                                        </td>
                                    </tr>
                                    <!-- Título -->
                                    <tr>
                                        <td height="36px">
                                            <a href="#">
                                                <h1 width="100%" style="color:#444; font-size:36px; line-height: 36px; font-weight:600;">{{destaque.title}}
                                                <span ng-hide="destaque.title">Título</span>
                                                </h1>
                                            </a>
                                            <span class="published">{{destaque.date}}
                                                <span ng-hide="destaque.date">Data de publicação</span>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
            
                            </table>
                        </td>
                    </tr>
            
                    <!-- Colunas de Notícias -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;" class="content">
                            <table cellpadding="0" cellspacing="0" align="center" class="responsive-table" style="max-width:600px;margin-bottom:12px;" >
                                <tbody>
                                    <tr>
                                        <td class="two-column" style="padding-top: 0px; padding-bottom:0;">
                                            <div class="column col-1">
                                                <table id="{{news1.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news1" jqyoui-droppable>
                                                    <tbody>
                                                        <!-- Imagem do Destaque -->
                                                        <tr>
                                                            <td class="img-destaque" width="100%">

                                                                  <img src="{{news1.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news1.image" data-drag="false">

                                                            </td>
                                                        </tr>
                                                        <!-- Categoria -->
                                                        <tr height="36px">
                                                            <td height="36px">
                                                                <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                                </span>
                                                                <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                                  <span ng-hide="news1.category">Categoria</span>{{news1.category}}</span>
                                                            </td>
                                                        </tr>
                                                        <!-- Título -->
                                                        <tr>
                                                            <td height="36px">
                                                                <a href="#">
                                                                    <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news1.title}}<span ng-hide="news1.title">Título</span>
                                                                    </h1>
                                                                </a>
                                                                <span class="published">{{news1.date}}
                                                                  <span ng-hide="news1.date">Data de publicação</span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="column col-2">
                                                  <table id="{{news2.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news2"
                                                    jqyoui-droppable>
                                                    <tbody>
                                                      <!-- Imagem do Destaque -->
                                                      <tr>
                                                        <td class="img-destaque" width="100%">
                                                  
                                                          <img src="{{news2.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news2.image" data-drag="false">
                                                  
                                                        </td>
                                                      </tr>
                                                      <!-- Categoria -->
                                                      <tr height="36px">
                                                        <td height="36px">
                                                          <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                          </span>
                                                          <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                            <span ng-hide="news2.category">Categoria</span>{{news2.category}}</span>
                                                        </td>
                                                      </tr>
                                                      <!-- Título -->
                                                      <tr>
                                                        <td height="36px">
                                                          <a href="#">
                                                            <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news2.title}}
                                                              <span ng-hide="news2.title">Título</span>
                                                            </h1>
                                                          </a>
                                                          <span class="published">{{news2.date}}
                                                            <span ng-hide="news2.date">Data de publicação</span>
                                                          </span>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="two-column" style="padding-top:0px; padding-bottom:0;padding-right:0;padding-left:0;">
                                            <div class="column col-1">
                                              <table id="{{news3.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news3"
                                                jqyoui-droppable>
                                                <tbody>
                                                  <!-- Imagem do Destaque -->
                                                  <tr>
                                                    <td class="img-destaque" width="100%">
                                            
                                                      <img src="{{news3.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news3.image" data-drag="false">
                                            
                                                    </td>
                                                  </tr>
                                                  <!-- Categoria -->
                                                  <tr height="36px">
                                                    <td height="36px">
                                                      <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                      </span>
                                                      <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                        <span ng-hide="news3.category">Categoria</span>{{news3.category}}</span>
                                                    </td>
                                                  </tr>
                                                  <!-- Título -->
                                                  <tr>
                                                    <td height="36px">
                                                      <a href="#">
                                                        <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news3.title}}
                                                          <span ng-hide="news3.title">Título</span>
                                                        </h1>
                                                      </a>
                                                      <span class="published">{{news3.date}}
                                                        <span ng-hide="news3.date">Data de publicação</span>
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <div class="column col-2">
                                              <table id="{{news4.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news4"
                                                jqyoui-droppable>
                                                <tbody>
                                                  <!-- Imagem do Destaque -->
                                                  <tr>
                                                    <td class="img-destaque" width="100%">
                                            
                                                      <img src="{{news4.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news4.image" data-drag="false">
                                            
                                                    </td>
                                                  </tr>
                                                  <!-- Categoria -->
                                                  <tr height="36px">
                                                    <td height="36px">
                                                      <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                      </span>
                                                      <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                        <span ng-hide="news4.category">Categoria</span>{{news4.category}}</span>
                                                    </td>
                                                  </tr>
                                                  <!-- Título -->
                                                  <tr>
                                                    <td height="36px">
                                                      <a href="#">
                                                        <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news4.title}}
                                                          <span ng-hide="news4.title">Título</span>
                                                        </h1>
                                                      </a>
                                                      <span class="published">{{news4.date}}
                                                        <span ng-hide="news4.date">Data de publicação</span>
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>



                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            
                    <!-- DESTAQUE BG ESCURO -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;" class="content">
                            <div class="bg--escuro" style="margin-bottom:24px">
                                <table id="news{{destaque2.id}}" cellpadding="0" cellspacing="0" width="100%" align="center" class="destaque2" style="max-width:600px;"
                                  data-drop="true" data-jqyoui-options ng-model="destaque2" jqyoui-droppable>
                                  <tbody>
                                    <!-- Titulo destaque2 -->
                                    <tr height="48">
                                      <td class="title-section" width="100%" height="36" style="vertical-align:middle">
                                        <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px">
                                        </span>
                                        <span style="font-size:34px;line-height:36px;font-family:'Asap Condensed', sans-serif;color:#fff;font-weight:900; letter-spacing: -1px;">EM DESTAQUE</span>
                                      </td>
                                    </tr>
                                    <!-- Imagem do destaque2 -->
                                    <tr>
                                      <td class="img-destaque" height="100%" style="overflow:hidden!important; display:inline-block;" align="center">
                                        <img data-drag="false" src="{{destaque2.image}}" width="100%" height="400px" alt="Imagem de destaque2" style="margin:0 auto;"
                                          ng-hide="!destaque2.image">
                                      </td>
                                    </tr>
                                    <!-- Categoria -->
                                    <tr height="36px">
                                      <td height="36px">
                                        <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                        </span>
                                        <span class="category" style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#fff;font-weight:400; letter-spacing: -1px; vertical-align:middle; position:relative">
                                          <span ng-hide="destaque2.category">Categoria</span> {{destaque2.category}}
                                          <!-- <div class="placeholder"> </div>-->
                                        </span>
                                
                                      </td>
                                    </tr>
                                    <!-- Título -->
                                    <tr>
                                      <td height="36px">
                                        <a href="#">
                                          <h1 width="100%" style="color:#fff; font-size:36px; line-height: 36px; font-weight:600;">{{destaque2.title}}
                                            <span ng-hide="destaque2.title">Título</span>
                                          </h1>
                                        </a>
                                        <span class="published" style="color:#fff">{{destaque2.date}}
                                          <span ng-hide="destaque2.date">Data de publicação</span>
                                        </span>
                                      </td>
                                    </tr>
                                  </tbody>
                                
                                </table>
                            </div>
                        </td>
                    </tr>
            
                    <!-- Colunas de Notícias -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;" class="content">
                            <table cellpadding="0" cellspacing="0" align="center" class="responsive-table" style="max-width:600px;margin-bottom:12px;" >
                                <tbody>
                                    <tr>
                                        <td class="two-column" style="padding-top: 0px; padding-bottom:0;">
                                            <div class="column col-1">
                                                <table id="{{news5.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news5" jqyoui-droppable>
                                                    <tbody>
                                                        <!-- Imagem do Destaque -->
                                                        <tr>
                                                            <td class="img-destaque" width="100%">

                                                                  <img src="{{news5.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news5.image" data-drag="false">

                                                            </td>
                                                        </tr>
                                                        <!-- Categoria -->
                                                        <tr height="36px">
                                                            <td height="36px">
                                                                <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                                </span>
                                                                <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                                  <span ng-hide="news5.category">Categoria</span>{{news5.category}}</span>
                                                            </td>
                                                        </tr>
                                                        <!-- Título -->
                                                        <tr>
                                                            <td height="36px">
                                                                <a href="#">
                                                                    <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news5.title}}<span ng-hide="news5.title">Título</span>
                                                                    </h1>
                                                                </a>
                                                                <span class="published">{{news5.date}}
                                                                  <span ng-hide="news5.date">Data de publicação</span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="column col-2">
                                                  <table id="{{news6.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news6"
                                                    jqyoui-droppable>
                                                    <tbody>
                                                      <!-- Imagem do Destaque -->
                                                      <tr>
                                                        <td class="img-destaque" width="100%">
                                                  
                                                          <img src="{{news6.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news6.image" data-drag="false">
                                                  
                                                        </td>
                                                      </tr>
                                                      <!-- Categoria -->
                                                      <tr height="36px">
                                                        <td height="36px">
                                                          <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                          </span>
                                                          <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                            <span ng-hide="news6.category">Categoria</span>{{news6.category}}</span>
                                                        </td>
                                                      </tr>
                                                      <!-- Título -->
                                                      <tr>
                                                        <td height="36px">
                                                          <a href="#">
                                                            <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news6.title}}
                                                              <span ng-hide="news6.title">Título</span>
                                                            </h1>
                                                          </a>
                                                          <span class="published">{{news6.date}}
                                                            <span ng-hide="news6.date">Data de publicação</span>
                                                          </span>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="two-column" style="padding-top:0px; padding-bottom:0;padding-right:0;padding-left:0;">
                                            <div class="column col-1">
                                              <table id="{{news7.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news7"
                                                jqyoui-droppable>
                                                <tbody>
                                                  <!-- Imagem do Destaque -->
                                                  <tr>
                                                    <td class="img-destaque" width="100%">
                                            
                                                      <img src="{{news7.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news7.image" data-drag="false">
                                            
                                                    </td>
                                                  </tr>
                                                  <!-- Categoria -->
                                                  <tr height="36px">
                                                    <td height="36px">
                                                      <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                      </span>
                                                      <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                        <span ng-hide="news7.category">Categoria</span>{{news7.category}}</span>
                                                    </td>
                                                  </tr>
                                                  <!-- Título -->
                                                  <tr>
                                                    <td height="36px">
                                                      <a href="#">
                                                        <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news7.title}}
                                                          <span ng-hide="news7.title">Título</span>
                                                        </h1>
                                                      </a>
                                                      <span class="published">{{news7.date}}
                                                        <span ng-hide="news7.date">Data de publicação</span>
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <div class="column col-2">
                                              <table id="{{news8.id}}" cellpadding="0" cellspacing="0" align="center" data-drop="true" data-jqyoui-options ng-model="news8"
                                                jqyoui-droppable>
                                                <tbody>
                                                  <!-- Imagem do Destaque -->
                                                  <tr>
                                                    <td class="img-destaque" width="100%">
                                            
                                                      <img src="{{news8.image}}" width="100%" alt="Imagem de Destaque" ng-hide="!news8.image" data-drag="false">
                                            
                                                    </td>
                                                  </tr>
                                                  <!-- Categoria -->
                                                  <tr height="36px">
                                                    <td height="36px">
                                                      <span class="block-mark" style="width: 10px!important; height:24px!important; display: inline-block; margin-right:5px;vertical-align:middle;">
                                                      </span>
                                                      <span style="font-size:18px;line-height:24px;font-family:'Asap Condensed', sans-serif;color:#444;font-weight:400; letter-spacing: -1px; vertical-align:middle;">
                                                        <span ng-hide="news8.category">Categoria</span>{{news8.category}}</span>
                                                    </td>
                                                  </tr>
                                                  <!-- Título -->
                                                  <tr>
                                                    <td height="36px">
                                                      <a href="#">
                                                        <h1 width="100%" style="color:#444; font-size:20px; line-height: 24px; font-weight:600;"> {{news8.title}}
                                                          <span ng-hide="news8.title">Título</span>
                                                        </h1>
                                                      </a>
                                                      <span class="published">{{news8.date}}
                                                        <span ng-hide="news8.date">Data de publicação</span>
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            
                    <!-- ANÚNCIO -->
                    <tr>
                        <td align="center">
            
                            <div style="width:300px; height:250px; background-color:#4B4B4B; margin:12px auto;">
                                <img src="http://www.shakeout.org/2008/downloads/ShakeOut_BannerAds_GetReady_300x250_v3.gif">
            
                            </div>
            
                        </td>
                    </tr>
            
                    <!-- FOOTER -->
                    <tr>
                        <td align="center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" class="content-newsletter">
                            <div class="bg--footer" style="background-color:#A6A5A5;">
                                <table cellpadding="0" cellspacing="0" width="100%" height="300" align="center" class="responsive-table" style="max-width:600px; color:#444;">
                                    <tbody>
                                        <tr style="vertical-align:bottom!important;">
                                            <td align="center" class="btn-social">
                                                <h3 style="font-weight:400; text-transform:uppercase">Siga-nos nas redes sociais</h3>
                                                <a href="#" title="Curta a nossa Página no Facebook">
                                                    <img style="width:40px; margin:0px 5px;" alt="Instagram" src="data:image/svg+xml;base64,
                                                        PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDQ5LjY1MiA0OS42NTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ5LjY1MiA0OS42NTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0yNC44MjYsMEMxMS4xMzcsMCwwLDExLjEzNywwLDI0LjgyNmMwLDEzLjY4OCwxMS4xMzcsMjQuODI2LDI0LjgyNiwyNC44MjZjMTMuNjg4LDAsMjQuODI2LTExLjEzOCwyNC44MjYtMjQuODI2ICAgIEM0OS42NTIsMTEuMTM3LDM4LjUxNiwwLDI0LjgyNiwweiBNMzEsMjUuN2gtNC4wMzljMCw2LjQ1MywwLDE0LjM5NiwwLDE0LjM5NmgtNS45ODVjMCwwLDAtNy44NjYsMC0xNC4zOTZoLTIuODQ1di01LjA4OGgyLjg0NSAgICB2LTMuMjkxYzAtMi4zNTcsMS4xMi02LjA0LDYuMDQtNi4wNGw0LjQzNSwwLjAxN3Y0LjkzOWMwLDAtMi42OTUsMC0zLjIxOSwwYy0wLjUyNCwwLTEuMjY5LDAuMjYyLTEuMjY5LDEuMzg2djIuOTloNC41NkwzMSwyNS43eiAgICAiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgc3R5bGU9ImZpbGw6IzQ0NDQ0NCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiPjwvcGF0aD4KCTwvZz4KPC9nPjwvZz4gPC9zdmc+"
                                                    />
                                                </a>
                                                <a href="#" class="icon-instagram" title="Siga-nos no Instagram">
                                                    <img style="width:40px;margin:0px 5px;" alt="Instagram" src="data:image/svg+xml;base64,
                                                    PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDQ5LjY1MiA0OS42NTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ5LjY1MiA0OS42NTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48Zz4KCTxnPgoJCTxnPgoJCQk8cGF0aCBkPSJNMjQuODI1LDI5Ljc5NmMyLjczOSwwLDQuOTcyLTIuMjI5LDQuOTcyLTQuOTdjMC0xLjA4Mi0wLjM1NC0yLjA4MS0wLjk0LTIuODk3Yy0wLjkwMy0xLjI1Mi0yLjM3MS0yLjA3My00LjAyOS0yLjA3MyAgICAgYy0xLjY1OSwwLTMuMTI2LDAuODItNC4wMzEsMi4wNzJjLTAuNTg4LDAuODE2LTAuOTM5LDEuODE1LTAuOTQsMi44OTdDMTkuODU0LDI3LjU2NiwyMi4wODUsMjkuNzk2LDI0LjgyNSwyOS43OTZ6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIHN0eWxlPSJmaWxsOiM0NDQ0NDQiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BhdGg+CgkJCTxwb2x5Z29uIHBvaW50cz0iMzUuNjc4LDE4Ljc0NiAzNS42NzgsMTQuNTggMzUuNjc4LDEzLjk2IDM1LjA1NSwxMy45NjIgMzAuODkxLDEzLjk3NSAzMC45MDcsMTguNzYyICAgICIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBzdHlsZT0iZmlsbDojNDQ0NDQ0IiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCI+PC9wb2x5Z29uPgoJCQk8cGF0aCBkPSJNMjQuODI2LDBDMTEuMTM3LDAsMCwxMS4xMzcsMCwyNC44MjZjMCwxMy42ODgsMTEuMTM3LDI0LjgyNiwyNC44MjYsMjQuODI2YzEzLjY4OCwwLDI0LjgyNi0xMS4xMzgsMjQuODI2LTI0LjgyNiAgICAgQzQ5LjY1MiwxMS4xMzcsMzguNTE2LDAsMjQuODI2LDB6IE0zOC45NDUsMjEuOTI5djExLjU2YzAsMy4wMTEtMi40NDgsNS40NTgtNS40NTcsNS40NThIMTYuMTY0ICAgICBjLTMuMDEsMC01LjQ1Ny0yLjQ0Ny01LjQ1Ny01LjQ1OHYtMTEuNTZ2LTUuNzY0YzAtMy4wMSwyLjQ0Ny01LjQ1Nyw1LjQ1Ny01LjQ1N2gxNy4zMjNjMy4wMSwwLDUuNDU4LDIuNDQ3LDUuNDU4LDUuNDU3VjIxLjkyOXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgc3R5bGU9ImZpbGw6IzQ0NDQ0NCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiPjwvcGF0aD4KCQkJPHBhdGggZD0iTTMyLjU0OSwyNC44MjZjMCw0LjI1Ny0zLjQ2NCw3LjcyMy03LjcyMyw3LjcyM2MtNC4yNTksMC03LjcyMi0zLjQ2Ni03LjcyMi03LjcyM2MwLTEuMDI0LDAuMjA0LTIuMDAzLDAuNTY4LTIuODk3ICAgICBoLTQuMjE1djExLjU2YzAsMS40OTQsMS4yMTMsMi43MDQsMi43MDYsMi43MDRoMTcuMzIzYzEuNDkxLDAsMi43MDYtMS4yMSwyLjcwNi0yLjcwNHYtMTEuNTZoLTQuMjE3ICAgICBDMzIuMzQyLDIyLjgyMywzMi41NDksMjMuODAyLDMyLjU0OSwyNC44MjZ6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIHN0eWxlPSJmaWxsOiM0NDQ0NDQiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BhdGg+CgkJPC9nPgoJPC9nPgo8L2c+PC9nPiA8L3N2Zz4="
                                                    />
                                                </a>
                                                <a href="#" class="icon-youtube" title="Se inscreva nosso canal no Youtube">
                                                    <img style="width:40px;margin:0px 5px;" alt="Youtube" src="data:image/svg+xml;base64,
                                                    PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDk3Ljc1IDk3Ljc1IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5Ny43NSA5Ny43NTsiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxnPgoJPGc+CgkJPHBvbHlnb24gcG9pbnRzPSIyNS42NzYsNTIuNDgyIDI5LjU1MSw1Mi40ODIgMjkuNTUxLDczLjQ1NSAzMy4yMTcsNzMuNDU1IDMzLjIxNyw1Mi40ODIgMzcuMTY0LDUyLjQ4MiAzNy4xNjQsNDkuMDQ3ICAgICAyNS42NzYsNDkuMDQ3ICAgIiBzdHlsZT0iZmlsbDojNDQ0NDQ0IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BvbHlnb24+CgkJPHBhdGggZD0iTTU2LjY3NCw1NS4wNDZjLTEuMjEyLDAtMi4zNDMsMC42NjItMy40MDYsMS45NzJ2LTcuOTcyaC0zLjI5NXYyNC40MDloMy4yOTV2LTEuNzYyYzEuMTAzLDEuMzYxLDIuMjMzLDIuMDEzLDMuNDA2LDIuMDEzICAgIGMxLjMxMSwwLDIuMTkzLTAuNjksMi42MzMtMi4wNDRjMC4yMjEtMC43NzEsMC4zMzQtMS45ODIsMC4zMzQtMy42NjV2LTcuMjQyYzAtMS43MjItMC4xMTMtMi45MjQtMC4zMzQtMy42NTUgICAgQzU4Ljg2OCw1NS43MzYsNTcuOTg0LDU1LjA0Niw1Ni42NzQsNTUuMDQ2eiBNNTYuMzQ0LDY4LjI1NWMwLDEuNjQ0LTAuNDgyLDIuNDU0LTEuNDM0LDIuNDU0Yy0wLjU0MSwwLTEuMDkyLTAuMjU5LTEuNjQzLTAuODExICAgIFY1OC44MTRjMC41NTEtMC41NDUsMS4xMDItMC44MDMsMS42NDMtMC44MDNjMC45NTEsMCwxLjQzNCwwLjg0MiwxLjQzNCwyLjQ4MlY2OC4yNTV6IiBzdHlsZT0iZmlsbDojNDQ0NDQ0IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BhdGg+CgkJPHBhdGggZD0iTTQzLjgyNCw2OS4xNjdjLTAuNzMxLDEuMDMzLTEuNDIyLDEuNTQyLTIuMDg0LDEuNTQyYy0wLjQ0LDAtMC42OTEtMC4yNTktMC43NzEtMC43NzFjLTAuMDMtMC4xMDYtMC4wMy0wLjUwOC0wLjAzLTEuMjggICAgdi0xMy4zOWgtMy4yOTZ2MTQuMzc5YzAsMS4yODUsMC4xMTEsMi4xNTMsMC4yOTEsMi43MDVjMC4zMzEsMC45MjIsMS4wNjMsMS4zNTQsMi4xMjMsMS4zNTRjMS4yMTMsMCwyLjQ1Ny0wLjczMiwzLjc2Ny0yLjIzNCAgICB2MS45ODRoMy4yOThWNTUuMjY4aC0zLjI5OFY2OS4xNjd6IiBzdHlsZT0iZmlsbDojNDQ0NDQ0IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BhdGg+CgkJPHBhdGggZD0iTTQ2LjY1MywzOC40NjZjMS4wNzMsMCwxLjU4OC0wLjg1MSwxLjU4OC0yLjU1MXYtNy43MzFjMC0xLjcwMS0wLjUxNS0yLjU0OC0xLjU4OC0yLjU0OGMtMS4wNzQsMC0xLjU5LDAuODQ4LTEuNTksMi41NDggICAgdjcuNzMxQzQ1LjA2MywzNy42MTYsNDUuNTc5LDM4LjQ2Niw0Ni42NTMsMzguNDY2eiIgc3R5bGU9ImZpbGw6IzQ0NDQ0NCIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCI+PC9wYXRoPgoJCTxwYXRoIGQ9Ik00OC44NzUsMEMyMS44ODIsMCwwLDIxLjg4MiwwLDQ4Ljg3NVMyMS44ODIsOTcuNzUsNDguODc1LDk3Ljc1Uzk3Ljc1LDc1Ljg2OCw5Ny43NSw0OC44NzVTNzUuODY4LDAsNDguODc1LDB6ICAgICBNNTQuMzExLDIyLjg2aDMuMzIxdjEzLjUzMmMwLDAuNzgxLDAsMS4xODYsMC4wNCwxLjI5NWMwLjA3MywwLjUxNiwwLjMzNSwwLjc4LDAuNzgxLDAuNzhjMC42NjYsMCwxLjM2NS0wLjUxNiwyLjEwNC0xLjU1OSAgICBWMjIuODZoMy4zM3YxOC4zNzloLTMuMzN2LTIuMDA0Yy0xLjMyNiwxLjUyLTIuNTksMi4yNTctMy44MDUsMi4yNTdjLTEuMDcyLDAtMS44MTItMC40MzUtMi4xNDYtMS4zNjUgICAgYy0wLjE4NC0wLjU1Ny0wLjI5NS0xLjQzNi0wLjI5NS0yLjczM1YyMi44Nkw1NC4zMTEsMjIuODZ6IE00MS43MzMsMjguODUzYzAtMS45NjUsMC4zMzQtMy40MDEsMS4wNDItNC4zMyAgICBjMC45MjEtMS4yNTcsMi4yMTgtMS44ODUsMy44NzgtMS44ODVjMS42NjgsMCwyLjk2NCwwLjYyOCwzLjg4NSwxLjg4NWMwLjY5OCwwLjkyOCwxLjAzMiwyLjM2NSwxLjAzMiw0LjMzdjYuNDM2ICAgIGMwLDEuOTU0LTAuMzM0LDMuNDAzLTEuMDMyLDQuMzIyYy0wLjkyMSwxLjI1NC0yLjIxNywxLjg4MS0zLjg4NSwxLjg4MWMtMS42NiwwLTIuOTU3LTAuNjI3LTMuODc4LTEuODgxICAgIGMtMC43MDgtMC45MTktMS4wNDItMi4zNjktMS4wNDItNC4zMjJWMjguODUzeiBNMzIuODI3LDE2LjU3NmwyLjYyMiw5LjY4NWwyLjUxOS05LjY4NWgzLjczNUwzNy4yNiwzMS4yNTF2OS45ODloLTMuNjkydi05Ljk4OSAgICBjLTAuMzM1LTEuNzctMS4wNzQtNC4zNjMtMi4yNTktNy44MDNjLTAuNzc4LTIuMjg5LTEuNTg5LTQuNTg1LTIuMzY3LTYuODcySDMyLjgyN3ogTTc1LjE4Niw3NS4wNjEgICAgYy0wLjY2OCwyLjg5OS0zLjAzOSw1LjAzOS01Ljg5NCw1LjM1OGMtNi43NjMsMC43NTUtMTMuNjA0LDAuNzU5LTIwLjQyLDAuNzU1Yy02LjgxMywwLjAwNC0xMy42NTgsMC0yMC40MTktMC43NTUgICAgYy0yLjg1NS0wLjMxOS01LjIyNy0yLjQ1OC01Ljg5My01LjM1OGMtMC45NTEtNC4xMjktMC45NTEtOC42MzgtMC45NTEtMTIuODlzMC4wMTItOC43NiwwLjk2Mi0xMi44OSAgICBjMC42NjctMi45LDMuMDM3LTUuMDQsNS44OTItNS4zNThjNi43NjItMC43NTUsMTMuNjA2LTAuNzU5LDIwLjQyMS0wLjc1NWM2LjgxMy0wLjAwNCwxMy42NTcsMCwyMC40MTksMC43NTUgICAgYzIuODU1LDAuMzE5LDUuMjI3LDIuNDU4LDUuODk2LDUuMzU4YzAuOTQ4LDQuMTMsMC45NDIsOC42MzgsMC45NDIsMTIuODlTNzYuMTM3LDcwLjkzMiw3NS4xODYsNzUuMDYxeiIgc3R5bGU9ImZpbGw6IzQ0NDQ0NCIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCI+PC9wYXRoPgoJCTxwYXRoIGQ9Ik02Ny4xNyw1NS4wNDZjLTEuNjg2LDAtMi45OTUsMC42MTktMy45NDcsMS44NjRjLTAuNjk5LDAuOTItMS4wMTgsMi4zNDItMS4wMTgsNC4yODV2Ni4zNzEgICAgYzAsMS45MzMsMC4zNTcsMy4zNjUsMS4wNTksNC4yNzZjMC45NTEsMS4yNDIsMi4yNjQsMS44NjMsMy45ODgsMS44NjNjMS43MjEsMCwzLjA3Mi0wLjY1MSwzLjk4NC0xLjk3MiAgICBjMC40LTAuNTg0LDAuNjYtMS4yNDUsMC43Ny0xLjk3NWMwLjAzMS0wLjMzLDAuMDctMS4wNjEsMC4wNy0yLjEyNHYtMC40NzloLTMuMzYxYzAsMS4zMi0wLjA0MywyLjA1My0wLjA3MiwyLjIzMiAgICBjLTAuMTg4LDAuODgxLTAuNjYyLDEuMzIxLTEuNDczLDEuMzIxYy0xLjEzMiwwLTEuNjg2LTAuODQtMS42ODYtMi41MjJ2LTMuMjI2aDYuNTkydi0zLjc2N2MwLTEuOTQzLTAuMzI5LTMuMzY1LTEuMDItNC4yODUgICAgQzcwLjEzNSw1NS42NjYsNjguODI0LDU1LjA0Niw2Ny4xNyw1NS4wNDZ6IE02OC43ODIsNjIuMjE4aC0zLjI5NnYtMS42ODNjMC0xLjY4MiwwLjU1My0yLjUyMywxLjY1NC0yLjUyMyAgICBjMS4wOSwwLDEuNjQyLDAuODQyLDEuNjQyLDIuNTIzVjYyLjIxOHoiIHN0eWxlPSJmaWxsOiM0NDQ0NDQiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiPjwvcGF0aD4KCTwvZz4KPC9nPjwvZz4gPC9zdmc+"
                                                    />
                                                </a>
                                                <a href="#" class="icon-rss" title="Receba nossos feeds">
                                                    <img style="width:40px;margin:0px 5px;" src="data:image/svg+xml;base64,
                                                    PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48Zz4KCTxwYXRoIGQ9Ik0yNTYsMEMxMTQuNjE1LDAsMCwxMTQuNjE1LDAsMjU2czExNC42MTUsMjU2LDI1NiwyNTZzMjU2LTExNC42MTUsMjU2LTI1NlMzOTcuMzg1LDAsMjU2LDB6IE0xNjAsMzg0ICAgYy0xNy42NzMsMC0zMi0xNC4zMjctMzItMzJzMTQuMzI3LTMyLDMyLTMyczMyLDE0LjMyNywzMiwzMlMxNzcuNjczLDM4NCwxNjAsMzg0eiBNMjQ4LDM4NGMwLTY2LjE2OC01My44MzItMTIwLTEyMC0xMjB2LTQ4ICAgYzkyLjYzNiwwLDE2OCw3NS4zNjQsMTY4LDE2OEgyNDh6IE0zNDQsMzg0YzAtNTcuNjk1LTIyLjQ2OS0xMTEuOTM4LTYzLjI2Ni0xNTIuNzM1QzIzOS45MzgsMTkwLjQ2OCwxODUuNjk2LDE2OCwxMjgsMTY4di00OCAgIGM3MC41MTcsMCwxMzYuODEyLDI3LjQ2MSwxODYuNjc4LDc3LjMyNEMzNjQuNTM5LDI0Ny4xODcsMzkyLDMxMy40ODIsMzkyLDM4NEgzNDR6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIHN0eWxlPSJmaWxsOiM0NDQ0NDQiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIj48L3BhdGg+CjwvZz48L2c+IDwvc3ZnPg=="
                                                    />
                                                </a>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:middle!important; line-height:18px;" height="144" class="footer-info">
                                            <td align="center">
                                                <address>
                                                    <p>Endereço, endereço, endereco</p>
                                                    <p>Endereço, endereço, endereco</p>
                                                    <br/>
                                                </address>
                                                <p style="font-size:12px; color:#4B4B4B; line-height:24px">Se você não deseja mais receber esta Newsletter, cancele sua inscrição
                                                    <a href="#" style="font-size:12px; color:#4B4B4B;">aqui</a>
                                                </p>
                                                <p style="font-size:12px; color:#4B4B4B; line-height:24px">ⓒ 2018 Copyright Nome do Jornal | Todos os direitos reservados</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="btn-publish">
                <button ng-click="sendDatas()"> Publicar</button>
            </div>
        </div>
        <div class="sidebar">
            <div class="news-list__filter">
                <input type="text" id="news-list-filter" name="news-list-filter" ng-model="filterNews" placeholder="filtrar">
            </div>

            <ul class="news-list">
                <li ng-repeat="newsItem in news | orderBy : '-date' | filter: filterNews" class="news-list__item" >
                    <div id="item{{newsItem.id}}" class="news-block" data-drag="true" jqyoui-draggable="{animate:true, placeholder:true}" ng-hide="!newsItem.id" ng-model="newsItem">
                        <div class="news-block__img">
                            <img src="{{newsItem.image}}" />
                        </div>
                        <div class="news-block__text">
                            <span class="category">{{newsItem.category}}</span>
                            <h1>{{newsItem.title}}</h1>
                            <span class="date-published" role="date" content="{{newsItem.date}}">{{newsItem.date}}</span>
                            <summary>{{newsItem.summary}}</summary>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
        
    </div>

    </script>

</body>

</html>