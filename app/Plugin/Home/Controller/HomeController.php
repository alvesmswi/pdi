<?php
App::uses('HomeAppController', 'Home.Controller');

  class HomeController extends HomeAppController {

    public $name = 'Home';
    public $uses = array('Home.Home', 'Settings.Setting', 'Nodes.Node');    
    
    public function beforeFilter() {
        parent::beforeFilter();
        //desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
        $this->Security->authCheck = false;
        $this->Security->unlockedActions += array('admin_index', 'admin_montar','admin_search_node');
    }

    public function admin_montar()
    {
        $this->layout ='ajax';
    }
    public function admin_index()
    {
        $this->set('title_for_layout','Configurações da Home');     
        
        
        if (!empty($this->request->data)) {
            foreach($this->request->data as $model => $value){
                foreach($value as $key => $v){
                    //procura pra ver se existe
                    $setting = $this->Setting->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Setting.key' => $model.'.'.$key
                            ),
                            'recursive' => -1
                        )
                    );
                    //se já existe
                    if(!empty($setting)){
                        //atualiza
                        $this->Setting->updateAll(
                            array(
                                'Setting.value' => "'".$v."'"
                            ),
                            array(
                                'Setting.id' => $setting['Setting']['id']
                            )
                        );
                    }else{
                        //cria
                        $arraySave = array(
                            'id' => null,
                            'key' => $model.'.'.$key,
                            'value' => $v,
                            'description' => '',
                            'params' => '',
                            'title' => $model.'.'.$key,
                            'editable' => 1
                        );
                        $this->Setting->saveAll($arraySave);
                        //limpa o cache da home
                        Cache::clear(false, 'home_');
                    }                    
                }               
            }
        }

        //limpa o this->data
        $this->request->data = array();

        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Home.%'),'recursive' => -1)
        );
        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                //se tem opções
                $arrayOptions = explode(',',trim($dados['Setting']['value']));
                //se encntrou e é numero
                if(!empty($arrayOptions) && is_numeric($arrayOptions[0])){
                    //procura os nodes
                    $sql = "SELECT id, title FROM nodes WHERE id IN(".$dados['Setting']['value'].");";
                    $arrayNodes = $this->Home->query($sql);
                    //se encontrou alguma coisa
                    if(!empty($arrayNodes)){    
                        $hiddenValue = array();               
                        foreach($arrayNodes as $node){
                            $hiddenValue[] = $node['nodes'];   
                        }
                        $arrayKey = explode('.',$dados['Setting']['key']);
                        $key = $arrayKey[1];
                        $key = 'hidden_'.$key;
                        $this->request->data['Home'][$key] = json_encode($hiddenValue);                      
                    }
                }
                $arrayKey = explode('.',$dados['Setting']['key']);
                $key = $arrayKey[1];
                $this->request->data['Home'][$key] = $dados['Setting']['value'];
            }
        }
        
    }

    public function admin_search_node($search = '')
    {
        Configure::write('debug',0);
        $this->autoRender = false;
        $keyword = (isset($this->data['keyword']) && !empty($this->data['keyword'])) ? trim($this->data['keyword']) : $search;

        $sql = "
        SELECT 
            Node.id, 
            Node.title, 
            Node.type 
        FROM nodes AS Node
        WHERE Node.type = 'noticia' AND (Node.id = '$keyword' OR Node.title LIKE '%$keyword%') 
        ORDER BY Node.publish_start DESC
        LIMIT 20;
        ";

        $nodes = $this->Home->query($sql);

        $response['nodes'] = array();
        if (!empty($nodes)) {
            foreach ($nodes as $key => $node) {
                $response['nodes'][$key]['id'] = $node['Node']['id'];
                $response['nodes'][$key]['title'] = $node['Node']['title'];
                $response['nodes'][$key]['type'] = $node['Node']['type'];
            }
        }

        return json_encode($response);
    }

    public function admin_get_news($search = '')
    {
        //$this->log($this->params);
        Configure::write('debug',0);
        $this->autoRender = false;

        $sql = "
        SELECT 
            Node.id, 
            Node.title, 
            Node.excerpt,
            Node.publish_start,
            Node.terms,              
            NoticiumDetail.chapeu,
            NoticiumDetail.titulo_capa, 
            Multiattach.filename 
        FROM nodes AS Node
        LEFT JOIN multiattachments AS Multiattach ON(Multiattach.node_id = Node.id)
        LEFT JOIN noticium_details AS NoticiumDetail ON(NoticiumDetail.node_id = Node.id)
        GROUP BY Node.id 
        ORDER BY Node.publish_start DESC
        LIMIT 3;
        ";

        $nodes = $this->Home->query($sql);
//pr($nodes);exit();
        $response = array();
        if (!empty($nodes)) {
            foreach ($nodes as $key => $node) {
                //pega o termo

                $response[$key]['id'] = $node['Node']['id'];
                $response[$key]['date'] = $node['Node']['publish_start'];
                $response[$key]['category'] = $node['NoticiumDetail']['chapeu'];
                $response[$key]['title'] = $node['Node']['title'];
                $response[$key]['image'] = '/fl/600largura/'.$node['Multiattach']['filename'];
                $response[$key]['summary'] = $node['Node']['excerpt'];
            }
        }

        echo json_encode($response);
    }
}
?>