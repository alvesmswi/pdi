<?php
App::uses('OnesignalAppController', 'Onesignal.Controller');

  class OnesignalController extends OnesignalAppController {

    public $name = 'Onesignal';
    public $uses = array('Onesignal.Onesignal', 'Settings.Setting');    
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('manifest');
        //desabilita o CSRF
		$this->Security->enabled = false;
		$this->Security->csrfCheck = false;
		$this->Security->authCheck = false;
    }

    public function admin_index()
    {
        //$cache = new \Memcached();
        //$cache->addServer('localhost', '11211');
        //Configure::write('debug',1);
        //$this->autoRender = false;
        //$caches = Cache::read('diario_cake_model_default_diariodosudoeste_nodes','_cake_model_');
        //$caches = Cache::read('diario_cake_model_default_diariodosudoeste_nodes','default');
        //$caches = Cache::settings();
        //Cache::clearGroup('diario_cake_model_default_diariodosudoeste_nodes');
        //Cache::clear(false, 'diario_cake_model_default_diariodosudoeste_nodes');
        //pr($caches);
        
        //$keys = $cache->getAllKeys();
        //var_dump($cache->get('diario_cake_model_default_diariodosudoeste_nodes'));
        //pr($keys);
              // Cache::clear(false, '_cake_model_');
        $this->set('title_for_layout','Onesignal');     
        
        if (!empty($this->request->data)) {
            //pr($this->data);exit();
            if($this->Setting->deleteAll(
                array('Setting.key LIKE' => 'Onesignal.%')
            )){
                foreach($this->request->data['Onesignal'] as $key => $value){
                    $arraySave = array();
                    $arraySave['id']            = NULL;
                    $arraySave['editable']      = 0;
                    $arraySave['input_type']    = 'text';
                    $arraySave['key']           = 'Onesignal.'.$key;
                    $arraySave['value']         = $value;
                    $arraySave['title']         = $key;

                    $this->Setting->create();
                    if ($this->Setting->saveAll($arraySave)) {
                        $this->Session->setFlash('Configuração salva com sucesso', 'flash', array('class' => 'success'));
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao gravar os dados', 'flash', array('class' => 'error'));
                    }
                }
            }
        } 

        //limpa o this->data
        $this->request->data = array();

        //Pega os dados
        $arrayDados = $this->Setting->find(
            'all',
            array( 'conditions' => array('key LIKE' => 'Onesignal.%'),'recursive' => -1)
        );

        if(!empty($arrayDados)){
            foreach($arrayDados as $dados){
                    $this->request->data['Onesignal'][$dados['Setting']['title']] = $dados['Setting']['value'];
            }
        }
        
    }

    public function admin_create($nodeId, $type='noticia')
    {
        $this->autoRender = false;

        $baseUrl = FULL_BASE_URL;
        $baseUrl = str_replace('http:', 'https:', $baseUrl);
        $baseUrl = str_replace('mswi.', '', $baseUrl);

        //se é uma noticia
        if($type=='noticia'){
            $this->loadModel('Node.Node');
            //Busca o node
            $node = $this->Node->find(
                'first',
                array(
                    'conditions' => array(
                        'Node.id' => $nodeId,
                        //'Node.type' => 'noticia',
                        'Node.status' => 1
                    ),
                    'fields' => array(
                        'Node.id',
                        'Node.title',
                        'Node.path',
                        'Node.excerpt',
                        'Node.exclusivo'
                    ),
                    'recursive' => -1
                )
            );
            //variaveis
            $title      = $node['Node']['title'];
            $url        = $baseUrl.$node['Node']['path'];
            $exclusivo  = $node['Node']['exclusivo']; 

            //se tem resumo
            if(!empty($node['Node']['excerpt'])){
                $chamada = $node['Node']['excerpt'];
            }else{
                $chamada = $title;
            }
            //se a noticia tem imagem
            if(isset($node['Multiattach'][0]['Multiattach']['filename']) && !empty($node['Multiattach'][0]['Multiattach']['filename'])){
                $img = $baseUrl.'/fl/thumb/'.$node['Multiattach'][0]['Multiattach']['filename'];
            }else{
                $img = $baseUrl.'/img/logo.png';
            }
            $redirect   = '/admin/nodes';
        }

        //se é uma post
        if($type=='post'){
            //Busca o Post
            $sql = "SELECT Post.id, Post.title, Post.excerpt, Post.slug, Blog.title, Blog.slug, Image.filename   
            FROM blog_nodes AS Post 
            LEFT JOIN blogs AS Blog ON(Blog.id = Post.blog_id) 
            LEFT JOIN media_images AS Image ON(Image.node_id = Post.id AND Image.controller = 'blogs') 
            WHERE Post.id = $nodeId;";           
            $node = $this->Onesignal->query($sql);
            //variaveis
            $title      = $node[0]['Blog']['title'];
            $url        = $baseUrl.'/blog/'.$node[0]['Blog']['slug'].'/post/'.$node[0]['Post']['slug'];
            $chamada    = $node[0]['Post']['title'];
            $exclusivo  = 0; 

            //se a noticia tem imagem
            if(isset($node[0]['Image']['filename']) && !empty($node[0]['Image']['filename'])){
                $img = $baseUrl.'/media/images/300/'.$node[0]['Image']['filename'];
            }else{
                $img = $baseUrl.'/img/logo.png';
            }
            $redirect   = '/admin/blog/posts';
        }
        
        //Prepara o array do Push
        $arrayPush = array();
        $arrayPush['app_id']                = Configure::read('Onesignal.oneSignalId');
        $arrayPush['included_segments']     = 'All';//Todos
        $arrayPush['headings']['en']        = $title;
        $arrayPush['contents']['en']        = $chamada;
        $arrayPush['small_icon']            = $img;
        $arrayPush['large_icon']            = $img;
        $arrayPush['chrome_web_icon']       = $img;

        // Para os Aplicativos | Marcelo Carvalho - 08/02/2019
        $arrayPush['data']                  = array('id' => $nodeId, 'exclusivo' => $exclusivo);
        $arrayPush['buttons']               = array(array('id' => 'cancel', 'text' => 'Cancelar'));
        $arrayPush['web_url']               = $url; // Para poder enviar o APP URL
        $arrayPush['app_url']               = '';

        //envia o push
        if($this->Onesignal->oneSignalCreatePush($arrayPush)){
            //marca como enviado
            $this->Onesignal->query("UPDATE nodes SET onesignal_push = 1 WHERE id = $nodeId");
            $this->Session->setFlash('Os inscritos foram notificados! Registro <b>'.$nodeId.'</b>!', 'flash', array('class' => 'success', 'escape'=>false));
        }else{
            $this->Session->setFlash('Ocorreu um erro ao tentar notificar os inscritos. Registros <b>'.$nodeId.'</b>!', 'flash', array('class' => 'error', 'escape'=>false));
        }
        $this->redirect($redirect);
        
    }	

    public function manifest(){
        $this->layout = 'ajax';
    }
}
?>