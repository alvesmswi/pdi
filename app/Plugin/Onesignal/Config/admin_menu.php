<?php
CroogoNav::add('sidebar', 'settings.children.onesignal', array(
	'icon' => 'link',
	'title' => 'Onesignal',
	'url' => array(
		'admin' => true,
		'plugin' => 'onesignal',
		'controller' => 'onesignal',
		'action' => 'index',
	)
));