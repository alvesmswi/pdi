<?php
Croogo::hookRoutes('onesignal');

CroogoNav::add('settings.children.onesignal',array(
	'title' => 'Onesignal',
	'url' => array('plugin' => 'onesignal', 'controller' => 'onesignal', 'action' => 'admin_index'),
	'access' => array('admin')
));
  /**
   * Admin menu
   */
  Croogo::hookAdminMenu('Onesignal');
    
?>