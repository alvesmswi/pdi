<?php
class OnesignalActivation {
	public function beforeActivation(Controller $controller) 
	{
		return true;
	}
	
    public function onActivation(Controller $controller) {
        //da permissões para acessar plugin
        $controller->Croogo->addAco('Onesignal');
		$controller->Croogo->addAco('Onesignal/Onesignal/admin_index');
		$controller->Croogo->addAco('Onesignal/Onesignal/admin_create');

		//cria a região
		$controller->loadModel('Region');
		$region = $controller->Region->find('first', array('conditions'=>array('Region.alias'=>'header_scripts')));
		//se a região não existe
		if(empty($region)){
			$controller->Region->create();
			$controller->Region->set(
				array(
					'title'            => 'Scripts no Header',
					'alias'            => 'header_scripts',
					'description'      => 'Scripts no Header'
				)
			);
			$controller->Region->save();
			$regionId = $controller->Region->id;	
		}else{
			$regionId = $region['Region']['id'];
		} 

		//Cria o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'onesignal')));
		//se o Bloco não existe
		if(empty($block)){
			$controller->Block->create();
			$controller->Block->set(array(
				'visibility_roles' => '',
				'visibility_paths' => '',
				'region_id'        => $regionId,
				'title'            => 'OneSignal',
				'alias'            => 'onesignal',
				'body'             => '[element:onesignal_tag plugin="onesignal"]',
				'show_title'       => 0,
				'status'           => 1
			));
			$controller->Block->save();
		}

		$this->moveFiles();
		
		//CRIA UM CAMPO NA TABELA NODES PARA INFORMAR SE JÁ FOI NOTIFICADO
		$controller->Block->query('ALTER TABLE nodes ADD onesignal_push TINYINT(1) DEFAULT 0');

		Cache::clear(false, '_cake_model_');

    }

	public function beforeDeactivation(Controller $controller) 
	{
		return true;
	}

	public function onDeactivation(Controller $controller) 
	{
		$controller->Croogo->removeAco('Onesignal');
		//Apaga o bloco
		$controller->loadModel('Block');
		$block = $controller->Block->find('first', array('conditions'=>array('Block.alias'=>'onesignal')));
        if($block){
            $controller->Block->delete($block['Block']['id']);
		} 
		$controller->Block->query('ALTER TABLE nodes DROP onesignal_push');
	}

	function moveFiles(){
		//monta o diretorio para buscar os arquivos
		$diretorio = App::pluginPath('Onesignal') .'Webroot';
		$dir = new Folder($diretorio);
		//procura todos os arquivos
		$files = $dir->find('.*.');
		//se encontrou algum arquivo
		if(!empty($files)){
			//percore os arquivos
			foreach($files as $file){
				//move o arquivos
				$arq1 = new File($diretorio.DS.$file);
				if ($arq1->exists()) {
					//Copia o arquivo para o root (public)
				    $arq2 = new Folder(WWW_ROOT, true);
					$arq1->copy($arq2->path . DS . $arq1->name);
				}
			}
		}
	}
}