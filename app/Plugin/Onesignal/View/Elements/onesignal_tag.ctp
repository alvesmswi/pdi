<link rel="manifest" href="/onesignal/onesignal/manifest">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<?php
//se já foi configurado corretamente o Plugin
if(Configure::read('Onesignal.oneSignalId') != '' && Configure::read('Onesignal.oneSignalKey') != ''){
    $position = 'bottom-right';
    $configOneSignal = Configure::read('Onesignal.position');
    if($configOneSignal == 1){
        $position = 'bottom-left';
    }
?>    
    <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
        appId: "<?php echo Configure::read('Onesignal.oneSignalId'); ?>",
        promptOptions: {
            actionMessage: "Nós gostaríamos de enviar notificações com as últimas notícias da cidade e região.",
            acceptButtonText: "Permitir",
            cancelButtonText: "Não, obrigado"
        },
        welcomeNotification: {
            "title": "<?php echo Configure::read('Site.title') ?>",
            "message": "Obrigado por se inscrever!"
        },
        notifyButton: {
            enable: true, /* Required to use the notify button */
            size: 'large', /* One of 'small', 'medium', or 'large' */
            theme: 'default', /* One of 'default' (red-white) or 'inverse" (white-red) */
            position: '<?php echo $position; ?>', /* Either 'bottom-left' or 'bottom-right' */
            prenotify: true, /* Show an icon with 1 unread message for first-time site visitors */
            showCredit: false, /* Hide the OneSignal logo */
            text: {
                'tip.state.unsubscribed': 'Inscreva-se para receber notificações',
                'tip.state.subscribed': "Você está inscrito para receber notificações",
                'tip.state.blocked': "Você bloqueou as notificações",
                'message.prenotify': 'Clique para assinar as notificações',
                'message.action.subscribed': "Obrigado por se inscrever!",
                'message.action.resubscribed': "Você está inscrito em notificações",
                'message.action.unsubscribed': "Você não receberá notificações novamente",
                'dialog.main.title': 'Gerenciar notificações do site',
                'dialog.main.button.subscribe': 'SE INSCREVER',
                'dialog.main.button.unsubscribe': 'CANCELAR INSCRIÇÃO',
                'dialog.blocked.title': 'Desbloquear notificações',
                'dialog.blocked.message': "Siga estas instruções para permitir notificações:"
            }
        }
    }]);
    OneSignal.push(function() {
        OneSignal.showHttpPrompt();
    });
    </script>
<?php } ?>