<?php
$this->extend('/Common/admin_edit');

$this->Html
  ->addCrumb($this->Html->icon('home'), '/admin')
  ->addCrumb('Onesignal', array(
    'admin' => true,
    'plugin' => 'onesignal',
    'controller' => 'onesignal',
    'action' => 'index',
  ));

$this->append('form-start', $this->Form->create('Onesignal', array(
    'class' => 'protected-form',
)));

$this->start('tab-heading');
    echo $this->Croogo->adminTab('Configurações', '#tab-config');
$this->end();

$this->start('tab-content');
    echo $this->Html->tabStart('tab-config');
       echo $this->Form->input('Onesignal.oneSignalId', array(
            'label' => 'ID do OneSignal',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Onesignal.oneSignalKey', array(
            'label' => 'Chave do OneSignal',
            'type'  => 'text',
            'required'	=> true
        )) .
        $this->Form->input('Onesignal.position', array(
            'label' => 'Posicionado na esquerda',
            'type'  => 'checkbox'
        ));
    echo $this->Html->tabEnd();

    echo $this->Croogo->adminTabs();
$this->end();

$this->start('panels');
    echo $this->Html->beginBox('Publicação') .
        $this->Form->button('Salvar', array('button' => 'default')) .
        $this->Html->link('Cancelar', array('action' => 'index'), array(
            'button' => 'danger')
        ) .
$this->Html->endBox();

    echo $this->Croogo->adminBoxes();
$this->end();
$this->append('form-end', $this->Form->end());